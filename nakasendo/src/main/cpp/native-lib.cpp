

#include <jni.h>
#include <string>
#include <cstdlib>
//#include "node.h"




#include "include/handle.h"


#include <BigNumbers/BigNumbers.h>
#include <iostream>
#include <memory>

#include <map>


#if 0
Sum = c590e57ee64fcef321395bba088ca0a867e1e85a1ea77478f8783e6a6cf8f3e582bff83cb2d7d9fd549fcbb40dea22ac140351007030059500bdca81413600e9
A = c590e57ee64fced18aff6e2f0c6ac05625b1e94f394f42470cae14d12cadea4f5ab6b9d77225fe3b4903825966c78752ae51b6a0a2caca555fd0ffcbd9704b01
B = 219639ed8afc21e052422fff0ae5583231ebca2999404b099628093e6540b1dbc20b9c495aa7229b5965b19a5fcd653b3fa0eccab567c5b5e8
#endif






#include <pthread.h>
#include <unistd.h>
#include <android/log.h>

// Start threads to redirect stdout and stderr to logcat.
int pipe_stdout[2];
int pipe_stderr[2];
pthread_t thread_stdout;
pthread_t thread_stderr;
const char *ADBTAG = ">>NAKASENDO-JNI";

void *thread_stderr_func(void*) {
    ssize_t redirect_size;
    char buf[2048];
    while((redirect_size = read(pipe_stderr[0], buf, sizeof buf - 1)) > 0) {
        //__android_log will add a new line anyway.
        if(buf[redirect_size - 1] == '\n')
            --redirect_size;
        buf[redirect_size] = 0;
        __android_log_write(ANDROID_LOG_ERROR, ADBTAG, buf);
    }
    return 0;
}

void *thread_stdout_func(void*) {
    ssize_t redirect_size;
    char buf[2048];
    while((redirect_size = read(pipe_stdout[0], buf, sizeof buf - 1)) > 0) {
        //__android_log will add a new line anyway.
        if(buf[redirect_size - 1] == '\n')
            --redirect_size;
        buf[redirect_size] = 0;
        __android_log_write(ANDROID_LOG_INFO, ADBTAG, buf);
    }
    return 0;
}

int start_redirecting_stdout_stderr() {
    //set stdout as unbuffered.
    setvbuf(stdout, 0, _IONBF, 0);
    pipe(pipe_stdout);
    dup2(pipe_stdout[1], STDOUT_FILENO);

    //set stderr as unbuffered.
    setvbuf(stderr, 0, _IONBF, 0);
    pipe(pipe_stderr);
    dup2(pipe_stderr[1], STDERR_FILENO);

    if(pthread_create(&thread_stdout, 0, thread_stdout_func, 0) == -1)
        return -1;
    pthread_detach(thread_stdout);

    if(pthread_create(&thread_stderr, 0, thread_stderr_func, 0) == -1)
        return -1;
    pthread_detach(thread_stderr);

    return 0;
}

//todo must kill threads correctly
/*
All threads are Linux threads, scheduled by the kernel. They're usually started from managed code (using Thread.start()), but they can also be created elsewhere and then attached to the JavaVM. For example, a thread started with pthread_create() or std::thread can be attached using the AttachCurrentThread() or AttachCurrentThreadAsDaemon() functions. Until a thread is attached, it has no JNIEnv, and cannot make JNI calls.

It's usually best to use Thread.start() to create any thread that needs to call in to Java code. Doing so will ensure that you have sufficient stack space, that you're in the correct ThreadGroup, and that you're using the same ClassLoader as your Java code. It's also easier to set the thread's name for debugging in Java than from native code (see pthread_setname_np() if you have a pthread_t or thread_t, and std::thread::native_handle() if you have a std::thread and want a pthread_t).

Attaching a natively-created thread causes a java.lang.Thread object to be constructed and added to the "main" ThreadGroup, making it visible to the debugger. Calling AttachCurrentThread() on an already-attached thread is a no-op.

Android does not suspend threads executing native code. If garbage collection is in progress, or the debugger has issued a suspend request, Android will pause the thread the next time it makes a JNI call.

Threads attached through JNI must call DetachCurrentThread() before they exit. If coding this directly is awkward, in Android 2.0 (Eclair) and higher you can use pthread_key_create() to define a destructor function that will be called before the thread exits, and call DetachCurrentThread() from there. (Use that key with pthread_setspecific() to store the JNIEnv in thread-local-storage; that way it'll be passed into your destructor as the argument.)
 */



extern "C"
JNIEXPORT void JNICALL
Java_wrap_nakasendo_kit_NakasendoSDK_init(JNIEnv *env, jclass clazz) {
    //Start threads to show stdout and stderr in logcat.
    if (start_redirecting_stdout_stderr()==-1) {
        __android_log_write(ANDROID_LOG_ERROR, ADBTAG, "Couldn't start redirecting stdout and stderr to logcat.");
    }
}


//extern "C"
//JNIEXPORT jstring JNICALL
//Java_com_yourorg_sample_MainActivity_stringFromJNI(
//        JNIEnv *env,
//        jobject /* this */) {
//    std::string hello = "Hello from C++";
//    return env->NewStringUTF(hello.c_str());
//}


//extern "C" JNIEXPORT jint JNICALL
//Java_app_bitcoin_moneybutton_1interface_Node_startNodeWithArguments(
//        JNIEnv* env,
//        jobject /* this */,
//        jobjectArray arguments) {
//
////    std::string hello = "Hello from C++";
////    return env->NewStringUTF(hello.c_str());
//
//    //argc
//    jsize argument_count = env->GetArrayLength(arguments);
//
//    //Compute byte size need for all arguments in contiguous memory.
//    int c_arguments_size = 0;
//    for (int i = 0; i < argument_count ; i++) {
//        c_arguments_size += strlen(env->GetStringUTFChars((jstring)env->GetObjectArrayElement(arguments, i), 0));
//        c_arguments_size++; // for '\0'
//    }
//
//    //Stores arguments in contiguous memory.
//    char* args_buffer = (char*) calloc(c_arguments_size, sizeof(char));
//
//    //argv to pass into node.
//    char* argv[argument_count];
//
//    //To iterate through the expected start position of each argument in args_buffer.
//    char* current_args_position = args_buffer;
//
//    //Populate the args_buffer and argv.
//    for (int i = 0; i < argument_count ; i++)
//    {
//        const char* current_argument = env->GetStringUTFChars((jstring)env->GetObjectArrayElement(arguments, i), 0);
//
//        //Copy current argument to its expected position in args_buffer
//        strncpy(current_args_position, current_argument, strlen(current_argument));
//
//        //Save current argument start position in argv
//        argv[i] = current_args_position;
//
//        //Increment to the next argument's expected position.
//        current_args_position += strlen(current_args_position) + 1;
//    }
//
//    //Start threads to show stdout and stderr in logcat.
//    if (start_redirecting_stdout_stderr()==-1) {
//        __android_log_write(ANDROID_LOG_ERROR, ADBTAG, "Couldn't start redirecting stdout and stderr to logcat.");
//    }
//
//    //Start node, with argc and argv.
//    int node_result = node::Start(argument_count, argv);
//    free(args_buffer);
//
//    return jint(node_result);
//
//}

void testSomeBigNumbers() {
    BigNumber val;

    std::string SumA("");
    std::string AVal = "c590e57ee64fced18aff6e2f0c6ac05625b1e94f394f42470cae14d12cadea4f5ab6b9d77225fe3b4903825966c78752ae51b6a0a2caca555fd0ffcbd9704b01";
    std::string BVal("219639ed8afc21e052422fff0ae5583231ebca2999404b099628093e6540b1dbc20b9c495aa7229b5965b19a5fcd653b3fa0eccab567c5b5e8");

    BigNumber BNValA, BNValB;
    BNValA.FromHex(AVal);
    BNValB.FromHex(BVal);


    std::cout << BNValA.ToHex() << "\n" << BNValB.ToHex() << std::endl;

    BigNumber Sum = BNValA + BNValB;
    std::cout << Sum.ToHex() << std::endl;

    {
        const BigNumber bn_256 = GenerateRand(256);
        const BigNumber bn_512 = GenerateRand(512);
        const BigNumber bn_1024 = GenerateRand(1024);
        const BigNumber bn_2048 = GenerateRand(2048);

        std::cout << "256-bit dec\n" << bn_256.ToDec() << "\n"
                  << "512-bit dec\n" << bn_512.ToDec() << "\n"
                  << "1024-bit dec\n" << bn_1024.ToDec() << "\n"
                  << "2048-bit dec\n" << bn_2048.ToDec()
                  << std::endl;
    }
    {
        const BigNumber bn_256 = GenerateRand(256);
        const BigNumber bn_512 = GenerateRand(512);
        const BigNumber bn_1024 = GenerateRand(1024);
        const BigNumber bn_2048 = GenerateRand(2048);

        std::cout << "256-bit hex\n" << bn_256.ToHex()  << "\n"
                  << "512-bit hex\n" << bn_512.ToHex() << "\n"
                  << "1024-bit hex\n" << bn_1024.ToHex() << "\n"
                  << "2048-bit hex\n" << bn_2048.ToHex()
                  << std::endl;
    }
    /// Comment out this code because it discover some design issue : the code can compile only in C++ and not compile in wasm
    //BIGNUM_RETURN_TYPE SubRes = subFromHex(BNRandomHex (2048).get(), BNRandomHex (2048).get () );
    //std::cout << "Result of 2048-bit subtraction " << SubRes.get() << std::endl ;


    //std::string StrSmallNumberMin ("-5");
    //std::string strSmallNumberMax ("5");

    std::string StrSmallNumberMin("1000");
    std::string strSmallNumberMax("100000000000000000000");

    BigNumber minVal, maxVal;
    minVal.FromDec(StrSmallNumberMin);
    maxVal.FromDec(strSmallNumberMax);

    for (int i = 0; i < 10000; ++i)
    {
        BigNumber randRange = GenerateRandRange(minVal, maxVal);
        //std::cout << randRange.ToDec () << std::endl ;


        // check
        if (randRange < minVal || randRange > maxVal)
        {
            std::cout << "success on random range" << std::endl;
        }
    }


    BigNumber seedval;
    seedval.generateRandHex(1024);
    BigNumber seededRanVal;
    seededRanVal.generateRandHexWithSeed(seededRanVal.ToHex(), 1024);

    std::cout << " Random number generated with seed " << "\n"
              << seededRanVal.ToHex() << std::endl;
    ++seededRanVal;
    std::cout << " pre- Incremented " << seededRanVal.ToHex() << std::endl;
    seededRanVal++;
    std::cout << " post - Incremented " << seededRanVal.ToHex() << std::endl;

    --seededRanVal;
    std::cout << " pre - decremented " << seededRanVal.ToHex() << std::endl;

    seededRanVal--;
    std::cout << " post - decremented " << seededRanVal.ToHex() << std::endl;


    std::cout << "Initialise with the seed: The quick brown fox jumped over the lazy dog" << std::endl;


    // A SHA256 of "The quick brown fox jumped over the lazy dog";
    std::string seedVal = "8d18fbf476ed981c3a21662c6244abaf4e1f976f6e2421690ea8daf5093a5740";
    BigNumber seed;
    seed.FromHex(seedVal.c_str());
    BigNumber seedVal1;
    seedVal1.generateRandHexWithSeed(seedVal, 512);
    BigNumber seedVal2;
    seedVal2.generateRandHexWithSeed(seedVal, 512);
    std::cout << seedVal1.ToHex() << std::endl;
    std::cout << seedVal2.ToHex() << std::endl;

    std::cout << "\n======================1 <<4================================\n";
    BigNumber bnValue;
    bnValue.FromDec("1");
    BigNumber res = bnValue << 4;
    std::cout << "1 << 4 :" << res.ToDec () <<  std::endl;
    std::cout << "\n======================================================\n";

    std::cout << "\n======================1 << 65536================================\n";
    bnValue.FromDec("1");
    res = bnValue << 65536;
    std::cout << "1 <<" << 65536 << " :" << res.ToDec () <<  std::endl;
    //sleep(1);
    std::cout << "\n======================================================\n";
    std::cout << "\n======================(1 << 65536) >> 65536================================\n";
    res = res >> 65536;
    std::cout << "(1 << 65536) >>" << 65536 << " :" << res.ToDec () <<  std::endl;
    std::cout << "\n======================================================\n";

    std::cout << "\n======================1 << 65536================================\n";
    BigNumber bnValueShift;
    bnValue.FromDec("1");
    bnValueShift.FromDec("65536");
    res = bnValue << bnValueShift;
    std::cout << "1 <<" << 65536 << " :" << res.ToDec () <<  std::endl;
    //sleep(1);
    std::cout << "\n======================================================\n";

    std::cout << "\n======================(1 << 65536) >> 65536================================\n";
    res = res >> bnValueShift;
    std::cout << "(1 << 65536) >>" << 65536 << " :" << res.ToDec () <<  std::endl;
    std::cout << "\n======================================================\n";

    std::string  AvalMul("1000");
    std::string  BValMul ("100000000000000000000");

    BigNumber aValMul, bValMul;
    aValMul.FromDec(AvalMul);
    bValMul.FromDec(BValMul);

    BigNumber resMul = aValMul * bValMul;
    std::cout << aValMul.ToDec() << " * " << bValMul.ToDec() << " = " << resMul.ToDec() << std::endl;

    BigNumber resDiv = bValMul/aValMul;
    std::cout << bValMul.ToDec() << " / " << aValMul.ToDec() << " = " << resDiv.ToDec() << std::endl;

    resDiv = aValMul/bValMul;
    std::cout << aValMul.ToDec() << " / " << bValMul.ToDec() << " = " << resDiv.ToDec() << std::endl;
}

void testSymEndDec() {

}

extern "C" JNIEXPORT jstring JNICALL
Java_wrap_nakasendo_kit_NakasendoSDK_computeSomething(
        JNIEnv* env,
        jobject /* this */,
        jobjectArray arguments) {


    //Start threads to show stdout and stderr in logcat.
    if (start_redirecting_stdout_stderr()==-1) {
        __android_log_write(ANDROID_LOG_ERROR, ADBTAG, "Couldn't start redirecting stdout and stderr to logcat.");
    }

    // todo//////////////////////////////
    // todo//////////////////////////////
    // todo//////////////////////////////
    // todo//////////////////////////////
    // todo//////////////////////////////



    testSomeBigNumbers();
















    // todo//////////////////////////////
    // todo//////////////////////////////
    // todo//////////////////////////////
    // todo//////////////////////////////
    // todo//////////////////////////////


    std::string hello = "Hello from C++; Tee Hee.";
    return env->NewStringUTF(hello.c_str());

}









extern "C"
JNIEXPORT jobject JNICALL
Java_wrap_nakasendo_kit_bigNumbers_BigNumber_generateZero(JNIEnv *env, jclass clazz) {
    // TODO: implement generateZero()
}


















class SingletonBigNumberHelper
{
public:
    SingletonBigNumberHelper(const SingletonBigNumberHelper&) = delete;
    SingletonBigNumberHelper(SingletonBigNumberHelper&&) = delete;
    SingletonBigNumberHelper& operator=(const SingletonBigNumberHelper&) = delete;
    SingletonBigNumberHelper& operator=(SingletonBigNumberHelper&&) = delete;
private:

    SingletonBigNumberHelper();
    ~SingletonBigNumberHelper();

    static std::map<intptr_t,BigNumber> bigNumberMap;

    intptr_t keyGen = 0;

    intptr_t getNewKey();

public:

    static SingletonBigNumberHelper& instance()
    {
        static SingletonBigNumberHelper INSTANCE;
        return INSTANCE;
    }



    intptr_t add(BigNumber& bn);
    void remove(intptr_t key);


    BigNumber get(intptr_t intptr);
};












//////////////////////////////////////
//////////////////////////////////////
//////////////////////////////////////





//////////////////////////////////////
intptr_t SingletonBigNumberHelper::getNewKey()
{
    return keyGen++;
}

intptr_t SingletonBigNumberHelper::add(BigNumber& bn)
{
    intptr_t key = getNewKey();
    std::cout << "add() called" << " key:" << key << std::endl;

    bigNumberMap.insert(std::pair<intptr_t, BigNumber>(key, bn));

    return key;
}

void SingletonBigNumberHelper::remove(intptr_t key)
{
    bigNumberMap.erase(key);
    std::cout << "remove() called; current size of map:" << bigNumberMap.size() << std::endl;
}

BigNumber SingletonBigNumberHelper::get(intptr_t intptr)
{
    return bigNumberMap.find(intptr)->second;
}

SingletonBigNumberHelper::SingletonBigNumberHelper()
{
    std::cout << "CONSTRUCTOR CALLED" << std::endl;
}

SingletonBigNumberHelper::~SingletonBigNumberHelper()
{
    std::cout << "DESTRUCTOR CALLED" << std::endl;

    //todo
    //todo
    //todo
    //todo
}


//////////////////////////////////////






//////////////////////////////////////
//////////////////////////////////////
//////////////////////////////////////
//////////////////////////////////////













std::map<intptr_t,BigNumber> SingletonBigNumberHelper::bigNumberMap;















//////////////////////////////////////
//////////////////////////////////////
//////////////////////////////////////
//////////////////////////////////////




extern "C"
JNIEXPORT jlong JNICALL
Java_wrap_nakasendo_kit_bigNumbers_BigNumber_fromDecInternal(JNIEnv *env, jobject obj,
                                                             jstring value) {
    //get the actual dec-value as string
    const char *decValue = env->GetStringUTFChars(value, 0);

//    jobject globalRef = env->NewGlobalRef(obj);
//    intptr_t key = reinterpret_cast<intptr_t>(&globalRef);

    //create BigNumber equivalent
    BigNumber inst;
    inst.FromDec(decValue);

    //make the just-created object-instance findable later
    intptr_t key = SingletonBigNumberHelper::instance().add(inst);


    //save the finding-key on the java-object
    setHandle(env, obj, key);

//    std::cout << "fromDecInternal pointer:" << &inst << "\n>dec: " << inst.ToDec() << "\n>hex: " << inst.ToHex() << "\n";

    //release memory
    env->ReleaseStringUTFChars(value, decValue);

    return (jlong) key; //todo the return is redundant at this point
}


extern "C"
JNIEXPORT jstring JNICALL
Java_wrap_nakasendo_kit_bigNumbers_BigNumber_toHex(JNIEnv *env, jobject thiz) {

    //get the BigNumber object-instance finding-key
    intptr_t mappingKey = getHandle(env, thiz);
//    std::cout << "\n\n mappingKey:" << mappingKey << "\n\n\n";

    //get object-instance mapping holder
    auto& mappingHolder = SingletonBigNumberHelper::instance();


    BigNumber instance;
    try {
        //find the instance matching the mapping-key
        instance = mappingHolder.get(mappingKey);
    } catch (const std::exception& exception) {
        std::cout << "\n#toHex tried to retrieve the mapped object\n" << exception.what();

        throw exception;
    }

    std::string value = "";
    try {
        // !!!!!!!
        value = instance.ToHex();
//        std::cout << "\ntoHex:" << value << "\n\n\n";
    } catch (const std::exception& exception) {
        std::cout << "\n\n\n#toHex tried to execute a member-function on retrieved instance \n" << exception.what();

        throw exception;
    }


    return env->NewStringUTF(value.c_str());
}







extern "C"
JNIEXPORT void JNICALL
Java_wrap_nakasendo_kit_bigNumbers_BigNumber_dispose(JNIEnv *env, jobject thiz) {

    //get the BigNumber object-instance finding-key
    intptr_t mappingKey = getHandle(env, thiz);

    //get object-instance mapping holder
    auto& mappingHolder = SingletonBigNumberHelper::instance();

    BigNumber instance;
    try {
        //find the instance matching the mapping-key
        instance = mappingHolder.get(mappingKey);
    } catch (const std::exception& exception) {
        std::cout << "\n#dispose tried to retrieve the mapped object\n" << exception.what();

        throw exception;
    }

    //remove the mapping from the holder
    mappingHolder.remove(mappingKey);

    //todo the following does not compile #WTF
//    //release memory
//    delete instance;

    //clear the mapping held by the java-object
    setHandle(env, thiz, 0);
}





