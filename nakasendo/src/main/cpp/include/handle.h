//
// Created by Janis Graumanis on 26/03/2020.
//

#ifndef BAEMAIL_APP_HANDLE_H
#define BAEMAIL_APP_HANDLE_H


jfieldID getHandleField(JNIEnv *env, jobject obj)
{
    jclass c = env->GetObjectClass(obj);
    // J is the type signature for long:
    return env->GetFieldID(c, "nativeHandle", "J");
}



//template <typename T>
//T *getHandle0(JNIEnv *env, jobject obj)
//{
//    jlong handle = env->GetLongField(obj, getHandleField(env, obj));
//    return reinterpret_cast<T *>(handle);
//}


intptr_t getHandle(JNIEnv *env, jobject obj)
{
    jlong handle = env->GetLongField(obj, getHandleField(env, obj));

//todo consider switching `intptr_t` out with `int64_t`
    return handle;
}



template <typename T>
void setHandle(JNIEnv *env, jobject obj, T *t)
{
    jlong handle = reinterpret_cast<jlong>(t);
    env->SetLongField(obj, getHandleField(env, obj), handle);
}


void setHandle(JNIEnv *env, jobject obj, intptr_t t)
{
    jlong handle = t;
    env->SetLongField(obj, getHandleField(env, obj), handle);
}


#endif //BAEMAIL_APP_HANDLE_H
