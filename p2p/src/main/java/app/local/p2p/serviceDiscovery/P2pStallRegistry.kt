package app.local.p2p.serviceDiscovery

import kotlinx.coroutines.flow.StateFlow

interface P2pStallRegistry {
    val supportedStatus: StateFlow<Boolean>
    val advertisingStatus: StateFlow<AdvertisingStatus>
    val discoveredServices: StateFlow<DiscoveryModel>
    val currentPeerId: P2pStallManager.PeerServiceId
    fun tearDown()
    suspend fun beginAdvertisingServiceOfLocalPeer(port: Int, id: P2pStallManager.PeerServiceId)
    suspend fun stopAdvertisingServiceOfLocalPeer()
    fun beginDiscovery(id: P2pStallManager.PeerServiceId)
    fun stopDiscovery()
}