package app.local.p2p.serviceDiscovery

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.net.wifi.WpsInfo
import android.net.wifi.p2p.*
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest
import android.os.Looper
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber
import kotlin.coroutines.resume
import kotlin.collections.ArrayList


@SuppressLint("MissingPermission")
class P2pStallRegistryHelper1(
    var context: Context,
    val scope: CoroutineScope,
    private val onConnectionAttempt: (FoundStall) -> Unit,
    private val onGroupInfoChange: (WifiP2pInfo) -> Unit,
    private val onParticipantChange: (Collection<DeviceStatus>) -> Unit //todo only call this if the list has changes; do not call with repeating values
) : P2pStallRegistry {


    val wifiManager: WifiManager by lazy {
        context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
    }

    private val p2pManager: WifiP2pManager by lazy {
        context.getSystemService(Context.WIFI_P2P_SERVICE) as WifiP2pManager
    }


    private var _supportedStatus = MutableStateFlow(true)
    override val supportedStatus: StateFlow<Boolean>
        get() = _supportedStatus


    private val _advertisingStatus = MutableStateFlow(AdvertisingStatus.UNREGISTERED)
    override val advertisingStatus: StateFlow<AdvertisingStatus>
        get() = _advertisingStatus






    private val _discoveredServices = MutableStateFlow(
        DiscoveryModel(
            false,
            mapOf()
        )
    )
    override val discoveredServices: StateFlow<DiscoveryModel>
        get() = _discoveredServices





    private var _currentPeerId = P2pStallManager.PeerServiceId(
        "INVALID_UNSET",
        "INVALID_UNSET",
        "INVALID_UNSET"
    )
    override val currentPeerId: P2pStallManager.PeerServiceId
        get() = _currentPeerId





    var channel: WifiP2pManager.Channel? = null
    val p2pIntentFilter: IntentFilter by lazy {
        getP2pIntentFilter()
    }

    val p2pEventReceiver: P2pBroadcastReceiver by lazy {
        P2pBroadcastReceiver()
    }

    var activeDiscoveryJob: Job? = null




    //todo move
    private var isInitializing = false
    private var isWifiP2pEnabled = false
    private val p2pEnabledWaitingContinuations = ArrayList<CancellableContinuation<Unit>>()


    //todo clear when torn down
    //todo
    //todo
    //todo
    private var currentPeerState = mutableMapOf<String, DeviceStatus>()


    //todo must be cleared on tear-down
    //todo
    //todo
    //todo
    private val historicConnectedStalls = mutableMapOf<String, FoundStall?>()


    //todo move
    //todo cleanup on tear-down
    //todo cleanup on stop-discovery
    //todo
    //todo
    //todo ...this might not be satisfactory if two users both create stalls & attempt to browse each-other simultaniously
    private val outgoingConnectionAttempts = mutableSetOf<String>()




    private var frameworkPingJob: Job? = null


    fun setup() {
        if (channel != null) {
            Timber.d("#setup has been dropped because it already is in this state")
            return
        }

        context.registerReceiver(p2pEventReceiver, p2pIntentFilter)

        channel = p2pManager.initialize(context, Looper.getMainLooper()) {
            Timber.d("onP2pChannelDisconnected")
            frameworkPingJob?.cancel()
            frameworkPingJob = null
        }

        _peerDiscoveryState.value = false
        _localServiceAdvertisingState.value = false
        _peerServiceDiscoveryState.value = false
        _peerGroupConnectionState.value = false

        isWifiP2pEnabled = true
        isInitializing = false

        Timber.d("P2pChannel has been initialized")

        frameworkPingJob?.cancel()
        frameworkPingJob = scope.launch {
            //start periodically checking that system-p2p-state is in the same state as expected by the app
            delay(5000)
            while (true) {
                ensureActive()
                Timber.d("...starting `frameworkPingJob` cycle")
                ensureP2pFrameworkInExpectedState()

                delay(11000)
            }
        }
    }


    private var tearDownJob: Job? = null


    override fun tearDown() {
        Timber.d("#tearDown")

        if (tearDownJob?.isActive == true) {
            Timber.d("#tearDown dropped; already in progress")
            return
        }

        //todo ensure #setup await for the following job to stop before doing its thing


        tearDownJob = scope.launch {

//            suspendCancellableCoroutine<Boolean> { continuation ->
//                p2pManager.cancelConnect(
//                    channel,
//                    object : WifiP2pManager.ActionListener {
//                        override fun onSuccess() {
//                            continuation.resume(true)
//                        }
//
//                        override fun onFailure(reason: Int) {
//                            Timber.d("discoverServices#onFailure code:$reason")
//                            // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
//                            continuation.resume(false)
//                        }
//
//                    }
//                )
//            }


            frameworkPingJob?.cancel()
            frameworkPingJob = null


            val info = suspendCancellableCoroutine<WifiP2pInfo?> { continuation ->
                p2pManager.requestConnectionInfo(channel, object : WifiP2pManager.ConnectionInfoListener {
                    override fun onConnectionInfoAvailable(info: WifiP2pInfo?) {
                        continuation.resume(info)
                    }

                })
            }

            if (info?.groupFormed == true) {
                Timber.d("...doing #tearDown; removing group")
                suspendCancellableCoroutine<Boolean> { continuation ->
                    p2pManager.removeGroup(
                        channel,
                        object : WifiP2pManager.ActionListener {
                            override fun onSuccess() {
                                continuation.resume(true)
                            }

                            override fun onFailure(reason: Int) {
                                Timber.d("discoverServices#onFailure code:$reason")
                                // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                                continuation.resume(false)
                            }

                        }
                    )
                }.let { result ->
                    if (!result) Timber.d("...unexpected error when attempting to #removeGroup")
                }
            }

            suspendCancellableCoroutine<Boolean> { continuation ->
                p2pManager.stopPeerDiscovery(
                    channel,
                    object : WifiP2pManager.ActionListener {
                        override fun onSuccess() {
                            continuation.resume(true)
                        }

                        override fun onFailure(reason: Int) {
                            Timber.d("discoverServices#onFailure code:$reason")
                            // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                            continuation.resume(false)
                        }

                    }
                )
            }.let { result ->
                if (!result) Timber.d("...unexpected error when attempting to #stopPeerDiscovery")
            }

            context.unregisterReceiver(p2pEventReceiver)

            stopAdvertisingServiceOfLocalPeer()
            stopDiscovery()

            channel?.close()
            channel = null

            _peerDiscoveryState.value = false
            _localServiceAdvertisingState.value = false
            _peerServiceDiscoveryState.value = false
            _peerGroupConnectionState.value = false


            isInitializing = false
            isWifiP2pEnabled = false

            ArrayList(p2pEnabledWaitingContinuations).let { toResume ->
                p2pEnabledWaitingContinuations.clear()
                toResume.forEach { continuation ->
                    continuation.cancel()
                }
            }

        }
    }

    private fun considerTearDown() {
        if (!_discoveredServices.value.isActive &&
            AdvertisingStatus.UNREGISTERED == advertisingStatus.value) {
            tearDown()
        }
    }


    fun onError(throwable: Throwable) {
        tearDown()
    }


    @SuppressLint("MissingPermission")
    override suspend fun beginAdvertisingServiceOfLocalPeer(port: Int, id: P2pStallManager.PeerServiceId) {
        Timber.d("#beginAdvertisingServiceOfLocalPeer pre #ensureConnection")

        try {
            ensureConnection()
        } catch (e: Exception) {
            Timber.e(e)
            throw e
        }

        Timber.d("#beginAdvertisingServiceOfLocalPeer")


        _currentPeerId = id //todo disallow advertising with a diff id from the one used when discovering
        _advertisingStatus.value = AdvertisingStatus.PREPARING


        val serviceInfo: WifiP2pDnsSdServiceInfo = let {
            val record: Map<String, String> = mapOf(
                "listenport" to port.toString(),
                "paymail" to id.paymail,
                "pki" to id.pki,
                "title" to id.title,
            )

            WifiP2pDnsSdServiceInfo.newInstance(
                "_bsv_p2p",
                SERVICE_TYPE,
                record
            )
        }

        suspendCancellableCoroutine<Boolean> { continuation ->
            p2pManager.addLocalService(channel, serviceInfo, object : WifiP2pManager.ActionListener {
                override fun onSuccess() {
                    _localServiceAdvertisingState.value = true
                    continuation.resume(true)
                }

                override fun onFailure(reason: Int) {
                    // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                    Timber.e("addLocalService failed; reason:$reason")
                    continuation.resume(false)
                }
            })
        }.let { result ->
            if (!result) {
                _advertisingStatus.value = AdvertisingStatus.ERROR
                return
            }
        }



        if (_discoveredServices.value.isActive) {
            //both advertising local service & discovering services
            //
            suspendCancellableCoroutine<Boolean> { continuation ->
                Timber.d("about to #addServiceRequest filter for the service-discovery-scanning")
                p2pManager.addServiceRequest(
                    channel,
                    serviceRequest,
                    object : WifiP2pManager.ActionListener {
                        override fun onSuccess() {
                            continuation.resume(true)
                        }
                        override fun onFailure(code: Int) {
                            Timber.d("addServiceRequest#onFailure code:$code")
                            // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                            continuation.resume(false)
                        }
                    }
                )
            }.let {
                if (!it) {
                    Timber.e(RuntimeException("#addServiceRequest fail"))
                    return
                }
            }

            suspendCancellableCoroutine<Boolean> { continuation ->
                Timber.d("about to #discoverServices")
                p2pManager.discoverServices(
                    channel,
                    object : WifiP2pManager.ActionListener {
                        override fun onSuccess() {
                            _peerServiceDiscoveryState.value = true
                            continuation.resume(true)
                        }
                        override fun onFailure(code: Int) {
                            Timber.d("discoverServices#onFailure code:$code")
                            // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                            continuation.resume(false)
                        }
                    }
                )
            }.let {
                if (!it) {
                    Timber.e(RuntimeException("#discoverServices fail"))
                    return
                }
            }

        } else {
            //just advertising local service

            suspendCancellableCoroutine<Boolean> { continuation ->
                Timber.d("about to #discoverPeers")
                p2pManager.discoverPeers(
                    channel,
                    object : WifiP2pManager.ActionListener {
                        override fun onSuccess() {
                            _peerDiscoveryState.value = true
                            continuation.resume(true)
                        }
                        override fun onFailure(code: Int) {
                            Timber.d("discoverServices#onFailure code:$code")
                            // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                            continuation.resume(false)
                        }
                    }
                )
            }.let {
                if (!it) {
                    Timber.e(RuntimeException("#discoverPeers fail"))
                    return
                }
            }
        }



        _advertisingStatus.value = AdvertisingStatus.REGISTERED

        Timber.d("...a service has now been registered on wifi-direct")
    }

    //todo move
    private var activeAdvertisingJob: Job? = null

    private var _peerDiscoveryState = MutableStateFlow(false)
    private var _localServiceAdvertisingState = MutableStateFlow(false)
    private var _peerServiceDiscoveryState = MutableStateFlow(false)
    private var _peerGroupConnectionState = MutableStateFlow(false)



    override suspend fun stopAdvertisingServiceOfLocalPeer() {
        if (AdvertisingStatus.UNREGISTERED == advertisingStatus.value) {
            return
        }

        activeAdvertisingJob?.cancel()
        activeAdvertisingJob = null

        //reset -- clearLocalServices
        suspendCancellableCoroutine<Boolean> { continuation ->
            p2pManager.clearLocalServices(channel, object : WifiP2pManager.ActionListener {
                override fun onSuccess() {
                    _localServiceAdvertisingState.value = false
                    continuation.resume(true)
                }

                override fun onFailure(reason: Int) {
                    // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                    Timber.e("clearLocalServices failed; reason:$reason")
                    continuation.resume(false)
                }
            })
        }.let { result ->
            if (!result) {
                _advertisingStatus.value = AdvertisingStatus.ERROR
                return
            }
        }


        _advertisingStatus.value = AdvertisingStatus.UNREGISTERED


        considerTearDown()
    }






    @SuppressLint("MissingPermission")
    override fun beginDiscovery(id: P2pStallManager.PeerServiceId) {
        _currentPeerId = id //todo disallow advertising with a diff id from the one used when discovering

        activeDiscoveryJob?.cancel()
        activeDiscoveryJob = scope.launch {
            doBeginDiscovery()
        }

    }


    @SuppressLint("MissingPermission")
    private suspend fun doBeginDiscovery() {
        ensureConnection()

        val txtListener = WifiP2pManager.DnsSdTxtRecordListener { fullDomain, record, device ->
            //expected fullDomain: _bsv_p2p._peer_bsv_ws._tcp.local.

            Timber.d("DnsSdTxtRecord available -- \n$fullDomain\n -- \n$record\n -- \n$device ")

            try {
                val port = record["listenport"]!!.toInt()
                val title = record["title"]!!
                val paymail = record["paymail"]!!
                val pki = record["pki"]!!
                val host = device.deviceAddress

                //val serviceName = P2pStallServiceHelper.getServiceName(paymail, pki, title)
//                val serviceName = device.deviceAddress
                val serviceName = pki

                val gotStall = FoundStall(serviceName, host, port, title, pki, paymail)

                val updated = HashMap<String, FoundStall>(_discoveredServices.value.peers)
                updated[serviceName] = gotStall
                _discoveredServices.value = _discoveredServices.value.copy(
                    peers = updated
                )



            } catch (e: Exception) {
                Timber.e(e)
            }
        }

        val servListener = WifiP2pManager.DnsSdServiceResponseListener {
                instanceName, registrationType, resourceType ->
            Timber.d("onBonjourServiceAvailable $instanceName; resourceType.deviceName: ${resourceType.deviceName}\n$registrationType\n$resourceType")
        }

        p2pManager.setDnsSdResponseListeners(channel, servListener, txtListener)

        p2pManager.setServiceResponseListener(channel, object : WifiP2pManager.ServiceResponseListener {
            override fun onServiceAvailable(
                protocolType: Int,
                responseData: ByteArray?,
                srcDevice: WifiP2pDevice?
            ) {
                Timber.d("#setServiceResponseListener#onServiceAvailable protocolType: $protocolType srcDevice: $srcDevice ........")
            }
        })





        suspendCancellableCoroutine<Boolean> { continuation ->
            Timber.d("about to #addServiceRequest filter for the service-discovery-scanning")
            p2pManager.addServiceRequest(
                channel,
                serviceRequest,
                object : WifiP2pManager.ActionListener {
                    override fun onSuccess() {
                        continuation.resume(true)
                    }
                    override fun onFailure(code: Int) {
                        Timber.d("addServiceRequest#onFailure code:$code")
                        // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                        continuation.resume(false)
                    }
                }
            )
        }.let {
            if (!it) {
                Timber.e(RuntimeException("addServiceRequest fail"))
                return
            }
        }

        suspendCancellableCoroutine<Boolean> { continuation ->
            Timber.d("about to #discoverServices")
            p2pManager.discoverServices(
                channel,
                object : WifiP2pManager.ActionListener {
                    override fun onSuccess() {
                        _peerServiceDiscoveryState.value = true
                        continuation.resume(true)
                    }
                    override fun onFailure(code: Int) {
                        Timber.d("discoverServices#onFailure code:$code")
                        // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                        continuation.resume(false)
                    }
                }
            )
        }.let {
            if (!it) {
                Timber.e(RuntimeException("discoverServices fail"))
                return
            }
        }

        _discoveredServices.value = _discoveredServices.value.copy(
            isActive = true
        )

        Timber.d("... service-discovery has been started")

    }

    override fun stopDiscovery() {
        if (!_discoveredServices.value.isActive) {
            return
        }
        Timber.d("#stopDiscovery")

        activeDiscoveryJob?.cancel()
        activeDiscoveryJob = null

        _discoveredServices.value = _discoveredServices.value.copy(
            isActive = false,
            peers = emptyMap()
        )

        //todo disallow start discovery until the following has completed
        val job = scope.launch {
            suspendCancellableCoroutine<Unit> { continuation ->
                Timber.d("about to #clearServiceRequests")
                p2pManager.clearServiceRequests(
                    channel,
                    object : WifiP2pManager.ActionListener {
                        override fun onSuccess() {
                            _peerServiceDiscoveryState.value = false
                            continuation.resume(Unit)
                        }

                        override fun onFailure(code: Int) {
                            Timber.d("clearServiceRequests#onFailure code:$code")
                            // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                            continuation.cancel()
                        }
                    }
                )
            }

            considerTearDown()
        }
    }



    @SuppressLint("MissingPermission")
    suspend fun connectPeer(serviceName: String) {

        //todo
        // there could be a situation where the current peer is already in a p2p-group with
        // the participant advertising the specified stall-name;
        // this situation should be handled by noting the user desire for this stall, passing it
        // to the node-side & respectively proceeding to actually connect with it from there
        // inside node-vm.

        outgoingConnectionAttempts.add(serviceName)

        val gotPeer = _discoveredServices.value.peers[serviceName]
            ?: throw RuntimeException()




        val matchingPeer = currentPeerState[serviceName] //for wifi-direct the stall's name is identical to it's `pki`
        matchingPeer?.let { peer ->
            if (peer.isConnected()) {
                Timber.d("...this device is a p2p-group peer already; skipping connecting")
                onConnectionAttempt(gotPeer)
                return
            }
        }




        val config = WifiP2pConfig().apply {
            deviceAddress = gotPeer.host
            wps.setup = WpsInfo.PBC
        }

        suspendCancellableCoroutine<Boolean> { continuation ->
            Timber.d("about to #connect")
            try {
                p2pManager.connect(
                    channel,
                    config,
                    object : WifiP2pManager.ActionListener {
                        override fun onSuccess() {
                            continuation.resume(true)
                        }

                        override fun onFailure(reason: Int) {
                            Timber.d("connect#onFailure code:$reason")
                            // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                            continuation.resume(false)
                        }

                    }
                )

            } catch (e: Exception) {
                Timber.e(e)
                //continuation.resume(false)
            }
        }.let {
            if (!it) {
                Timber.e(RuntimeException("p2p-group-forming failed; what is the state right now? did anything broke?"))
                return
            }
        }


        // inform the node-side of user-desire to connect to a stall with this PKI
        onConnectionAttempt(gotPeer)


        //todo
        // start a concurrent job that functions as a timeout for this connect & ensures nothing
        // gets broken if the timeout gets tripped
    }

    private suspend fun ensureP2pFrameworkInExpectedState() {
        val info = suspendCancellableCoroutine<WifiP2pInfo?> { continuation ->
            p2pManager.requestConnectionInfo(channel, object : WifiP2pManager.ConnectionInfoListener {
                override fun onConnectionInfoAvailable(info: WifiP2pInfo?) {
                    continuation.resume(info)
                }

            })
        }

        info ?: let {
            //info is NULL
            Timber.d("...#ensureP2pFrameworkInExpectedState dropped; p2p-info == NULL")
            return
        }

        val didJustJoinGroup = (_peerGroupConnectionState.value == false).let {
            if (!info.groupFormed) {
                return@let false
            }
            it
        }
        if (didJustJoinGroup) {
            _peerDiscoveryState.value = false
            _peerServiceDiscoveryState.value = false
            _localServiceAdvertisingState.value = false
            _peerGroupConnectionState.value = true
        }

        val didJustLeaveGroup = (_peerGroupConnectionState.value == true).let {
            if (!info.groupFormed) {
                return@let true
            }
            false
        }
        if (didJustLeaveGroup) {
            _peerDiscoveryState.value = false
            _peerServiceDiscoveryState.value = false
            _localServiceAdvertisingState.value = false
            _peerGroupConnectionState.value = false
        }





        if (AdvertisingStatus.REGISTERED == _advertisingStatus.value && _discoveredServices.value.isActive) {
            suspendCancellableCoroutine<Boolean> { continuation ->
                Timber.d("...pinging with with p2p-framework to both advertise & discover remote services")
                p2pManager.addServiceRequest(
                    channel,
                    serviceRequest,
                    object : WifiP2pManager.ActionListener {
                        override fun onSuccess() {
                            continuation.resume(true)
                        }
                        override fun onFailure(code: Int) {
                            Timber.d("addServiceRequest#onFailure code:$code")
                            // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                            continuation.resume(false)
                        }
                    }
                )
            }.let {
                if (!it) {
                    Timber.e(RuntimeException("#addServiceRequest fail"))

                    return
                }
            }

        } else if (AdvertisingStatus.REGISTERED == _advertisingStatus.value) {
            suspendCancellableCoroutine<Boolean> { continuation ->
                Timber.d("...pinging with with p2p-framework to just advertise local service")
                p2pManager.discoverPeers(
                    channel,
                    object : WifiP2pManager.ActionListener {
                        override fun onSuccess() {
                            _peerDiscoveryState.value = true
                            continuation.resume(true)
                        }
                        override fun onFailure(code: Int) {
                            Timber.d("discoverServices#onFailure code:$code")
                            // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                            continuation.resume(false)
                        }
                    }
                )
            }.let {
                if (!it) {
                    Timber.e(RuntimeException("#discoverPeers fail"))
                    return
                }
            }

        } else if (_discoveredServices.value.isActive) {
            suspendCancellableCoroutine<Boolean> { continuation ->
                Timber.d("...pinging with with p2p-framework to just discover remote service")
                p2pManager.addServiceRequest(
                    channel,
                    serviceRequest,
                    object : WifiP2pManager.ActionListener {
                        override fun onSuccess() {
                            continuation.resume(true)
                        }
                        override fun onFailure(code: Int) {
                            Timber.d("addServiceRequest#onFailure code:$code")
                            // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                            continuation.resume(false)
                        }
                    }
                )
            }.let {
                if (!it) {
                    Timber.e(RuntimeException("#addServiceRequest fail"))
                    return
                }
            }

        } else {
            considerTearDown()

        }

    }



    private suspend fun ensureConnection() {
        if (isWifiP2pEnabled) return
        if (isInitializing) {
            suspendCancellableCoroutine<Unit> { continuation ->
                p2pEnabledWaitingContinuations.add(continuation)
            }
            return
        }
        isInitializing = true

        // Device capability definition check
        if (!context.packageManager.hasSystemFeature(PackageManager.FEATURE_WIFI_DIRECT)) {
            Timber.e("Wi-Fi Direct is not supported by this device.")
            _supportedStatus.value = false
            throw RuntimeException()
        }

        //todo enable WIFI if disabled
        //todo
        //todo
        //todo

        //todo disable wifi-hotspot if enabled
        //todo
        //todo
        //todo

        // Hardware capability check
        if (!wifiManager.isP2pSupported) {
            Timber.e("Wi-Fi Direct is not supported by the hardware or Wi-Fi is off.")
            _supportedStatus.value = false
            throw RuntimeException()
        }
        _supportedStatus.value = true

        setup()


        scope.launch {
            Timber.d("p2pEnabledWaitingContinuations.size: ${p2pEnabledWaitingContinuations.size}")

            ArrayList(p2pEnabledWaitingContinuations).let { toResume ->
                p2pEnabledWaitingContinuations.clear()
                toResume.forEach { continuation ->
                    Timber.d("....resuming")
                    continuation.resume(Unit)
                }
            }
        }

    }




    inner class P2pBroadcastReceiver : android.content.BroadcastReceiver() {

        @SuppressLint("MissingPermission")
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.action ?: return

            when (intent.action) {
                WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION -> {
                    // Determine if Wifi P2P mode is enabled or not, alert
                    // the Activity.
                    val state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1)
                    isWifiP2pEnabled = state == WifiP2pManager.WIFI_P2P_STATE_ENABLED
                    Timber.d("WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION; enabled:$isWifiP2pEnabled")

                    if (isWifiP2pEnabled) {
                        val toResume = ArrayList(p2pEnabledWaitingContinuations)
                        p2pEnabledWaitingContinuations.clear()
                        toResume.forEach { continuation ->
                            continuation.resume(Unit)
                        }
                    }


                }
                WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION -> {
                    Timber.d("WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION")

                    val deviceList: WifiP2pDeviceList? = intent
                        .getParcelableExtra(WifiP2pManager.EXTRA_P2P_DEVICE_LIST) as WifiP2pDeviceList?

                    scope.launch {
                        p2pManager.requestPeers(channel, WifiP2pManager.PeerListListener { deviceList ->
                            handleLatestPeerInfo(deviceList.deviceList)
                        })
                    }

                }
                WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION -> {

                    // Connection state changed! We should probably do something about
                    // that.
                    Timber.d("WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION")

                    val latestWifiP2pInfo: WifiP2pInfo = intent
                        .getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_INFO)!!

                    val latestNetworkInfo: NetworkInfo = intent
                        .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO)!!

                    val latestP2pGroup: WifiP2pGroup? = intent
                        .getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_GROUP) as WifiP2pGroup?

                    p2pManager.requestConnectionInfo(channel) { wifiP2pInfo ->
                        onGroupInfoChange(latestWifiP2pInfo)
                    }


                    scope.launch {
                        p2pManager.requestPeers(channel) { deviceList ->
                            handleLatestPeerInfo(deviceList.deviceList)
                        }
                    }



                    val didJustJoinGroup = (_peerGroupConnectionState.value == false).let {
                        if (!latestWifiP2pInfo.groupFormed) {
                            return@let false
                        }
                        it
                    }
                    if (didJustJoinGroup) {
                        _peerDiscoveryState.value = false
                        _peerServiceDiscoveryState.value = false
                        _localServiceAdvertisingState.value = false
                        _peerGroupConnectionState.value = true
                    }

                    val didJustLeaveGroup = (_peerGroupConnectionState.value == true).let {
                        if (!latestWifiP2pInfo.groupFormed) {
                            return@let true
                        }
                        false
                    }
                    if (didJustLeaveGroup) {
                        _peerDiscoveryState.value = false
                        _peerServiceDiscoveryState.value = false
                        _localServiceAdvertisingState.value = false
                        _peerGroupConnectionState.value = false
                    }

                    Timber.d("WifiP2pInfo .... $latestWifiP2pInfo\n\nNetworkInfo .... $latestNetworkInfo\n\nWifiP2pGroup ... $latestP2pGroup")


                }
                WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION -> {
                    val device = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE)
                            as WifiP2pDevice?

                    Timber.d("WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION ...device:$device")

                }
                WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION -> {
                    val state = intent.getIntExtra(WifiP2pManager.EXTRA_DISCOVERY_STATE, -1)
                    Timber.d("discover is started == ${WifiP2pManager.WIFI_P2P_DISCOVERY_STARTED == state}")
                    _peerDiscoveryState.value = WifiP2pManager.WIFI_P2P_DISCOVERY_STARTED == state

                }
                else -> {
                    Timber.d("P2pBroadcastReceiver.onReceive: ${intent.action}")
                }
            }

        }




    }


    /**
     * Function processes the passed peers as the current source-of-truth.
     */
    fun handleLatestPeerInfo(peers: Collection<WifiP2pDevice>) {
        val latestState = mutableMapOf<String, DeviceStatus>()

        peers.map { device ->
            Timber.d("peer: $device")
            DeviceStatus(
                device.deviceName,
                device.deviceAddress,
                device.status
            )
        }.forEach { status ->
            latestState[status.peerAddress] = status
        }


        if (currentPeerState.size == latestState.size) {
            val foundDifferent = latestState.values.find { peer ->
                val matchingStatus = currentPeerState[peer.peerAddress]
                return@find matchingStatus == null || matchingStatus.status != peer.status
            }

            foundDifferent ?: let {
                //failed to find an address whose matching device-status' are not matching between the different state-lists
                //the device-status-set is unchanged
                return
            }

        }

        currentPeerState = latestState
        Timber.d("...very latest peer states: ${currentPeerState.values}")

        // pass the very latest p2p-group-participants info to the node-side
        onParticipantChange(latestState.values)
    }


    data class DeviceStatus(
        val peerName: String,
        val peerAddress: String,
        val status: Int
    ) {
        fun isConnected(): Boolean {
            return WifiP2pDevice.CONNECTED == status
        }

        fun isOnline(): Boolean {
            return WifiP2pDevice.CONNECTED == status ||
                    WifiP2pDevice.INVITED == status ||
                    WifiP2pDevice.AVAILABLE == status ||
                    WifiP2pDevice.FAILED == status //todo does this last one really make sense here?
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as DeviceStatus

            if (peerName != other.peerName) return false
            if (peerAddress != other.peerAddress) return false

            return true
        }

        override fun hashCode(): Int {
            var result = peerName.hashCode()
            result = 31 * result + peerAddress.hashCode()
            return result
        }
    }

    companion object {

        private const val SERVICE_PROTOCOL = "peer_bsv_ws"
        const val SERVICE_NAME_PREFIX = "NsdPeer"
        const val SERVICE_TYPE = "_$SERVICE_PROTOCOL._tcp"

        val serviceRequest = WifiP2pDnsSdServiceRequest.newInstance(
            "_bsv_p2p",
            SERVICE_TYPE
        )

        const val KEY_TITLE = "title"
        const val KEY_PKI = "pki"
        const val KEY_PAYMAIL = "paymail"


        fun getP2pIntentFilter(): IntentFilter {
            val intentFilter = IntentFilter()

            // Indicates a change in the Wi-Fi P2P status.
            intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION)

            // Indicates a change in the list of available peers.
            intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)

            // Indicates the state of Wi-Fi P2P connectivity has changed.
            intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)

            // Indicates this device's details have changed.
            intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION)

            intentFilter.addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION)

            return intentFilter
        }
    }
}