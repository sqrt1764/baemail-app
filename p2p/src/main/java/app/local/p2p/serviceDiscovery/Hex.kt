package app.local.p2p.serviceDiscovery

import java.lang.StringBuilder

object Hex {

    private const val HEX_CHARS = "0123456789abcdef"

    fun toHex(bytes: ByteArray): String {
        val sb = StringBuilder()
        for (b in bytes) {
            val st = String.format("%02X", b)
            sb.append(st)
        }
        return sb.toString()
    }

    fun fromHex(s: String): ByteArray {
        val result = ByteArray(s.length / 2)

        for (i in 0 until s.length step 2) {
            val firstIndex = HEX_CHARS.indexOf(s[i])
            val secondIndex = HEX_CHARS.indexOf(s[i + 1])

            val octet = firstIndex.shl(4).or(secondIndex)
            result.set(i.shr(1), octet.toByte())
        }

        return result
    }

}