package app.local.p2p.serviceDiscovery

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import androidx.core.app.NotificationManagerCompat
import timber.log.Timber

class P2pStallService : Service()  {

    //todo inject
    private val notificationHelper: NotificationHelper by lazy {
        NotificationHelper(baseContext)
    }

    val manager: NotificationManagerCompat by lazy {
        NotificationManagerCompat.from(this@P2pStallService)
    }

    private var isInitialized = false

    override fun onCreate() {
        Timber.d("onCreate")
        super.onCreate()

        //inject?

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("onStartCommand")


        intent?.data?.let {

            Timber.d("intent: $it host: ${it.host}")

            when (it.host) {
                HOST_STOP_SELF -> {
                    stopSelf()
                }
                else -> {
                    Timber.d("intent-data was not recognized")
                }
            }

            return super.onStartCommand(intent, flags, startId)
        }

        //check has been initialized; enter foreground
        if (!isInitialized) {
            isInitialized = true

            startForeground(
                NotificationHelper.ID_NOTIFICATION_STALL_SERVICE,
                notificationHelper.buildNotification(P2pStallManager.get().getGoToManageStallIntent())
            )
        }

        P2pStallManager.onServiceCreated()


        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        stopForeground(true)
        P2pStallManager.onServiceDestroyed()

        super.onDestroy()
        Timber.d("onDestroy")
    }


    override fun onBind(intent: Intent?): IBinder? {
        return null
    }



    companion object {

        const val HOST_STOP_SELF = "stop_self"

        fun getStartIntent(
            context: Context
        ): Intent {
            return Intent(context, P2pStallService::class.java)
        }
    }
}