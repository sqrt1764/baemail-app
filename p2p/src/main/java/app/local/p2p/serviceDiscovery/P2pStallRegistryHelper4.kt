package app.local.p2p.serviceDiscovery

import android.util.ArrayMap
import androidx.collection.SimpleArrayMap
import com.google.android.gms.nearby.connection.Payload
import com.google.android.gms.nearby.connection.PayloadTransferUpdate
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.*
import timber.log.Timber
import java.io.*
import java.nio.charset.StandardCharsets
import kotlin.coroutines.resume


class P2pStallRegistryHelper4(
    private val nearby: NearbyConnectionsHelper,
    private val gson: Gson,
    private val scope: CoroutineScope,
    private val playServicesAvailable: Flow<Boolean>,
    private val onNewConnection: (String, String, Boolean) -> Unit,
    private val onLostConnection: (String, Boolean) -> Unit,
    private val sendChannel: ReceiveChannel<SendEvent>,
    private val getStreamDataInputToSend: (SendEvent) -> InputStream,
    private val delegateIncomingMessageHandling: suspend (StreamResponseHolder) -> Unit,
    private val delegateIncomingByteMessageHandling: (IncomingByteMessage) -> Unit,
    private val delegateIncomingFileRequestHandling: (FileReceiveEvent) -> FileResponseHolder,
    private val delegateIncomingFileResponseHandling: suspend (FileResponseRedirectingHolder) -> Unit,
    private val delegateIncomingErrorResponseHandling: (ErrorResponseHolder) -> Unit
) : P2pStallRegistry {


    private var _supportedStatus = MutableStateFlow(false)
    override val supportedStatus: StateFlow<Boolean>
        get() = _supportedStatus


    private val _advertisingStatus = MutableStateFlow(AdvertisingStatus.UNREGISTERED)
    override val advertisingStatus: StateFlow<AdvertisingStatus>
        get() = _advertisingStatus


    private val _discoveredServices = MutableStateFlow(
        DiscoveryModel(
            false,
            mapOf()
        )
    )
    override val discoveredServices: StateFlow<DiscoveryModel>
        get() = _discoveredServices


    private var _currentPeerId = P2pStallManager.PeerServiceId(
        "INVALID_UNSET",
        "INVALID_UNSET",
        "INVALID_UNSET"
    )
    override val currentPeerId: P2pStallManager.PeerServiceId
        get() = _currentPeerId





    private var millisSetup: Long = 0L

    private var isInitializing = false
    private var isWifiP2pEnabled = false
    private val p2pEnabledWaitingContinuations = ArrayList<CancellableContinuation<Unit>>()

    private var outgoingMessageProcessingJob: Job? = null
    private var clientAutoConnectJob: Job? = null
    private var incomingMessageRedirectJob: Job? = null


    //the following hold info about incoming streams; items get consumed as stream-processing proceeds
    private val incomingStreamsMetaMap = mutableMapOf<Long, StreamMeta>()
    private val incomingStreamsMap = mutableMapOf<Long, Payload>()

    //holds references to jobs of delegates doing incoming-stream processing; items get consumed as stream-processing proceeds
    private val streamingJobMap: SimpleArrayMap<Long, Job> = SimpleArrayMap()
    private val transferTracker = ArrayMap<Long, Int>()


    private var cachedIsPlayServicesAvailable: Boolean = false

    init {
        scope.launch {
            playServicesAvailable.collect { isAvailable ->
                cachedIsPlayServicesAvailable = isAvailable

                if (!isAvailable) return@collect
                _supportedStatus.value = true
            }
        }
    }

    private fun handleChangesOfDiscovered(
        currentState: NearbyConnectionsHelper.EndpointConnectedStateSnapshot,
        latestState: NearbyConnectionsHelper.EndpointConnectedStateSnapshot,
    ) {
        latestState.discovered.let { discoveredList ->
            val newEntries = mutableListOf<String>()
            val lostEntries = mutableListOf<String>()

            discoveredList.filter {
                !currentState.discovered.contains(it)
            }.toCollection(newEntries)

            currentState.discovered.filter {
                !discoveredList.contains(it)
            }.filter {
                !latestState.pending.contains(it) && !latestState.established.contains(it)
            }.toCollection(lostEntries)


            Timber.d("...these endpoints are now `discovered`: $newEntries")
            Timber.d("...these endpoints are no longer `discovered`: $lostEntries")


            val updated = HashMap(_discoveredServices.value.peers)
            lostEntries.forEach { endpointId ->
                Timber.d("...removing stall-entry from discovered: $endpointId")
                updated.values.find {
                    it.host == endpointId
                }?.let { matching ->
                    updated.remove(matching.name)
                }
            }
            newEntries.mapNotNull {
                nearby.getDiscoveredEndpoints().find { endpoint ->
                    endpoint.id == it
                }
            }.forEach { endpoint ->
                updated[endpoint.name] = FoundStall(
                    endpoint.name,
                    endpoint.id,
                    -1,
                    RegisteredNameHelper.extractTitle(endpoint.name),
                    RegisteredNameHelper.extractPki(endpoint.name),
                    RegisteredNameHelper.extractPaymail(endpoint.name)
                )

            }

            _discoveredServices.value = _discoveredServices.value.copy(
                peers = updated
            )
        }
    }


    private fun handleChangesOfConnected(
        currentState: NearbyConnectionsHelper.EndpointConnectedStateSnapshot,
        latestState: NearbyConnectionsHelper.EndpointConnectedStateSnapshot,
    ) {

        latestState.established.let { establishedList ->
            val newEntries = mutableListOf<String>()
            val lostEntries = mutableListOf<String>()

            establishedList.filter {
                !currentState.established.contains(it)
            }.toCollection(newEntries)

            currentState.established.filter {
                !establishedList.contains(it)
            }.toCollection(lostEntries)


            Timber.d("...these endpoints are now `established`; newly connected: $newEntries")
            val currentOutgoing = nearby.outgoingConnections
            newEntries.forEach { endpointId ->
                val matchingEndpoint = nearby.getConnectedEndpoints().find { endpoint ->
                    endpoint.id == endpointId
                } ?: let {
                    Timber.e(Exception())
                    return@forEach
                }
                onNewConnection(endpointId, matchingEndpoint.name, currentOutgoing.contains(matchingEndpoint))
            }
            if (currentState.established.size == 0 && newEntries.size != 0) {
                ensureSendRequestsAreProcessed()
            }


            Timber.d("...these endpoints are no longer `established`; they must have disconnected: $lostEntries")
            val historicOutgoing = nearby.historicOutgoingConnections
            lostEntries.forEach { endpointId ->
                val matchingOutgoing = historicOutgoing.find { endpoint ->
                    endpoint.id == endpointId
                }
                val isOutgoing = matchingOutgoing != null

                onLostConnection(endpointId, isOutgoing)
            }
            if (lostEntries.size != 0 && currentState.established == lostEntries) {
                Timber.d("...... lostEntries.size != 0 && currentState.established == lostEntries")
                ensureSendRequestProcessingIsTornDown()
            }
        }
    }

    private suspend fun handleChangesOfPending(
        currentState: NearbyConnectionsHelper.EndpointConnectedStateSnapshot,
        latestState: NearbyConnectionsHelper.EndpointConnectedStateSnapshot,
    ) {

        latestState.pending.let { establishedList ->
            val newEntries = mutableListOf<String>()
            val lostEntries = mutableListOf<String>()

            establishedList.filter {
                !currentState.pending.contains(it)
            }.toCollection(newEntries)

            currentState.pending.filter {
                !establishedList.contains(it)
            }.toCollection(lostEntries)

            Timber.d("...these endpoints are no longer `pending`; they must have connected: $lostEntries")
            Timber.d("...these endpoints are newly `pending`; $newEntries")

            ///start auto-connect to newly found entries
            scope.launch {
                delay(100) //is this even necessary?
                commenceAutoConnect(newEntries)
            }



        }
    }

    private fun setup() {
        Timber.d("....setup")
        millisSetup = System.currentTimeMillis()

        nearby.setup()

        clientAutoConnectJob = scope.launch {
            var currentState = NearbyConnectionsHelper.EndpointConnectedStateSnapshot()
            launch {
                nearby.endpointState.mapLatest {
                    delay(200)
                    it
                }.collect { endpoints ->

                    handleChangesOfDiscovered(currentState, endpoints)
                    handleChangesOfPending(currentState, endpoints)
                    handleChangesOfConnected(currentState, endpoints)

                    currentState = endpoints
                }
            }

            launch {
                //todo detect those that failed auto-connect
                //todo
                //todo
                //todo
                //todo
            }

        }

        incomingMessageRedirectJob = scope.launch {
            launch {
                nearby.incomingChannel.receiveAsFlow().collect { (endpoint, payload) ->
                    Timber.d("...new incoming message; endpoint: $endpoint; payload: $payload")
                    if (payload.type == Payload.Type.BYTES) {
                        processIncomingBytes(endpoint, payload.id, payload.asBytes()!!)
                    } else if (payload.type == Payload.Type.STREAM) {
                        processIncomingStream(endpoint, payload)
                    }
                }
            }

            launch {
                nearby.incomingTransferUpdateChannel.receiveAsFlow().collect { (endpoint, update) ->
                    processIncomingTransferUpdate(endpoint, update)
                }
            }
        }

        isWifiP2pEnabled = true
        isInitializing = false

        ArrayList(p2pEnabledWaitingContinuations).let { toResume ->
            p2pEnabledWaitingContinuations.clear()
            toResume.forEach { continuation ->
                continuation.resume(Unit)
            }
        }
    }


    /**
     * This must be called when both advertising & discover go down
     */
    override fun tearDown() {
        Timber.d("....tearDown")
        scope.launch {
            stopAdvertisingServiceOfLocalPeer()
            stopDiscovery()

            nearby.stopAllEndpoints()
            nearby.clearOutgoingConnectionTracking()

            incomingMessageRedirectJob?.cancel()
            clientAutoConnectJob?.cancel()

            isInitializing = false
            isWifiP2pEnabled = false

            ArrayList(p2pEnabledWaitingContinuations).let { toResume ->
                p2pEnabledWaitingContinuations.clear()
                toResume.forEach { continuation ->
                    continuation.cancel()
                }
            }
        }
    }

    private fun considerTearDown() {
        if (!_discoveredServices.value.isActive &&
            AdvertisingStatus.UNREGISTERED == advertisingStatus.value) {
            tearDown()
        }
    }

    private fun applyLocalId(
        id: P2pStallManager.PeerServiceId,
        shouldCheckAdvertising: Boolean = false,
        shouldCheckDiscovery: Boolean = false
    ) {
        //disallow proceeding `advertising` if already `discovering` with different ID & reverse
        if (id.paymail != _currentPeerId.paymail || id.pki != _currentPeerId.pki) {
            if (shouldCheckAdvertising) {
                if (_advertisingStatus.value != AdvertisingStatus.UNREGISTERED) throw Exception()
            }
            if (shouldCheckDiscovery) {
                if (_discoveredServices.value.isActive) throw Exception()
            }
        }
        _currentPeerId = id
    }

    override suspend fun beginAdvertisingServiceOfLocalPeer(port: Int, id: P2pStallManager.PeerServiceId) {
        Timber.d(".....#registerServiceForLocalPeer $id")

        try {
            ensureConnection()

            applyLocalId(id, shouldCheckDiscovery = true)
            _advertisingStatus.value = AdvertisingStatus.PREPARING

            nearby.setName(RegisteredNameHelper.assembleServiceName(millisSetup, _currentPeerId))
            nearby.startAdvertising()

        } catch (e: Exception) {
            tearDown()
            return
        }

        _advertisingStatus.value = AdvertisingStatus.REGISTERED
    }


    override suspend fun stopAdvertisingServiceOfLocalPeer() {
        if (AdvertisingStatus.UNREGISTERED == advertisingStatus.value) {
            return
        }

        val currentlyConnected = nearby.getConnectedEndpoints()

        if (nearby.isAdvertising()) {
            nearby.stopAdvertising()
        }

        //disconnect from incoming connections only
        currentlyConnected.filter { endpoint ->
            !nearby.outgoingConnections.contains(endpoint)
        }.forEach { endpoint ->
            nearby.disconnect(endpoint)
        }

        _advertisingStatus.value = AdvertisingStatus.UNREGISTERED

        considerTearDown()
    }






    override fun beginDiscovery(id: P2pStallManager.PeerServiceId) {
        scope.launch {
            try {
                ensureConnection()
                Timber.d("#beginDiscovery")

                applyLocalId(id, shouldCheckAdvertising = true)
                nearby.setName(RegisteredNameHelper.assembleServiceName(millisSetup, _currentPeerId))
                nearby.startDiscovering()

                _discoveredServices.value = _discoveredServices.value.copy(
                    isActive = true
                )

            } catch (e: Exception) {
                tearDown()
            }
        }
    }

    override fun stopDiscovery() {
        if (!_discoveredServices.value.isActive) {
            return
        }
        Timber.d("#stopDiscovery")

        nearby.stopDiscovering()

        //disconnect from outgoing connections only
        val currentlyConnected = nearby.getConnectedEndpoints()
        currentlyConnected.filter { endpoint ->
            nearby.outgoingConnections.contains(endpoint)
        }.forEach { endpoint ->
            nearby.disconnect(endpoint)
        }

        _discoveredServices.value = _discoveredServices.value.copy(
            isActive = false,
            peers = emptyMap()
        )

        considerTearDown()
    }


    private suspend fun ensureConnection() {
        if (!cachedIsPlayServicesAvailable) {
            _supportedStatus.value = false
            throw RuntimeException() //prevent further initialisation

        } else if (!_supportedStatus.value) {
            _supportedStatus.value = true
        }

        if (isWifiP2pEnabled) return
        if (isInitializing) {
            suspendCancellableCoroutine<Unit> { continuation ->
                p2pEnabledWaitingContinuations.add(continuation)
            }
            return
        }
        isInitializing = true

        suspendCancellableCoroutine<Unit> { continuation ->
            p2pEnabledWaitingContinuations.add(continuation)
            setup()
        }
    }






    suspend fun connectToEndpoint(endpointId: String) {
        val found = nearby.getDiscoveredEndpoints().find { it.id == endpointId }
            ?: throw RuntimeException()

        nearby.connectToEndpoint(found)
    }


    fun disconnectFromEndpoint(endpointId: String) {
        val matchingEndpoint = nearby.getConnectedEndpoints().find { it.id == endpointId }
            ?: let {
                Timber.d("matching endpoint not found; disconnect request has been dropped: $endpointId")
                return
            }

        nearby.disconnect(matchingEndpoint)
    }


    private suspend fun commenceAutoConnect(endpoints: List<String>) {
        endpoints.forEach {
            nearby.acceptConnection(it)
        }
    }





    private fun ensureSendRequestsAreProcessed() {
        outgoingMessageProcessingJob?.let { job ->
            Timber.d(".....job not started; already active")
            if (job.isActive) return
        }
        outgoingMessageProcessingJob = startProcessingSendRequests()
    }

    private fun ensureSendRequestProcessingIsTornDown() {
        outgoingMessageProcessingJob?.let { job ->
            Timber.d(".....ensureSendRequestProcessingIsTornDown")
            job.cancel()
            outgoingMessageProcessingJob = null
        }
    }


    private fun startProcessingSendRequests(): Job = scope.launch {
        Timber.d(".....startProcessingSendRequests")
        sendChannel.receiveAsFlow().collect { request ->
            Timber.d("....collected a request: $request")

            when (request) {
                is FileReceiveEvent -> {
                    handleSendingFileReceiveRequest(request)
                }
                is ShortSendEvent -> {
                    handleSendingShortMessage(request)
                }
                else -> {
                    handleSendingLargeMessage(request)
                }
            }

        }
        Timber.d("the collector has completed")
    }

    private fun handleSendingShortMessage(request: ShortSendEvent) {
        val messagePayload = JsonObject().let { json ->
            json.addProperty("salt", request.salt)
            json.addProperty("token", request.token)
            json.addProperty("data", request.data)
            json.toString()
        }.let { encodedMessage ->
            Payload.fromBytes(encodedMessage.toByteArray()) //theoretically these bytes could also be encrypted
        }

        nearby.send(messagePayload, setOf(request.endpointId))
    }

    private fun handleSendingFileReceiveRequest(request: FileReceiveEvent) {
        //construct the outgoing-payload
        val metaBytesPayload = JsonObject().let { json ->
            json.addProperty("fileReceiveRequest", request.token)
            json.addProperty("salt", request.salt)
            json.addProperty("data", request.data)
            json.toString()
        }.let { encodedMessage ->
            Payload.fromBytes(encodedMessage.toByteArray()) //theoretically these bytes could also be encrypted
        }

        nearby.send(metaBytesPayload, setOf(request.endpointId))
    }

    private fun handleSendingLargeMessage(request: SendEvent) {
        val streamPayload = Payload.fromStream(getStreamDataInputToSend(request))

        // Construct a simple message mapping the ID of the file payload to the desired filename.
        val metaBytesPayload = JsonObject().let { json ->
            json.addProperty("streamPayloadId", streamPayload.id)
            json.addProperty("salt", request.salt)
            json.addProperty("token", request.token)
            json.toString()
        }.let { encodedMessage ->
            Payload.fromBytes(encodedMessage.toByteArray()) //theoretically these bytes could also be encrypted
        }

        // Send the meta-info about the stream-payload as a bytes payload.
        nearby.send(metaBytesPayload, setOf(request.endpointId))

        //send stream payload
        nearby.send(streamPayload, setOf(request.endpointId))
    }

    private fun handleSendingErrorResponse(
        endpointId: String,
        token: String,
        type: RequestType
    ) {
        val bytesPayload = JsonObject().let { json ->
            json.addProperty("errorResponse", true)
            json.addProperty("token", token)
            json.addProperty("type", type.const)
            json.toString()
        }.let { encodedMessage ->
            Payload.fromBytes(encodedMessage.toByteArray()) //theoretically these bytes could also be encrypted
        }

        nearby.send(bytesPayload, setOf(endpointId))
    }



    private fun processIncomingBytes(
        endpoint: NearbyConnectionsHelper.Endpoint,
        payloadId: Long,
        bytes: ByteArray
    ) {
        val encodedMetaMessage = String(bytes, StandardCharsets.UTF_8)
        val json = gson.fromJson(encodedMetaMessage, JsonObject::class.java)

        when {
            json.has("errorResponse") -> {
                //receiving info about an error on the other device during the processing of an earlier message
                val token = json.get("token").asString
                val type = json.get("type").asString

                try {
                    delegateIncomingErrorResponseHandling(ErrorResponseHolder(
                        endpoint.id,
                        token,
                        RequestType.parse(type)
                    ))
                } catch (e: Exception) {
                    Timber.e(e)
                }

            }
            json.has("streamPayloadId") -> {
                //receiving metadata for large-message-stream
                val streamPayloadId = json.get("streamPayloadId").asLong
                val salt = json.get("salt").asString
                val token = json.get("token").asString

                incomingStreamsMetaMap[streamPayloadId] = StreamMeta(
                    endpoint,
                    streamPayloadId,
                    salt,
                    token
                )

                Timber.d("...processIncomingBytes of STREAM; payloadId:$payloadId; streamPayloadId:${streamPayloadId}")

                ensureCompletedTransfersGetRedirected(streamPayloadId)

            }
            json.has("fileReceiveRequest") -> {
                //receiving a file-request message
                val token = json.get("fileReceiveRequest").asString
                val salt = json.get("salt").asString
                val data = json.get("data").asString

                handleIncomingFileRequest(
                    endpoint,
                    token,
                    salt,
                    data
                )

            }
            json.has("fileReceiveResponse") -> {
                //receiving metadata for the response of an earlier file-request outgoing-message
                val token = json.get("fileReceiveResponse").asString
                val streamPayloadId = json.get("payloadId").asLong
                val saltOfMeta = json.get("saltOfMeta").asString
                val saltOfStream = json.get("saltOfStream").asString
                val meta = json.get("meta").asString

                incomingStreamsMetaMap[streamPayloadId] = FileResponseStreamMeta(
                    endpoint,
                    streamPayloadId,
                    saltOfStream,
                    token,
                    saltOfMeta,
                    meta
                )

                Timber.d("metadata for file-response-stream; payloadId:$payloadId; streamPayloadId:${streamPayloadId}")

                ensureCompletedTransfersGetRedirected(streamPayloadId)

            }
            else -> {
                //receiving a short-message
                val salt = json.get("salt").asString
                val token = json.get("token").asString
                val data = json.get("data").asString

                Timber.d("short-message; payloadId:$payloadId")

                try {
                    delegateIncomingByteMessageHandling(
                        IncomingByteMessage(
                            endpoint.id,
                            token,
                            salt,
                            data
                        )
                    )

                } catch (e: Exception) {
                    Timber.e(e)
                    handleSendingErrorResponse(
                        endpoint.id,
                        token,
                        RequestType.SMALL_MESSAGE
                    )
                }
            }
        }
    }

    private fun processIncomingStream(
        endpoint: NearbyConnectionsHelper.Endpoint,
        payload: Payload
    ) {
        incomingStreamsMap[payload.id] = payload
        Timber.d("new incoming stream-payload; payloadId:${payload.id}")

        ensureCompletedTransfersGetRedirected(payload.id)
    }


    /**
     * TODO must inform node-side when outgoing payload either fails or is cancelled
     */
    private fun processIncomingTransferUpdate(
        endpoint: NearbyConnectionsHelper.Endpoint,
        update: PayloadTransferUpdate
    ) {
        val matchingStreamTransferJob = streamingJobMap[update.payloadId]

        if (matchingStreamTransferJob != null) {
            transferTracker[update.payloadId] = update.status

            when(update.status) {
                PayloadTransferUpdate.Status.SUCCESS -> {
                    Timber.d("...handleStreamTransferUpdate SUCCESS; payloadId:${update.payloadId}")
                }
                PayloadTransferUpdate.Status.FAILURE -> {
                    Timber.d("...handleStreamTransferUpdate FAILURE; payloadId:${update.payloadId}")
                    //todo
                    //todo
                    //todo
                }
                PayloadTransferUpdate.Status.CANCELED -> {
                    Timber.d("...handleStreamTransferUpdate CANCELED; payloadId:${update.payloadId}")
                    //todo
                    //todo
                    //todo
                }
                else -> {
                    Timber.d("...handleStreamTransferUpdate update of stream; payloadId:${update.payloadId}")
                }
            }

        } else {
            when {
                PayloadTransferUpdate.Status.SUCCESS == update.status -> {
                    Timber.d(".... handleTransferUpdate SUCCESS endpoint: ${endpoint.id} payloadId:${update.payloadId}")
                }
                PayloadTransferUpdate.Status.FAILURE == update.status -> {
                    Timber.d("......handleTransferUpdate FAILURE endpoint: ${endpoint.id} payloadId:${update.payloadId}")
                    //todo
                    //todo
                    //todo
                }
                PayloadTransferUpdate.Status.CANCELED == update.status -> {
                    Timber.d("......handleTransferUpdate CANCELED endpoint: ${endpoint.id} payloadId:${update.payloadId}")
                    //todo
                    //todo
                    //todo
                }
                else -> {
                    Timber.d("......handleTransferUpdate ... endpoint: ${endpoint.id} payloadId:${update.payloadId}")
                }
            }
        }
    }


    private fun ensureCompletedTransfersGetRedirected(payloadId: Long) {
        val meta = incomingStreamsMetaMap[payloadId]
        val payload = incomingStreamsMap[payloadId]

        if (meta != null && payload != null) {
            incomingStreamsMetaMap.remove(payloadId)
            incomingStreamsMap.remove(payloadId)

            Timber.d("ready to pass the stream to the delegate-handler; payloadId: $payloadId; endpoint: ${meta.endpoint};  token: ${meta.token}")

            val job = scope.launch {
                val getStreamStatus: ()->Int = {
                    when (transferTracker[payloadId]) {
                        null -> 0
                        PayloadTransferUpdate.Status.SUCCESS -> 1
                        PayloadTransferUpdate.Status.FAILURE -> -1
                        PayloadTransferUpdate.Status.IN_PROGRESS -> 0
                        else -> -1
                    }
                }

                if (meta is FileResponseStreamMeta) {
                    try {
                        delegateIncomingFileResponseHandling(
                            FileResponseRedirectingHolder(
                                meta,
                                payload.asStream()!!.asInputStream(),
                                getStreamStatus
                            )
                        )
                    } catch (e: Exception) {
                        Timber.e(e, "crash during processing of file-response stream-payload $payloadId")
                        try {
                            delegateIncomingErrorResponseHandling(ErrorResponseHolder(
                                meta.endpoint.id,
                                meta.token,
                                RequestType.FILE_RESPONSE
                            ))
                        } catch (e: Exception) {
                            Timber.e(e)
                        }
                    }

                } else {
                    try {
                        delegateIncomingMessageHandling(
                            StreamResponseHolder(
                                meta,
                                payload.id,
                                payload.asStream()!!.asInputStream(),
                                getStreamStatus
                            )
                        )
                    } catch (e: Exception) {
                        Timber.e(e, "crash during processing of stream-payload $payloadId")
                        handleSendingErrorResponse(
                            meta.endpoint.id,
                            meta.token,
                            RequestType.LARGE_MESSAGE
                        )
                    }
                }

                streamingJobMap.remove(payloadId)
                transferTracker.remove(payloadId)

            }

            streamingJobMap.put(payloadId, job)

        } else {
            //further processing can only be done when both values are ready
            //Timber.d("...dropped $payloadId; transferring requires both - meta: $meta payload: $payload")
        }
    }


    private fun handleIncomingFileRequest(
        endpoint: NearbyConnectionsHelper.Endpoint,
        token: String,
        salt: String,
        data: String
    ) {
        scope.launch {
            try {
                val response = delegateIncomingFileRequestHandling(
                    FileReceiveEvent(
                        endpoint.id,
                        token,
                        salt,
                        data
                    )
                )

                //
                //
                //pass the response to the nearby-comms library
                val streamPayload = Payload.fromStream(response.stream)

                // Construct a message mapping the ID of the file-response payload to the desired filename.
                val metaBytesPayload = JsonObject().let { json ->
                    json.addProperty("fileReceiveResponse", token)
                    json.addProperty("payloadId", streamPayload.id)
                    json.addProperty("saltOfMeta", response.saltOfMeta)
                    json.addProperty("saltOfStream", response.saltOfStream)
                    json.addProperty("meta", response.meta)
                    json.toString()
                }.let { encodedMessage ->
                    Payload.fromBytes(encodedMessage.toByteArray()) //theoretically these bytes could also be encrypted
                }

                // Send the meta-info about the stream-payload as a bytes payload.
                nearby.send(metaBytesPayload, setOf(endpoint.id))

                //send stream payload
                nearby.send(streamPayload, setOf(endpoint.id))
            } catch (e: Exception) {
                Timber.e(e)
                handleSendingErrorResponse(
                    endpoint.id,
                    token,
                    RequestType.FILE_REQUEST
                )
            }
        }
    }


    open class StreamMeta(
        val endpoint: NearbyConnectionsHelper.Endpoint,
        val streamPayloadId: Long,
        val salt: String,
        val token: String
    )

    class FileResponseStreamMeta(
        endpoint: NearbyConnectionsHelper.Endpoint,
        streamPayloadId: Long,
        saltOfStream: String,
        token: String,
        val saltOfMeta: String,
        val meta: String,
    ) : StreamMeta(
        endpoint,
        streamPayloadId,
        saltOfStream,
        token
    )

    class IncomingByteMessage(
        val endpointId: String,
        val token: String,
        val salt: String,
        val data: String
    )


    ////////////////


    object RegisteredNameHelper {
        fun assembleServiceName(
            nonce: Long,
            id: P2pStallManager.PeerServiceId
        ): String {
            return "$nonce;${id.paymail};${id.pki};${id.title}"
        }

        fun extractStartMillis(name: String): Long {
            val separatorPosition = name.indexOf(";")
            if (separatorPosition < 0) throw Exception()
            return name.substring(0, separatorPosition).toLong()
        }

        fun extractPaymail(name: String): String {
            val nameWithoutNonce = name.let {
                val separatorPosition = it.indexOf(";")
                it.substring(separatorPosition + 1)
            }
            val separatorPosition = nameWithoutNonce.indexOf(";")
            if (separatorPosition < 0) throw Exception()
            return nameWithoutNonce.substring(0, separatorPosition)
        }

        fun extractPki(name: String): String {
            val nameWithoutNonce = name.let {
                val separatorPosition = it.indexOf(";")
                it.substring(separatorPosition + 1)
            }
            val separator0Position = nameWithoutNonce.indexOf(";")
            if (separator0Position < 0) throw Exception()
            val separator1Position = nameWithoutNonce.indexOf(";", separator0Position + 1)
            if (separator1Position < 0) throw Exception()
            return nameWithoutNonce.substring(separator0Position + 1, separator1Position)
        }

        fun extractTitle(name: String): String {
            val nameWithoutNonce = name.let {
                val separatorPosition = it.indexOf(";")
                it.substring(separatorPosition + 1)
            }
            val separator0Position = nameWithoutNonce.indexOf(";")
            if (separator0Position < 0) throw Exception()
            val separator1Position = nameWithoutNonce.indexOf(";", separator0Position + 1)
            if (separator1Position < 0) throw Exception()
            return nameWithoutNonce.substring(separator1Position + 1)
        }

    }

    enum class RequestType(val const: String) {
        SMALL_MESSAGE("smallMessage"),
        LARGE_MESSAGE("largeMessage"),
        FILE_REQUEST("fileRequest"),
        FILE_RESPONSE("fileResponse");

        companion object {
            fun parse(const: String): RequestType {
                return values().find { it.const == const }!!
            }
        }
    }


    ////////////////


    open class SendEvent(
        val endpointId: String,
        val token: String,
        val salt: String
    )

    class FileReceiveEvent(
        endpointId: String,
        token: String,
        salt: String,
        val data: String
    ): SendEvent(endpointId, token, salt)

    class ShortSendEvent(
        endpointId: String,
        token: String,
        salt: String,
        val data: String
    ): SendEvent(endpointId, token, salt)


    ////////////////


    class StreamResponseHolder(
        val meta: StreamMeta,
        val streamPayloadId: Long,
        val stream: InputStream,
        val getStreamStatus: ()->Int
    )

    open class FileResponseHolder(
        val saltOfMeta: String,
        val saltOfStream: String,
        val meta: String,
        val stream: InputStream
    )

    class FileResponseRedirectingHolder(
        meta: FileResponseStreamMeta,
        stream: InputStream,
        val getStreamStatus: ()->Int
    ) : FileResponseHolder(
        meta.saltOfMeta,
        meta.salt,
        meta.meta,
        stream
    ) {
        val endpointId: String = meta.endpoint.id
        val token: String = meta.token
    }

    class ErrorResponseHolder(
        val endpoint: String,
        val token: String,
        val type: RequestType
    )

}