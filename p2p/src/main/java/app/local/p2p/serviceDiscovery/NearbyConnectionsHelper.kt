package app.local.p2p.serviceDiscovery

import android.content.Context
import androidx.annotation.CallSuper
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.connection.*


import com.google.android.gms.tasks.OnFailureListener

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.suspendCancellableCoroutine

import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException


/**
 * Modified version of following code:
 * https://github.com/android/connectivity-samples/blob/main/NearbyConnectionsWalkieTalkie/app/src/main/java/com/google/location/nearby/apps/walkietalkie/ConnectionsActivity.java
 */
class NearbyConnectionsHelper(
    val context: Context
) {

    /** Our handler to Nearby Connections.  */
    private val mConnectionsClient: ConnectionsClient
        get() {
            return Nearby.getConnectionsClient(context)
        }

    /** The devices we've discovered near us.  */
    private val mDiscoveredEndpoints: MutableMap<String, Endpoint> = HashMap()

    /**
     * The devices we have pending connections to. They will stay pending until we call [ ][.acceptConnection] or [.rejectConnection].
     */
    private val mPendingConnections: MutableMap<String, Endpoint> = HashMap()

    /**
     * The devices we are currently connected to. For advertisers, this may be large. For discoverers,
     * there will only be one entry in this map.
     */
    private val mEstablishedConnections: MutableMap<String, Endpoint> = HashMap()

    data class EndpointConnectedStateSnapshot(
        val discovered: List<String> = emptyList(),
        val pending: List<String> = emptyList(),
        val established: List<String> = emptyList()
    )

    private val _endpointState = MutableStateFlow(EndpointConnectedStateSnapshot())
    val endpointState: StateFlow<EndpointConnectedStateSnapshot>
        get() = _endpointState

    private fun refreshEndpointState() {
        _endpointState.value = EndpointConnectedStateSnapshot(
            discovered = ArrayList(mDiscoveredEndpoints.keys),
            pending = ArrayList(mPendingConnections.keys),
            established = ArrayList(mEstablishedConnections.keys)
        )
    }

    /**
     * True if we are asking a discovered device to connect to us. While we ask, we cannot ask another
     * device.
     */
    private var mIsConnecting = false

    /** True if we are discovering.  */
    private var mIsDiscovering = false

    /** True if we are advertising.  */
    private var mIsAdvertising = false

    /** Callbacks for connections to other devices.  */
//    private val mConnectionLifecycleCallback: ConnectionLifecycleCallback = ConnectionCallback()
    inner class ConnectionCallback : ConnectionLifecycleCallback() {
            override fun onConnectionInitiated(endpointId: String, connectionInfo: ConnectionInfo) {
                logD("onConnectionInitiated(endpointId=$endpointId, endpointName=${connectionInfo.endpointName}, endpointInfo=${String(connectionInfo.endpointInfo)}, auth-digits:${connectionInfo.authenticationDigits}, isIncomingConnection:${connectionInfo.isIncomingConnection})")
                val endpoint = Endpoint(endpointId, connectionInfo.endpointName)
                mPendingConnections[endpointId] = endpoint
                refreshEndpointState()
                NearbyConnectionsHelper@onConnectionInitiated(endpoint, connectionInfo)
            }

            override fun onConnectionResult(endpointId: String, result: ConnectionResolution) {
                logD("onConnectionResponse(endpointId=$endpointId, result=$result)")

                // We're no longer connecting
                mIsConnecting = false


                //
                //must handle the awaiting continuations
                val toResume: List<Continuation<Unit>> =
                    connectionAwaitingContinuations[endpointId] ?: let {
                        listOf()
                    }
                connectionAwaitingContinuations.remove(endpointId)


                if (!result.status.isSuccess) {
                    logW("Connection failed. Received status ${NearbyConnectionsHelper@toString(result.status)}.")

                    mPendingConnections.remove(endpointId).let {
                        it?.let {
                            _outgoingConnections.remove(it)
                        }
                        refreshEndpointState()
                        onConnectionFailed(it)
                    }

                    toResume.forEach { continuation ->
                        continuation.resumeWithException(Exception(result.status.toString()))
                    }

                    return
                }
                mPendingConnections.remove(endpointId)!!.let {
                    refreshEndpointState()
                    connectedToEndpoint(it)


                    toResume.forEach { continuation ->
                        continuation.resume(Unit)
                    }
                }

            }

            override fun onDisconnected(endpointId: String) {
                if (!mEstablishedConnections.containsKey(endpointId)) {
                    logW("Unexpected disconnection from endpoint $endpointId")
                    return
                }
                disconnectedFromEndpoint(mEstablishedConnections[endpointId]!!)
            }
        }

    /** Callbacks for payloads (bytes of data) sent from another device to us.  */
//    private val mPayloadCallback: PayloadCallback = object : PayloadCallback() {
    inner class P2pPayloadCallback : PayloadCallback() {
        override fun onPayloadReceived(endpointId: String, payload: Payload) {
//            logD("onPayloadReceived(endpointId=$endpointId, payload=$payload)")
            onReceive(mEstablishedConnections[endpointId]!!, payload)
        }

        override fun onPayloadTransferUpdate(endpointId: String, update: PayloadTransferUpdate) {
//            logD("onPayloadTransferUpdate(endpointId=$endpointId, update=$update)")
            onReceiveTransferUpdate(mEstablishedConnections[endpointId]!!, update)
        }
    }


    fun setup() {

    }

    /**
     * Sets the device to advertising mode. It will broadcast to other devices in discovery mode.
     * Either [.onAdvertisingStarted] or [.onAdvertisingFailed] will be called once
     * we've found out if we successfully entered this mode.
     */
    suspend fun startAdvertising() {
        if (mIsAdvertising) return
        mIsAdvertising = true

        suspendCancellableCoroutine<Unit> { continuation ->
            val localEndpointName: String = getName()
            val advertisingOptions = AdvertisingOptions.Builder()
            advertisingOptions.setStrategy(getStrategy())
            advertisingOptions.setDisruptiveUpgrade(true)
            try {

                mConnectionsClient
                    .startAdvertising(
                        localEndpointName.toByteArray(),
                        getServiceId(),
                        ConnectionCallback(),
                        advertisingOptions.build()
                    ).let { task ->
                        AddSuccessListener.invoke(task) {
                            try {
                                logV("Now advertising endpoint $localEndpointName")
                                onAdvertisingStarted()
                                continuation.resume(Unit)
                            } catch (e: Exception) {
                                Timber.e(e)
                            }
                        }
                        task.addOnFailureListener { e ->
                            mIsAdvertising = false
                            logW("startAdvertising() failed.", e)
                            onAdvertisingFailed()
                            continuation.resumeWithException(e)
                        }

                    }
            } catch (e: Exception) {
                Timber.e(e)
                try {
                    logV("Now advertising endpoint $localEndpointName")
                    onAdvertisingStarted()
                    continuation.resume(Unit)
                } catch (e: Exception) {
                    Timber.e(e)
                }

            }
        }

    }

    /** Stops advertising.  */
    fun stopAdvertising() {
        if (!mIsAdvertising) return
        mIsAdvertising = false

        mConnectionsClient.stopAdvertising()
    }

    /** Returns `true` if currently advertising.  */
    fun isAdvertising(): Boolean {
        return mIsAdvertising
    }

    /** Called when advertising successfully starts. Override this method to act on the event.  */
    private fun onAdvertisingStarted() {
    }

    /** Called when advertising fails to start. Override this method to act on the event.  */
    private fun onAdvertisingFailed() {
    }


    /**
     * Called when a pending connection with a remote endpoint is created. Use [ConnectionInfo]
     * for metadata about the connection (like incoming vs outgoing, or the authentication token). If
     * we want to continue with the connection, call [.acceptConnection]. Otherwise,
     * call [.rejectConnection].
     */
    private fun onConnectionInitiated(endpoint: Endpoint?, connectionInfo: ConnectionInfo?) {}

//    /** Rejects a connection request.  */
//    private fun rejectConnection(endpoint: Endpoint) {
//        mConnectionsClient
//            .rejectConnection(endpoint.id)
//            .addOnFailureListener { e -> logW("rejectConnection() failed.", e) }
//    }

    /**
     * Sets the device to discovery mode. It will now listen for devices in advertising mode. Either
     * [.onDiscoveryStarted] or [.onDiscoveryFailed] will be called once we've found
     * out if we successfully entered this mode.
     */
    suspend fun startDiscovering() {
        if (isDiscovering()) return
        mIsDiscovering = true

        suspendCancellableCoroutine<Unit> { continuation ->
            mDiscoveredEndpoints.clear()
            refreshEndpointState()
            val discoveryOptions = DiscoveryOptions.Builder()
            discoveryOptions.setStrategy(getStrategy())
            mConnectionsClient
                .startDiscovery(
                    getServiceId(),
                    object : EndpointDiscoveryCallback() {
                        override fun onEndpointFound(endpointId: String, info: DiscoveredEndpointInfo) {
                            logD("onEndpointFound(endpointId=$endpointId, serviceId=${info.serviceId}, endpointName=${info.endpointName}, endpointInfo=${String(info.endpointInfo)})")

                            if (getServiceId().equals(info.serviceId)) {
                                val endpoint = Endpoint(endpointId, info.endpointName)
                                mDiscoveredEndpoints.put(endpointId, endpoint)
                                refreshEndpointState()
                                onEndpointDiscovered(endpoint)
                            }
                        }

                        override fun onEndpointLost(endpointId: String) {
                            logD("onEndpointLost(endpointId=$endpointId)")
                            if (mDiscoveredEndpoints.containsKey(endpointId)) {
                                mDiscoveredEndpoints.remove(endpointId)
                                refreshEndpointState()

                            } else {
                                logD("......#onEndpointLost for endpoint that was not in `discovered` !!!!")
                            }
                        }
                    },
                    discoveryOptions.build()
                ).let { task ->
                    AddSuccessListener.invoke(task) {
                        onDiscoveryStarted()
                        continuation.resume(Unit)
                    }
                    task.addOnFailureListener { e ->
                        mIsDiscovering = false
                        logW("startDiscovering() failed.", e)
                        onDiscoveryFailed()
                        continuation.resumeWithException(Exception())
                    }

                }

        }



    }

    /** Stops discovery.  */
    fun stopDiscovering() {
        if (!isDiscovering()) return
        mIsDiscovering = false
        mConnectionsClient.stopDiscovery()
    }

    /** Returns `true` if currently discovering.  */
    fun isDiscovering(): Boolean {
        return mIsDiscovering
    }

    /** Called when discovery successfully starts. Override this method to act on the event.  */
    private fun onDiscoveryStarted() {}

    /** Called when discovery fails to start. Override this method to act on the event.  */
    private fun onDiscoveryFailed() {}

    /**
     * Called when a remote endpoint is discovered. To connect to the device, call [ ][.connectToEndpoint].
     */
    private fun onEndpointDiscovered(endpoint: Endpoint?) {}


    /** Disconnects from the given endpoint.  */
    fun disconnect(endpoint: Endpoint) {
        mConnectionsClient.disconnectFromEndpoint(endpoint.id)
        mEstablishedConnections.remove(endpoint.id)
        refreshEndpointState()
    }

    /** Disconnects from all currently connected endpoints.  */
    fun disconnectFromAllEndpoints() {
        for (endpoint in mEstablishedConnections.values) {
            mConnectionsClient.disconnectFromEndpoint(endpoint.id)
        }
        mEstablishedConnections.clear()
        refreshEndpointState()
    }

    /** Resets and clears all state in Nearby Connections.  */
    fun stopAllEndpoints() {
        mConnectionsClient.stopAllEndpoints()
        mIsAdvertising = false
        mIsDiscovering = false
        mIsConnecting = false
        mDiscoveredEndpoints.clear()
        mPendingConnections.clear()
        mEstablishedConnections.clear()
        refreshEndpointState()
    }


    /**
     * Keeps a track of outgoing connection to assist with proper cleanup when stopping
     * advertising VS when stopping discovering
     */
    private val _outgoingConnections = mutableListOf<Endpoint>()
    val outgoingConnections: List<Endpoint>
        get() = _outgoingConnections

    //todo refactor so that the existence of the following is not required
    private val _historicOutgoingConnections = mutableListOf<Endpoint>()
    val historicOutgoingConnections: List<Endpoint>
        get() = _historicOutgoingConnections



    fun clearOutgoingConnectionTracking() {
        _outgoingConnections.clear()
        _historicOutgoingConnections.clear()
    }

    /**
     * Sends a connection request to the endpoint. Either [.onConnectionInitiated] or [.onConnectionFailed] will be called once we've found out
     * if we successfully reached the device.
     */
    suspend fun connectToEndpoint(endpoint: Endpoint) {
        logV("Sending a connection request to endpoint $endpoint")
        // Mark ourselves as connecting so we don't connect multiple times
        mIsConnecting = true

        _outgoingConnections.add(endpoint)
        _historicOutgoingConnections.add(endpoint)


        suspendCancellableCoroutine<Unit> { continuation ->
            // Ask to connect
            val optionsBuilder = ConnectionOptions.Builder().setDisruptiveUpgrade(true)
            mConnectionsClient
                .requestConnection(
                    getName(),
                    endpoint.id,
                    ConnectionCallback(),
                    optionsBuilder.build()
                ).let { task ->
                    AddSuccessListener.invoke(task) {
                        continuation.resume(Unit)
                    }
                    task.addOnFailureListener(
                        OnFailureListener { e ->
                            if (e is ApiException) {
                                if (ConnectionsStatusCodes.STATUS_ALREADY_CONNECTED_TO_ENDPOINT == e.statusCode) {
                                    onGotStatusAlreadyConnectedToEndpoint(endpoint.id)
                                    continuation.resume(Unit)
                                    return@OnFailureListener
                                }
                            }

                            _outgoingConnections.remove(endpoint)

                            logW("requestConnection() failed.", e)
                            mIsConnecting = false
                            onConnectionFailed(endpoint)

                            continuation.resumeWithException(e)
                        })
                }

        }


    }







    private fun onGotStatusAlreadyConnectedToEndpoint(endpointId: String) {
        Timber.d("...onGotStatusAlreadyConnectedToEndpoint $endpointId")
        var gotEndpoint: Endpoint? = null

        if (mDiscoveredEndpoints.containsKey(endpointId)) {
            gotEndpoint = mDiscoveredEndpoints[endpointId]
        }

        if (gotEndpoint == null) {
            logE("could not recover endpoint: $endpointId", Exception())
            return
        }

        if (mPendingConnections.containsKey(endpointId)) {
            throw Exception(".....temp")
//            gotEndpoint = mPendingConnections[endpointId]
//            mPendingConnections.remove(endpointId)
        } else {
            mPendingConnections[endpointId] = gotEndpoint
            refreshEndpointState()
            NearbyConnectionsHelper@onConnectionInitiated(gotEndpoint, null)
        }



//        if (!mEstablishedConnections.containsKey(endpointId)) {
//            connectedToEndpoint(gotEndpoint)
//        }



//        val toResume: List<Continuation<Unit>> =
//            connectionAwaitingContinuations[endpointId] ?: let {
//                listOf()
//            }
//        connectionAwaitingContinuations.remove(endpointId)
//
//        toResume.forEach { continuation ->
//            continuation.resume(Unit)
//        }

    }


    //todo
    //todo
    //todo
    //todo
    //todo


    val connectionAwaitingContinuations = HashMap<String, MutableList<Continuation<Unit>>>()

    private fun onStartingAwaitingConnection(endpointId: String, continuation: Continuation<Unit>) {
        connectionAwaitingContinuations[endpointId].let { found ->
            val list = found ?: let {
                val newList = mutableListOf<Continuation<Unit>>()
                connectionAwaitingContinuations[endpointId] = newList
                newList
            }

            list.add(continuation)
        }
    }

    private fun onFailedAcceptingConnection(endpointId: String, continuation: Continuation<Unit>) {
        connectionAwaitingContinuations[endpointId].let { found ->
            val list = found ?: return

            list.remove(continuation)
            if (list.isEmpty()) connectionAwaitingContinuations.remove(endpointId)
        }
    }

    suspend fun acceptConnection(endpointId: String) {
        logD("#acceptConnection $endpointId; started")

        suspendCancellableCoroutine<Unit> { continuation ->
            onStartingAwaitingConnection(endpointId, continuation)
            mConnectionsClient.acceptConnection(endpointId, P2pPayloadCallback()).let { task ->
                AddSuccessListener.invoke(task) {
                    logD("....acceptConnection; success; $endpointId")
                }
                task.addOnFailureListener { e ->
                    logW("acceptConnection() failed.", e)
                    onFailedAcceptingConnection(endpointId, continuation)
                    continuation.resumeWithException(e)
                }
            }

        }
    }


    /** Returns `true` if we're currently attempting to connect to another device.  */
    private fun isConnecting(): Boolean {
        return mIsConnecting
    }

    private fun connectedToEndpoint(endpoint: Endpoint) {
        logD("connectedToEndpoint(endpoint=$endpoint)")
        mEstablishedConnections.put(endpoint.id, endpoint)
        refreshEndpointState()
        onEndpointConnected(endpoint)
    }


    private fun disconnectedFromEndpoint(endpoint: Endpoint) {
        logD("disconnectedFromEndpoint(endpoint=$endpoint)")
        mEstablishedConnections.remove(endpoint.id)
        _outgoingConnections.remove(endpoint)
        refreshEndpointState()
        onEndpointDisconnected(endpoint)
    }

    /**
     * Called when a connection with this endpoint has failed. Override this method to act on the
     * event.
     */
    private fun onConnectionFailed(endpoint: Endpoint?) {}

    /** Called when someone has connected to us. Override this method to act on the event.  */
    private fun onEndpointConnected(endpoint: Endpoint?) {}

    /** Called when someone has disconnected. Override this method to act on the event.  */
    private fun onEndpointDisconnected(endpoint: Endpoint?) {}

    /** Returns a list of currently connected endpoints.  */
    fun getDiscoveredEndpoints(): Set<Endpoint> {
        return HashSet(mDiscoveredEndpoints.values)
    }

    /** Returns a list of currently connected endpoints.  */
    fun getConnectedEndpoints(): Set<Endpoint> {
        return HashSet(mEstablishedConnections.values)
    }

//    /**
//     * Sends a [Payload] to all currently connected endpoints.
//     *
//     * @param payload The data you want to send.
//     */
//    private fun send(payload: Payload) {
//        send(payload, mEstablishedConnections.keys)
//    }

    fun send(payload: Payload, endpoints: Set<String>) {
        mConnectionsClient.sendPayload(ArrayList(endpoints), payload).let { task ->
//            AddSuccessListener.invoke(task) {
//                Timber.d("success")
//            }
            task.addOnFailureListener { e ->
                logW("sendPayload() failed; $endpoints ... $payload", e)

                if (e is ApiException) {
                    if (ConnectionsStatusCodes.STATUS_ENDPOINT_UNKNOWN == e.statusCode) {
                        //none of the endpoints are not connected
                    }
                }
            }

        }
    }

    /**
     * Someone connected to us has sent us data. Override this method to act on the event.
     *
     * @param endpoint The sender.
     * @param payload The data.
     */
    private fun onReceive(endpoint: Endpoint, payload: Payload) {
        _incomingChannel.trySend(endpoint to payload)
    }

    private fun onReceiveTransferUpdate(endpoint: Endpoint, update: PayloadTransferUpdate) {
        _incomingTransferUpdateChannel.trySend(endpoint to update)
    }

    private var name: String = "stall_01"

    fun setName(name: String) {
        this.name = name
    }


    private val _incomingChannel = Channel<Pair<Endpoint, Payload>>(
        capacity = Channel.UNLIMITED
    )
    val incomingChannel: ReceiveChannel<Pair<Endpoint, Payload>>
        get() = _incomingChannel


    private val _incomingTransferUpdateChannel = Channel<Pair<Endpoint, PayloadTransferUpdate>>(
        capacity = Channel.UNLIMITED
    )
    val incomingTransferUpdateChannel: ReceiveChannel<Pair<Endpoint, PayloadTransferUpdate>>
        get() = _incomingTransferUpdateChannel

    /** Returns the client's name. Visible to others when connecting.  */
    private fun getName(): String = name

    /**
     * Returns the service id. This represents the action this connection is for. When discovering,
     * we'll verify that the advertiser has the same service id before we consider connecting to them.
     */
    private fun getServiceId(): String = "bsv_p2p"

    /**
     * Returns the strategy we use to connect to other devices. Only devices using the same strategy
     * and service id will appear when discovering. Stragies determine how many incoming and outgoing
     * connections are possible at the same time, as well as how much bandwidth is available for use.
     */
    private fun getStrategy(): Strategy = Strategy.P2P_CLUSTER

    /**
     * Transforms a [Status] into a English-readable message for logging.
     *
     * @param status The current status
     * @return A readable String. eg. [404]File not found.
     */
    private fun toString(status: Status): String? {
        CommonStatusCodes.SUCCESS
        return java.lang.String.format(
            Locale.US,
            "[%d]%s",
            status.statusCode,
            if (status.statusMessage != null) status.statusMessage else ConnectionsStatusCodes.getStatusCodeString(
                status.statusCode
            )
        )
    }


    @CallSuper
    private fun logV(msg: String?) {
        Timber.v(msg)
    }

    @CallSuper
    private fun logD(msg: String?) {
        Timber.d(msg)
    }

    @CallSuper
    private fun logW(msg: String?) {
        Timber.w(msg)
    }

    @CallSuper
    private fun logW(msg: String?, e: Throwable?) {
        Timber.v(e, msg)
    }

    @CallSuper
    private fun logE(msg: String?, e: Throwable?) {
        Timber.e(e, msg)
    }

    /** Represents a device we can talk to.  */
    class Endpoint constructor(
        val id: String,
        val name: String
    ) {

        override fun equals(obj: Any?): Boolean {
            if (obj is Endpoint) {
                return id == obj.id
            }
            return false
        }

        override fun hashCode(): Int {
            return id.hashCode()
        }

        override fun toString(): String {
            return "Endpoint{id=$id, name=$name}"
        }
    }

}