package app.local.p2p.serviceDiscovery

import android.content.Context
import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import android.util.Patterns
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.receiveAsFlow
import timber.log.Timber
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume

class P2pStallRegistryHelper0(
    val context: Context,
    val scope: CoroutineScope
) : P2pStallRegistry {

    companion object {
        private const val SERVICE_PROTOCOL = "peer_bsv_ws"
        const val SERVICE_NAME_PREFIX = "NsdPeer"
        const val SERVICE_TYPE = "_$SERVICE_PROTOCOL._tcp"

        const val KEY_TITLE = "title"
        const val KEY_PKI = "pki"
        const val KEY_PAYMAIL = "paymail"
    }

    private val nsdManager: NsdManager by lazy {
        context.getSystemService(Context.NSD_SERVICE) as NsdManager
    }



    private var _supportedStatus = MutableStateFlow(true)
    override val supportedStatus: StateFlow<Boolean>
        get() = _supportedStatus


    private val _advertisingStatus = MutableStateFlow(AdvertisingStatus.UNREGISTERED)
    override val advertisingStatus: StateFlow<AdvertisingStatus>
        get() = _advertisingStatus



    private val _discoveredServices = MutableStateFlow(DiscoveryModel(
        false,
        mapOf()
    ))
    override val discoveredServices: StateFlow<DiscoveryModel>
        get() = _discoveredServices



    private var _currentPeerId = P2pStallManager.PeerServiceId(
        "INVALID_UNSET",
        "INVALID_UNSET",
        "INVALID_UNSET"
    )
    override val currentPeerId: P2pStallManager.PeerServiceId
        get() = _currentPeerId



    private var resolveRequestFulfillment: Job? = null


    private val requestStopServiceDiscovery: ()->Unit by lazy {{
        scope.launch {
            stopDiscovery()
        }
    }}

    private val resolveServicePeerListener = ResolveServicePeerListener()
    private val peerRegistrationListener = ServiceRegistrationListener()
    private val peerDiscoveryListener = ServiceDiscoveryListener(
        onPotentialServiceFound = { serviceInfo ->
            val gotStall = FoundStall.create(serviceInfo) ?: let {
                Timber.d("service dropped because it did not conform to the expected protocol")
                return@ServiceDiscoveryListener
            }

            val updated = HashMap<String, FoundStall>(_discoveredServices.value.peers)
            updated[serviceInfo.serviceName] = gotStall
            _discoveredServices.value = _discoveredServices.value.copy(
                peers = updated
            )

            requestResolveService(serviceInfo)

        },
        onDiscoveredServiceLost = { serviceInfo ->
            val updated = HashMap<String, FoundStall>(_discoveredServices.value.peers)
            updated.remove(serviceInfo.serviceName)
            _discoveredServices.value = _discoveredServices.value.copy(
                peers = updated
            )

            requestClearOfResolveService(serviceInfo)
        },
        requestStopServiceDiscovery,
        { isDiscoveryActive ->
            _discoveredServices.value = _discoveredServices.value.copy(
                isActive = isDiscoveryActive
            )


            resolveRequestFulfillment = if (isDiscoveryActive) {
                resolveServicePeerListener.startFulfillingResolveRequests()
            } else {
                resolveRequestFulfillment?.cancel()
                null
            }
        }
    )


    init {

    }

    private fun getRegisteredServiceName(): String {
        return P2pStallServiceHelper.getServiceName(
            _currentPeerId.paymail,
            _currentPeerId.pki,
            _currentPeerId.title
        )
    }

    override suspend fun beginAdvertisingServiceOfLocalPeer(port: Int, id: P2pStallManager.PeerServiceId) {
        if (!Patterns.EMAIL_ADDRESS.matcher(id.paymail).matches()) throw RuntimeException()
        //todo validate the pki

        _currentPeerId = id
        _advertisingStatus.value = AdvertisingStatus.PREPARING

        val serviceInfo = NsdServiceInfo().apply {
            // The name is subject to change based on conflicts
            // with other services advertised on the same network.
            serviceName = getRegisteredServiceName()
            serviceType = SERVICE_TYPE
            setPort(port)

            setAttribute(KEY_TITLE, id.title)
            setAttribute(KEY_PKI, id.pki)
            setAttribute(KEY_PAYMAIL, id.paymail)
        }

        nsdManager.registerService(
            serviceInfo,
            NsdManager.PROTOCOL_DNS_SD,
            peerRegistrationListener
        )
    }

    override suspend fun stopAdvertisingServiceOfLocalPeer() {
        if (AdvertisingStatus.UNREGISTERED == advertisingStatus.value) {
            return
        }

        suspendCancellableCoroutine<Unit> { continuation ->
            peerRegistrationListener.addContinuationToResumeOnUnregistered(continuation)
            try {
                nsdManager.unregisterService(peerRegistrationListener)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }

        _advertisingStatus.value = AdvertisingStatus.UNREGISTERED

        considerTearDown()
    }



    override fun beginDiscovery(id: P2pStallManager.PeerServiceId) {
        _currentPeerId = id
        nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, peerDiscoveryListener)
    }

    override fun stopDiscovery() {
        if (!_discoveredServices.value.isActive) {
            return
        }

        Timber.d("....#stopDiscovery")

        scope.launch {
            suspendCancellableCoroutine<Unit> { continuation ->
                val job = scope.launch {
                    val thisJob = coroutineContext[Job]!!

                    _discoveredServices.collect { model ->
                        if (!model.isActive) {
                            continuation.resume(Unit)
                            _discoveredServices.value = _discoveredServices.value.copy(
                                isActive = false,
                                peers = emptyMap()
                            )

                            thisJob.cancel()
                        }
                    }
                }

                try {
                    nsdManager.stopServiceDiscovery(peerDiscoveryListener)
                } catch (e: Exception) {
                    Timber.e(e)
                    job.cancel()
                    if (!continuation.isCompleted) continuation.resume(Unit)
                }
            }

            Timber.d("post of #stopDiscovery")
            considerTearDown()
        }
    }

    private fun considerTearDown() {
        if (!_discoveredServices.value.isActive &&
            AdvertisingStatus.UNREGISTERED == advertisingStatus.value) {
            tearDown()
        }
    }

    override fun tearDown() {
        scope.launch {
            stopAdvertisingServiceOfLocalPeer()
            stopDiscovery()
        }
    }





    private fun requestResolveService(serviceInfo: NsdServiceInfo) {
        resolveServicePeerListener.requestResolveService(serviceInfo)
    }

    private fun requestClearOfResolveService(serviceInfo: NsdServiceInfo) {
        resolveServicePeerListener.clearRequestedResolve(serviceInfo)
    }






    inner class ServiceRegistrationListener : NsdManager.RegistrationListener {

        var registeredName: String? = null

        override fun onServiceRegistered(nsdServiceInfo: NsdServiceInfo) {
            // Save the service name. Android may have changed it in order to
            // resolve a conflict, so update the name you initially requested
            // with the name Android actually used.
            registeredName = nsdServiceInfo.serviceName

            _advertisingStatus.value = AdvertisingStatus.REGISTERED
        }

        override fun onRegistrationFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
            // Registration failed! Put debugging code here to determine why.
            Timber.d("onRegistrationFailed: $errorCode")

//            _advertisingStatus.value = AdvertisingStatus.ERROR //todo currently undone by the call to onError
            tearDown()
        }

        override fun onServiceUnregistered(arg0: NsdServiceInfo) {
            _advertisingStatus.value = AdvertisingStatus.UNREGISTERED

            resumeUnregisteredWaitingContinuations()
        }

        override fun onUnregistrationFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
            // Unregistration failed. Put debugging code here to determine why.
            Timber.d("onUnregistrationFailed: ${errorCode}")

            //_advertisingStatus.value = AdvertisingStatus.ERROR

            resumeUnregisteredWaitingContinuations()
        }

        private fun resumeUnregisteredWaitingContinuations() {
            try {
                considerTearDown()
            } catch (e: Exception) {
                Timber.e(e)
            }

            val toBeResumed = ArrayList(toBeResumedOnUnregistered)
            toBeResumedOnUnregistered.clear()
            toBeResumed.forEach {
                it.cancel()
            }
        }


        val toBeResumedOnUnregistered = ArrayList<CancellableContinuation<Unit>>()

        fun addContinuationToResumeOnUnregistered(continuation: CancellableContinuation<Unit>) {
            toBeResumedOnUnregistered.add(continuation)
        }
    }





    class ServiceDiscoveryListener(
        val onPotentialServiceFound: (NsdServiceInfo)->Unit,
        val onDiscoveredServiceLost: (NsdServiceInfo)->Unit,
        val requestStopServiceDiscovery: ()->Unit,
        val onActiveStatusChanged: (Boolean)->Unit
    ) : NsdManager.DiscoveryListener {

        override fun onServiceFound(service: NsdServiceInfo) {
            // A service was found! Do something with it.
//            Timber.d("Service discovery success $service")
            when {
                !service.serviceType.contains(SERVICE_TYPE) -> // Service type is the string containing the protocol and
                    // transport layer for this service.
                    Timber.d("Unknown Service Type: ${service.serviceType}")
//                !P2pStallServiceHelper.isValidServiceName(service.serviceName) -> {
//                    Timber.d("Service dropped, the service-name does not adhere to the expected format")
//                }
//                service.serviceName.contains(SERVICE_NAME_PREFIX) -> onPotentialServiceFound(service)
//                else -> {
//                    Timber.d("...found service dropped - service.serviceName")
//                }
                else -> {
                    onPotentialServiceFound(service)
                }
            }
        }

        override fun onServiceLost(service: NsdServiceInfo) {
            // When the network service is no longer available.
            // Internal bookkeeping code goes here.
            Timber.e("service lost: $service")
            onDiscoveredServiceLost(service)
        }


        // Called as soon as service discovery begins.
        override fun onDiscoveryStarted(regType: String) {
            Timber.d("Service discovery started.... regType:$regType")
            onActiveStatusChanged(true)
        }

        override fun onDiscoveryStopped(serviceType: String) {
            Timber.d("Discovery stopped: $serviceType")
            onActiveStatusChanged(false)
        }

        override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
            Timber.e("Discovery failed: Error code:$errorCode")
            onActiveStatusChanged(false)
            requestStopServiceDiscovery()
        }

        override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
            Timber.e("Discovery failed: Error code:$errorCode")
            onActiveStatusChanged(false)
            requestStopServiceDiscovery()
        }
    }

    inner class ResolveServicePeerListener : NsdManager.ResolveListener {

        val inProgressResolveRequests = HashSet<String>()
        var inProgressResolveContinuation: Continuation<Unit>? = null
        val resolveRequestChannel = Channel<NsdServiceInfo>()

        override fun onResolveFailed(serviceInfo: NsdServiceInfo?, errorCode: Int) {
            // Called when the resolve fails. Use the error code to debug.
            Timber.e("Resolve failed: $errorCode")

            serviceInfo?.serviceName?.let {
                inProgressResolveRequests.remove(it)
            }
        }

        override fun onServiceResolved(serviceInfo: NsdServiceInfo?) {
//            Timber.e("Resolve Succeeded. $serviceInfo")


            serviceInfo ?: let {
                inProgressResolveContinuation?.resume(Unit)
                return
            }



            if (!inProgressResolveRequests.contains(serviceInfo.serviceName)) {
                Timber.d("resolve-request has been dropped; skipping parsing of successful resolve")
                return
            }

            serviceInfo.serviceName?.let {
                inProgressResolveRequests.remove(it)
            }

            val gotStall = FoundStall.create(serviceInfo) ?: let {
                Timber.d("service dropped because it did not conform to the expected protocol")
                return
            }

            val updated = HashMap<String, FoundStall>(_discoveredServices.value.peers)
            updated[serviceInfo.serviceName] = gotStall
            _discoveredServices.value = _discoveredServices.value.copy(
                peers = updated
            )


            inProgressResolveContinuation?.resume(Unit)
        }




        fun requestResolveService(serviceInfo: NsdServiceInfo) {
            val current = resolveServicePeerListener.inProgressResolveRequests
            if (current.contains(serviceInfo.serviceName)) {
//                Timber.d("dropped a request to resolve-service; already in progress")
                return
            }

//            Timber.d("Attempting to resolve service: ${serviceInfo.serviceName}")
            resolveServicePeerListener.inProgressResolveRequests.add(serviceInfo.serviceName)


            resolveRequestChannel.trySend(serviceInfo)
        }

        fun clearRequestedResolve(serviceInfo: NsdServiceInfo) {
            resolveServicePeerListener.inProgressResolveRequests.remove(serviceInfo.serviceName)
        }


        fun startFulfillingResolveRequests(): Job {
            return scope.launch {
                resolveRequestChannel.receiveAsFlow().collect { service ->
                    if (!resolveServicePeerListener.inProgressResolveRequests
                            .contains(service.serviceName)) {
                        Timber.d("resolve-request has been dropped; skipping")
                        return@collect
                    }

                    suspendCancellableCoroutine<Unit> { continuation ->
                        inProgressResolveContinuation = continuation

                        nsdManager.resolveService(service, this@ResolveServicePeerListener)
                    }

                    Timber.d("post fulfilled")

                    resolveServicePeerListener.inProgressResolveRequests.remove(service.serviceName)
                }
            }
        }

    }


}


//todo the name had to be simplified because some mDNS libraries get broken with non-hex name-values
object P2pStallServiceHelper {
    fun getServiceName(
        paymail: String,
        pki: String,
        title: String
    ): String {
        return HashUtils.sha256(paymail + pki + title)
    }

    fun isValidServiceName(name: String): Boolean {
        return true //todo
//        val parts = name.split(".")
//        if (parts.size < 2) return false
//        if (P2pStallRegistryHelper.SERVICE_NAME_PREFIX != parts[0]) return false
//
//        return true
    }
}