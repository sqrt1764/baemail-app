package app.local.p2p.serviceDiscovery

import android.os.HandlerThread

object WorkThread {

    const val NAME = "AppHandlerThread"

    val thread : HandlerThread by lazy {
        HandlerThread(NAME).let {
            it.start()

            it
        }
    }



}