package app.local.p2p.serviceDiscovery

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import timber.log.Timber

class DelayedReleaseHelper(
    val activeRequestedFlow: StateFlow<Boolean>,
    val lifecycleFlow: StateFlow<Boolean>,
    val scope: CoroutineScope,
    val delay: Long,
    val startNow: suspend ()->Unit,
    val stopNow: suspend ()->Unit
) {

    private var killJob: Job? = null

    private var isStarted = false


    init {

        scope.launch {
            activeRequestedFlow.combine(lifecycleFlow) { activeRequested, lifecycle ->
                activeRequested to lifecycle
            }.collect { (activeRequested, lifecycle) ->
                when {
                    !activeRequested -> {
                        //must stop NOW!!!!
                        if (!isStarted) return@collect
                        cancelScheduledRelease()
                        isStarted = false
                        stopNow()
                    }
                    activeRequested && lifecycle -> {
                        //must start NOW!!!
                        cancelScheduledRelease()
                        if (isStarted) return@collect
                        isStarted = true
                        startNow()
                    }
                    activeRequested && !lifecycle -> {
                        //must schedule disconnect
                        if (isReleaseScheduled()) return@collect
                        scheduleRelease()
                    }
                }

            }
        }



    }




    private fun isReleaseScheduled(): Boolean {
        return killJob != null
    }

    private fun cancelScheduledRelease() {
        Timber.d("cancelScheduledRelease")
        killJob?.cancel()
        killJob = null
    }

    private fun scheduleRelease() {
        Timber.d("scheduleRelease")
        killJob ?: let {
            killJob = scope.launch {
                Timber.d("scheduleRelease kill-job launched")
                delay(delay)
                if (!isActive) {
                    Timber.d("`killJob` job has been cancelled")
                    return@launch
                }

                Timber.d("scheduleRelease kill-job ... time passed, stopping")
                isStarted = false
                stopNow()
                killJob = null
            }
        }
    }



}