package app.local.p2p.serviceDiscovery

import android.content.Context
import android.content.Intent
import android.os.Build
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import timber.log.Timber
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.random.Random

class P2pStallManager private constructor()  {

    private var port: Int = -1

    private val _stallRunningState = MutableStateFlow(Model(State.OFF))
    val stallRunningState: StateFlow<Model>
        get() = _stallRunningState


    lateinit var serviceRegistryHelper: P2pStallRegistry

    private lateinit var appContext: Context
    private lateinit var scope: CoroutineScope
    private lateinit var goToManageStallIntent: Intent
    private lateinit var currentPeerFlow: StateFlow<PeerServiceId>

    private var serviceRegistryObservationJob: Job? = null


    private val stopAdvertisingAwaitingCoroutines = mutableListOf<Continuation<Unit>>()


    private var doStartServer: suspend (Int, PeerServiceId)->Boolean = { port, peerId ->
        Timber.d("placeholder of #startServer")
        true
    }
    private var doStopServer: suspend ()->Boolean = {
        Timber.d("placeholder of #startServer")
        true
    }

    private fun _setup(
        appContext: Context,
        scope: CoroutineScope,
        goToManageStallIntent: Intent,
        currentPeerFlow: StateFlow<PeerServiceId>,
        registryHelper: P2pStallRegistry,
        startServer: suspend (Int, PeerServiceId)->Boolean,
        stopServer: suspend ()->Boolean
    ) {
        this.appContext = appContext
        this.scope = scope
        this.goToManageStallIntent = goToManageStallIntent
        this.currentPeerFlow = currentPeerFlow
        this.serviceRegistryHelper = registryHelper
        this.doStartServer = startServer
        this.doStopServer = stopServer

    }

    fun requestStartAdvertisingService() {
        Timber.d("#requestStartAdvertisingService")
        startStallService()
    }

    fun stopAdvertisingService() {
        Timber.d("#stopAdvertisingService")
        clearServiceRegistryObservation()
        appContext.stopService(P2pStallService.getStartIntent(appContext))
        //the cleanup completes when the service is destroyed
    }



    private fun resumeCoroutinesAwaitingStopAdvertising() {
        val toResume = ArrayList(stopAdvertisingAwaitingCoroutines)
        stopAdvertisingAwaitingCoroutines.clear()
        toResume.forEach { it.resume(Unit) }
    }

    suspend fun stopAdvertisingServiceAndAwait() {
        if (State.OFF == _stallRunningState.value.state) return

        suspendCancellableCoroutine<Unit> { continuation ->
            stopAdvertisingService()
            stopAdvertisingAwaitingCoroutines.add(continuation)
        }
    }

    fun getGoToManageStallIntent(): Intent {
        return goToManageStallIntent
    }






    private fun _onServiceCreated() {
        Timber.d("#_onServiceCreated")

        val serviceRegistryJob = scope.launch {


            val success = startServer()

            if (!success) {

                //cleanup everything & exit
                Timber.d("stall-server failed to start; cleaning everything up")

                port = -1

                //stop the service
                appContext.stopService(P2pStallService.getStartIntent(appContext))
                //the cleanup completes when the service is destroyed

                return@launch
            }



            //register the service
            try {
                serviceRegistryHelper.beginAdvertisingServiceOfLocalPeer(port, currentPeerFlow.value)
                setupServiceAdvertisingObservation()
                Timber.d("service has been registered")


                _stallRunningState.value = _stallRunningState.value.copy(state = State.RUNNING)

            } catch (e: Exception) {
                Timber.e(e)
                //stop the service
                appContext.stopService(P2pStallService.getStartIntent(appContext))
                //the cleanup completes when the service is destroyed
            }

        }
    }

    private fun _onServiceDestroyed() {
        Timber.d("#_onServiceDestroyed")


        scope.launch {
            launch {
                serviceRegistryHelper.stopAdvertisingServiceOfLocalPeer()
            }
            launch {
                doStopServer()
            }
            clearServiceRegistryObservation()


            Timber.d(".....#_onServiceDestroyed")
            _stallRunningState.value = _stallRunningState.value.copy(state = State.OFF)
            resumeCoroutinesAwaitingStopAdvertising()
        }
    }



    private suspend fun setupServiceAdvertisingObservation() {
        serviceRegistryObservationJob = scope.launch {
            launch {
                currentPeerFlow.collect { id ->
                    if (serviceRegistryHelper.currentPeerId == id) {
                        Timber.d("...current peer is unchanged")
                        return@collect
                    }
                    if (AdvertisingStatus.REGISTERED != serviceRegistryHelper.advertisingStatus.value) {
                        Timber.d("...dropping processing of reaction to the change of current peer; unnecessary, stall-advertising was not active")
                        return@collect
                    }

                    Timber.d("#setupServiceAdvertisingObservation causing refreshing/restarting of p2p-advertised-service-entry $id")

                    //re-register with the new metadata
                    serviceRegistryHelper.stopAdvertisingServiceOfLocalPeer()
                    serviceRegistryHelper.beginAdvertisingServiceOfLocalPeer(port, id)
                }
            }

            launch {
                serviceRegistryHelper.advertisingStatus.collect { model ->
                    Timber.d("serviceRegistryHelper.status.collect: $model")
                    if (AdvertisingStatus.ERROR == model) {
                        //stop the service
                        appContext.stopService(P2pStallService.getStartIntent(appContext))
                        //the cleanup completes when the service is destroyed
                    }
                }
            }
        }
    }

    private fun clearServiceRegistryObservation() {
        serviceRegistryObservationJob?.cancel()
        serviceRegistryObservationJob = null
    }



    private fun startStallService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            appContext.startForegroundService(P2pStallService.getStartIntent(appContext))
        } else {
            appContext.startService(P2pStallService.getStartIntent(appContext))
        }

        _stallRunningState.value = _stallRunningState.value.copy(state = State.STARTING)
    }

    private suspend fun startServer(): Boolean {
        Timber.d("#startServer")
        val portToUse = Random.nextInt(9999, 13337)
        val peerId = currentPeerFlow.value

        val result = doStartServer(portToUse, peerId)
        if (result) {
            port = portToUse
        }

        return result
    }






    data class PeerServiceId(
        val pki: String,
        val paymail: String,
        val title: String
    )

    data class Model(
        val state: State
    )

    enum class State {
        OFF,
        STARTING,
        RUNNING
    }

    companion object {

        private val instance: P2pStallManager by lazy {
            P2pStallManager()
        }

        fun setup(
            appContext: Context,
            scope: CoroutineScope,
            goToManageStallIntent: Intent,
            currentPeerFlow: StateFlow<PeerServiceId>,
            registryHelper: P2pStallRegistry,
            startServer: suspend (Int, PeerServiceId)->Boolean,
            stopServer: suspend ()->Boolean
        ): P2pStallManager {
            instance._setup(
                appContext,
                scope,
                goToManageStallIntent,
                currentPeerFlow,
                registryHelper,
                startServer,
                stopServer
            )

            return get()
        }

        fun get(): P2pStallManager {
            return instance
        }

        fun onServiceCreated() {
            instance._onServiceCreated()
        }

        fun onServiceDestroyed() {
            instance._onServiceDestroyed()
        }
    }
}