package app.local.p2p.serviceDiscovery

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch

class P2pStallRegistryManager(
    private val nearbyHelper: P2pStallRegistryHelper4,
    private val nsdHelper: P2pStallRegistryHelper0,
    private val wifiDirectHelper: P2pStallRegistryHelper1,
    private val scope: CoroutineScope
) : P2pStallRegistry {

    private var _type: ConnectionType = ConnectionType.NEARBY
    val type: ConnectionType
        get() { return _type }


    private var _supportedStatus = MutableStateFlow(false)
    override val supportedStatus: StateFlow<Boolean>
        get() = _supportedStatus

    private val _advertisingStatus = MutableStateFlow(AdvertisingStatus.UNREGISTERED)
    override val advertisingStatus: StateFlow<AdvertisingStatus>
        get() = _advertisingStatus





    private val _discoveredServices = MutableStateFlow(
        DiscoveryModel(
            false,
            mapOf()
        )
    )
    override val discoveredServices: StateFlow<DiscoveryModel>
        get() = _discoveredServices



    override val currentPeerId: P2pStallManager.PeerServiceId
        get() {
            return when (_type) {
                ConnectionType.NEARBY -> nearbyHelper.currentPeerId
                ConnectionType.NSD -> nsdHelper.currentPeerId
                else -> wifiDirectHelper.currentPeerId
            }
        }

    private var stateFlowObservationJob: Job? = null

    init {
//        initStateFlows()
    }


    private fun initStateFlows() {
        stateFlowObservationJob?.cancel()
        stateFlowObservationJob = scope.launch {
            launch {
                nearbyHelper.advertisingStatus.combine(nsdHelper.advertisingStatus) { p0, p1 ->
                    p0 to p1
                }.combine(wifiDirectHelper.advertisingStatus) { (p0, p1), p2 ->
                    when (_type) {
                        ConnectionType.NEARBY -> p0
                        ConnectionType.NSD -> p1
                        ConnectionType.WIFI_DIRECT -> p2
                    }
                }.collect { status ->
                    _advertisingStatus.value = status
                }
            }

            launch {
                nearbyHelper.discoveredServices.combine(nsdHelper.discoveredServices) { p0, p1 ->
                    p0 to p1
                }.combine(wifiDirectHelper.discoveredServices) { (p0, p1), p2 ->
                    when (_type) {
                        ConnectionType.NEARBY -> p0
                        ConnectionType.NSD -> p1
                        ConnectionType.WIFI_DIRECT -> p2
                    }
                }.collect { discoveryModel ->
                    _discoveredServices.value = discoveryModel
                }
            }

            launch {
                nearbyHelper.supportedStatus.combine(nsdHelper.supportedStatus) { p0, p1 ->
                    p0 to p1
                }.combine(wifiDirectHelper.supportedStatus) { (p0, p1), p2 ->
                    when (_type) {
                        ConnectionType.NEARBY -> p0
                        ConnectionType.NSD -> p1
                        ConnectionType.WIFI_DIRECT -> p2
                    }
                }.collect { supportedStatus ->
                    _supportedStatus.value = supportedStatus
                }
            }
        }
    }



    override fun tearDown() {
        when (_type) {
            ConnectionType.NEARBY -> nearbyHelper.tearDown()
            ConnectionType.NSD -> nsdHelper.tearDown()
            ConnectionType.WIFI_DIRECT -> wifiDirectHelper.tearDown()
        }
    }

    override suspend fun beginAdvertisingServiceOfLocalPeer(port: Int, id: P2pStallManager.PeerServiceId) {
        when (_type) {
            ConnectionType.NEARBY -> nearbyHelper.beginAdvertisingServiceOfLocalPeer(port, id)
            ConnectionType.NSD -> nsdHelper.beginAdvertisingServiceOfLocalPeer(port, id)
            ConnectionType.WIFI_DIRECT -> wifiDirectHelper.beginAdvertisingServiceOfLocalPeer(port, id)
        }
    }

    override suspend fun stopAdvertisingServiceOfLocalPeer() {
        when (_type) {
            ConnectionType.NEARBY -> nearbyHelper.stopAdvertisingServiceOfLocalPeer()
            ConnectionType.NSD -> nsdHelper.stopAdvertisingServiceOfLocalPeer()
            ConnectionType.WIFI_DIRECT -> wifiDirectHelper.stopAdvertisingServiceOfLocalPeer()
        }
    }

    override fun beginDiscovery(id: P2pStallManager.PeerServiceId) {
        when (_type) {
            ConnectionType.NEARBY -> nearbyHelper.beginDiscovery(id)
            ConnectionType.NSD -> nsdHelper.beginDiscovery(id)
            ConnectionType.WIFI_DIRECT -> wifiDirectHelper.beginDiscovery(id)
        }
    }

    override fun stopDiscovery() {
        when (_type) {
            ConnectionType.NEARBY -> nearbyHelper.stopDiscovery()
            ConnectionType.NSD -> nsdHelper.stopDiscovery()
            ConnectionType.WIFI_DIRECT -> wifiDirectHelper.stopDiscovery()
        }
    }

    fun setConnectionType(type: ConnectionType) {
        stateFlowObservationJob?.let { //it has been initialized for one of the types
            if (_type == type) return //unchanged
        }

        //crash if this is called while advertising/discovering is active
        if (AdvertisingStatus.UNREGISTERED != advertisingStatus.value) throw Exception()
        if (discoveredServices.value.isActive) throw Exception()

        this._type = type
        initStateFlows()
    }
}