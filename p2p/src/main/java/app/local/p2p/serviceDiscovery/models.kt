package app.local.p2p.serviceDiscovery

import android.net.nsd.NsdServiceInfo




data class FoundStall(
    val name: String,
    val host: String?,
    val port: Int,
    val title: String,
    val pki: String,
    val paymail: String
) {

    companion object {
        fun create(serviceInfo: NsdServiceInfo): FoundStall {
            val attributes = serviceInfo.attributes

            val title = attributes[P2pStallRegistryHelper0.KEY_TITLE]?.let {
                String(it)
            } ?: ""

            val pki = attributes[P2pStallRegistryHelper0.KEY_PKI]?.let {
                String(it)
            } ?: ""

            val paymail = attributes[P2pStallRegistryHelper0.KEY_PAYMAIL]?.let {
                String(it)
            } ?: ""

            return FoundStall(
                serviceInfo.serviceName,
                serviceInfo.host?.hostAddress,
                serviceInfo.port,
                title,
                pki,
                paymail
            )
        }
    }
}


enum class AdvertisingStatus {
    UNREGISTERED,
    PREPARING,
    REGISTERED,
    ERROR //todo seems like this should ever be set if the feature cannot be initialized due to device incompatibility
}


data class DiscoveryModel(
    val isActive: Boolean,
    val peers: Map<String, FoundStall>
)


enum class ConnectionType(
    val const: String
) {
    NEARBY("nearby"),
    WIFI_DIRECT("wifidirect"),
    NSD("wifi"); //todo rename the const to NSD

    companion object {
        fun parse(const: String): ConnectionType {
            return values().find { it.const == const }!!
        }
    }
}




