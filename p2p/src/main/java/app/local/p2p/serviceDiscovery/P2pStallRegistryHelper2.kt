package app.local.p2p.serviceDiscovery

import android.annotation.SuppressLint
import android.content.Context
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.os.Handler
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.UnknownHostException
import java.util.*
import javax.jmdns.*
import kotlin.collections.ArrayList
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import javax.jmdns.JmDNS




class P2pStallRegistryHelper2(
    var context: Context
) {

    companion object {
        const val SERVICE_TYPE = "${P2pStallRegistryHelper0.SERVICE_TYPE}.local."
    }

    val workHandler = Handler(WorkThread.thread.looper)


    //todo inject
    //todo dispatcher
    private val scope: CoroutineScope = CoroutineScope(
        SupervisorJob()
                + Dispatchers.Default
                + CoroutineExceptionHandler { _, throwable -> Timber.e(throwable) }
    )

    private val _advertisingStatus = MutableStateFlow(AdvertisingStatus.UNREGISTERED)
    val advertisingStatus: StateFlow<AdvertisingStatus>
        get() = _advertisingStatus






    private val _discoveredServices = MutableStateFlow(
        DiscoveryModel(
            false,
            mapOf()
        )
    )
    val discoveredServices: StateFlow<DiscoveryModel>
        get() = _discoveredServices





    private var _currentPeerId = P2pStallManager.PeerServiceId(
        "INVALID_UNSET",
        "INVALID_UNSET",
        "INVALID_UNSET"
    )
    val currentPeerId: P2pStallManager.PeerServiceId
        get() = _currentPeerId




    private var activeDiscoveryJob: Job? = null

    private val wifi: WifiManager by lazy {
        context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
    }



    private var multicastLock: WifiManager.MulticastLock? = null

    private var jmdns: JmDNS? = null



    private var isInitializing = false
    private var isWifiP2pEnabled = false
    private val p2pEnabledWaitingContinuations = ArrayList<Continuation<Unit>>()




    init {
        //todo
        //todo
        //todo
        //todo
    }






    fun setup() {
        if (multicastLock != null) {
            Timber.d("avoided repeated setup")
            return
        }


        workHandler.post {
            try {
                val deviceIpAddress: InetAddress = getDeviceIpAddress(wifi)

                wifi.createMulticastLock("bitcoin-p2p-lock").let { lock ->
                    multicastLock = lock
                    lock.setReferenceCounted(true)
                    lock.acquire()
                    Timber.i("Starting initializing p2p-lib....")
                    JmDNS.create(deviceIpAddress, "stallmdns").let {
                        jmdns = it
                        it.addServiceTypeListener(serviceTypeListener)
                    }
                    Timber.i("....Finished initializing p2p-lib")

                    ArrayList(p2pEnabledWaitingContinuations).let { toResume ->
                        p2pEnabledWaitingContinuations.clear()
                        toResume.forEach { continuation ->
                            continuation.resume(Unit)
                        }
                    }

                    isWifiP2pEnabled = true
                }
            } catch (e: Exception) {
                Timber.e(e)
                onError(e)
            }
        }


    }




    private fun getRegisteredServiceName(): String {
        return P2pStallServiceHelper.getServiceName(
            _currentPeerId.paymail,
            _currentPeerId.pki,
            _currentPeerId.title
        ).let {
            it.substring(0, 6) + it.substring(it.length - 6)
        }
    }

    private fun getDeviceIpAddress(wifi: WifiManager): InetAddress {
        var result: InetAddress? = null
        try {
            // default to Android localhost
            result = getIPAddress(true)!!.let{ ip ->
                InetAddress.getByName(ip)
            }

            // figure out our wifi address, otherwise bail
            val wifiinfo: WifiInfo = wifi.connectionInfo
            val intaddr: Int = wifiinfo.getIpAddress()
            val byteaddr = byteArrayOf(
                (intaddr and 0xff).toByte(), (intaddr shr 8 and 0xff).toByte(),
                (intaddr shr 16 and 0xff).toByte(), (intaddr shr 24 and 0xff).toByte()
            )
            result = InetAddress.getByAddress(byteaddr)
        } catch (ex: UnknownHostException) {
            Timber.d("getDeviceIpAddress Error: ${ex.message}")
            throw ex
        }
        return result
    }

    /**
     * Get IP address from first non-localhost interface
     * @param useIPv4   true=return ipv4, false=return ipv6
     * @return  address or empty string
     */
    fun getIPAddress(useIPv4: Boolean): String? {
        val interfaces: List<NetworkInterface> =
            Collections.list(NetworkInterface.getNetworkInterfaces())

        for (intf in interfaces) {
            Timber.d("....intf: $intf")
            val addrs: List<InetAddress> = Collections.list(intf.inetAddresses)
            for (addr in addrs) {
                Timber.d("....addr: $addr isLoopback:${addr.isLoopbackAddress} hostAddress:${addr.hostAddress}")
                if (!addr.isLoopbackAddress) {
                    val sAddr = addr.hostAddress
                    //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                    val isIPv4 = sAddr.indexOf(':') < 0
                    if (useIPv4) {
                        if (isIPv4) return sAddr
                    } else {
                        if (!isIPv4) {
                            val delim = sAddr.indexOf('%') // drop ip6 zone suffix
                            return if (delim < 0) sAddr.uppercase() else sAddr.substring(
                                0,
                                delim
                            ).uppercase()
                        }
                    }
                }
            }
        }

        //address not found
        throw Exception()
    }

    //todo call this when both advertising & discover go down
    fun tearDown() {
        workHandler.post {
            try {

                cleanupRegistry()
                stopDiscovery()

                jmdns?.let {
                    jmdns = null
                    it.unregisterAllServices()
                    it.close()
                }



                multicastLock?.let {
                    multicastLock = null
                    it.release()
                }

                Timber.d("p2p discovery/advertising stuff has been released")


                isInitializing = false
                isWifiP2pEnabled = false


            } catch (e: Exception) {
                Timber.e(e)
            }

        }
    }


    fun onError(throwable: Throwable) {
        Timber.e(throwable)

        tearDown()
    }


    @SuppressLint("MissingPermission")
    suspend fun registerServiceForLocalPeer(port: Int, id: P2pStallManager.PeerServiceId) {
        Timber.d(".....#registerServiceForLocalPeer $id")

        _currentPeerId = id
        _advertisingStatus.value = AdvertisingStatus.PREPARING

        val jmdns = ensureConnection()

        suspendCancellableCoroutine<Unit> { continuation ->
            workHandler.post {
                try {

                    val registeredName = getRegisteredServiceName()
                    Timber.d("registeredName: $registeredName")

                    val serviceInfo: ServiceInfo = ServiceInfo.create(
                        SERVICE_TYPE,
                        registeredName,
                        port,
                        0,
                        0,
                        mapOf(
                            P2pStallRegistryHelper0.KEY_TITLE to id.title,
                            P2pStallRegistryHelper0.KEY_PKI to id.pki,
                            P2pStallRegistryHelper0.KEY_PAYMAIL to id.paymail,
                        )
                    )

                    jmdns.registerService(serviceInfo)

                    Timber.d(".....#registerServiceForLocalPeer SUCCESS")

                    _advertisingStatus.value = AdvertisingStatus.REGISTERED
                    continuation.resume(Unit)

                } catch (e: Exception) {
                    Timber.e(e)
                    _advertisingStatus.value = AdvertisingStatus.ERROR
                    onError(e)
                    continuation.cancel(e)
                }
            }
        }

    }


    suspend fun unregisterServiceOfLocalPeer() {
        suspendCancellableCoroutine<Unit> { continuation ->

            workHandler.post {
                try {
                    jmdns?.unregisterAllServices()
                    _advertisingStatus.value = AdvertisingStatus.UNREGISTERED
                    continuation.resume(Unit)

                } catch (e: Exception) {
                    Timber.e(e)
                    onError(e)
                    continuation.cancel(e)
                }
            }
        }

    }

    fun cleanupRegistry() {
        workHandler.post {
            // Unregister all services
            jmdns?.unregisterAllServices()

            _advertisingStatus.value = AdvertisingStatus.UNREGISTERED
        }
    }


    fun stopDiscovery() {
        Timber.d("....#stopDiscovery")
        activeDiscoveryJob?.cancel()
        activeDiscoveryJob = null


        workHandler.post {
            jmdns?.removeServiceListener(SERVICE_TYPE, serviceListener)

            _discoveredServices.value = _discoveredServices.value.copy(
                isActive = false,
                peers = emptyMap()
            )
        }
    }



    @SuppressLint("MissingPermission")
    suspend fun beginDiscovery() {
        Timber.d("....#beginDiscovery")
        activeDiscoveryJob?.cancel()
        activeDiscoveryJob = scope.launch {

            val jmDns = ensureConnection()

            suspendCancellableCoroutine<Unit> { continuation ->
                workHandler.post {
                    jmDns.addServiceListener(SERVICE_TYPE, serviceListener)

                    _discoveredServices.value = _discoveredServices.value.copy(
                        isActive = true
                    )

                    continuation.resume(Unit)
                }
            }


        }

    }



    private suspend fun ensureConnection(): JmDNS {
        if (isWifiP2pEnabled) return jmdns!!
        if (isInitializing) {
            suspendCancellableCoroutine<Unit> { continuation ->
                p2pEnabledWaitingContinuations.add(continuation)
            }
            return jmdns!!
        }
        isInitializing = true

//        // Device capability definition check
//        if (!context.packageManager.hasSystemFeature(PackageManager.FEATURE_WIFI_DIRECT)) {
//            Timber.e("Wi-Fi Direct is not supported by this device.")
//            throw RuntimeException()
//        }

        // Hardware capability check
        val wifiManager: WifiManager? = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager?
        if (wifiManager == null) {
            Timber.e("Cannot get Wi-Fi system service.")
            throw RuntimeException()
        }
//        if (!wifiManager.isP2pSupported) {
//            Timber.e("Wi-Fi Direct is not supported by the hardware or Wi-Fi is off.")
//            throw RuntimeException()
//        }


        suspendCancellableCoroutine<Unit> { continuation ->
            p2pEnabledWaitingContinuations.add(continuation)
            setup()
        }

        return jmdns!!
    }

    val serviceListener = object : ServiceListener {

        fun parseStall(event: ServiceEvent): FoundStall? {
            try {
                val info = event.info

                val address = info.inetAddresses?.let { array ->
                    if (array.isEmpty()) return null
                    array[0]
                } ?: return null

                val iterator = info.propertyNames
                var paymail: String? = null
                var pki: String? = null
                var title: String? = null


                while (iterator.hasMoreElements()) {
                    try {
                        when (val key = iterator.nextElement()) {
                            P2pStallRegistryHelper0.KEY_TITLE -> {
                                title = info.getPropertyString(key)
                            }
                            P2pStallRegistryHelper0.KEY_PKI -> {
                                pki = info.getPropertyString(key)
                            }
                            P2pStallRegistryHelper0.KEY_PAYMAIL -> {
                                paymail = info.getPropertyString(key)
                            }
                        }
                    } catch (e: Exception) {
                        Timber.e(e)
                    }
                }

                if (paymail == null || pki == null || title == null) {
                    Timber.d("...resolved service dropped; failed to parse it")
                    return null
                }

                val finalAddress = address.toString().replace("/", "")

                return FoundStall(
                    event.name,
                    finalAddress,
                    info.port,
                    title,
                    pki,
                    paymail
                )

            } catch (e: Exception) {
                Timber.e(e)
                return null
            }
        }

        override fun serviceAdded(event: ServiceEvent?) {
            Timber.d(".... #serviceAdded: $event")
            event ?: return

            val gotStall = parseStall(event) ?: let {
                jmdns?.requestServiceInfo(event.type, event.name)
                return
            }
            Timber.d("#serviceAdded $gotStall")

            val updated = HashMap(_discoveredServices.value.peers)
            updated[event.name] = gotStall
            _discoveredServices.value = _discoveredServices.value.copy(
                peers = updated
            )
        }

        override fun serviceRemoved(event: ServiceEvent?) {
            Timber.d(".... #serviceRemoved: $event")
            event ?: return


            val updated = HashMap(_discoveredServices.value.peers)
            updated.remove(event.name)
            _discoveredServices.value = _discoveredServices.value.copy(
                peers = updated
            )
        }

        override fun serviceResolved(event: ServiceEvent?) {
            Timber.d(".... #serviceResolved: $event")
            event ?: return

            val gotStall = parseStall(event) ?: return

            val updated = HashMap(_discoveredServices.value.peers)
            updated[event.name] = gotStall
            _discoveredServices.value = _discoveredServices.value.copy(
                peers = updated
            )

        }

    }


    val serviceTypeListener = object : ServiceTypeListener {
        override fun serviceTypeAdded(event: ServiceEvent?) {
            Timber.d("mDNS.... #serviceTypeAdded: $event")
        }

        override fun subTypeForServiceTypeAdded(event: ServiceEvent?) {
            Timber.d("mDNS.... #subTypeForServiceTypeAdded: $event")
        }

    }


}