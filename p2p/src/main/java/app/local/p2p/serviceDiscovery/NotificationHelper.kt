package app.local.p2p.serviceDiscovery

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import app.local.p2p.R

class NotificationHelper(
    val appContext: Context
) {
    companion object {
        const val STALL_SERVICE_CHANNEL = "StallService"

        const val ID_NOTIFICATION_STALL_SERVICE = 222
    }

    init {
        //ensure the channel exists
        constructStallNotificationChannel(appContext)
    }

    private fun constructStallNotificationChannel(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = context.getString(R.string.stall_service_channel_name)
            val descriptionText = context.getString(R.string.stall_service_channel_description)
            val importance = NotificationManager.IMPORTANCE_LOW
            val channel = NotificationChannel(STALL_SERVICE_CHANNEL, name, importance)
            channel.description = descriptionText
            channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = context
                .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

    }


    fun buildNotification(contentIntent: Intent): Notification {
        //todo do not hard-code these
        val title = "Market Stall"
        val subtitle = "Hosting a stall; servicing nearby peers"
        val description = "BitcoinSV"
        val iconTint = Color.parseColor("#5075ee")

        return NotificationCompat.Builder(appContext, STALL_SERVICE_CHANNEL).apply {
            setContentTitle(title)
            setContentText(subtitle)
            setSubText(description)

//            val bitmapDrawable = ContextCompat.getDrawable(appContext, R.drawable.ic_wallet_36)!!
//                    as BitmapDrawable
//            setLargeIcon(bitmapDrawable.bitmap)


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                setContentIntent(
                    PendingIntent.getActivity(appContext, 0, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)
                )
            } else {
                setContentIntent(
                    PendingIntent.getActivity(appContext, 0, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                )
            }

//            setDeleteIntent(actionHolder.actionStopIntent)

            //make the transport controls visible on the lock-screen
            setVisibility(NotificationCompat.VISIBILITY_PUBLIC)

            //add an app icon and set its accent color
            //be careful about the color
            setSmallIcon(R.drawable.baseline_store_24)
            color = iconTint


            //todo add a button that stops the service
            val stopIntent = Intent(appContext, P2pStallService::class.java)
            stopIntent.data = Uri.parse("app://${P2pStallService.HOST_STOP_SELF}")
//            val stopIntent = Intent(Intent.ACTION_RUN, Uri.parse("app://stop_self"))
            addAction(
                NotificationCompat.Action(
                    R.drawable.baseline_cancel_24,
                    appContext.getString(R.string.stop),
                    PendingIntent.getService(appContext, 0, stopIntent, PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_IMMUTABLE)
                )
            )

        }.build()
    }
}