package app.local.p2p.serviceDiscovery;


import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class AddSuccessListener {
    public static Task<Void> invoke(Task<Void> task, Runnable callback) {
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                callback.run();
            }
        });
        return task;
    }
}
