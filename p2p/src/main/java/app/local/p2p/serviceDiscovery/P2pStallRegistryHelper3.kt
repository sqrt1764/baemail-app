package app.local.p2p.serviceDiscovery

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.wifi.aware.*
import android.os.Build
import android.os.Handler
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber
import kotlin.coroutines.resume

@SuppressLint("NewApi")
class P2pStallRegistryHelper3(
    var context: Context
)  {

    companion object {
        const val SERVICE_TYPE = "${P2pStallRegistryHelper0.SERVICE_TYPE}.local."

        const val MESSAGE_TYPE_INFO = 111
    }

    //todo inject
    //todo dispatcher
    private val scope: CoroutineScope = CoroutineScope(
        SupervisorJob()
                + Dispatchers.Default
                + CoroutineExceptionHandler { _, throwable -> Timber.e(throwable) }
    )


    private val _advertisingStatus = MutableStateFlow(AdvertisingStatus.UNREGISTERED)
    val advertisingStatus: StateFlow<AdvertisingStatus>
        get() = _advertisingStatus



    private val _discoveredServices = MutableStateFlow(
        DiscoveryModel(
            false,
            mapOf()
        )
    )
    val discoveredServices: StateFlow<DiscoveryModel>
        get() = _discoveredServices





    private var _currentPeerId = P2pStallManager.PeerServiceId(
        "INVALID_UNSET",
        "INVALID_UNSET",
        "INVALID_UNSET"
    )
    val currentPeerId: P2pStallManager.PeerServiceId
        get() = _currentPeerId




    private var activeDiscoveryJob: Job? = null

//    private val wifi: WifiManager by lazy {
//        context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
//    }

    val workHandler = Handler(WorkThread.thread.looper)

    val wifiAwareManager: WifiAwareManager by lazy {
        context.getSystemService(Context.WIFI_AWARE_SERVICE) as WifiAwareManager
    }




    private var isInitializing = false
    private var isWifiP2pEnabled = false
    private val p2pEnabledWaitingContinuations = ArrayList<CancellableContinuation<Unit>>()




    init {
        //todo
        //todo
        //todo
        //todo
    }





    @SuppressLint("NewApi")
    fun setup() {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
//            throw Exception("android version too old")
//        }


        //todo start observing for service-interrupts
        val filter = IntentFilter(WifiAwareManager.ACTION_WIFI_AWARE_STATE_CHANGED)
        val myReceiver = object : BroadcastReceiver() {


            override fun onReceive(context: Context, intent: Intent) {
                if (wifiAwareManager.isAvailable) {
                    //todo all cool for now
                } else {
                    //todo discard all existing sessions
                }
            }
        }
        context.registerReceiver(myReceiver, filter)


        wifiAwareManager.attach(object : AttachCallback() {
            override fun onAttached(session: WifiAwareSession?) {
                Timber.d("........onAttached")
                attachedSession = session

                ArrayList(p2pEnabledWaitingContinuations).let { toResume ->
                    p2pEnabledWaitingContinuations.clear()
                    toResume.forEach { continuation ->
                        continuation.resume(Unit)
                    }
                }

                isWifiP2pEnabled = true




            }

            override fun onAttachFailed() {
                onError(Exception())
            }
        }, workHandler)


    }

    var attachedSession: WifiAwareSession? = null


    private fun getRegisteredServiceName(): String {
        return P2pStallServiceHelper.getServiceName(
            _currentPeerId.paymail,
            _currentPeerId.pki,
            _currentPeerId.title
        ).let {
            it.substring(0, 6) + it.substring(it.length - 6)
        }
    }


//todo call this when both advertising & discover go down
    fun tearDown() {
        publishingSession?.close()
        attachedSession?.close()
        subscribeSession?.close()

        isInitializing = false
        isWifiP2pEnabled = false

        ArrayList(p2pEnabledWaitingContinuations).let { toResume ->
            p2pEnabledWaitingContinuations.clear()
            toResume.forEach { continuation ->
                continuation.cancel()
            }
        }
    }




    fun onError(throwable: Throwable) {
        Timber.e(throwable)

        tearDown()
    }



    private var publishingSession: PublishDiscoverySession? = null

    @SuppressLint("MissingPermission", "NewApi")
    suspend fun registerServiceForLocalPeer(port: Int, id: P2pStallManager.PeerServiceId) {
        Timber.d(".....#registerServiceForLocalPeer $id")


        ensureConnection()

        _currentPeerId = id
        _advertisingStatus.value = AdvertisingStatus.PREPARING

        val session = attachedSession ?: let {
            _advertisingStatus.value = AdvertisingStatus.ERROR
            onError(Exception())
            return
        }

        val config: PublishConfig = PublishConfig.Builder()
            .setServiceName("aware-bitcoin-stall")
            .setMatchFilter(listOf(
                "aware-bitcoin-stall".toByteArray()
            ))
            .build()

        suspendCancellableCoroutine<Unit> { continuation ->
            session.publish(config, object : DiscoverySessionCallback() {

                override fun onPublishStarted(session: PublishDiscoverySession) {
                    Timber.d("onPublishStarted")
                    publishingSession = session
                    _advertisingStatus.value = AdvertisingStatus.REGISTERED
                    continuation.resume(Unit)
                }

                override fun onMessageReceived(peerHandle: PeerHandle, message: ByteArray) {
                    //a device connected to this publication
                    //todo
                    //todo
                    //todo
                    //todo
                    publishingSession!!.sendMessage(peerHandle, MESSAGE_TYPE_INFO, "{}".toByteArray())
                    Timber.d("onMessageReceived message: ${String(message)}")
                }

                override fun onMessageSendSucceeded(messageId: Int) {
                    //todo
                    //todo
                    //todo
                    //todo
                }

                override fun onMessageSendFailed(messageId: Int) {
                    //todo
                    //todo
                    //todo
                    //todo
                }

            }, workHandler)
        }
    }



    suspend fun unregisterServiceOfLocalPeer() {
        publishingSession?.close()
        _advertisingStatus.value = AdvertisingStatus.UNREGISTERED
    }




    fun cleanupRegistry() {
        publishingSession?.close()
        _advertisingStatus.value = AdvertisingStatus.UNREGISTERED
    }



    var subscribeSession: SubscribeDiscoverySession? = null

    @SuppressLint("NewApi")
    fun beginDiscovery() {
        activeDiscoveryJob?.cancel()
        activeDiscoveryJob = scope.launch {
            ensureConnection()

            val config: SubscribeConfig = SubscribeConfig.Builder()
                .setServiceName("aware-bitcoin-stall")
                .build()
            attachedSession!!.subscribe(config, object : DiscoverySessionCallback() {

                override fun onSubscribeStarted(session: SubscribeDiscoverySession) {
                    subscribeSession = session

                    _discoveredServices.value = _discoveredServices.value.copy(
                        isActive = true
                    )
                }

                override fun onServiceDiscovered(
                    peerHandle: PeerHandle,
                    serviceSpecificInfo: ByteArray,
                    matchFilter: List<ByteArray>
                ) {
                    Timber.d("....... onServiceDiscovered .... info: $serviceSpecificInfo peerId:${peerHandle}")
                    //todo ...
                }
            }, null)


        }
    }

    fun stopDiscovery() {
        Timber.d("....#stopDiscovery")
        activeDiscoveryJob?.cancel()
        activeDiscoveryJob = null



        subscribeSession?.close()

        _discoveredServices.value = _discoveredServices.value.copy(
            isActive = false,
            peers = emptyMap()
        )
    }


private suspend fun ensureConnection() {
    if (isWifiP2pEnabled) return
    if (isInitializing) {
        suspendCancellableCoroutine<Unit> { continuation ->
            p2pEnabledWaitingContinuations.add(continuation)
        }
        return
    }
    isInitializing = true


    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
        throw Exception("android version too old")
    }

    if (!context.packageManager.hasSystemFeature(PackageManager.FEATURE_WIFI_AWARE)) {
        throw Exception("wifi aware is not supported")
    }

    if (!wifiAwareManager.isAvailable) {
        throw Exception("cannot use wifi-aware at this time")
    }



    suspendCancellableCoroutine<Unit> { continuation ->
        p2pEnabledWaitingContinuations.add(continuation)
        setup()
    }
}

}