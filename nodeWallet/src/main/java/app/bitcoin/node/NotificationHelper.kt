package app.bitcoin.node

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat

class NotificationHelper(
    val appContext: Context
) {
    companion object {
        const val NODE_SERVICE_CHANNEL = "NodeService"

        const val ID_NOTIFICATION_NODE_SERVICE = 111
    }

    init {
        //ensure the channel exists
        constructPlayerNotificationChannel(appContext)
    }

    private fun constructPlayerNotificationChannel(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = context.getString(R.string.node_service_channel_name)
            val descriptionText = context.getString(R.string.node_service_channel_description)
            val importance = NotificationManager.IMPORTANCE_LOW
            val channel = NotificationChannel(NODE_SERVICE_CHANNEL, name, importance)
            channel.description = descriptionText
            channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = context
                .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

    }


    fun buildNotification(): Notification {
        //todo do not hard-code these
        val title = "Wallet"
        val subtitle = "Working with transactions"
        val description = "Bitcoin"
        val iconTint = Color.parseColor("#5075ee")

        return NotificationCompat.Builder(appContext, NODE_SERVICE_CHANNEL).apply {
            setContentTitle(title)
            setContentText(subtitle)
            setSubText(description)

//            val bitmapDrawable = ContextCompat.getDrawable(appContext, R.drawable.ic_wallet_36)!!
//                    as BitmapDrawable
//            setLargeIcon(bitmapDrawable.bitmap)

//            setContentIntent(contentIntent)

//            setDeleteIntent(actionHolder.actionStopIntent)

            //make the transport controls visible on the lock-screen
            setVisibility(NotificationCompat.VISIBILITY_PUBLIC)

            //add an app icon and set its accent color
            //be careful about the color
            setSmallIcon(R.drawable.ic_wallet_36)
            color = iconTint

        }.build()
    }
}