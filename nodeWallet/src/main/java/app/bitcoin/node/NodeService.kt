package app.bitcoin.node

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.*
import android.util.Log
import androidx.core.os.postDelayed
import java.lang.RuntimeException

@SuppressLint("LogNotTimber")
class NodeService : Service() {

    private val notificationHelper: NotificationHelper by lazy {
        NotificationHelper(baseContext)
    }

    private val messageHandler = MessageHandler(
        { enterForeground() },
        { exitForeground() }
    )
    private val messenger = Messenger(messageHandler)

    private var node: Node? = null

    override fun onCreate() {
        Log.d(TAG, "onCreate")
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand")
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")

        node?.kill()
        node = null

        messageHandler.postDelayed(500) {
            Log.d(TAG, "killProcess running node-service component")
            Process.killProcess(Process.myPid())
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.d(TAG, "onBind")

        val mustStartInForeground = intent?.getBooleanExtra(KEY_START_FOREGROUND, false) ?: false

        if (mustStartInForeground) {
            enterForeground()
        }

        //auto-start
        node = Node.get(applicationContext) { log ->
            //todo this lambda gets called before #sendInfoToClients is ready (has at least 1 client)
            messageHandler.sendInfoToClients(log)
        }

        return messenger.binder
    }

    private fun enterForeground() {
        Log.d(TAG, "enterForeground")
        startForeground(
            NotificationHelper.ID_NOTIFICATION_NODE_SERVICE,
            notificationHelper.buildNotification()
        )
    }

    private fun exitForeground() {
        Log.d(TAG, "exitForeground")
        stopForeground(true)
    }


    class MessageHandler(
        val enterForeground: ()->Unit,
        val exitForeground: ()->Unit,
    ) : Handler(Looper.getMainLooper()) {

        //tracks components that have bound this service
        val clients = mutableListOf<Messenger>()

        override fun handleMessage(msg: Message) {
            when (msg.what) {
                REGISTER -> {
                    msg.replyTo?.let {
                        clients.add(it)

                        sendInfoToClients("....backwards messaging has been registered")
                    }
                }
                MESSAGE_ENTER_FOREGROUND -> {
                    enterForeground()
                }
                MESSAGE_EXIT_FOREGROUND -> {
                    exitForeground()
                }
                else -> {
                    Log.d(TAG, "unexpected message: ${msg.what}")
                    throw RuntimeException()
                }
            }
        }

        fun sendInfoToClients(info: String) {
            clients.forEach { messenger ->
                try {
                    val wrap = Bundle().also {
                        it.putString("info", info)
                    }

                    messenger.send(Message.obtain(null, INFO, wrap))

                } catch (e: Exception) {
                    Log.e(TAG, "client/messenger is dead", e)
                }

            }
        }
    }

    companion object {

        const val TAG = "NodeService"

        const val REGISTER = 1
        const val INFO = 2
        const val MESSAGE_ENTER_FOREGROUND = 11
        const val MESSAGE_EXIT_FOREGROUND = 12

        const val TAG_CPU_WAKE_LOCK = "foreground-service:wallet-nodejs"

        private const val KEY_START_FOREGROUND = "foreground"

        fun getStartIntent(
            context: Context,
            startForeground: Boolean
        ): Intent {


            return Intent(context, NodeService::class.java).also {
                it.putExtra(KEY_START_FOREGROUND, startForeground)
            }
        }
    }



}