package app.bitcoin.node

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.res.AssetManager
import android.util.Log
import kotlinx.coroutines.*
import org.json.JSONObject
import org.zeroturnaround.zip.ZipUtil
import java.io.*
import java.lang.Runnable


@SuppressLint("LogNotTimber")
class Node private constructor() {

    private var startedThread: Thread? = null

    lateinit var logger: (String)->Unit

    private fun start(context: Context) {
        val t = Thread(Runnable {
            logger("start of node-thread")
            //The path where we expect the node project to be at runtime.
            val nodeDir = context.filesDir.absolutePath + "/nodejs-project"
            val isVersionUpdated = isVersionUpdated(context)
            if (isVersionUpdated) {
                //Recursively delete any existing nodejs-project.
                val nodeDirReference = File(nodeDir)
                val existingFound = nodeDirReference.exists()

                if (existingFound) {
                    val now = System.currentTimeMillis()
                    val copyReference =
                        File(nodeDirReference.parent, "$now-${nodeDirReference.name}")
                    val renameResult = nodeDirReference.renameTo(copyReference)
                    if (!renameResult) throw RuntimeException()
                }

                //Copy the node project from assets into the application's data path.
                logger("starting copying")
                val pre = System.currentTimeMillis()
                val success = copyNodeProject(
                    context.assets,
                    nodeDir
                )
                val post = System.currentTimeMillis()
                logger("end of copyAssetFolder success:$success .. ${post - pre}ms")

                saveCurrentVersion(context)
                saveLastUpdateTime(context)

                logger("the node project has been moved to the app's sand-boxed directory")
            }

            considerDeleteOfOldProject(context)




            val configJson = JSONObject()
            configJson.put("isVersionUpdated", isVersionUpdated)
            configJson.put("version", getCurrentVersion(context))


            for (i in 0 until 5) {
                Log.d(NodeService.TAG, "invoking cpp-function #startNodeWithArguments ... attempt: $i")
                logger("invoking cpp-function #startNodeWithArguments ... attempt: $i")
                Log.d(NodeService.TAG, "node config-json: $configJson")
                logger("node config-json: $configJson")
                startNodeWithArguments(arrayOf("node", "$nodeDir/main.js", configJson.toString()))
                logger("post cpp-function #startNodeWithArguments")
            }
            throw RuntimeException()
        })

        logger("starting node-thread")
        t.start()

        startedThread = t
    }

    fun kill() {
        startedThread?.let {
            it.interrupt()
            startedThread = null
            obj = null
        }

        logger("thread killed")
    }

    external fun wireUpLogcat()

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun startNodeWithArguments(array: Array<String>): Int

    companion object {

        private var obj: Node? = null

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
            System.loadLibrary("node")
        }

        @Synchronized
        fun get(
            context: Context,
            logger: (String)->Unit = {
                Log.d(NodeService.TAG, "placeholder logger: $it")
            }
        ): Node = obj.let {
            if (it == null) {
                val n = Node()
                n.logger = logger

                if (isDebuggable(context)) {
                    n.wireUpLogcat()
                }

                n.start(context)

                obj = n
            }
            return obj!!
        }

        ///////////////////////

        fun isDebuggable(context: Context): Boolean {
            return (context.applicationInfo.flags
                    and ApplicationInfo.FLAG_DEBUGGABLE) != 0
        }

        ///////////////////////

        private fun considerDeleteOfOldProject(context: Context) {

            val projectParentDir = File(context.filesDir.absolutePath)
            val dirsToDelete = projectParentDir.listFiles()!!.filter {
                var isInt = false

                try {
                    it.name.substring(0, 4).toInt()
                    isInt = true

                } catch (e: Exception) {

                }

                isInt
            }

            if (dirsToDelete.isEmpty()) {
                Log.d(NodeService.TAG, "nothing needs deleting")
                return
            }

            dirsToDelete.forEach { dir ->
                GlobalScope.launch(Dispatchers.IO) {
                    delay(10 * 1000L)
                    Log.d(NodeService.TAG, "start of deleteFolderRecursively ${dir.path}")
                    val success = deleteFolderRecursively(dir)
                    Log.d(NodeService.TAG, "end of deleteFolderRecursively success:$success ${dir.path}")
                }
            }
        }

        ///////////////////////

        private suspend fun deleteFolderRecursively(file: File): Boolean {
            //Timber.d("deleteFolderRecursively $file")
            try {
                var res = true
                for (childFile in file.listFiles()!!) {
                    if (childFile.isDirectory()) {
                        res = res and deleteFolderRecursively(childFile)
                    } else {
                        res = res and childFile.delete()
                    }
                }
                res = res and file.delete()
                return res
            } catch (e: Exception) {
                Log.e(NodeService.TAG, "crash in #deleteFolderRecursively", e)
                return false
            }

        }

        private fun copyNodeProject(
            assetManager: AssetManager,
            toPath: String
        ): Boolean {

            var zipStream: InputStream? = null
            try {
                zipStream = assetManager.open("nodejs-project.zip")
                ZipUtil.unpack(zipStream, File(toPath))
            } catch (e: Exception) {
                Log.e(NodeService.TAG, "crash in #copyNodeProject", e)
                return false
            } finally {
                zipStream?.close()
            }
            return true
        }


        ////////////////

        private fun getCurrentVersion(context: Context): Int {
            return context.resources.getInteger(R.integer.nodejs_project__version)
        }

        private fun isVersionUpdated(context: Context): Boolean {

            val prefs = context.getSharedPreferences(
                "NODEJS_MOBILE_PREFS",
                Context.MODE_PRIVATE
            )
            val previousVersion = prefs.getInt("NODEJS_MOBILE_APK_version", 0)
            val currentVersion = getCurrentVersion(context)
            Log.d(NodeService.TAG, "isVersionUpdated; previous:$previousVersion current:$currentVersion")

            return currentVersion > previousVersion
        }

        private fun saveCurrentVersion(context: Context) {
            val currentVersion = getCurrentVersion(context)
            val prefs = context.getSharedPreferences(
                "NODEJS_MOBILE_PREFS",
                Context.MODE_PRIVATE
            )
            val editor = prefs.edit()
            editor.putInt("NODEJS_MOBILE_APK_version", currentVersion)
            editor.apply()
        }

        private fun wasAPKUpdated(context: Context): Boolean {
            val prefs = context.getSharedPreferences(
                "NODEJS_MOBILE_PREFS",
                Context.MODE_PRIVATE
            )
            val previousLastUpdateTime = prefs.getLong("NODEJS_MOBILE_APK_LastUpdateTime", 0)
            var lastUpdateTime: Long = 1
            try {
                val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                lastUpdateTime = packageInfo.lastUpdateTime
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            val wasUpdated = lastUpdateTime != previousLastUpdateTime
            Log.d(NodeService.TAG, "wasAPKUpdated; previous:$wasUpdated")

            return wasUpdated
        }

        private fun saveLastUpdateTime(context: Context) {
            var lastUpdateTime: Long = 1
            try {
                val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                lastUpdateTime = packageInfo.lastUpdateTime
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }

            val prefs = context.getSharedPreferences(
                "NODEJS_MOBILE_PREFS",
                Context.MODE_PRIVATE
            )
            val editor = prefs.edit()
            editor.putLong("NODEJS_MOBILE_APK_LastUpdateTime", lastUpdateTime)
            editor.apply()
        }





    }




}