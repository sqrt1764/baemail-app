package app.bitcoin.node

import android.content.Context
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.java_websocket.WebSocket
import org.java_websocket.framing.CloseFrame
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer
import timber.log.Timber
import java.io.File
import java.lang.RuntimeException
import java.net.BindException
import java.net.InetSocketAddress

// https://proandroiddev.com/security-best-practices-symmetric-encryption-with-aes-in-java-and-android-part-2-b3b80e99ad36

@Suppress("ConstantConditionIf")
class NodeCommsServer(
    private val appContext: Context,
    private val serverAddress: InetSocketAddress
) : WebSocketServer(serverAddress) {

    private val encryptionHelper = EncryptionHelper()

    private val alreadyEncounteredSaltSet = HashSet<String>()
    private val connectionToken = System.currentTimeMillis().toString()

    private val _connectionLD = MutableLiveData<SocketWrapper>()
    val connectionLD: LiveData<SocketWrapper> get() = _connectionLD

    var listener: Listener? = null

    private var _lastException: Exception? = null
    private var _lastExceptionConn: String? = null

    val lastException: Exception?
        get() = _lastException

    init {
        setup()
    }

    fun setup() {
        connectionLostTimeout = 15
        writeCommsSecrets(
            EncryptionHelper.toHex(encryptionHelper.encKey),
            EncryptionHelper.toHex(encryptionHelper.iv)
        )
        if (isDebug) Timber.d("comms secrets have been written to file")

    }

    private fun writeCommsSecrets(encryptionKey: String, initializationVector: String) {
        val commsDir = File(appContext.filesDir, DIR_NODE_COMMS)
        if (!commsDir.exists()) {
            if (!commsDir.mkdir()) throw RuntimeException()
        }
        val commsFile = File(commsDir, NODE_COMMS_FILE)
        if (commsFile.exists()) {
            if (!commsFile.delete()) throw RuntimeException()
        }
        if (!commsFile.createNewFile()) throw RuntimeException()

        commsFile.printWriter().use { out ->
            out.println(encryptionKey)
            out.println(initializationVector)
            out.println(connectionToken)
            out.println("ws://${getIP()}")
        }
    }

    private fun getIP(): String {
        return "${address.hostName}:${address.port}"
    }

    override fun stop() {
        listener = null

        super.stop()
    }

    override fun onOpen(conn: WebSocket, handshake: ClientHandshake) {
        if (isDebug) Timber.d("new connection from:${conn.remoteSocketAddress} resourceDescriptor:${handshake.resourceDescriptor}")

        val uri = Uri.parse(handshake.resourceDescriptor)

        val salt = uri.getQueryParameter(GET_PARAM_SALT)
        val token = uri.getQueryParameter(GET_PARAM_TOKEN)
        if (!checkValidConnectionParams(salt, token)) {
            conn.close(CloseFrame.REFUSE, "expected a valid connection-token")
            return
        }

        //close an existing connection if there is one
        _connectionLD.value?.sendClose(CloseFrame.UNEXPECTED_CONDITION, "new node connection")

        _connectionLD.postValue(SocketWrapper(conn) { message ->
            encryptionHelper.encrypt(message)
        })

        if (isDebug) Timber.d("_connectionLD updated")
    }

    private fun checkValidConnectionParams(salt: String?, token: String?): Boolean {
        salt ?: return false
        token ?: return false
        if (alreadyEncounteredSaltSet.contains(salt)) return false
        alreadyEncounteredSaltSet.add(salt)

        val decryptedToken = encryptionHelper.decrypt(token)

        return decryptedToken == connectionToken + salt
    }

    override fun onClose(conn: WebSocket, code: Int, reason: String?, remote: Boolean) {
        /*if (isDebug) */Timber.d("closed ${conn.remoteSocketAddress} with exit code $code additional info: $reason")


        _connectionLD.postValue(null)

    }

    override fun onMessage(conn: WebSocket, message: String) {
        if (isDebug) Timber.d("received message: $message")


        listener?.onMessage(encryptionHelper.decrypt(message))
    }

    override fun onStart() {
        if (isDebug) Timber.d("server started successfully")

        listener?.onStart()
    }

    override fun onError(conn: WebSocket?, ex: Exception) {
        if (isDebug) Timber.d(ex, "an error occurred on connection ${conn?.remoteSocketAddress}")

        if (ex is BindException) {
            onBindException(ex)
            return
        }

        _lastException = ex
    }


    var onBindException: (BindException) -> Unit = { e ->
        Timber.e(e)
    }








    interface Listener {
        fun onStart()
        fun onMessage(message: String)
    }

    class SocketWrapper(
        val socket: WebSocket,
        private val encrypt: (String)->String
    ) {
        fun send(message: String) {
            val encryptedMessage = encrypt(message)
            socket.send(encryptedMessage)
        }

        internal fun sendClose(code: Int, message: String) {
            socket.close(code, message)
        }
    }

    companion object {
        const val isDebug = false

        private const val DIR_NODE_COMMS = "NodeComms"
        private const val NODE_COMMS_FILE = "secrets"

        private const val GET_PARAM_SALT = "salt"
        private const val GET_PARAM_TOKEN = "token"
    }
}