package app.bitcoin.node

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.*
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import timber.log.Timber
import java.net.BindException
import java.net.InetSocketAddress
import kotlin.random.Random

class NodeComms(
    val appContext: Context,
    private val _connectionLD: MutableLiveData<NodeCommsServer.SocketWrapper>
) {

    private val scope = CoroutineScope(Dispatchers.IO)

    private var host: String? = null
    private var port: Int? = null
    private var commsServer: NodeCommsServer? = null

    private var sourceLD: LiveData<NodeCommsServer.SocketWrapper>? = null //todo doublecheck if this is cleaned-up properly

    private var listener: MessageListener? = null

    var boundConnection: MessengerConnection? = null

    private var serviceMustBeForeground = false

    private var isKillingNow = false

    private val foregroundWakeLock: PowerManager.WakeLock


    init {
        Timber.d("constructor")

        val powerManager = appContext.getSystemService(Context.POWER_SERVICE) as PowerManager
        foregroundWakeLock = powerManager.newWakeLock(
            PowerManager.PARTIAL_WAKE_LOCK,
            NodeService.TAG_CPU_WAKE_LOCK
        )
        foregroundWakeLock.setReferenceCounted(false)

    }

    private fun startForeground() {
        if (serviceMustBeForeground) {
            Timber.d("already in foreground; returning")
            return
        }

        serviceMustBeForeground = true


        val boundConn = boundConnection ?: let {
            Timber.d("boundConnection was NULL; returning")
            return
        }

        boundConn.sendEnterForeground()
    }

    private fun stopForeground() {
        if (!serviceMustBeForeground) {
            Timber.d("already NOT in foreground; returning")
            return
        }

        serviceMustBeForeground = false

        val boundConn = boundConnection ?: let {
            Timber.d("boundConnection was NULL; returning")
            return
        }

        boundConn.sendExitForeground()
    }

    private fun ensureAlive() {
        commsServer?.let {
            return

        }
        startCommsServer()
    }

    private fun startNodeService() {
        Timber.d("#startNodeService")

        val connection = MessengerConnection()
        boundConnection = connection

        val intent = NodeService.getStartIntent(appContext, serviceMustBeForeground)
        val flags = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Context.BIND_AUTO_CREATE or
                    Context.BIND_ABOVE_CLIENT or
                    Context.BIND_INCLUDE_CAPABILITIES or
                    Context.BIND_IMPORTANT
        } else {
            Context.BIND_AUTO_CREATE or
                    Context.BIND_ABOVE_CLIENT or
                    Context.BIND_IMPORTANT
        }

        val result = appContext.bindService(intent, connection, flags)
        //appContext.startService(intent)
        Timber.d("#startNodeService service is binding: $result")
    }



    inner class MessengerConnection : ServiceConnection {

        //use this object to send messages to the NodeService if this ever becomes a thing in the future
        var messenger: Messenger? = null

        val backwardsMessenger = Messenger(BackwardsMessageHandler())

        var queuedMessage: Runnable? = null

        override fun onServiceDisconnected(name: ComponentName?) {
            //called when the service dies (not stopped)

            Timber.d("onServiceDisconnected ... starting of kill-self")
            NodeComms.kill {
                Timber.d("onServiceDisconnected ... callback of kill-self")
            }
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Timber.d("onServiceConnected")
            messenger = Messenger(service)

            //register for receiving of backwards-messages from the service
            val registeringMessage = Message.obtain(null, NodeService.REGISTER)
            registeringMessage.replyTo = backwardsMessenger
            messenger!!.send(registeringMessage)

            queuedMessage?.run()
            queuedMessage = null
        }

        fun sendEnterForeground() {
            Timber.d("sendEnterForeground")

            val m = Message.obtain()
            m.what = NodeService.MESSAGE_ENTER_FOREGROUND

            messenger?.send(m) ?: let {
                Timber.d("sendEnterForeground message queued")
                queuedMessage = Runnable {
                    messenger!!.send(m)
                }
            }
        }

        fun sendExitForeground() {
            Timber.d("sendExitForeground")

            val m = Message.obtain()
            m.what = NodeService.MESSAGE_EXIT_FOREGROUND

            messenger?.send(m) ?: let {
                Timber.d("sendExitForeground message queued")
                queuedMessage = Runnable {
                    messenger!!.send(m)
                }
            }
        }

    }


    class BackwardsMessageHandler : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                NodeService.INFO -> {
                    if (msg.obj is Bundle) {
                        (msg.obj as Bundle).getString("info")?.let { info ->
                            Timber.d("NodeService>>> $info")
                        }

                    } else {
                        Timber.d("NodeService>>> ... unexpected argument")
                    }

                }
                else -> {
                    Timber.e(RuntimeException())
                }
            }
        }
    }


    private fun kill(callback: (resultCode: Int) -> Unit) {
        Timber.d("#kill do")
        isKillingNow = true

        commsServer?.stop()
        commsServer?.listener = null
        commsServer = null
        host = null
        port = null


        foregroundWakeLock.release()

        boundConnection?.let {
            appContext.unbindService(it)
        }
        boundConnection = null


        isKillingNow = false

        callback(1)
    }

    private fun onServerConnectionDisconnected() {
        commsServer ?: let {
            Timber.d("`commsServer` is already cleaned up")
            return
        }

        if (isKillingNow) {
            Timber.d("returning; isKillingNow == TRUE")
            return
        }

        Timber.d("cleaning up NodeComms")
        NodeComms.kill {
            Timber.d("clean up of NodeComms completed")
        }
    }

    private fun startCommsServer() {
        Timber.d("#startCommsServer")

        val host = "localhost"

        val port = Random.nextInt(9999, 13337)

        doStartCommsServer(0, host, port)
    }

    private fun doStartCommsServer(attempts: Int, host: String, port: Int) {

        if (maxBindAttempts < attempts) {
            Timber.d("max bind attempts exceeded")
            throw BindException()
        }
        Timber.d("doStartCommsServer attempts: $attempts")

        val server = NodeCommsServer(appContext, InetSocketAddress(host, port))
        server.listener = commsServerListener
        server.onBindException = { e ->
            Timber.d("onBindException ... attempts:$attempts \n$e")

            this.commsServer = null
            this.host = null
            this.port = null

            doStartCommsServer(attempts + 1, host, port + 1)
        }


        server.connectionLostTimeout = 10
        server.start()
        Timber.d("#startSocketServer ...running")



        this.commsServer = server
        this.host = host
        this.port = port
    }

    private val commsServerListener = object : NodeCommsServer.Listener {
        override fun onStart() {
            Timber.d("#onStart")
            startNodeService()

            foregroundWakeLock.acquire(1000L * 60 * 50) //TODO currently hardcodes 50min ............this should be reacquired every once in a while.............................

            //`plug in` & start observing messages coming from node
            scope.launch(Dispatchers.Main) {
                sourceLD = commsServer?.connectionLD
                sourceLD?.let {
                    Timber.d("plugging the web-sockets connection-ld into connection-ld of this")
                    it.observeForever { value ->
                        Timber.d("onConnected to socket-server ....ld:$value")
                        _connectionLD.value = value

                        if (value == null) {
                            onServerConnectionDisconnected()
                        }
                    }


//                    _connectionLD.addSource(it) { value ->
//                        Timber.d("onConnected to socket-server")
//                        _connectionLD.value = value
//                    }
                }
            }

        }

        override fun onMessage(message: String) {
            listener?.onMessage(message) ?: let {
                Timber.d("#onMessage was called, but there was not listener")
            }
        }
    }

    companion object {

        data class ConnectHolder(
            val appContext: Context,
            val listener: MessageListener,
            val startInForeground: Boolean,
            val socketLd: MutableLiveData<NodeCommsServer.SocketWrapper>
        )


        private var comms: NodeComms? = null

        private const val maxBindAttempts = 20

        private var isInMiddleOfKilling = false

        /**
         * Holds connect params until the killing of the previous connection has completed
         */
        private var awaitingKillCleanUp: ConnectHolder? = null

        /**
         * The passed in listener will become invalid when the socket closes.
         * Calling this method multiple times will replace message-listeners invalidating
         * the previous one.
         */
        @Synchronized
        fun connect(
            appContext: Context,
            listener: MessageListener,
            startInForeground: Boolean
        ): LiveData<NodeCommsServer.SocketWrapper> {

            val socketLd = MutableLiveData<NodeCommsServer.SocketWrapper>()

            if (isInMiddleOfKilling) {
                Timber.d("previous connection is in the middle of killing; deferring reconnection")

                awaitingKillCleanUp?.let {
                    throw RuntimeException("unexpected connect attempts")
                }

                awaitingKillCleanUp = ConnectHolder(
                    appContext.applicationContext,
                    listener,
                    startInForeground,
                    socketLd
                )

                return socketLd
            }

            return comms?.let {
                Timber.d("comms exists; returning connection LD")

                //swap out the listeners
                it.listener = listener

                it._connectionLD
            } ?: let {
                doConnect(
                    appContext.applicationContext,
                    listener,
                    startInForeground,
                    socketLd
                )

                socketLd
            }

        }



        @Synchronized
        private fun doConnect(
            appContext: Context,
            listener: MessageListener,
            startInForeground: Boolean,
            socketLd: MutableLiveData<NodeCommsServer.SocketWrapper>
        ) {

            //instantiate
            val c = NodeComms(appContext, socketLd)
            comms = c

            //configure
            c.listener = listener
            if (startInForeground) c.startForeground()

            //start up
            c.ensureAlive()
        }


        @Synchronized
        fun kill(callback: () -> Unit) {
            Timber.d("#kill")

            comms?.let {
                isInMiddleOfKilling = true

                it._connectionLD.postValue(null)

                it.kill {
                    comms = null
                    isInMiddleOfKilling = false
                    callback()

                    //
                    //
                    //killing of the connection has completed

                    val awaitingKillList = awaitingKillCleanUp
                    awaitingKillCleanUp = null

                    awaitingKillList?.let { holder ->
                        Timber.d("there is a deferred connect attempt; fulfill it")

                        doConnect(
                            holder.appContext,
                            holder.listener,
                            holder.startInForeground,
                            holder.socketLd
                        )

                    } ?: Timber.d("deferred connect attempt not found")


                }

            } ?: callback()
        }


        fun enterForeground() {
            val c = comms ?: let {
                Timber.d("returning; comms is not alive")
                return
            }

            c.startForeground()
        }

        fun exitForeground() {
            val c = comms ?: let {
                Timber.d("returning; comms is not alive")
                return
            }

            c.stopForeground()
        }
    }

    interface MessageListener {
        fun onMessage(m: String)
    }
}