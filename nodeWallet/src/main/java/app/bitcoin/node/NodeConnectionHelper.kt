package app.bitcoin.node

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import timber.log.Timber
import kotlin.RuntimeException
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

//todo
//todo
//todo there is a lot of tech-debt in this class ... clean it up !!!
//todo


//todo consider adding StateFlow that will get updated as node is started/killed; `connectionLD` does not serve this purpose at the moment, it just is required for internal state-keeping
class NodeConnectionHelper(
    private val appContext: Context,
    private val scope: CoroutineScope,
    private val appInForegroundLiveData: LiveData<Boolean>,
    private val notifyNodeForegroundLiveData: MutableLiveData<Boolean>,
    private val notifyNodeConnectedLiveData: MutableLiveData<Boolean>
) {
    private var connectionLD: LiveData<NodeCommsServer.SocketWrapper>? = null

    private var nodeSocket: NodeCommsServer.SocketWrapper? = null

    private var unsatisfiedCallbacks = ArrayList<(NodeCommsServer.SocketWrapper)->Unit>()

    private var killNode: Job? = null

    private val millisKillDelay = 1000 * 45L
    private val millisEnsurePeriod = 1000 * 10L //todo ...prob can be reduced; this was increased before app-update-node-directory-swap was optimised & it was taking a long time for LARGE node-projects

    private lateinit var nodeListener: NodeComms.MessageListener




    private var requestedForeground = false
    private var clearDelayOfDisconnect: ((Boolean)->Unit)? = null

    val isStarted: Boolean
        get() {
            if (nodeSocket != null) return true
            if (unsatisfiedCallbacks.isNotEmpty()) return true

            return false
        }

    private val isStartingNow = MutableStateFlow(false)
    val isStarting: StateFlow<Boolean>
        get() = isStartingNow

    private val isKillingNow = MutableStateFlow(false)
    val isKilling: StateFlow<Boolean>
        get() = isKillingNow

    fun setup(messageListener: NodeComms.MessageListener) {
        nodeListener = messageListener

        appInForegroundLiveData.observeForever { isForeground ->
            if (isForeground) {
                cancelDisconnect()
                connectionLD ?: scope.launch {
                    ensureSocket()
                }

            } else {
                scheduleDisconnect()

            }
        }
    }

    val isForeground: Boolean
        get() {
            return notifyNodeForegroundLiveData.value ?: false
        }

    fun startForeground() {
        Timber.d("startForeground")

        requestedForeground = true

        if (connectionLD != null) {
            Timber.d("connection existed")
            //connecting or connected
            NodeComms.enterForeground()
        } else {
            Timber.d("no connection; connect NOW")
            scope.launch {
                ensureSocket()
            }
            scheduleDisconnect()

        }

        notifyNodeForegroundLiveData.postValue(true)
    }

    fun stopForeground() {
        Timber.d("stopForeground")
        requestedForeground = false

        clearDelayOfDisconnect?.let {
            //disconnect was prevented ...proceed
            it.invoke(true)
        }
        clearDelayOfDisconnect = null

        NodeComms.exitForeground()

        notifyNodeForegroundLiveData.postValue(false)
    }

    private fun cancelDisconnect() {
        Timber.d("cancelDisconnect")
        killNode?.cancel()
        killNode = null

        clearDelayOfDisconnect?.let {
            //disconnect was prevented ...clear it without proceeding
            it.invoke(false)
        }
        clearDelayOfDisconnect = null

    }

    private fun scheduleDisconnect() {
        killNode.let { job ->
            if (job != null && job.isActive) {
                Timber.d("scheduleDisconnect ... a node-killing-job is already scheduled")
                return
            }
        }

        killNode = scope.launch {
            try {
                Timber.d("scheduleDisconnect ... kill-job launched")
                delay(millisKillDelay)
                if (!isActive) {
                    return@launch
                }

                val proceedWithDisconnect = considerDelayOfDisconnect()

                if (proceedWithDisconnect) {
                    disconnect()
                    killNode = null
                }
            } catch (e: Exception) {
                if (e is CancellationException) {
                    Timber.d("`killNode` job has been cancelled")
                } else {
                    Timber.e(e)
                }
                killNode = null
            }
        }
    }

    /**
     * @return TRUE to proceed with disconnect
     */
    private suspend fun considerDelayOfDisconnect(): Boolean {
        return suspendCoroutine { continuation ->

            if (!requestedForeground) {
                Timber.d("proceed with disconnection")
                continuation.resume(true)
                return@suspendCoroutine
            }

            Timber.d("prevent proceeding with disconnection")
            //...prevent proceeding by not calling continuation
            //...provide remote means to proceed
            clearDelayOfDisconnect = { proceedWithDisconnect ->
                Timber.d("disconnection prevention cleared; proceedWithDisconnect:$proceedWithDisconnect")
                continuation.resume(proceedWithDisconnect)

            }
        }
    }





    private suspend fun awaitStartingHasFinished() {
        if (!isStartingNow.value) return

        try {
            Timber.d("...now awaiting for node-js to finish starting")
            isStartingNow.collect { isStartingNow ->
                if (!isStartingNow) {
                    throw RuntimeException()
                }
            }
        } catch (e: Exception) {
            Timber.d("...awaiting completed; node is now no longer starting")
        }
    }

    private suspend fun awaitKillingHasFinished() {
        if (!isKillingNow.value) return

        try {
            Timber.d("...now awaiting for node-js to finish being killed")
            isKillingNow.collect { isKillingNow ->
                if (!isKillingNow) {
                    throw RuntimeException()
                }
            }
        } catch (e: Exception) {
            Timber.d("...awaiting completed; node is now no longer being killed")
        }
    }








    /**
     * Be careful when keeping a reference to the returned object beyond app's `loss of focus`.
     * Node gets murdered after some time of app `going into background`. This will close the
     * socket & it will become invalid.
     *
     * Suggestion: just call this method every time you wish to send a message to the
     * node-project.
     *
     */
    suspend fun ensureSocket(): NodeCommsServer.SocketWrapper = withContext(scope.coroutineContext) {
        //TODO this method can be majorly simplified now that it is run on a single-threaded dispatcher
        awaitKillingHasFinished()
        awaitStartingHasFinished()


        nodeSocket?.let {
            return@withContext it
        }

        val now = System.currentTimeMillis()
        Timber.d("#ensureSocket token:$now")


        return@withContext suspendCoroutine { continuation ->

            var returnedException = false
            val ensuringFailedJob = scope.launch {
                delay(millisEnsurePeriod)
                val e = RuntimeException()
                Timber.e(e, "connection ensuring failed; token:$now; returning an exception")
                returnedException = true
                continuation.resumeWithException(e)
            }


            try {
                setupConnection { socket ->
                    if (returnedException) {
                        Timber.e(RuntimeException(), "#setupConnection callback got called but an exception was already returned")
                        return@setupConnection
                    }

                    Timber.d("onConnection; token:$now")
                    ensuringFailedJob.cancel()
                    continuation.resume(socket)
                }
            } catch (e: Exception) {
                Timber.e(e)

                if (e is CancellationException) {
                    Timber.e(e,"CancellationException detected during #ensureSocket >>> cancelling 'ensuringFailedJob'")
                    ensuringFailedJob.cancel()
                }
            }
        }
    }


    private suspend fun disconnect() {
        awaitStartingHasFinished()

        Timber.d("#disconnect")
        isKillingNow.value = true

        notifyNodeConnectedLiveData.postValue(false)

        NodeComms.kill {
            Timber.d("kill callback")
            isKillingNow.value = false
        }
    }

    private /*suspend */fun setupConnection(callback: (NodeCommsServer.SocketWrapper)->Unit) {

        val socket = nodeSocket
        if (socket != null) {
            //satisfy callback
            Timber.d("socket existed; executing callback")
            scope.launch { callback(socket) }

        } else if (connectionLD != null) {
            //register callback
            unsatisfiedCallbacks.add(callback)
            Timber.d("connecting right now; callback deferred")

            return
        } else {
            //register callback
            Timber.d("defer the callback and connect now")
            isStartingNow.value = true

            unsatisfiedCallbacks.add(callback)


            val startInForeground = requestedForeground

            //establish connection
            NodeComms.connect(appContext, nodeListener, startInForeground).let {
                connectionLD = it

                scope.launch(Dispatchers.Main) {
                    Timber.d("node-connection-live-data is now being observed")
                    it.observeForever { socket ->
                        if (socket == null && nodeSocket == null) {
                            return@observeForever
                        }

                        if (socket != null) {
                            //new connection
                            Timber.d("#onChanged node connected")

                            nodeSocket = socket

                            onConnected()

                        } else {
                            //node died; do cleanup
                            Timber.d("#onChanged node died")

                            nodeSocket = null
                            connectionLD = null

                            onDisconnected()

                        }
                    }
                }




            }

        }
    }

    private fun onConnected() {
        isStartingNow.value = false

        val callbacks = unsatisfiedCallbacks
        unsatisfiedCallbacks = ArrayList()

        val socket = nodeSocket!!
        callbacks.forEach {
            it(socket)
        }

        notifyNodeConnectedLiveData.postValue(true)
    }

    private fun onDisconnected() {
        isStartingNow.value = false

        if (notifyNodeConnectedLiveData.value != false) {
            //this is situation can occur when the NodeService gets killed
            // by the android framework because of SEVERELY constraint resources
            notifyNodeConnectedLiveData.postValue(false)
        }

        if (unsatisfiedCallbacks.isNotEmpty())
            throw RuntimeException("did you try to communicate with node while in background?")
    }


}