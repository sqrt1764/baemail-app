package app.bitcoin.baemail.wallet

import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ThrottledExecQueueTest {

    @Before
    fun setUp() {
    }

    @Test
    fun throttling() = runTest {

        val scope = TestScope(StandardTestDispatcher(testScheduler))

        val queue = ThrottledExecQueue(3, scope)
        queue.start()

        //counter for the number of #execute call done
        var i = 0

        //generate lots of execute calls
        val eventCreationJob = backgroundScope.launch {
            for (j in 0 .. 10) {
                val result = queue.execute {
                    ++i
                    i
                }
            }
        }


        advanceTimeBy(1000)

        eventCreationJob.cancel()
        scope.cancel()

        //check whether the throttling has happened
        assertEquals(3, i)

    }
}