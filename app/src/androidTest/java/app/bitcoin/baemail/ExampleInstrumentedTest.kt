package app.bitcoin.baemail

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.json.JSONArray
import org.junit.Assert

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("app.bitcoin.baemail", appContext.packageName)
    }


    @Test
    fun test0() {
        val arr = JSONArray()
        arr.put("abc")
        arr.put("def1")
        val arrString = arr.toString()

        val query = """
                {
                  "v": 3,
                  "q": {
                    "find": { "out.e.a" : { "${"$"}in": $arrString }},
                    "limit": 20
                  },
                  "r": {
                    "f": "[ .[] | {tx_id: .tx.h, o: .out[].e} | {tx_id: .tx_id, sats: .o.v, index: .o.i, address: .o.a} ]"
                  }
                }
            """.trimIndent()

        assertEquals("", query)
    }
}
