package app.bitcoin.baemail.room.dao

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import app.bitcoin.baemail.core.data.room.AppDatabase
import app.bitcoin.baemail.core.data.room.converters.DbConverters
import app.bitcoin.baemail.core.data.room.dao.CoinDao
import app.bitcoin.baemail.core.data.room.entity.Coin
import com.google.gson.Gson
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class CoinDaoTest {

    private lateinit var coinDao: CoinDao
    private lateinit var appDatabase: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val gson = Gson()
        appDatabase = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java)
            .addTypeConverter(DbConverters(gson))
            .build()
        coinDao = appDatabase.coinDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        appDatabase.close()
    }

    @Test
    fun getHighestCoinAddressByPaymail() = runTest {

        Coin(
            "p0@moneybutton.com",
            "/0/33",
            33,
            "addr033",
            "abc0124",
            0,
            333,
            111
        ).let {
            coinDao.insertCoin(it)
        }

        Coin(
            "p0@moneybutton.com",
            "/0/3",
            3,
            "addr03",
            "abc0124",
            0,
            13,
            111
        ).let {
            coinDao.insertCoin(it)
        }


        Coin(
            "p1@moneybutton.com",
            "/0/4",
            4,
            "addr04",
            "abc01245",
            0,
            13,
            112
        ).let {
            coinDao.insertCoin(it)
        }

        Coin(
            "p1@moneybutton.com",
            "/0/8",
            8,
            "addr08",
            "abc01245",
            0,
            888,
            118
        ).let {
            coinDao.insertCoin(it)
        }

        Coin(
            "p1@moneybutton.com",
            "/0/1",
            1,
            "addr111",
            "abc01245",
            0,
            111,
            188
        ).let {
            coinDao.insertCoin(it)
        }

        Coin(
            "p2@moneybutton.com",
            "/0/5",
            5,
            "addr05",
            "000abc012456",
            0,
            13,
            113
        ).let {
            coinDao.insertCoin(it)
        }




        val list = coinDao.getHighestCoinAddressByPaymail().first()

        assertEquals(3, list.size)

        list.find { it.paymail == "p0@moneybutton.com" }.let {
            assertNotNull(it)
            assertEquals(33, it!!.addressPathIndex)
        }

        list.find { it.paymail == "p1@moneybutton.com" }.let {
            assertNotNull(it)
            assertEquals(8, it!!.addressPathIndex)
        }

        list.find { it.paymail == "p2@moneybutton.com" }.let {
            assertNotNull(it)
            assertEquals(5, it!!.addressPathIndex)
        }


    }
}