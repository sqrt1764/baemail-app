
const originalStdoutWrite = process.stdout.write.bind(process.stdout);

const bufferedStdOutWrites = [];
const registeredLogListenerHolder = {};

//override the Console#log
process.stdout.write = (chunk, encoding, callback) => {
    if (typeof chunk === 'string') {
        //log-entries must be sent to android
        bufferedStdOutWrites.push(chunk);

        //inform registered-log-listeners of the log-entry
        Object.keys(registeredLogListenerHolder).forEach((key) => {
            registeredLogListenerHolder[key](chunk);
        });
    }

    return originalStdoutWrite(chunk, encoding, callback);
};

const originalStderrWrite = process.stderr.write.bind(process.stderr);
process.stderr.write = (chunk, encoding, callback) => {
    if (typeof chunk === 'string') {
        //log-entries must be sent to android
        bufferedStdOutWrites.push(chunk);

        //inform registered-log-listeners of the log-entry
        Object.keys(registeredLogListenerHolder).forEach((key) => {
            registeredLogListenerHolder[key](chunk);
        });
    }

    return originalStderrWrite(chunk, encoding, callback);
};

const redirectLogsToAndroid = function() {
    //originalStdoutWrite('executing #redirectLogsToAndroid', 'utf-8')

    var copyArray = bufferedStdOutWrites.splice(0, bufferedStdOutWrites.length);
    //bufferedStdOutWrites is now empty

    if (copyArray.length < 1) {
        //originalStdoutWrite('#redirectLogsToAndroid ... not sent because it is empty', 'utf-8')
        return
    }

    //communicate the found logs to android
    var callbackSendLogs = (status) => {
        //console.log('response to -- SendLogs... success:' + status);
    };
    var workRequest = context.requestManager.forwardRequests.requests.sendLogs
            .buildRequest(copyArray, callbackSendLogs);

    context.requestManager.forwardRequests.send(workRequest);
};

const registerLogListener = function(id, writeLogLambda) {
    if (registeredLogListenerHolder[id]) {
        throw new Error("a listener with this ID is already registered; id:" + id);
    }

    registeredLogListenerHolder[id] = writeLogLambda;
};

const disconnectLogListener = function(id) {
    delete registeredLogListenerHolder[id];
};









////////////////////////////
////////////////////////////


const bsv = require('bsv')
bsv.Message = require('bsv/message')

const mnemonic = require('bsv/mnemonic');

const ECIES = require('bsv/ecies');

const CommsHelper = require('./comms-helper');


const aesjs = require('aes-js');
const EncryptionHelper = require('./encryption-helper');




//find config-json in the start parameters of node
const nodeConfigArgs = JSON.parse(process.argv[2]); //todo not sure hardcoding `2` is the best




const context = {
    bsv: bsv,
    mnemonic: mnemonic,
    ECIES: ECIES,
    moneyButtonDerivationPath: "m/44'/0'/0'/0/0/0",
    simplyCashDerivationPath: "m/44'/145'/0'/2/0",
    relayXPath: "m/44'/236'/0'",
    fundingWalletDerivationPathRoot: "m/44'/0'/0'/0",
    requestManager: null, //initialized later
    helper: {
        requestKeepAlive: null, //initialized later
        releaseKeepAlive: null, //initialized later
        getPaymailDestinationOutput: null, //initialized later
    },
    repl: {
        registerLogListener: registerLogListener,
        disconnectLogListener: disconnectLogListener
    },
    nodeConfig: nodeConfigArgs
    // `twetch` is initialized later;
    // `stall` is initialized later;
}






console.log('start of `main.js`; node version: ' + process.version);








////////////////////////////
////////////////////////////
// wire up the stall-module to the context

const StallHelper = require('./stall/index');
new StallHelper(context);


////////////////////////////
////////////////////////////
// wire up the twetch lib to the context

const TwetchHelper = require('./twetch/index');
new TwetchHelper(context);


////////////////////////////
////////////////////////////
////////////////////////////
////////////////////////////
//////////////////////////// PREPARE 'android_work_request' BEING SENT INTO THE REQUEST MANAGER
//////////////////////////// (sending to the android side of project)

const AddXpriv = require('./requests/add-xpriv');
const SignRequest = require('./requests/sign-request');
const NotifyCoinsSpent = require('./requests/notify-coins-spent');
const RequestRefreshOfCoins = require('./requests/request-refresh-of-coins');
const SendLogs = require('./requests/send-logs');
const RequestKeepAlive = require('./requests/request-keep-alive');
const ReleaseKeepAlive = require('./requests/release-keep-alive');
const GetPaymailDestinationOutput = require('./requests/get-paymail-destination-output');
const InformP2pConnectedStallChange = require('./requests/inform-p2p-connected-stall-change');
const InformP2pEvent = require('./requests/inform-p2p-event');
const InformP2pNearbyInfo = require('./requests/inform-p2p-nearby-info');

const androidWorkRequests = {
    addXpriv: new AddXpriv(context),
    signRequest: new SignRequest(context),
    notifyCoinsSpent: new NotifyCoinsSpent(context),
    requestRefreshOfCoins: new RequestRefreshOfCoins(context),
    sendLogs: new SendLogs(context),
    requestKeepAlive: new RequestKeepAlive(context),
    releaseKeepAlive: new ReleaseKeepAlive(context),
    getPaymailDestinationOutput: new GetPaymailDestinationOutput(context),
    informP2pConnectedStallChange: new InformP2pConnectedStallChange(context),
    informP2pEvent: new InformP2pEvent(context),
    informP2pNearbyInfo: new InformP2pNearbyInfo(context),
    //todo add others
}






////////////////////////////
////////////////////////////
////////////////////////////
////////////////////////////
//////////////////////////// PREPARE HANDLERS FOR INCOMING 'node_work_request' OF REQUEST MANAGER
//////////////////////////// (coming from android side of project)

const MoneyButtonPublicKeyFromSeed = require('./handlers/money-button-public-key-from-seed');
const PrivateKeyForPathFromSeed = require('./handlers/private-key-for-path');
const SignMessage = require('./handlers/sign-message');
const CheckMnemonicIsValid = require('./handlers/check-mnemonic-is-valid');
const DeriveNextAddressSet = require('./handlers/derive-next-address-set');
const CreateSimpleTx = require('./handlers/create-simple-tx');
const CreateEqualSplitCoinTx = require('./handlers/create-equal-split-coin-tx');
const CreateSimpleOutputScriptTx = require('./handlers/create-simple-output-script-tx');
const CreateBaemailMessageTx = require('./handlers/create-baemail-message-tx');
const DecryptPkiEncryptedData = require('./handlers/decrypt-pki-encrypted-data');
const SignMessageWithXpriv = require('./handlers/sign-message-with-xpriv');
const InformActivePaymailChanged = require('./handlers/inform-active-paymail-changed');
const InformAboutAvailableCoins = require('./handlers/inform-about-available-coins');
const TwetchAuth = require('./handlers/twetch-auth');
const RunScript = require('./handlers/run-script');
const StartStallSocketServer = require('./handlers/start-stall-socket-server');
const StopStallSocketServer = require('./handlers/stop-stall-socket-server');
const InformStallConfigChanged = require('./handlers/inform-stall-config-changed');
const InformDiscoveredStallsChanged = require('./handlers/inform-discovered-stalls-changed');
const InformNearbyP2pConnectionChange = require('./handlers/inform-nearby-p2p-connection-changed');
const InformWifiDirectP2pConnectionChange = require('./handlers/inform-wifi-direct-p2p-connection-changed');


//all supported handlers
const requestHandlers = [
    new MoneyButtonPublicKeyFromSeed(context),
    new PrivateKeyForPathFromSeed(context),
    new SignMessage(context),
    new CheckMnemonicIsValid(context),
    new DeriveNextAddressSet(context),
    new CreateSimpleTx(context),
    new CreateEqualSplitCoinTx(context),
    new CreateSimpleOutputScriptTx(context),
    new CreateBaemailMessageTx(context),
    new DecryptPkiEncryptedData(context),
    new SignMessageWithXpriv(context),
    new InformActivePaymailChanged(context),
    new InformAboutAvailableCoins(context),
    new TwetchAuth(context),
    new RunScript(context),
    new StartStallSocketServer(context),
    new StopStallSocketServer(context),
    new InformStallConfigChanged(context),
    new InformDiscoveredStallsChanged(context),
    new InformNearbyP2pConnectionChange(context),
    new InformWifiDirectP2pConnectionChange(context),
    //add others
];






///////////////////////////
///////////////////////////
///////////////////////////
///////////////////////////
/////////////////////////// SETUP ENCRYPTED WEB-SOCKET COMMUNICATION WITH ANDROID-SIDE

const commsHelper = new CommsHelper();

commsHelper.onOpen = function() {
    console.log('socket-on-open');

    setInterval(redirectLogsToAndroid, 1337);
};

commsHelper.onError = function(error) {
    console.log('socket-on-error: ' + error);
};

commsHelper.onMessage = function(message) {
    try {
        requestManager.onReceivedMessage(message);
    } catch (e) {
        console.log(e);
    }
};






///////////////////////////
///////////////////////////
///////////////////////////
/////////////////////////// SETUP SIMPLE TWO-WAY REQUEST-RESPONSE MESSAGING INSIDE OF THE WEB-SOCKET
/////////////////////////// COMMUNICATING WITH ANDROID-SIDE


const RequestManager = require('./request-manager');

const requestManager = new RequestManager(
    'android_work_request',
    'node_work_request',
    androidWorkRequests,
    requestHandlers,
    (message) => {
        commsHelper.sendMessage(message);
    }
);
context.requestManager = requestManager;






///////////////////////////
///////////////////////////
///////////////////////////
/////////////////////////// HELPER FUNCTIONS USED FROM ALL OVER THE PLACE

context.helper.requestKeepAlive = async function() {
    await new Promise((resolve, reject) => {

        var callback = (status) => {
            if (!status) {
                console.log(new Error("request failed"));
                reject();
                return;
            }

            //console.log('node-js will be kept alive');

            resolve();
        };
        var workRequest = context.requestManager.forwardRequests.requests.requestKeepAlive
                .buildRequest(callback);

        context.requestManager.forwardRequests.send(workRequest);

        //console.log('keep-alive request made');
    }).catch((e) => {
        console.log(e);
    });
}


context.helper.releaseKeepAlive = async function() {
    await new Promise((resolve, reject) => {

            var callback = (status) => {
                if (!status) {
                    console.log(new Error("request failed"));
                    reject();
                    return;
                }

                //console.log('keep-alive has been released');

                resolve();
            };
            var workRequest = context.requestManager.forwardRequests.requests.releaseKeepAlive
                    .buildRequest(callback);

            context.requestManager.forwardRequests.send(workRequest);

            //console.log('release-keep-alive request made');
        }).catch((e) => {
            console.log(e);
        });
}

context.helper.getPaymailDestinationOutput = async function(destinationPaymail) {
    return await new Promise((resolve, reject) => {

        var callback = (status, outputScript) => {
            if (!status) {
                console.log(new Error("request failed"));
                //reject();
                resolve(null);
                return;
            }

            //console.log('get-paymail-destination-output completed - ' + outputScript);

            resolve(outputScript);
        };
        var workRequest = context.requestManager.forwardRequests.requests.getPaymailDestinationOutput
                .buildRequest(destinationPaymail, callback);

        context.requestManager.forwardRequests.send(workRequest);

        //console.log('get-paymail-destination-output request made');
    }).catch((e) => {
        console.log(e);
    });
}


context.helper.getNewEncryptionHelper = function(sharedKey) {
   var encryptionHelper = new EncryptionHelper(aesjs);

   let key = context.bsv.crypto.Hash.sha256(Buffer.from(sharedKey + '', 'hex')).toString('hex')
   let iv = context.bsv.crypto.Hash.sha256(Buffer.from(key + '', 'hex')).toString('hex').substring(0, 32);

   encryptionHelper.initialise(key, iv);

   return encryptionHelper;
};




////////////////////////////
////////////////////////////
////////////////////////////
//////////////////////////// ON START OF LIFECYCLE

commsHelper.connect();
console.log('ws should be connecting to the app')

setInterval(
    function(){
        originalStdoutWrite('teee-hee', 'utf-8')
    },
    4000
)







