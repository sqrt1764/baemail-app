



/* 

# message from peer to redirecting server ... handshake
....possible improvement - salt included in the evelope should be specified by the redirect server
{
    envelope: '{type: \'handshake\', by: \'public-key\', salt: \'12345\'}',
    by: 'public-key',
    signature: 'signature'
}

# message from peer to redirecting server
{
    id: 'unique-id',
    type: 'redirect',
    from: 'public-key',
    to: 'public-key',
    payload: 'base64-masked-string',
    salt: 'salt-string'
}

# message from redirecting server to peer
{
    type: 'redirect-error',
    from: 'public-key',
    to: 'public-key',
    id: 'unique-id'
}

*/


const startPeerMessageRedirectingServer = require('./start-peer-redirecting-server');
const startPeerClient = require('./start-peer-client');

function createGroupStateDismissed() {
    return {
        isGroupOwner: false,
        ownerIp: null
    };
}

function createGroupStateAsOwner() {
    return {
        isGroupOwner: true,
        ownerIp: null
    };
}

function createGroupStateAsPeer(ownerIp) {
    return {
        isGroupOwner: false,
        ownerIp: ownerIp
    };
}

function createGroupStateFrom(isGroupOwner, ownerIp) {
    if (typeof(isGroupOwner) != 'boolean') throw new Error('unexpected value');

    if (!ownerIp) {
        return {
            isGroupOwner: isGroupOwner,
            ownerIp: null
        };
    } else {
        if (typeof(ownerIp) != 'string') throw new Error('unexpected value');

        return {
            isGroupOwner: isGroupOwner,
            ownerIp: ownerIp
        };
    }
}

function isGroupStateUnchanged(groupState0, groupState1) {
    return groupState0.isGroupOwner == groupState1.isGroupOwner && groupState0.ownerIp == groupState1.ownerIp;
}

function isGroupDismissed(groupState) {
    return groupState.isGroupOwner == false && groupState.ownerIp == null
}


const PORT = 50301;


function WifiDirectPeer(
    context,
    lambdaGetMyStallPki,
    lambdaGetMyStallPkiPrivateKey,
    lambdaOnMessage,
    lambdaOnConnectedToGroup,
    lambdaOnDisconnectedFromGroup
) {
    this.context = context;
    this.lambdaGetMyStallPki = lambdaGetMyStallPki;
    this.lambdaGetMyStallPkiPrivateKey = lambdaGetMyStallPkiPrivateKey;
    this.lambdaOnMessage = lambdaOnMessage;
    this.lambdaOnConnectedToGroup = lambdaOnConnectedToGroup;
    this.lambdaOnDisconnectedFromGroup = lambdaOnDisconnectedFromGroup;
    this.p2pGroupState = createGroupStateDismissed();

    this.redirectingServer = null;
    this.peerClient = null;

    this.onMessage = (from, messageString) => {
        this.lambdaOnMessage(from, messageString);
    };


}

WifiDirectPeer.prototype.onGroupChanged = function(isGroupOwner, ownerIp) {
    var groupState = createGroupStateFrom(isGroupOwner, ownerIp);

    //if the group setup is unchanged then do nothing
    if (isGroupStateUnchanged(this.p2pGroupState, groupState)) {
        console.log('#onGroupChagned exiting because the group is unchanged');
        return;
    }

    //if the group has been dismissed then ensure everything is cleaned up
    if (isGroupDismissed(groupState)) {
        console.log('#onGroupChagned exiting because the group got dismissed');
        this.p2pGroupState = groupState;
        this.cleanupCurrentRedirectingServer();
        this.cleanupCurrentPeerClient();

        return;
    }

    //if this peer has become the owner of the group then cleanup peer-client if active
    if (isGroupOwner) {
        console.log('#onGroupChagned; this device is the peer-message redirecting-server');
        this.p2pGroupState = groupState;
        this.cleanupCurrentPeerClient();
        this.setupRedirectingServer();

        return;
    }


    //if this peer has lost owner then cleanup & connect to the new owner
    this.p2pGroupState = groupState;
    console.log('#onGroupChanged; p2pGroupState updated: ' + JSON.stringify(groupState));

    //ensure peer-message-redirecting-server is cleaned up
    this.cleanupCurrentRedirectingServer();

    //ensure that any existing instance of peer-client is cleaned up
    this.cleanupCurrentPeerClient();

    //ensure to start a peer-client that is communicating to the latest `ownerIp`
    setTimeout(() => {//todo placeholder .... do not do this !!!!
        console.log('#onGroupChagned; this device is client communicating with redirecting-server');
        this.setupPeerClient(ownerIp);

    }, 999);

}

WifiDirectPeer.prototype.isConnected = function() {
    return this.redirectingServer != null || this.peerClient != null;
};

WifiDirectPeer.prototype.getActivePki = function() {
    return this.peerPki;
};


WifiDirectPeer.prototype.send = function(destination, messageString, callback) {
    if (this.p2pGroupState.isGroupOwner) {
        if (this.redirectingServer == null) throw new Error('unexpected instance-state');
        this.redirectingServer.sendMessage(destination, messageString, callback);

    } else if (this.p2pGroupState.ownerIp != null) {
        if (this.peerClient == null) throw new Error('unexpected instance-state');
        this.peerClient.sendMessage(destination, messageString, callback);

    } else {
        throw new Error('p2p-group is not active; cannot send messages, dropped');

    }
}










WifiDirectPeer.prototype.cleanupCurrentRedirectingServer = function() {
    if (this.redirectingServer == null) return;

    var current = this.redirectingServer;
    this.redirectingServer = null;

    current.cleanUp();
}

WifiDirectPeer.prototype.cleanupCurrentPeerClient = function() {
    if (this.peerClient == null) return;

    var current = this.peerClient;
    this.peerClient = null;

    current.disconnectNow();
}




WifiDirectPeer.prototype.setupRedirectingServer = function() {
    const myPki = this.lambdaGetMyStallPki();
    const myPkiPrivateKey = this.lambdaGetMyStallPkiPrivateKey();
    const server = startPeerMessageRedirectingServer(this.context, PORT, myPki, myPkiPrivateKey);

    //todo make `lambdaOnConnectedToGroup` functional

    server.interfaceOfSelf.onMessage = (from, messageString) => {
        this.onMessage(from, messageString);
    };

    server.onDisconnected = () => {
        this.redirectingServer = null;
        this.peerPki = null;

        console.log('...peer-redirect-server -- #onDisconnected ');
        this.lambdaOnDisconnectedFromGroup();

    }

    server.onNewClient = (clientInterface) => {
        this.lambdaOnConnectedToGroup();
    };


    //todo anything else needs wiring up?
    //todo
    //todo

    this.redirectingServer = server;
    this.peerPki = myPki;
}

WifiDirectPeer.prototype.setupPeerClient = function(ownerIp) {
    const myPki = this.lambdaGetMyStallPki();
    const myPkiPrivateKey = this.lambdaGetMyStallPkiPrivateKey();
    const peerClient = startPeerClient(this.context, myPki, myPkiPrivateKey, ownerIp, PORT);

    peerClient.onConnected = () => {
        this.lambdaOnConnectedToGroup();
    };

    peerClient.onDisconnected = (error) => {
        this.peerClient = null;
        this.peerPki = null;

        console.log('...peer-client -- #onDisconnected');
        this.lambdaOnDisconnectedFromGroup();

        //todo when should this do connect-retries & when it should not?
        //todo
        //todo
    };
    peerClient.onMessage = (from, plainString) => {
        this.onMessage(from, plainString);
    };
    peerClient.onRedirectError = (plainString) => {
        console.log('...#onRedirectError ' + plainString);
        //todo
        //todo
        //todo
    };

    //todo anything else needs wiring up?
    //todo
    //todo

    this.peerClient = peerClient;
    this.peerPki = myPki;
}




module.exports = WifiDirectPeer;