

const fs = require('fs');
const fsPromises = fs.promises;

const xor = require('buffer-xor/inplace');
const getMaskingSecretGenerator = require('./masking-secret-generator');

const path = require('path');
const http = require('http');

const LRU = require("lru-cache");





function DownloadHelper(context) {
    this.context = context;

    let appDir = path.dirname(require.main.filename);
    let projectParentDir = path.resolve(appDir, '..');

    this.clientSideStallCacheDirectoryCachePath = projectParentDir + path.sep + 'stall-client-cache';
    fs.mkdirSync(this.clientSideStallCacheDirectoryCachePath, { recursive: true });







    this.downloadedFileCache = new LRU({
        max: 66,
        length: (n, key) => { return 1 },
        dispose: (key, filePath) => {
           //deleting this file.....
           setTimeout(async () => {
               try {
                   await fsPromises.access(filePath);
                   await fsPromises.unlink(filePath, {force : true});
               } catch (e) {
                   if (e.code == 'ENOENT') {
                       //file does not exist
                       return;
                   }
                   console.log(e);
               }
           }, 1);
        },
        maxAge: 1000 * 60 * 60,
        updateAgeOnGet: true
    });




    //make sure this dir `clientSideStallCacheDirectoryCachePath` is clean at startup
    setTimeout(async () => {
        fsPromises.readdir(this.clientSideStallCacheDirectoryCachePath).then((result) => {
            setTimeout(async () => {
                for (f of result) {
                    await fsPromises.unlink(
                        this.clientSideStallCacheDirectoryCachePath + path.sep + f,
                        { force : true }
                    );
                }
            }, 1);

        });
    }, 1234);

};




module.exports = DownloadHelper;




DownloadHelper.prototype.provideNewMaskingHelper = function(sharedKeyHex, saltString) {
    const Hash = this.context.bsv.crypto.Hash;

    var finalKey = Hash.sha256(Buffer.concat([
        Buffer.from(sharedKeyHex, "hex"),
        Buffer.from(saltString, "utf8")
    ]));

    return getMaskingSecretGenerator(
        finalKey,
        (maskingChunkBuffer) => {
            return Hash.sha256(maskingChunkBuffer);
        }
    );
};


DownloadHelper.prototype.downloadFileFromStall = function(downloadPath, resultingFileName, stallInterface, callback) {

    //todo do not put all files in the same directory; there can be collisions of filenames

    if (this.context.stall.my.config.connectionType == 'nearby') {
        this.context.stall.nearbyComms.requestFile(
            stallInterface.host,
            stallInterface.sharedKey,
            downloadPath,
            this.clientSideStallCacheDirectoryCachePath,
            resultingFileName,
            callback
        );
    } else if (this.context.stall.my.config.connectionType == 'wifi') {
        this.httpDownloadFileFromStall(downloadPath, resultingFileName, stallInterface, callback);
    } else if (this.context.stall.my.config.connectionType == 'wifidirect') {
        this.context.stall.wifiDirectComms.requestFile(
            stallInterface,
            downloadPath,
            this.clientSideStallCacheDirectoryCachePath,
            resultingFileName,
            callback
        );


    } else {
        throw new Error('unexpected `connectionType`');
    }
};



DownloadHelper.prototype.httpDownloadFileFromStall = function(downloadPath, resultingFileName, stallInterface, callback) {

    let resultingFilePath = this.clientSideStallCacheDirectoryCachePath + path.sep + resultingFileName;

    const BYTE_BUFFER_SIZE = 2048;


    const options = {
        hostname: stallInterface.host,
        port: stallInterface.port,
        path: `${downloadPath}?pki=${this.context.stall.my.config.pkiKey}`,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    const req = http.request(options, res => {
        //make the file in the cache directory
        var writeStream = fs.createWriteStream(resultingFilePath);


        var saltString = res.headers['salt'];
        var extensionString = res.headers['extension'];

        let maskingHelper = this.provideNewMaskingHelper(
            stallInterface.sharedKey,
            saltString
        );

        var totalReceivedBytes = 0;
        res.on('data', d => {

            try {
                if (!writeStream) return;

                totalReceivedBytes += d.length;

                //decrypt the data
                let b0 = Buffer.alloc(d.length); //todo is alloc necessary?
                d.copy(b0, 0, 0, d.length);

                xor(b0, maskingHelper.next(d.length).value);
                writeStream.write(b0);


            } catch (e) {
                console.log(e);

                //download failed
                writeStream.end();
                writeStream = null;

                callback(false);

            }
        });

        res.on('close', () => {
            console.log('..........totalReceivedBytes: ' + totalReceivedBytes);
            if (writeStream) {
                writeStream.end();
                writeStream = null;
                //download finished successfully

                //rename the downloaded file to have an extension
                fsPromises.rename(
                    resultingFilePath,
                    resultingFilePath + '.' + extensionString
                ).then((value) => {
                    callback(true, resultingFileName + '.' + extensionString);

                }).catch((err) => {
                    console.log(err);
                });

            } else {
                callback(false);
            }
        });


    });

    req.on('error', error => {
        console.error(error);

        if (writeStream) {
            writeStream.end();
            writeStream = null;

            //download failed
            callback(false);
        }

    })


    req.end()

};




DownloadHelper.prototype.getFile = function(downloadPath, stallInterface) {
    return new Promise((resolve, reject) => {
        let fileName = downloadPath
            .replace(new RegExp('[/]','g'), '')
            .replace(new RegExp('[.]','g'), '')
            + new Date().getTime();

        this.downloadedFileCache.set(downloadPath, fileName);


        this.downloadFileFromStall(
            downloadPath,
            fileName,
            stallInterface,
            (isSuccess, finalFileName) => {
                if (isSuccess) {
                    let finalPath = this.clientSideStallCacheDirectoryCachePath + path.sep + finalFileName;
                    this.downloadedFileCache.set(downloadPath, finalPath)
                    resolve(finalPath)

                } else {
                    //the half-downloaded file must be removed
                    this.downloadedFileCache.del(downloadPath);

                    reject();
                }
            }
        );


    });
};