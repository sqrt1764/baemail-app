'use strict';

const { createServer } = require('http');
const WebSocket = require('ws');
const queryString = require('query-string');


const fs = require('fs');
const fsPromises = require('fs').promises;
const path = require('path');


const xor = require('buffer-xor/inplace');
const getMaskingSecretGenerator = require('./masking-secret-generator');


const { StringDecoder } = require('string_decoder');


//https://medium.com/hackernoon/implementing-a-websocket-server-with-node-js-d9b78ec5ffa8

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function calculateSharedKey(bsv, privateKeyWif, publicKeyHex) {
    var privateKey = bsv.PrivateKey.fromWIF(privateKeyWif);
    var publicKey = bsv.PublicKey.fromHex(publicKeyHex);
    return publicKey.point.mul(privateKey.bn).toHex();
}


//this function #ends the response
async function handleFileRequest (context, response, url, sharedKey) {
    //todo parse & figure out the file that is requested
    //////todo /file?pki=${this.getMyConfig().pkiKey}


    let appDir = path.dirname(require.main.filename);
    let projectParentDir = path.resolve(appDir, '..');

    let stallDir = projectParentDir + path.sep + 'Stall';


    //todo locate the file
    let bundledImageDestinationPath = stallDir + path.sep + 'teehee.jpg';
    var fileStats = fs.statSync(bundledImageDestinationPath)
    var fileSizeInBytes = fileStats.size;


    const salt = new Date().getTime() + "";

    let respondWithError = () => {
        response.writeHead(401);
        response.end();
    };





    var fileHandle;
    try {
        fileHandle = await fsPromises.open(bundledImageDestinationPath, 'r');

        console.log('........file opened ... starting of streaming to response ..... size:' + fileSizeInBytes);
        response.writeHead(200, {
            'Content-Type': 'application/octet-stream',
            'extension': 'jpg',
            'salt': salt
        });


        var Hash = context.bsv.crypto.Hash;


        var finalKey = Hash.sha256(Buffer.concat([
            Buffer.from(sharedKey, "hex"),
            Buffer.from(salt, "utf8")
        ]));
        const BYTE_BUFFER_SIZE = 2048; //, finalKeyBuffer.length;


        var maskingHelper = getMaskingSecretGenerator(
            finalKey,
            (maskingChunkBuffer) => {
                return Hash.sha256(maskingChunkBuffer);
            }
        );


        var buffer = Buffer.alloc(BYTE_BUFFER_SIZE);
        var totalRead = 0;


        response.on('close', () => {
            if (fileHandle) {
                fileHandle.close();
            }
        });

        while(true) {
            var readResult = await fileHandle.read(buffer, 0, buffer.length, totalRead);
            totalRead += readResult.bytesRead;

            if (readResult.bytesRead == 0) break;


            let b0 = Buffer.alloc(readResult.bytesRead);
            buffer.copy(b0, 0, 0, readResult.bytesRead);

            //encrypt file
            xor(b0, maskingHelper.next(readResult.bytesRead).value);


            response.write(b0);
        }

        console.log('.......completed');

        response.end();



    } catch (e) {
        console.log(e);
        respondWithError();

    }

}









function startStallServer(context, my, port) {

    const server = createServer();
    const wss = new WebSocket.Server({ noServer: true });

    server.on('upgrade', function upgrade(request, socket, head) {
        wss.handleUpgrade(request, socket, head, function done(ws) {
            wss.emit('connection', ws, request);
        });
    });





    // Listen to the request event
    server.on('request', (req, res) => {
        var reqQuery = req.url.substring(req.url.indexOf('?'));
        var parsedQuery = queryString.parse(reqQuery);

        let shouldReturn401 = false;

        var clientPki = parsedQuery['pki'];
        if (!clientPki || clientPki.length < 32) {
            console.log('...............clientPki is missing ');
            console.log(`stall request is missing the expected 'pki' query-param; drop the connection`);
            shouldReturn401 = true;
        }

        if (req.method != 'GET') {
            console.log('...............unexpected request type');
            shouldReturn401 = true;
        }

        if (shouldReturn401) {
            res.writeHead(401, { 'Content-Type': 'text/plain' });
            res.end();
            return;
        }

        const sharedKey = calculateSharedKey(context.bsv, my.config.pkiPrivateKey, clientPki);


        setTimeout(async () => {
            try {
                await handleFileRequest(context, res, req.url, sharedKey);

            } catch (e) {
                console.log('.....aaaaaand crash', e);

                res.writeHead(404, { 'Content-Type': 'text/plain' });
                res.end();
            }
        }, 1);




    });


    ///////////////////////////////////////////////////////////////////

    my.wss = wss;
    my.server = server;

    const clients = {};
    my.clients = clients;

    my.stopStall = (callback) => {
        let wss = my.wss;
        let server = my.server;
        my.wss = null;
        my.server = null;

        let clients = my.clients;


        //release
        Object.keys(clients).forEach((key) => {
            let clientInterface = clients[key];

            try { clientInterface.ws.close(); } catch (e) { console.log(e); }
        });

        let onClosed = () => {
            my.stopStall = null;
            callback();
        }

        if (wss) {
            console.log('about to close the stall-server')
            server.close(() => {
                console.log('...executing close-callback of the stall-server')
                onClosed()
            })
            wss.close(() => {
                console.log('...executing close-callback of the stall-web-socket-server')
                //onClosed()
            })
        } else {
            onClosed()
        }

    };


    wss.on('connection', (ws, req) => {
        const id = uuidv4(); //todo why not remove this?

        var reqQuery = req.url.substring(req.url.indexOf('?'))
        var parsedQuery = queryString.parse(reqQuery)

        //check for the expected pki-key
        var clientPki = parsedQuery['pki']
        if (!clientPki || clientPki.length < 32) {
            console.log(`stall-client web-socket is missing the expected 'pki' query-param; drop the connection`)
            ws.close()
            return
        }

        const sharedKey = calculateSharedKey(context.bsv, my.config.pkiPrivateKey, clientPki);

        const clientInterface = {
            sharedKey: sharedKey,
            id: id,
            pki: clientPki,
            ws: ws,
            sendEncrypted: null, //initialized later
            onDisconnected: null //initialized later
        };

        const encryptionHelper = context.helper.getNewEncryptionHelper(sharedKey);

        clientInterface.sendEncrypted = function(message) {
            let encryptedMessage = encryptionHelper.encrypt(message);
            ws.send(encryptedMessage);
        };


        console.log(`new connection to my stall; clientPki:${clientPki}`)



        ws.on('message', (messageAsString) => {
            try {
                const decryptedMessage = encryptionHelper.decrypt(messageAsString);

                my.handleClientMessage(
                    clientInterface,
                    decryptedMessage
                )
            } catch (e) {
                console.log(e)
            }
        })

        ws.on('close', () => {
            try {
                console.log('a client disconnected from the stall; client-pk:' + clientInterface.pki);
            } catch (e) {
                console.log(e);
            }

            clientInterface.onDisconnected();
        });


        my.onClientConnected(clientInterface);


    })




    server.listen(port);
    console.log('started a web-socket server representing my stall');



}

module.exports = startStallServer;






