
const fs = require('fs');
const path = require('path');

const DownloadHelper = require('./download-helper');

const appDir = path.dirname(require.main.filename);
const bundledStaticDir = appDir + path.sep + 'static';
const bundledPlaceholderImagePath = bundledStaticDir + path.sep + 'placeholder.jpg';



function SessionManager(context, getMyConfig) {
    this.context = context;
    this.getMyConfig = getMyConfig;
    this.tracked = new Map();
    this.downloadHelper = new DownloadHelper(this.context);

}

module.exports = SessionManager;





SessionManager.prototype.onStateChanged = function(stallInterfaceArray) {
    console.log('placeholder implementation of SessionManager#onStateChanged');
};





SessionManager.prototype.add = function(stallInterface) {
    let invalidateLayout = () => {
        this.onStateChanged([stallInterface]);
    };

    this.tracked.set(
        stallInterface,
        new Session(
            this.context,
            this.downloadHelper,
            this.getMyConfig,
            stallInterface,
            invalidateLayout
        )
    );


    this.onStateChanged([stallInterface]);
};



SessionManager.prototype.remove = function(stallInterface) {
    this.tracked.delete(stallInterface);


    this.onStateChanged([stallInterface])
};



/*
Returns JSON that contains the current interface-layout for the specific stall-connection
*/
SessionManager.prototype.getLayoutState = function(stallInterface) {
    var session = this.tracked.get(stallInterface);
    if (session == null) {
        return {
            version: 0,
            items: [
                {
                    type: "text",
                    style: "body",
                    value: "Session is missing."
                }
            ]
        };
    }

    return session.getCurrentLayout();
}





//////////////////////////

// holds the layout for a specific stall-interface
function Session(context, downloadHelper, getMyConfig, stallInterface, invalidateLayout) {
    this.context = context;
    this.downloadHelper = downloadHelper;
    this.getMyConfig = getMyConfig;

    stallInterface.session = this;
    this.stallInterface = stallInterface;

    this.millisConnected = new Date().getTime();
    this.invalidateLayout = invalidateLayout; //call this to get the android-side to display to very latest layout of this session .... #getCurrentLayout

    this.getRootLayout = () => {
        let connectedDateLabel = new Date(this.millisConnected).toJSON();

        return {
            version: 0,
            items: [
                {
                    type: "button_disconnect_from_stall",
                    label: stallInterface.title,
                    labelStyle: "h2",
                    buttonLabel: "Leave"
                },
                {
                    type: "gap_0",
                    size: 1.7
                },
                {
                    type: "text",
                    style: "body",
                    value: "Greetings, you are now communicating directly with a nearby device using WiFiDirect. The communication is encrypted. Communicate with this peer by sending prepared/supported commands to it.\n\nMaybe you could think of this as like a chat-bot-like communication, or interaction with a smart-watch (trying to get that vibe)."
                },
                {
                    type: 'gap_0',
                    size: 3.7
                },
                {
                    type: "button",
                    value: `
                    //console.log('user clicked INFO; stall:${stallInterface.title}');
                        let stallInterface = context.stall.peers.connected['${stallInterface.stallName}'];
                        if (!stallInterface) {
                            console.log('...stall not found');
                            return;
                        }
                        let currentSession = stallInterface.session;

                        currentSession.showRemoteLayout("/welcome");
                    `,
                    label: "Welcome"
                },
                {
                    type: "button",
                    value: `
                       console.log('user clicked RING RING; stall:${stallInterface.title}');
                       let stallInterface = context.stall.peers.connected['${stallInterface.stallName}'];
                       if (!stallInterface) {
                           console.log('...stall not found');
                           return;
                       }
                       let currentSession = stallInterface.session;

                       currentSession.onRingRingClicked();
                   `,
                    label: "Ring Ring"
                },
                {
                    type: "button",
                    value: `console.log('user clicked REQUEST SERVICE; stall:${stallInterface.title}');`,
                    label: "Request IRL service"
                },
                {
                    type: 'gap_0',
                    size: 3.7
                },
                {
                    type: "text",
                    style: "body",
                    value: `Time session connected: ${connectedDateLabel}`
                },
                {
                    type: "gap_0",
                    size: 1.7
                }
           ]
       };
    };

    this.getCurrentLayout = this.getRootLayout;



    this.handleRemoteButtonLink = (link) => {
        if (link.length == 0) {
            this.getCurrentLayout = this.getRootLayout;
            this.invalidateLayout();

            return
        }

        if ('?' == link.charAt(0)) {
            this.getCurrentLayout = this.getRootLayout;
            this.invalidateLayout();

            return
        }

        console.log('Session#handleRemoteButtonLink ... executing #showRemoteLayout ' + link);
        setTimeout(() => {
            this.showRemoteLayout(link);
        },1);
    };


    this.transformIntoLocalLayout = (remoteLayout) => {

        var assembledLayout = [];

        remoteLayout.forEach((remoteComponent) => {
            if (remoteComponent.type == 'text') {
                assembledLayout.push({
                    type: 'text',
                    style: remoteComponent.style,
                    value: remoteComponent.value
                });
            } else if (remoteComponent.type == 'gap_0') {
                assembledLayout.push({
                    type: 'gap_0',
                    size: remoteComponent.size
                });
            } else if (remoteComponent.type == 'progress_0') {
                assembledLayout.push({
                    type: 'progress_0',
                    size: remoteComponent.size
                });
            } else if (remoteComponent.type == 'toolbar') {
                let stallName = stallInterface.stallName;

                assembledLayout.push({
                    type: 'toolbar',
                    label: remoteComponent.label,
                    labelStyle: remoteComponent.labelStyle,
                    buttonValue: `
                        console.log('click of buttonLink: ${stallName}');
                        let stallInterface = context.stall.peers.connected['${stallName}'];
                        if (!stallInterface) {
                            console.log('...stall not found; redirecting to root');
                            currentSession.handleRemoteButtonLink('?root');
                            return;
                        }
                        let currentSession = stallInterface.session;
                        currentSession.handleRemoteButtonLink('${remoteComponent.buttonLink}');
                    `
                });


            } else if (remoteComponent.type == 'button') {
                let stallName = stallInterface.stallName;

                assembledLayout.push({
                    type: 'button',
                    label: remoteComponent.label,
                    value: `
                        console.log('click of buttonLink: ${stallName}');
                        let stallInterface = context.stall.peers.connected['${stallName}'];
                        if (!stallInterface) {
                            console.log('...stall not found; redirecting to root');
                            currentSession.handleRemoteButtonLink('?root');
                            return;
                        }
                        let currentSession = stallInterface.session;
                        currentSession.handleRemoteButtonLink('${remoteComponent.buttonLink}');
                    `
                });

            } else if (remoteComponent.type == 'image') {
                assembledLayout.push({
                    type: 'image',
                    caption: remoteComponent.caption,
                    showCaption: remoteComponent.showCaption,
                    downloadPath: remoteComponent.downloadPath,
                    filePath: undefined
                });

            } else {
                assembledLayout.push({
                    type: 'text',
                    style: 'h2',
                    value: 'ERROR TYPE NOT RECOGNIZED: ' + remoteComponent.type
                });
            }
        });



        return {
            version: 0,
            items: assembledLayout
        };

    };

    //////

    this.showRemoteLayout = (link) => {
        //console.log('showRemoteLayout.............');


        var callbackGetLayout = (status, remoteLayout) => {
            if (!status) {
                console.log('response to -- getLayoutClientSideRequest... success:' + status + ' ERROR, NAVIGATING TO ROOT');
                currentSession.handleRemoteButtonLink("?root");
                return;
            }

            //convert `remoteLayout` into `localLayout` which is displayed in the stall-item in the discover screen
            let localLayout = this.transformIntoLocalLayout(remoteLayout);


            //find images that are not available in the cache
            let newImageDownloadPaths = localLayout.items.filter((item) => {
                if (item.type == 'image') return true;
                return false;
            }).map((item) => {
                return item.downloadPath;
            }).filter((item) => {
                return this.downloadHelper.downloadedFileCache.get(item) == undefined;
            });



            //add placeholder-file-paths for image-files yet to be downloaded images
            localLayout.items.forEach((item) => {
                if (item.type == 'image') {
                    if (newImageDownloadPaths.includes(item.downloadPath)) {
                        item.filePath = bundledPlaceholderImagePath
                    } else {
                        let cachedImagePath = this.downloadHelper.downloadedFileCache.get(item.downloadPath)
                        item.filePath = cachedImagePath
                    }
                }
            })

            //update the stall-component inside of the app
            this.getCurrentLayout = () => {
                return localLayout;
            }
            this.invalidateLayout();



            if (newImageDownloadPaths.length == 0) {
                return;
            }



            let fileDownloadingPromiseArray = newImageDownloadPaths.map((item) => {
                return new Promise((resolve, reject) => {
                    setTimeout(async () => {
                        await this.downloadHelper.getFile(item, stallInterface);
                        resolve();
                    }, 1);
                });
            });
            Promise.all(fileDownloadingPromiseArray).then((values) => {
                //success
                //must update the images in `localLayout` so they point at the correct file-paths
                let layout = this.getCurrentLayout();
                layout.items.forEach((item) => {
                    if (item.type == 'image') {
                        item.filePath = this.downloadHelper.downloadedFileCache.get(item.downloadPath);
                    }
                });


                //update the stall-component inside of the app
                this.getCurrentLayout = () => {
                    return layout;
                }
                this.invalidateLayout();



            }).catch(error => {
                //todo should there be an error layout that is shown?
                console.error(error.message);

            });
        };

        var workRequest = this.stallInterface.requestManager.forwardRequests.requests
                .getLayoutClientSideRequest.buildRequest(link, callbackGetLayout);

        this.stallInterface.requestManager.forwardRequests.send(workRequest);

    };


    this.onRingRingClicked = () => {
        console.log('onRingRingClicked.............');
    };




};

