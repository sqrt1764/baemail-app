'use strict';


function GetLayout(context) {
    this.context = context
    this.command = "GET_LAYOUT"

}

module.exports = GetLayout;



/*

# expected structure of params
[
    {
        link: "/welcome",
    }
]


# expected structure of the response data passed in the callback
[
    {
        layout: [..]
    }
]






///////////////
///////////////
///////////////
///////////////
supported layout-items

// Button-links that start with a `/` get redirected to this request; links that start with `?`
// are handled without communication with the stall.

// buttonLink example: '/welcome'
// buttonLink example: '/welcome/intro?page=0'
// buttonLink example: '?root'
// buttonLink example: '?leave'

{
   type: "text",
   style: "body",
   value: "Loading info; requesting it from a 'stall' hosted by this peer device."
}
// body/h1/h2/caption


{
   type: "gap_0",
   size: 0.7
}


{
   type: "progress_0",
   size: 0.6
}

{
    type: "button",
    buttonLink: `/welcome/intro?page=0`,
    label: "Request IRL service"
}

{
    type: "toolbar",
    label: "Welcome",
    labelStyle: 'h2',
    buttonLink: '/welcome/intro?page=0'
}
// a toolbar with a `back` button on the left


//todo
//todo
{
   type: "image",
   caption: "photo of product from top",
   downloadPath: "/file/00fff3333abcdef0123456789ffff",
   showCaption: true,
}


//todo add a horizontally scrolling button group




! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!!!!!!!! NEVER eval(..) A STRING RECEIVED FROM THE STALL



*/
GetLayout.prototype.handle = function(params, callback) {

    var isSuccess = false

    var responseData = [
        null
    ];

    let errorLayout = [
        {
            type: "toolbar",
            label: "ERROR",
            labelStyle: 'h2',
            buttonLink: '?root'
        },
        {
           type: "gap_0",
           size: 1.7
        },
        {
           type: "text",
           style: "body",
           value: "ERROR RECEIVED INVALID LAYOUT"
        }
    ];

    try {

        var runAsync = async () => {

            try {

                let requestConfig = params[0];
                let link = requestConfig.link;

                var finalLayout = errorLayout;

                if ("/welcome" == link) {
                    finalLayout = [
                        {
                            type: "toolbar",
                            label: "Welcome",
                            labelStyle: 'h2',
                            buttonLink: '?root'
                        },
                        {
                            type: "gap_0",
                            size: 1.7
                        },
                        {
                            type: "text",
                            style: "body",
                            value: this.context.stall.my.util.getStallInfo()
                        },
                        {
                           type: "gap_0",
                           size: 1.7
                        },
                        {
                            type: "button",
                            label: "About products",
                            buttonLink: '/products'
                        },
                        {
                            type: "gap_0",
                            size: 1.7
                        }
                    ];

                } else if ("/products" == link) {
                    finalLayout = [
                        {
                            type: "toolbar",
                            label: "About products",
                            labelStyle: 'h2',
                            buttonLink: '/welcome'
                        },
                        {
                            type: "gap_0",
                            size: 1.7
                        },
                        {
                            type: "text",
                            style: "body",
                            value: "Placeholder: Squishy product warning!"
                        },
                        {
                            type: "gap_0",
                            size: 2.5
                        },
                        {
                           type: "image",
                           showCaption: true,
                           caption: "photo of product from top",
                           downloadPath: "/file/teehee"
                        },
                        {
                            type: "text",
                            style: "body",
                            value: "!!!!! ???? @@@@@ ######## $$$$$$$ ******** >>>>>>> //////// &&&&&&&&& !!!!! ???? @@@@@ ######## $$$$$$$ ******** >>>>>>> //////// &&&&&&&&& !!!!! ???? @@@@@ ######## $$$$$$$ "
                        },
                        {
                            type: "gap_0",
                            size: 3.5
                        },
                        {
                            type: "text",
                            style: "caption",
                            value: "!!!!! ???? @@@@@ ######## $$$$$$$ ******** >>>>>>> //////// &&&&&&&&& !!!!! ???? @@@@@ ######## $$$$$$$ ******** >>>>>>> //////// &&&&&&&&& !!!!! ???? @@@@@ ######## $$$$$$$ "
                        },
                        {
                           type: "gap_0",
                           size: 1.7
                        },
                        {
                            type: "button",
                            label: "Next",
                            buttonLink: '/products1'
                        }
                    ];

                } else if ("/products1" == link) {
                    finalLayout = [
                        {
                            type: "toolbar",
                            label: "About products Pg1",
                            labelStyle: 'h2',
                            buttonLink: '/products'
                        },
                        {
                            type: "gap_0",
                            size: 1.7
                        },
                        {
                            type: "text",
                            style: "body",
                            value: "!!!!! ???? @@@@@ ######## $$$$$$$ ******** >>>>>>> //////// &&&&&&&&& !!!!! ???? @@@@@ ######## $$$$$$$ ******** >>>>>>> //////// &&&&&&&&& !!!!! ???? @@@@@ ######## $$$$$$$ ******** >>>>>>> //////// &&&&&&&&& "
                        },
                        {
                           type: "gap_0",
                           size: 3.7
                        },
                        {
                            type: "text",
                            style: "h2",
                            value: "Navigate"
                        },
                        {
                            type: "button",
                            label: "To welcome",
                            buttonLink: '/welcome'
                        },
                        {
                            type: "button",
                            label: "To start",
                            buttonLink: '?root'
                        }
                    ];

                } else {
                    finalLayout = [
                        {
                            type: "toolbar",
                            label: "Error",
                            labelStyle: 'h2',
                            buttonLink: '?root'
                        },
                        {
                            type: "gap_0",
                            size: 1.7
                        },
                        {
                            type: "text",
                            style: "body",
                            value: "Link not recognized: " + link
                        },
                        {
                            type: "gap_0",
                            size: 1.7
                        }
                    ];
                }



//                callback(true, [this.context.stall.my.util.getStallInfo()]);
                callback(true, [{
                    layout: finalLayout
                }]);



            } catch (err) {
                //todo sending the stack-trace to the other device is probably not smart
                responseData = [
                    err.stack
                ]
                console.log(err)

                callback(false, responseData)
            }

        }


        setTimeout(runAsync, 1)

    } catch (e) {
        responseData = [
            e.stack
        ];

        callback(false, responseData);
    }
};