

//calculateNextBytes: (Buffer)->Buffer

//function assumes that each masking chunk of bytes will be the length of the secret
// which was used to initialize the generator
function getMaskingSecretGenerator(secret, calculateNextMaskingBytes) {

    var currentSecret = secret;
    if (typeof currentSecret == "string") {
        currentSecret = Buffer.from(currentSecret, 'hex');
    }

    var positionCurrentlyMasked = 0;
    const chunkSize = currentSecret.length;

    var maskedChunkBuffer = [];

    let popFirstBytesFromChunkBuffer = (size) => {
        var s = 0;
        var requiredChunks = maskedChunkBuffer.filter((item) => {
            if (s >= size) {
                return false;
            }
            s = s + item.length;

            return true;
        });

        if (s < size) throw new Error("....unexpected usage");

        var updatedLastChunkBuffer = null;
        var newLastChunkForRequestedSize = null;

        let extra = s - size;

        if (s > size) {
            //last of required chunks needs resizing
            let lastChunkBuffer = requiredChunks[requiredChunks.length - 1];
            updatedLastChunkBuffer = Buffer.alloc(extra);
            lastChunkBuffer.copy(updatedLastChunkBuffer, 0, lastChunkBuffer.length - extra);
            newLastChunkForRequestedSize = Buffer.alloc(lastChunkBuffer.length - extra);
            lastChunkBuffer.copy(newLastChunkForRequestedSize, 0, 0, lastChunkBuffer.length - extra);
        }

        //popping is only requiring a part of the last chunk
        if (newLastChunkForRequestedSize != null) {
            requiredChunks.splice(requiredChunks.length - 1, 1, newLastChunkForRequestedSize);
        }

        //update the chunk-holder-array
        if (updatedLastChunkBuffer != null) {
            maskedChunkBuffer.splice(0, requiredChunks.length, updatedLastChunkBuffer);
        } else {
            maskedChunkBuffer.splice(0, requiredChunks.length);
        }

        return Buffer.concat(requiredChunks);
    };

    let requestChunk = (size) => {
        if ('number' != typeof(size)) return null;

        while (true) {
            var currentBufferSize = maskedChunkBuffer.reduce((acc, item) => {
                return acc + item.length;
            }, 0);

            if (currentBufferSize >= size) {
                return popFirstBytesFromChunkBuffer(size);
            }


            //todo increase efficiency -- multiple chunks at the same time
            currentSecret = calculateNextMaskingBytes(currentSecret);
            maskedChunkBuffer.push(currentSecret);

        }
    }

    function* fetchRequestedChunk() {
        var requestedSize = null;
        while (true) {
            var requestedSize = yield requestChunk(requestedSize);
        }
    };
    let genny = fetchRequestedChunk();
    //init the genny for proper usage (with a parameter)
    genny.next()



    return genny;
}


module.exports = getMaskingSecretGenerator;