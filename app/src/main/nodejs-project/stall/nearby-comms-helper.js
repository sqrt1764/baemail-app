'use strict';

const { createServer } = require('http');
const queryString = require('query-string');

const { Readable } = require("stream");

const xor = require('buffer-xor/inplace');
const getMaskingSecretGenerator = require('./masking-secret-generator');
const { StringDecoder } = require('string_decoder');

const fs = require('fs');
const fsPromises = require('fs').promises;
const path = require('path');


function NearbyCommsHelper(stall) {
    this.context = stall.context;
    this.stall = stall;
    this.isActive = false;
    this.server = {};
    this.queuedMessages = {};
    this.expectingFiles = {};

    this.isAdvertisingStall = false;
};

module.exports = NearbyCommsHelper;




function calculateSharedKey(bsv, privateKeyWif, publicKeyHex) {
    var privateKey = bsv.PrivateKey.fromWIF(privateKeyWif);
    var publicKey = bsv.PublicKey.fromHex(publicKeyHex);
    return publicKey.point.mul(privateKey.bn).toHex();
}

function chunkSubstr(str, size) {
    const numChunks = Math.ceil(str.length / size);
    const chunks = new Array(numChunks);

    for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
        chunks[i] = str.substr(o, size);
    }

    return chunks;
}


async function transferMessagePayloadToAndroid(context, response, meta) {
    const message = meta.message;

    let respondWithError = () => {
        response.writeHead(401);
        response.end();
    };

    try {

        response.writeHead(200, {
            'Content-Type': 'application/octet-stream'
        });


        const Hash = context.bsv.crypto.Hash;


        var finalKey = Hash.sha256(Buffer.concat([
            Buffer.from(meta.sharedKey, "hex"),
            Buffer.from(meta.salt, "utf8")
        ]));
        const BYTE_BUFFER_SIZE = 2048;


        var maskingHelper = getMaskingSecretGenerator(
            finalKey,
            (maskingChunkBuffer) => {
                return Hash.sha256(maskingChunkBuffer);
            }
        );


        var chunks = chunkSubstr(message, BYTE_BUFFER_SIZE);

        const decoder = new StringDecoder('base64');

        chunks.forEach((chunk) => {
            var b0 = Buffer.from(chunk);
            var maskBuffer = maskingHelper.next(b0.length).value;
            xor(b0, maskBuffer);

            response.write(decoder.write(b0));
        });

        response.write(decoder.end());
        response.end();



    } catch (e) {
        console.log(e);
        respondWithError();

    } finally {
        //releasing the message for GCing
        delete meta['message'];
        //todo more cleanup required

    }
}

async function transferMessagePayloadFromAndroid(context, request, response, endpointId, token, salt) {

    let respondWithError = () => {
        response.writeHead(401);
        response.end();
    };


    var isMessageFromStall = false;
    var matchingInterface = context.stall.my.clients[endpointId];
    if (!matchingInterface) {
        //is this endpoint matching a stall-interface?
        //todo this might not work if both are running stalls & interacting with each-other
        matchingInterface = Object.values(context.stall.peers.connected).find((item) => {
            return item.host == endpointId;
        });
        isMessageFromStall = true;
    }

    if (!matchingInterface) {
        console.log('.....no client or stall interfaces match endpointId:' + endpointId);
        respondWithError();
        return;
    }


    const Hash = context.bsv.crypto.Hash;

    var finalKey = Hash.sha256(Buffer.concat([
        Buffer.from(matchingInterface.sharedKey, "hex"),
        Buffer.from(salt, "utf8")
    ]));

//    console.log(".......................finalKey: " + finalKey.toString('hex')); //todo rm
    var maskingHelper = getMaskingSecretGenerator(
        finalKey,
        (maskingChunkBuffer) => {
            return Hash.sha256(maskingChunkBuffer);
        }
    );



    const BYTE_BUFFER_SIZE = 2048;
    const chunks = [];

    request.on('data', chunk => {
        let d = Buffer.from(chunk.toString('utf8'), 'base64')

        try {
            //decrypt the data
            var maskBuffer = maskingHelper.next(d.length).value;
            xor(d, maskBuffer);

            chunks.push(d);

        } catch (e) {
            console.log(e);
            respondWithError();

        }
    });


    request.on('end', () => {
        response.writeHead(200);
        response.end();
    });

    request.on('close', () => {
        const decoder = new StringDecoder();
        matchingInterface.onMessageDecrypted(
            decoder.write(Buffer.concat(chunks)) + decoder.end()
        );

    });

}



async function respondWithFile(context, sharedKey, response, stringMessage) {
    console.log('...respondWithFile .. onFileRequestPayloadDecrypted: ' + stringMessage);
    //expected stringMessage in format ... {fileResourceUri: 'abc01ABC!!'}
    let message = JSON.parse(stringMessage);
    let fileResourceUri = message.fileResourceUri; //todo currently unused
    //todo must respond with error if requested file not found

    const Hash = context.bsv.crypto.Hash;

    //todo ... figure out the actual file-path
    //todo ...
    let appDir = path.dirname(require.main.filename);
    let projectParentDir = path.resolve(appDir, '..');
    let stallDir = projectParentDir + path.sep + 'Stall';

    let fileName = 'teehee.jpg';
    let filePath = stallDir + path.sep + fileName; //todo placeholder

    //helper function that masks a string
    let encryptString = (plainString, sharedKey, salt) => {
        var finalKey = Hash.sha256(Buffer.concat([
            Buffer.from(sharedKey, 'hex'),
            Buffer.from(salt, 'utf8')
        ]));
        var maskingHelper = getMaskingSecretGenerator(
            finalKey,
            (maskingChunkBuffer) => {
                return Hash.sha256(maskingChunkBuffer);
            }
        );

        var b0 = Buffer.from(plainString);
        var maskBuffer = maskingHelper.next(b0.length).value;
        xor(b0, maskBuffer);
        const decoder = new StringDecoder('base64');
        return decoder.write(b0) + decoder.end();
    };

    let plainMetaString = JSON.stringify({
        extension: fileName.substring(fileName.lastIndexOf('.') + 1)
    });
    let saltOfMeta = new Date().getTime() + '';
    let maskedMetaString = encryptString(plainMetaString, sharedKey, saltOfMeta);



    let saltOfStream = new Date().getTime() + '';

    let finalKeyOfStream = Hash.sha256(Buffer.concat([
        Buffer.from(sharedKey, 'hex'),
        Buffer.from(saltOfStream, 'utf8')
    ]));
    let streamMaskingHelper = getMaskingSecretGenerator(
        finalKeyOfStream,
        (maskingChunkBuffer) => {
            return Hash.sha256(maskingChunkBuffer);
        }
    );

    //
    //start masking & piping the file into the http-response
    response.writeHead(200, {
        'content-type': 'application/octet-stream',
        'salt-of-meta': saltOfMeta,
        'salt-of-stream': saltOfStream,
        'meta': maskedMetaString,
    });

    const BYTE_BUFFER_SIZE = 2048;

    var buffer = Buffer.alloc(BYTE_BUFFER_SIZE);
    var totalRead = 0;

    let fileHandle = await fsPromises.open(filePath, 'r');
    const decoder = new StringDecoder('base64');

    while(true) {
        var readResult = await fileHandle.read(buffer, 0, buffer.length, totalRead);
        totalRead += readResult.bytesRead;

        if (readResult.bytesRead == 0) break;

        let b0 = Buffer.alloc(readResult.bytesRead); //todo is the alloc necessary?
        buffer.copy(b0, 0, 0, readResult.bytesRead);

        //encrypt file
        xor(b0, streamMaskingHelper.next(readResult.bytesRead).value);

        //writeStream.write(b0);
        response.write(decoder.write(b0));
    }

    response.write(decoder.end());
    response.end();

    console.log('completed piping the file into the response; read:' + totalRead + ' path:' + filePath);
};


async function handleFileRequest(context, request, response, endpointId, salt) {
    let respondWithError = () => {
        response.writeHead(401);
        response.end();
    };

    var isMessageFromStall = false;
    var matchingInterface = context.stall.my.clients[endpointId];
    if (!matchingInterface) {
        //is this endpoint matching a stall-interface?
        //todo this might not work if both are running stalls & interacting with each-other
        matchingInterface = Object.values(context.stall.peers.connected).find((item) => {
            return item.host == endpointId;
        });
        isMessageFromStall = true;
    }

    if (!matchingInterface) {
        console.log('.....no client or stall interfaces match endpointId:' + endpointId);
        respondWithError();
        return;
    }


    const Hash = context.bsv.crypto.Hash;

    var finalKey = Hash.sha256(Buffer.concat([
        Buffer.from(matchingInterface.sharedKey, "hex"),
        Buffer.from(salt, "utf8")
    ]));
    const BYTE_BUFFER_SIZE = 2048;

//    console.log(".......................finalKey: " + finalKey.toString('hex')); //todo rm
    var maskingHelper = getMaskingSecretGenerator(
        finalKey,
        (maskingChunkBuffer) => {
            return Hash.sha256(maskingChunkBuffer);
        }
    );


    const chunks = [];
    request.on('data', chunk => {
        try {
            let d = Buffer.from(chunk.toString('utf8'), 'base64')
            //decrypt the data
            var maskBuffer = maskingHelper.next(d.length).value;
            xor(d, maskBuffer);

            chunks.push(d);

        } catch (e) {
            console.log(e);
            respondWithError();

        }
    });

    request.on('end', () => {
        console.log('...handleFileRequest end-event of payload');
        try {
            const decoder = new StringDecoder();
            const plainRequestData = decoder.write(Buffer.concat(chunks)) + decoder.end();
            respondWithFile(
                context,
                matchingInterface.sharedKey,
                response,
                plainRequestData
            );
        } catch (e) {
            console.log(e);
        }
    });

    request.on('close', () => {
        console.log('...handleFileRequest close-event of payload');

    });
}


async function handleFileResponse(context, request, response, downloadHelper, expectingFiles, endpoint, token) {
    let respondWithError = () => {
        response.writeHead(401);
        response.end();
    };

    //find a matching file-request event
    let expectingMeta = expectingFiles[token];
    if (!expectingMeta) {
        respondWithError();
        return;
    }


    var isMessageFromStall = false;
    var matchingInterface = context.stall.my.clients[endpoint];
    if (!matchingInterface) {
        //is this endpoint matching a stall-interface?
        //todo this might not work if both are running stalls & interacting with each-other
        matchingInterface = Object.values(context.stall.peers.connected).find((item) => {
            return item.host == endpoint;
        });
        isMessageFromStall = true;
    }

    if (!matchingInterface) {
        console.log('.....no client or stall interfaces match endpointId:' + endpoint);
        respondWithError();
        return;
    }

    //todo
    //todo
    //todo
    //todo
    let saltOfMeta = request.headers['salt-of-meta'];
    let saltOfStream = request.headers['salt-of-stream'];
    let meta = request.headers['meta'];


    //todo
    //todo
    const Hash = context.bsv.crypto.Hash;

    var finalKeyOfMeta = Hash.sha256(Buffer.concat([
        Buffer.from(matchingInterface.sharedKey, "hex"),
        Buffer.from(saltOfMeta, "utf8")
    ]));
    var maskingHelperOfMeta = getMaskingSecretGenerator(
        finalKeyOfMeta,
        (maskingChunkBuffer) => {
            return Hash.sha256(maskingChunkBuffer);
        }
    );

    let d = Buffer.from(meta, 'base64');
    var maskBuffer = maskingHelperOfMeta.next(d.length).value;
    xor(d, maskBuffer);

    const decoderForMeta = new StringDecoder();
    const plainMeta = decoderForMeta.write(d) + decoderForMeta.end();
    //expected structure of plainMeta `{extension: 'jpg'}`
    const metaObject = JSON.parse(plainMeta);
    const extension = metaObject.extension;
    //todo handle error if `plainMeta` is not in the expected format
    //todo


    //todo
    //todo
    //todo pipe data into the file
    let finalKeyOfStream = Hash.sha256(Buffer.concat([
        Buffer.from(matchingInterface.sharedKey, "hex"),
        Buffer.from(saltOfStream, "utf8")
    ]));
    let maskingHelperOfStream = getMaskingSecretGenerator(
        finalKeyOfStream,
        (maskingChunkBuffer) => {
            return Hash.sha256(maskingChunkBuffer);
        }
    );


    let finalFileName = expectingMeta.resultingFileName + '.' + extension;
    let finalPath = expectingMeta.destinationDirectory + path.sep + finalFileName;
    let writeStream = fs.createWriteStream(finalPath);

    const BYTE_BUFFER_SIZE = 2048;
    const chunks = [];

    request.on('data', chunk => {
        let d = Buffer.from(chunk.toString('utf8'), 'base64')

        try {
            //decrypt the data
            var maskBuffer = maskingHelperOfStream.next(d.length).value;
            xor(d, maskBuffer);

            writeStream.write(d);

        } catch (e) {
            console.log(e);
            respondWithError();

        }
    });


    request.on('end', () => {
        response.writeHead(200);
        response.end();
        writeStream.end();
        expectingMeta.onSuccess(finalFileName);
    });


    request.on('close', () => {
        console.log('...handleFileResponse close-event of payload');
    });

    //todo if there is a failure, then `expectingMeta` should be informed

};




NearbyCommsHelper.prototype.setupFileSending = function() {
    //todo
    //todo
    let downloadHelper = this.context.stall.peers.sessionManager.downloadHelper;
    this.provideNewMaskingHelper = downloadHelper.provideNewMaskingHelper;

    let appDir = path.dirname(require.main.filename);
    let projectParentDir = path.resolve(appDir, '..');

//    this.fileSendingTempDirectoryPath = projectParentDir + path.sep + 'nearby-file-send-temp';
//    fs.mkdirSync(this.fileSendingTempDirectoryPath, { recursive: true });

}


NearbyCommsHelper.prototype.setup = function() {
    this.setupFileSending();

    const server = createServer();


    //todo the metadata is unencrypted at this point (data itself is safe though)

    server.on('request', (req, res) => {
        console.log('....on-request url:' + req.url);
        var reqQuery = req.url.substring(req.url.indexOf('?'));
        var parsedQuery = queryString.parse(reqQuery);

        let authorityAndPath = req.url.substring(0, req.url.indexOf('?'));
        console.log('....authorityAndPath:' + authorityAndPath);

        let respondWith401 = () => {
            res.writeHead(401, { 'Content-Type': 'text/plain' });
            res.end();
        };

        var asyncHandler = async () => {};

        if (authorityAndPath.includes('messagePayload')) {
            if (req.method != 'GET') {
                console.log('...............unexpected request type');
                respondWith401();
                return;
            }

            let token = parsedQuery['token'];
            if (!token) {
                console.log('...............token is missing');
                respondWith401();
                return;
            }
            let gotMeta = this.queuedMessages[token];
            if (!gotMeta) {
                console.log('...............unexpected token');
                respondWith401();
                return;
            }

            asyncHandler = async () => {
                return await transferMessagePayloadToAndroid(this.context, res, gotMeta);
            };

        } else if (authorityAndPath.includes('messageRedirect')) {
            if (req.method != 'POST') {
                console.log('...............unexpected request type');
                respondWith401();
                return;
            }

            let token = parsedQuery['token'];
            let endpoint = parsedQuery['endpoint'];
            let salt = parsedQuery['salt'];
            if (!token || !endpoint || !salt) {
                console.log('...............invalid request-params');
                respondWith401();
                return;
            }

            asyncHandler = async () => {
                return await transferMessagePayloadFromAndroid(this.context, req, res, endpoint, token, salt);
            };

        } else if (authorityAndPath.includes('shortMessageRedirect')) {
            if (req.method != 'POST') {
                console.log('...............unexpected request type');
                respondWith401();
                return;
            }

            let token = parsedQuery['token'];
            let endpoint = parsedQuery['endpoint'];
            let salt = parsedQuery['salt'];
            if (!token || !endpoint || !salt) {
                console.log('...............invalid request-params');
                respondWith401();
                return;
            }

            asyncHandler = async () => {
                return await transferMessagePayloadFromAndroid(this.context, req, res, endpoint, token, salt);
            };

        } else if (authorityAndPath.includes('fileRequest')) {
            if (req.method != 'POST') {
                console.log('...............unexpected request type');
                respondWith401();
                return;
            }

            let endpoint = parsedQuery['endpoint'];
            let salt = parsedQuery['salt'];

            if (!endpoint || !salt) {
                console.log('...............invalid request-params');
                respondWith401();
                return;
            }

            asyncHandler = async () => {
                return await handleFileRequest(this.context, req, res, endpoint, salt);
            };

        } else if (authorityAndPath.includes('fileRedirect')) {
            if (req.method != 'POST') {
                console.log('...............unexpected request type');
                respondWith401();
                return;
            }

            let endpoint = parsedQuery['endpoint'];
            let token = parsedQuery['token'];

            if (!endpoint || !token) {
                console.log('...............invalid request-params');
                respondWith401();
                return;
            }

            asyncHandler = async () => {
                let downloadHelper = this.context.stall.peers.sessionManager.downloadHelper;
                return await handleFileResponse(
                    this.context,
                    req,
                    res,
                    downloadHelper,
                    this.expectingFiles,
                    endpoint,
                    token
                );
            };

        } else if (authorityAndPath.includes('errorResponse')) {
            if (req.method != 'GET') {
                console.log('...............unexpected request type');
                respondWith401();
                return;
            }

            let endpoint = parsedQuery['endpoint'];
            let token = parsedQuery['token'];
            let type = parsedQuery['type'];
            /*
            possible values of `type`
                'smallMessage' - sent when handing the message to the node-code in the destination-device returned an unsuccessful response-code
                'largeMessage' - sent when handing the message to the node-code in the destination-device returned an unsuccessful response-code
                'fileRequest' - sent when handing the message to the node-code in the destination-device returned an unsuccessful response-code
                'fileResponse' - sent when handing the message to the node-code in current returned an unsuccessful response-code
            */

            //todo handle this, cancel stuff ........
            //todo
            //todo
            //todo
            //todo
            //todo



        } else {
            console.log('...............unexpected path');
            respondWith401();
            return;
        }


        setTimeout(async () => {
            try {
                await asyncHandler();

            } catch (e) {
                console.log('.....aaaaaand crash', e);

                res.writeHead(404, { 'content-type': 'text/plain' });
                res.end();
            }
        }, 1);

    });


    this.server.instance = server;
    this.server.port = Math.floor(Math.random() * 10000 + 20000);

    server.listen(this.server.port);

    this.isActive = true;

    console.log('started a server for nearby-comms');



    //must inform the android-side of the port used
    var workRequest = this.context.requestManager.forwardRequests.requests.informP2pNearbyInfo
            .buildRequest('COMMS_SERVER_META', { port: this.server.port });

    this.context.requestManager.forwardRequests.send(workRequest);

}

NearbyCommsHelper.prototype.tearDown = function() {
    let server = this.server.instance;
    this.server.instance = null;
    this.server.port = null;

    this.isActive = false;

    server.close(() => {
        console.log('...nearby-comms server has been torn down');
    });
}


NearbyCommsHelper.prototype.onStartedAdvertisingStall = function() {
    this.isAdvertisingStall = true;
}

NearbyCommsHelper.prototype.onStoppedAdvertisingStall = function() {
    this.isAdvertisingStall = false;
}


NearbyCommsHelper.prototype.ensureNearbyCommsIsSetupAppropriately = function() {
    let isHostingStall = this.isAdvertisingStall; //this.stall.my.wss != null;
    let isDiscoveringStalls = this.stall.discovery.isActive;
    let isUsingNearby = this.stall.my.config.connectionType == "nearby";

    if (!isUsingNearby) {
        console.log('...ensureNearbyCommsIsSetupAppropriately DROPPED; not using nearby');
        return;
    }

    if (!isHostingStall && !isDiscoveringStalls) {
        console.log('...ensureNearbyCommsIsSetupAppropriately DROPPED; nearby-comms should be disabled');
        return;
    }

    if (this.isActive) {
        console.log('...ensureNearbyCommsIsSetupAppropriately DROPPED; already active');
        return;
    }
    this.setup();
}

NearbyCommsHelper.prototype.ensureNearbyCommsIsTornDownAppropriately = function() {
    let isHostingStall = this.isAdvertisingStall; //this.stall.my.wss != null;
    let isDiscoveringStalls = this.stall.discovery.isActive;
    let isUsingNearby = this.stall.my.config.connectionType == "nearby";

    if (!isUsingNearby) return;

    if (isHostingStall || isDiscoveringStalls) {
        //nearby-comms should be enabled
        return;
    }

    if (this.isActive) {
        this.tearDown();
    }
}


NearbyCommsHelper.prototype.sendMessage = function(endpointId, sharedKey, messageString) {
    let estimatedBase64Length = ((4 * messageString.length / 3) + 3) & ~3

    if (estimatedBase64Length > 28000) {
        this.sendLargeMessage(endpointId, sharedKey, messageString)
    } else {
        this.sendShortMessage(endpointId, sharedKey, messageString)
    }
}

NearbyCommsHelper.prototype.sendLargeMessage = function(endpointId, sharedKey, messageString) {
    console.log('...........sendLargeMessage');

    let token = endpointId + new Date().getTime();
    let salt = new Date().getTime() + "";
    let requestMeta = {
        endpointId: endpointId,
        sharedKey: sharedKey,
        token: token,
        message: messageString,
        salt: salt
    };
    //todo include file-extension for file-messages

    //todo queues message-handler
    this.queuedMessages[token] = requestMeta;


    //must inform the android-side
    var workRequest = this.context.requestManager.forwardRequests.requests.informP2pNearbyInfo
            .buildRequest(
                'REQUEST_MESSAGE_SEND',
                {
                    endpointId: endpointId,
                    token: token,
                    salt: salt,
                }
            );

    this.context.requestManager.forwardRequests.send(workRequest);
}




NearbyCommsHelper.prototype.sendShortMessage = function(endpointId, sharedKey, messageString) {
    console.log('...........sendShortMessage');

    let token = endpointId + new Date().getTime();
        let salt = new Date().getTime() + "";
    let maskingHelper = this.provideNewMaskingHelper(sharedKey, salt);


    let length = Buffer.byteLength(messageString, 'utf8');
    var b0 = Buffer.from(messageString, 'utf8');
    xor(b0, maskingHelper.next(length).value);

    const decoder = new StringDecoder('base64');
    let maskedString = decoder.write(b0) + decoder.end();
    b0 = null;

    if (maskedString.length > 29000) throw new Error("message too big");

    var workRequest = this.context.requestManager.forwardRequests.requests.informP2pNearbyInfo
            .buildRequest(
                'REQUEST_SEND_SHORT',
                {
                    endpointId: endpointId,
                    token: token,
                    salt: salt,
                    data: maskedString
                }
            );

    this.context.requestManager.forwardRequests.send(workRequest);

}





NearbyCommsHelper.prototype.requestFile = function(
    endpointId,
    sharedKey,
    fileResourceUri,
    destinationDirectory,
    resultingFileName,
    callback
) {
    console.log('...........requestFile fileResourceUri:' + fileResourceUri);

    let token = endpointId + new Date().getTime();

    //cache metadata, it will be necessary when the requested file arrives back
    let meta = {
        endpointId: endpointId,
        sharedKey: sharedKey,
        token: token,
        fileResourceUri: fileResourceUri,
        destinationDirectory: destinationDirectory,
        resultingFileName: resultingFileName,
        callback: callback,
        onSuccess: (filePath) => {
            console.log('...requestFile#onSuccess filePath:' + filePath)
            delete this.expectingFiles[token]

            callback(true, filePath)
        },
        onError: (error) => { //todo this is never called
            console.log('...requestFile#onError error:' + error);
            //todo
            //todo
        }
    };
    this.expectingFiles[token] = meta;

    //todo implement timeout for the download
    //todo
    //todo
    //todo
    //todo

    //construct the request-payload that will be passed to the node on the other device
    let plainMessageString = JSON.stringify({
        fileResourceUri: fileResourceUri
    });

    //encrypt the request-payload
    let salt = new Date().getTime() + "";
    let maskingHelper = this.provideNewMaskingHelper(sharedKey, salt);

    let length = Buffer.byteLength(plainMessageString, 'utf8');
    var b0 = Buffer.from(plainMessageString, 'utf8');
    xor(b0, maskingHelper.next(length).value);

    const decoder = new StringDecoder('base64');
    let maskedString = decoder.write(b0) + decoder.end();
    b0 = null;

    if (maskedString.length > 29000) throw new Error("message too big");

    //start the requesting
    var workRequest = this.context.requestManager.forwardRequests.requests.informP2pNearbyInfo
            .buildRequest(
                'REQUEST_RECEIVE_FILE',
                {
                    endpointId: endpointId,
                    token: token,
                    salt: salt,
                    data: maskedString
                }
            );

    this.context.requestManager.forwardRequests.send(workRequest);

}