const Net = require('net');
const xor = require('buffer-xor/inplace');
const { StringDecoder } = require('string_decoder');
const getMaskingSecretGenerator = require('./masking-secret-generator');

const MessageBuffer = require('./message-buffer');

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}


function calculateSharedKey(bsv, privateKeyWif, publicKeyHex) {
    const privateKey = bsv.PrivateKey.fromWIF(privateKeyWif);
    const publicKey = bsv.PublicKey.fromHex(publicKeyHex);
    return publicKey.point.mul(privateKey.bn).toHex();
}


function startPeerClient(context, pkiKey, pkiPrivateKey, host, port) {

    const Hash = context.bsv.crypto.Hash;


    const createHandshakeMessage = () => {
        const salt = new Date().getTime() + '';

        const wrapped = JSON.stringify({
            type: 'handshake',
            by: pkiKey,
            salt: salt
        });

        const bufferOfMessage = Buffer.from(wrapped);
        var bsvMessage = new context.bsv.Message(bufferOfMessage);

        var signature = bsvMessage.sign(context.bsv.PrivateKey.fromWIF(pkiPrivateKey));

        return JSON.stringify({
            envelope: wrapped,
            by: pkiKey,
            signature: signature
        });
    };

    const createRedirectedMaskedMessage = (destination, messageString) => {
        const salt = new Date().getTime() + '';
        const sharedKey = calculateSharedKey(context.bsv, pkiPrivateKey, destination);

        const finalKey = Hash.sha256(Buffer.concat([
            Buffer.from(sharedKey, 'hex'),
            Buffer.from(salt, 'utf8')
        ]));

        const maskingHelper = getMaskingSecretGenerator(
            finalKey,
            (maskingChunkBuffer) => {
                return Hash.sha256(maskingChunkBuffer);
            }
        );

        const buf = Buffer.from(messageString, 'utf8');
        const maskBuffer = maskingHelper.next(buf.length).value;
        xor(buf, maskBuffer);
        //todo modify this so that masking is done in chunks as opposed to in one move

        const decoder = new StringDecoder('base64');
        const maskedMessage = decoder.write(buf) + decoder.end();

        return JSON.stringify({
            type: 'redirect',
            id: uuidv4(),
            from: pkiKey,
            to: destination,
            payload: maskedMessage,
            salt: salt
        });
    };


    // Create a new TCP client.
    const socket = new Net.Socket();

    const serverInterface = {
        host: host,
        port: port,
        socket: socket,
        send: (messageString, callback) => {
            if (callback) {
                socket.write(messageString + '\n', callback);
            } else {
                socket.write(messageString + '\n');
            }
        },
        disconnectNow: () => {
            try {
                console.log('...executing #disconnectNow');
                socket.end();

            } catch (e) {
                console.log(e)
            }
        },
        onConnected: () => {
            console.log('placeholder implementation of #onConnected');
        },
        onDisconnected: (error) => {
            console.log('placeholder implementation of #onDisconnected');
        },
        onMessage: (from, plainString) => {
            console.log('placeholder implementation of #onMessage; ' + plainString);
        },
        onRedirectError: (json) => {
            console.log('placeholder implementation of #onRedirectError; ' + JSON.stringify(json));
        },
        sendMessage: null, //initalized later
        lastError: null
    };

    serverInterface.sendMessage = (destination, messageString, callback) => {
        const message = createRedirectedMaskedMessage(destination, messageString);
        console.log('...about to send: ' + message);

        serverInterface.send(message, callback);
    }


    const handleReceivedMessage = (jsonString) => {
        //the expected json-string can have multiple formats - handshake-acceptence, masked-message

        var json = JSON.parse(jsonString);

        if ('redirect' == json['type']) {
            if (json['to'] != pkiKey) {
                throw new Error('unexpected message');
            }

            const salt = json['salt'];
            const from = json['from'];
            const payload = json['payload'];


            const sharedKey = calculateSharedKey(context.bsv, pkiPrivateKey, from);

            const finalKey = Hash.sha256(Buffer.concat([
                Buffer.from(sharedKey, 'hex'),
                Buffer.from(salt, 'utf8')
            ]));

            const maskingHelper = getMaskingSecretGenerator(
                finalKey,
                (maskingChunkBuffer) => {
                    return Hash.sha256(maskingChunkBuffer);
                }
            );

            var buf = Buffer.from(payload, 'base64');
            var maskBuffer = maskingHelper.next(buf.length).value;
            xor(buf, maskBuffer);

            var unmaskedMessage = buf.toString('utf8');

            serverInterface.onMessage(from, unmaskedMessage);

        } else if ('redirect-error' == json['type']) {
            serverInterface.onRedirectError(json);

        } else {
            throw new Error('unexpected message ' + chunk);
        }
    };

    // The client can receive data from the server by reading from its socket.
    const received = new MessageBuffer('\n');

    socket.on('data', (chunk) => {

        try {
            received.push(chunk);
            while (!received.isFinished()) {
                const message = received.handleData();
                handleReceivedMessage(message);
            }

            
        } catch (e) {
            console.log(e, chunk);
        }
    });

    socket.on('close', () => {
        try {
            console.log('a socket to a stall closed; cleaning up');
            serverInterface.onDisconnected(serverInterface.lastError);

        } catch (e) {
            console.log(e);
        }
    });

    socket.on('error', (e) => { //...FYI there is another event `close` that is called right after this one
        serverInterface.lastError = e;
        console.log(e);
    });



    // Send a connection request to the server.
    socket.connect({ port: port, host: host }, () => {
        // If there is no error, the server has accepted the request and created a new 
        // socket dedicated to us.
        console.log('TCP connection established with the redirect-server; sending handshake');

        //pass the handshake message to the server
        serverInterface.send(createHandshakeMessage());

        //todo it maybe make sense to add a delay before calling the following? (allows for the handshake-roundtrip)
        serverInterface.onConnected();

    });


    return serverInterface;
}


module.exports = startPeerClient;