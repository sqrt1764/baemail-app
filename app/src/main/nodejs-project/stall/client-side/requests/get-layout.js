'use strict';



/*

# expected structure of params
[
    {
        link: "/welcome",
    }
]


# expected structure of the response data passed in the callback
[
    {
        layout: [..]
    }
]






///////////////
///////////////
///////////////
///////////////
supported layout-items

// Button-links that start with a `/` get redirected to this request; links that start with `?`
// are handled without communication with the stall.

// buttonLink example: '/welcome'
// buttonLink example: '/welcome/intro?page=0'
// buttonLink example: '?root'
// buttonLink example: '?leave'

{
   type: "text",
   style: "body",
   value: "Loading info; requesting it from a 'stall' hosted by this peer device."
}
// body/h1/h2/caption


{
   type: "gap_0",
   size: 0.7
}


{
   type: "progress_0",
   size: 0.6
}

{
    type: "button",
    buttonLink: `/welcome/intro?page=0`,
    label: "Request IRL service"
}

{
    type: "toolbar",
    label: "Welcome",
    labelStyle: 'h2',
    buttonLink: '/welcome/intro?page=0'
}
// a toolbar with a `back` button on the left




{
   type: "image",
   showCaption: true,
   caption: "photo of product from top",
   downloadPath: "/file/teehee"
}


//todo add a horizontally scrolling button group




! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!  !!!!!!!! !!!!!!!!!!!!!!!!!!!!!
!!!!!!!!! NEVER eval(..) A STRING RECEIVED FROM THE STALL



*/
function GetLayout(context) {
    this.context = context
    this.command = 'GET_LAYOUT'
}


module.exports = GetLayout;

GetLayout.prototype.buildRequest = function(link, callback) {
    if (typeof(link) != "string") {
        throw new Error();
    }

    return {
        command: this.command,
        params: [{
            link: link,
        }],
        callback: callback
    };
};

GetLayout.prototype.onResponse = function(status, dataArray, request) {

    let errorLayout = [
        {
            type: "toolbar",
            label: "Error",
            labelStyle: 'h2',
            buttonLink: '?root'
        },
        {
           type: "text",
           style: "body",
           value: "ERROR RECEIVED INVALID LAYOUT"
        }
    ];

    var responseObject = dataArray[0];
    if (responseObject == undefined) {
        request.callback(false, errorLayout);
        return
    }
    var layout = responseObject.layout;
    if (layout == undefined) {
        request.callback(false, errorLayout);
        return
    }

    if (status == 'success') {
        request.callback(true, layout);
        return
    }

    request.callback(false, layout);

};