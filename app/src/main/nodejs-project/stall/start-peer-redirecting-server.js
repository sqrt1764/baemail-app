
const Net = require('net');
const xor = require('buffer-xor/inplace');
const { StringDecoder } = require('string_decoder');
const getMaskingSecretGenerator = require('./masking-secret-generator');

const MessageBuffer = require('./message-buffer');




function calculateSharedKey(bsv, privateKeyWif, publicKeyHex) {
    const privateKey = bsv.PrivateKey.fromWIF(privateKeyWif);
    const publicKey = bsv.PublicKey.fromHex(publicKeyHex);
    return publicKey.point.mul(privateKey.bn).toHex();
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}


function startPeerRedirectingServer(context, port, pki, pkiPrivateKey) {

    const Hash = context.bsv.crypto.Hash;


    const clients = {};
    const server = new Net.Server();

    const interfaceOfSelf = {
        pki: pki,
        send: null, //initialized later
        onMessage: (from, messageString) => {
            console.log('placeholder implementation of #onMessage: ' + from + ' .. ' + messageString);
        }
    };
    interfaceOfSelf.send = (messageString) => {

        //handle the message that got redirected to this current peer
        var json = JSON.parse(messageString);

        if ('redirect' != json['type']) {
            throw new Error('unexpected message type ' + messageString);
        }

        if (json['to'] != pki) {
            throw new Error('unexpected message destination');
        }

        const salt = json['salt'];
        const from = json['from'];
        const payload = json['payload'];

        const sharedKey = calculateSharedKey(context.bsv, pkiPrivateKey, from);

        const finalKey = Hash.sha256(Buffer.concat([
            Buffer.from(sharedKey, 'hex'),
            Buffer.from(salt, 'utf8')
        ]));

        const maskingHelper = getMaskingSecretGenerator(
            finalKey,
            (maskingChunkBuffer) => {
                return Hash.sha256(maskingChunkBuffer);
            }
        );

        var buf = Buffer.from(payload, 'base64');
        var maskBuffer = maskingHelper.next(buf.length).value;
        xor(buf, maskBuffer);

        var unmaskedMessage = buf.toString('utf8');

        interfaceOfSelf.onMessage(from, unmaskedMessage);
    };

    clients[interfaceOfSelf.pki] = interfaceOfSelf;


    const doesPeerExist = (publicKey) => {
        var matchingInterface = Object.values(clients).find((interface) => {
            return interface.pki == publicKey;
        });

        return matchingInterface != null;
    };

    const redirectMessage = (json, callback) => {
        var destination = json['to'];

        var matchingInterface = Object.values(clients).find((interface) => {
            return interface.pki == destination;
        });

        if (!matchingInterface) throw new Error('interface not found; pki: ' + destination);

        //console.log('...message redirected to ' + destination + '; json: ' + JSON.stringify(json));
        matchingInterface.send(JSON.stringify(json), callback);
    };



    //this is essentially the same as the matching function in `start-peer-client`
    const createRedirectedMaskedMessage = (destination, messageString) => {
        const salt = new Date().getTime() + '';
        const sharedKey = calculateSharedKey(context.bsv, pkiPrivateKey, destination);

        const finalKey = Hash.sha256(Buffer.concat([
            Buffer.from(sharedKey, 'hex'),
            Buffer.from(salt, 'utf8')
        ]));

        const maskingHelper = getMaskingSecretGenerator(
            finalKey,
            (maskingChunkBuffer) => {
                return Hash.sha256(maskingChunkBuffer);
            }
        );

        const buf = Buffer.from(messageString, 'utf8');
        const maskBuffer = maskingHelper.next(buf.length).value;
        xor(buf, maskBuffer);
        //todo maybe modify this so that masking is done in chunks as opposed to in one move?

        const decoder = new StringDecoder('base64');
        const maskedMessage = decoder.write(buf) + decoder.end();

        return JSON.stringify({
            type: 'redirect',
            id: uuidv4(),
            from: pki,
            to: destination,
            payload: maskedMessage,
            salt: salt
        });
    };




    const cleanUp = () => {
        console.log('...redirecting-server #cleanUp');
        server.close(() => {
            console.log('...on server closed');
        });

        Object.values(clients).forEach((clientInterface) => {
            clientInterface.disconnectNow();
        });
    };

    const holder = {
        clients: clients,
        server: server,
        interfaceOfSelf: interfaceOfSelf,
        doesPeerExist: doesPeerExist,
        redirectMessage: redirectMessage,
        cleanUp: cleanUp,
        onDisconnected: () => {
            console.log('placeholder implementation of #onDisconnected');
        },
        onNewClient: (clientInterface) => {
            console.log('placeholder implementation of #onNewClient');
        },
        onLostClient: (clientInterface) => {
            console.log('placeholder implementation of #onLostClient');
        },
        verifyValidMessage: (signedString, publicKey, signature) => {
            var address = context.bsv.PublicKey.fromString(publicKey).toAddress('mainnet').toString();
            return context.bsv.Message.verify(signedString, address, signature);
        },
        sendMessage: (destination, messageString, callback) => {
            const message = createRedirectedMaskedMessage(destination, messageString);
            console.log('...about to send: ' + message);
            redirectMessage(JSON.parse(message), callback);
        }
    };

    const onLostClient = (clientInterface) => {
        delete clients[clientInterface.pki]
        console.log('#onLostClient ' + clientInterface.pki);
        holder.onLostClient(clientInterface);
    };

    const onNewClient = (clientInterface) => {
        clients[clientInterface.pki] = clientInterface;
        console.log('#onNewClient ' + clientInterface.pki);
        holder.onNewClient(clientInterface);
    };


    //
    // When a client requests a connection with the server, the server creates a new
    // socket dedicated to that client.
    server.on('connection', (socket) => {


        const clientInterface = {
            pki: null, //initialized with handshake
            socket: socket,
            send: (messageString, callback) => {
                if (callback) {
                    socket.write(messageString + '\n', callback);
                } else {
                    socket.write(messageString + '\n');
                }
            },
            onHandshake: null, //initialized later
            onDisconnected: null, //initialized later
            disconnectNow: null //initialized later
        };

        clientInterface.onHandshake = (publicKey) => {
            clientInterface.pki = publicKey;
            onNewClient(clientInterface);
        };
        clientInterface.onDisconnected = () => {
            onLostClient(clientInterface);
        };
        clientInterface.disconnectNow = () => {
            try {
                console.log('...executing #disconnectNow');
                socket.end();

            } catch (e) {
                console.log(e)
            }
        };






        const handleReceivedMessage = (jsonString) => {
            //the expected json-string  can have multiple formats - handshake, pki-masked
            var json = JSON.parse(jsonString);

            if (json['envelope']) {
                var wrapped = json['envelope'];
                var publicKey = json['by'];
                var signature = json['signature'];

                if (holder.verifyValidMessage(wrapped, publicKey, signature)) {
                    var wrappedJson = JSON.parse(wrapped);

                    if ('handshake' == wrappedJson['type']) {
                        //this client has proven ownership of private-key matching this public-key;
                        //using public-key for redirection
                        clientInterface.onHandshake(publicKey);

                    } else {
                        throw new Error('unexpected message-type');
                    }

                } else {
                    console.log('a client failed to comply with the communication protocol; dropping');
                    clientInterface.disconnectNow();
                }


            } else if ('redirect' == json['type']) {
                var destination = json['to'];
                if (!holder.doesPeerExist(destination)) {
                    console.log('...failed to redirect a message; responding to sender with error-reponse-message');

                    var errorResponseMessage = JSON.stringify({
                        type: 'redirect-error',
                        id: json['id']
                    });

                    clientInterface.send(errorResponseMessage);


                } else {
                    holder.redirectMessage(json);
                }




            } else {
                throw new Error('not supported');

            }
        };

        // The server can also receive data from the client by reading from its socket.
        const received = new MessageBuffer('\n');

        socket.on('data', (chunk) => {
            try {
                received.push(chunk);
                while (!received.isFinished()) {
                    const message = received.handleData();
                    handleReceivedMessage(message);
                }

            } catch (e) {
                console.log(e, chunk);

            }
        });

        // When the client requests to end the TCP connection with the server, the server
        // ends the connection.
        socket.on('end', () => {
            try {
                console.log('a client disconnected from the redirect-server; client-pki:' + clientInterface.pki);
            } catch (e) {
                console.log(e);
            }

            clientInterface.onDisconnected();
        });

        // Don't forget to catch error, for your own sake.
        socket.on('error', (err) => {
            console.log(`Error: ${err}`);
            try {
                console.log('a client disconnected from the redirect-server (with error); client-pki:' + clientInterface.pki);
            } catch (e) {
                console.log(e);
            }

            clientInterface.onDisconnected();

        });


        console.log('A new connection has been established.');



    });

    server.listen(port, function() {
        console.log(`Server listening for connection requests on socket localhost:${port}`);
    });


    server.on('close', () => {
        holder.onDisconnected();
    });

    return holder;
}

module.exports = startPeerRedirectingServer;