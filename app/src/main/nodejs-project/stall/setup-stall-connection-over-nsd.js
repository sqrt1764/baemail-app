'use strict';

const WebSocket = require('ws');


function calculateSharedKey(bsv, privateKeyWif, publicKeyHex) {
    var privateKey = bsv.PrivateKey.fromWIF(privateKeyWif);
    var publicKey = bsv.PublicKey.fromHex(publicKeyHex);
    return publicKey.point.mul(privateKey.bn).toHex();
}


function connectToStall(context, myConfig, stallName, title, paymail, host, port, pki) {
    const sharedKey = calculateSharedKey(context.bsv, myConfig.pkiPrivateKey, pki);
    const encryptionHelper = context.helper.getNewEncryptionHelper(sharedKey);


    const stallInterface = {
        sharedKey: sharedKey,
        host: host,
        port: port,
        ws: null, //initialized later
        paymail: paymail,
        pki: pki,
        stallName: stallName,
        title: title,
        onMessageDecrypted: (message) => {
            console.log('decrypted message:' +  message);
        },
        onConnected: () => {
            console.log('placeholder implementation of #onConnected');
        },
        onDisconnected: () => {
            console.log('placeholder implementation of #onDisconnected');
        },
        onError: (code) => {
            console.log('placeholder implementation of #onError');
        },
        disconnectNow: null, //initialized later
        sendEncrypted: null //initialized later
    };

    const ws = new WebSocket(`ws://${host}:${port}?pki=${myConfig.pkiKey}`);

    ws.on('open', function() {
        try {
            console.log('connection to stall opened:' + stallName);
            stallInterface.onConnected();

        } catch (e) {
            console.log(e);
        }
    });

    ws.on('message', function(message) {
        try {
            //console.log('message from stall:' + message);
            let decryptedMessage = encryptionHelper.decrypt(message)

            stallInterface.onMessageDecrypted(decryptedMessage);

        } catch (e) {
            console.log('crash during processing of message', e);
        }
    });

    ws.on('close', function() {
        try {
            console.log('a socket to a stall closed; cleaning up');
            stallInterface.onDisconnected();

        } catch (e) {
            console.log(e);
        }
    });

    ws.on('error', function(e) {
        try {
            stallInterface.onError(e.code);

        } catch (e) {
            console.log(e);
        }
    });



    stallInterface.ws = ws;

    stallInterface.disconnectNow = () => {
        try {
            console.log('...executing #disconnectNow');
            ws.close()

        } catch (e) {
            console.log(e)
        }
    }

    stallInterface.sendEncrypted = function(message) {
        try {
            let encrypted = encryptionHelper.encrypt(message);
            ws.send(encrypted);
        } catch (e) {
            console.log('error sending an encrypted message', e);
        }
    };


    return stallInterface;
}



module.exports = connectToStall;