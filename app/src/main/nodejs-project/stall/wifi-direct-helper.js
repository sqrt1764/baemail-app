
const WifiDirectPeer = require('./wifi-direct-peer');
const { StringDecoder } = require('string_decoder');

const fs = require('fs');
const fsPromises = require('fs').promises;
const path = require('path');




function calculateSharedKey(bsv, privateKeyWif, publicKeyHex) {
    const privateKey = bsv.PrivateKey.fromWIF(privateKeyWif);
    const publicKey = bsv.PublicKey.fromHex(publicKeyHex);
    return publicKey.point.mul(privateKey.bn).toHex();
}


function WifiDirectHelper(stall) {
    this.context = stall.context;
    this.stall = stall;

    this.isActive = false;
    this.isAdvertisingStall = false;
    this.isGroupConnected = false;

    this.peer = null;

    this.connectionAttempts = {};
    this.currentParticipants = {};
    this.expectingFiles = {};
    this.receivedFilePartTracking = {};

    this.queuedReceived = {
        processing: false,
        messages: []
    };

};

WifiDirectHelper.prototype.ensureIsSetupAppropriately = function() {
    let isHostingStall = this.isAdvertisingStall;
    let isDiscoveringStalls = this.stall.discovery.isActive;
    let isUsingWifiDirect = this.stall.my.config.connectionType == "wifidirect";

    if (!isUsingWifiDirect) {
        console.log('...WifiDirectHelper#ensureIsSetupAppropriately DROPPED; not using nearby');
        return;
    }

    if (!isHostingStall && !isDiscoveringStalls) {
        console.log('...WifiDirectHelper#ensureIsSetupAppropriately DROPPED; nearby-comms should be disabled');
        return;
    }

    if (this.isActive) {
        console.log('...WifiDirectHelper#ensureIsSetupAppropriately DROPPED; already active');
        return;
    }

    this.setup();
}

WifiDirectHelper.prototype.ensureIsTornDownAppropriately = function() {
    let isHostingStall = this.isAdvertisingStall;
    let isDiscoveringStalls = this.stall.discovery.isActive;
    let isUsingWifiDirect = this.stall.my.config.connectionType == "wifidirect";

    if (!isUsingWifiDirect) return;

    if (isHostingStall || isDiscoveringStalls) {
        //wifi-direct-peer should be enabled
        return;
    }

    if (this.isActive) {
        this.tearDown();
    }
};

WifiDirectHelper.prototype.setup = function() {
    this.isActive = true;

    if (this.peer != null) throw new Error('unexpected state');

    const getMyStallPki = () => {
        return this.stall.my.config.pkiKey;
    };
    const getMyStallPkiPrivateKey = () => {
        return this.stall.my.config.pkiPrivateKey;
    };
    const onMessage = (fromPki, messageString) => {
        this.onMessage(fromPki, messageString);
    };
    const onConnectedToGroup = () => {
        console.log('#onConnectedToGroup');
        this.isGroupConnected = true;

        this.fulfilStallJoinAttempts();
    };
    const onDisconnectedFromGroup = () => {
        console.log('#onDisconnectedFromGroup');
        this.isGroupConnected = true;
        this.clearAwayAllInterfaces();
    };

    this.peer = new WifiDirectPeer(
        this.context,
        getMyStallPki,
        getMyStallPkiPrivateKey,
        onMessage,
        onConnectedToGroup,
        onDisconnectedFromGroup
    );

    console.log('WifiDirectHelper#setup completed');
};

WifiDirectHelper.prototype.tearDown = function() {
    this.isActive = false;

    //force the cleanup of the wifi-direct-peer
    if (this.peer) {
        this.peer.onGroupChanged(false, null);
        this.peer = null;
    }

    //be sure to clear this on tear-down
    this.connectionAttempts = {};

    //be sure to clear this on tear-down
    this.currentParticipants = {};

    this.expectingFiles = {};

    console.log('WifiDirectHelper#tearDown completed');
};

WifiDirectHelper.prototype.onStartedAdvertisingStall = function() {
    this.isAdvertisingStall = true;
};

WifiDirectHelper.prototype.onStoppedAdvertisingStall = function() {
    this.isAdvertisingStall = false;
};

WifiDirectHelper.prototype.onConnectionAttempt = function(host, paymail, pki, title) {
    const currentMatching = this.connectionAttempts[pki];

    if (!currentMatching) {
        this.connectionAttempts[host] = {
            host: host,
            paymail: paymail,
            pki: pki,
            title: title
        };
        console.log('WifiDirectHelper#onConnectionAttempt added');

    }

    this.fulfilStallJoinAttempts();
};

WifiDirectHelper.prototype.onGroupChanged = function(isFormed, isGroupOwner, ownerIp) {
    if (this.peer == null) throw new Error('invoked in unexpected state');

    this.peer.onGroupChanged(isGroupOwner, ownerIp);

    if (this.peer.isConnected()) {
        // send automated `browse` messages to the peer-stall's that got queued when forming the group
        this.fulfilStallJoinAttempts();
    }
};

/*
the expected structure of the 'participants' parameter

[
    {
        peerName: 'abc',
        peerAddress: 'mac-address',
        status: true
    },
    ..
]
*/
WifiDirectHelper.prototype.onParticipantsChanged = function(participants) {
    const latestParticipants = {};

    participants.forEach((peer) => {
        if (peer.status) {
            latestParticipants[peer.peerAddress] = {
                name: peer.peerName,
                address: peer.peerAddress
            };
        }
    });

    this.currentParticipants = latestParticipants;
    console.log('WifiDirectHelper#onParticipantsChanged; ' + JSON.stringify(latestParticipants));

    this.clearAwayBrokenInterfaces();
    this.fulfilStallJoinAttempts();
}

WifiDirectHelper.prototype.onStallConfigChanged = function(stallConfig) {
    if (this.peer == null) {
        console.log('WifiDirectHelper#onStallConfigChanged dropped as wifi-direct-peer is NULL at this time');
        return;
    }

    const activePeerPki = this.peer.getActivePki();
    const currentPki = this.stall.my.config.pkiKey;
    if (activePeerPki != null && activePeerPki != currentPki) {
        //current user's key's changed; this affects the message-encryption
        throw new Error("NOT YET HANDLED");
        //todo check if the wifi-direct-peer needs to be recreated in case of a pkiPrivateKey change
        //todo ...temp implementation - crash if this is attempted while the wifi-direct-peer is active
        //todo
    }

};

WifiDirectHelper.prototype.fulfilStallJoinAttempts = function() {
    if (this.peer == null) {
        console.log('#fulfilStallJoinAttempts dropped as wifi-direct-peer is NULL at this time');
        return;
    }
    if (!this.isGroupConnected) {
        console.log('#fulfilStallJoinAttempts dropped as wifi-direct-peer is not yet connected to a p2p-group');
        return;
    }

    console.log('#fulfilStallJoinAttempts');

    Object.values(this.connectionAttempts).forEach((attempt) => {
        var matchingParticipant = Object.values(this.currentParticipants).find((peer) => {
            return peer.address == attempt.host;
        });
        if (matchingParticipant) {
            //this participant is reachable via `this.peer`; be sure to start-browsing
            if (!this.isCommunicatingWithStall(attempt.pki)) {
                console.log('WifiDirectHelper#fulfilStallJoinAttempts; found a p2p-group-peer who has can fulfil a stall-browse-attempt; pki:' + attempt.pki);
                this.sendMessageAsClientToStartBrowse(attempt.pki);
            }

        } else {
            console.log('#fulfilStallJoinAttempts; could not find a participant which would fulfil the stall-join attempts; attempt.host:' + attempt.host + '; currentParticipants:' + JSON.stringify(this.currentParticipants));
        }
    });
};

//auto-disconnect interfaces when matching participants go away from the p2p-group
WifiDirectHelper.prototype.clearAwayBrokenInterfaces = function() {

    Object.values(this.stall.peers.connected).forEach((stallInterface) => {
        var matchingP2pGroupParticipant = this.currentParticipants[stallInterface.stallName];
        if (!matchingP2pGroupParticipant) {
            console.log('WifiDirectHelper#clearAwayBrokenInterfaces detected a stall-interface whose p2p-group-participant is gone; ' + stallInterface.pki);
            stallInterface.onDisconnected();
        }
    });

    Object.values(this.stall.my.clients).forEach((clientInterface) => {
        var matchingP2pGroupParticipant = this.currentParticipants[clientInterface.stallName];

        clientInterface.onDisconnected();
    });

};

WifiDirectHelper.prototype.clearAwayAllInterfaces = function() {
    console.log('#clearAwayAllInterfaces');
    Object.values(this.stall.peers.connected).forEach((stallInterface) => {
        stallInterface.onDisconnected();
    });
    Object.values(this.stall.my.clients).forEach((clientInterface) => {
        clientInterface.onDisconnected();
    });
};


WifiDirectHelper.prototype.isCommunicatingWithStall = function(pki) {
    var interface = Object.values(this.stall.peers.connected).find((stallInterface) => {
        return stallInterface.pki == pki;
    });
    return interface != null;
};



WifiDirectHelper.prototype.sendMessage = function(toPki, asStallNotClient, messageString) {
    const json = {
        type: 'message',
        data: messageString
    };
    if (asStallNotClient) {
        json['as'] = 'stall';
    } else {
        json['as'] = 'client';
    }

    this.peer.send(toPki, JSON.stringify(json));
};

WifiDirectHelper.prototype.sendMessageAsClientToStartBrowse = function(toPki) {
    const json = {
        as: 'client',
        type: 'browse'
    };

    this.peer.send(toPki, JSON.stringify(json));
};

WifiDirectHelper.prototype.sendMessageAsStallToAllowBrowse = function(toPki) {
    const json = {
        as: 'stall',
        type: 'browse_accepted'
    };

    this.peer.send(toPki, JSON.stringify(json));
};

WifiDirectHelper.prototype.sendMessageAsClientToStopBrowse = function(toPki) {
    const json = {
        as: 'client',
        type: 'leave'
    };

    this.peer.send(toPki, JSON.stringify(json));
};

WifiDirectHelper.prototype.sendMessageAsStallToKickBrowse = function(toPki) {
    const json = {
        as: 'stall',
        type: 'browse_kick'
    };

    this.peer.send(toPki, JSON.stringify(json));
};







/*
//all of the currently possible message-types that are masked & wrapped & redirected to the correct participant

{
    as: 'client',
    type: 'browse'
}

{
    as: 'stall',
    type: 'browse_accepted'
}

{
    as: 'client',
    type: 'leave'
}

{
    as: 'stall',
    type: 'browse_kick'
}

{
    as: 'client',
    type: 'message',
    data: '{\'w\':443000}'
}

{
    as: 'stall',
    type: 'message',
    data: '{\'w\':443000}'
}

////


{
    as: 'client',
    type: 'file_request',
    token: 'abc',
    uri: 'a/resource/string?abc=lol'
}


{
    as: 'stall',
    type: 'file_part',
    index: 0,
    token: 'abc',
    data: 'base64-encoded-chunk',
    extension: 'jpeg', //...this field will only exist on the 0th part
    end: true //...this field will only exist on the last part
}
*/
WifiDirectHelper.prototype.onMessage = function(fromPki, messageString) {
    const startProcessingMessages = async () => {
        this.queuedReceived.processing = true;

        while (true) {
            if (this.queuedReceived.messages.length == 0) break;

            //pop 0th
            let queued = this.queuedReceived.messages.splice(0, 1)[0];
            await this.onMessage1(queued.from, queued.data);


        }

        this.queuedReceived.processing = false;
    };

    this.queuedReceived.messages.push({
        from: fromPki,
        data: messageString
    });

    if (this.queuedReceived.processing) {
        //the processing which is already active will eventually get the the just-pushed-message
    } else {
        startProcessingMessages();
    }

}

//this must be executed serially; a race condition among multiple messages of type 'file_part' could corrupt a file
WifiDirectHelper.prototype.onMessage1 = async function(fromPki, messageString) {
    try {
        var json = JSON.parse(messageString);
        if (!json['as']) throw new Error('unexpected json-structure');

        if ('client' == json['as']) {
            if ('browse' == json['type']) {
                this.onBrowseStallMessage(fromPki);

            } else if ('leave' == json['type']) {
                this.onLeaveStallMessage(fromPki);

            } else if ('message' == json['type']) {
                this.onStallCommsMessage(fromPki, false, json['data']);

            } else if ('file_request' == json['type']) {
                this.onIncomingFileRequest(fromPki, false, json['token'], json['uri']);

            } else if ('file_part' == json['type']) {
                await this.onIncomingFilePart(fromPki, false, json['meta'], json['data']);

            } else {
                throw new Error('unexpected json-structure, type-value');
            }

        } else if ('stall' == json['as']) {
            if ('browse_accepted' == json['type']) {
                this.onBrowseAccepted(fromPki);

            } else if ('browse_kick' == json['type']) {
                this.onBrowseKick(fromPki);

            } else if ('message' == json['type']) {
                this.onStallCommsMessage(fromPki, true, json['data']);

            } else if ('file_request' == json['type']) {
                this.onIncomingFileRequest(fromPki, true, json['token'], json['uri']);

            } else if ('file_part' == json['type']) {
                await this.onIncomingFilePart(fromPki, true, json['meta'], json['data']);

            } else {
                throw new Error('unexpected json-structure, type-value');
            }

        } else {
            throw new Error('unexpected json-entry value');
        }

    } catch (e) {
        console.log(e);
    }
};




WifiDirectHelper.prototype.onBrowseStallMessage = function(fromPki, messageString) {
    if (!this.isAdvertisingStall) {
        console.log('WifiDirectHelper#onBrowseStallMessage dropped because the stall is not active');
        return;
    }


    const myConfig = this.stall.my.config;
    const sharedKey = calculateSharedKey(this.context.bsv, myConfig.pkiPrivateKey, fromPki);

    const clientInterface = {
        sharedKey: sharedKey,
        id: fromPki,
        pki: fromPki,
        sendEncrypted: (message) => {
            this.sendMessage(fromPki, true, message);
        },
        onMessageDecrypted: (message) => {
            console.log('decrypted message:' +  message);
        }, //initialized later
        onDisconnected: null //initialized later
    };

    this.stall.my.onClientConnected(clientInterface);

    this.sendMessageAsStallToAllowBrowse(fromPki);
};

WifiDirectHelper.prototype.onLeaveStallMessage = function(fromPki) {
    const foundClientInterface = this.stall.my.clients[fromPki];
    if (!foundClientInterface) {
        console.log(new Error('matching client-interface not found; ' + fromPki));
        return;
    }
    foundClientInterface.onDisconnected();
};

WifiDirectHelper.prototype.onBrowseAccepted = function(fromPki) {
    let isDiscoveringStalls = this.stall.discovery.isActive;
    if (!isDiscoveringStalls) {
        console.log('WifiDirectHelper#onBrowseAccepted dropped because the stall-discovery is not active');
        return;
    }

    var connectionAttempt = Object.values(this.connectionAttempts).find((attempt) => {
        return attempt.pki == fromPki;
    });
    if (!connectionAttempt) {
        console.log('WifiDirectHelper#onBrowseAccepted dropped; could not find a matching connection-attempt');
        return;
    }


    const myConfig = this.stall.my.config;
    const sharedKey = calculateSharedKey(this.context.bsv, myConfig.pkiPrivateKey, fromPki);

    const stallInterface = {
            sharedKey: sharedKey,
            host: connectionAttempt.host,
            paymail: connectionAttempt.paymail,
            pki: connectionAttempt.pki,
            stallName: connectionAttempt.pki,
            title: connectionAttempt.title,
            onMessageDecrypted: (message) => {
                console.log('decrypted message:' +  message);
            },
            onConnected: () => {
                console.log('placeholder implementation of #onConnected');
            },
            onDisconnected: () => {
                console.log('placeholder implementation of #onDisconnected');
            },
            onError: (code) => {
                console.log('placeholder implementation of #onError');
            },
            disconnectNow: null, //initialized later
            sendEncrypted: null //initialized later
        };

        stallInterface.disconnectNow = () => {
            try {
                console.log('...executing #disconnectNow');

                let matching = Object.values(this.stall.peers.connected).find((item) => {
                    return item.pki == fromPki;
                });

                if (matching == null) {
                    console.log('matching stall not found; dropping #handleDisconnectedAsStallClient; pki:' + fromPki);
                    return;
                }

                matching.onDisconnected();

                this.sendMessageAsClientToStopBrowse(fromPki);

            } catch (e) {
                console.log(e)
            }
        };


        stallInterface.sendEncrypted = (message) => {
            try {
                this.sendMessage(fromPki, false, message);

            } catch (e) {
                console.log('error sending an encrypted message', e);
            }
        };

        this.stall.peers.onStallConnectedViaNearby(stallInterface);
        stallInterface.onConnected();
};

WifiDirectHelper.prototype.onBrowseKick = function(fromPki) {
    let matching = Object.values(this.stall.peers.connected).find((item) => {
        return item.pki == fromPki;
    });

    if (matching == null) {
        console.log('matching stall not found; dropping #handleDisconnectedAsStallClient; pki:' + fromPki);
        return;
    }

    matching.onDisconnected();
};

WifiDirectHelper.prototype.onStallCommsMessage = function(fromPki, asStallNotClient, messageString) {
    if (asStallNotClient) {
        let matching = Object.values(this.stall.peers.connected).find((item) => {
            return item.pki == fromPki;
        });

        if (matching == null) {
            console.log('matching stall-interface not found; dropping #onStallCommsMessage; pki:' + fromPki);
            return;
        }

        matching.onMessageDecrypted(messageString);

    } else {
        const foundClientInterface = Object.values(this.stall.my.clients).find((item) => {
            return item.pki == fromPki;
        });
        //const foundClientInterface = this.stall.my.clients[fromPki];
        if (!foundClientInterface) {
            console.log(new Error('matching client-interface not found; dropping #onStallCommsMessage; pki:' + fromPki));
            return;
        }

        foundClientInterface.onMessageDecrypted(messageString);

    }
};




/*
{
    as: 'stall',
    type: 'file_request',
    token: 'abc',
    uri: '/a/resource/string?abc=lol'
}
*/
WifiDirectHelper.prototype.requestFile = function(
    targetInterface,
    fileResourceUri,
    destinationDirectory,
    resultingFileName,
    callback
) {
    console.log('...WifiDirectHelper#requestFile fileResourceUri:' + fileResourceUri);

    let token = targetInterface.pki + new Date().getTime();


    //cache metadata, it will be necessary when the requested file arrives back
    let meta = {
        targetInterface: targetInterface,
        token: token,
        fileResourceUri: fileResourceUri,
        destinationDirectory: destinationDirectory,
        resultingFileName: resultingFileName,
        callback: callback,
        onSuccess: (filePath) => {
            console.log('...WifiDirectHelper#requestFile#onSuccess filePath:' + filePath);
            delete this.expectingFiles[token];

            callback(true, filePath);
        },
        onError: (error) => { //todo this is never called
            console.log('...WifiDirectHelper#requestFile#onError error:' + error);
            //todo
            //todo
        }
    };
    this.expectingFiles[token] = meta;


    //todo implement timeout for the download
    //todo
    //todo
    //todo
    //todo


    const isTargetingAStall = targetInterface.stallName != undefined;
    let asValue = 'client';
    if (!isTargetingAStall) asValue = 'stall';



    const json = {
        as: asValue,
        type: 'file_request',
        token: token,
        uri: fileResourceUri
    };

    this.peer.send(targetInterface.pki, JSON.stringify(json));

};




WifiDirectHelper.prototype.findFileRequestPath = function(uri) {
    //todo ... figure out the actual file-path
    //todo ...
    let appDir = path.dirname(require.main.filename);
    let projectParentDir = path.resolve(appDir, '..');
    let stallDir = projectParentDir + path.sep + 'Stall';

    let fileName = 'teehee.jpg';
    let filePath = stallDir + path.sep + fileName; //todo placeholder

    return filePath;
};


WifiDirectHelper.prototype.onIncomingFileRequest = async function(
    fromPki,
    fromStallNotClient,
    token,
    uri
) {

    //todo must respond with error if requested file not found
    let filePath = this.findFileRequestPath(uri);
    let fileExtension = filePath.substring(filePath.lastIndexOf('.') + 1);




    const sendPart = async (index, base64Data, isLastPart) => {
        console.log('peer#sendPart ' + index);
        var as = 'stall';
        if (!fromStallNotClient) {
            as = 'client';
        }
        var json = {
            as: as,
            type: 'file_part',
            meta: {
                index: index,
                token: token,
                extension: fileExtension,
                isLast: isLastPart
            },
            data: base64Data
        };

        await new Promise((resolve, reject) => {
            var callback = () => {
                //console.log('peer#send callback returned');
                resolve();
            };

            this.peer.send(fromPki, JSON.stringify(json), callback);
        });
    }


    //
    const sendFileParts = async () => {
        const BYTE_BUFFER_SIZE = 2048;

        var buffer = Buffer.alloc(BYTE_BUFFER_SIZE);
        var totalRead = 0;

        let fileHandle = await fsPromises.open(filePath, 'r');
        const decoder = new StringDecoder('base64');

        var payloadCounter = 0;
        var encodedFileParts = [];

        while (true) {
            //console.log('#sendFileParts loop: ' + payloadCounter);
            var readResult = await fileHandle.read(buffer, 0, buffer.length, totalRead);
            totalRead += readResult.bytesRead;

            if (readResult.bytesRead == 0) break;

            var b0 = Buffer.alloc(readResult.bytesRead);
            buffer.copy(b0, 0, 0, readResult.bytesRead);

            encodedFileParts.push(decoder.write(b0));

            if (encodedFileParts.length > 9) {
                //concat contents of `encodedFileParts` &
                //...send them out & await completed-callback &
                //...& only then resume with the next while-loop
                await sendPart(payloadCounter, encodedFileParts.join(''), false);
                payloadCounter++;
                encodedFileParts.splice(0, encodedFileParts.length);
            }
        }

        //console.log('#sendFileParts post loop');

        encodedFileParts.push(decoder.end());
        await sendPart(payloadCounter, encodedFileParts.join(''), true);

        fileHandle.close();

        //console.log('#sendFileParts last-part has been sent');
    };



    await sendFileParts();
};




WifiDirectHelper.prototype.onIncomingFilePart = async function(
    fromPki,
    fromStallNotClient,
    meta,
    data
) {
    const token = meta.token;
    const isLast = meta.isLast;

    var partTracking = this.receivedFilePartTracking[token];
    if (!partTracking) {
        if (meta.index != 0) throw new Error('part tracking should have been initialized by now');

        partTracking = {
            token: token,
            expectingPart: 0,
            extension: meta.extension
        };
        this.receivedFilePartTracking[token] = partTracking;
    }


    if (partTracking.expectingPart != meta.index) {
        throw new Error('part tracking detected a part-gap');
    }
    partTracking.expectingPart = meta.index + 1;


    //handle the part; write to file, append
    //
    const fileRequestMeta = this.expectingFiles[token];

    var fileHandle = partTracking.fileHandle;
    if (!fileHandle) {
        const finalPath = fileRequestMeta.destinationDirectory + path.sep +
            fileRequestMeta.resultingFileName + '.' + partTracking.extension;
        fileHandle = await fsPromises.open(finalPath, 'a');
        partTracking.fileHandle = fileHandle;
        partTracking.finalName = fileRequestMeta.resultingFileName + '.' + partTracking.extension;
        partTracking.finalPath = finalPath;
        partTracking.start = new Date();
    }

    var b = Buffer.from(data, 'base64');
    await fileHandle.appendFile(b);


    if (isLast) {
        //cleanup the part-tracking
        const downloadCompletedCallback = fileRequestMeta.onSuccess;

        delete this.expectingFiles[token];
        delete this.receivedFilePartTracking[token];
        fileHandle.close();

        setTimeout(() => {
            downloadCompletedCallback(partTracking.finalName);
        }, 1);

    }
};




module.exports = WifiDirectHelper;





