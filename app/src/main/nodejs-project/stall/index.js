'use strict';


const WebSocket = require('ws');
const queryString = require('query-string');

const doSetupStallConnectionOverNsd = require('./setup-stall-connection-over-nsd');

const RequestManager = require('../request-manager');

const SessionManager = require('./session-manager');

const NearbyCommsHelper = require('./nearby-comms-helper');

const WifiDirectHelper = require('./wifi-direct-helper');









//todo
//todo
//todo
//todo move image bundled with the project to its expected directory


const fs = require('fs');
const path = require('path');


let appDir = path.dirname(require.main.filename);
let projectParentDir = path.resolve(appDir, '..');

let stallDir = projectParentDir + path.sep + 'Stall';
fs.mkdirSync(stallDir, { recursive: true });


let bundledStaticDir = appDir + path.sep + 'static';
let bundledImage = bundledStaticDir + path.sep + 'teehee.jpg';

let bundledImageDestinationPath = stallDir + path.sep + 'teehee.jpg';

// File destination.txt will be created or overwritten by default.
fs.copyFile(bundledImage, bundledImageDestinationPath, (err) => {
  if (err) throw err;
  console.log('...... teehee.jpg was copied to destination.txt');
});


//todo
//todo
//todo











function Stall(context) {
    this.context = context;
    context.stall = this;


    this.nearbyComms = new NearbyCommsHelper(this);
    this.wifiDirectComms = new WifiDirectHelper(this);



    //TODO
    //todo
    //todo
    //todo
    let stallClientWorkRequests = {};


    const GetLayoutServerSideHandler = require('./server-side/handlers/get-layout');
    let stallClientRequestHandlers = [
        new GetLayoutServerSideHandler(this.context)
    ];




    this.discovery = {
        isActive: false,
        foundStalls: [],
        onDiscoveryStateChanged: (isActive, foundStallArray) => {
            try {
                let didDeactivate = this.discovery.isActive == true && isActive == false;
                let didActivate = this.discovery.isActive == false && isActive == true;
                this.discovery.isActive = isActive;
                this.discovery.foundStalls = foundStallArray;


                if (didActivate) {
                    this.nearbyComms.ensureNearbyCommsIsSetupAppropriately();
                    this.wifiDirectComms.ensureIsSetupAppropriately();
                }

                if (didDeactivate) {
                    //all clients must disconnect from all connected-stalls
                    Object.values(this.peers.connected).forEach((stallInterface) => {
                        stallInterface.onDisconnected();
                    });

                    this.nearbyComms.ensureNearbyCommsIsTornDownAppropriately();
                    this.wifiDirectComms.ensureIsTornDownAppropriately();
                }

            } catch (e) {
                console.log(e);
            }
        }
    };




    this.my = {
        wss: null,
        server: null,
        clients: {},
        config: null,
        onClientConnected: (clientInterface) => {
            let serverSideRequestManager = new RequestManager(
                'stall_client_work_request',
                'stall_work_request',
                stallClientWorkRequests,
                stallClientRequestHandlers,
                (message) => {
                    clientInterface.sendEncrypted(message);
                }
            );

            clientInterface.requestManager = serverSideRequestManager;

            clientInterface.onDisconnected = () => {
                delete this.my.clients[clientInterface.id];
                console.log('...stall-client disconnected; pki: ' + clientInterface.pki);
            }
            clientInterface.onMessageDecrypted = (message) => {
                this.my.handleClientMessage(clientInterface, message);
            }

            this.my.clients[clientInterface.id] = clientInterface;

            console.log(`stall-wss received a new connection; requestManager has been initialized; id:${clientInterface.id} clientPki:${clientInterface.pki}`)
        },
        handleClientMessage: (clientInterface, messageAsString) => { ///todo get rid of this function
            clientInterface.requestManager.onReceivedMessage(messageAsString);
            console.log(`#stall-host-server web-socket incoming message; clientPki:${clientInterface.pki}; message:${messageAsString};`)

        },
        handleConfigChanged: (
            pkiPrivateKey,
            pkiKey,
            paymail,
            title,
            forceUsePaymailPki,
            customPkiWifPrivate,
            connectionType
        ) => {

            //todo
            //todo
            //todo rm the log
            console.log('#stall.my.handleConfigChanged ... connectionType: ' + connectionType);

            let config = {
                pkiPrivateKey: pkiPrivateKey,
                pkiKey: pkiKey,
                paymail: paymail,
                title: title,
                forceUsePaymailPki: forceUsePaymailPki,
                customPkiWifPrivate: customPkiWifPrivate,
                connectionType: connectionType
            };

            this.my.config = config;

            this.wifiDirectComms.onStallConfigChanged(config);
        }
    };



    this.my.util = {
        state: {},
        onStallInfoChanged: (info) => {
            this.my.util.state['info'] = info;
            console.log('`info` of my-stall has been updated');
        },
        getStallInfo: () => {
            let info = this.my.util.state['info'];
            if (info == undefined) {
                return "UNSET";
            } else {
                return info;
            }
        }
    };







    this.passP2pEventToAndroid = (errorCode) => {
        var workRequest = this.context.requestManager.forwardRequests.requests.informP2pEvent
                    .buildRequest(errorCode, null);

        this.context.requestManager.forwardRequests.send(workRequest);
    }











    const GetLayoutClientSideRequest = require('./client-side/requests/get-layout');
    let stallWorkRequests = {
        getLayoutClientSideRequest: new GetLayoutClientSideRequest(this.context)
    };

    //todo
    //todo
    //todo
    let stallRequestHandlers = [];







    this.peers = {
        connected: {},
        sessionManager: new SessionManager(this.context, () => { return this.my.config }),
        connectToStall: (stallName, title, paymail, host, port, pki) => { ///todo merge with the function below this one

            var stallInterface = null;
            if ('wifi' == this.my.config.connectionType) {
                stallInterface = doSetupStallConnectionOverNsd(this.context, this.my.config,
                    stallName, title, paymail, host, port, pki);

            } else {
                throw new Error('unexpected use');
            }


            this.peers.connected[stallName] = stallInterface;

            let clientSideRequestManager = new RequestManager(
                'stall_work_request',
                'stall_client_work_request',
                stallWorkRequests,
                stallRequestHandlers,
                (message) => {
                    stallInterface.sendEncrypted(message);
                }
            );
            stallInterface.requestManager = clientSideRequestManager;



            stallInterface.onMessageDecrypted = (message) => {
                console.log('#stall-client web-socket incoming message; stallPki:' + stallInterface.pki + ' message:' +  message);
                clientSideRequestManager.onReceivedMessage(message);
            }

            stallInterface.onConnected = () => {
                //use this opportunity to initialize with the stall
                this.peers.sessionManager.add(stallInterface);

            }

            stallInterface.onDisconnected = () => {
                //an opportunity to do cleanup related to this peer-connection
                delete this.peers.connected[stallName]
                this.peers.sessionManager.remove(stallInterface);
                console.log('...this client disconnected from stall; pki: ' + stallInterface.pki);
            }

            stallInterface.onError = (code) => {
                //todo request repeated #resolve ...maybe?
                this.passP2pEventToAndroid(code);
            }
        },
        onStallConnectedViaNearby: (stallInterface) => { //todo this needs to be renamed as it is now also used from wifi-direct-integration
            console.log('onStallConnectedViaNearby: ' + JSON.stringify(stallInterface)); //todo rm

            this.peers.connected[stallInterface.stallName] = stallInterface


            let clientSideRequestManager = new RequestManager(
                'stall_work_request',
                'stall_client_work_request',
                stallWorkRequests,
                stallRequestHandlers,
                (message) => {
                    stallInterface.sendEncrypted(message);
                }
            );
            stallInterface.requestManager = clientSideRequestManager;

            stallInterface.onMessageDecrypted = (message) => {
                console.log('#stall-client incoming message; stallPki:' + stallInterface.pki + ' message:' +  message);
                clientSideRequestManager.onReceivedMessage(message);
            }

            stallInterface.onConnected = () => {
                //use this opportunity to initialize with the stall
                this.peers.sessionManager.add(stallInterface);

            }

            stallInterface.onDisconnected = () => {
                //an opportunity to do cleanup related to this peer-connection
                delete this.peers.connected[stallInterface.stallName]
                this.peers.sessionManager.remove(stallInterface);
            }

            stallInterface.onError = (code) => {
                this.passP2pEventToAndroid(code);
            }

        }
    };



    // setup informing the android side of up-to-date layouts of connected-stalls
    this.peers.sessionManager.onStateChanged = (stallInterfaceArray) => {
        this.passCurrentStallSessionStateToAndroidSide();
    };

}

module.exports = Stall;





Stall.prototype.passCurrentStallSessionStateToAndroidSide = function() {
    let currentLayout = this.serializeCurrentLayoutsOfConnectedStalls(); //json-array
    let connectedStalls = this.serializeCurrentConnectedStallState(); //json-array

    var callbackInform = (status) => {
        if (!status) console.log('response to -- informP2pConnectedStallChange... success:' + status);
    };
    var workRequest = this.context.requestManager.forwardRequests.requests.informP2pConnectedStallChange
            .buildRequest(currentLayout, connectedStalls, callbackInform);

    this.context.requestManager.forwardRequests.send(workRequest);


};


Stall.prototype.serializeCurrentLayoutsOfConnectedStalls = function() {
    let connected = Object.values(this.peers.connected);
    if (connected.length == 0) {
        return [];
    }

    return connected.map((stallInterface) => {
        return {
            layout: this.peers.sessionManager.getLayoutState(stallInterface),
            name: stallInterface.stallName,
            paymail: stallInterface.paymail,
            pki: stallInterface.pki
        }
    });
};

Stall.prototype.serializeCurrentConnectedStallState = function() {
    let connected = Object.values(this.peers.connected);
    if (connected.length == 0) {
        return [];
    }

    return connected.map((stallInterface) => {
        let json = {};
        json['name'] = stallInterface.stallName;
        json['paymail'] = stallInterface.paymail;
        json['pki'] = stallInterface.pki;

        return json;
    });
};