'use strict';




function SendLogs(context) {
    this.context = context
    this.command = 'SEND_LOGS'
}


module.exports = SendLogs;

/*
expected `bufferedLogs` structure
[
    "abc 00",
    "abc 01",
    "abc 02",
    ..
]

*/
SendLogs.prototype.buildRequest = function(bufferedLogs, callback) {
    var params = Array.from(bufferedLogs);

    return {
        command: this.command,
        params: params,
        callback: callback
    };
};

SendLogs.prototype.onResponse = function(status, dataArray, request) {


    if (status == 'success') {
        request.callback(true);
        return
    }

    request.callback(false);

};