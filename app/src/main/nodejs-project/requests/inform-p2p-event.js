'use strict';




function InformP2pEvent(context) {
    this.context = context
    this.command = 'INFORM_P2P_EVENT'
}


module.exports = InformP2pEvent;


/*
expected structure
[
    {
        type: type,
        model: {..}
    }
]
*/
InformP2pEvent.prototype.buildRequest = function(type, model/*, callback*/) {
    var params = [{
        type: type,
        model: model
    }];

    return {
        command: this.command,
        params: params,
//        callback: callback
    };
};

InformP2pEvent.prototype.onResponse = function(status, dataArray, request) {


//    if (status == 'success') {
//        request.callback(true);
//        return
//    }
//
//    request.callback(false);

};