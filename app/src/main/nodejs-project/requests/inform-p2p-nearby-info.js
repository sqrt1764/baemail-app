'use strict';




function InformP2pNearbyInfo(context) {
    this.context = context
    this.command = 'INFORM_P2P_NEARBY_INFO'
}


module.exports = InformP2pNearbyInfo;


/*
expected structure
[
    {
        type: 'COMMS_SERVER_META',
        model: {..}
    }

    Possible values of `type`: 'COMMS_SERVER_META', 'REQUEST_MESSAGE_SEND', 'REQUEST_RECEIVE_FILE', 'REQUEST_SEND_SHORT', 'REQUEST_DISCONNECT'

    Possible structures of `model`;
    When 'COMMS_SERVER_META':
    {
        port: 12345
    }

    When 'REQUEST_MESSAGE_SEND':
    {
        endpointId: 'ABC0',
        token: '0011ffaa',
        hasStreamPayload: false,
        bytePayload: ''
    }

    When 'REQUEST_RECEIVE_FILE':
    {
        endpointId: 'ABC0',
        token: '0011ffaa',
        salt: '0011ffaa',
        data: '{fileResourceUri: 'abc01ABC!!'}' [salt-masked]
    }

    When 'REQUEST_SEND_SHORT':
    {
        endpointId: 'ABC0',
        token: '0011ffaa',
        salt: '0011ffaa',
        data: '0011ffaa'
    }

    When 'REQUEST_DISCONNECT':
    {
        endpointId: 'ABC0'
    }
]
*/
InformP2pNearbyInfo.prototype.buildRequest = function(type, model/*, callback*/) {
    var params = [{
        type: type,
        model: model
    }];

    return {
        command: this.command,
        params: params,
//        callback: callback
    };
};

InformP2pNearbyInfo.prototype.onResponse = function(status, dataArray, request) {


//    if (status == 'success') {
//        request.callback(true);
//        return
//    }
//
//    request.callback(false);

};