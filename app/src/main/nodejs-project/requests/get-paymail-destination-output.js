'use strict';



/*

# expected structure of params array
[
    "yaanis@moneybutton.com"
]



# expected structure of result array
[
    "00ff99aa"
]


*/
function GetPaymailDestinationOutput(context) {
    this.context = context
    this.command = 'GET_PAYMAIL_DESTINATION_OUTPUT'
}


module.exports = GetPaymailDestinationOutput;

GetPaymailDestinationOutput.prototype.buildRequest = function(destinationPaymail, callback) {
    var params = [destinationPaymail];

    return {
        command: this.command,
        params: params,
        callback: callback
    };
};

GetPaymailDestinationOutput.prototype.onResponse = function(status, dataArray, request) {
    if (status == 'success') {
        var paymentOutputScript = dataArray[0];
        request.callback(true, paymentOutputScript);
        return
    }

    request.callback(false, null);

};