'use strict';




function SignRequest(context) {
    this.context = context
    this.command = 'SIGN_REQUEST'
}


module.exports = SignRequest;



/*

# expected structure of params array
[
    "keyAlias",
    "m/some/path/0",
    "message content"
]



# expected structure of result array
[
    "signature"
]


*/
SignRequest.prototype.buildRequest = function(callback, keyAlias, path, message) {
    var params = [keyAlias, path, message];

    return {
        command: this.command,
        params: params,
        callback: callback
    };
};

SignRequest.prototype.onResponse = function(status, dataArray, request) {

    if (status == 'success') {
        var signature = dataArray[0];
        request.callback(true, signature);
        return
    }

    request.callback(false, null);

};