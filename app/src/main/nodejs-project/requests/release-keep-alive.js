'use strict';




function ReleaseKeepAlive(context) {
    this.context = context
    this.command = 'RELEASE_KEEP_ALIVE'
}


module.exports = ReleaseKeepAlive;

ReleaseKeepAlive.prototype.buildRequest = function(callback) {
    return {
        command: this.command,
        params: [],
        callback: callback
    };
};

ReleaseKeepAlive.prototype.onResponse = function(status, dataArray, request) {


    if (status == 'success') {
        request.callback(true);
        return
    }

    request.callback(false);

};