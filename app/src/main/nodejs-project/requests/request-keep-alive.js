'use strict';




function RequestKeepAlive(context) {
    this.context = context
    this.command = 'REQUEST_KEEP_ALIVE'
}


module.exports = RequestKeepAlive;


RequestKeepAlive.prototype.buildRequest = function(callback) {
    return {
        command: this.command,
        params: [],
        callback: callback
    };
};

RequestKeepAlive.prototype.onResponse = function(status, dataArray, request) {


    if (status == 'success') {
        request.callback(true);
        return
    }

    request.callback(false);

};