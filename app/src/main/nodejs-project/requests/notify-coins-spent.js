'use strict';




function NotifyCoinsSpent(context) {
    this.context = context
    this.command = 'NOTIFY_COINS_SPENT'
}


module.exports = NotifyCoinsSpent;

/*
expected `spentCoins` structure
[
    {
        spentCoin: {
            tx_id: '00..ff',
            address: '1FFF..FF',
            index: 0
        },
        spendingTxId: '01..ff'
    },
    {
        spentCoin: {
            tx_id: '00..ee',
            address: '1eee..ee',
            index: 2
        },
        spendingTxId: '01..ff'
    },
    ..
]

*/
NotifyCoinsSpent.prototype.buildRequest = function(spentCoins, callback) {
    var params = [spentCoins];

    return {
        command: this.command,
        params: params,
        callback: callback
    };
};

NotifyCoinsSpent.prototype.onResponse = function(status, dataArray, request) {


    if (status == 'success') {
        request.callback(true);
        return
    }

    request.callback(false);

};