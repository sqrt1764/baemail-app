'use strict';




function InformP2pConnectedStallChange(context) {
    this.context = context
    this.command = 'INFORM_P2P_CONNECTED_STALL_CHANGE'
}


module.exports = InformP2pConnectedStallChange;

/*
expected structure
[
    {
        currentLayout: [{
            layout: { .. },
            name: 'aa'',
            paymail: 'bbb'',
            pki: 'ccc'
        }, .. ],
        connectedStalls: [
            {
                name: 'aaa',
                paymail: 'bb',
                pki: 'ccc'
            },
            ..
        ]
    }
]

///////////////
///////////////
///////////////
///////////////
///////////////
supported layout-items

{
   type: "text",
   style: "body",
   value: "Loading info; requesting it from a 'stall' hosted by this peer device."
}
// body/h1/h2/caption

{
   type: "gap_0",
   size: 0.7
}


{
   type: "progress_0",
   size: 0.6
}

{
    type: "toolbar",
    label: "Welcome",
    labelStyle: "h2",
    buttonValue: `
        console.log('a script executed in node using eval(..)');
    `
}
// a toolbar with a `back` button on the left

{
    type: "button",
    value: `console.log('a script executed in node using eval(..)');`,
    label: "Request IRL service"
}
//full width button


{
    type: "button_disconnect_from_stall",
    label: "Title goes here",
    labelStyle: "h2",
    buttonLabel: "Leave"
}
//button is right-aligned; title is left-aligned



{
   type: "image",
   showCaption: true,
   caption: "photo of product from top",
//   downloadPath: "/file/teehee",
   filePath: "/data/data/a/b/c/lol.jpg"
}


*/
InformP2pConnectedStallChange.prototype.buildRequest = function(currentLayout, connectedStalls, callback) {
    var params = [{
        currentLayout: currentLayout,
        connectedStalls: connectedStalls
    }];

    return {
        command: this.command,
        params: params,
        callback: callback
    };
};

InformP2pConnectedStallChange.prototype.onResponse = function(status, dataArray, request) {


    if (status == 'success') {
        request.callback(true);
        return
    }

    request.callback(false);

};