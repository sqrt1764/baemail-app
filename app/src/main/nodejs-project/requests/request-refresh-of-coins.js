'use strict';




function RequestRefreshOfCoins(context) {
    this.context = context
    this.command = 'REQUEST_REFRESH_OF_COINS'
}


module.exports = RequestRefreshOfCoins;

/*
expected `coinsToCheck` structure
[
    {
        tx_id: '00..ff',
        address: '1FFF..FF',
        index: 0
    },
    {
        tx_id: '00..ee',
        address: '1eee..ee',
        index: 2
    },
    ..
]

*/
RequestRefreshOfCoins.prototype.buildRequest = function(coinsToCheck, callback) {
    var params = [coinsToCheck];

    return {
        command: this.command,
        params: params,
        callback: callback
    };
};

RequestRefreshOfCoins.prototype.onResponse = function(status, dataArray, request) {


    if (status == 'success') {
        request.callback(true);
        return
    }

    request.callback(false);

};