'use strict';

const fs = require('fs');
const path = require('path');

//const Twetch = require('@twetch/sdk/dist/twetch.node.min.js');
const Twetch = require('./client');

const Storage = require('./storage')
const Wallet = require('./wallet')
const CoinReservation = require('./coin-reservation')


function TwetchHelper(context) {
    this.context = context;

    context.twetch = this;

    this.instance = undefined;


    this.twetchPrivWIF = undefined; //this will be initialized during #onAuthenticated
    this.isAuthenticated = false; //flag that indicates that the twetch-instance has been authenticated


    //todo do these need to be cleared on #setup being called with different paymail
    this.runOnSetup = [];
    this.runOnAuth = [];
}

module.exports = TwetchHelper;


TwetchHelper.prototype.ensureSetupHasCompleted = async function() {
    if (!this.instance) {
        //twetch instance has not yet been initialised
        await new Promise((resolve, reject) => {
            this.runOnSetup.push(p0 => {
                resolve();
            });
            console.log('twetch.runOnSetup registered');
        }).catch((e) => {
            console.log(e);
        });
        //twetch instance now exists
    }
}

TwetchHelper.prototype.ensureAuthHasCompleted = async function() {
    if (!this.isAuthenticated) {
        //twetch instance has not yet been authenticated
        await new Promise((resolve, reject) => {
            this.runOnAuth.push(p0 => {
                console.log('executing lambda registered to >> twetch.runOnAuth');
                resolve();
            });
            console.log('twetch.runOnAuth registered');
        }).catch((e) => {
            console.log(e);
        });
        //twetch instance is now authenticated
    }
}


TwetchHelper.prototype.onAuthenticated = async function(publicKeysArray) {
    if (!this.instance) {
        throw new Error("unexpected usage");
    }

    //extract the private-key used in twetch-chat
    await this.initTwetchWallet(
        publicKeysArray
    );

    this.isAuthenticated = true;

    //execute code queued up for execution after authentication

    let lambdasToRun = this.runOnAuth;
    this.runOnAuth = [];

    console.log('TwetchHelper#onAuthenticated; lambdasToRun.size:' + lambdasToRun.length);
    lambdasToRun.forEach((item, i) => {
        try {
            //console.log('TwetchHelper#onAuthenticated; lambdasToRun i:' + i);
            item();
        } catch (e) {
            console.log(Error('TwetchHelper#onAuthenticated; crash executing i:' + i));
            //console.log(e);
        }
    })
}




/*
#example structure of `encryptedMnemonicArray`
[{
    "id": "69cdeb4d-f28f-4b61-9e91-4f5fc5fbe715",
    "walletType": "onebutton",
    "signingAddress": null,
    "identityPublicKey": "03efcfbda3804876ea0056e24208b9620abffdff5ffd9c235c549df91357b025d6",
    "encryptedMnemonic": "QklFMQK550TctQb0hGnhpZe+z8e/d85u7/llArVJ0pWJiNniRyruH74xRLf95w+ObxtqmApsmGTUSQTTQix1iw/kC/+ZVtvexg5ax41laj4j1wRnNx7XYLA0PKjMgCCNBfDspUMMB4F05EUm7hmlcvPDTAqpMzMVJftGGMOpDJmmEfOfpJU9sPcdWW58u6YqcnB+1YiYpyhBgtLnPSG0vRIN/FLC1uq3A/fcQGZG3oev3oBBwg=="
  }, {
    "id": "b09f0394-2425-4c0e-8b64-1c82d930b473",
    "walletType": "moneybutton",
    "signingAddress": "1MLa4N2xA8bKPPQhGMss3zG53cNdHFsvr9",
    "identityPublicKey": "03f182c6be59037b474ace3e669e3671fdbddd48aa02e667bb7e0074bad342cf74",
    "encryptedMnemonic": "QklFMQOV9R8TeVajbi7NvAK76JS8nZSwzayrjDVICieLA8WykkYeXfDl1LifGcP1lgWlo9inYQqiwVRG+wYFqKqleZj6CMthlaM4ERaYn+RS9dckQC83WRlVE4fd49twi+d/bC0yr0IzzIqpVBZEyTGcx6MOdP31tJRnV2b/RXASnFaLLQ7QAYjxbWRYFxVA9//99V4="
  }]
*/
TwetchHelper.prototype.initTwetchWallet = async function(encryptedMnemonicArray) {
    let paymailPrivateKey = this.instance.options.privateKey;
    let paymailPublicKey = this.context.bsv.PublicKey.fromPrivateKey(paymailPrivateKey).toString();


    let signingAddressItem = encryptedMnemonicArray.find((item) => {
        return item.identityPublicKey == paymailPublicKey
    });
    let encryptedMnemonicValue = signingAddressItem.encryptedMnemonic;

    let twetchCryptoHelper = this.instance.crypto;
    let twetchMnemonic = twetchCryptoHelper.eciesDecrypt(encryptedMnemonicValue, paymailPrivateKey.toWIF());

    if (signingAddressItem.walletType == 'onebutton') {
        let indexOfSeparator = twetchMnemonic.indexOf('::');
        if (indexOfSeparator > 0) {
            twetchMnemonic = twetchMnemonic.substring(indexOfSeparator + 2)
        }
    }
    this.twetchPrivWIF = twetchCryptoHelper.privFromMnemonic(twetchMnemonic.toString());

    console.log('TwetchHelper#initTwetchWallet completed');
}


/*
TODO talk about this.instance.options.coinReservation & obligations that come with it
TODO !!!!
*/
TwetchHelper.prototype.setup = async function(activePaymail, seed, paymailPkiPath) {
    //cleanup if this has been already initialized for a different paymail
    if (this.instance != undefined) {
        if (this.instance.options.paymail == activePaymail) {
            console.log('TwetchHelper#setup instance already initialized for paymail:' + activePaymail);
            return;
        }

        console.log('TwetchHelper#setup existing instance cleaned up');
        this.instance = undefined;

        this.isAuthenticated = false;

    }

    console.log('TwetchHelper#setup paymail: ' + activePaymail);


    let paymailMnemonic = this.context.mnemonic.fromString(seed.join(' '));
    let paymailHdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(paymailMnemonic.toSeed());
    let paymailPkiHdPrivateKey = paymailHdPrivateKey.deriveChild(paymailPkiPath);

    let appDir = path.dirname(require.main.filename);
    let projectParentDir = path.resolve(appDir, '..');

    let twetchDir = projectParentDir + path.sep + 'Twetch';
    fs.mkdirSync(twetchDir, { recursive: true });
    let secretsFile = twetchDir + path.sep + 'paymail_' + activePaymail;


    let options = {
        coinReservation: new CoinReservation(),
        storage: Storage,
        Wallet: Wallet,
        clientIdentifier: 'b7a38647-b420-4dfa-888b-ef18efe4f304', //todo do not hardcode
//        apiUrl: 'https://gw.twetch.app/',
        feeb: 0.5,
        network: 'mainnet',
        filePath: secretsFile,
        context: this.context,
        privateKey: paymailPkiHdPrivateKey.privateKey,
        paymail: activePaymail
    };


    this.instance = new Twetch(options);
    this.instance.recreateHttpClientWithTimeout();

    //todo consider overriding the publish method so that it flushes the spent outputs


    //execute code queued up for execution after setup

    let lambdasToRun = this.runOnSetup;
    this.runOnSetup = [];

    console.log('TwetchHelper#setup; lambdasToRun.size:' + lambdasToRun.length);
    lambdasToRun.forEach((item, i) => {
        try {
            //console.log('TwetchHelper#setup; lambdasToRun i:' + i);
            item();
        } catch (e) {
            console.log(Error('TwetchHelper#setup; crash executing i:' + i));
            //console.log(e);
        }
    })

}

TwetchHelper.prototype.resolveChatImageDirPath = function(chatId) {
    let appDir = path.dirname(require.main.filename);
    let projectParentDir = path.resolve(appDir, '..');

    let twetchDir = projectParentDir + path.sep + 'Twetch';
    let chatImageDir = twetchDir + path.sep + 'chatImage' + path.sep + chatId;
    return chatImageDir;
}

TwetchHelper.prototype.checkChatImageExists = async function(chatId, fileName) {
    let chatImageDir = this.resolveChatImageDirPath(chatId);
    fs.mkdirSync(chatImageDir, { recursive: true });

    return await new Promise((resolve, reject) => {
        fs.readdir(chatImageDir, function (err, files) {
            if (err) {
                console.log('Unable to scan directory: ' + err);
                resolve(undefined);
                return;
            }

            var found = files.find((f) => {
                return f.startsWith(fileName);
            });

            resolve(found);
        });

    }).catch((e) => {
        console.log(e);
    });
}

TwetchHelper.prototype.writeChatImage = function(chatId, fileName, base64FileData) {
    let chatImageDir = this.resolveChatImageDirPath(chatId);
    fs.mkdirSync(chatImageDir, { recursive: true });

    function decodeBase64Image(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
        var response = {};

        if (matches.length !== 3)
        {
        return new Error('Invalid input string');
        }

        response.type = matches[1];
        response.data = Buffer.from(matches[2], 'base64');

        return response;
    }

    var imageBuffer = decodeBase64Image(base64FileData);
    var imageTypeDetected = imageBuffer.type.match(/\/(.*?)$/);


    let imagePath = chatImageDir + path.sep + fileName + '.' + imageTypeDetected[1];

    fs.writeFileSync(imagePath, imageBuffer.data);

    return fileName + '.' + imageTypeDetected[1];
}

/*

#expecting params

action = 'twetch/unfollow@0.0.1'
payload = {
    userId: '${getActiveUserId()}',
    followerUserId: '$userId'
}


#response

{
    published: false,
    errors: ['Action failed']
}

`published` will be TRUE if the message will show up on twetch
`errors` will be an empty array when `published` == TRUE

*/
TwetchHelper.prototype.postTwetchAction = async function(action, payload) {
    let wallet = this.instance.wallet;

    const { abi, payees, invoice } = await this.instance.build(
        action,
        payload
    );
    await abi.replace({
        '#{mySignature}': () => wallet.sign(abi.contentHash()),
        '#{myAddress}': () => wallet.address()
    });
    const tx = await wallet.buildTx(abi.toArray(), payees, action);


    const instanceAbi = this.instance.abi;
    const network = this.instance.network;
    new this.instance.BSVABI(instanceAbi, { network: network })
        .action(action)
        .fromTx(tx.toString());



    var res = undefined;
    try {
        const response = await this.instance.publishRequest({
            signed_raw_tx: tx.toString(),
            invoice,
            action,
            payParams: payload.payParams,
            broadcast: true
        });

        res = { ...response, txid: tx.hash, abi };

    } catch (e) {
        console.log('publishRequest crash: ' + JSON.stringify(e));

        if (e.response && e.response.data && e.response.data.errors) {
            res = e.response.data;
            res.txid = tx.hash;
        } else {
            res = this.instance.handleError(e);
        }
    }


    ///
    //console.log('publish-res:' + JSON.stringify(res));



    var response = {
        published: false,
        errors: ['Action failed']
    };

    let reservationManager = this.instance.options.coinReservation;



    if (res.broadcasted) {
        //notify android-side of spending of coins
        let spentCoins = reservationManager.getReservedCoins(res.txid);

        let reportArray = [];
        spentCoins.forEach((coin) => {
            reportArray.push({
                spentCoin: coin,
                spendingTxId: res.txid
            });
        });


        var callbackNotifyCoinsSpent = (status) => {
            if (!status) console.log('response to -- notifyCoinsSpent... success:' + status);

            reservationManager.clearReservation(res.txid);

        };
        var workRequest = this.context.requestManager.forwardRequests.requests.notifyCoinsSpent
                .buildRequest(reportArray, callbackNotifyCoinsSpent);

        this.context.requestManager.forwardRequests.send(workRequest);


    } else {
        console.log('action failed broadcast; releasing coins reserved in the making of the transaction data-structure');
        let coinsToRelease = reservationManager.getReservedCoins(tx.hash);
        console.log('coinsToRelease: ' + JSON.stringify(coinsToRelease));
        reservationManager.clearReservation(tx.hash);



        //broadcast-failure means that the coins used in the assembling of the transaction were spent...
        //request refresh these coins & see if they have been spent
        var callbackRequestRefreshOfCoins = (status) => {
            if (!status) console.log('response to -- requestRefreshOfCoins... success:' + status + ' ...checking');
        };
        var workRequest = this.context.requestManager.forwardRequests.requests.requestRefreshOfCoins
                .buildRequest(coinsToRelease, callbackRequestRefreshOfCoins);

        this.context.requestManager.forwardRequests.send(workRequest);
    }



    if (res.txid && res.published) {
        response.published = true;
        response.errors = [];

    } else {
        console.log('action did not get published');

        if (res.errors && res.errors.length != 0) {
            response = res;
        }
    }

    return response;
}