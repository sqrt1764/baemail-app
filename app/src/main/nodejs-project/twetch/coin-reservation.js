



class CoinReservation {
    constructor() {
        this.reservations = {};
    }

    makeReservation(txHash, reservedCoins) {
        if (this.reservations[txHash]) {
            throw new Error('reservation already exists');
        }

        console.log('CoinReservation#makeReservation txHash:' + txHash);
        this.reservations[txHash] = reservedCoins;
    }

    getReservedCoins(txHash) {
        return this.reservations[txHash];
    }

    getAllReservedCoins() {
        return Object.values(this.reservations).flat();
    }

    clearReservation(txHash) {
        delete this.reservations[txHash]
    }


}


module.exports = CoinReservation;