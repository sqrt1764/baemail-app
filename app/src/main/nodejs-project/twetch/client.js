'use strict';

const BaseClient = require('@twetch/sdk/dist/twetch.node.min.js');
const axios = require('axios');

class Client extends BaseClient {

    async query(query, variables = {}) {
        try {
            const response = await this.client.post('https://gw.twetch.app', {
                variables,
                query
            });

            return response.data.data;

        } catch (e) {
            if (e && e.response && (e.response.status == 401 || e.response.status == 400)) {
                console.log(e.response.status + ' detected; key:millisLastAuthCheck has been cleared');
                this.storage.removeItem('millisLastAuthCheck');
            }
            console.log('crash when executing #query', e);
            throw e;
        }
    }

    async authenticate(options = {}) {
        try {
            return await super.authenticate(options);
        } catch (e) {
            if (e && e.response && (e.response.status == 401 || e.response.status == 400)) {
                console.log('....' + e.response.status + ' detected');
            }
            throw e;
        }
    }

    recreateHttpClientWithTimeout() {
        this.client = axios.create({
            baseURL: this.options.apiUrl || 'https://api.twetch.app/v1',
            timeout: 15 * 1000,
            headers: {
                Authorization: `Bearer ${this.storage.getItem('tokenTwetchAuth')}`
            }
        });
        console.log('HttpClient will have timeout of 15s; apiUrl: ' + this.options.apiUrl);
    }


    handleError(e) {
    	if (e && e.response && e.response.data) {
    		console.log(e.response.data);
    		if (e.response.status === 401) {
    			return { error: 'unauthenticated' };
    		}
    	} else if (e.toString) {
    		console.log(e.toString());
    		return { error: e.toString() };
    	} else {
    		return { error: e };
    		console.log(e);
    	}

    	return { error: 'something went wrong' };
    }

}


module.exports = Client;