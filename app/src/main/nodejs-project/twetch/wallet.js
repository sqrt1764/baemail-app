'use strict';

const _Buffer = require('buffer/');

const BaseWallet = require('@twetch/sdk/src/wallet/base-wallet.js');
//const Message = require('../../bsvabi/bsv/message');

//todo
//todo
//todo
class Wallet extends BaseWallet {
	constructor(options = {}) {
		super(options);
//		const Storage = options.Storage;

//		this.storage = new Storage(options);
		this.feeb = options.feeb;
		this.network = options.network;

		this.options = options;

		this.PrivateKey = options.context.bsv.PrivateKey;
        this.Address = options.context.bsv.Address;
        this.Transaction = options.context.bsv.Transaction;
        this.Script = options.context.bsv.Script;
        this.Opcode = options.context.bsv.Opcode;
        this.Message = options.context.bsv.Message;



	}

	get privateKey() {
	    return this.options.privateKey;
	}

	address() {
		return this.options.privateKey.toAddress().toString();
	}

	sign(message) {
		return this.Message.sign(message, this.privateKey);
	}

	async balance() {
	    const availableCoinsInfo = this.options.context.availableCoins;
        if (!availableCoinsInfo) return 0;

	    let coinsArray = availableCoinsInfo.coins;
        let totalSats = coinsArray.reduce((acc, coin) => { return acc + coin.sats }, 0);

        console.log('calculated balance:' + totalSats); //todo rm

        return totalSats;

//		const response = await axios.get(`${this.rpc}/addr/${this.address()}/utxo`);
//		return response.data.reduce((a, e) => a + e.satoshis, 0);
	}
//
//	get rpc() {
//		return {
//			mainnet: 'https://api.bitindex.network/api',
//			testnet: 'https://api.bitindex.network/api/v3/test'
//		}[this.network];
//	}
//
//	async utxos() {
//		const response = await axios.post(`${this.rpc}/addrs/utxo`, {
//			addrs: [this.address()].join(',')
//		});
//		return response.data;
//	}
//


    /*
     * slightly modified copy from build-transaction.js
     */
    _script(data) {
        let s = new this.Script();
        s.add(this.Opcode.OP_FALSE);
        s.add(this.Opcode.OP_RETURN);
        data.forEach(function(item) {
            if (item.constructor.name === 'ArrayBuffer') {
                let buffer = _Buffer.Buffer.from(item);
                s.add(buffer);
            } else if (item.constructor.name === 'Buffer') {
                s.add(item);
            } else if (typeof item === 'string') {
                if (/^0x/i.test(item)) {
                    s.add(Buffer.from(item.slice(2), 'hex'));
                } else {
                    s.add(Buffer.from(item));
                }
            } else if (typeof item === 'object' && item.hasOwnProperty('op')) {
                s.add({ opcodenum: item.op });
            }
        });
        return s;
    };

    findAppropriateUtxos(availableCoins, totalPaySatsRequired, dataOutputSize) {

        //calculate the amount of satoshis required for the assembling transaction
        let estimatedDataOutputFee = Math.ceil(dataOutputSize * this.feeb);
        let satsRequiredForInputSizeFee = 2000; //todo crude & ugly
        let totalMinimumSatsRequired = totalPaySatsRequired
            + estimatedDataOutputFee
            + satsRequiredForInputSizeFee;


        //exclude reserved-available-coins (this is only really required if building multiple
        // transactions without publishing them serially)
        let reservedCoins = this.options.coinReservation.getAllReservedCoins();
        let filteredCoins = availableCoins.filter(coin => !reservedCoins.includes(coin));
        filteredCoins.sort(function(a, b) {
            return a.sats - b.sats;
        });


        //build data-structures for returning
        const availableCoinsInfo = this.options.context.availableCoins;
        let fundingWalletMnemonic = this.options.context.mnemonic.fromString(availableCoinsInfo.fundingWalletSeed.join(' '));
        let fundingWalletHdPrivateKey = this.options.context.bsv.HDPrivateKey.fromSeed(fundingWalletMnemonic.toSeed());
        let fundingWalletHdPrivateKeyRoot = fundingWalletHdPrivateKey.deriveChild(availableCoinsInfo.fundingRootPath);

        let usedCoinSet = [];
        let privateKeySet = [];

        for (var i = 0; i < filteredCoins.length; i++) {
            let spendingCoin = filteredCoins[i];
            let path = spendingCoin.derivation_path;
            let pathPrivateKey = fundingWalletHdPrivateKeyRoot.deriveChild("m" + path).privateKey;

            usedCoinSet.push(spendingCoin);
            privateKeySet.push(pathPrivateKey);

            //check if enough
            let satsInUsedCoins = usedCoinSet.reduce((acc, v) => { return acc + v.sats }, 0);
            if (satsInUsedCoins > totalMinimumSatsRequired) {
                break;
            }
        }

        //final check for the minimum-size of inputs
        var satsInUsedCoins = usedCoinSet.reduce((acc, v) => { return acc + v.sats }, 0);
        if (satsInUsedCoins < totalMinimumSatsRequired) {
            throw new Error('request dropped, not enough BSV');
        }


        var spendingOutputSet = [];
        for (let i = 0; i < usedCoinSet.length; i++) {
            let coin = usedCoinSet[i];
            spendingOutputSet.push({
                address: coin.address,
                txId: coin.tx_id,
                outputIndex: coin.index,
                script: this.Script.buildPublicKeyHashOut(coin.address).toString(),
                satoshis: coin.sats
            });
        }

        return {
            outputs: spendingOutputSet,
            privateKeys: privateKeySet,
            coins: usedCoinSet
        }
    }


	async buildTx(data, payees = [], options = {}) {
	    let millisStartOfBuildTx = new Date().getTime();

        const availableCoinsInfo = this.options.context.availableCoins;
        if (!availableCoinsInfo) throw new Error('not ready yet!');


		const payToList = payees.map(e => ({
			address: e.to,
			value: parseInt((e.amount * 100000000).toFixed(0), 10)
		}));

		let totalPaySatsRequired = payToList.reduce((acc, item) => {
		    return acc + item.value;
		}, 0);




        let scriptOutput = null;
        let dataOutputSize = 0;
        if (data) {
            let script = this._script(data);
            scriptOutput = new this.Transaction.Output({ script: script, satoshis: 0 });
            dataOutputSize = scriptOutput.getSize();
        }


        let coinsArray = availableCoinsInfo.coins;
        let utxoCoins = this.findAppropriateUtxos(
            coinsArray,
            totalPaySatsRequired,
            dataOutputSize
        );

        let changeAddress = availableCoinsInfo.upcomingChangeAddressList[0].address;





        //assemble the TX
        let tx = new this.Transaction().from(utxoCoins.outputs);

        if (scriptOutput) {
            tx.addOutput(scriptOutput);
        }

        payToList.forEach((receiver) => {
            tx.to(receiver.address, receiver.value);
        });


        {//part copied from build-transaction.js
            tx.fee(0).change(changeAddress);
            let myfee = Math.ceil(tx._estimateSize() * this.feeb);
            tx.fee(myfee);

            for (let i = 0; i < tx.outputs.length; i++) {
                if (tx.outputs[i]._satoshis > 0 && tx.outputs[i]._satoshis < 546) {
                    tx.outputs.splice(i, 1);
                    i--;
                }
            }
        }

        tx.sign(utxoCoins.privateKeys);


        // Android-side must be informed of spends of utxoCoins.coins;
        // note the coins used in the inputs of the TX so the information is not lost &
        // can be propagated to Android-side when it makes sense.
        this.options.coinReservation.makeReservation(tx.hash, utxoCoins.coins);




	    let millisEndOfBuildTx = new Date().getTime();

	    console.log("time spent building transaction-data-structure:"
        + (millisEndOfBuildTx - millisStartOfBuildTx)
        + "ms");

        return tx;
	}
}

module.exports = Wallet;
