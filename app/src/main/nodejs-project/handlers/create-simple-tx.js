'use strict';




function CreateSimpleTx(context) {
    this.context = context
    this.command = "CREATE_SIMPLE_TX"
}

module.exports = CreateSimpleTx;



/*
#expecting params
[
    ['seed', 'words', ..],
    [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/13'}, {..}],
    '1FFF..EE', //destination address
    123, //sats
]

# response
[
    {
        'hexTx': '1f1f1f1f1f',
        'hash': '1f1f',
        'output': {
            address: '1C6BBGPhq57UCA1DxK5HQkT7Essr8zzLXJ',
            outputIndex: 0,
            sats: 123
        }
    }
]
*/
CreateSimpleTx.prototype.handle = function(params, callback) {

    var Transaction = this.context.bsv.Transaction;
    var Script = this.context.bsv.Script;

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        //verify params
        if (params.length != 4) throw new Error("invalid params");
        var seedPhraseArray = params[0];
        var spendingCoinsArray = params[1];
        var destinationAddress = params[2];
        var sats = params[3];

        //get hd-priv-key
        var phrase = seedPhraseArray.join(' ');
        var mnemonic = this.context.mnemonic.fromString(phrase);
        var hdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(mnemonic.toSeed());

        var hdPrivateKeyRoot = hdPrivateKey.deriveChild(this.context.fundingWalletDerivationPathRoot);

        //assemble private-key-set
        var privateKeySet = [];

        for (var i = 0; i < spendingCoinsArray.length; i++) {
            var spendingCoin = spendingCoinsArray[i];
            var path = spendingCoin.derivation_path;
            var pathPrivateKey = hdPrivateKeyRoot.deriveChild("m" + path).privateKey;

            privateKeySet.push(pathPrivateKey);
        }

        //assemble spending-output-set
        var spendingCoinsSet = [];
        var totalSats = 0;

        for (var i = 0; i < spendingCoinsArray.length; i++) {
            var spendingCoin = spendingCoinsArray[i];
            spendingCoinsSet.push({
                address: spendingCoin.address,
                txId: spendingCoin.tx_id,
                outputIndex: spendingCoin.index,
                script: Script.buildPublicKeyHashOut(spendingCoin.address).toString(),
                satoshis: spendingCoin.sats
            });

            totalSats += spendingCoin.sats;
        }

        //make and serialize the tx
        var transaction = new Transaction()
            .from(spendingCoinsSet);

        transaction.addOutput(new Transaction.Output({
            script: Script.fromAddress(destinationAddress),
            satoshis: sats
        }));

        var addedOutput = {
            address: destinationAddress,
            outputIndex: 0,
            sats: sats
        };


        transaction.sign(privateKeySet);

        var txHash = transaction.hash;
        var txInHex = transaction.serialize(true);

        //finish the response
        responseData = [{
            hexTx: txInHex,
            hash: txHash,
            output: addedOutput
        }];
        isSuccess = true;

    } catch (e) {
        responseData = [
            e.stack
        ];
    }

     callback(isSuccess, responseData);



//todo https://medium.com/namjungsoo/bitcoinsv-4-%EB%8B%A4%EC%A4%91-%EC%9E%85%EB%A0%A5-transaction-a8f362768de


}