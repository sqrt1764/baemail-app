'use strict';




function CheckMnemonicIsValid(context) {
    this.context = context
    this.command = "CHECK_MNEMONIC_IS_VALID"
}

module.exports = CheckMnemonicIsValid;

CheckMnemonicIsValid.prototype.handle = function(params, callback) {

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        var phrase = params.join(' ')
        var mnemonic = this.context.mnemonic.fromString(phrase);
        var hdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(mnemonic.toSeed());

        //did not crash .... success

        responseData = [];

        isSuccess = true;

    } catch (e) {
        //console.log(e);
        responseData = [
            "not_valid"
        ];
    }

    callback(isSuccess, responseData)
};