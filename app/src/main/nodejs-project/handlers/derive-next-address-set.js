'use strict';




function DeriveNextAddressSet(context) {
    this.context = context
    this.command = "DERIVE_NEXT_ADDRESS_SET"
}

module.exports = DeriveNextAddressSet;

/* #expecting params
[
    ['seed', 'words', ..],
    'm/0/1',
    0 //derived key-set index
]
with index `0` should generate paths
/0
/1
/2
/3
/4
..
/18
/19

# response
[
    {'path': '/0', address: '1C6BBGPhq57UCA1DxK5HQkT7Essr8zzLXJ'},
    {'path': '/1', address: '1G7tZTw1z1XTuE7xsDR5J5s6uz57PrmYe1'},
    ..
]
*/
DeriveNextAddressSet.prototype.handle = function(params, callback) {

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        if (params.length != 3) throw new Error("invalid params");
        var seedPhraseArray = params[0]
        var path = params[1]
        var keySetIndex = params[2]



        var phrase = seedPhraseArray.join(' ')
        var mnemonic = this.context.mnemonic.fromString(phrase);
        var hdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(mnemonic.toSeed());
        var hdPrivateKeyRoot = hdPrivateKey.deriveChild(path);

        var generatedAddresses = []

        for (var i = 0; i < 20; i++) {
            var path = "/" + (keySetIndex * 20 + i)
            // var path = "/" + keySetIndex + "/" + i
            var hdPrivateKeyDerived = hdPrivateKeyRoot.deriveChild("m" + path);
            var publicKey = this.context.bsv.PublicKey.fromPrivateKey(hdPrivateKeyDerived.privateKey);
            var address = this.context.bsv.Address.fromPublicKey(publicKey)

            generatedAddresses.push({
                path: path,
                address: address.toString()
            })
        }

        responseData = generatedAddresses

        isSuccess = true;

    } catch (e) {
        console.log(e);
        responseData = [
            e.stack
        ];
    }

    callback(isSuccess, responseData)
};
