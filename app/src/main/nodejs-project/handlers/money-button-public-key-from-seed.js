'use strict';




function MoneyButtonPublicKeyFromSeed(context) {
    this.context = context
    this.command = "MONEY_BUTTON_PUBLIC_KEY_FROM_SEED"
}

module.exports = MoneyButtonPublicKeyFromSeed;





MoneyButtonPublicKeyFromSeed.prototype.handle = function(params, callback) {

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        var phrase = params.join(' ')
        var mnemonic = this.context.mnemonic.fromString(phrase);
        var hdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(mnemonic.toSeed());
        var pathPrivateKey = hdPrivateKey.deriveChild(this.context.moneyButtonDerivationPath);

        //console.log('private key: ' + pathPrivateKey.privateKey.toString());

        var publicKey = this.context.bsv.PublicKey.fromPrivateKey(pathPrivateKey.privateKey);
        //console.log('public key: ', publicKey.toString());

        responseData = [
            publicKey.toString()
        ];

        isSuccess = true;

    } catch (e) {
        //console.log(e);
        responseData = [
            e.stack
        ];
    }

    callback(isSuccess, responseData)
};
