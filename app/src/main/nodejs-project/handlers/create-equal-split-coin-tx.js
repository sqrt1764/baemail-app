'use strict';




function CreateEqualSplitCoinTx(context) {
    this.context = context
    this.command = "CREATE_EQUAL_SPLIT_COIN_TX"
}

module.exports = CreateEqualSplitCoinTx;



/*
#expecting params
[
    ['seed', 'words', ..],
    [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/13'}, {..}],
    "m/44'/0'/0'", //spendingCoinRootPath
    [
        {
            address: '1FFF..EE',
            sats: 123
        },
        {
            address: '1F99..EE',
            sats: 1234
        },
        ..
    ] //destination address info list
]

# response
[
    {
        'hexTx': '1f1f1f1f1f',
        'hash': '1f1f',
        'outputs': [
            { address: '1C6BBGPhq57UCA1DxK5HQkT7Essr8zzLXJ' outputIndex: 3, sats: 123},
            ..
        ]
    }
]
*/
CreateEqualSplitCoinTx.prototype.handle = function(params, callback) {

    var Transaction = this.context.bsv.Transaction
    var Script = this.context.bsv.Script

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        //verify params
        if (params.length != 4) throw new Error("invalid params");
        var seedPhraseArray = params[0]
        var spendingCoinsArray = params[1]
        var spendingCoinRootPath = params[2]
        var destinationAddressInfoList = params[3]

        //get hd-priv-key
        var phrase = seedPhraseArray.join(' ')
        var mnemonic = this.context.mnemonic.fromString(phrase);
        var hdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(mnemonic.toSeed());

        var hdPrivateKeyRoot = hdPrivateKey.deriveChild(spendingCoinRootPath);

        //assemble private-key-set
        var privateKeySet = [];

        for (var i = 0; i < spendingCoinsArray.length; i++) {
            var spendingCoin = spendingCoinsArray[i];
            var path = spendingCoin.derivation_path;
            var pathPrivateKey = hdPrivateKeyRoot.deriveChild("m" + path).privateKey;

            privateKeySet.push(pathPrivateKey)
        }

        //assemble spending-output-set
        var spendingCoinsSet = [];
        var totalSats = 0;

        for (var i = 0; i < spendingCoinsArray.length; i++) {
            var spendingCoin = spendingCoinsArray[i];
            spendingCoinsSet.push({
                address: spendingCoin.address,
                txId: spendingCoin.tx_id,
                outputIndex: spendingCoin.index,
                script: Script.buildPublicKeyHashOut(spendingCoin.address).toString(),
                satoshis: spendingCoin.sats
            })

            totalSats += spendingCoin.sats
        }

        if (totalSats <= 0) {
            throw new Error();
        }


        var transaction = new Transaction().from(spendingCoinsSet);

        //calc sats in part
        var partCount = destinationAddressInfoList.length;
        if (partCount == 0) {
            throw new Error();
        }

        var addedOutputs = [];

        for (var i = 0; i < partCount; i++) {
            var addressInfo = destinationAddressInfoList[i];

            //build outputs & add them to tx
            transaction.addOutput(new Transaction.Output({
                script: Script.fromAddress(addressInfo.address),
                satoshis: addressInfo.sats
            }));

            addedOutputs.push({
                address: addressInfo.address,
                outputIndex: i,
                sats: addressInfo.sats
            });
        }

        transaction.sign(privateKeySet);

        var txHash = transaction.hash;
        var txInHex = transaction.serialize(true);

        //finish the response
        responseData = [{
            hexTx: txInHex,
            hash: txHash,
            outputs: addedOutputs
        }];
        isSuccess = true;

    } catch (e) {
        responseData = [
            e.stack
        ];
    }

    callback(isSuccess, responseData)



//todo https://medium.com/namjungsoo/bitcoinsv-4-%EB%8B%A4%EC%A4%91-%EC%9E%85%EB%A0%A5-transaction-a8f362768de


}