'use strict';


const doSetupStallOverNSD = require('../stall/setup-stall-over-nsd');
//const doSetupStallOverWifiDirect = require('../stall/setup-stall-over-wifidirect');



function StartStallSocketServer(context) {
    this.context = context
    this.command = "START_STALL_SOCKET_SERVER"

}

module.exports = StartStallSocketServer;



/*

# expected structure of params
[
    {
        port: 99999,
        pkiKey: "hex",
        pkiPrivateKey: "hex",
        paymail: "",
        title: "",
        forceUsePaymailPki: false,
        customPkiWifPrivate: "hex",
        connectionType: "nearby"  //possible values: `nearby`, `wifi`
    }
]


# expected structure of the response data passed in the callback
[
    true
]




*/
StartStallSocketServer.prototype.handle = function(params, callback) {

    var isSuccess = false

    var responseData = [
        null
    ];

    try {

        var runAsync = async () => {

            try {

                let config = params[0]

                this.context.stall.my.handleConfigChanged(
                    config.pkiPrivateKey,
                    config.pkiKey,
                    config.paymail,
                    config.title,
                    config.forceUsePaymailPki,
                    config.customPkiWifPrivate,
                    config.connectionType
                );

                if ('nearby' == config.connectionType) {
                    this.context.stall.nearbyComms.onStartedAdvertisingStall();
                    this.context.stall.nearbyComms.ensureNearbyCommsIsSetupAppropriately();

                } else if ('wifi' == config.connectionType) {
                    doSetupStallOverNSD(this.context, this.context.stall.my, config.port);

                } else if ('wifidirect' == config.connectionType) {
                    this.context.stall.wifiDirectComms.onStartedAdvertisingStall();
                    this.context.stall.wifiDirectComms.ensureIsSetupAppropriately();
//                    doSetupStallOverWifiDirect(this.context, this.context.stall.my, config.port);

                } else {
                    throw new Error('unexpected connection-type');
                }


                callback(true, [true])



            } catch (err) {

                responseData = [
                    err.stack
                ]
                console.log(err)

                callback(false, responseData)
            }

        }


        setTimeout(runAsync, 1)

    } catch (e) {
        responseData = [
            e.stack
        ];

        callback(false, responseData);
    }
};