


function InformWifiDirectP2pConnectionChange(context) {
    this.context = context
    this.command = "INFORM_WIFI_DIRECT_P2P_CONNECTION_CHANGED"
}

module.exports = InformWifiDirectP2pConnectionChange;



/*
# expected structure of params
[
    (1),
    (2)
]


(1) possible values: 'stall-connection-attempt', 'p2p-group-change', 'p2p-group-participants'


(2) possible structures
when 'stall-connection-attempt':
{
    host: 'ABC0',
    paymail: 'aa@yy.zz',
    pki: '00aaff11',
    title: 'stall 01'
}

when 'p2p-group-change':
{
    formed: true,
    isGroupOwner: false,
    groupOwnerAddress: 'ip-address'
}

when 'p2p-group-participants':
[
    {
        peerName: 'abc',
        peerAddress: 'mac-address',
        status: 0
    }
]

status values:
    CONNECTED == 0
    INVITED == 1
    FAILED == 2
    AVAILABLE == 3
    UNAVAILABLE == 4



# expected structure of the response data passed in the callback
[
    true
]
 */
InformWifiDirectP2pConnectionChange.prototype.handle = function(params, callback) {

    var isSuccess = false

    var responseData = [
        null
    ];

    try {

        var runAsync = async () => {

            try {
                let messageType = params[0];
                let payload = params[1];

                if ('stall-connection-attempt' == messageType) {
                    console.log('#InformWifiDirectP2pConnectionChange; stall-connection-attempt');
                    this.context.stall.wifiDirectComms.onConnectionAttempt(
                        payload.host,
                        payload.paymail,
                        payload.pki,
                        payload.title
                    );

                } else if ('p2p-group-change' == messageType) {
                    console.log('#InformWifiDirectP2pConnectionChange; p2p-group-change');
                    this.context.stall.wifiDirectComms.onGroupChanged(
                        payload.formed,
                        payload.isGroupOwner,
                        payload.groupOwnerAddress
                    );

                } else if ('p2p-group-participants' == messageType) {
                    console.log('#InformWifiDirectP2pConnectionChange; p2p-group-participants');
                    this.context.stall.wifiDirectComms.onParticipantsChanged(
                        payload
                    );

                }

                callback(true, [true]);



            } catch (err) {
                responseData = [
                    err.stack
                ]
                console.log(err)

                callback(false, responseData)
            }
        }

        setTimeout(runAsync, 1)

    } catch (e) {
        responseData = [
            e.stack
        ];

        callback(false, responseData);
    }
};
