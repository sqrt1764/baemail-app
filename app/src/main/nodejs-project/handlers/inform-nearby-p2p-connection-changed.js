'use strict';


function InformNearbyP2pConnectionChange(context) {
    this.context = context
    this.command = "INFORM_NEARBY_P2P_CONNECTION_CHANGED"
}

module.exports = InformNearbyP2pConnectionChange;



/*
# expected structure of params
[
    (1),
    (2)
]

(1) possible values: 'new', 'lost'

(2) possible structures;
when 'new':
{
    name: 'SPAMAABBCCC00111...'
    endpointId: 'ABC0',
    isOutgoing: true,
    millisStarted: 1234567890,
    paymail: 'aa@yy.zz',
    pki: '00aaff11',
    title: 'stall 01'
}

when 'lost':
{
    endpointId: 'ABC0',
    isOutgoing: true
}


# expected structure of the response data passed in the callback
[
    true
]
 */
InformNearbyP2pConnectionChange.prototype.handle = function(params, callback) {

    var isSuccess = false

    var responseData = [
        null
    ];

    try {

        var runAsync = async () => {

            try {

                let isNew = params[0] == 'new';
                let model = params[1];


                if (isNew) {
                    //new-connection
                    if (model.isOutgoing) {
                        await this.handleConnectedAsStallClient(model);
                    } else {
                        await this.handleConnectedAsStallHost(model);
                    }
                } else {
                    //connection-lost
                    if (model.isOutgoing) {
                        await this.handleDisconnectedAsStallClient(model);
                    } else {
                        await this.handleDisconnectedAsStallHost(model);
                    }

                }

                callback(true, [true]);





            } catch (err) {

                responseData = [
                    err.stack
                ]
                console.log(err)

                callback(false, responseData)
            }

        }


        setTimeout(runAsync, 1)

    } catch (e) {
        responseData = [
            e.stack
        ];

        callback(false, responseData);
    }
};



function calculateSharedKey(bsv, privateKeyWif, publicKeyHex) {
    var privateKey = bsv.PrivateKey.fromWIF(privateKeyWif);
    var publicKey = bsv.PublicKey.fromHex(publicKeyHex);
    return publicKey.point.mul(privateKey.bn).toHex();
}


InformNearbyP2pConnectionChange.prototype.handleConnectedAsStallClient = async function(model) {
    const myConfig = this.context.stall.my.config;
    const sharedKey = calculateSharedKey(this.context.bsv, myConfig.pkiPrivateKey, model.pki);
//    const encryptionHelper = this.context.helper.getNewEncryptionHelper(sharedKey);

    const stallInterface = {
        sharedKey: sharedKey,
        host: model.endpointId,
        port: -1,
        ws: null, //missing for `nearby`
        paymail: model.paymail,
        pki: model.pki,
        stallName: model.name,
        title: model.title,
        onMessageDecrypted: (message) => {
            console.log('decrypted message:' +  message);
        },
        onConnected: () => {
            console.log('placeholder implementation of #onConnected');
        },
        onDisconnected: () => {
            console.log('placeholder implementation of #onDisconnected');
        },
        onError: (code) => {
            console.log('placeholder implementation of #onError');
        },
        disconnectNow: () => {
            try {
                console.log('...executing #disconnectNow');
                var workRequest = this.context.requestManager.forwardRequests.requests
                        .informP2pNearbyInfo.buildRequest(
                            'REQUEST_DISCONNECT',
                            { endpointId: model.endpointId }
                        );

                this.context.requestManager.forwardRequests.send(workRequest);

            } catch (e) {
                console.log(e)
            }
        },
        sendEncrypted: null //initialized later
    };



    stallInterface.sendEncrypted = (message) => {
        try {
            this.context.stall.nearbyComms.sendMessage(model.endpointId, sharedKey, message);
            //todo
            //todo
            //todo
            //let encrypted = encryptionHelper.encrypt(message);
            //ws.send(encrypted);

        } catch (e) {
            console.log('error sending an encrypted message', e);
        }
    };


    //todo must handle messages incoming from a stall
    //todo
    //todo
    //todo

    this.context.stall.peers.onStallConnectedViaNearby(stallInterface);
    stallInterface.onConnected();


}



InformNearbyP2pConnectionChange.prototype.handleConnectedAsStallHost = async function(model) {
    const myConfig = this.context.stall.my.config;
    const sharedKey = calculateSharedKey(this.context.bsv, myConfig.pkiPrivateKey, model.pki);
//    const encryptionHelper = this.context.helper.getNewEncryptionHelper(sharedKey);

    const clientInterface = {
        sharedKey: sharedKey,
        id: model.endpointId,
        pki: model.pki,
        sendEncrypted: (message) => {
            this.context.stall.nearbyComms.sendMessage(model.endpointId, sharedKey, message);
        },
        onDisconnected: null //initialized later
    };

    //todo must handle message from client
    //todo
    //todo
    //todo
//    my.handleClientMessage(
//        clientInterface,
//        decryptedMessage
//    )

    this.context.stall.my.onClientConnected(clientInterface);

}



InformNearbyP2pConnectionChange.prototype.handleDisconnectedAsStallClient = async function(model) {
    const endpointId = model.endpointId;

    let matching = Object.values(this.context.stall.peers.connected).find((item) => {
        return item.host == endpointId;
    });

    if (matching == null) {
        console.log('matching stall found; dropping #handleDisconnectedAsStallClient; endpointId:' + endpointId);
        return;
    }

    matching.onDisconnected();
}

InformNearbyP2pConnectionChange.prototype.handleDisconnectedAsStallHost = async function(model) {
    const foundClientInterface = this.context.stall.my.clients[model.endpointId];
    if (!foundClientInterface) {
        console.log(new Error('matching client-interface not found; ' + model.endpointId));
        return;
    }
    foundClientInterface.onDisconnected();

}


