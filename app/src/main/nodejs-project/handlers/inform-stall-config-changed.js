'use strict';

const WebSocket = require('ws');
const queryString = require('query-string');


function InformStallConfigChanged(context) {
    this.context = context
    this.command = "INFORM_STALL_CONFIG_CHANGED"
}

module.exports = InformStallConfigChanged;



/*

# expected structure of params
[
    {
        pkiKey: "hex",
        pkiPrivateKey: "hex",
        paymail: "",
        title: "",
        forceUsePaymailPki: false,
        customPkiWifPrivate: "hex",
        connectionType: "nearby"  //possible values: `nearby`, `wifi`
    }
]


# expected structure of the response data passed in the callback
[
    true
]



*/
InformStallConfigChanged.prototype.handle = function(params, callback) {

    var isSuccess = false

    var responseData = [
        null
    ];

    try {

        var runAsync = async () => {

            try {

                let config = params[0]

                let pkiKey = config.pkiKey
                let pkiPrivateKey = config.pkiPrivateKey
                let paymail = config.paymail
                let title = config.title
                let forceUsePaymailPki = config.forceUsePaymailPki
                let customPkiWifPrivate = config.customPkiWifPrivate

                let connectionType = config.connectionType




                this.context.stall.my.handleConfigChanged(
                    pkiPrivateKey,
                    pkiKey,
                    paymail,
                    title,
                    forceUsePaymailPki,
                    customPkiWifPrivate,
                    connectionType
                )





                callback(true, [true])





            } catch (err) {

                responseData = [
                    err.stack
                ]
                console.log(err)

                callback(false, responseData)
            }

        }


        setTimeout(runAsync, 1)

    } catch (e) {
        responseData = [
            e.stack
        ];

        callback(false, responseData);
    }
};