'use strict';




function DecryptPkiEncryptedData(context) {
    this.context = context
    this.command = 'DECRYPT_PKI_ENCRYPTED_DATA'
}

module.exports = DecryptPkiEncryptedData;





/*
#expecting params
[
    {
        seed: ['seed', 'words', ..],
        pkiPath: 'm/44'/0'/ ..'
    },
    [
        {
            txId: '1fa1..ccff',
            data: 'abc...cba'
        },
        ...
    ]
]

#response
[
    [
        {
            txId: '1fa1..ccff',
            decrypted: 'This content..been decrypted'
        },
        ...
    ]
]
*/
DecryptPkiEncryptedData.prototype.handle = function(params, callback) {

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        //verify params
        if (params.length != 2) throw new Error('invalid params');

        var secrets = params[0];
        var data = params[1];

        var paymailMnemonic = this.context.mnemonic.fromString(secrets.seed.join(' '));
        var paymailHdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(paymailMnemonic.toSeed());
        var paymailPkiHdPrivateKey = paymailHdPrivateKey.deriveChild(secrets.pkiPath);
        var paymailPkiPrivateKey = paymailPkiHdPrivateKey.privateKey;


        var cid = this.context.ECIES();
        cid.privateKey(paymailPkiPrivateKey);


        var decryptedData = [];

        for (var i = 0; i < data.length; i++) {

            var item = data[i]

            var decryptedItem = {
                txId: item.txId,
                decrypted: cid.decrypt(Buffer.from(item.data, 'hex')).toString()
            }

            decryptedData.push(decryptedItem)
        }

        responseData = [
            decryptedData
        ];
        isSuccess = true;

    } catch (e) {
         responseData = [
             e.stack
         ];
     }

     callback(isSuccess, responseData);


}
