'use strict';



function StopStallSocketServer(context) {
    this.context = context
    this.command = "STOP_STALL_SOCKET_SERVER"
}

module.exports = StopStallSocketServer;



/*

# expected structure of params
[
]


# expected structure of the response data passed in the callback
[
    true
]



*/
StopStallSocketServer.prototype.handle = function(params, callback) {

    var isSuccess = false

    var responseData = [
        null
    ];

    try {

        var runAsync = async () => {

            try {

                if ('nearby' == this.context.stall.my.config.connectionType) {
                    this.context.stall.nearbyComms.onStoppedAdvertisingStall();
                    this.context.stall.nearbyComms.ensureNearbyCommsIsTornDownAppropriately();

                } else if ('wifi' == this.context.stall.my.config.connectionType) {
                    var stopStall = this.context.stall.my.stopStall;
                    if (stopStall) {
                        stopStall(() => {
                            console.log('stopped server representing my stall (NSD)');
                        });

                    } else {
                        console.log('...a #stopStall lambda was not found');
                    }

                } else if ('wifidirect' == this.context.stall.my.config.connectionType) {
                    this.context.stall.wifiDirectComms.onStoppedAdvertisingStall();
                    this.context.stall.wifiDirectComms.ensureIsTornDownAppropriately();

                } else {
                    throw new Error('unexpected connection-type');
                }

                callback(true, [true]);


            } catch (err) {

                responseData = [
                    err.stack
                ]
                console.log(err)

                callback(false, responseData)
            }

        }


        setTimeout(runAsync, 1)

    } catch (e) {
        responseData = [
            e.stack
        ];

        callback(false, responseData);
    }
};