'use strict';




function InformAboutAvailableCoins(context) {
    this.context = context
    this.command = "INFORM_ABOUT_AVAILABLE_COINS"

    //todo add default value for `this.context.availableCoins`
}

module.exports = InformAboutAvailableCoins;


/*
!!! Spending of the funding-coins comes with obligations.
!!! The android-side needs to be informed when the node-code spends these coins, failure
!!! to do so will result in source-of-truth coin-db becoming corrupted.
!!! There is a node-to-android request prepared that makes this coin-spend-notification easy.
!!! Scroll down to view an example usage of this request (it is commented out).


#expecting params
[
    {
        activePaymail: 'aa@moneybutton.com',
        syncStatus: 'disconnected'
        fundingWalletSeed: ['seed', 'words', ..],
        fundingRootPath: 'm/44'/0'/ ..',
        coins: [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/13'}, {..}],
        upcomingChangeAddressList: [{'address': '1abcedf..', 'derivation_path':'/13'}, {'address': '1abcedf..', 'derivation_path':'/14'}, ..]
    }
]




syncStatus {
    disconnected,
    initialising,
    live,
    error
}




# response
[
    {
        informed: true
    }
]

*/
InformAboutAvailableCoins.prototype.handle = function(params, callback) {

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        var infoObject = params[0];

        //make this info available from wherever node has reference of `context`
        this.context.availableCoins = infoObject;

        console.log('InformAboutAvailableCoins syncStatus:' + infoObject.syncStatus +
            ' activePaymail:' + infoObject.activePaymail);

        // an opportunity to execute some code & use the fresh info











/*


//This is an example of how android-side should be notified when any of the funding-wallet
// coins are used in a creation of a transaction.

// ... Missing code part that creates a tx which uses infoObject.coins[0]
// & infoObject.coins[1] as inputs.

var spentCoins = [];
spentCoins.push({
    spentCoin: infoObject.coins[0],
    spendingTxId: '01..ff'
});
spentCoins.push({
    spentCoin: infoObject.coins[1],
    spendingTxId: '01..ff'
});


var callbackNotifyCoinsSpent = (status) => {
    console.log('response to -- notifyCoinsSpent... success:' + status);
};
var workRequest = this.context.requestManager.forwardRequests.requests.notifyCoinsSpent
        .buildRequest(spentCoins, callbackNotifyCoinsSpent);

this.context.requestManager.forwardRequests.send(workRequest);



*/











        //finish the response
        responseData = [
            {
                informed: true
            }
        ];
        isSuccess = true;


    } catch (e) {
//        console.log(e);
        responseData = [
            e.stack
        ];
    }

    callback(isSuccess, responseData);

};