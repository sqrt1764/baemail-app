'use strict';




function RunScript(context) {
    this.context = context
    this.command = "RUN_SCRIPT"
}

module.exports = RunScript;



/*

# expected structure of params
[
    {
        "content": ";"
    }
]


# expected structure of the response data passed in the callback

# success
[
    {
        content: "content",
        logs: "logs",
        success: true
    }
]

# error
[
    "stack-trace of exception"
]

*/
RunScript.prototype.handle = function(params, callback) {

    var responseData = [
        null
    ];

    try {

        var runAsync = async () => {
            //console.log("!!!! <<<< ????");

            let logRegistrationId = new Date().getTime() + "";
            let logsHaveBeenUnregistered = false;
            let collectedLogs = [];

            //register for logs
            this.context.repl.registerLogListener(logRegistrationId, (logEntry) => {
                if (logsHaveBeenUnregistered) {
                    console.log("a registered listener was invoked after disconnection");
                    return;
                }
                collectedLogs.push(logEntry);
            });

            let unregisterLogs = () => {
                logsHaveBeenUnregistered = true;
                this.context.repl.disconnectLogListener(logRegistrationId)
            };

            try {

                let content = params[0];

                let context = this.context;

                let holder = [];
                let errorHolder = [];


                let cb = () => {
                    var result0;
                    if (holder.length == 0) {
                        result0 = errorHolder[0].stack.toString();
                        console.log(errorHolder[0]);
                    } else {
                        result0 = holder[0];
                    }

                    //execution completed
                    unregisterLogs();

                    let isSuccess = errorHolder.length == 0;
                    responseData = [
                        {
                            logs: collectedLogs,
                            content: result0,
                            success: isSuccess
                        }
                    ];
                    callback(true, responseData);

                    if (!isSuccess) {
                        console.log(content.content);
                    }
                }

                eval(`(async () => {try {let fun0 = async () => {${content.content + '\n'}
                            };
                            var res = await fun0();
                            if (typeof(res) != 'string') {
                                res = JSON.stringify(res);
                            }
                            holder.push(res);
                        } catch (e) {
                            errorHolder.push(e);
                        }

                        cb();
                    })()
                `);


            } catch (err) {
                unregisterLogs();

                responseData = [
                    err.stack
                ];
                console.log(err);

                callback(false, responseData);
            }


//            callback(isSuccess, responseData);

        }


        setTimeout(runAsync, 1)


    } catch (e) {
        //console.log(e);
        responseData = [
            e.stack
        ];

        callback(false, responseData);
    }


};