'use strict';


function InformDiscoveredStallsChanged(context) {
    this.context = context
    this.command = "INFORM_DISCOVERED_STALLS_CHANGED"
}

module.exports = InformDiscoveredStallsChanged;



/*
# expected structure of params
[
    {
        isActive: false,
        peers: [
            {
                name: 'aaa',
                host: '10.0.2.16',
                port: 9999,
                title: 'aa',
                paymail: 'bb',
                pki: 'cc'
            }
        ]
    }
]


# expected structure of the response data passed in the callback
[
    true
]
 */
InformDiscoveredStallsChanged.prototype.handle = function(params, callback) {

    var isSuccess = false

    var responseData = [
        null
    ];

    try {

        var runAsync = async () => {

            try {

                let model = params[0];

                let isActive = model.isActive;
                let peers = model.peers;

//                console.log(`discovered stalls of peers: ${JSON.stringify(peers)}`)

                this.context.stall.discovery.onDiscoveryStateChanged(isActive, peers);


                callback(true, [true]);





            } catch (err) {

                responseData = [
                    err.stack
                ]
                console.log(err)

                callback(false, responseData)
            }

        }


        setTimeout(runAsync, 1)

    } catch (e) {
        responseData = [
            e.stack
        ];

        callback(false, responseData);
    }
};