'use strict';




function TwetchAuth(context) {
    this.context = context
    this.command = "TWETCH_AUTH"

    this.millisAllowedWithoutForceCheck = 1000 * 60 * 60 * 48; //48h
}

module.exports = TwetchAuth;



/*

# expected structure of params
[
    {
        isForced: false
    }
]


# expected structure of the response data passed in the callback
`me` will only be filled when keyAdded==TRUE
`signingAddress` will only be filled when keyAdded==FALSE
[
    {
        token: 'abc',
        keyAdded: true,
        data: {
            "me": {
                "id": "1470",
                "name": "yaanis",
                "publicKey": "03827d631df08ba2e4f177a433ac7aed8ac114bf404024f0c3d959c5483b3322a3",
                "description": "doing Android native programming",
                "dmConversationId": "3ad40c2f-7e91-4686-b754-20314eae2937",
                "followerCount": "45",
                "followingCount": "105",
                "icon": "https://cimg.twetch.com/avatars/2020/07/09/a78511334b7142810d6e3b904389b1aa402fd0ba.jpg",
                "invitesRemaining": "3",
                "myConversationId": "8f704efa-593a-41a5-a0a0-f179582e9bc8",
                "notificationsCount": "0",
                "numPosts": "645",
                "numLikes": "176",
                "numReferrals": "0",
                "paidNotificationsCount": "0",
                "unreadMessagesCount": "1"
            }
        },
        signingAddress: {
            address: '1af..ff',
            message: 'twetch-api-rocks',
            signature: 'ff..00'
        }
    }
]



*/
TwetchAuth.prototype.handle = function(params, callback) {

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {

        let isForced = params[0].isForced;


        var runAsync = async () => {
            try {

                await this.context.twetch.ensureSetupHasCompleted();

                let message = 'twetch-api-rocks';
                let preparedNotAuthorisedResponseData =  [
                    {
                        token: '',
                        keyAdded: false,
                        data: null,
                        signingAddress: {
                            address: this.context.twetch.instance.wallet.address(),
                            message: message,
                            signature: this.context.twetch.instance.wallet.sign(message)
                        }
                    }
                ];





                var token = this.context.twetch.instance.storage.getItem('tokenTwetchAuth');
                var twetchMe = null;
                var publicKeysArray = null;

                var onAuthCompleted = false;



                //check if node-project has been updated since last auth
                var lastSeenProjectVersion = this.context.twetch.instance.storage
                    .getItem('lastSeenProjectVersion');
                if (!lastSeenProjectVersion) {
                    lastSeenProjectVersion = 0;
                }
                var currentProjectVersion = this.context.nodeConfig.version;
                var hasNodeProjectBeenUpdated = currentProjectVersion > lastSeenProjectVersion;
                console.log('currentProjectVersion:' + currentProjectVersion
                    + '; lastSeenProjectVersion:' + lastSeenProjectVersion);

                if (hasNodeProjectBeenUpdated) {
                    console.log('node-project has been changed since last authentication; invalidating stored twetch-token');
                    this.context.twetch.instance.storage.removeItem('tokenTwetchAuth');
                    this.context.twetch.instance.storage.setItem('lastSeenProjectVersion', currentProjectVersion);
                    token = undefined;
                }



                if (this.context.twetch.instance.authenticated) {
                    console.log('twetch is already authenticated');
                    publicKeysArray = this.context.twetch.instance.storage
                        .getItem('publicKeysArray');

                } else if (token != undefined) {
                    console.log('twetch-token found; checking that this token has not expired; isForced:' + isForced);


                    let finalIsForced = isForced;
                    if (!finalIsForced) {
                        let millisLastCheck = this.context.twetch.instance.storage
                            .getItem('millisLastAuthCheck');

                        if (millisLastCheck == undefined) {
                            console.log('finalIsForced set to TRUE; millisLastCheck == undefined');
                            finalIsForced = true;
                        } else {
                            let millisNow = new Date().getTime();
                            let millisDelta = millisNow - millisLastCheck;

                            console.log('millis since millisLastCheck: ' + millisDelta + 'ms');

                            finalIsForced = millisDelta > this.millisAllowedWithoutForceCheck;
                        }
                    }


                    try {
                        if (finalIsForced) {
                            twetchMe = await this.doAuth();
                            //the token is valid
                            this.context.twetch.instance.authenticated = true;

                            //twetch-helper expects this be called on-auth
                            onAuthCompleted = true;

                            publicKeysArray = twetchMe.me.publicKeys.nodes;


                        } else {
                            console.log('assuming the token is valid');

                            //assuming the token is valid
                            this.context.twetch.instance.authenticated = true;

                            //twetch-helper expects this be called on-auth
                            onAuthCompleted = true;

                            publicKeysArray = this.context.twetch.instance.storage
                                .getItem('publicKeysArray');

                        }

                    } catch (e) {
                        console.log(e);
                        //token is not valid
                        token = null;
                        twetchMe = null;
                        this.context.twetch.instance.storage.removeItem('tokenTwetchAuth');
                        this.context.twetch.instance.storage.removeItem('publicKeysArray');
                        this.context.twetch.instance.storage.removeItem('millisLastAuthCheck');
                    }
                }



                if (token == null) {
                    console.log('authenticating twetch now');
                    try {
                        token = await this.context.twetch.instance.authenticate();
                        this.context.twetch.instance.recreateHttpClientWithTimeout();

                        //twetch-helper expects this be called on-auth
                        onAuthCompleted = true;

                    } catch (e) {
                        console.log('crash when attempting to twetch#authenticate; status:' + e.response.status);
                        console.log(e);
                        this.context.twetch.instance.authenticated = false;

                        // the twetch-wallet-interface-key has not been added to an account;
                        // must return info necessary for adding this paymail
                        callback(true, preparedNotAuthorisedResponseData);

                        return;
                    }

                }


                if ((isForced && twetchMe == null) || publicKeysArray == null) {
                    try {
                        twetchMe = await this.doAuth();
                        publicKeysArray = twetchMe.me.publicKeys.nodes;

                    } catch (e) {
                        console.log(e);
                    }
                }




                if (onAuthCompleted) {
                    try {
                        await this.context.twetch.onAuthenticated(publicKeysArray);

                    } catch (e) {
                        console.log(e);
                        this.context.twetch.instance.authenticated = false;

                        // the twetch-wallet-interface-key has not been added to an account;
                        // must return info necessary for adding this paymail
                        callback(true, preparedNotAuthorisedResponseData);

                        return;
                    }
                }


                //format & finish the response
                responseData = [
                    {
                        token: token,
                        keyAdded: true,
                        data: twetchMe,
                        signingAddress: {}
                    }
                ];
                isSuccess = true;


            } catch (err) {
                console.log(err);
            }


            callback(isSuccess, responseData);

        }


        setTimeout(runAsync, 1)
        //runAsync()



    } catch (e) {
        //console.log(e);
        responseData = [
            e.stack
        ];

        callback(isSuccess, responseData);
    }


};

TwetchAuth.prototype.doAuth = async function() {
    let millisStart = new Date().getTime();

    let response = await this.context.twetch.instance.query(`
        query {
            me {
                id
                name
                publicKey
                description
                dmConversationId
                followerCount
                followingCount
                icon
                iconMediaId
                invitesRemaining
                isDarkMode
                myConversationId
                notificationsCount
                numPosts
                numLikes
                numReferrals
                paidNotificationsCount
                unreadMessagesCount
                publicKeys: publicKeysByUserId {
                    nodes {
                        id
                        walletType
                        signingAddress
                        identityPublicKey
                        encryptedMnemonic
                    }
                }
            }
        }

    `);

    let millisPostAuth = new Date().getTime();
    console.log("time spent awaiting twetch-auth-request:" + (millisPostAuth - millisStart) + "ms");


    //must persist the publicKeysArray
    this.context.twetch.instance.storage
        .setItem('publicKeysArray', response.me.publicKeys.nodes);

    this.context.twetch.instance.storage.setItem('millisLastAuthCheck', new Date().getTime());

    return response;
}