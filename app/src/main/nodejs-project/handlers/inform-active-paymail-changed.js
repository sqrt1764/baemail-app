'use strict';




function InformActivePaymailChanged(context) {
    this.context = context;
    this.command = "INFORM_ACTIVE_PAYMAIL_CHANGED";

    this.lastInformedPaymail = null;
}

module.exports = InformActivePaymailChanged;


/*

# expected param structure
[
    "21589@moneybutton.com",
    [
        "aaa", "bbb", "ccc", ..
    ],
    "m/44'/0'/0'/0/0/0"
]



# expected structure of the response data passed in the callback
[
    {
        "informed": true
    }
]

*/
InformActivePaymailChanged.prototype.handle = function(params, callback) {

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        var paymail = params[0];
        var seed = params[1];
        var paymailPkiPath = params[2];
        console.log('InformActivePaymailChanged paymail:' + paymail + ' paymailPkiPath:' + paymailPkiPath);

        if (this.lastInformedPaymail == paymail) {
            console.log('InformActivePaymailChanged is returning; this paymail was already set as the active one');
            callback(true, [{informed: true}]);
            return;
        }

        // does something need cleaning up?
        // are there paymail-specific long-running processes or objects that need some work done?



        var runAsync = async () => { //todo does this need to be async? why not fire & forget?
            try {

                await this.context.twetch.setup(
                    paymail,
                    seed,
                    paymailPkiPath
                );


                //todo anything else?
                //todo
                //todo




                //finish the response
                responseData = [
                    {
                        informed: true
                    }
                ];
                isSuccess = true;

            } catch (err) {
//                console.log(new Error());
                console.log(err);
            }


            callback(isSuccess, responseData);

        }


        setTimeout(runAsync, 1)
        //runAsync()



    } catch (e) {
//        console.log(e);
        responseData = [
            e.stack
        ];
        callback(isSuccess, responseData);
    }

};