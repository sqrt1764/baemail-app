





function RequestManager(forwardRequestTypeKey, backwardRequestTypeKey, forwardRequestMap,
        backwardRequestHandlers, sendMessage) {
    this.forwardRequestTypeKey = forwardRequestTypeKey;
    this.backwardRequestTypeKey = backwardRequestTypeKey;
    this.forwardRequestMap = forwardRequestMap;
    this.backwardRequestHandlers = backwardRequestHandlers;

    this.sendMessage = sendMessage;

    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////
    ////////////////////////////

    this.forwardRequests = {
        uniqueId: 0, //keeps the last-used token value; this is used to gen new, unique ones
        map: {}, //keeps track of all running android-work-requests, mapped by request-token
        requests: forwardRequestMap //keeps all supported/implemented android-work-requests
    };

    /**
    * Get the next token valid for use with android-work-requests.
    */
    this.forwardRequests.getNextId = () => {
        var id = this.forwardRequests.uniqueId;
        this.forwardRequests.uniqueId++;
        return id;
    };


    /**
    * Update global state so this work-request-response can be handled correctly.
    * Send the request to android-code.
    */
    this.forwardRequests.send = (request) => {

        try {
            //crash on an unsupported situation
            var existingWork = this.forwardRequests.map[request.token];
            if (existingWork != undefined) throw Error('This work-request is already active.');

            //remember it, it will be required when work-result comes back
            var token = this.forwardRequests.getNextId();
            request.token = token;
            this.forwardRequests.map[token] = request;

            //send the request
            this.sendMessage(JSON.stringify({
               message_type: this.forwardRequestTypeKey,
               command: request.command,
               token: request.token,
               params: request.params
            }));

            //console.log('AndroidWork requested; token: ' + request.token);


        } catch(error) {
            console.log(error, request, 'Failed to request android-work.');
        }

    };




}

module.exports = RequestManager;



////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////




function HandlerCallback(typeValue, request, sendResponse) {
    this.request = request;
    this.sendResponse = sendResponse;
    this.typeValue = typeValue;
};

HandlerCallback.prototype.processingCallback = function(isSuccess, data) {
    var responseStatus = 'fail';
    if (isSuccess) {
        responseStatus = 'success';
    }

    var response = {
        message_type: this.typeValue,
        token: this.request.token,
        content: {
            status: responseStatus,
            data: data
        }
    };

    this.sendResponse(JSON.stringify(response));
};




////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////



RequestManager.prototype.onReceivedMessage = function(message) {
        if (message === '') {
            console.log('onMessage: `' + message + '`; ignoring !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            return;
        }
        var parsedMessage = JSON.parse(message);

        var messageType = parsedMessage.message_type;
        if (messageType == this.backwardRequestTypeKey) {
            this.handleBackwardRequest(parsedMessage);

        } else if (messageType == this.forwardRequestTypeKey) {
            this.handleForwardRequestResponse(parsedMessage);

        } else {
            console.log('socket message is missing message_type');
            return;
        }
};




RequestManager.prototype.respondWithHandlerNotFound = function(request, sendResponse) {
    var response = {
         message_type: this.backwardRequestTypeKey,
         token: request.token,
         content: {
             status: 'fail',
             data: ['HANDLER_NOT_FOUND']
         }
     };

    sendResponse(JSON.stringify(response));
};



RequestManager.prototype.handleBackwardRequest = function(parsedMessage) {
    console.log('......RequestManager handling incoming message; requestCommand:' + parsedMessage.command + ' type:' + this.backwardRequestTypeKey);

    //find the appropriate handler
    var matchingHandler = this.backwardRequestHandlers.find(handler => {
        return handler.command == parsedMessage.command;
    })

    //handle the request
    if (matchingHandler == null) {
        this.respondWithHandlerNotFound(parsedMessage, this.sendMessage/*.bind(commsHelper)*/);

    } else {

        var handlerCallback = new HandlerCallback(
            this.backwardRequestTypeKey,
            parsedMessage,
            this.sendMessage/*.bind(commsHelper)*/
        );

        matchingHandler.handle(
            parsedMessage.params,
            handlerCallback.processingCallback.bind(handlerCallback)
        );

    }
};





RequestManager.prototype.handleForwardRequestResponse = function(parsedMessage) {
    try {

        //find the matching callback to execute

        var token = parsedMessage.token;
        var content = parsedMessage.content;

        var resultStatus = content.status;
        var resultDataArray = content.data;

        //cleanup
        var matchingFinishedAndroidWork = this.forwardRequests.map[token];
        delete this.forwardRequests.map[token];

        //find the android-work-request matching the returned work result
        var matchingRequest = Object.values(this.forwardRequests.requests).find((item) => {
            return item.command == matchingFinishedAndroidWork.command
        });

        //execute on-response
        matchingRequest.onResponse(resultStatus, resultDataArray, matchingFinishedAndroidWork)

        //console.log('android-work-response successful; callback executed; token ' + token);

    } catch(error) {
        console.log(error, parsedMessage, 'error when handling android-work-response');
    }


};

