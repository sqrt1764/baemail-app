package app.bitcoin.baemail.core.domain.usecases

/**
 * This implementation makes sure that there are no leading zeros in the indexes of the derivation
 * path segments.
 * Input: m/2/345/0'/023/045'/0
 * Output: m/2/345/0'/23/45'/0
 *
 */
class NormalizePkiPathUseCaseImpl : NormalizePkiPathUseCase {
    override fun invoke(path: String): String {
        val parts = path.split("/")
        val final = StringBuilder("m/")

        for (i in 1 until parts.size) {
            val p = parts[i]
            if (i + 1 == parts.size && p.isEmpty()) {
                continue
            }


            if (p.endsWith('\'')) {
                final.append(p.substring(0, p.length - 1).toInt())
                final.append('\'')
            } else {
                final.append(p.toInt())
            }

            if (i + 1 != parts.size) {
                final.append("/")
            }
        }

        return final.toString()
    }

}

interface NormalizePkiPathUseCase {
    operator fun invoke(path: String): String
}