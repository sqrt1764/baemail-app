package app.bitcoin.baemail.core.data.util

import android.os.Looper
import androidx.annotation.Keep
import androidx.lifecycle.*

@Keep
class AppForegroundLiveData : MutableLiveData<Boolean> {

    private constructor() : super() {
        register()

        val isStarted = ProcessLifecycleOwner.get().lifecycle.currentState
            .isAtLeast(Lifecycle.State.STARTED)

        if (Looper.myLooper() == Looper.getMainLooper()) {
            value = isStarted
        } else {
            postValue(isStarted)
        }
    }

    companion object {

        private var liveData: AppForegroundLiveData? = null

        fun get(): AppForegroundLiveData {
            liveData?.let {
                return it
            }

            AppForegroundLiveData().let {
                liveData = it
                return it
            }
        }
    }

    private fun register() {
        ProcessLifecycleOwner.get().lifecycle.addObserver(ForegroundObserver())
    }

    inner class ForegroundObserver : DefaultLifecycleObserver {
        override fun onStart(owner: LifecycleOwner) {
            value = true
        }

        override fun onStop(owner: LifecycleOwner) {
            value = false
        }
    }
}