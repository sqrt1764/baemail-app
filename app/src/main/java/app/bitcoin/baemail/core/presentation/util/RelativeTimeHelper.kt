package app.bitcoin.baemail.core.presentation.util

import android.content.Context
import android.text.format.DateFormat
import app.bitcoin.baemail.R
import org.threeten.bp.ZonedDateTime
import java.util.*

class RelativeTimeHelper(
    val context: Context,
    val now: Long
) {

    companion object {
        private const val millisInMinute = 1000 * 60L
        private const val millisInHour = millisInMinute * 60L
        private const val millisInDay = millisInHour * 24L
    }

    private val dateFormat = DateFormat.getDateFormat(context)

    private val millisYearAgo = ZonedDateTime.now().minusYears(1).toInstant().toEpochMilli()
    private val millisDayAgo = ZonedDateTime.now().minusDays(1).toInstant().toEpochMilli()
    private val millisHourAgo = ZonedDateTime.now().minusHours(1).toInstant().toEpochMilli()
    private val millisMinuteAgo = ZonedDateTime.now().minusMinutes(1).toInstant().toEpochMilli()

    fun getLabel(millis: Long): String {
        when {
            millis < millisYearAgo -> {
                return dateFormat.format(Date(millis))
            }
            millis < millisDayAgo -> {
                val delta = now - millis
                val days = (delta / millisInDay).toInt()
                return context.resources.getString(R.string.day_label, days)
            }
            millis < millisHourAgo -> {
                val delta = now - millis
                val hours = (delta / millisInHour).toInt()
                return context.resources.getString(R.string.hour_label, hours)
            }
            millis < millisMinuteAgo -> {
                val delta = now - millis
                val minutes = (delta / millisInMinute).toInt()
                return context.resources.getString(R.string.minute_label, minutes)
            }
            else -> {
                return context.resources.getString(R.string.now)
            }
        }
    }
}