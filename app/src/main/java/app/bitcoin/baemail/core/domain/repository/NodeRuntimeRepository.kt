package app.bitcoin.baemail.core.domain.repository

import android.net.wifi.p2p.WifiP2pInfo
import app.bitcoin.baemail.core.data.nodejs.request.CreateBaemailMessageTx
import app.bitcoin.baemail.core.data.nodejs.request.CreateSimpleOutputScriptTx
import app.bitcoin.baemail.core.data.nodejs.request.DerivedAddress
import app.bitcoin.baemail.core.data.nodejs.request.GetPrivateKeyForPathFromSeed
import app.bitcoin.baemail.core.data.nodejs.request.InformNearbyP2pConnectionChanged
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.core.data.nodejs.request.TxInfoEqualSplit
import app.bitcoin.baemail.core.data.nodejs.request.TxInfoSendToAddress
import app.bitcoin.baemail.core.domain.entities.FundingCoinInfo
import app.bitcoin.baemail.core.domain.entities.ReceivedCoin
import app.bitcoin.baemail.p2p.P2pStallRepository
import app.bitcoin.baemail.twetch.ui.TwetchAuthStore
import app.local.p2p.serviceDiscovery.DiscoveryModel
import app.local.p2p.serviceDiscovery.FoundStall
import app.local.p2p.serviceDiscovery.P2pStallRegistryHelper1

interface NodeRuntimeRepository {

    fun startForegroundFromNode()

    fun stopForegroundFromNode()

    fun startForeground()

    fun stopForeground()

    suspend fun informActivePaymailChanged(
        paymail: String,
        seed: List<String>,
        pkiPath: String
    )

    suspend fun informCoinAvailabilityChanged(
        info: FundingCoinInfo
    )

    suspend fun getMoneyButtonPublicKey(seed: List<String>): String?

    suspend fun getPrivateKeyForPathFromSeed(
        path: String,
        seed: List<String>
    ): GetPrivateKeyForPathFromSeed.PrivateKeyHolder?

    suspend fun signMessageWithXpriv(
        xpriv: String,
        path: String,
        message: String
    ): String?

    suspend fun signMessage(
        message: String,
        wifPrivateKey: String
    ): String?

    suspend fun checkMnemonic(seed: List<String>): Boolean

    suspend fun deriveNextAddressSet(seed: List<String>, path: String, setIndex: Int): List<DerivedAddress>

    suspend fun createSplitCoinTx(
        seed: List<String>,
        spendingCoins: Map<String, ReceivedCoin>,
        spendingCoinRootPath: String,
        destinationAddressInfo: List<Pair<String, Long>>
    ): TxInfoEqualSplit?

    suspend fun createSimpleTx(
        seed: List<String>,
        spendingCoins: Map<String, ReceivedCoin>,
        destinationAddress: String,
        sats: Long
    ): TxInfoSendToAddress?

    suspend fun createSimpleOutputScriptTx(
        seed: List<String>,
        spendingCoins: Map<String, ReceivedCoin>,
        destinationList: List<CreateSimpleOutputScriptTx.Destination>
    ): CreateSimpleOutputScriptTx.TxInfo?

    suspend fun createBaemailMessageTx(
        sendingPaymail: CreateBaemailMessageTx.SendingPaymail,
        fundingWallet: CreateBaemailMessageTx.FundingWallet,
        message: CreateBaemailMessageTx.Message
    ): CreateBaemailMessageTx.Tx?

    suspend fun decryptPkiEncryptedData(
        seed: List<String>,
        pkiPath: String,
        mappedCypherText: Map<String, String>
    ): Map<String, String>?

    suspend fun twetchAuth(
        paymail: String,
        isForced: Boolean
    ): TwetchAuthStore.AuthInfo?

    suspend fun runAsyncScript(content: String): RunScript.Response

    suspend fun startStallSocketServer(
        port: Int,
        config: P2pStallRepository.PeerServiceConfig
    ): Boolean

    suspend fun stopStallSocketServer(): Boolean

    suspend fun informStallConfigChanged(
        config: P2pStallRepository.PeerServiceConfig
    ): Boolean

    suspend fun informDiscoveredStallsChanged(
        model: DiscoveryModel
    ): Boolean

    suspend fun informNearbyP2pConnectionChange(
        isNew: Boolean,
        isOutgoing: Boolean,
        endpointId: String,
        meta: InformNearbyP2pConnectionChanged.EndpointMeta?
    ): Boolean

    suspend fun informWifiDirectP2pConnectionChange(
        connectionAttempt: FoundStall?,
        groupInfo: WifiP2pInfo?,
        participants: Collection<P2pStallRegistryHelper1.DeviceStatus>?,
    ): Boolean



}