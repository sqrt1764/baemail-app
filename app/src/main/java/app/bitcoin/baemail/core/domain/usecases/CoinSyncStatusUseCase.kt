package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.domain.repository.CoinRepository
import kotlinx.coroutines.flow.Flow

class CoinSynStatusUseCaseImpl(
    private val coinRepository: CoinRepository
) : CoinSyncStatusUseCase {
    override fun invoke(): Flow<ChainSyncStatus> =
        coinRepository.syncStatus
}


interface CoinSyncStatusUseCase {
    operator fun invoke(): Flow<ChainSyncStatus>
}