package app.bitcoin.baemail.core.domain.usecases


class CalculateBasicTxSizeUseCaseImpl : CalculateBasicTxSizeUseCase {
    //todo current hardcoded byte-size values have been pulled out of thin air; do some research
    override suspend fun invoke(inputCount: Int, outputCount: Int): Int {
        return 190 + ((inputCount - 1) * 80) + ((outputCount - 1) * 50)
    }
}

interface CalculateBasicTxSizeUseCase {
    suspend operator fun invoke(inputCount: Int, outputCount: Int): Int
}