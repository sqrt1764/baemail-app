package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATSectionTitle
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder

class ATSectionTitleViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    var model: ATSectionTitle? = null

    val title: TextView = itemView.findViewById(R.id.title)

    init {
    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        model as ATSectionTitle

        if (this.model != model) {
            val titleLabel = itemView.resources.getString(model.id)
            title.text = titleLabel
        }

        this.model = model
    }


    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATSectionTitleViewHolder {
            return ATSectionTitleViewHolder(
                inflater.inflate(
                    R.layout.li_section_title,
                    parent,
                    false
                )
            )
        }
    }
}