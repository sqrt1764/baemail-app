package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.logs.domain.LogsRepository

class DeleteAllLogsUseCaseImpl(
    private val logsRepository: LogsRepository
) : DeleteAllLogsUseCase {
    override suspend fun invoke() = logsRepository.deleteAllLogs()

}

interface DeleteAllLogsUseCase {
    suspend operator fun invoke()
}