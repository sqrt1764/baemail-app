package app.bitcoin.baemail.core.domain.repository

import app.bitcoin.baemail.core.data.wallet.sending.PaymailExistsHelper
import app.bitcoin.baemail.fundingWallet.core.domain.entity.P2pDestinationInfo
import app.bitcoin.baemail.fundingWallet.core.domain.entity.P2pTxMetadata
import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import kotlinx.coroutines.flow.Flow

interface PaymailInfoRepository {
    val paymailExistsHelper: PaymailExistsHelper
    fun checkExistence(paymail: String): Flow<PaymailExistence>

    suspend fun getPaymailDestinationOutput(
        sendingPaymail: String,
        receivingPaymail: String,
        signMessageWithSendingPaymailPki: suspend (sendingPaymail: String, message: String)->String? //todo wut?
    ): String?

    suspend fun getP2pPaymentDestinationInfo(
        receivingPaymail: String,
        satsToSend: Long
    ): P2pDestinationInfo?

    suspend fun sendP2pPayment(
        receivingPaymail: String,
        hexTx: String,
        metadata: P2pTxMetadata,
        reference: String
    )

}