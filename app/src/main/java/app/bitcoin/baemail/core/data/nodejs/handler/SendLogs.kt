package app.bitcoin.baemail.core.data.nodejs.handler

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.core.data.util.FileLoggingTree
import com.google.gson.JsonArray
import timber.log.Timber

class SendLogs() : NodeRuntimeInterface.WorkHandler("SEND_LOGS") {


    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<NodeRuntimeInterface.Result, JsonArray> {

        try {
            val fileLoggerTree = Timber.forest().find {
                it is FileLoggingTree
            } ?: throw Exception()

            params.map { it.asString }.forEach {
                fileLoggerTree.d("NODE>> $it")
            }


            val jsonArray = JsonArray()

            return Pair(NodeRuntimeInterface.Result.SUCCESS, jsonArray)

        } catch (e: Exception) {
            Timber.e(e)
            return Pair(NodeRuntimeInterface.Result.FAIL, JsonArray())

        }

    }


}