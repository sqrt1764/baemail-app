package app.bitcoin.baemail.core.data.util

object Tr {

    const val METHOD_SEND_BAEMAIL_MESSAGE = "send-baemail-message"
    const val METHOD_ASSEMBLE_BAEMAIL_MESSAGE_TX = "assemble-baemail-message-tx"
    const val METHOD_SEND_TX_TO_MINER = "send-tx-to-miner"
    const val METHOD_SEND_TX_TO_BAEMAIL = "send-tx-to-baemail"
    const val METHOD_GET_PAYMAIL_DESTINATION_OUTPUT = "get-paymail-destination-output"
    const val METHOD_SIGN_MESSAGE_WITH_PRIVATE_KEY = "sign-message-with-private-key"
    const val METHOD_REQUEST_PAYMENT_DESTINATION_OUTPUT = "request-payment-destination-output"
    const val METHOD_DECRYPT_PKI_ENCRYPTED_DATA = "decrypt-pki-encrypted-data"
    const val METHOD_TWETCH_AUTH = "twetch-auth"
    const val METHOD_INFORM_ACTIVE_PAYMAIL_CHANGED = "inform-active-paymail-changed"
    const val METHOD_INFORM_COIN_AVAILABILITY_CHANGED = "inform-coin-availability-changed"
    const val METHOD_GET_MONEY_BUTTON_PUBLIC_KEY = "get-money-button-public-key"
    const val METHOD_GET_PRIVATE_KEY_FOR_PATH_FROM_SEED = "get-private-key-for-path-from-seed"
    const val METHOD_SIGN_MESSAGE_WITH_XPRIV = "sign-message-with-xpriv"
    const val METHOD_CHECK_MNEMONIC = "check-mnemonic"
    const val METHOD_DERIVE_NEXT_ADDRESS_SET = "derive-next-address-set"
    const val METHOD_CREATE_SPLIT_COIN_TX = "create-split-coin-tx"
    const val METHOD_CREATE_SIMPLE_TX = "create-simple-tx"
    const val METHOD_CREATE_SIMPLE_OUTPUT_SCRIPT_TX = "create-simple-output-script-tx"
    const val METHOD_RUN_ASYNC_SCRIPT = "run-async-script"
    const val METHOD_START_STALL_SOCKET_SERVER = "start-stall-socket-server"
    const val METHOD_STOP_STALL_SOCKET_SERVER = "stop-stall-socket-server"
    const val METHOD_INFORM_STALL_CONFIG_CHANGED = "inform-stall-config-changed"
    const val METHOD_INFORM_DISCOVERED_STALLS_CHANGED = "inform-discovered-stalls-changed"
    const val METHOD_INFORM_NEARBY_P2P_CONNECTION_CHANGE = "inform-nearby-p2p-connection-change"
    const val METHOD_INFORM_WIFI_DIRECT_P2P_CONNECTION_CHANGE = "inform-wifi-direct-p2p-connection-change"

}