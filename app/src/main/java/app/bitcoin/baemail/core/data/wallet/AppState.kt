package app.bitcoin.baemail.core.data.wallet

data class AppState(
    val activePaymail: String? = null
)