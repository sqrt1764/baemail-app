package app.bitcoin.baemail.core.presentation.util

data class AppliedInsets(
    var left: Int = 0,
    var top: Int = 0,
    var right: Int = 0,
    var bottom: Int = 0,
    var ime: Int = 0,
)