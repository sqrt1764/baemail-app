package app.bitcoin.baemail.core.presentation.drawable

import android.graphics.*
import android.graphics.drawable.Drawable
import android.text.TextPaint
import timber.log.Timber

class DrawableLineNumbers(
        private val dp: Float,
        private var offsetX: Int = (dp * 4).toInt(),
        private var align: Paint.Align = Paint.Align.LEFT,
        private val textSize: Float = 50f,
        private var labelColor: Int = Color.RED
) : Drawable() {


    private val tp = TextPaint(Paint.ANTI_ALIAS_FLAG)

    var getBaseline: ()->Int = {
        0
    }

    var getScrollY: ()->Int = {
        0
    }

    var getLineHeight: ()->Int = {
        50
    }

    var getLineCount: ()->Int = {
        1
    }

    init {
        tp.color = labelColor
        tp.textSize = textSize
        tp.textAlign = align
    }


    override fun onBoundsChange(bounds: Rect) {
        bounds ?: return

        Timber.d("...new bounds: $bounds")

    }

    override fun draw(canvas: Canvas) {

        val token = canvas.save()

        canvas.clipRect(
                bounds.left,
                bounds.top + getBaseline() - getLineHeight(),
                bounds.left + bounds.width(),
                bounds.bottom
        )

        val xOffset = 1f * bounds.left + offsetX

        val linesSkipped = getScrollY() / getLineHeight()
        val lineOffset = (-1f * (getScrollY() % getLineHeight()))


        var baseline = lineOffset + getBaseline()


        for (i in 0 until 100) {

            canvas.drawText("" + (i + 1 + linesSkipped), xOffset, baseline, tp)
            baseline += getLineHeight()


            if (baseline - getLineHeight() > bounds.height()) {
                break
            }
            if (i + 1 >= getLineCount()) break
        }


        canvas.restoreToCount(token)

    }

    override fun setAlpha(alpha: Int) {
        //not supported
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        //not supported
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

}