package app.bitcoin.baemail.core.domain.entities

data class ActivePaymail(
    val paymail: String?
)