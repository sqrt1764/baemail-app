package app.bitcoin.baemail.core.domain.entities

data class CoinRef(
    val address: String,
    val txId: String,
    val outputIndex: Int
)
