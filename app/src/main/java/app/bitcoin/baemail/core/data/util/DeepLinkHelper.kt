package app.bitcoin.baemail.core.data.util

import app.bitcoin.baemail.intercept.AppDeepLink
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber

class DeepLinkHelper {

    private val _unsatisfiedLink = MutableStateFlow<AppDeepLink?>(null)
    val unsatisfiedLink: StateFlow<AppDeepLink?>
        get() = _unsatisfiedLink


    fun requestNavigateToLink(link: AppDeepLink) {
        Timber.d("navigating: $link")
        _unsatisfiedLink.value = link
    }

    fun informLinkNavigationSatisfied() {
        _unsatisfiedLink.value = null
    }

}