package app.bitcoin.baemail.core.data.room

const val APP_DB_NAME = "app_database"
const val TABLE_PAYMAILS = "paymails"
const val TABLE_COINS = "coins"
const val TABLE_TX_CONFIRMATION = "tx_confirmation"
const val TABLE_BLOCK_SYNC_INFO = "block_sync_info"
const val TABLE_DECRYPTED_MESSAGE = "decrypted_message"
const val TABLE_CACHED_DERIVED_ADDRESS = "cached_derived_address"


const val TWETCH_DB_NAME = "twetch_database"
const val TABLE_TWETCH_CHAT_MESSAGES = "twetch_chat_messages"
const val TABLE_TWETCH_USER_PROFILE_BASE = "twetch_user_profile_base"