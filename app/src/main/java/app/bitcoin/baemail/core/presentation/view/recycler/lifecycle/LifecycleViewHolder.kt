package app.bitcoin.baemail.core.presentation.view.recycler.lifecycle

import android.view.View
import androidx.annotation.CallSuper
import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber

@Suppress("ConstantConditionIf")
abstract class LifecycleViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView), LifecycleOwner {

    companion object {
        const val isDebugging = false
    }

    private var registry: LifecycleRegistry? = null

    private val lifecycleRegistry: LifecycleRegistry
        get() {
            if (registry == null) {
                registry = LifecycleRegistry(this).also {
                    it.currentState = Lifecycle.State.CREATED
                }
            }
            return registry!!
        }

    private var parentLifecycle: Lifecycle? = null
    private var parentLifecycleObserver = ParentLifecycleObserver()

    private var maxLifecycleOfHolder: Lifecycle.State = Lifecycle.State.CREATED

    init {
        if (isDebugging) Timber.i("${Integer.toHexString(System.identityHashCode(this))} new instance")
    }

    fun onBind(parentLifecycle: Lifecycle) {

        if (!maxLifecycleOfHolder.isAtLeast(Lifecycle.State.RESUMED)) {
            if (isDebugging) Timber.i("${Integer.toHexString(System.identityHashCode(this))} onBind >> maxLifecycleOfHolder = STARTED")
            maxLifecycleOfHolder = Lifecycle.State.STARTED
        } else {
            if (isDebugging) Timber.i("${Integer.toHexString(System.identityHashCode(this))} onBind >> current maxLifecycleOfHolder = ${maxLifecycleOfHolder.name}")
        }


        if (this.parentLifecycle != parentLifecycle) {
            if (isDebugging) Timber.i("${Integer.toHexString(System.identityHashCode(this))} parentLifecycle changed; adding observer")

            if (this.parentLifecycle != null) {
                resetLifecycleRegistry()
                resetParentLifecycleObserver()
            }
            this.parentLifecycle = parentLifecycle
            parentLifecycleObserver.attachedTo = parentLifecycle

            parentLifecycle.addObserver(parentLifecycleObserver)
        } else {
            if (isDebugging) Timber.i("${Integer.toHexString(System.identityHashCode(this))} parentLifecycle unchanged; refreshing lifecycle")
        }

        bringHolderLifecycleToMaxState()
    }

    @CallSuper
    open fun onAppear() {
        maxLifecycleOfHolder = Lifecycle.State.RESUMED
        bringHolderLifecycleToMaxState()
    }

    @CallSuper
    open fun onDisappear() {
        maxLifecycleOfHolder = Lifecycle.State.STARTED
        bringHolderLifecycleToMaxState()
    }

    @CallSuper
    open fun onRecycled() {
        if (isDebugging) Timber.i("${Integer.toHexString(System.identityHashCode(this))} onRecycled")
        maxLifecycleOfHolder = Lifecycle.State.CREATED
        bringHolderLifecycleToMaxState()
    }

    override val lifecycle: Lifecycle
        get() = lifecycleRegistry

    private fun bringHolderLifecycleToMaxState() {
        val finalState = if (maxLifecycleOfHolder.isAtLeast(parentLifecycleObserver.maxLifecycle)) {
            parentLifecycleObserver.maxLifecycle
        } else {
            maxLifecycleOfHolder
        }

        if (Lifecycle.State.DESTROYED == finalState) {
            throw RuntimeException()
        }

        lifecycleRegistry.currentState = finalState
        if (isDebugging) Timber.i("${Integer.toHexString(System.identityHashCode(this))} bringHolderLifecycleToMaxState; final:${finalState.name}; holder:${maxLifecycleOfHolder.name}; parent:${parentLifecycleObserver.maxLifecycle}")
    }

    private fun resetLifecycleRegistry() {
        if (isDebugging) Timber.i("${Integer.toHexString(System.identityHashCode(this@LifecycleViewHolder))} cleanupLifecycle")
        lifecycleRegistry.currentState = Lifecycle.State.DESTROYED

        val freshRegistry = LifecycleRegistry(this)
        freshRegistry.currentState = Lifecycle.State.CREATED
        registry = freshRegistry
    }

    private fun resetParentLifecycleObserver() {
        val lc = parentLifecycle ?: return
        parentLifecycle = null

        if (isDebugging) Timber.i("${Integer.toHexString(System.identityHashCode(this@LifecycleViewHolder))} resetParentLifecycleObserver")

        lc.removeObserver(parentLifecycleObserver)

        parentLifecycleObserver.attachedTo = null
        parentLifecycleObserver.maxLifecycle = Lifecycle.State.CREATED
    }

    inner class ParentLifecycleObserver : DefaultLifecycleObserver {

        var maxLifecycle = Lifecycle.State.CREATED
        var attachedTo: Lifecycle? = null

        override fun onStart(owner: LifecycleOwner) {
            maxLifecycle = Lifecycle.State.STARTED

            bringHolderLifecycleToMaxState()
        }

        override fun onResume(owner: LifecycleOwner) {
            maxLifecycle = Lifecycle.State.RESUMED

            bringHolderLifecycleToMaxState()
        }

        override fun onPause(owner: LifecycleOwner) {
            maxLifecycle = Lifecycle.State.STARTED

            bringHolderLifecycleToMaxState()
        }

        override fun onStop(owner: LifecycleOwner) {
            maxLifecycle = Lifecycle.State.CREATED

            bringHolderLifecycleToMaxState()
        }

        override fun onDestroy(owner: LifecycleOwner) {
            if (attachedTo == parentLifecycle) {
                if (isDebugging) Timber.i("${Integer.toHexString(System.identityHashCode(this@LifecycleViewHolder))} parent destroyed")

                maxLifecycleOfHolder = Lifecycle.State.CREATED
                resetParentLifecycleObserver()
                resetLifecycleRegistry()
            } else {
                throw RuntimeException()
            }
        }
    }
}