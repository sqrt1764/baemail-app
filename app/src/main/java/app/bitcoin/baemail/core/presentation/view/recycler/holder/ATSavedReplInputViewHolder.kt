package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATSavedReplInput
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder

class ATSavedReplInputViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATSavedReplInputListener

    var model: ATSavedReplInput? = null

    val content: TextView = itemView.findViewById(R.id.content)
    val copy: Button = itemView.findViewById(R.id.copy)
    val remove: Button = itemView.findViewById(R.id.remove)

    init {

        copy.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onCopy(m)
        }

        remove.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onRemove(m)
        }


    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATSavedReplInput

        this.listener = listener!! as BaseListAdapter.ATSavedReplInputListener

        content.text = model.content
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATSavedReplInputViewHolder {
            return ATSavedReplInputViewHolder(
                inflater.inflate(
                    R.layout.li_saved_repl_input,
                    parent,
                    false
                )
            )
        }
    }
}