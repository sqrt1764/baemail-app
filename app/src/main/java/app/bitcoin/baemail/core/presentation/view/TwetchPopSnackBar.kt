package app.bitcoin.baemail.core.presentation.view

import android.content.Context
import android.graphics.Outline
import android.view.*
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import app.bitcoin.baemail.R
import com.google.android.material.snackbar.BaseTransientBottomBar

class TwetchPopSnackBar : BaseTransientBottomBar<TwetchPopSnackBar> {

    companion object {

        fun make(
            context: Context,
            parent: ViewGroup,
            contentValue: String
        ): TwetchPopSnackBar {

            val content: View = LayoutInflater.from(context)
                .inflate(R.layout.view_pop_snackbar, parent, false)

            content.layoutParams = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            ).also {
                it.gravity = Gravity.BOTTOM
            }

            return TwetchPopSnackBar(context, parent, content, contentValue)
        }


    }


    private var info: TextView



    private constructor(
        context: Context,
        parent: ViewGroup,
        content: View,
        contentValue: String
    ) : super(
        context,
        parent,
        content,
        object : com.google.android.material.snackbar.ContentViewCallback {
            override fun animateContentIn(delay: Int, duration: Int) {
                content.scaleY = 0f
                ViewCompat.animate(content).scaleY(1f)
                    .setDuration(duration.toLong())
                    .setStartDelay(delay.toLong())
            }

            override fun animateContentOut(delay: Int, duration: Int) {
                content.scaleY = 1f
                ViewCompat.animate(content).scaleY(0f)
                    .setDuration(duration.toLong())
                    .setStartDelay(delay.toLong())
            }

        }
    ) {

        val dp = context.resources.displayMetrics.density


        view.setBackgroundColor(
            ContextCompat.getColor(
                view.context,
                android.R.color.transparent
            )
        )
        view.setPadding(
            (16 * dp).toInt(),
            0,
            (16 * dp).toInt(),
            (8 * dp).toInt()
        )


        info = view.findViewById(R.id.info)


        info.clipToOutline = true
        info.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                outline.setRoundRect(0, 0, view.width, view.height, 12 * dp)
            }

        }


        info.text = contentValue

    }





}