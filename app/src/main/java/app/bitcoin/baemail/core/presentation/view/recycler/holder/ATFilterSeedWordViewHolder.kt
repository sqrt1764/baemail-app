package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.drawable.LayerDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.view.recycler.ATFilterSeedWord
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr

class ATFilterSeedWordViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATFilterSeedWordListener

    var model: ATFilterSeedWord? = null

    val word: TextView = itemView.findViewById(R.id.word)

    init {

        val strokeColor = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(itemView.context, id)
        }

        val strokeWidth = R.dimen.li_filter_seed_word__stroke_width.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        val strokePaddingSides = R.dimen.li_filter_seed_word__stroke_padding_sides.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        word.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(strokeColor)
                it.setStrokeWidths(0, 0, 0, strokeWidth)
                it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))

        word.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onClick(m.value)
        }



    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATFilterSeedWord

        this.listener = listener!! as BaseListAdapter.ATFilterSeedWordListener

        word.text = model.value
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATFilterSeedWordViewHolder {
            return ATFilterSeedWordViewHolder(
                inflater.inflate(
                    R.layout.li_filter_seed_word,
                    parent,
                    false
                )
            )
        }
    }
}