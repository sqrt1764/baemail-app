package app.bitcoin.baemail.core.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import app.bitcoin.baemail.core.data.room.TABLE_COINS
import app.bitcoin.baemail.core.data.room.entity.Coin
import kotlinx.coroutines.flow.Flow

@Dao
abstract class CoinDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertCoin(coin: Coin)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertList(coins: List<Coin>)

    @Query("select * from $TABLE_COINS where address = :address and txId = :txId and outputIndex = :outputIndex")
    abstract suspend fun getCoin(address: String, txId: String, outputIndex: Int): Coin?

    @Query("select * from $TABLE_COINS where paymail = :paymail and address = :address and txId = :txId and outputIndex = :outputIndex limit 1")
    abstract fun getCoinOfPaymail(paymail: String, address: String, txId: String, outputIndex: Int): Flow<Coin?>

    @Query("select * from $TABLE_COINS where paymail = :paymail and address = :address and txId = :txId and outputIndex = :outputIndex")
    abstract suspend fun getCoinOfPaymail1(paymail: String, address: String, txId: String, outputIndex: Int): Coin?

    @Query("select * from $TABLE_COINS where paymail = :paymail")
    abstract suspend fun getFundingCoins(paymail: String): List<Coin>

    @Query("select * from $TABLE_COINS where paymail = :paymail")
    abstract fun getFundingCoinsLD(paymail: String): LiveData<List<Coin>>

    @Query("select * from $TABLE_COINS where paymail = :paymail")
    abstract fun getFundingCoinsFlow(paymail: String): Flow<List<Coin>>

    @Query("select * from $TABLE_COINS where paymail = :paymail order by pathIndex desc")
    abstract fun getUnspentFundingCoinFlow(paymail: String): Flow<List<Coin>>

    @Query("select * from $TABLE_COINS where paymail = :paymail order by pathIndex desc")
    abstract fun getUnspentFundingCoinLD(paymail: String): LiveData<List<Coin>>

    @Query("select * from $TABLE_COINS where paymail = :paymail order by pathIndex desc")
    abstract suspend fun getUnspentFundingCoins(paymail: String): List<Coin>

    @Query("select * from $TABLE_COINS where paymail = :paymail and address in (:addressList)")
    abstract suspend fun getSublist(paymail: String, addressList: List<String>): List<Coin>

    @Query("select * from $TABLE_COINS where txId in (:txIdList)")
    abstract suspend fun getCoinsFromTransactions(txIdList: List<String>): List<Coin>

    @Query("select * from $TABLE_COINS where paymail = :paymail order by pathIndex desc limit 1")
    abstract suspend fun getHighestFundingCoin(paymail: String): Coin?

    @Query("select paymail, max(pathIndex) as addressPathIndex from $TABLE_COINS group by paymail")
    abstract fun getHighestCoinAddressByPaymail(): Flow<List<PathIndexByPaymail>>

    @Query("select * from $TABLE_COINS where address = :address")
    abstract suspend fun getCoinsForAddress(address: String): List<Coin>

    @Query("select * from $TABLE_COINS where txId = :txId")
    abstract suspend fun getCoinsWithTxId(txId: String): List<Coin>

    @Query("delete from $TABLE_COINS where txId = :txId and address = :address and outputIndex = :outputIndex")
    abstract suspend fun delete(txId: String, address: String, outputIndex: Int)

    @Query("delete from $TABLE_COINS")
    abstract fun deleteAll()

    @Delete
    abstract suspend fun deleteList(coins: List<Coin>)

    @Query("delete from $TABLE_COINS where paymail = :paymail")
    abstract fun deleteAll(paymail: String)

    @Query("delete from $TABLE_COINS where paymail = :paymail and txHeight > :txHeight")
    abstract fun deleteAllNewerThanHeight(paymail: String, txHeight: Int)

    @Query("delete from $TABLE_COINS where address in (:addressList)")
    abstract fun deleteByAddress(addressList: Set<String>)

    @Query("delete from $TABLE_COINS where txId = :txId")
    abstract suspend fun deleteCoinWithId(txId: String)

    @Transaction
    open suspend fun updateUnspent(
        paymail: String,
        addressList: List<String>,
        matchingUnspentTruth: List<Coin>
    ) {
        val currentUnspent = getSublist(paymail, addressList)

        //coins currently in db that are missing in `matchingUnspentTruth`
        val missing = currentUnspent.mapNotNull { coin ->
            val matching = matchingUnspentTruth.find {
                coin.address == it.address &&
                        coin.outputIndex == it.outputIndex &&
                        coin.sats == it.sats &&
                        coin.txId == it.txId &&
                        coin.txHeight == it.txHeight
            }

            if (matching == null) coin else null
        }

        //coins from `matchingUnspentTruth` that are not currently in db
        val fresh = matchingUnspentTruth.mapNotNull { coin ->
            val matching = currentUnspent.find {
                coin.address == it.address &&
                        coin.outputIndex == it.outputIndex &&
                        coin.sats == it.sats &&
                        coin.txId == it.txId &&
                        coin.txHeight == it.txHeight
            }

            if (matching == null) coin else null
        }

        deleteList(missing)
        insertList(fresh)


        if (missing.isEmpty() && fresh.isEmpty()) return

        notifyCoinsChanged(paymail)
    }

    @Transaction
    open suspend fun markCoinsSpent(coins: List<Coin>) {
        if (coins.isEmpty()) return

        coins.forEach { spentCoin ->
            delete(spentCoin.txId, spentCoin.address, spentCoin.outputIndex)
        }

        notifyCoinsChanged(coins.first().paymail)
    }

    @Transaction
    open suspend fun saveCoins(coins: List<Coin>) {
        insertList(coins)

        notifyCoinsChanged(coins.first().paymail)
    }

    fun injectNotifyCoinsChanged(function: (String) -> Unit) {
        notifyCoinsChanged = function
    }

    private var notifyCoinsChanged: (String) -> Unit = { _ -> }
}

data class PathIndexByPaymail(
    @ColumnInfo(name = "paymail") val paymail: String,
    @ColumnInfo(name = "addressPathIndex") val addressPathIndex: Int
)
