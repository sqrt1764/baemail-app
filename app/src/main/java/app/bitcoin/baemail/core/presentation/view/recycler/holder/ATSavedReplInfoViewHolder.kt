package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATSavedReplInfo
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder

class ATSavedReplInfoViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    var model: ATSavedReplInfo? = null

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATSavedReplInfo
    }

    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATSavedReplInfoViewHolder {
            return ATSavedReplInfoViewHolder(
                inflater.inflate(
                    R.layout.li_repl_no_saved_items,
                    parent,
                    false
                )
            )
        }
    }
}