package app.bitcoin.baemail.core.data.nodejs

import android.util.Log
import android.util.LongSparseArray
import androidx.core.util.containsKey
import androidx.core.util.valueIterator
import app.bitcoin.node.NodeComms
import app.bitcoin.node.NodeConnectionHelper
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.*
import timber.log.Timber
import kotlin.RuntimeException

/*

Class that implements request-response communication with the node-js runtime.
It adds support for both types of requests - node-runtime-to-android and android-to-node-runtime.

//todo talk about node-to-android events
/todo
/todo
/todo
/todo


android-to-node events:

//example request object
{
    token: 11,
    command: 'getMoneyButtonPubKey',
    params: [1, 'abc', {a:1}]
}



//possible values for `status` of the response
success
fail


//example response object
{
    token: 11,
    content: {
        status: 'success',
        data: [2, 'cba', {b:2}]
    }
}
 */
class NodeRuntimeInterface(
    private val nodeConnection: NodeConnectionHelper,
    private val nodeConnectedLiveData: NodeConnectedLiveData
) {

    private val scope = CoroutineScope(SupervisorJob() + Dispatchers.IO)

    private val activeRequests = LongSparseArray<NodeRequest>()

    private lateinit var workHandlers: AndroidWorkHandlerProvider

    private val nodeMessageHandlingJobSet = mutableSetOf<Array<Job?>>()


    val isStarted: Boolean
        get() = nodeConnection.isStarted

    val isKilling: Boolean
        get() { return nodeConnection.isKilling.value }

    fun setup(handlerProvider: AndroidWorkHandlerProvider) {
        workHandlers = handlerProvider

        nodeConnection.setup(object : NodeComms.MessageListener {
            override fun onMessage(m: String) {
                handleMessage(m)
            }
        })

        nodeConnectedLiveData.observeForever { isConnected ->
            Timber.d("nodeConnectedLiveData#observed - $isConnected")

            if (!isConnected) {
                val activeRequestCount = cancelAllActiveRequests()
                val messageHandlingJobCount = cancelAllNodeMessageHandlingJobs()

                //todo cancel #ensureSocket fallback-kill-job
                //todo
                //todo
                //todo

                Timber.d("node-requests cancelled because of node-disconnect: $activeRequestCount; message-handling jobs cancelled: $messageHandlingJobCount")
            }

        }
    }

    val isForeground: Boolean
        get() {
            return nodeConnection.isForeground
        }

    fun startForeground() {
        nodeConnection.startForeground()
    }

    fun stopForeground() {
        nodeConnection.stopForeground()
    }

    fun request(r: NodeRequest) {
        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            Timber.e(throwable)
        }

        scope.launch(exceptionHandler) {

            //todo disallow continuing if node is in the process of being killed
            //todo
            //todo
            //todo

            if (activeRequests.containsKey(r.token))
                throw RuntimeException("unexpected request-token ${r.token}")

            activeRequests.put(r.token, r)
            Timber.d("making request to node; token:${r.token}; command:${r.command}") //todo comment out

            try {
                nodeConnection.ensureSocket().send(assembleRequestMessage(r))
            } catch (e: Exception) {
                Timber.e(e, "#ensureSocket failed; nodejs-request will not be carried out; request-data: ${assembleRequestMessage(r)}")

                //fail the request
                //
                //
                activeRequests.remove(r.token)

                val responseArray = JsonArray()
                val infoJson = JsonObject()
                infoJson.addProperty("info", "NODE_DISCONNECTED")
                responseArray.add(infoJson)

                r.setResponse(Result.FAIL, responseArray)
                r.onResponse()
            }
        }
    }

    private fun assembleRequestMessage(r: NodeRequest): String {
        val requestJson = JsonObject()

        requestJson.addProperty(KEY_MESSAGE_TYPE, VALUE_TYPE_NODE_WORK_REQUEST)
        requestJson.addProperty(KEY_TOKEN, r.token)
        requestJson.addProperty(KEY_COMMAND, r.command)
        requestJson.add(KEY_PARAMS, r.getReqParams())

        return requestJson.toString()
    }

    private fun handleMessage(m: String) {
        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            Timber.e(throwable, "RequestManager#handleMessage")
        }

        val jobHolder = arrayOfNulls<Job?>(1)
        nodeMessageHandlingJobSet.add(jobHolder)

        jobHolder[0] = scope.launch(exceptionHandler) {
            try {
                val json = Gson().fromJson(m, JsonObject::class.java)
                when (json.get(KEY_MESSAGE_TYPE).asString) {
                    VALUE_TYPE_ANDROID_WORK_REQUEST -> {
                        handleIncomingRequest(json)
                    }
                    VALUE_TYPE_NODE_WORK_REQUEST -> {
                        handleRequestResponse(json)
                    }
                    else -> throw RuntimeException()
                }
            } catch (e: Exception) {
                Timber.e(e)
            } finally {
                nodeMessageHandlingJobSet.remove(jobHolder)
            }

        }
    }

    private fun handleRequestResponse(json: JsonObject) {

        val content: JsonObject = json.getAsJsonObject(KEY_CONTENT)

        val token: Long = json.getAsJsonPrimitive(KEY_TOKEN).asLong
        val status: String = content.getAsJsonPrimitive(KEY_STATUS).asString
        val data: JsonArray = content.getAsJsonArray(KEY_DATA)

        if (Result.FAIL.const == status)
            Timber.d("handleRequestResponse; token:$token; status:$status")


        val matchingRequest = activeRequests.get(token) ?: throw RuntimeException(token.toString())
        activeRequests.remove(token)

        val responseStatus = if (Result.SUCCESS.const == status) {
            Result.SUCCESS
        } else if (Result.FAIL.const == status) {
            Result.FAIL
        } else {
            throw RuntimeException(json.toString())
        }

        matchingRequest.setResponse(responseStatus, data)
        matchingRequest.onResponse()
    }


    private suspend fun handleIncomingRequest(json: JsonObject) {

        val command = json.get(KEY_COMMAND).asString

        val token = json.get(KEY_TOKEN).asLong
        val paramsJson = json.getAsJsonArray(KEY_PARAMS)

        try {

            //find the matching handler
            val handler = workHandlers.get(command) ?: let {
                //respond with error 'handler not found'
                val param = JsonArray()
                param.add("HANDLER_NOT_FOUND")
                sendIncomingRequestResult(token, Result.FAIL, param)
                return
            }


            val result = handler.handle(token, paramsJson)
            //respond with result
            sendIncomingRequestResult(token, result.first, result.second)

        } catch (e: Exception) {
            //respond with crash-resp
            val param = JsonArray()
            param.add(Log.getStackTraceString(e))
            sendIncomingRequestResult(token, Result.FAIL, param)
        }

    }

    private fun cancelAllActiveRequests(): Int {
        val copy = activeRequests.clone()
        activeRequests.clear()

        copy.valueIterator().forEach { request ->
            val responseArray = JsonArray()
            val infoJson = JsonObject()
            infoJson.addProperty("info", "NODE_DISCONNECTED")
            responseArray.add(infoJson)

            request.setResponse(Result.FAIL, responseArray)
            request.onResponse()
        }

        return copy.size()
    }

    private fun cancelAllNodeMessageHandlingJobs(): Int {
        val set = HashSet(nodeMessageHandlingJobSet)
        set.forEach { job ->
            job[0]?.cancel()
        }
        return set.size
    }

    private suspend fun sendIncomingRequestResult(token: Long, result: Result, params: JsonArray) {

        val contentJson = JsonObject()
        contentJson.addProperty(KEY_STATUS, result.const)
        contentJson.add(KEY_DATA, params)

        val requestJson = JsonObject()
        requestJson.addProperty(KEY_MESSAGE_TYPE, VALUE_TYPE_ANDROID_WORK_REQUEST)
        requestJson.addProperty(KEY_TOKEN, token)
        requestJson.add(KEY_CONTENT, contentJson)

        try {
            nodeConnection.ensureSocket().send(requestJson.toString())
        } catch (e: Exception) {
            Timber.e(e, "dropping responding to the request from nodejs because it is dead")
        }
    }


    enum class Result(val const: String) {
        SUCCESS("success"),
        FAIL("fail")
    }

    abstract class NodeRequest(
        val command: String
    ) {
        val token = getNextId()
        val stackTrace = RuntimeException()

        var responseResult: Result? = null
        var responseData: JsonArray? = null

        abstract fun getReqParams(): JsonArray

        fun setResponse(result: Result, data: JsonArray) {
            responseResult = result
            responseData = data
        }

        abstract fun onResponse()
    }

    abstract class WorkHandler(
        val command: String
    ) {

        abstract suspend fun handle(token: Long, params: JsonArray): Pair<Result, JsonArray>

    }

    companion object {
        private const val KEY_MESSAGE_TYPE = "message_type"
        private const val KEY_TOKEN = "token"
        private const val KEY_COMMAND = "command"
        private const val KEY_PARAMS = "params"

        private const val KEY_DATA = "data"
        private const val KEY_STATUS = "status"
        private const val KEY_CONTENT = "content"

        private const val VALUE_TYPE_NODE_WORK_REQUEST = "node_work_request"
        private const val VALUE_TYPE_ANDROID_WORK_REQUEST = "android_work_request"





        private var uniqueId = 0L

        @Synchronized
        fun getNextId(): Long {
            return uniqueId++
        }
    }
}