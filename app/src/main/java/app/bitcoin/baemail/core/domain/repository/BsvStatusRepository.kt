package app.bitcoin.baemail.core.domain.repository

import app.bitcoin.baemail.core.data.wallet.BlockInfo
import app.bitcoin.baemail.core.data.wallet.BlockStatsFee
import app.bitcoin.baemail.core.data.wallet.ExchangeRateHelper
import kotlinx.coroutines.flow.Flow

interface BsvStatusRepository {
    val liveBlockInfo: Flow<BlockInfo?>
    val txFee: Flow<BlockStatsFee>
    val exchangeRate: Flow<ExchangeRateHelper.Model>
    suspend fun sendTx(hexTx: String): Boolean
    suspend fun sendTxToBaemail(hexTx: String): Boolean
}