package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.JsonArray
import timber.log.Timber
import java.lang.RuntimeException

class CheckMnemonicIsValid(
    val seed: List<String>,
    val callback: (Boolean)->Unit
) : NodeRuntimeInterface.NodeRequest("CHECK_MNEMONIC_IS_VALID") {

    override fun getReqParams(): JsonArray {
        val array = JsonArray()
        seed.forEach {
            array.add(it)
        }
        return array
    }

    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "mnemonic was not valid; token: $token")
            callback(false)
            return
        }
        callback(true)
    }

}