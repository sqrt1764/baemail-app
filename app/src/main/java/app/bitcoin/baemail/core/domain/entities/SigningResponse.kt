package app.bitcoin.baemail.core.domain.entities

data class SigningResponse(
    val signature: String,
    val pubkey: String
)
