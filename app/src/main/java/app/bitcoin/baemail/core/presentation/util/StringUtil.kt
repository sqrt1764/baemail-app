package app.bitcoin.baemail.core.presentation.util

import java.lang.StringBuilder


fun formatSatsLabel(sats: String): String {
    val satsSB = StringBuilder()
    sats.reversed().forEachIndexed { i, c ->
        if (i == 3 || i == 6 || i == 8) {
            satsSB.append('\'')
        }
        satsSB.append(c)
    }
    return satsSB.reverse().toString()
}