package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.Color
import android.graphics.drawable.LayerDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.view.recycler.ATCoin
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr

class ATP2pRequiresPermissionsViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATCoinListener

    var model: ATCoin? = null

    val containerRequiresPermissions: ConstraintLayout = itemView.findViewById(R.id.container_missing_permissions)
    val buttonRequiresPermissionsProceed: Button = itemView.findViewById(R.id.button_requires_permissions_proceed)

    lateinit var strokeDrawable: DrawableStroke

    val strokeColor = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
//            R.color.li_filter_seed_word__stroke_color.let { id ->
//        ContextCompat.getColor(itemView.context, id)
//    }

    init {
        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(itemView.context, id)
        }

        val strokeWidth = R.dimen.li_filter_seed_word__stroke_width.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        val strokePaddingSides = R.dimen.li_filter_seed_word__stroke_padding_sides.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        strokeDrawable = DrawableStroke().also {
            it.setStrokeColor(strokeColor)
            it.setStrokeWidths(0, 0, 0, strokeWidth)
            it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
        }

        itemView.background = LayerDrawable(arrayOf(
            strokeDrawable,
            DrawableState.getNew(colorSelector)
        ))

        itemView.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onClick(m.coin)
        }



    }






    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATCoin

        this.listener = listener as BaseListAdapter.ATCoinListener

//        path.text = model.coin.derivationPath
//        sats.text = model.coin.sats.toString()
//
//        if (model.coin.spendingTxId != null) {
//            isActive.visibility = View.VISIBLE
//        } else {
//            isActive.visibility = View.INVISIBLE
//        }

        if (model.isLast) {
            strokeDrawable.setStrokeColor(Color.TRANSPARENT)
        } else {
            strokeDrawable.setStrokeColor(strokeColor)
        }
    }






    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATP2pRequiresPermissionsViewHolder {
            return ATP2pRequiresPermissionsViewHolder(
                inflater.inflate(
                    R.layout.li_p2p_requires_permissions,
                    parent,
                    false
                )
            )
        }
    }
}