package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableOval
import app.bitcoin.baemail.core.presentation.view.recycler.ATPaymentItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder

class ATPaymentItemViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATPaymentCarouselListener

    var model: ATPaymentItem? = null

    val buttonManage: Button = itemView.findViewById(R.id.button0)
    val buttonInfo: Button = itemView.findViewById(R.id.button1)

//    val word: TextView = itemView.findViewById(R.id.word)

    init {

//        val strokeColor = R.color.li_filter_seed_word__stroke_color.let { id ->
//            ContextCompat.getColor(itemView.context, id)
//        }
//        val colorSelector = R.color.selector_on_light.let { id ->
//            ContextCompat.getColor(itemView.context, id)
//        }
//
//        val strokeWidth = R.dimen.li_filter_seed_word__stroke_width.let { id ->
//            itemView.resources.getDimensionPixelSize(id)
//        }
//
//        val strokePaddingSides = R.dimen.li_filter_seed_word__stroke_padding_sides.let { id ->
//            itemView.resources.getDimensionPixelSize(id)
//        }

//        word.background = LayerDrawable(arrayOf(
////            DrawableStroke().also {
////                it.setStrokeColor(strokeColor)
////                it.setStrokeWidths(0, 0, 0, strokeWidth)
////                it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
////            },
//            DrawableState.getNew(colorSelector)
//        ))

        buttonInfo.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onInfoClick(m.id)
        }

        buttonManage.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onManageClick(m.id)
        }


        val dp = itemView.resources.displayMetrics.density


        val peakSpaceWidthPx = R.dimen.payment_item__card_peak_space_width.let {
            itemView.resources.getDimensionPixelSize(it)
        }

        val cardStrokeWidth = R.dimen.payment_item__stroke_width.let { id ->
            itemView.resources.getDimension(id)
        }

        val drawableOval = DrawableOval(itemView.context).also {
            val strokeColor = R.color.yellow1.let { id ->
                ContextCompat.getColor(itemView.context, id)
            }
            val finalStrokeColor = Color.argb(
                137,
                Color.red(strokeColor),
                Color.green(strokeColor),
                Color.blue(strokeColor)
            )
//            val backgroundColor = R.color.message_0__actions_b2_color.let { id ->
//                ContextCompat.getColor(itemView.context, id)
//            }

            it.setBgColor(Color.TRANSPARENT)
            it.setStrokeColor(finalStrokeColor)
            it.setStrokeWidth(cardStrokeWidth)
            it.setCornerRadius(22f * dp)
            it.setPaddings(peakSpaceWidthPx.toFloat(), 0 * dp, peakSpaceWidthPx.toFloat(), 0 * dp)
        }

        itemView.background = drawableOval

        itemView.clipToOutline = true
        itemView.outlineProvider = drawableOval.getOutlineProvider()


        itemView.updatePadding(
            left = (peakSpaceWidthPx + cardStrokeWidth * 0.5f).toInt(),
            right = (peakSpaceWidthPx + cardStrokeWidth * 0.5f).toInt()
        )


//        itemView.elevation = R.dimen.payment_item__card_elevation.let {
//            itemView.resources.getDimension(it)
//        }


    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATPaymentItem



        this.listener = listener!! as BaseListAdapter.ATPaymentCarouselListener

//        word.text = "Payment ${model.id}"//model.value
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATPaymentItemViewHolder {
            return ATPaymentItemViewHolder(
                inflater.inflate(
                    R.layout.li_payment,
                    parent,
                    false
                )
            )
        }
    }
}