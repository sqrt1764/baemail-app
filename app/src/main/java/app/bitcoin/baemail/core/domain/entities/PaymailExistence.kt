package app.bitcoin.baemail.core.domain.entities

sealed class PaymailExistence(
    val paymail: String
) {
    class Checking(paymail: String = ""): PaymailExistence(paymail)
    class Invalid(paymail: String): PaymailExistence(paymail)
    class Valid(
        paymail: String,
        val pubKey: String,
        val supportsProfile: Boolean,
        val supportsP2p: Boolean
    ): PaymailExistence(paymail)

}