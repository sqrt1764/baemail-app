package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.data.util.SeedWordList
import kotlinx.coroutines.flow.Flow

class GetSeedWordListUseCaseImpl(
    private val seedWordList: SeedWordList
) : GetSeedWordListUseCase {
    override fun invoke(): Flow<Array<String>> {
        return seedWordList.get()
    }

}

interface GetSeedWordListUseCase {
    operator fun invoke(): Flow<Array<String>>
}