package app.bitcoin.baemail.core.data.util

import android.content.res.Resources
import app.bitcoin.baemail.R
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext

class SeedWordList(
    private val resources: Resources,
    private val dispatcher: CoroutineDispatcher
) {
    fun get(): Flow<Array<String>> = flow {
        emit(withContext(dispatcher) { resources.getStringArray(R.array.seed_word_list) })
    }
}