package app.bitcoin.baemail.core.data.nodejs.handler

import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.core.data.room.dao.CoinDao
import com.google.gson.JsonArray
import timber.log.Timber


/*
# expected structure of params array
[
    {
        spentCoin: {
            tx_id: '00..ff',
            address: '1FFF..FF',
            index: 0
        },
        spendingTxId: '01..ff'
    },
    {
        spentCoin: {
            tx_id: '00..ee',
            address: '1eee..ee',
            index: 2
        },
        spendingTxId: '01..ff'
    },
    ..
]



*/
class NotifyCoinsSpentHandler(
    val secureDataSource: SecureDataSource,
    val coinDao: CoinDao,
) : NodeRuntimeInterface.WorkHandler("NOTIFY_COINS_SPENT") {



    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<NodeRuntimeInterface.Result, JsonArray> {

        try {

            val spendingJsonArray = params[0].asJsonArray

            val spendingList = spendingJsonArray.map { spending ->
                val outputJson = spending.asJsonObject.getAsJsonObject("spentCoin")
                val spendingTxId = spending.asJsonObject.get("spendingTxId").asString
                val outputTxId = outputJson.get("tx_id").asString
                val outputAddress = outputJson.get("address").asString
                val outputIndex = outputJson.get("index").asInt

                OutputSpending(
                    outputTxId,
                    outputAddress,
                    outputIndex,
                    spendingTxId
                )
            }

            if (spendingList.isEmpty()) {
                Timber.d("received and empty coin-spending notification")
                return Pair(NodeRuntimeInterface.Result.SUCCESS, JsonArray())
            }

            val spentCoinsList = spendingList.map { spentCoin ->
                coinDao.getCoin(spentCoin.address, spentCoin.txId, spentCoin.index)!!
            }

            coinDao.markCoinsSpent(spentCoinsList)


            return Pair(NodeRuntimeInterface.Result.SUCCESS, JsonArray())

        } catch (e: Exception) {
            Timber.e(e)
            return Pair(NodeRuntimeInterface.Result.FAIL, JsonArray())

        }

    }


    data class OutputSpending(
        val txId: String,
        val address: String,
        val index: Int,
        val spendingTxId: String
    )
}