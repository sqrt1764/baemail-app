package app.bitcoin.baemail.core.data.nodejs

import app.bitcoin.baemail.core.data.wallet.sending.SignMessageHelper
import app.bitcoin.baemail.p2p.InformP2pChangeHelper
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.data.nodejs.handler.AddXprivHandler
import app.bitcoin.baemail.core.data.nodejs.handler.GetPaymailDestinationOutput
import app.bitcoin.baemail.core.data.nodejs.handler.InformP2pConnectedStallChange
import app.bitcoin.baemail.core.data.nodejs.handler.InformP2pEventHandler
import app.bitcoin.baemail.core.data.nodejs.handler.InformP2pNearbyInfo
import app.bitcoin.baemail.core.data.nodejs.handler.NotifyCoinsSpentHandler
import app.bitcoin.baemail.core.data.nodejs.handler.RequestKeepNodeAlive
import app.bitcoin.baemail.core.data.nodejs.handler.RequestRefreshOfCoinsHandler
import app.bitcoin.baemail.core.data.nodejs.handler.RequestReleaseKeepNodeAlive
import app.bitcoin.baemail.core.data.nodejs.handler.SendLogs
import app.bitcoin.baemail.core.data.nodejs.handler.SignRequestHandler
import app.bitcoin.baemail.core.data.room.AppDatabase
import app.bitcoin.baemail.core.data.wallet.AppStateLiveData
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.domain.repository.PaymailInfoRepository
import timber.log.Timber

class AndroidWorkHandlerProviderImpl(
    private val secureDataSource: SecureDataSource,
    private val informP2pChangeHelper: InformP2pChangeHelper,
    private val appDatabase: AppDatabase,
    private val coinRefreshRequestHelper: CoinRefreshRequestHelper,
    private val appStateLD: AppStateLiveData,
    private val paymailInfoRepository: PaymailInfoRepository,
    private val signMessageHelper: SignMessageHelper,
) : AndroidWorkHandlerProvider {

    private val workHandlers: Map<String, NodeRuntimeInterface.WorkHandler> = listOf(
        AddXprivHandler(
            secureDataSource
        ),
        SignRequestHandler(
            secureDataSource,
            ::sign
        ),
        NotifyCoinsSpentHandler(
            secureDataSource,
            appDatabase.coinDao()
        ),
        RequestRefreshOfCoinsHandler(
            coinRefreshRequestHelper,
            ::getActivePaymail
        ),
        SendLogs(),
        RequestKeepNodeAlive(::startForegroundFromNode),
        RequestReleaseKeepNodeAlive(::stopForegroundFromNode),
        GetPaymailDestinationOutput(::getPaymailDestinationOutput),
        InformP2pConnectedStallChange(informP2pChangeHelper),
        InformP2pEventHandler(informP2pChangeHelper),
        InformP2pNearbyInfo(informP2pChangeHelper)
    ).let { list ->
        val asPairs = list.map { it.command to it }.toTypedArray()
        mapOf(*asPairs)
    }

    private var nodeRuntimeRepository: NodeRuntimeRepository? = null

    fun inject(repository: NodeRuntimeRepository) {
        nodeRuntimeRepository = repository
    }

    //TODO this implementation is cringe ... node-project requests work, that get redirected back to it? strange
    private suspend fun sign(xpriv: String, keyPath: String, messageToSign: String): String?  {
        return nodeRuntimeRepository!!.signMessageWithXpriv(
            xpriv,
            keyPath,
            messageToSign
        )
    }

    private fun getActivePaymail(): String {
        return appStateLD.value?.activePaymail!!
    }

    private fun startForegroundFromNode() {
        nodeRuntimeRepository!!.startForegroundFromNode()
    }

    private fun stopForegroundFromNode() {
        nodeRuntimeRepository!!.stopForegroundFromNode()
    }

    private suspend fun getPaymailDestinationOutput(destinationPaymail: String): String? {
        val sendingPaymail = appStateLD.value?.activePaymail ?: let {
            Timber.e("error, `activePaymail` is NULL")
            return null
        }

        return paymailInfoRepository.getPaymailDestinationOutput(
            sendingPaymail,
            destinationPaymail,
            ::signMessageWithPaymailPki
        ) ?: let {
            Timber.e("error retrieving paymail destination output")
            return null
        }
    }

    private suspend fun signMessageWithPaymailPki(
        sendingPaymail: String,
        message: String
    ): String? {
        return signMessageHelper.signMessageWithPaymailPki(
            sendingPaymail,
            message,
            secureDataSource,
            nodeRuntimeRepository!!
        )?.signature
    }

    override fun get(command: String): NodeRuntimeInterface.WorkHandler? {
        return workHandlers[command]
    }
}

interface AndroidWorkHandlerProvider {
    fun get(command: String): NodeRuntimeInterface.WorkHandler?
}