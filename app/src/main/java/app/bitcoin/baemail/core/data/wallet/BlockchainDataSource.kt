package app.bitcoin.baemail.core.data.wallet

import android.content.Context
import androidx.annotation.Keep
import androidx.tracing.Trace
import app.bitcoin.baemail.core.data.util.Tr
import app.bitcoin.baemail.core.data.util.ThrottledExecQueue
import app.bitcoin.baemail.core.data.net.AppApiService
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import kotlin.random.Random

class BlockchainDataSource(
    val appContext: Context,
    val apiService: AppApiService,
    val execQueue: ThrottledExecQueue,
    val gson: Gson,
    val coroutineUtil: CoroutineUtil
) {


    /**
     * @return NULL on crash, non-empty list if there are any unspent coins for specified addresses
     */
    suspend fun findUnspentCoinsForAddress(list: List<String>): List<UnspentCoin>?
            = withContext(coroutineUtil.ioDispatcher) {
        execQueue.execute {
            try {

                val jsonAddressArray = JsonArray()
                list.forEach { jsonAddressArray.add(it) }

                val jsonRequestObject = JsonObject()
                jsonRequestObject.add("addresses", jsonAddressArray)


                val response = apiService.postJson(
                    "https://api.whatsonchain.com/v1/bsv/main/addresses/unspent",
                    jsonRequestObject.toString().toRequestBody()
                ).execute()

                val body = response.body()

                val gotUnspentCoins = mutableListOf<UnspentCoin>()

                if (response.isSuccessful && body != null) {
                    val jsonString = String(body.bytes())
                    val responseJsonArray = gson.fromJson(jsonString, JsonArray::class.java)

                    for (entry in responseJsonArray) {

                        val address = entry.asJsonObject.get("address").asString

                        val error = entry.asJsonObject.get("error").asString
                        if (error.isNotBlank()) throw RuntimeException(jsonString)

                        val unspentJsonArray = entry.asJsonObject.getAsJsonArray("unspent")

                        unspentJsonArray.forEach { coinJson ->
                            gotUnspentCoins.add(
                                UnspentCoin(
                                address,
                                coinJson.asJsonObject.get("height").asInt,
                                coinJson.asJsonObject.get("tx_pos").asInt,
                                coinJson.asJsonObject.get("value").asLong,
                                coinJson.asJsonObject.get("tx_hash").asString,
                            )
                            )
                        }
                    }

                } else {
                    throw ErrorStatusCodeException(response.code())

                }

                return@execute gotUnspentCoins

            } catch (e: Exception) {
                Timber.d(e)
                return@execute null
            }
        }
    }

    /**
     * @return NULL on crash, non-empty list if there is any coin-activity for the address
     */
    suspend fun findHistoryForAddress(address: String): List<AddressHistory>?
            = withContext(coroutineUtil.ioDispatcher) {
        execQueue.execute {
            try {
                val response = apiService.fetch(
                    "https://api.whatsonchain.com/v1/bsv/main/address/$address/history"
                ).execute()

                val body = response.body()

                val gotHistory = mutableListOf<AddressHistory>()

                if (response.isSuccessful && body != null) {
                    val jsonString = String(body.bytes())
                    val historyJson = gson.fromJson(jsonString, JsonArray::class.java)

                    for (entry in historyJson) {
                        gotHistory.add(
                            AddressHistory(
                                address,
                                entry.asJsonObject.get("tx_hash").asString,
                                entry.asJsonObject.get("height").asString.toInt()
                            )
                        )
                    }

                } else throw RuntimeException()

                return@execute gotHistory

            } catch (e: Exception) {
                Timber.d(e)
                return@execute null
            }
        }
    }

    suspend fun checkAddressHasBeenUsed(address: String): Boolean =
        withContext(coroutineUtil.ioDispatcher) {
            execQueue.execute {
                val response = apiService.fetch(
                    "https://api.whatsonchain.com/v1/bsv/main/address/$address/used"
                ).execute()

                val body = response.body()

                if (response.isSuccessful && body != null) {
                    val jsonString = String(body.bytes())
                    return@execute jsonString.trim() == "true"
                }

                return@execute false
            }
    }



    suspend fun fetchBlockInfoForHashFromWhatsOnChain(hash: String?): BlockInfo? = withContext(coroutineUtil.ioDispatcher) {
        hash ?: let {
            Timber.d("fetchBlockInfoForHash; cannot get block-info for null-hash")
            return@withContext null
        }

        execQueue.execute {
            try {
                val url = "https://api.whatsonchain.com/v1/bsv/main/block/$hash/header"


                val blockHeaderResponse = apiService.fetch(
                    url
                ).execute()

                val body = blockHeaderResponse.body()


                if (blockHeaderResponse.isSuccessful && body != null) {

                    val jsonString = String(body.bytes())
                    val headerJson = gson.fromJson(jsonString, JsonObject::class.java)

                    val blockHash = headerJson.get("hash").asString
                    val blockMerkleRoot = headerJson.get("merkleroot").asString
                    val blockHeight = headerJson.get("height").asInt
                    val blockTime = headerJson.get("time").asLong * 1000
                    val prevBlockHash = headerJson.get("previousblockhash").asString


                    return@execute BlockInfo(
                        blockHeight,
                        blockTime,
                        blockHash,
                        blockMerkleRoot,
                        prevBlockHash
                    )
                } else {
                    Timber.d("request failed; isSuccessful:${blockHeaderResponse.isSuccessful} non-null body:${body != null}")
                    return@execute null
                }

            } catch (e: Exception) {
                Timber.d(e)

                return@execute null
            }
        }
    }

    /**
     * @return null if not found
     */
    suspend fun fetchBlockInfoForHeight(height: Int): BlockInfo? = withContext(coroutineUtil.ioDispatcher) {
        execQueue.execute {
            try {
                val url = "https://api.whatsonchain.com/v1/bsv/main/block/height/$height"


                val blockInfoResponse = apiService.fetch(
                    url
                ).execute()

                val body = blockInfoResponse.body()


                if (blockInfoResponse.isSuccessful && body != null) {
                    val jsonString = String(body.bytes())
                    val json = gson.fromJson(jsonString, JsonObject::class.java)

                    val blockHash = json.get("hash").asString
                    val blockTime = json.get("time").asLong
                    val blockMerkleRoot = json.get("merkleroot").asString
                    val previousBlockHash = json.get("previousblockhash").asString

                    return@execute BlockInfo(
                        height,
                        blockTime,
                        blockHash,
                        blockMerkleRoot,
                        previousBlockHash
                    )
                } else {
                    return@execute null
                }

            } catch (e: Exception) {
                Timber.d(e)

                return@execute null
            }
        }

    }


    suspend fun fetchExchangeRate(): ExchangeRate? = withContext(Dispatchers.IO) {
        execQueue.execute {
            try {
                val exchangeRateResponse = apiService.fetch(
                    "https://api.whatsonchain.com/v1/bsv/main/exchangerate"
                ).execute()

                val body = exchangeRateResponse.body()
                if (exchangeRateResponse.isSuccessful && body != null) {
                    val jsonString = String(body.bytes())
                    val json = gson.fromJson(jsonString, JsonObject::class.java)

                    val currency = json.get("currency").asString
                    val rate = json.get("rate").asDouble

                    return@execute ExchangeRate(
                        currency,
                        rate,
                        System.currentTimeMillis()
                    )
                } else {
                    return@execute null
                }
            } catch (e: Exception) {
                Timber.e(e)
                return@execute null
            }
        }
    }

    suspend fun fetchBlockStatsFee(blockHeight: Int): BlockStatsFee? = withContext(coroutineUtil.ioDispatcher) {
        execQueue.execute {
            try {
                val blockInfoResponse = apiService.fetch(
                    "https://api.whatsonchain.com/v1/bsv/main/block/height/$blockHeight/stats"
                ).execute()

                val body = blockInfoResponse.body()
                if (blockInfoResponse.isSuccessful && body != null) {
                    val jsonString = String(body.bytes())
                    val json = gson.fromJson(jsonString, JsonObject::class.java)

                    val medianFeeSats = json.get("median_fee").asInt
                    val medianTxSizeBytes = json.get("median_tx_size").asInt

                    return@execute BlockStatsFee(
                        blockHeight,
                        medianFeeSats,
                        medianTxSizeBytes
                    )
                } else {
                    return@execute null
                }

            } catch (e: Exception) {
                Timber.e(e)
                return@execute null
            }
        }
    }


    suspend fun fetchCurrentStatusFromWhatsOnChain(): BlockInfo? = withContext(coroutineUtil.ioDispatcher) {
        execQueue.execute {
            try {
                val url = "https://api.whatsonchain.com/v1/bsv/main/block/headers"

                val currentHeadersResponse = apiService.fetch(
                    url
                ).execute()

                val body = currentHeadersResponse.body()


                if (currentHeadersResponse.isSuccessful && body != null) {
                    val jsonString = String(body.bytes())
                    val headerArray = gson.fromJson(jsonString, JsonArray::class.java)
                    if (headerArray.size() == 0) {
                        return@execute null
                    }
                    val latestHeaderJson = headerArray[0].asJsonObject

                    val blockHash = latestHeaderJson.get("hash").asString
                    val blockMerkleRoot = latestHeaderJson.get("merkleroot").asString
                    val blockHeight = latestHeaderJson.get("height").asInt
                    val blockTime = latestHeaderJson.get("time").asLong * 1000
                    val prevBlockHash = latestHeaderJson.get("previousblockhash").asString


                    return@execute BlockInfo(
                        blockHeight,
                        blockTime,
                        blockHash,
                        blockMerkleRoot,
                        prevBlockHash
                    )
                } else {
                    Timber.d("request failed; isSuccessful:${currentHeadersResponse.isSuccessful} non-null body:${body != null}")
                    return@execute null
                }
            } catch (e: Exception) {
                Timber.e(e)
                return@execute null
            }
        }
    }

    suspend fun getTxByHash(txId: String): JsonObject {
        return execQueue.execute {
            val url = "https://api.whatsonchain.com/v1/bsv/main/tx/hash/$txId"

            val txResponse = apiService.fetch(
                url
            ).execute()

            val body = txResponse.body()

            if (txResponse.isSuccessful && body != null) {
                val jsonString = String(body.bytes())
                return@execute gson.fromJson(jsonString, JsonObject::class.java)
            } else {
                throw RuntimeException()
            }
        }
    }


    suspend fun sendHexTx(hexTx: String): String? {
        return execQueue.execute {
            val id = Random.nextInt()
            try {
                Trace.beginAsyncSection(Tr.METHOD_SEND_TX_TO_MINER, id)

                val requestContent = JsonObject()
                requestContent.addProperty("txhex", hexTx)

                val requestBody = requestContent.toString().toRequestBody()
                val response = apiService.postHexTx(requestBody).execute()
                val body = response.body()

                if (response.isSuccessful && body != null) {
                    var txId: String? = null
                    body.byteStream().bufferedReader().use { reader ->
                        while (true) {
                            val line = reader.readLine() ?: break
                            txId = line
                            Timber.d("body-line: $line")
                        }
                    }
                    return@execute txId
                }

                return@execute null
            } finally {
                Trace.endAsyncSection(Tr.METHOD_SEND_TX_TO_MINER, id)
            }
        }
    }


    suspend fun sendTxToBaemail(hexTx: String): Boolean {
        val id = Random.nextInt()
        try {
            Trace.beginAsyncSection(Tr.METHOD_SEND_TX_TO_BAEMAIL, id)

            val requestContent = JsonObject()
            requestContent.addProperty("rawtx", hexTx)

            val response = apiService.post(
                    "https://baemail.me/api/send",
                    requestContent.toString().toRequestBody()
            ).execute()



            val body = response.body()

            if (response.isSuccessful/* && body != null*/) {
//                var txId: String? = null
//                body.byteStream().bufferedReader().use { reader ->
//                    while (true) {
//                        val line = reader.readLine() ?: break
//                        txId = line
//                        Timber.d("body-line: $line")
//                    }
//                }
                return true
            } else {
                Timber.e(RuntimeException())
            }

        } finally {
            Trace.endAsyncSection(Tr.METHOD_SEND_TX_TO_BAEMAIL, id)
        }

        return false
    }


    fun getTwetchFilesMimeType(txId: String): String? {
        val url = "https://dogefiles.twetch.app/$txId"

        val txResponse = apiService.fetchHead(
                url
        ).execute()

        if (txResponse.isSuccessful) {
            return txResponse.headers()["content-type"]
        } else {
            throw RuntimeException()
        }
    }







}



data class AddressHistory(
    val address: String,
    val txHash: String,
    val blockHeight: Int
)

data class UnspentCoin(
    val address: String,
    val blockHeight: Int,
    val txPos: Int,
    val value: Long,
    val txHash: String
)

data class BlockInfo (
    val height: Int,
    val time: Long,
    val hash: String,
    val merkleRoot: String,
    val previousBlockHash: String
)

data class ExchangeRate(
    @SerializedName("currency") val currency: String,
    @SerializedName("rate") val rate: Double,
    @SerializedName("millisCreated") val millisCreated: Long
) {
    val satsInOneUnit: Int = if (rate == 0.0) -1 else (100_000_000 / rate).toInt()
    val satsInACent: Int = satsInOneUnit / 100
}

class ErrorStatusCodeException(
    val code: Int
) : RuntimeException()