package app.bitcoin.baemail.core.presentation

import app.bitcoin.baemail.core.data.nodejs.AndroidWorkHandlerProvider
import app.bitcoin.baemail.core.data.nodejs.AndroidWorkHandlerProviderImpl
import app.bitcoin.baemail.core.data.nodejs.NodeConnectedLiveData
import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.core.data.util.AuthenticatedPaymail
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.domain.usecases.CoinInfoUseCase
import kotlinx.coroutines.launch
import timber.log.Timber

class NodeModel(
    private val coroutineUtil: CoroutineUtil,
    private val authRepository: AuthRepository,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val secureDataSource: SecureDataSource,
    private val coinInfoUseCase: CoinInfoUseCase,
    private val nodeConnectedLiveData: NodeConnectedLiveData,
    private val nodeRuntimeInterface: NodeRuntimeInterface,
    private val androidWorkHandlerProvider: AndroidWorkHandlerProvider,
) {

    //todo move management of node-project to this place

    init {
        setupNodeToAndroidRequests()

        coroutineUtil.appScope.launch(coroutineUtil.defaultDispatcher) {
            authRepository.activePaymail.collect {
                it?.paymail ?: return@collect

                informActivePaymailChanged(it.paymail)
            }
        }

        nodeConnectedLiveData.observeForever { isConnected ->
            isConnected ?: return@observeForever

            if (isConnected) {
                val activePaymail = authRepository.activePaymail.value?.paymail ?: return@observeForever
                informActivePaymailChanged(activePaymail)
            }
        }

        coroutineUtil.appScope.launch(coroutineUtil.defaultDispatcher) {
            coinInfoUseCase().collect { fundingCoinInfo ->
                nodeRuntimeRepository.informCoinAvailabilityChanged(fundingCoinInfo)
            }
        }
    }

    private fun setupNodeToAndroidRequests() {
        if (androidWorkHandlerProvider is AndroidWorkHandlerProviderImpl) {
            androidWorkHandlerProvider.inject(nodeRuntimeRepository)
        }
        nodeRuntimeInterface.setup(androidWorkHandlerProvider)
    }


    private fun informActivePaymailChanged(paymail: String) {
        //inform the node-instance of the active paymail
        coroutineUtil.appScope.launch {
            val sendingPaymailWalletSeed = try {
                secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(paymail))
            } catch (e: Exception) {
                Timber.e(e, "error retrieving seed")
                return@launch
            }

            val pkiPath = try {
                secureDataSource.getPathForPaymailMnemonic(AuthenticatedPaymail(paymail))
            } catch (e: Exception) {
                Timber.d(e,"failed to retrieve paymail pki-path")
                return@launch
            }

            Timber.d("informing node of currently active paymail: $paymail")
            nodeRuntimeRepository.informActivePaymailChanged(
                paymail,
                sendingPaymailWalletSeed,
                pkiPath
            )
        }
    }

}