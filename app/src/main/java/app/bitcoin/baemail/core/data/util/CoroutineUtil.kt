package app.bitcoin.baemail.core.data.util

import kotlinx.coroutines.*
import java.util.concurrent.Executors

class CoroutineUtil {

    val appScope = CoroutineScope(Dispatchers.Default + SupervisorJob())

    val mainDispatcher = Dispatchers.Main
    val defaultDispatcher = Dispatchers.Default
    val ioDispatcher = Dispatchers.IO
    val fileLoggingDispatcher: CoroutineDispatcher by lazy {
        Executors.newSingleThreadExecutor().asCoroutineDispatcher()
    }
}