package app.bitcoin.baemail.core.presentation.view.recycler.lifecycle

import androidx.lifecycle.Lifecycle
import androidx.paging.LoadStateAdapter

abstract class LifecycleLoadStateAdapter <T : LifecycleViewHolder> : LoadStateAdapter<T>() {
    lateinit var parentLifecycle: Lifecycle

    override fun onViewAttachedToWindow(holder: T) {
        super.onViewAttachedToWindow(holder)
        holder.onAppear()
    }

    override fun onViewDetachedFromWindow(holder: T) {
        super.onViewDetachedFromWindow(holder)
        holder.onDisappear()
    }

    override fun onBindViewHolder(holder: T, position: Int, payloads: MutableList<Any>) {
        holder.onBind(parentLifecycle)
        super.onBindViewHolder(holder, position, payloads)
    }


    override fun onViewRecycled(holder: T) {
        holder.onRecycled()
    }
}