package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchSeparatorItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder

class ATTwetchSeparatorViewHolder(
    view: View
): BaseViewHolder(view) {

    var model: ATTwetchSeparatorItem? = null

    var drawableBackground = ColorDrawable(Color.TRANSPARENT)

    var sideMargins = (itemView.resources.displayMetrics.density * 16).toInt()

    init {
        itemView.background = drawableBackground
    }


    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATTwetchSeparatorItem

        val lp = itemView.layoutParams as ViewGroup.MarginLayoutParams
        if (lp.height != model.heightPx
            && lp.marginStart != sideMargins
            && lp.marginEnd != sideMargins
        ) {
            lp.height = model.heightPx
            lp.marginStart = sideMargins
            lp.marginEnd = sideMargins

            itemView.layoutParams = lp
            itemView.requestLayout()
        }

        if (drawableBackground.color != model.color) {
            drawableBackground.color = model.color
        }

    }



    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTwetchSeparatorViewHolder {
            return ATTwetchSeparatorViewHolder(
                inflater.inflate(
                    R.layout.li_twetch_separator,
                    parent,
                    false
                )
            )
        }
    }
}