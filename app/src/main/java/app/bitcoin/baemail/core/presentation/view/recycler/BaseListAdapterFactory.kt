package app.bitcoin.baemail.core.presentation.view.recycler

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.ViewGroup
import android.widget.ImageView
import app.bitcoin.baemail.twetch.ui.TwetchFilesMimeTypeHelper

class BaseListAdapterFactory(
    val context: Context,
    val twetchFilesMimeHelper: TwetchFilesMimeTypeHelper,
    val imageAspectRatioCache: ImageAspectRatioCache = ImageAspectRatioCache()
) {

    val intCache = HashMap<String, Int>()

}

class ImageAspectRatioCache {

    private val map = HashMap<String, Float>()

    fun applyMinHeightFromCachedRatio(view: ImageView, imageUrl: String, cachedWidth: Int?) {
        val ratio = map[imageUrl] ?: let {
            view.layoutParams.let { lp ->
                lp.height = ViewGroup.LayoutParams.WRAP_CONTENT
                view.adjustViewBounds = true
                view.layoutParams = lp
            }
            //Timber.d(">>>>>>>>>>>>>>> ratio not cached url:$imageUrl")
            return
        }
        if (ratio < 0.01f) {
            //Timber.d(">>>>>>>>>>>>>>> <0.01f; ratio not cached url:$imageUrl")
            return
        }
        if (cachedWidth == null) {
            //Timber.d(">>>>>>>>>>>>>>> cachedWidth; ratio not cached url:$imageUrl")
            return
        }

        val height = cachedWidth / ratio

        view.layoutParams.let { lp ->
            lp.height = height.toInt()
            view.adjustViewBounds = false
            view.layoutParams = lp
        }
        //view.minimumHeight = height.toInt()

    }

    fun cacheRatio(drawable: Drawable, imageUrl: String) {
        val w = drawable.intrinsicWidth
        if (w == -1) return

        map[imageUrl] = w.toFloat() / drawable.intrinsicHeight
    }
}
