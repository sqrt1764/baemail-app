package app.bitcoin.baemail.core.domain.repository

interface AppConfigRepository {
    companion object {
        const val KEY_BITCOIN_INDEXER_URL = "bitcoin_indexer_url"
        const val KEY_BITCOIN_INDEXER_ACCOUNT = "bitcoin_indexer_account"
        const val KEY_BITCOIN_INDEXER_PASSWORD = "bitcoin_indexer_password"
    }

    suspend fun getIndexerUrl(): String?
    suspend fun getIndexerAccount(): String?
    suspend fun getIndexerPassword(): String?

    suspend fun updateIndexerUrl(url: String?)
    suspend fun updateIndexerAccount(account: String?)
    suspend fun updateIndexerPassword(password: String?)
}