package app.bitcoin.baemail.core.presentation.view.recycler

import androidx.recyclerview.widget.DiffUtil

class AdapterTypeDiffCallback : DiffUtil.ItemCallback<AdapterType>() {
    override fun areItemsTheSame(itemOld: AdapterType, itemNew: AdapterType): Boolean {
        if (itemOld.type != itemNew.type) {
            return false
        }

        return when (itemOld.type) {
            AdapterTypeEnum.P2P_STALL -> {
                itemOld as ATP2pStallItem
                itemNew as ATP2pStallItem

                itemOld.name == itemNew.name
            }
            AdapterTypeEnum.SAVED_REPL_INFO -> {
                itemOld as ATSavedReplInfo
                itemNew as ATSavedReplInfo

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.SAVED_REPL_INPUT -> {
                itemOld as ATSavedReplInput
                itemNew as ATSavedReplInput

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.REPL_OUTPUT_ACTIONS -> {
                itemOld as ATReplOutputActions
                itemNew as ATReplOutputActions

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.REPL_INPUT -> {
                itemOld as ATReplInput
                itemNew as ATReplInput

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.REPL_OUTPUT -> {
                itemOld as ATReplOutput
                itemNew as ATReplOutput

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.REPL_RETURN -> {
                itemOld as ATReplReturn
                itemNew as ATReplReturn

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_NOTIFICATION -> {
                itemOld as ATTwetchNotificationItem
                itemNew as ATTwetchNotificationItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_POST_DETAIL_MAIN -> {
                itemOld as ATTwetchPostDetailMainItem
                itemNew as ATTwetchPostDetailMainItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_POST_LOADING -> {
                itemOld as ATTwetchPostLoadingItem
                itemNew as ATTwetchPostLoadingItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_POST_SECONDARY_LOADING -> {
                itemOld as ATTwetchPostSecondaryLoadingItem
                itemNew as ATTwetchPostSecondaryLoadingItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_SEPARATOR_0 -> {
                itemOld as ATTwetchSeparatorItem
                itemNew as ATTwetchSeparatorItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_SEARCH_POSTS_HINT -> {
                itemOld as ATTwetchSearchPostsHintItem
                itemNew as ATTwetchSearchPostsHintItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_NO_POSTS_FOUND -> {
                itemOld as ATTwetchNoPostsFoundItem
                itemNew as ATTwetchNoPostsFoundItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_TABS -> {
                itemOld as ATTwetchTabsItem
                itemNew as ATTwetchTabsItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_POST -> {
                itemOld as ATTwetchPostItem
                itemNew as ATTwetchPostItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_USER_PROFILE_ROOT -> {
                itemOld as ATTwetchUserProfileRootItem
                itemNew as ATTwetchUserProfileRootItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_CHAT_MESSAGE_OTHER -> {
                itemOld as ATTwetchChatMessageOtherItem
                itemNew as ATTwetchChatMessageOtherItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_CHAT_MESSAGE_ME -> {
                itemOld as ATTwetchChatMessageMeItem
                itemNew as ATTwetchChatMessageMeItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_CHAT_LIST_ITEM -> {
                itemOld as ATTwetchChatListItem
                itemNew as ATTwetchChatListItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.TWETCH_CHAT_PARTICIPANT -> {
                itemOld as ATTwetchChatParticipantItem
                itemNew as ATTwetchChatParticipantItem

                itemOld.participant.id == itemNew.participant.id
            }
            AdapterTypeEnum.MESSAGE_0 -> {
                itemOld as ATMessage0
                itemNew as ATMessage0

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.CONTENT_LOADING_0 -> {
                true
            }
            AdapterTypeEnum.SELECTED_SEED_WORD -> {
                itemOld as ATSelectedSeedWord
                itemNew as ATSelectedSeedWord

                itemOld.value == itemNew.value
            }
            AdapterTypeEnum.FILTER_SEED_WORD -> {
                itemOld as ATFilterSeedWord
                itemNew as ATFilterSeedWord

                itemOld.value == itemNew.value
            }
            AdapterTypeEnum.COIN_0 -> {
                itemOld as ATCoin
                itemNew as ATCoin

                itemOld.coin.txId == itemNew.coin.txId &&
                        itemOld.coin.address == itemNew.coin.address &&
                        itemOld.coin.outputIndex == itemNew.coin.outputIndex
            }
            AdapterTypeEnum.SECTION_TITLE_0 -> {
                itemOld as ATSectionTitle
                itemNew as ATSectionTitle

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.PAYMENT_ITEM -> {
                itemOld as ATPaymentItem
                itemNew as ATPaymentItem

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.HEIGHT_DECOR_DYNAMIC -> {
                itemOld as ATHeightDecorDynamic
                itemNew as ATHeightDecorDynamic

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.ACCOUNT_0,
            AdapterTypeEnum.ACTIVITY_0,
            AdapterTypeEnum.NO_PAYMAILS_ADDED,
            AdapterTypeEnum.AUTHENTICATED_PAYMAIL,
            AdapterTypeEnum.SEED_WORD_HINT,
            AdapterTypeEnum.FILTER_SEED_NO_MATCH,
            AdapterTypeEnum.UNUSED_ADDRESS_0 -> {
                false
            }
        }
    }

    override fun areContentsTheSame(itemOld: AdapterType, itemNew: AdapterType): Boolean {
        return when (itemOld.type) {
            AdapterTypeEnum.TWETCH_NOTIFICATION -> {
                itemOld as ATTwetchNotificationItem
                itemNew as ATTwetchNotificationItem

                itemOld.notification == itemNew.notification
            }
            AdapterTypeEnum.TWETCH_POST_DETAIL_MAIN -> {
                itemOld as ATTwetchPostDetailMainItem
                itemNew as ATTwetchPostDetailMainItem

                itemOld.holder == itemNew.holder
            }
            AdapterTypeEnum.TWETCH_POST_LOADING -> {
                itemOld as ATTwetchPostLoadingItem
                itemNew as ATTwetchPostLoadingItem

                itemOld == itemNew
            }
            AdapterTypeEnum.TWETCH_POST_SECONDARY_LOADING -> {
                itemOld as ATTwetchPostSecondaryLoadingItem
                itemNew as ATTwetchPostSecondaryLoadingItem

                itemOld == itemNew
            }
            AdapterTypeEnum.TWETCH_SEPARATOR_0 -> {
                itemOld as ATTwetchSeparatorItem
                itemNew as ATTwetchSeparatorItem

                itemOld == itemNew
            }
            AdapterTypeEnum.TWETCH_SEARCH_POSTS_HINT -> {
                itemOld as ATTwetchSearchPostsHintItem
                itemNew as ATTwetchSearchPostsHintItem

                itemOld == itemNew
            }
            AdapterTypeEnum.TWETCH_NO_POSTS_FOUND -> {
                itemOld as ATTwetchNoPostsFoundItem
                itemNew as ATTwetchNoPostsFoundItem

                itemOld == itemNew
            }
            AdapterTypeEnum.TWETCH_TABS -> {
                itemOld as ATTwetchTabsItem
                itemNew as ATTwetchTabsItem

                itemOld == itemNew
            }
            AdapterTypeEnum.TWETCH_POST -> {
                itemOld as ATTwetchPostItem
                itemNew as ATTwetchPostItem

                itemOld.holder == itemNew.holder
            }
            AdapterTypeEnum.TWETCH_USER_PROFILE_ROOT -> {
                itemOld as ATTwetchUserProfileRootItem
                itemNew as ATTwetchUserProfileRootItem

                itemOld == itemNew
            }
            AdapterTypeEnum.P2P_STALL -> {
                itemOld as ATP2pStallItem
                itemNew as ATP2pStallItem

                itemOld == itemNew
            }
            AdapterTypeEnum.SAVED_REPL_INFO -> {
                itemOld as ATSavedReplInfo
                itemNew as ATSavedReplInfo

                itemOld == itemNew
            }
            AdapterTypeEnum.SAVED_REPL_INPUT -> {
                itemOld as ATSavedReplInput
                itemNew as ATSavedReplInput

                itemOld == itemNew
            }
            AdapterTypeEnum.REPL_OUTPUT_ACTIONS -> {
                itemOld as ATReplOutputActions
                itemNew as ATReplOutputActions

                itemOld == itemNew
            }
            AdapterTypeEnum.REPL_INPUT -> {
                itemOld as ATReplInput
                itemNew as ATReplInput

                itemOld == itemNew
            }
            AdapterTypeEnum.REPL_OUTPUT -> {
                itemOld as ATReplOutput
                itemNew as ATReplOutput

                itemOld == itemNew
            }
            AdapterTypeEnum.REPL_RETURN -> {
                itemOld as ATReplReturn
                itemNew as ATReplReturn

                itemOld == itemNew
            }
            AdapterTypeEnum.TWETCH_CHAT_MESSAGE_OTHER -> {
                itemOld as ATTwetchChatMessageOtherItem
                itemNew as ATTwetchChatMessageOtherItem

                itemOld == itemNew
            }
            AdapterTypeEnum.TWETCH_CHAT_MESSAGE_ME -> {
                itemOld as ATTwetchChatMessageMeItem
                itemNew as ATTwetchChatMessageMeItem

                itemOld == itemNew
            }
            AdapterTypeEnum.TWETCH_CHAT_LIST_ITEM -> {
                itemOld as ATTwetchChatListItem
                itemNew as ATTwetchChatListItem

                itemOld.chat == itemNew.chat
            }
            AdapterTypeEnum.TWETCH_CHAT_PARTICIPANT -> {
                itemOld as ATTwetchChatParticipantItem
                itemNew as ATTwetchChatParticipantItem

                itemOld.participant == itemNew.participant
            }
            AdapterTypeEnum.MESSAGE_0 -> {
                itemOld as ATMessage0
                itemNew as ATMessage0

                itemOld == itemNew
            }
            AdapterTypeEnum.CONTENT_LOADING_0 -> {
                itemOld as ATContentLoading0
                itemNew as ATContentLoading0

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.SELECTED_SEED_WORD -> {
                itemOld as ATSelectedSeedWord
                itemNew as ATSelectedSeedWord

                itemOld.value == itemNew.value && itemOld.isDismissible == itemNew.isDismissible
            }
            AdapterTypeEnum.FILTER_SEED_WORD -> {
                itemOld as ATFilterSeedWord
                itemNew as ATFilterSeedWord

                itemOld.value == itemNew.value
            }
            AdapterTypeEnum.COIN_0 -> {
                itemOld as ATCoin
                itemNew as ATCoin

                itemOld.coin == itemNew.coin && itemOld.isLast == itemNew.isLast &&
                        itemOld.isSelected == itemNew.isSelected
            }
            AdapterTypeEnum.SECTION_TITLE_0 -> {
                itemOld as ATSectionTitle
                itemNew as ATSectionTitle

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.PAYMENT_ITEM -> {
                itemOld as ATPaymentItem
                itemNew as ATPaymentItem

                itemOld.id == itemNew.id
            }

            AdapterTypeEnum.HEIGHT_DECOR_DYNAMIC -> {
                itemOld as ATHeightDecorDynamic
                itemNew as ATHeightDecorDynamic

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.ACCOUNT_0,
            AdapterTypeEnum.ACTIVITY_0,
            AdapterTypeEnum.NO_PAYMAILS_ADDED,
            AdapterTypeEnum.AUTHENTICATED_PAYMAIL,
            AdapterTypeEnum.SEED_WORD_HINT,
            AdapterTypeEnum.FILTER_SEED_NO_MATCH,
            AdapterTypeEnum.UNUSED_ADDRESS_0 -> {
                true
            }
        }
    }
}