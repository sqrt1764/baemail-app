package app.bitcoin.baemail.core.data.nodejs.handler

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.JsonArray
import timber.log.Timber

/*

# expected structure of params array
[
    "yaanis@moneybutton.com"
]



# expected structure of result array
[
    "00ff99aa"
]


*/
class GetPaymailDestinationOutput(
    val doGetPaymailDestinationOutput: suspend (destinationPaymail: String)->String?
) : NodeRuntimeInterface.WorkHandler("GET_PAYMAIL_DESTINATION_OUTPUT") {

    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<NodeRuntimeInterface.Result, JsonArray> {

        try {
            val destinationPaymail = params[0].asString

            val outputScript = doGetPaymailDestinationOutput(destinationPaymail)

            val jsonArray = JsonArray()
            jsonArray.add(outputScript)

            return Pair(NodeRuntimeInterface.Result.SUCCESS, jsonArray)

        } catch (e: Exception) {
            Timber.e(e)
            return Pair(NodeRuntimeInterface.Result.FAIL, JsonArray())
        }

    }
}

