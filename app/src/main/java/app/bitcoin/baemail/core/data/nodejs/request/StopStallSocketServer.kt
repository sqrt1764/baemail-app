package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.JsonArray
import timber.log.Timber

/*
# expected structure of params
[
]


# expected structure of the response data passed in the callback
[
    true
]
 */
class StopStallSocketServer(
    val callback: (Boolean?)->Unit
) : NodeRuntimeInterface.NodeRequest("STOP_STALL_SOCKET_SERVER") {

    override fun getReqParams(): JsonArray {
        val finalArray = JsonArray()
        return finalArray
    }

    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "stall server not stopped; token: $token")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(stackTrace, "unexpected response format; token: $token")
            callback(null)
            return
        }

        try {
            callback(data[0].asBoolean)

        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")
            callback(null)
        }
    }
}