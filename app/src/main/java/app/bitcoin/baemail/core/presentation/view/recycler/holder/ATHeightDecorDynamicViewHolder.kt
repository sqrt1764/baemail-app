package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder

class ATHeightDecorDynamicViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    var model: ATHeightDecorDynamic? = null

    private val observer = Observer<Int> {
        val applyHeight = Runnable {
            val height = model?.heightLD?.value ?: return@Runnable
            val lp = itemView.layoutParams

            if (lp.height == height) return@Runnable

            lp.height = height

            itemView.layoutParams = lp
        }

        if (itemView.isInLayout) {
            itemView.post(applyHeight)
            return@Observer
        }

        applyHeight.run()
    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATHeightDecorDynamic

        model.heightLD.observe(this, observer)

        val decorHeight: Int = model.heightLD.value.let {
            if (it == null) return@let ViewGroup.LayoutParams.WRAP_CONTENT

            it
        }

        //apply initial
        observer.onChanged(decorHeight)
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATHeightDecorDynamicViewHolder {
            return ATHeightDecorDynamicViewHolder(
                inflater.inflate(
                    R.layout.li_height_decor_dynamic,
                    parent,
                    false
                )
            )
        }
    }
}