package app.bitcoin.baemail.core.presentation.entity

import app.bitcoin.baemail.R

enum class CentralTab(val id: Int, val labelResource: Int, val iconResource: Int) {
    BAEMAIL(12, R.string.main_tabs_inbox_title, R.drawable.main_tabs_inbox_icon),
    PAYMENTS(16, R.string.main_tabs_payments_title, R.drawable.main_tabs_payments_icon),
    TWETCH(17, R.string.main_tabs_twetch_title, R.drawable.main_tabs_twetch_icon),
    REPL(18, R.string.main_tabs_repl_title, R.drawable.main_tabs_repl_icon),
    P2P(19, R.string.main_tabs_p2p_title, R.drawable.main_tabs_p2p_icon),
}