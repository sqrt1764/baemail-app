package app.bitcoin.baemail.core.data.wallet

import app.bitcoin.baemail.core.data.util.CoroutineUtil
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.github.centrifugal.centrifuge.*
import io.github.centrifugal.centrifuge.EventListener
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import timber.log.Timber


class NewBlockDetector(
    val coroutineUtil: CoroutineUtil,
    val gson: Gson
) {
    companion object {
        private const val NEW_BLOCK_POLLING_ENDPOINT = "wss://socket.whatsonchain.com/blockheaders?format=protobuf"
        private const val BLOCK_POLLING_CHANNEL = "woc:blockHeader"
    }

    private var client: Client? = null

    private val _currentBlockState = MutableStateFlow(
        BlockInfo(
        659311,
        1604175885L,
        "0000000000000000054ae6e15c579e3f04dd6d73445878952b28029298b42aa4",
        "533bc72eb1e8a3a9c1eb74b61c45e179deefd52bfc0b2454af87aeb078c2493f",
        "00000000000000000527d29c79cfb00d17eee38503871c1060aed42bc637cf4e"
    )
    )

    val currentBlockState: StateFlow<BlockInfo>
        get() = _currentBlockState.asStateFlow()

    private lateinit var onError: ()->Unit

    fun setup(onError: ()->Unit) {
        this.onError = onError
    }

    fun initialize(currentBlock: BlockInfo) {
        _currentBlockState.value = currentBlock

        startPolling()
    }

    fun stop() {
        Timber.d("#stop")
        client?.disconnect()
        client = null
    }

    private fun startPolling() {
        Client(
            NEW_BLOCK_POLLING_ENDPOINT,
            Options().also {
                it.timeout = 15000
            },
            clientListener
        ).let {
            client = it

            it.connect()
        }
    }


    private fun onError(e: Exception) {
        Timber.e(e)
        onError()
    }

    private fun onBlockHeaderReceived(content: String) {
        val json = gson.fromJson(content, JsonObject::class.java)

        val height = json.get("height").asInt
        val time = json.get("time").asLong * 1000
        val hash = json.get("hash").asString
        val merkleRoot = json.get("merkleroot").asString
        val previousBlockHash = json.get("previousblockhash").asString

        _currentBlockState.value = BlockInfo(
            height,
            time,
            hash,
            merkleRoot,
            previousBlockHash
        )
    }


    val channelSubscriptionListener = object : SubscriptionEventListener() {
        override fun onPublish(sub: Subscription?, event: PublishEvent?) {
            Timber.d("onPublish")
            event?.data?.let { String(it) }?.let { content ->
                onBlockHeaderReceived(content)
            }
        }

        override fun onJoin(sub: Subscription?, event: JoinEvent?) {
            Timber.d("onJoin")
        }

        override fun onLeave(sub: Subscription?, event: LeaveEvent?) {
            Timber.d("onLeave")
        }

        override fun onSubscribeSuccess(sub: Subscription?, event: SubscribeSuccessEvent?) {
            Timber.d("onSubscribeSuccess")
        }

        override fun onSubscribeError(sub: Subscription?, event: SubscribeErrorEvent?) {
            Timber.d("onSubscribeError ...message:${event?.message} ...code:${event?.code}")
            //client!!.disconnect()
        }

        override fun onUnsubscribe(sub: Subscription?, event: UnsubscribeEvent?) {
            Timber.d("onUnsubscribe")
        }
    }


    val clientListener: EventListener = object : EventListener() {
        override fun onConnect(client: Client, event: ConnectEvent?) {
            client.newSubscription(BLOCK_POLLING_CHANNEL, channelSubscriptionListener)
            Timber.d("socket connected to `woc` ...subscribed to channel:$BLOCK_POLLING_CHANNEL")
        }

        override fun onDisconnect(client: Client?, event: DisconnectEvent?) {
            Timber.d("onDisconnect ...reason:${event?.reason} ...reconnect:${event?.reconnect}")
        }

        override fun onError(client: Client?, event: ErrorEvent?) {
            Timber.d("onError $event")
            onError(RuntimeException(event.toString()))
        }

        override fun onMessage(client: Client?, event: MessageEvent?) {
            val data = event?.data?.let { String(it) }
            Timber.d("onMessage ...data:$data")
        }

        override fun onRefresh(client: Client?, event: RefreshEvent?, cb: TokenCallback?) {
            Timber.d("onRefresh")
        }

        override fun onPrivateSub(
            client: Client?,
            event: PrivateSubEvent?,
            cb: TokenCallback?
        ) {
            Timber.d("onPrivateSub")
        }
    }


}