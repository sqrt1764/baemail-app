package app.bitcoin.baemail.core.presentation.drawable

import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.annotation.IntRange

class DrawableExpandingTopStroke : Drawable() {

    private var strokeWidth = 0

    private var paddingTop = 0

    private var expandedAmount = 0f

    private var strokeColor = Color.RED
    private var _alpha = 255

    private val strokePaint = Paint(Paint.ANTI_ALIAS_FLAG)

    fun setStrokeColor(color: Int) {
        strokeColor = color

        val finalAlpha = (((Color.alpha(color) / 255f) * (_alpha / 255f)) * 255).toInt()

        val finalColor = Color.argb(
            finalAlpha, Color.red(strokeColor), Color.green(strokeColor), Color.blue(strokeColor)
        )

        strokePaint.color = finalColor

        invalidateSelf()
    }

    fun setStrokeWidths(width: Int = 0) {
        strokeWidth = width

        invalidateSelf()
    }

    fun setPadding(top: Int = 0) {
        paddingTop = top
    }

    fun setPercentExpanded(expandedAmount: Float) {
        if (expandedAmount > 1) {
            this.expandedAmount = 1f
            invalidateSelf()
            return
        }
        if (expandedAmount < 0) {
            this.expandedAmount = 0f
            invalidateSelf()
            return
        }

        if (expandedAmount == this.expandedAmount) return

        this.expandedAmount = expandedAmount
        invalidateSelf()
    }

    override fun draw(canvas: Canvas) {
        if (_alpha == 0) return

        val strokeWidth = expandedAmount * bounds.width()
        val leftOffset = (bounds.width() - strokeWidth) / 2

        // top stroke
        canvas.drawRect(
            bounds.left + leftOffset,
            bounds.top + paddingTop.toFloat(),
            bounds.left + leftOffset + strokeWidth,
            (this.strokeWidth + paddingTop + bounds.top).toFloat(),
            strokePaint
        )
    }

    override fun setAlpha(@IntRange(from = 0, to = 255) alpha: Int) {
        _alpha = alpha

        setStrokeColor(strokeColor)
    }

    override fun getAlpha(): Int {
        return _alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        // not supported
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }
}