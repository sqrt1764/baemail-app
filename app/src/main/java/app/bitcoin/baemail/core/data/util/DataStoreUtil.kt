package app.bitcoin.baemail.core.data.util

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import app.bitcoin.baemail.twetch.ui.TwetchChatInfoStore


val Context.replPrefs: DataStore<Preferences>
        by preferencesDataStore(
            name = "repl_input_store"
        )

val Context.baemailCachingPrefs: DataStore<Preferences>
        by preferencesDataStore(
            name = "baemail"
        )

val Context.baemailMessageDraftPrefs: DataStore<Preferences>
        by preferencesDataStore(
            name = "draft_new_message"
        )

val Context.twetchFilesMimeCachingPrefs: DataStore<Preferences>
        by preferencesDataStore(
            name = "twetch_files_mime_caching_prefs"
        )

val Context.twetchChatMessageSyncMetaPrefs: DataStore<Preferences>
        by preferencesDataStore(
            name = "twetch_chat_message_sync_meta"
        )

val Context.appUtilPrefs: DataStore<Preferences>
        by preferencesDataStore(
            name = "app_util"
        )


val Context.twetchAuthPrefs: DataStore<Preferences>
        by preferencesDataStore(
            name = "twetch"
        )

val Context.twetchChatInfoStore: DataStore<Preferences>
        by preferencesDataStore(
            name = "twetch_chats"
        )


val Context.p2pPrefs: DataStore<Preferences>
        by preferencesDataStore(
            name = "p2p"
        )
