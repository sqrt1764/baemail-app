package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.Outline
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableOval
import app.bitcoin.baemail.core.presentation.view.recycler.ATSelectedSeedWord
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import kotlin.math.min

class ATSelectedSeedWordViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATSelectedSeedWordListener

    var model: ATSelectedSeedWord? = null

    val word: TextView = itemView.findViewById(R.id.word)
    val dismiss: ImageView = itemView.findViewById(R.id.dismiss)

    init {
        //todo
        //todo
        //todo
        val dp = itemView.resources.displayMetrics.density

        val drawableOval = DrawableOval(itemView.context).also {
            val color = itemView.context.getColorFromAttr(R.attr.colorPrimary)

            it.setBgColor(color)
            it.setFullyRounded()
            it.setStrokeWidth(0f)
            it.setPaddings(6 * dp, 16 * dp, 6 * dp, 16 * dp)
        }

        itemView.background = drawableOval


        dismiss.clipToOutline = true
        dismiss.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return
                if (view.width < dp) return
                if (view.height < dp) return

                val left = dp.toInt()
                val top = dp.toInt()
                val right = (view.width - dp).toInt()
                val bottom = (view.height - dp).toInt()

                val side = min(right - left, bottom - top)
                val radius = side / 2

                val centerX = (right - left) / 2 + left
                val centerY = (bottom - top) / 2 + top


                outline.setOval(
                    centerX - radius,
                    centerY - radius,
                    centerX + radius,
                    centerY + radius
                )
            }

        }

        dismiss.setOnClickListener { listener.onDismissClicked() }
    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.listener = listener!! as BaseListAdapter.ATSelectedSeedWordListener
        this.model = model as ATSelectedSeedWord

        word.text = model.value

        if (model.isDismissible) {
            dismiss.visibility = View.VISIBLE
        } else {
            dismiss.visibility = View.GONE
        }
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATSelectedSeedWordViewHolder {
            return ATSelectedSeedWordViewHolder(
                inflater.inflate(
                    R.layout.li_selected_seed_word,
                    parent,
                    false
                )
            )
        }
    }
}