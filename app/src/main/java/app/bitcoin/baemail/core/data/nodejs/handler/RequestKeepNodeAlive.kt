package app.bitcoin.baemail.core.data.nodejs.handler

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.JsonArray
import timber.log.Timber

class RequestKeepNodeAlive(
    val requestNow: ()->Unit
) : NodeRuntimeInterface.WorkHandler("REQUEST_KEEP_ALIVE") {


    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<NodeRuntimeInterface.Result, JsonArray> {

        try {
            requestNow()

            return Pair(NodeRuntimeInterface.Result.SUCCESS, JsonArray())

        } catch (e: Exception) {
            Timber.e(e)
            return Pair(NodeRuntimeInterface.Result.FAIL, JsonArray())

        }

    }


}