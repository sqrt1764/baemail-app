package app.bitcoin.baemail.core.data.room.converters

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import app.bitcoin.baemail.message.data.entity.MessageBucket
import app.bitcoin.baemail.twetch.ui.TwetchChatInfoStore
import com.google.gson.Gson
import com.google.gson.JsonArray

@ProvidedTypeConverter
class DbConverters(
    private val gson: Gson
) {

    @TypeConverter
    fun serializeStringList(list: List<String>): String {
        val jsonList = JsonArray()
        list.forEach {
            jsonList.add(it)
        }

        return jsonList.toString()
    }

    @TypeConverter
    fun parseStringList(serialized: String): List<String> {
        val jsonList = gson.fromJson(serialized, JsonArray::class.java)
        val list = mutableListOf<String>()

        (0 until jsonList.size()).forEach {
            list.add(jsonList.get(it).asString)
        }

        return list
    }

    @TypeConverter
    fun serializeMessageBucket(bucket: MessageBucket): String {
        return bucket.const
    }

    @TypeConverter
    fun parseMessageBucket(serialized: String): MessageBucket {
        return MessageBucket.parse(serialized)
    }

    @TypeConverter
    fun serializeParticipant(participant: TwetchChatInfoStore.Participant?): String? {
        return participant?.let {
            gson.toJson(participant)
        }
    }

    @TypeConverter
    fun parseParticipant(serialized: String?): TwetchChatInfoStore.Participant? {
        return serialized?.let {
            gson.fromJson(serialized, TwetchChatInfoStore.Participant::class.java)
        }
    }


}