package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.HorizontalScrollView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchTabsItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import com.google.android.material.button.MaterialButton
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@FlowPreview
@ExperimentalCoroutinesApi
class ATTwetchTabsViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    private lateinit var listener: BaseListAdapter.ATTwetchTabsListener

    private var model: ATTwetchTabsItem? = null


    private val tabScrollView: HorizontalScrollView = itemView.findViewById(R.id.tab_button_scroller)
    private val tab0: MaterialButton = itemView.findViewById(R.id.tab_0)
    private val tab1: MaterialButton = itemView.findViewById(R.id.tab_1)
    private val tab2: MaterialButton = itemView.findViewById(R.id.tab_2)
    private val tab3: MaterialButton = itemView.findViewById(R.id.tab_3)
    private val tab4: MaterialButton = itemView.findViewById(R.id.tab_4)
    private val tab5: MaterialButton = itemView.findViewById(R.id.tab_5)
    private val tab6: MaterialButton = itemView.findViewById(R.id.tab_6)
    private val tab7: MaterialButton = itemView.findViewById(R.id.tab_7)

    private val tabViewList = listOf(
        tab0,
        tab1,
        tab2,
        tab3,
        tab4,
        tab5,
        tab6,
        tab7
    )

    private val colorPrimary: Int = itemView.context.getColorFromAttr(R.attr.colorPrimary)

    private val colorDanger: Int = itemView.context.getColorFromAttr(R.attr.colorDanger)


    private var lastAppliedTabs: List<TabItem> = emptyList()

    init {


        tabScrollView.overScrollMode = View.OVER_SCROLL_NEVER


        //todo helper.cachedTabsScrolledAmount = tabScrollView.scrollX


        tabViewList.forEach {
            it.setOnClickListener { view ->
                val m = model ?: return@setOnClickListener
                val tabItem = view.tag ?: return@setOnClickListener
                tabItem as TabItem

                lastAppliedTabs = listener.onTabClicked(m.id, tabItem)
                initializeTabs()
            }
        }

        itemView.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                if (::listener.isInitialized) {
                    listener.onAttached(this@ATTwetchTabsViewHolder)
                }
            }

            override fun onViewDetachedFromWindow(v: View) {
                if (::listener.isInitialized) {
                    listener.onDetached()
                }
            }

        })
    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATTwetchTabsItem

        this.listener = listener!! as BaseListAdapter.ATTwetchTabsListener

        lastAppliedTabs = this.listener.onBound(model.id)
        initializeTabs()
    }

    private fun initializeTabs() {
        if (lastAppliedTabs.size > 8) throw RuntimeException()

        val extraViews = tabViewList.size - lastAppliedTabs.size

        if (extraViews > 0) {
            //remove the extra views that will not be necessary further down
            for (i in lastAppliedTabs.size until tabViewList.size) {
                tabViewList[i].visibility = View.GONE
            }
        }

        lastAppliedTabs.forEachIndexed { index, tabItem ->
            val matchingView = tabViewList[index]

            if (matchingView.visibility != View.VISIBLE) {
                matchingView.visibility = View.VISIBLE
            }

            matchingView.tag = tabItem

            matchingView.text = tabItem.label

            if (tabItem.active) {
                matchingView.setTextColor(colorDanger)
                matchingView.strokeColor = ColorStateList.valueOf(colorDanger)
            } else {
                matchingView.setTextColor(colorPrimary)
                matchingView.strokeColor = ColorStateList.valueOf(colorPrimary)
            }


        }



    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory?,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTwetchTabsViewHolder {
            return ATTwetchTabsViewHolder(
                inflater.inflate(
                    R.layout.li_twetch_tabs,
                    parent,
                    false
                )
            )
        }
    }
}

///////////////////////////////////////////////

data class TabItem(
    val id: String,
    val label: String,
    val active: Boolean
)