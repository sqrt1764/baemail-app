package app.bitcoin.baemail.core.presentation.view.recycler

import android.view.View
import app.bitcoin.baemail.core.presentation.view.recycler.lifecycle.LifecycleViewHolder

abstract class BaseViewHolder(itemView: View) : LifecycleViewHolder(itemView) {
    abstract fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?)
}