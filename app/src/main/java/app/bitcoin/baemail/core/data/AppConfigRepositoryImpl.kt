package app.bitcoin.baemail.core.data

import app.bitcoin.baemail.core.domain.repository.AppConfigRepository
import app.bitcoin.baemail.core.domain.repository.AppConfigRepository.Companion.KEY_BITCOIN_INDEXER_ACCOUNT
import app.bitcoin.baemail.core.domain.repository.AppConfigRepository.Companion.KEY_BITCOIN_INDEXER_PASSWORD
import app.bitcoin.baemail.core.domain.repository.AppConfigRepository.Companion.KEY_BITCOIN_INDEXER_URL
import app.bitcoin.baemail.core.data.util.SecureConfigDataSource

class AppConfigRepositoryImpl(
    private val secureConfigDataSource: SecureConfigDataSource
) : AppConfigRepository {
    override suspend fun getIndexerUrl(): String? {
        return secureConfigDataSource.getConfig(KEY_BITCOIN_INDEXER_URL)
    }

    override suspend fun getIndexerAccount(): String? {
        return secureConfigDataSource.getConfig(KEY_BITCOIN_INDEXER_ACCOUNT)
    }

    override suspend fun getIndexerPassword(): String? {
        return secureConfigDataSource.getConfig(KEY_BITCOIN_INDEXER_PASSWORD)
    }

    override suspend fun updateIndexerUrl(url: String?) {
        if (url.isNullOrBlank()) {
            secureConfigDataSource.removeConfig(KEY_BITCOIN_INDEXER_URL)
        } else {
            secureConfigDataSource.addConfig(KEY_BITCOIN_INDEXER_URL, url)
        }
    }

    override suspend fun updateIndexerAccount(account: String?) {
        if (account.isNullOrBlank()) {
            secureConfigDataSource.removeConfig(KEY_BITCOIN_INDEXER_ACCOUNT)
        } else {
            secureConfigDataSource.addConfig(KEY_BITCOIN_INDEXER_ACCOUNT, account)
        }
    }

    override suspend fun updateIndexerPassword(password: String?) {
        if (password.isNullOrBlank()) {
            secureConfigDataSource.removeConfig(KEY_BITCOIN_INDEXER_PASSWORD)
        } else {
            secureConfigDataSource.addConfig(KEY_BITCOIN_INDEXER_PASSWORD, password)
        }
    }

}
