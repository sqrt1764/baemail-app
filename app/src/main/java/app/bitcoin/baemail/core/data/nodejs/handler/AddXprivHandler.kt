package app.bitcoin.baemail.core.data.nodejs.handler

import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.JsonArray
import timber.log.Timber

class AddXprivHandler(
    val secureDataSource: SecureDataSource
) : NodeRuntimeInterface.WorkHandler("ADD_XPRIV") {



    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<NodeRuntimeInterface.Result, JsonArray> {

        try {
            val xpriv = params[0].asString

            val alias = secureDataSource.addCustom(xpriv)


            val jsonArray = JsonArray()
            jsonArray.add(alias)

            return Pair(NodeRuntimeInterface.Result.SUCCESS, jsonArray)

        } catch (e: Exception) {
            Timber.e(e)
            return Pair(NodeRuntimeInterface.Result.FAIL, JsonArray())

        }

    }
}