package app.bitcoin.baemail.core.presentation.entity

data class CentralTabModel(
    val tab: CentralTab,
    val colorId: Int,
    val isSelected: Boolean
)