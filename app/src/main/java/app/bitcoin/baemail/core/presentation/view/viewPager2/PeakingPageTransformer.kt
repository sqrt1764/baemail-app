package app.bitcoin.baemail.core.presentation.view.viewPager2

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import kotlin.math.abs

class PeakingPageTransformer(
    private val peakSpaceWidthPx: Float,
    private val peakGapPx: Float
) : ViewPager2.PageTransformer {

    override fun transformPage(view: View, position: Float) {
        view.apply {
            val pageWidth = width
            val pageHeight = height
            when {
                position < -1 -> { // [-Infinity,-1)
                    // This page is way off-screen to the left.
//                    alpha = 0f
                    translationX = (2 * peakSpaceWidthPx - peakGapPx)
                }
                position <= 1 -> { // [-1,1]
                    // Modify the default slide transition to shrink the page as well


                    translationX = if (position == 0f) {
                        0f
                    } else if (position < 0) {
                        (2 * peakSpaceWidthPx - peakGapPx) * abs(position)
                    } else {
                        -1 * (2 * peakSpaceWidthPx - peakGapPx) * abs(position)
                    }



//                    val scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position))
//                    val vertMargin = pageHeight * (1 - scaleFactor) / 2
//                    val horzMargin = pageWidth * (1 - scaleFactor) / 2
//                    translationX = if (position < 0) {
//                        horzMargin - vertMargin / 2
//                    } else {
//                        horzMargin + vertMargin / 2
//                    }
//
//                    // Scale the page down (between MIN_SCALE and 1)
//                    scaleX = scaleFactor
//                    scaleY = scaleFactor

//                    // Fade the page relative to its size.
//                    alpha = (MIN_ALPHA +
//                            (((scaleFactor - MIN_SCALE) / (1 - MIN_SCALE)) * (1 - MIN_ALPHA)))
                }
                else -> { // (1,+Infinity]
                    // This page is way off-screen to the right.
//                    alpha = 0f
                    translationX = -1 * (2 * peakSpaceWidthPx - peakGapPx)
                }
            }
        }
    }

    companion object {

        val MIN_SCALE = 0.8f
        val MIN_ALPHA = 0.6f
    }
}
