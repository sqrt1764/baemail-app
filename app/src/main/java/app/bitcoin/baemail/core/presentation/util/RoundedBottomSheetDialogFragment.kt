package app.bitcoin.baemail.core.presentation.util

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import app.bitcoin.baemail.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import timber.log.Timber

abstract class RoundedBottomSheetDialogFragment : BottomSheetDialogFragment() {

    companion object {
        const val CONST_UNSET = -1
        const val CONST_MAX = 0
    }

    lateinit var behavior: BottomSheetBehavior<FrameLayout>

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.setOnShowListener { dialogInterface ->
            val d = dialogInterface as BottomSheetDialog
            val bottomSheet = d.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.state = getStartingDialogState()

            var peekHeight = getPeekHeight()
            if (peekHeight != CONST_UNSET) {
                if (peekHeight > bottomSheet.height || peekHeight < 100) {
                    Timber.w("#getPeekHeight() returned an invalid value, overriding it")
                    peekHeight = bottomSheet.height
                }

                bottomSheetBehavior.peekHeight = peekHeight
            }
            bottomSheetBehavior.skipCollapsed = true
            bottomSheetBehavior.isFitToContents = shouldFitToContents()

            behavior = bottomSheetBehavior

            onDialogShown()
        }

        return dialog
    }


    abstract fun getPeekHeight(): Int

    open fun getStartingDialogState(): Int = BottomSheetBehavior.STATE_COLLAPSED

    open fun getBottomSheetBehavior(): BottomSheetBehavior<FrameLayout> = behavior

    open fun shouldFitToContents(): Boolean = false

    open fun onDialogShown() {}
}