package app.bitcoin.baemail.core.data.nodejs.handler

import app.bitcoin.baemail.core.data.nodejs.CoinRefreshRequestHelper
import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.JsonArray
import timber.log.Timber

/*
expected `coinsToCheck` structure
[
    {
        tx_id: '00..ff',
        address: '1FFF..FF',
        index: 0
    },
    {
        tx_id: '00..ee',
        address: '1eee..ee',
        index: 2
    },
    ..
]

*/
class RequestRefreshOfCoinsHandler(
    private val coinRefreshRequestHelper: CoinRefreshRequestHelper,
    private val getActivePaymail: ()->String
) : NodeRuntimeInterface.WorkHandler("REQUEST_REFRESH_OF_COINS") {


    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<NodeRuntimeInterface.Result, JsonArray> {
        try {
            val activePaymail = getActivePaymail()

            val coinJsonArray = params[0].asJsonArray
            val coinRefreshRequests = coinJsonArray.mapNotNull { coinJson ->
                val coinTxId = coinJson.asJsonObject.get("tx_id").asString
                val address = coinJson.asJsonObject.get("address").asString
                val index = coinJson.asJsonObject.get("index").asInt

                CoinRefreshRequestHelper.Request(
                    activePaymail,
                    coinTxId,
                    address,
                    index
                )
            }

            if (coinRefreshRequests.isNotEmpty()) {
                coinRefreshRequestHelper.enqueueRefresh(coinRefreshRequests)
            } else {
                Timber.d("zero requests created")
            }

            return Pair(NodeRuntimeInterface.Result.SUCCESS, JsonArray())

        } catch (e: Exception) {
            Timber.e(e)
            return Pair(NodeRuntimeInterface.Result.FAIL, JsonArray())

        }
    }


}