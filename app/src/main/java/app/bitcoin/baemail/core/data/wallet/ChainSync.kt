package app.bitcoin.baemail.core.data.wallet

import android.content.Context
import app.bitcoin.baemail.core.data.nodejs.CoinRefreshRequestHelper
import app.bitcoin.baemail.core.data.room.AppDatabase
import app.bitcoin.baemail.core.data.room.entity.BlockSyncInfo
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import timber.log.Timber
import kotlin.Exception
import kotlin.coroutines.resume

//todo what happens in the situation when user triggered manual refreshing of coins
// and a new block gets discovered before the refresh logic has completed?
class ChainSync(
    val context: Context,
    val appDatabase: AppDatabase,
    val coroutineUtil: CoroutineUtil,
    val blockchainDataSource: BlockchainDataSource,
    val fullSyncHelper: WocFullSyncHelper,
    val newBlockDetector: NewBlockDetector
) {

    private val _status = MutableStateFlow(ChainSyncStatus.DISCONNECTED)

    val status = _status.combine(fullSyncHelper.isSyncing) { status, isSyncing ->
        return@combine if (isSyncing) {
            ChainSyncStatus.INITIALISING
        } else {
            status
        }
    }


    private lateinit var paymail: String


    private var newBlockObservationJob: Job? = null


    private var isSyncingNow = true
    private var resumeBlockDetectorContinuation: CancellableContinuation<Unit>? = null
    private var resumeCoinRefreshRequestContinuation: CancellableContinuation<Unit>? = null


    val liveBlockInfo: StateFlow<BlockInfo>
        get() = newBlockDetector.currentBlockState

    private var initializeJob: Job? = null
//    private var coinRefreshRequestProcessingJob: Job? = null

    suspend fun initialize(
        paymail: String,
        seed: List<String>,
        fundingWalletPath:String,
        coinRefreshRequestFlow: Flow<CoinRefreshRequestHelper.Request>
    ) = withContext(coroutineUtil.ioDispatcher) {
        Timber.d("starting chain-sync of funding coins of paymail:$paymail")

        this@ChainSync.paymail = paymail

        fullSyncHelper.setup(
            paymail,
            seed,
            fundingWalletPath
        )

        newBlockDetector.setup {
            Timber.e(RuntimeException("newBlockDetector#onError"))
            //onListenError(RuntimeException())
        }

        initializeJob?.cancel()
        initializeJob = launch {
            doInitialize()
        }

//        coinRefreshRequestProcessingJob?.cancel()
//        coinRefreshRequestProcessingJob = launch { //todo disabled for now; look into restoring this when the coin-sync is now again functional
//            coinRefreshRequestFlow.collect { request ->
//                try {
//                    awaitSyncCompletedOfCoinRefreshRequest()
//                    Timber.d("coin-refresh request received; processing $request ...")
//
//                    val matchingDbCoin = appDatabase.coinDao().getCoin(
//                        request.coinAddress,
//                        request.coinTxId,
//                        request.coinOutputIndex
//                    ) ?: let {
//                        Timber.d("db-coin not found; dropping request")
//                        return@collect
//                    }
//                    if (matchingDbCoin.spendingTxId != null) {
//                        Timber.d("db-coin has spendingTxId that is NOT NULL; dropping request")
//                        return@collect
//                    }
//                    Timber.d("matchingDbCoin: $matchingDbCoin")
//
//                    val txConfirmation = appDatabase.txConfirmationDao().getByTxHash(matchingDbCoin.txId)
//
//                    val spendingInfo = wocExecQueue.execute {
//                        blockchainDataSource.findSpendOfCoinFromWhatsOnChain(
//                            matchingDbCoin,
//                            txConfirmation
//                        )
//                    }
//
//                    if (spendingInfo == null) {
//                        Timber.d("spend not found; coin: $request")
//                        return@collect
//                    }
//
//                    val updatedCoin = matchingDbCoin.copy(
//                        spendingTxId = spendingInfo.txHash
//                    )
//                    appDatabase.coinDao().insertCoin(updatedCoin)
//
//                    Timber.d("spendingInfo updated: $spendingInfo; request: $request")
//                    appDatabase.txConfirmationDao().insertConfirmationInfo(spendingInfo)
//
//
//                } catch (e: Exception) {
//                    Timber.e(e)
//                }
//            }
//        }
    }

    fun disconnect() {
        Timber.d("#disconnect")
        releaseEverything()

        _status.value = ChainSyncStatus.DISCONNECTED
    }

    private fun releaseEverything() {
        //stop & clean everything up

        isSyncingNow = true

        initializeJob?.cancel()
        initializeJob = null



        resumeBlockDetectorContinuation?.cancel()
        resumeBlockDetectorContinuation = null


        //cleanup new block observing
        newBlockObservationJob?.cancel()
        newBlockObservationJob = null
        newBlockDetector.stop()

    }

    private suspend fun doInitialize() = withContext(Dispatchers.IO) {

        _status.value = ChainSyncStatus.INITIALISING

        val info = blockchainDataSource.fetchCurrentStatusFromWhatsOnChain()
            ?: let {
                onSyncError(RuntimeException())
                return@withContext
            }
        Timber.d("current info: $info")

        startCheckingForNewBlocks(info)

        val didSucceed = syncCoins(info)
        if (!didSucceed) {
            Timber.d("syncCoins failed; returning")
            return@withContext
        }

        ensureActive()

        _status.value = ChainSyncStatus.LIVE

    }



    //////////////////////////

    private suspend fun syncCoins(info: BlockInfo): Boolean {
        var success = false
        try {
            isSyncingNow = true

            success = doSyncCoins(info)
            return success

        } catch (e: Exception) {
            Timber.e(e)
            if (_status.value != ChainSyncStatus.ERROR) {
                onSyncError(e)
            }
            return success

        } finally {
            isSyncingNow = false
            onAfterSync(info, success)
        }
    }

    private suspend fun onAfterSync(info: BlockInfo, wasSuccess: Boolean) {
        if (wasSuccess) {
            persistSuccessfulSync(info)

            //resume any continuations awaiting end of sync
            resumeBlockDetectorContinuation?.resume(Unit)
            resumeBlockDetectorContinuation = null

            resumeCoinRefreshRequestContinuation?.resume(Unit)
            resumeCoinRefreshRequestContinuation = null

            return
        }

        resumeBlockDetectorContinuation?.cancel()
        resumeBlockDetectorContinuation = null

        resumeCoinRefreshRequestContinuation?.cancel()
        resumeCoinRefreshRequestContinuation = null



    }


    private suspend fun doSyncCoins(currentBlockInfo: BlockInfo): Boolean {
        try {
            Timber.d("#syncCoins; current block info: $currentBlockInfo")

            val infoOfLastChecked = getInfoOnLastSuccessfulCheck()

            val now = System.currentTimeMillis()
            val timeInADay = 1000 * 60 * 60 * 24L

            if (infoOfLastChecked == null) {
                Timber.d("this is the very first check; full sync is required")
                //full sync from start
                fullSyncHelper.syncFromStart().let { success ->
                    if (!success) {
                        // sync failed
                        onSyncError(RuntimeException())
                        return false
                    }
                }
                //no need to check for a reorg
                //can proceed with coin-receive event observing

            } else if (infoOfLastChecked.height == currentBlockInfo.height &&
                infoOfLastChecked.hash == currentBlockInfo.hash
            ) {
                Timber.d("no blocks have advanced since the last check; everything is up to date")

            } else if (infoOfLastChecked.height + 1 == currentBlockInfo.height &&
                infoOfLastChecked.hash == currentBlockInfo.previousBlockHash
            ) {
                // blockchain has advanced by 1 block without a reorg
                Timber.d("blockchain has advanced by 1 block without a reorg")

                //sync only the upcoming addresses
                fullSyncHelper.syncFromCurrentState(true).let { success ->
                    if (!success) {
                        // sync failed
                        onSyncError(RuntimeException())
                        return false
                    }
                }

            } else {
                val infoOfHeight = blockchainDataSource.fetchBlockInfoForHeight(infoOfLastChecked.height)
                    ?: let {
                        // sync failed
                        onSyncError(RuntimeException())
                        return false
                    }

                if (infoOfHeight.hash != infoOfLastChecked.hash) {
                    // the block-info found in the last check is invalid because of a reorg
                    Timber.d("block-info of last sync does not match info of the same block on the chain; block-reorg detected")

                    //possibly also apply reorg on sent & received txs
                    try {
                        onReorgDetected(infoOfLastChecked.height)
                    } catch (e: Exception) {
                        // sync failed
                        onSyncError(e)
                        return false
                    }

                    fullSyncHelper.syncFromCurrentState(true).let { success ->
                        if (!success) {
                            // sync failed
                            onSyncError(RuntimeException())
                            return false
                        }
                    }

                } else {
                    //there was no reorg

                    val success = if (infoOfLastChecked.time + timeInADay < now) {
                        Timber.d("block-info indicates that the last sync was done more than a day ago")
                        fullSyncHelper.syncFromCurrentState(false)

                    } else {
                        Timber.d("block-info indicates that the last sync was done less than a day ago")
                        fullSyncHelper.syncFromCurrentState(true)
                    }

                    if (!success) {
                        // sync failed
                        onSyncError(RuntimeException())
                        return false
                    }

                }

            }

            return true
        } catch (e: Exception) {
            onSyncError(e)
            return false
        }
    }

    private suspend fun onReorgDetected(heightDetectedReorg: Int) {
        //iterate through the list finding the most recent valid sync point & matching block-height
        val recentSyncInfoList = appDatabase.blockSyncInfoDao().getRecentSyncInfoList(paymail)

        var validSyncInfo: BlockSyncInfo? = null

        for (info in recentSyncInfoList) {
            if (info.height >= heightDetectedReorg) continue

            //check if the hash of the height of current 'info' matches the hash specified by the indexer
            val infoOfHeight = blockchainDataSource.fetchBlockInfoForHeight(info.height)
                ?: throw RuntimeException()

            if (infoOfHeight.hash == info.hash) {
                validSyncInfo = info
                break
            }
        }

        if (validSyncInfo == null) {
            //clear coin-sync info so it can be restarted from beginning
            appDatabase.blockSyncInfoDao().clearForHeight(0, paymail)
            appDatabase.coinDao().deleteAll(paymail)
            return
        }

        //clear coin-sync info that is newer than the first valid sync-info
        appDatabase.blockSyncInfoDao()
            .clearForHeight(validSyncInfo.height + 1, paymail)
        appDatabase.coinDao()
            .deleteAllNewerThanHeight(paymail, validSyncInfo.height + 1)
    }

    private suspend fun getInfoOnLastSuccessfulCheck(): BlockSyncInfo? {
        return appDatabase.blockSyncInfoDao().getLatestInfo(paymail)
    }

    private fun onSyncError(e: Throwable) {
        Timber.e(e)

        releaseEverything()
        _status.value = ChainSyncStatus.ERROR
    }

    private fun onListenError(e: Throwable) {
        Timber.e(e)
        coroutineUtil.appScope.launch(Dispatchers.Main) {
            releaseEverything()

            _status.value = ChainSyncStatus.ERROR
        }
    }

    private suspend fun persistSuccessfulSync(info: BlockInfo) {
        val syncInfo = BlockSyncInfo(
            info.height,
            paymail,
            info.time,
            info.hash,
            info.merkleRoot,
            info.previousBlockHash
        )
        appDatabase.blockSyncInfoDao().insertInfo(syncInfo)
    }


    //////////////////////////


    private suspend fun startCheckingForNewBlocks(currentInfo: BlockInfo) {
        newBlockObservationJob?.cancel()
        newBlockObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            blockchainDataSource.execQueue.execute {
                newBlockDetector.initialize(currentInfo)
            }
            var current = currentInfo

            newBlockDetector.currentBlockState.collect {
                if (current.hash == it.hash) return@collect
                current = it

                //handle the new block
                Timber.d("new block detected; processing...")

                //start syncing of unspent coins
                val didSucceed = syncCoins(it)
                if (!didSucceed) {
                    Timber.d("syncCoins failed; returning")
                    return@collect
                }

            }
        }
    }
}