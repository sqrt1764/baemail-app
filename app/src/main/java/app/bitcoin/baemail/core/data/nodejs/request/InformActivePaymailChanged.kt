package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.JsonArray
import timber.log.Timber


/*

# expected param structure
[
    "21589@moneybutton.com",
    [
        "aaa", "bbb", "ccc", ..
    ],
    "m/44'/0'/0'/0/0/0"
]



# expected structure of the response data passed in the callback
[
    {
        "informed": true
    }
]

*/
class InformActivePaymailChanged(
    val paymail: String,
    val seed: List<String>,
    val paymailPkiPath: String,
    val callback: (Boolean)->Unit
) : NodeRuntimeInterface.NodeRequest("INFORM_ACTIVE_PAYMAIL_CHANGED") {


    override fun getReqParams(): JsonArray {
        val seedArray = JsonArray()
        seed.forEach {
            seedArray.add(it)
        }

        val array = JsonArray()
        array.add(paymail)
        array.add(seedArray)
        array.add(paymailPkiPath)

        return array
    }

    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "public key not received; token: $token")
            callback(false)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(stackTrace, "unexpected response format; token: $token")
            callback(false)
            return
        }

        try {
            callback(data[0].asJsonObject.get("informed").asBoolean)
        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")
            callback(false)
        }
    }

}