package app.bitcoin.baemail.core.data.util

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.annotation.Keep
import androidx.annotation.Nullable
import kotlinx.coroutines.*
import timber.log.Timber

import timber.log.Timber.DebugTree
import java.io.File
import java.io.FileWriter
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Exception

@Keep
class FileLoggingTree(
    private val context: Context,
    private val coroutineUtil: CoroutineUtil,
    private val daysOfLogsToKeep: Int
) : DebugTree() {

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        val now = Date()
        coroutineUtil.appScope.launch {
            withContext(coroutineUtil.fileLoggingDispatcher) {
                doLog(now, priority, tag, message, t)
            }
        }
    }

    override fun createStackElementTag(element: StackTraceElement): String? {
        // Add log statements line number to the log
        return super.createStackElementTag(element) + " - " + element.lineNumber
    }

    @SuppressLint("LogNotTimber")
    @Synchronized
    private fun doLog(now: Date, priority: Int, tag: String?, message: String, t: Throwable?) {
        try {
            val fileNameTimeStamp: String = SimpleDateFormat(
                "dd-MM-yyyy",
                Locale.getDefault()
            ).format(now)
            val logTimeStamp: String = SimpleDateFormat(
                "E MMM dd yyyy 'at' hh:mm:ss:SSS aaa",
                Locale.getDefault()
            ).format(now)
            val fileName = "$fileNameTimeStamp.html"

            // Create file
            val file: File = generateFile(context.cacheDir, fileName)

            // If file created or exists save logs
            val writer = FileWriter(file, true)
            writer.append("""
                <div class="log">
                    <div class="timestamp">
                        $logTimeStamp
                    </div>
                    <div class="tag">
                        $tag
                    </div>
                    <div class="message">
                        ${message.replace("\n", "<br>")}
                    </div>
                </div>
            """.trimIndent())
            writer.flush()
            writer.close()

        } catch (e: Exception) {
            Log.e(LOG_TAG, "Error while logging into file : $e", e)
        }
    }

    @SuppressLint("LogNotTimber")
    @Nullable
    private fun generateFile(parent: File, fileName: String): File {

        val logging = File(parent, "logging")
        var dirExists = true
        if (!logging.exists()) {
            dirExists = logging.mkdirs()
        }
        if (!dirExists) throw RuntimeException()

        val f = File(logging, fileName)
        if (!f.exists()) {
            //First start of the day
            val foundExistingLogs: Array<File> = logging.listFiles()!!

            //create a file for today's log-file
            f.createNewFile()

            val keepPeriod = 1000 * 60 * 60 * 24L * daysOfLogsToKeep
            val borderMillis = System.currentTimeMillis() - keepPeriod

            try {
                foundExistingLogs.forEach {
                    if (it.lastModified() < borderMillis) {
                        Log.d(
                            LOG_TAG,
                            "deleting old log-file; last-modified:${it.lastModified()} path:${it.absolutePath}"
                        )
                        it.delete()
                    }
                }
            } catch (e: Exception) {
                Log.d(
                    LOG_TAG,
                    "error when deleting old logs",
                    e
                )
            }

        }
        return f
    }

    companion object {
        private val LOG_TAG = FileLoggingTree::class.java.simpleName

        suspend fun deleteAllLogs(
            context: Context,
            coroutineUtil: CoroutineUtil
        ) = withContext(coroutineUtil.fileLoggingDispatcher) {
            val logging = File(context.cacheDir, "logging")
            logging.listFiles()!!.map {
                try {
                    it.delete()
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }
        }

    }
}