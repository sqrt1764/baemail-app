package app.bitcoin.baemail.core.presentation.drawable

import android.graphics.Outline
import android.graphics.Rect
import android.graphics.RectF
import android.view.View
import android.view.ViewOutlineProvider
import kotlin.math.min

class CircleOutlineProvider(
    private val paddedRect: RectF = RectF()
) : ViewOutlineProvider() {

    override fun getOutline(view: View, outline: Outline) {
        val cornerRadius = min(
            view.width - paddedRect.left - paddedRect.right,
            view.height - paddedRect.top - paddedRect.bottom
        ) / 2f

        outline.setRoundRect(
            Rect(
                paddedRect.left.toInt(),
                paddedRect.top.toInt(),
                view.width - paddedRect.right.toInt(),
                view.height - paddedRect.bottom.toInt()
            ), cornerRadius
        )
    }
}