package app.bitcoin.baemail.core.data

import app.bitcoin.baemail.core.data.util.AuthenticatedPaymail
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.data.wallet.AppStateLiveData
import app.bitcoin.baemail.core.domain.entities.ActivePaymail
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.stateIn
import timber.log.Timber

class AuthRepositoryImpl(
    private val coroutineUtil: CoroutineUtil,
    private val appStateLD: AppStateLiveData,
    private val secureDataSource: SecureDataSource
) : AuthRepository {

    override val addedPaymails: StateFlow<List<AuthenticatedPaymail>?> =
        secureDataSource.authenticatedPaymails

    override val activePaymail: StateFlow<ActivePaymail?> = appStateLD.state.mapNotNull { it }
        .combine(addedPaymails.mapNotNull { it }) { appState, addedPaymails ->
            if (addedPaymails.isEmpty()) {
                Timber.d("initializing with no active paymails")
                return@combine ActivePaymail(null)
            }

            val activePaymailId = appState.activePaymail ?: let {
                Timber.d("initializing; no paymails marked as active")
                return@combine ActivePaymail(null)
            }

            val matchingActivePaymail = addedPaymails.find {
                it.paymail == activePaymailId
            }

            if (matchingActivePaymail == null) {
                Timber.d("active paymail-id did not have a matching paymail entry; returning")
                return@combine ActivePaymail(null)
            }

            Timber.d("active paymail: ${matchingActivePaymail.paymail}")
            return@combine ActivePaymail(matchingActivePaymail.paymail)

        }.stateIn(
            scope = coroutineUtil.appScope,
            started = SharingStarted.Lazily,
            initialValue = null
        )

    override fun changeActivePaymail(paymail: String) {
        val addedPaymails = addedPaymails.value ?: return

        addedPaymails.map {
            it.paymail
        }.contains(paymail).let { containsPaymail ->
            if (!containsPaymail) return
        }

        appStateLD.setActivePaymail(paymail)
    }

    override suspend fun addPaymail(
        paymail: String,
        paymailPath: String,
        paymailMnemonic: List<String>,
        fundingPath: String,
        fundingMnemonic: List<String>
    ) {
        secureDataSource.addPaymail(paymail, paymailPath, paymailMnemonic,
            fundingPath, fundingMnemonic)
    }
}


