package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.local.p2p.serviceDiscovery.DiscoveryModel
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber


/*
# expected structure of params
[
    {
        isActive: false,
        peers: [
            {
                name: 'aaa',
                host: '10.0.2.16',
                port: 9999,
                title: 'aa',
                paymail: 'bb',
                pki: 'cc'
            }
        ]
    }
]


# expected structure of the response data passed in the callback
[
    true
]
 */
class InformDiscoveredStallsChanged(
    val model: DiscoveryModel,
    val callback: (Boolean?)->Unit
) : NodeRuntimeInterface.NodeRequest("INFORM_DISCOVERED_STALLS_CHANGED") {

    override fun getReqParams(): JsonArray {
        val obj  = JsonObject()
        obj.addProperty("isActive", model.isActive)

        val peerList = JsonArray()
        obj.add("peers", peerList)

        model.peers.values.forEach { foundStall ->
            val stall = JsonObject()
            stall.addProperty("name", foundStall.name)
            stall.addProperty("host", foundStall.host)
            stall.addProperty("port", foundStall.port)
            stall.addProperty("title", foundStall.title)
            stall.addProperty("paymail", foundStall.paymail)
            stall.addProperty("pki", foundStall.pki)

            peerList.add(stall)
        }

        val finalArray = JsonArray()
        finalArray.add(obj)

        return finalArray
    }

    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "discovered stall change has not been delivered; token: $token")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(stackTrace, "unexpected response format; token: $token")
            callback(null)
            return
        }

        try {
            callback(data[0].asBoolean)

        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")
            callback(null)
        }
    }
}