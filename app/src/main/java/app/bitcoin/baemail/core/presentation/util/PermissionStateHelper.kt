package app.bitcoin.baemail.core.presentation.util

import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class PermissionStateHelper {

    //todo
    //todo move caching for permission-checking to this class


    private val _playStateHolder = MutableStateFlow(
        PlayServicesStateHolder(
        mustCheckAvailability = true,
        availability = PlayServicesAvailability(ConnectionResult.UNKNOWN)
    )
    )
    val playStateHolder: StateFlow<PlayServicesStateHolder>
        get() = _playStateHolder



    fun onPlayServicesAvailabilityChanged(
        availability: PlayServicesAvailability
    ) {
        _playStateHolder.value = PlayServicesStateHolder(
            mustCheckAvailability = false,
            availability
        )
    }

    fun resetPlayServicesAvailabilityChecking() {
        _playStateHolder.value = PlayServicesStateHolder(
            mustCheckAvailability = true,
            PlayServicesAvailability(ConnectionResult.UNKNOWN)
        )
    }





    data class PlayServicesStateHolder(
        val mustCheckAvailability: Boolean,
        val availability: PlayServicesAvailability
    )

    data class PlayServicesAvailability(
        val statusCode: Int
    ) {
        fun isRecoverable(): Boolean {
            return GoogleApiAvailability.getInstance().isUserResolvableError(statusCode)
        }

        fun isAvailable(): Boolean {
            return statusCode == ConnectionResult.SUCCESS
        }
    }
}