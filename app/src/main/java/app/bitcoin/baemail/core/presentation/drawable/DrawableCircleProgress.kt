package app.bitcoin.baemail.core.presentation.drawable

import android.content.Context
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import timber.log.Timber

class DrawableCircleProgress(
    context: Context
) : CircularProgressDrawable(context) {

    var visibilityListener: (Boolean) -> Unit = {
        //Timber.d("visible: $it")
    }

    override fun setVisible(visible: Boolean, restart: Boolean): Boolean {
        visibilityListener(visible)
        return super.setVisible(visible, restart)
    }
}