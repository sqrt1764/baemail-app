package app.bitcoin.baemail.core.domain.usecases

class CheckIsValidPkiPathUseCaseImpl : CheckIsValidPkiPathUseCase {

    override fun invoke(path: String?, allowEndingWithSlash: Boolean): Boolean {
        path ?: return false

        if (!path.startsWith("m/")) return false
        if (path.length < 2) return false
        if (path.indexOf("m", 1) != -1) return false

        if (allowEndingWithSlash && path == "m/") return true

        path.replace(" ", "")
            .replace("m", "")
            .replace("/", "")
            .replace("'", "")
            .toCharArray().forEach { c ->
                if (!c.isDigit()) return false
            }

        val parts = path.split('/')

        for (i in 1 until parts.size) {
            val p = parts[i]
            val posOfPrime = p.indexOf('\'')
            if (posOfPrime != -1 && posOfPrime < p.length -1) return false
            if (allowEndingWithSlash && i + 1 == parts.size && p.isEmpty()) {
                continue
            }
            p.replace("\'", "").let { p0 ->
                try {
                    p0.toInt()
                } catch (e: Exception) {
                    return false
                }
            }
        }

        return true
    }

}

interface CheckIsValidPkiPathUseCase {
    operator fun invoke(path: String?, allowEndingWithSlash: Boolean = false): Boolean
}