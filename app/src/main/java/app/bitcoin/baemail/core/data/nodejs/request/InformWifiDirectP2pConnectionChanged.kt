package app.bitcoin.baemail.core.data.nodejs.request

import android.net.wifi.p2p.WifiP2pInfo
import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.local.p2p.serviceDiscovery.FoundStall
import app.local.p2p.serviceDiscovery.P2pStallRegistryHelper1
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonNull
import com.google.gson.JsonObject
import timber.log.Timber

/*
# expected structure of params
[
    (1),
    (2)
]


(1) possible values: 'stall-connection-attempt', 'p2p-group-change', 'p2p-group-participants'


(2) possible structures
when 'stall-connection-attempt':
{
    host: 'ABC0',
    paymail: 'aa@yy.zz',
    pki: '00aaff11',
    title: 'stall 01'
}

when 'p2p-group-change':
{
    formed: true,
    isGroupOwner: false,
    groupOwnerAddress: 'ip-address'
}

when 'p2p-group-participants':
[
    {
        peerName: 'abc',
        peerAddress: 'mac-address',
        status: 0
    }
]

status values:
    CONNECTED == 0
    INVITED == 1
    FAILED == 2
    AVAILABLE == 3
    UNAVAILABLE == 4



# expected structure of the response data passed in the callback
[
    true
]
 */
class InformWifiDirectP2pConnectionChanged(
    val connectionAttempt: FoundStall?,
    val groupInfo: WifiP2pInfo?,
    val participants: Collection<P2pStallRegistryHelper1.DeviceStatus>?,
    val callback: (Boolean?)->Unit
) : NodeRuntimeInterface.NodeRequest("INFORM_WIFI_DIRECT_P2P_CONNECTION_CHANGED") {


    private fun getStallJson(): JsonElement {
        if (connectionAttempt == null) return JsonNull.INSTANCE

        val stallJson = JsonObject()
        stallJson.addProperty("host", connectionAttempt.host)
        stallJson.addProperty("paymail", connectionAttempt.paymail)
        stallJson.addProperty("pki", connectionAttempt.pki)
        stallJson.addProperty("title", connectionAttempt.title)

        return stallJson
    }

    private fun getP2pGroupChange(): JsonElement {
        if (groupInfo == null) return JsonNull.INSTANCE

        val json = JsonObject()
        json.addProperty("formed", groupInfo.groupFormed)
        json.addProperty("isGroupOwner", groupInfo.isGroupOwner)
        groupInfo.groupOwnerAddress?.hostAddress?.let {
            json.addProperty("groupOwnerAddress", it)
        } ?: let {
            json.add("groupOwnerAddress", JsonNull.INSTANCE)
        }


        return json
    }

    private fun getP2pGroupParticipants(): JsonElement {
        if (participants == null) return JsonNull.INSTANCE

        val jsonArray = JsonArray()
        participants.forEach { peerStatus ->
            val json = JsonObject()
            json.addProperty("peerName", peerStatus.peerName)
            json.addProperty("peerAddress", peerStatus.peerAddress)
            json.addProperty("status", peerStatus.isConnected())

            jsonArray.add(json)
        }

        return jsonArray
    }

    override fun getReqParams(): JsonArray {
        val finalArray = JsonArray()

        when {
            connectionAttempt != null -> {
                finalArray.add("stall-connection-attempt")
                finalArray.add(getStallJson())

            }
            groupInfo != null -> {
                finalArray.add("p2p-group-change")
                finalArray.add(getP2pGroupChange())

            }
            participants != null -> {
                finalArray.add("p2p-group-participants")
                finalArray.add(getP2pGroupParticipants())

            }
            else -> throw RuntimeException()
        }

        return finalArray
    }

    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "failed to deliver wifi-direct-connection-change; token: $token")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(stackTrace, "unexpected response format; token: $token")
            callback(null)
            return
        }

        try {
            callback(data[0].asBoolean)

        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")
            callback(null)
        }
    }
}