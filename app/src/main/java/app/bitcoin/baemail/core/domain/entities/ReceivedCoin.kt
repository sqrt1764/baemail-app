package app.bitcoin.baemail.core.domain.entities

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ReceivedCoin(
    @SerializedName("tx_id") val txHash: String,
    @SerializedName("sats") val sats: Long,
    @SerializedName("index") val index: Int,
    @SerializedName("address") val address: String
)