package app.bitcoin.baemail.core.domain.repository

import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.data.nodejs.request.DerivedAddress
import app.bitcoin.baemail.core.data.room.entity.Coin
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface CoinRepository {
    val syncStatus: StateFlow<ChainSyncStatus>
    fun getCoin(address: String, txId: String, outputIndex: Int): Flow<Coin?>
    fun getFundingCoins(): Flow<List<Coin>>
    suspend fun getUpcomingUnusedAddresses(paymail: String, addressCount: Int): Flow<List<DerivedAddress>>
    suspend fun saveCoins(ownedCoins: List<Coin>)
    suspend fun markCoinsSpent(coins: List<Coin>)
    suspend fun resyncCoins()
    suspend fun refreshCoins()
    fun getHighestUsedAddressIndex(): Flow<Int>
}