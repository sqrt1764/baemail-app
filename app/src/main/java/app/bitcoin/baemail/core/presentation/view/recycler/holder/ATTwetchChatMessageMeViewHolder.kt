package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.Outline
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.text.util.LinkifyCompat
import androidx.core.view.updatePadding
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableRoundedBitmap
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchChatMessageMeItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import coil.load
import coil.target.ImageViewTarget
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlin.math.min

@FlowPreview
@ExperimentalCoroutinesApi
class ATTwetchChatMessageMeViewHolder(
        itemView: View
) : BaseViewHolder(itemView) {

    private lateinit var listener: BaseListAdapter.ATTwetchChatMessageMeListener

    private var model: ATTwetchChatMessageMeItem? = null

    private val contentContainer: LinearLayoutCompat = itemView.findViewById(R.id.content_container)
    private val image: ImageView = itemView.findViewById(R.id.image)
    private val icon: ImageView = itemView.findViewById(R.id.icon)
    private val contentBackground: View = itemView.findViewById(R.id.content_background)
    private val content: TextView = itemView.findViewById(R.id.content)
    private val messageTime: TextView = itemView.findViewById(R.id.message_time)


    private val imagePlaceholderDrawable = ColorDrawable(itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant))

    private val colorPrimary = itemView.context.getColorFromAttr(R.attr.colorPrimary)
    private val colorSurfaceVariant = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
    private val colorError = itemView.context.getColorFromAttr(R.attr.colorError)


    private val colorBackgroundDefault = colorSurfaceVariant

    private val colorBackgroundSending = ColorUtils.compositeColors(
        ColorUtils.setAlphaComponent(colorPrimary, 150),
        colorSurfaceVariant
    )

    private val colorBackgroundError = ColorUtils.compositeColors(
        ColorUtils.setAlphaComponent(colorPrimary, 150),
        colorError
    )

    private val drawableContentBackground = ColorDrawable(colorSurfaceVariant)

    private val dp = itemView.resources.displayMetrics.density

    private var backgroundOutlineExtraPaddingLeft = 0

    init {

        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(itemView.context, id)
        }

        content.movementMethod = LinkMovementMethod.getInstance()

        icon.clipToOutline = true
        icon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }

        }
        icon.foreground = DrawableState.getNew(colorSelector)

        contentBackground.clipToOutline = true
        contentBackground.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val dp = itemView.resources.displayMetrics.density * 12

                p1.setRoundRect(backgroundOutlineExtraPaddingLeft, 0, p0.width, p0.height, dp)
            }
        }

        contentBackground.background = LayerDrawable(arrayOf(
            drawableContentBackground,
            DrawableState.getNew(colorSelector)
        ))

        contentBackground.setOnClickListener {
            val m = model ?: return@setOnClickListener
//            listener.onClick(m.chat)
        }

        image.clipToOutline = true
        image.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val dp = itemView.resources.displayMetrics.density * 12

                p1.setRoundRect(0, 0, p0.width, p0.height, dp)
            }

        }
        image.foreground = DrawableState.getNew(colorSelector)



        content.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View?,
                left: Int,
                top: Int,
                right: Int,
                bottom: Int,
                oldLeft: Int,
                oldTop: Int,
                oldRight: Int,
                oldBottom: Int
            ) {
                backgroundOutlineExtraPaddingLeft = contentContainer.width - (right - left)
                contentBackground.invalidateOutline()
            }

        })

        icon.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onProfileClicked(m.participant)
        }

        image.setOnClickListener {
            val m = model ?: return@setOnClickListener
            m.image ?: return@setOnClickListener
            if (!m.imageFileFound) return@setOnClickListener
            listener.onImageClicked(m.image)
        }

        content.setOnClickListener {
            val m = model ?: return@setOnClickListener

            listener.onContentClicked(
                m.chatId,
                m.id,
                m.content,
                m.participant,
                m.dateLabel,
                m.millisCreatedAt
            )
        }


    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATTwetchChatMessageMeItem

        this.listener = listener!! as BaseListAdapter.ATTwetchChatMessageMeListener

        messageTime.text = model.dateLabel
        content.text = model.content


        LinkifyCompat.addLinks(content, Linkify.WEB_URLS)

        if (model.image == null) {
            image.visibility = View.GONE
        } else {
            image.visibility = View.VISIBLE

            if (!model.imageFileFound) {
                image.load(imagePlaceholderDrawable)
            } else {
                image.load(model.image) {
                    placeholder(imagePlaceholderDrawable)
                    error(imagePlaceholderDrawable)
                    target(object : ImageViewTarget(image) {
                        override fun onSuccess(result: Drawable) {
                            val d = if (result is BitmapDrawable) {
                                DrawableRoundedBitmap(itemView.context, 0f, result.bitmap)
                            } else {
                                result
                            }
                            super.onSuccess(d)
                        }
                    })
                }
            }


        }

        if (model.content.trim().isEmpty()) {
            contentContainer.visibility = View.GONE
        } else {
            contentContainer.visibility = View.VISIBLE
        }

        if (model.showDate) {
            messageTime.visibility = View.VISIBLE
            content.updatePadding(bottom = (4 * dp).toInt())
        } else {
            messageTime.visibility = View.GONE
            content.updatePadding(bottom = (11 * dp).toInt())
        }

        if (model.showIcon) {
            icon.visibility = View.VISIBLE
        } else {
            icon.visibility = View.INVISIBLE
        }


        if (model.showIcon) {
            model.participant.icon?.let {
                icon.load(it) {
                    placeholder(imagePlaceholderDrawable)
                    error(imagePlaceholderDrawable)
                    target(object : ImageViewTarget(icon) {
                        override fun onSuccess(result: Drawable) {
                            val d = if (result is BitmapDrawable) {
                                DrawableRoundedBitmap(itemView.context, 0f, result.bitmap)
                            } else {
                                result
                            }
                            super.onSuccess(d)
                        }
                    })
                }
            } ?: icon.load(imagePlaceholderDrawable)

        }

        when {
            model.inError -> {
                drawableContentBackground.color = colorBackgroundError
            }
            model.inSending -> {
                drawableContentBackground.color = colorBackgroundSending
            }
            else -> {
                drawableContentBackground.color = colorBackgroundDefault
            }
        }


    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTwetchChatMessageMeViewHolder {
            return ATTwetchChatMessageMeViewHolder(
                    inflater.inflate(
                            R.layout.li_twetch_message_me,
                            parent,
                            false
                    )
            )
        }
    }
}