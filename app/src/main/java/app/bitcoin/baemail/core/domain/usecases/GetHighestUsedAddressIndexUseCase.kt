package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.domain.repository.CoinRepository
import kotlinx.coroutines.flow.Flow

class GetHighestUsedAddressIndexUseCaseImpl(
    val coinRepository: CoinRepository
) : GetHighestUsedAddressIndexUseCase {
    override fun invoke(): Flow<Int> = coinRepository.getHighestUsedAddressIndex()

}

interface GetHighestUsedAddressIndexUseCase {
    operator fun invoke(): Flow<Int>
}