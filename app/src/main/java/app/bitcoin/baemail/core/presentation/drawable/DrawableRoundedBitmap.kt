package app.bitcoin.baemail.core.presentation.drawable

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import kotlin.math.max

class DrawableRoundedBitmap : Drawable {

    private var dp = 1f
    private var densityDpi = 160

    private val paint = Paint(
//        Paint.ANTI_ALIAS_FLAG
//                or Paint.FILTER_BITMAP_FLAG
//                or Paint.DITHER_FLAG
    )

    private var cornerRoundness = 10f
    private val bitmap: Bitmap

    private val rect = RectF()

    private var imageUrl: String? = null

    constructor(
        context: Context,
        roundnessDp: Float,
        bitmap: Bitmap,
        imageUrl: String? = null
    ) : this(context, roundnessDp, bitmap) {
        this.imageUrl = imageUrl
    }

    constructor(
        context: Context,
        roundnessDp: Float,
        bitmap: Bitmap
    ) : super() {
        dp = context.resources.displayMetrics.density
        densityDpi = context.resources.displayMetrics.densityDpi

        cornerRoundness = dp * roundnessDp
        this.bitmap = bitmap

        paint.shader = BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
    }

    override fun onBoundsChange(bounds: Rect) {
        if (bounds == null) return

        val xScale = bounds.width().toFloat() / bitmap.width
        val yScale = bounds.height().toFloat() / bitmap.height
        val scale = max(yScale, xScale)

        val xExtra = (scale * bitmap.width) - bounds.width()
        val yExtra = (scale * bitmap.height) - bounds.height()

        val scaleMatrix = Matrix()
        scaleMatrix.setScale(scale, scale)

        if (xExtra != 0f || yExtra != 0f) {
            val matrix = Matrix()
            val translateMatrix = Matrix()
            val xOffset: Float = if (xExtra < 1f) {
                0f
            } else {
                (-0.5f * xExtra)
            }

            val yOffset: Float = if (yExtra < 1f) {
                0f
            } else {
                (-0.5f * yExtra)
            }

            translateMatrix.setTranslate(xOffset, yOffset)

            matrix.setConcat(translateMatrix, scaleMatrix)

            paint.shader.setLocalMatrix(matrix)
        } else {
            paint.shader.setLocalMatrix(scaleMatrix)
        }
    }

    override fun draw(canvas: Canvas) {
        rect.set(bounds)

        val saveIndex = canvas.save()
        canvas.clipRect(bounds.left, bounds.top, bounds.right, bounds.bottom)
        if (cornerRoundness == 0f) {
            canvas.drawRect(rect, paint)
        } else {
            canvas.drawRoundRect(rect, cornerRoundness, cornerRoundness, paint)
        }
        canvas.restoreToCount(saveIndex)
    }

    override fun setAlpha(alpha: Int) {
        //not supported
    }

    override fun getOpacity(): Int {
        return if (cornerRoundness == 0f) {
            PixelFormat.OPAQUE
        } else {
            PixelFormat.TRANSPARENT
        }
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        //not supported
    }

    companion object {
        fun toBitmap(d: Drawable): Bitmap {
            val b = Bitmap.createBitmap(d.intrinsicWidth, d.intrinsicHeight,
                Bitmap.Config.ARGB_8888)
            val c = Canvas(b)
            d.setBounds(0, 0, c.width, c.height)
            d.draw(c)
            return b
        }
    }
}