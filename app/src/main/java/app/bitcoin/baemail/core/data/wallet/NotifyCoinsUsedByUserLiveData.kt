package app.bitcoin.baemail.core.data.wallet

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow

class NotifyCoinsUsedByUserLiveData : MutableLiveData<Long>()  {

    private val _state = MutableSharedFlow<Long>(1)
    val state = _state.asSharedFlow()

    init {
        _state.tryEmit(System.currentTimeMillis())
    }

    fun ping(paymail: String) {
        val millisHappened = System.currentTimeMillis()
        postValue(millisHappened)
    }

    override fun setValue(value: Long?) {
        super.setValue(value)
        value ?: return
        _state.tryEmit(value)
    }
}