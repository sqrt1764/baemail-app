package app.bitcoin.baemail.core.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.bitcoin.baemail.core.data.room.TABLE_CACHED_DERIVED_ADDRESS
import app.bitcoin.baemail.core.data.room.entity.CachedDerivedAddress

@Dao
abstract class CachedDerivedAddressDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun cacheThis(list: List<CachedDerivedAddress>)


    @Query("select * from $TABLE_CACHED_DERIVED_ADDRESS where paymail = :paymail and pathIndex >= :lower and pathIndex < :upperExcluding order by pathIndex asc")
    abstract suspend fun getCachedAddresses(paymail: String, lower: Int, upperExcluding: Int): List<CachedDerivedAddress>

}