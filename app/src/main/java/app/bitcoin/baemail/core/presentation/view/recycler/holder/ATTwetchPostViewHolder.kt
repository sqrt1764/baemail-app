package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.text.util.LinkifyCompat
import androidx.lifecycle.lifecycleScope
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableRoundedBitmap
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.drawable.DrawableTwetchReplyColumn
import app.bitcoin.baemail.twetch.ui.PostModel
import app.bitcoin.baemail.twetch.ui.TwetchFilesMimeTypeHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ViewTwetchTweetCard
import app.bitcoin.baemail.core.presentation.view.ViewVideoClip
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchPostItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.SpanType
import coil.load
import coil.size.Precision
import coil.target.ImageViewTarget
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.button.MaterialButton
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.textview.MaterialTextView
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.receiveAsFlow
import timber.log.Timber
import kotlin.math.min

@Suppress("UNUSED_PARAMETER")
@FlowPreview
@ExperimentalCoroutinesApi
class ATTwetchPostViewHolder(
    itemView: View,
    private val adapterFactory: BaseListAdapterFactory
) : BaseViewHolder(itemView) {

    private lateinit var listener: BaseListAdapter.ATTwetchPostListener

    private var model: ATTwetchPostItem? = null


    private val tweet: ViewTwetchTweetCard = itemView.findViewById(R.id.tweet)
    private val branchedLabel: MaterialTextView = itemView.findViewById(R.id.branched_label)
    private val replyingLabel: MaterialTextView = itemView.findViewById(R.id.replying_label)
    private val icon: ImageView = itemView.findViewById(R.id.icon)
    private val postImage: ImageView = itemView.findViewById(R.id.post_image)
    private val postVideo: ViewVideoClip = itemView.findViewById(R.id.post_video)
    private val postImageLabel: MaterialTextView = itemView.findViewById(R.id.post_image_label)
    private val created: MaterialTextView = itemView.findViewById(R.id.created)
    private val name: MaterialTextView = itemView.findViewById(R.id.name)
    private val userId: MaterialTextView = itemView.findViewById(R.id.user_id)
    private val content: MaterialTextView = itemView.findViewById(R.id.content)

    private val actionLike: MaterialButton = itemView.findViewById(R.id.action_like)
    private val actionComment: MaterialButton = itemView.findViewById(R.id.action_comment)
    private val actionBranch: MaterialButton = itemView.findViewById(R.id.action_branch)
    private val actionLink: MaterialButton = itemView.findViewById(R.id.action_link)


    private val animLike: LottieAnimationView = itemView.findViewById(R.id.anim_like)
    private val animBranch: LottieAnimationView = itemView.findViewById(R.id.anim_branch)






    private val quoteTwetchContainer: ConstraintLayout = itemView
        .findViewById(R.id.quote_twetch_container)
    private val quoteTweet: ViewTwetchTweetCard = itemView.findViewById(R.id.quote_tweet)
    private val quoteReplyingLabel: MaterialTextView = itemView.findViewById(R.id.quote_replying_label)
    private val quoteIcon: ImageView = itemView.findViewById(R.id.quote_icon)
    private val quotePostImage: ImageView = itemView.findViewById(R.id.quote_post_image)
    private val quotePostVideo: ViewVideoClip = itemView.findViewById(R.id.quote_post_video)
    private val quotePostImageLabel: MaterialTextView = itemView.findViewById(R.id.quote_post_image_label)
    private val quoteCreated: MaterialTextView = itemView.findViewById(R.id.quote_created)
    private val quoteName: MaterialTextView = itemView.findViewById(R.id.quote_name)
    private val quoteUserId: MaterialTextView = itemView.findViewById(R.id.quote_user_id)
    private val quoteContent: MaterialTextView = itemView.findViewById(R.id.quote_content)












    private val drawableNpc = ContextCompat.getDrawable(itemView.context, R.drawable.ic_npc3000)

    private val imagePlaceholderDrawable = ColorDrawable(
        itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
    )

    private val dp = itemView.resources.displayMetrics.density
    private val sp = itemView.resources.displayMetrics.scaledDensity





    private var strokeDrawable: DrawableStroke
    private val replyColumnDrawable = DrawableTwetchReplyColumn()

//    private val strokeColor = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
    private val colorPrimary = itemView.context.getColorFromAttr(R.attr.colorPrimary)
    private val colorReplyColumn: Int by lazy {
        val color = itemView.context.getColorFromAttr(R.attr.colorOnSurfaceSubtle)
        ColorUtils.setAlphaComponent(color, 88)
    }
    private val colorSelector = R.color.selector_on_primary.let { id ->
        ContextCompat.getColor(itemView.context, id)
    }


    private val drawableBranch: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_branch)!!

    private val drawableYouBranched: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_branch_filled)!!

    private val drawableLike: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_like)!!

    private val drawableYouLiked: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_like_filled)!!


    private val colorOnSurface = itemView.context.getColorFromAttr(R.attr.colorOnSurface)
    private val colorTwetchLike = ContextCompat.getColor(itemView.context, R.color.twetch_like)
    private val colorTwetchBranch = ContextCompat.getColor(itemView.context, R.color.twetch_branch)



    private var shouldDropNextContentClickHandlerCall = false
    private val onUnloadedPostClicked: (String) -> Unit = lambda@{ txId ->
        shouldDropNextContentClickHandlerCall = true


        val m = model ?: return@lambda

        val post = if (initializedReferencedPostAsMain) {
            val txId = m.holder.post.referencedPost
            m.holder.referenced[txId]!!
        } else {
            m.holder.post
        }

        listener.onUnloadedPostClicked(m.holder, post, txId)
    }

    private val colorOnPrimary = itemView.context.getColorFromAttr(R.attr.colorOnPrimary)

    init {

        tweet.setLifecycleOwner(this)
        quoteTweet.setLifecycleOwner(this)

        val colorTransparentSurface = ColorUtils.setAlphaComponent(
            itemView.context.getColorFromAttr(R.attr.colorSurface),
            123
        )

        val drawableBranch0 = ContextCompat.getDrawable(itemView.context, R.drawable.ic_branch)!!
        drawableBranch0.setTint(ContextCompat.getColor(itemView.context, R.color.twetch_branch))
        val branchAspectRatio = drawableBranch0.intrinsicHeight.toFloat() /
                drawableBranch0.intrinsicWidth
        drawableBranch0.setBounds(0, 0,
            (dp * 14f / branchAspectRatio).toInt(), (dp * 14f).toInt())
        branchedLabel.setCompoundDrawables(drawableBranch0, null, null, null)




        quotePostImageLabel.clipToOutline = true
        quotePostImageLabel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }
        quotePostImageLabel.background = ColorDrawable(colorTransparentSurface)

        postImageLabel.clipToOutline = true
        postImageLabel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }
        postImageLabel.background = ColorDrawable(colorTransparentSurface)








        postVideo.clipToOutline = true
        postVideo.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, 12 * dp)
            }
        }
        postVideo.foreground = DrawableState.getNew(colorSelector)



        postImage.clipToOutline = true
        postImage.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, 12 * dp)
            }
        }
        postImage.foreground = DrawableState.getNew(colorSelector)








        quotePostVideo.clipToOutline = true
        quotePostVideo.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, 12 * dp)
            }
        }
        quotePostVideo.foreground = DrawableState.getNew(colorSelector)



        quotePostImage.clipToOutline = true
        quotePostImage.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, 12 * dp)
            }
        }
        quotePostImage.foreground = DrawableState.getNew(colorSelector)





        content.movementMethod = LinkMovementMethod.getInstance()
        quoteContent.movementMethod = LinkMovementMethod.getInstance()

        icon.clipToOutline = true
        icon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }
        icon.foreground = DrawableState.getNew(colorSelector)



        val strokePaddingSides = (dp * 16).toInt()

        strokeDrawable = DrawableStroke().also {
            it.setStrokeColor(ColorUtils.setAlphaComponent(colorPrimary, 70))
            it.setStrokeWidths(0, 0, 0, (1.0f * dp).toInt())
            //it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
        }

        replyColumnDrawable.setColumnWidth((6 * dp).toInt())
        replyColumnDrawable.setColumnPaddingLeft((36.5 * dp).toInt())
        replyColumnDrawable.setColumnColor(colorReplyColumn)

        itemView.background = LayerDrawable(arrayOf(
            strokeDrawable,
            replyColumnDrawable
        ))










        val shape = ShapeAppearanceModel.Builder()
            .setAllCornerSizes(16 * dp)
            .build()
        val backgroundDrawable = MaterialShapeDrawable(shape)
        backgroundDrawable.strokeWidth = 1.5f * dp
        backgroundDrawable.fillColor = ColorStateList.valueOf(Color.TRANSPARENT)
        backgroundDrawable.strokeColor = ColorStateList
            .valueOf(ColorUtils.setAlphaComponent(colorPrimary, 180))
        quoteTwetchContainer.background = backgroundDrawable



        quoteIcon.clipToOutline = true
        quoteIcon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }
        quoteIcon.foreground = DrawableState.getNew(colorSelector)



        postImage.addOnLayoutChangeListener { _, left, _, right, _, _, _, _, _ ->
            adapterFactory.intCache[CACHE_KEY_POST_IMAGE_WIDTH] = right - left
        }

        quotePostImage.addOnLayoutChangeListener { _, left, _, right, _, _, _, _, _ ->
            adapterFactory.intCache[CACHE_KEY_QUOTE_POST_IMAGE_WIDTH] = right - left
        }

        tweet.addOnLayoutChangeListener { _, left, _, right, _, _, _, _, _ ->
            adapterFactory.intCache[CACHE_KEY_TWEET_IMAGE_WIDTH] = right - left
        }

        quoteTweet.addOnLayoutChangeListener { _, left, _, right, _, _, _, _, _ ->
            adapterFactory.intCache[CACHE_KEY_QUOTE_TWEET_IMAGE_WIDTH] = right - left
        }



        quoteTwetchContainer.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]!!
                }

                m.holder.referenced[refPost!!.referencedPost!!]!!
            } else {
                m.holder.post.referencedPost!!.let { txId ->
                    m.holder.referenced[txId]!!
                }
            }

            listener.onContentClicked(m.holder, post)
        }

        quoteTweet.mediaClickListener = lambda@{
            val m = model ?: return@lambda

            val post = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]!!
                }

                m.holder.referenced[refPost!!.referencedPost!!]!!
            } else {
                m.holder.post.referencedPost!!.let { txId ->
                    m.holder.referenced[txId]!!
                }
            }

            val urlList = post.tweet!!.media
            listener.onImageClicked(m.holder, urlList)
        }

        quotePostImage.setOnClickListener {
            val m = model ?: return@setOnClickListener


            val post = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]!!
                }

                m.holder.referenced[refPost!!.referencedPost!!]!!
            } else {
                m.holder.post.referencedPost!!.let { txId ->
                    m.holder.referenced[txId]!!
                }
            }

            val list = post.getFullImageUrlList()
            listener.onImageClicked(m.holder, list)
        }

        quotePostVideo.setOnClickListener {
            val m = model ?: return@setOnClickListener
            val txId = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]!!
                }

                m.holder.referenced[refPost!!.referencedPost!!]!!.let { doubleRefPost ->
                    doubleRefPost.getVideoFileTxId()!!
                }
            } else {
                m.holder.post.referencedPost!!.let { txId ->
                    m.holder.referenced[txId]!!.getVideoFileTxId()!!
                }
            }

            listener.onVideoClipClicked(m.holder, txId)
        }


        quoteTweet.clickListener = lambda@{
            val m = model ?: return@lambda

            val post = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]!!
                }

                m.holder.referenced[refPost!!.referencedPost!!]!!
            } else {
                m.holder.post.referencedPost!!.let { txId ->
                    m.holder.referenced[txId]!!
                }
            }

            listener.onTweetClicked(m.holder, post)
        }

        quoteContent.setOnClickListener {
            if (shouldDropNextContentClickHandlerCall) {
                shouldDropNextContentClickHandlerCall = false
                return@setOnClickListener
            }

            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]!!
                }

                m.holder.referenced[refPost!!.referencedPost!!]!!
            } else {
                m.holder.post.referencedPost!!.let { txId ->
                    m.holder.referenced[txId]!!
                }
            }

            listener.onContentClicked(m.holder, post)
        }

        quoteCreated.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]!!
                }

                m.holder.referenced[refPost!!.referencedPost!!]!!
            } else {
                m.holder.post.referencedPost!!.let { txId ->
                    m.holder.referenced[txId]!!
                }
            }

            listener.onCreatedLabelClicked(m.holder, post)
        }

        quoteIcon.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]!!
                }

                m.holder.referenced[refPost!!.referencedPost!!]!!
            } else {
                m.holder.post.referencedPost!!.let { txId ->
                    m.holder.referenced[txId]!!
                }
            }

            if (post.isRemoved) return@setOnClickListener

            listener.onProfileClicked(m.holder, post)
        }

        quoteName.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]!!
                }

                m.holder.referenced[refPost!!.referencedPost!!]!!
            } else {
                m.holder.post.referencedPost!!.let { txId ->
                    m.holder.referenced[txId]!!
                }
            }

            if (post.isRemoved) return@setOnClickListener

            listener.onProfileClicked(m.holder, post)
        }







        tweet.mediaClickListener = lambda@{
            val m = model ?: return@lambda
            val urlList = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]
                }

                refPost!!.tweet!!.media
            } else {
                m.holder.post.tweet!!.media
            }
            listener.onImageClicked(m.holder, urlList)
        }

        postImage.setOnClickListener {
            val m = model ?: return@setOnClickListener
            val list = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]
                }

                refPost!!.getFullImageUrlList()
            } else {
                m.holder.post.getFullImageUrlList()
            }
            listener.onImageClicked(m.holder, list)
        }

        postVideo.setOnClickListener {
            val m = model ?: return@setOnClickListener
            val txId = if (initializedReferencedPostAsMain) {
                val refPost = m.holder.post.referencedPost?.let { txId ->
                    m.holder.referenced[txId]
                }

                refPost!!.getVideoFileTxId()!!
            } else {
                m.holder.post.getVideoFileTxId()!!
            }

            listener.onVideoClipClicked(m.holder, txId)
        }

        tweet.clickListener = lambda@{
            val m = model ?: return@lambda

            val post = if (initializedReferencedPostAsMain) {
                val txId = m.holder.post.referencedPost
                m.holder.referenced[txId]!!
            } else {
                m.holder.post
            }

            listener.onTweetClicked(m.holder, post)
        }

        itemView.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val txId = m.holder.post.referencedPost
                m.holder.referenced[txId]!!
            } else {
                m.holder.post
            }

            listener.onContentClicked(m.holder, post)
        }

        content.setOnClickListener {
            if (shouldDropNextContentClickHandlerCall) {
                shouldDropNextContentClickHandlerCall = false
                return@setOnClickListener
            }

            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val txId = m.holder.post.referencedPost
                m.holder.referenced[txId]!!
            } else {
                m.holder.post
            }

            listener.onContentClicked(m.holder, post)
        }

        created.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val txId = m.holder.post.referencedPost
                m.holder.referenced[txId]!!
            } else {
                m.holder.post
            }

            listener.onCreatedLabelClicked(m.holder, post)
        }

        branchedLabel.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onBranchedLabelClicked(m.holder)
        }

        icon.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val txId = m.holder.post.referencedPost
                m.holder.referenced[txId]!!
            } else {
                m.holder.post
            }

            if (post.isRemoved) return@setOnClickListener

            listener.onProfileClicked(m.holder, post)
        }

        name.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val txId = m.holder.post.referencedPost
                m.holder.referenced[txId]!!
            } else {
                m.holder.post
            }

            if (post.isRemoved) return@setOnClickListener

            listener.onProfileClicked(m.holder, post)
        }




        actionLike.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val txId = m.holder.post.referencedPost
                m.holder.referenced[txId]!!
            } else {
                m.holder.post
            }

            val proceeding = listener.onLikeClicked(m.holder, post)
            if (proceeding) {
                //model modified
                post.onLikedByMe()

                //force refresh of views
                onBindViewHolder(m, listener)
            }

        }
        actionLike.bringToFront()

        actionComment.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val txId = m.holder.post.referencedPost
                m.holder.referenced[txId]!!
            } else {
                m.holder.post
            }

            listener.onCommentClicked(m.holder, post)
        }

        actionBranch.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val txId = m.holder.post.referencedPost
                m.holder.referenced[txId]!!
            } else {
                m.holder.post
            }

            listener.onBranchClicked(m.holder, post)
        }
        actionBranch.bringToFront()

        actionLink.setOnClickListener {
            val m = model ?: return@setOnClickListener

            val post = if (initializedReferencedPostAsMain) {
                val txId = m.holder.post.referencedPost
                m.holder.referenced[txId]!!
            } else {
                m.holder.post
            }

            listener.onCopyLinkClicked(m.holder, post)
        }

    }




    private var initializedReferencedPostAsMain = false


    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATTwetchPostItem

        this.listener = listener!! as BaseListAdapter.ATTwetchPostListener


//       Timber.d("${Integer.toHexString(System.identityHashCode(this@ATTwetchPostViewHolder))} onBindViewHolder post:${model.holder.post.data}")


        val refPost = model.holder.post.referencedPost?.let { txId ->
            model.holder.referenced[txId]
        }

        refPost?.let { referencedPost ->
            //branch

            if (model.holder.post.isBranchWithComment) {
                initializedReferencedPostAsMain = false

                branchedLabel.visibility = View.GONE

                initPostBase(model.holder.post, model.labelCreatedAtOfPost)
                initTweet(model.holder.post)
                initPostImage(model.holder.post)
                initReplyingLabel(model.holder.post)


                quoteTwetchContainer.visibility = View.VISIBLE

                initQuotePostBase(referencedPost, model.labelCreatedAtOfReferenced!!)
                initQuoteTweet(referencedPost)
                initQuotePostImage(referencedPost)
                initQuoteReplyingLabel(referencedPost)

            } else {
                initializedReferencedPostAsMain = true

                branchedLabel.visibility = View.VISIBLE
                branchedLabel.text = itemView.context.getString(
                    R.string.twetch_branched_label,
                    model.holder.post.user.name,
                    model.holder.post.user.id
                )

                initPostBase(referencedPost, model.labelCreatedAtOfReferenced!!)
                initTweet(referencedPost)
                initReplyingLabel(referencedPost)
                initPostImage(referencedPost)


                val doubleRefPost = referencedPost.referencedPost?.let { txId ->
                    val p = model.holder.referenced[txId]
                    p
                }

                if (doubleRefPost == null) {
                    quoteTwetchContainer.visibility = View.GONE
                } else {
                    quoteTwetchContainer.visibility = View.VISIBLE

                    initQuotePostBase(doubleRefPost, model.labelCreatedAtOfDoubleReferenced!!)
                    initQuoteTweet(doubleRefPost)
                    initQuotePostImage(doubleRefPost)
                    initQuoteReplyingLabel(doubleRefPost)
                }



            }

        } ?: let {
            //not a branch
            initializedReferencedPostAsMain = false

            quoteTwetchContainer.visibility = View.GONE
            branchedLabel.visibility = View.GONE

            initPostBase(model.holder.post, model.labelCreatedAtOfPost)
            initTweet(model.holder.post)
            initReplyingLabel(model.holder.post)
            initPostImage(model.holder.post)

        }








        //init item-decors

        if (!model.isParentOfPostDetails && !model.isReplyOfPostDetails) {
            //post from a list & not post-details
            strokeDrawable.alpha = 255
            replyColumnDrawable.alpha = 0

        } else {
            //post from post-details

            if (model.isReplyOfPostDetails) {
                strokeDrawable.alpha = 255
                replyColumnDrawable.alpha = 0

            } else if (model.isParentOfPostDetails) {
                strokeDrawable.alpha = 0
                replyColumnDrawable.alpha = 255

                replyColumnDrawable.setColumnHeightLimitation(
                    false,
                    0
                )

                if (model.isFirstOfParentsOfPostDetails) {
                    val pxDistance = 12 * dp + //accounts for gone-margin of user-icon
                            5 * dp //a little extra distance behind the icon
                    replyColumnDrawable.setColumnPaddingTop(pxDistance.toInt())

                } else {
                    replyColumnDrawable.setColumnPaddingTop(0)
                }



            }


        }






        spanClickHandlerJob?.cancel()
        spanClickHandlerJob = lifecycleScope.launchWhenStarted {
            model.spanClicks.receiveAsFlow().collect { (type, value) ->
                Timber.d("got type:$type; value:$value")
                shouldDropNextContentClickHandlerCall = true

                //process
                if (SpanType.USER_ID == type) {
                    this@ATTwetchPostViewHolder.listener.onUserIdBadgeClicked(model.holder, value)

                } else if (SpanType.REF_POST == type) {
                    onUnloadedPostClicked(value)

                }
            }
        }


        coinFlipChannelJob?.cancel()
        coinFlipChannelJob = lifecycleScope.launchWhenStarted {
            this@ATTwetchPostViewHolder.listener.getCoinFlipSuccessSignal().collectLatest { event ->
                event ?: return@collectLatest
                val peeking = event.peekContent()
                val m = model

                val postTxId = if (initializedReferencedPostAsMain) {
                    m.holder.post.referencedPost
                } else {
                    m.holder.post.txId
                }
                if (peeking.postTxId != postTxId) return@collectLatest

                val parentId = if (m.holder.post.txId == postTxId) null else m.holder.post.txId
                if (parentId != peeking.parentId) return@collectLatest

                val coinFlip = event.getContentIfNotHandled() ?: return@collectLatest

                if (coinFlip.flipBranch && coinFlip.flipLike) {
                    //delay(400)
                    animLike.playAnimation()
                    delay(200)
                    animBranch.playAnimation()

                } else if (coinFlip.flipLike) {
                    animLike.playAnimation()

                } else if (coinFlip.flipBranch) {
                    //delay(400)
                    animBranch.playAnimation()
                }
            }
        }

    }

    //todo
    private var coinFlipChannelJob: Job? = null

    private var spanClickHandlerJob: Job? = null
    private var postFilesMimeRefreshedOfMainJob: Job? = null
    private var postFilesMimeRefreshedOfQuoteJob: Job? = null



    private fun initPostBase(post: PostModel, createdAtLabel: String) {

        if (post.isRemoved) {
            name.text = itemView.context.getString(R.string.twetch_npc_3000)
            userId.text = ""

        } else {
            name.text = post.user.name
            userId.text = itemView.resources.getString(R.string.twetch_user_id, post.user.id)
        }

        created.text = createdAtLabel

        actionLike.text = post.numLikes.toString()
        actionComment.text = post.numComments.toString()
        actionBranch.text = post.numBranches.toString()

        if (post.youLiked != 0) {
            actionLike.icon = drawableYouLiked
            actionLike.iconTint = ColorStateList.valueOf(colorTwetchLike)
        } else {
            actionLike.icon = drawableLike
            actionLike.iconTint = ColorStateList.valueOf(colorOnSurface)
        }

        if (post.youBranched != 0) {
            actionBranch.icon = drawableYouBranched
            actionBranch.iconTint = ColorStateList.valueOf(colorTwetchBranch)
        } else {
            actionBranch.icon = drawableBranch
            actionBranch.iconTint = ColorStateList.valueOf(colorOnSurface)
        }


        model!!.contentMap[post.txId]?.let {
            content.setPrecomputedText(it)
        } ?: let {
            content.setText(SpannableStringBuilder(post.content), TextView.BufferType.SPANNABLE)
        }

        if (post.isRemoved) {
            content.visibility = View.VISIBLE

        } else if (post.content.isBlank()) {
            content.visibility = View.GONE

        } else {
            LinkifyCompat.addLinks(content, Linkify.WEB_URLS)
            content.visibility = View.VISIBLE
        }


        if (post.isRemoved) {
            icon.load(drawableNpc)
        } else {
            post.user.icon?.let {
                icon.load(it) {
                    placeholder(imagePlaceholderDrawable)
                    error(imagePlaceholderDrawable)
                    precision(Precision.INEXACT)
                    target(object : ImageViewTarget(icon) {
                        override fun onSuccess(result: Drawable) {
                            val d = if (result is BitmapDrawable) {
                                DrawableRoundedBitmap(itemView.context, 0f, result.bitmap)
                            } else {
                                result
                            }
                            super.onSuccess(d)
                        }
                    })
                }
            } ?: let {
                icon.load(imagePlaceholderDrawable)
            }
        }

    }

    private fun initQuotePostBase(post: PostModel, createdAtLabel: String) {
        if (post.isRemoved) {
            quoteContent.visibility = View.VISIBLE

        } else if (post.content.isBlank()) {
            quoteContent.visibility = View.GONE

        } else {
            quoteContent.visibility = View.VISIBLE
        }
        //quoteContent.text = post.content
        model!!.contentMap[post.txId]?.let {
            quoteContent.setPrecomputedText(it)
        } ?: let {
            quoteContent.setText(SpannableStringBuilder(post.content), TextView.BufferType.SPANNABLE)
        }


        if (post.isRemoved) {
            quoteName.text = itemView.context.getString(R.string.twetch_npc_3000)
            quoteUserId.text = ""

        } else {
            quoteName.text = post.user.name
            quoteUserId.text = itemView.resources.getString(R.string.twetch_user_id, post.user.id)
        }

        quoteCreated.text = createdAtLabel


        if (post.isRemoved) {
            quoteIcon.load(drawableNpc)
        } else {
            post.user.icon?.let {
                quoteIcon.load(it) {
                    placeholder(imagePlaceholderDrawable)
                    error(imagePlaceholderDrawable)
                    precision(Precision.INEXACT)
                    target(object : ImageViewTarget(quoteIcon) {
                        override fun onSuccess(result: Drawable) {
                            val d = if (result is BitmapDrawable) {
                                DrawableRoundedBitmap(itemView.context, 0f, result.bitmap)
                            } else {
                                result
                            }
                            super.onSuccess(d)
                        }
                    })
                }
            } ?: let {
                quoteIcon.load(imagePlaceholderDrawable)
            }
        }
    }

    private fun initTweet(post: PostModel) {
        if (post.isRemoved) {
            tweet.visibility = View.GONE
            return
        }

        post.tweet?.let {
            if (it.media.isNotEmpty()) {
                adapterFactory.imageAspectRatioCache.applyMinHeightFromCachedRatio(
                    tweet.mediaImage,
                    it.media[0],
                    adapterFactory.intCache[CACHE_KEY_TWEET_IMAGE_WIDTH]
                )
            }

            tweet.applyModel(it) { d ->
                adapterFactory.imageAspectRatioCache.cacheRatio(d, it.media[0])
            }
            tweet.visibility = View.VISIBLE
        } ?: let {
            tweet.visibility = View.GONE
        }
    }

    private fun initQuoteTweet(post: PostModel) {
        if (post.isRemoved) {
            quoteTweet.visibility = View.GONE
            return
        }

        post.tweet?.let {
            if (it.media.isNotEmpty()) {
                adapterFactory.imageAspectRatioCache.applyMinHeightFromCachedRatio(
                    quoteTweet.mediaImage,
                    it.media[0],
                    adapterFactory.intCache[CACHE_KEY_QUOTE_TWEET_IMAGE_WIDTH]
                )
            }

            quoteTweet.applyModel(it) { d ->
                adapterFactory.imageAspectRatioCache.cacheRatio(d, it.media[0])
            }
            quoteTweet.visibility = View.VISIBLE
        } ?: let {
            quoteTweet.visibility = View.GONE
        }
    }


    private fun initReplyingLabel(post: PostModel) {
        if (post.parents.isNotEmpty()) {
            val list = post.parents
            val namesList = list
                .map { it.user.name }
                .asReversed()
                .fold(mutableListOf<String>()) { acc, item ->
                    if (acc.contains(item)) {
                        return@fold acc
                    } else {
                        acc.add(item)
                        return@fold acc
                    }
                }
            replyingLabel.visibility = View.VISIBLE
            val sb = StringBuilder(itemView.resources.getString(R.string.replying_to))
            val maxIndex = min(namesList.size, 2)
            for (i in 0 until maxIndex) {
                sb.append(" @").append(namesList[i])
            }
            if (namesList.size > maxIndex) {
                sb.append(" ...")
            }
            replyingLabel.text = sb.toString()
        } else {
            replyingLabel.visibility = View.GONE
        }
    }



    private fun initQuoteReplyingLabel(post: PostModel) {
        if (post.parents.isNotEmpty()) {
            val list = post.parents
            val namesList = list
                .map { it.user.name }
                .asReversed()
                .fold(mutableListOf<String>()) { acc, item ->
                    if (acc.contains(item)) {
                        return@fold acc
                    } else {
                        acc.add(item)
                        return@fold acc
                    }
                }
            quoteReplyingLabel.visibility = View.VISIBLE
            val sb = StringBuilder(itemView.resources.getString(R.string.replying_to))
            val maxIndex = min(namesList.size, 2)
            for (i in 0 until maxIndex) {
                sb.append(" @").append(namesList[i])
            }
            if (namesList.size > maxIndex) {
                sb.append(" ...")
            }
            quoteReplyingLabel.text = sb.toString()
        } else {
            quoteReplyingLabel.visibility = View.GONE
        }
    }


    private var lastPostImageUrl: String? = null

    private fun initPostImage(post: PostModel) {
        if (post.isRemoved) {
            lastPostImageUrl = null

            postImage.visibility = View.GONE
            postImageLabel.visibility = View.GONE

            postFilesMimeRefreshedOfMainJob?.cancel()
            postVideo.visibility = View.GONE

            return
        }

        post.getFullImageUrlList().let { list ->
            if (list.isEmpty()) {
                lastPostImageUrl = null

                postImage.visibility = View.GONE
                postImageLabel.visibility = View.GONE
            } else {
                if (lastPostImageUrl == list[0]) return@let
                lastPostImageUrl = list[0]

                postImage.visibility = View.VISIBLE

                adapterFactory.imageAspectRatioCache.applyMinHeightFromCachedRatio(
                    postImage,
                    list[0],
                    adapterFactory.intCache[CACHE_KEY_POST_IMAGE_WIDTH]
                )

                postImage.load(list[0]) {
                    placeholder(imagePlaceholderDrawable)
                    error(imagePlaceholderDrawable)
                    precision(Precision.INEXACT)
                    target(object : ImageViewTarget(postImage) {
                        override fun onSuccess(result: Drawable) {
                            adapterFactory.imageAspectRatioCache.cacheRatio(result, list[0])
                            super.onSuccess(result)
                        }
                        //                        override fun onSuccess(result: Drawable) {
//                            val d = if (result is BitmapDrawable) {
//                                DrawableRoundedBitmap(itemView.context, 0f, result.bitmap)
//                            } else {
//                                result
//                            }
//                            super.onSuccess(d)
//                        }
                    })
                }

                if (list.size > 1) {
                    postImageLabel.visibility = View.VISIBLE
                    postImageLabel.text = itemView.context.getString(
                        R.string.one_of_label,
                        list.size
                    )
                } else {
                    postImageLabel.visibility = View.GONE
                }
            }
        }





        postFilesMimeRefreshedOfMainJob?.cancel()
        postFilesMimeRefreshedOfMainJob = null
        post.getVideoFileTxId()?.let { txId ->

            var isVideoViewSetup = false

            val itemHolder = adapterFactory.twetchFilesMimeHelper.ensureMemoryCacheItem(txId)
            postFilesMimeRefreshedOfMainJob = lifecycleScope.launchWhenStarted {
                itemHolder.state.collectLatest { mimeState ->
                    if (TwetchFilesMimeTypeHelper.State.VIDEO == mimeState) {

                        if (!isVideoViewSetup) {
                            isVideoViewSetup = true

                            Timber.d("`postVideo` setup for tx: $txId")

                            postVideo.setup(
                                    this@ATTwetchPostViewHolder,
                                    listener.getVideoClipManager().requestClip(post.getVideoFileUrl()!!)
                            )
                        }

                        Timber.d("video showing; tx: $txId")
                        postVideo.visibility = View.VISIBLE
                    } else {
//                        Timber.d("video hidden; tx: $txId")
                        postVideo.visibility = View.INVISIBLE
                    }
                }
            }

        } ?: let {
            postVideo.visibility = View.GONE
        }

    }


    private var lastQuotePostImageUrl: String? = null

    private fun initQuotePostImage(post: PostModel) {
        if (post.isRemoved) {
            lastQuotePostImageUrl = null

            quotePostImage.visibility = View.GONE
            quotePostImageLabel.visibility = View.GONE

            postFilesMimeRefreshedOfQuoteJob?.cancel()
            quotePostVideo.visibility = View.GONE

            return
        }


        post.getFullImageUrlList().let { list ->
            if (list.isEmpty()) {
                lastQuotePostImageUrl = null

                quotePostImage.visibility = View.GONE
                quotePostImageLabel.visibility = View.GONE
            } else {
                if (lastQuotePostImageUrl == list[0]) return@let
                lastQuotePostImageUrl = list[0]

                quotePostImage.visibility = View.VISIBLE

                adapterFactory.imageAspectRatioCache.applyMinHeightFromCachedRatio(
                    quotePostImage,
                    list[0],
                    adapterFactory.intCache[CACHE_KEY_QUOTE_POST_IMAGE_WIDTH]
                )

                quotePostImage.load(list[0]) {
                    placeholder(imagePlaceholderDrawable)
                    error(imagePlaceholderDrawable)
                    precision(Precision.INEXACT)
                    target(object : ImageViewTarget(quotePostImage) {
                        override fun onSuccess(result: Drawable) {
                            adapterFactory.imageAspectRatioCache.cacheRatio(result, list[0])
                            super.onSuccess(result)
                        }
                        //                        override fun onSuccess(result: Drawable) {
//                            val d = if (result is BitmapDrawable) {
//                                DrawableRoundedBitmap(itemView.context, 0f, result.bitmap)
//                            } else {
//                                result
//                            }
//                            super.onSuccess(d)
//                        }
                    })
                }

                if (list.size > 1) {
                    quotePostImageLabel.visibility = View.VISIBLE
                    quotePostImageLabel.text = itemView.context.getString(
                        R.string.one_of_label,
                        list.size
                    )
                } else {
                    quotePostImageLabel.visibility = View.GONE
                }
            }
        }


        postFilesMimeRefreshedOfQuoteJob?.cancel()
        postFilesMimeRefreshedOfQuoteJob = null
        post.getVideoFileTxId()?.let { txId ->

            var isVideoViewSetup = false

            val itemHolder = adapterFactory.twetchFilesMimeHelper.ensureMemoryCacheItem(txId)
            postFilesMimeRefreshedOfQuoteJob = lifecycleScope.launchWhenStarted {
                itemHolder.state.collectLatest { mimeState ->
                    if (TwetchFilesMimeTypeHelper.State.VIDEO == mimeState) {

                        if (!isVideoViewSetup) {
                            isVideoViewSetup = true
                            Timber.d("`quotePostVideo` setup for tx: $txId")

                            quotePostVideo.setup(
                                    this@ATTwetchPostViewHolder,
                                    listener.getVideoClipManager().requestClip(post.getVideoFileUrl()!!)
                            )
                        }

                        Timber.d("quote video showing; tx: $txId")
                        quotePostVideo.visibility = View.VISIBLE
                    } else {
//                        Timber.d("quote video hidden; tx: $txId")
                        quotePostVideo.visibility = View.INVISIBLE
                    }
                }
            }

        } ?: let {
            quotePostVideo.visibility = View.GONE
        }
    }







    companion object {

        const val LOG = false

        private const val CACHE_KEY_POST_IMAGE_WIDTH = "twetch_post__post_image_width"
        private const val CACHE_KEY_QUOTE_POST_IMAGE_WIDTH = "twetch_post__quote_post_image_width"

        private const val CACHE_KEY_TWEET_IMAGE_WIDTH = "twetch_post__tweet_image_width"
        private const val CACHE_KEY_QUOTE_TWEET_IMAGE_WIDTH = "twetch_post__quote_tweet_image_width"


        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTwetchPostViewHolder {
            return ATTwetchPostViewHolder(
                inflater.inflate(
                    R.layout.li_twetch_post,
                    parent,
                    false
                ),
                factory
            )
        }
    }
}