package app.bitcoin.baemail.core.presentation.util

import android.text.InputFilter
import android.text.SpannableStringBuilder
import android.text.Spanned
import app.bitcoin.baemail.core.domain.usecases.CheckIsValidPkiPathUseCase
import timber.log.Timber


class DerivationPathInputFilter(
    val checkIsValidPkiPath: CheckIsValidPkiPathUseCase
) : InputFilter {

    override fun filter(
        source: CharSequence?,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        if (source == null) return null
        Timber.d("source:$source; start:$start; end:$end; dest:$dest; dstart:$dstart; dend:$dend")

        //case deleting from middle of string
//        if (start == 0 && end == 0 && dest != null && dend <= dest.length - 1) {
//            return dest.subSequence(dstart, dend)
//        }

        if (dstart == 0 && dend == 1) return "m"
        if (dstart == 1 && dend == 2) return "/"
        if (dstart == 0 && dend == 2 && !checkIsValidPkiPath(source.toString(), true)) return "m/"
        if (dstart == 0 && dend > 0 && dend == dest?.length && checkIsValidPkiPath(source.toString(), true)) return source
        if (dstart == 0 && dend > 0 && dend == dest?.length && !checkIsValidPkiPath(source.toString(), true)) return "m/"


        return if (source is SpannableStringBuilder) {

            for (i in end - 1 downTo start) {
            //for (i in start until end) {
                val currentChar: Char = source[i]
                val prevChar = if (i == 0 && dstart == 0) {
                    null
                } else if (i == 0) {
                    dest?.get(dstart - 1)
                } else source[i - 1]

                if (currentChar == '/' && prevChar == '/') {
                    source.delete(i, i + 1)
                }
                if (currentChar == '\'' && prevChar == '\'') {
                    source.delete(i, i + 1)
                }
                if (currentChar == 'm' && dstart != 0) {
                    source.delete(i, i + 1)
                }
                if (currentChar == '\'' && prevChar != null && !Character.isDigit(prevChar)) continue
                if (!isValidCharacter(currentChar)) {
                    source.delete(i, i + 1)
                }
            }
            return source

        } else {
            val filteredStringBuilder = StringBuilder()
            for (i in start until end) {
                val currentChar: Char = source[i]
                val prevChar = if (i == 0 && dstart == 0) {
                    null
                } else if (i == 0) {
                    dest?.get(dstart - 1)
                } else source[i - 1]

                //if (dstart == 0 && source == "m'") return ""
                if (currentChar == '/' && prevChar == '/') continue
                if (currentChar == '\'' && prevChar == '\'') continue
                if (currentChar == 'm' && dstart != 0) continue
                if (currentChar == '\'' && prevChar != null && !Character.isDigit(prevChar)) continue
                if (isValidCharacter(currentChar)) {
                    filteredStringBuilder.append(currentChar)
                }
            }
            filteredStringBuilder.toString()
        }
    }

    private fun isValidCharacter(c: Char): Boolean {
        return Character.isDigit(c) || c == '/' || c == '\'' || c == 'm'
    }
}