package app.bitcoin.baemail.core.data.nodejs.handler

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.core.data.util.SecureDataSource
import com.google.gson.JsonArray
import timber.log.Timber

/*

# expected structure of params array
[
    "keyAlias",
    "m/some/path/0",
    "message content"
]



# expected structure of result array
[
    "signature"
]


*/
class SignRequestHandler(
    val secureDataSource: SecureDataSource,
    val sign: suspend (xpriv: String, path: String, message: String) -> String?
) : NodeRuntimeInterface.WorkHandler("SIGN_REQUEST") {

    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<NodeRuntimeInterface.Result, JsonArray> {

        try {


            val keyAlias = params[0].asString
            val keyPath = params[1].asString
            val messageToSign = params[2].asString

            //todo check if `keyAlias` is a constant type for `activePaymail` or `activeFundingWallet`
            //todo for rest of the keys do this

            val xpriv = secureDataSource.getCustom(keyAlias) ?: let {
                //there is no matching entry for this alias
                throw RuntimeException()
            }

            val signature = sign(xpriv, keyPath, messageToSign) ?: let {
                //there was an error when signing the message
                throw RuntimeException()
            }


            val jsonArray = JsonArray()
            jsonArray.add(signature)

            return Pair(NodeRuntimeInterface.Result.SUCCESS, jsonArray)

        } catch (e: Exception) {
            Timber.e(e)
            return Pair(NodeRuntimeInterface.Result.FAIL, JsonArray())
        }

    }


}