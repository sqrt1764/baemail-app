package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.domain.repository.CoinRepository
import app.bitcoin.baemail.core.domain.entities.CoinRef
import app.bitcoin.baemail.core.data.room.entity.Coin
import kotlinx.coroutines.flow.Flow

class GetCoinUseCaseImpl(
    private val coinRepository: CoinRepository
) : GetCoinUseCase {

    override operator fun invoke(coin: CoinRef): Flow<Coin?> =
        coinRepository.getCoin(coin.address, coin.txId, coin.outputIndex)

}

interface GetCoinUseCase {
    operator fun invoke(coin: CoinRef): Flow<Coin?>
}


