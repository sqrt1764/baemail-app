package app.bitcoin.baemail.core.presentation.view

import android.content.Context
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.ViewCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.util.match
import timber.log.Timber
import kotlin.math.min

class ChainSyncView : ConstraintLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private val dp1: Float

    private var icon: ImageView
    private var title: TextView
    private var subtitle: TextView
    private var retry: Button

    private var valueModel = Model(
            ColorDrawable(Color.GREEN),
            "unset-name",
            "unset-mail",
            false
    )

    var onRetryClicked: () -> Unit = {
        Timber.d("onRetryClicked")
    }

    init {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.view_chain_sync, this, false)
        val set = ConstraintSet()
        view.id = ViewCompat.generateViewId()
        addView(view)

        set.clone(this)
        set.match(view, this)

        dp1 = context.resources.displayMetrics.density


        icon = view.findViewById(R.id.icon)
        title = view.findViewById(R.id.title)
        subtitle = view.findViewById(R.id.subtitle)
        retry = view.findViewById(R.id.retry)


        icon.clipToOutline = true
        icon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val roundness = min(view.width, view.height)

                outline.setRoundRect(0, 0, view.width, view.height, roundness.toFloat())
            }
        }

        retry.setOnClickListener {
            onRetryClicked()
        }

        refresh()

    }

    private fun refresh() {
        icon.setImageDrawable(valueModel.icon)
        title.text = valueModel.title
        subtitle.text = valueModel.subtitle
        if (valueModel.showRetry) {
            retry.visibility = View.VISIBLE
        } else {
            retry.visibility = View.GONE
        }
    }

    fun applyModel(model: Model) {
        valueModel = model

        refresh()
    }

    data class Model(
        val icon: Drawable,
        val title: String,
        val subtitle: String,
        val showRetry: Boolean
    )
}