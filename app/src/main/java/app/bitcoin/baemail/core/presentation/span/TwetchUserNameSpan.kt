package app.bitcoin.baemail.core.presentation.span

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.Typeface
import android.text.TextPaint
import android.text.style.LineHeightSpan
import android.text.style.ReplacementSpan

class TwetchUserNameSpan (
    private val context: Context,
    private val name: String,
    private val strokeColor: Int,
    private val strokeWidth: Float,
    private val textColor: Int,
    private val idColor: Int,
    private val sidePadding: Float = 16f,
    private val radius: Float? = null,
    private val textSizePercent: Float = 0.9f
) : ReplacementSpan(), LineHeightSpan {

    companion object {
        var boldFont: Typeface? = null
        private fun typefaceBold(ctx: Context): Typeface  {
            return boldFont?.let { it } ?: let {
                boldFont = Typeface.DEFAULT_BOLD
                boldFont!!
            }
        }
    }

    private val namePaint = TextPaint(Paint.ANTI_ALIAS_FLAG)
    private val idPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)

    private val strokePaint = Paint(Paint.ANTI_ALIAS_FLAG).also {
        it.color = strokeColor
        it.strokeWidth = strokeWidth
        it.style = Paint.Style.STROKE
    }

    private fun getFinalText(
        text: String,
        start: Int,
        end: Int
    ): String {
        return "$name " + text.substring(start, end)
    }

    override fun getSize(paint: Paint, text: CharSequence?, start: Int, end: Int, fm: Paint.FontMetricsInt?): Int {
        namePaint.set(paint)

        namePaint.typeface = typefaceBold(context)


        namePaint.textSize = paint.textSize * textSizePercent


        val finalText = getFinalText(text?.toString() ?: "", start, end)

        return (sidePadding
                + namePaint.measureText(finalText)
                + sidePadding).toInt()
    }

    override fun draw(
        canvas: Canvas,
        text: CharSequence?,
        start: Int,
        end: Int,
        x: Float,
        top: Int,
        y: Int,
        bottom: Int,
        paint: Paint
    ) {
        namePaint.set(paint)
        namePaint.typeface = typefaceBold(context)
        namePaint.textSize = paint.textSize * textSizePercent

        idPaint.set(paint)
        idPaint.textSize = paint.textSize * textSizePercent


        val t = text?.toString() ?: ""
        val textLength = t.substring(start, end).length
        val finalText = getFinalText(t, start, end)

        val width = namePaint.measureText(finalText)
        val namePartWidth = width - namePaint.measureText(t.substring(start, end))
        val height = namePaint.fontMetrics.descent - namePaint.fontMetrics.ascent + 2 * sidePadding


        val finalTextY = y.toFloat()

//        Timber.d("x:$x y:$y textSize:${paint.textSize} top:$top bottom:$bottom start:$start end:$end ......finalText:$finalText")

        namePaint.color = textColor
        canvas.drawText(finalText, 0, finalText.length - textLength, x + sidePadding, finalTextY, namePaint)


        idPaint.color = idColor
        canvas.drawText(finalText, finalText.length - textLength, finalText.length, x + sidePadding + namePartWidth, finalTextY, idPaint)




        if(radius != null) {
            val rect = RectF(
                x + strokeWidth,
                finalTextY + namePaint.fontMetrics.top + strokeWidth,
                x + width + 2 * sidePadding - strokeWidth,
                finalTextY + namePaint.fontMetrics.bottom
            )
            canvas.drawRoundRect(rect, radius, radius, strokePaint)
        } else {
            canvas.drawCircle(x + ((width + 2 * sidePadding) / 2), ((bottom - top) / 2).toFloat(), height / 2, strokePaint)
        }

    }

    override fun chooseHeight(
        text: CharSequence?,
        start: Int,
        end: Int,
        spanstartv: Int,
        lineHeight: Int,
        fm: Paint.FontMetricsInt?
    ) {
    }
}