package app.bitcoin.baemail.core.presentation.view

import android.content.Context
import android.graphics.Outline
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.view.ViewCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.twetch.ui.CoinManagementUseCase
import timber.log.Timber

class ViewCoinManagementHelper : FrameLayout {

    companion object {
        val HELPER_HIDDEN = CoinManagementUseCase.UNINITIALIZED
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private val dp1: Float

    private var info: TextView
    private var action: Button

    private var valueModel = HELPER_HIDDEN

    var clickTopUpListener: ()->Unit = {
        Timber.d("on top-up clicked")
    }

    var clickSplitCoinsListener: ()->Unit = {
        Timber.d("on split-coins clicked")
    }

    var clickRetryReconnectListener: ()->Unit = {
        Timber.d("on retry reconnect clicked")
    }


    init {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.view_coin_management_helper, this, false)
        view.id = ViewCompat.generateViewId()
        addView(view)

        dp1 = context.resources.displayMetrics.density




        clipToOutline = true
        outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                outline.setRoundRect(0, 0, view.width, view.height, 12 * dp1)
            }

        }

        info = view.findViewById(R.id.info)
        action = view.findViewById(R.id.action)




        action.setOnClickListener {
            when (valueModel.fundStatus) {
                CoinManagementUseCase.FundState.TOO_FEW_COINS -> {
                    clickSplitCoinsListener()
                }
                CoinManagementUseCase.FundState.NO_FUNDS,
                CoinManagementUseCase.FundState.LOW_FUNDS -> {
                    clickTopUpListener()
                }
                else -> {
                    //do nothing
                }
            }

            if (ChainSyncStatus.ERROR == valueModel.syncStatus && valueModel.hasInternet) {
                clickRetryReconnectListener()
            }
        }



    }

    fun applyModel(model: CoinManagementUseCase.Model) {
        valueModel = model

        refresh()
    }



    private fun refresh() {
//        Timber.d("#refresh ... $valueModel ${Log.getStackTraceString(RuntimeException())}")
        if (valueModel == HELPER_HIDDEN) {
            visibility = View.INVISIBLE
            return
        }


        if (ChainSyncStatus.ERROR == valueModel.syncStatus) {
            info.setText(R.string.coin_sync_error)
            visibility = View.VISIBLE

            if (valueModel.hasInternet) {
                action.setText(R.string.retry)
                action.visibility = View.VISIBLE
            } else {
                action.visibility = View.GONE
            }

        } else if (ChainSyncStatus.DISCONNECTED == valueModel.syncStatus) {
            if (!valueModel.hasInternet) {
                info.setText(R.string.no_internet)
                visibility = View.VISIBLE
                action.visibility = View.GONE

            } else {
                visibility = View.INVISIBLE
            }

        } else {
            action.visibility = View.VISIBLE

            if (CoinManagementUseCase.FundState.NO_FUNDS == valueModel.fundStatus) {
                info.setText(R.string.no_funds)
                action.setText(R.string.top_up)
                visibility = View.VISIBLE

            } else if (CoinManagementUseCase.FundState.LOW_FUNDS == valueModel.fundStatus) {
                val cents = valueModel.satsAvailable / valueModel.satsInACent
                info.text = resources.getString(R.string.low_funds, cents)
                action.setText(R.string.top_up)
                visibility = View.VISIBLE

            } else if (CoinManagementUseCase.FundState.TOO_FEW_COINS == valueModel.fundStatus) {
                info.text = resources.getString(R.string.too_few_coins, valueModel.coinCount)
                action.setText(R.string.split_funding_coin)
                visibility = View.VISIBLE

            } else {
                visibility = View.INVISIBLE
            }
        }
    }



}