package app.bitcoin.baemail.core.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.bitcoin.baemail.core.data.room.TABLE_PAYMAILS
import app.bitcoin.baemail.core.data.room.entity.Paymail

@Dao
abstract class PaymailDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertPaymail(bookmark: Paymail)

    @Query("select * from $TABLE_PAYMAILS")
    abstract fun getAddedPaymails(): LiveData<List<Paymail>>
}