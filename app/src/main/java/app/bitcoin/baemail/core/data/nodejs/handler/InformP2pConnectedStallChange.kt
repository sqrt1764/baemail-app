package app.bitcoin.baemail.core.data.nodejs.handler

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.p2p.InformP2pChangeHelper
import com.google.gson.JsonArray
import timber.log.Timber


/*
expected structure
[
    {
        currentLayout: [{
            layout: { .. },
            name: 'aa'',
            paymail: 'bbb'',
            pki: 'ccc'
        }, .. ],
        connectedStalls: [
            {
                name: 'aaa',
                paymail: 'bb',
                pki: 'ccc'
            },
            ..
        ]
    }
]

*/
class InformP2pConnectedStallChange(
    private val informP2pChangeHelper: InformP2pChangeHelper
) : NodeRuntimeInterface.WorkHandler("INFORM_P2P_CONNECTED_STALL_CHANGE") {



    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<NodeRuntimeInterface.Result, JsonArray> {

        try {
            val config = params[0].asJsonObject
            val currentLayout = config.getAsJsonArray("currentLayout")
            val connectedStalls = config.getAsJsonArray("connectedStalls")

            Timber.d("..... $connectedStalls ....  $currentLayout")

            informP2pChangeHelper.informStallSessionChange(connectedStalls, currentLayout)

            Timber.d("#INFORM_P2P_CONNECTED_STALL_CHANGE ... updated layout for discovered stalls\n$currentLayout\n$connectedStalls")

            val jsonArray = JsonArray()
            return Pair(NodeRuntimeInterface.Result.SUCCESS, jsonArray)

        } catch (e: Exception) {
            Timber.e(e)
            return Pair(NodeRuntimeInterface.Result.FAIL, JsonArray())

        }

    }

}