package app.bitcoin.baemail.core.data.wallet

import app.bitcoin.baemail.core.data.nodejs.NodeConnectedLiveData
import app.bitcoin.baemail.core.data.util.ConnectivityLiveData
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import timber.log.Timber
import kotlin.coroutines.resume

class ExchangeRateHelper(
    private val appUtilDiskDAO: AppUtilDiskDAO,
    private val blockchainDataSource: BlockchainDataSource,
    private val connectivityLD: ConnectivityLiveData,
    private val nodeConnectedLiveData: NodeConnectedLiveData,
    private val coroutineUtil: CoroutineUtil
) {

    companion object {
        private const val MILLIS_REFRESH_PERIOD = 1000 * 60 * 10L // 10 minutes
        val INVALID_STATE = ExchangeRate("USD", 0.0, -1L)
    }


    private val _state = MutableStateFlow(Model(
        State.QUERYING,
        INVALID_STATE
    ))

    val state: StateFlow<Model>
        get() = _state.asStateFlow()

    private var shouldBeSuspended = true

    private var refreshResumingContinuation: CancellableContinuation<Unit>? = null


    fun setup() {

        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            val rate = appUtilDiskDAO.readExchangeRate() ?: INVALID_STATE

            val state = if (INVALID_STATE == rate) {
                State.QUERYING
            } else {
                State.SUCCESS
            }

            _state.value = Model(
                state,
                rate
            )

            val now = System.currentTimeMillis()
            val calculatedNextRefreshTime = MILLIS_REFRESH_PERIOD + rate.millisCreated

            val millisToDelay = when {
                calculatedNextRefreshTime < now -> 0L
                else -> calculatedNextRefreshTime - now
            }

            delay(millisToDelay)

            while (true) {
                refreshNow()

                delay(MILLIS_REFRESH_PERIOD)
            }

        }

        connectivityLD.observeForever { connected ->
            val currentIsConnected = connected?.isConnected ?: return@observeForever

            if (!currentIsConnected) return@observeForever
            if (State.FAIL == _state.value.state) {
                coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
                    Timber.d("device reconnected to internet & there was a fail exchange-rate refresh attempt")
                    refreshNow()
                }
            }
        }

        nodeConnectedLiveData.observeForever { connected ->
            shouldBeSuspended = !connected

            if (!shouldBeSuspended) {
                refreshResumingContinuation?.let {
                    Timber.d("resuming-continuation found; resuming now")
                    refreshResumingContinuation = null
                    it.resume(Unit)
                }
            }
        }
    }


    private suspend fun refreshNow() {
        awaitRefreshAllowed()

        _state.value = _state.value.copy(
            state = State.QUERYING
        )

        val rate = blockchainDataSource.fetchExchangeRate()

        if (rate == null) {
            _state.value = _state.value.copy(
                state = State.FAIL
            )

            Timber.d("exchange-rate refresh failed")

        } else {
            _state.value = Model(
                State.SUCCESS,
                rate
            )

            appUtilDiskDAO.writeExchangeRate(rate)

            Timber.d("exchange-rate refreshed - $rate")
        }
    }

    private suspend fun awaitRefreshAllowed() {
        suspendCancellableCoroutine { continuation ->
            if (!shouldBeSuspended) {
                continuation.resume(Unit)
            } else {
                Timber.d("refreshing should be suspended; exiting & awaiting")
                refreshResumingContinuation = continuation
            }
        }
    }

    enum class State {
        QUERYING,
        SUCCESS,
        FAIL
    }

    data class Model(
        val state: State,
        val rate: ExchangeRate
    )

}