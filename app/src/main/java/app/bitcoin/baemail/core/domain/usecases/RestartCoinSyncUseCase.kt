package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.data.wallet.DynamicChainSync

class RestartCoinSyncUseCaseImpl(
    private val dynamicChainSync: DynamicChainSync
) : RestartCoinSyncUseCase {

    override fun invoke() = dynamicChainSync.considerRetry()

}

interface RestartCoinSyncUseCase {
    operator fun invoke()
}