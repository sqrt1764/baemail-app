package app.bitcoin.baemail.core.presentation

import androidx.lifecycle.*
import app.bitcoin.baemail.R
import app.bitcoin.baemail.intercept.AppDeepLinkType
import app.bitcoin.baemail.core.data.util.DeepLinkHelper
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.core.presentation.entity.CentralModel
import app.bitcoin.baemail.core.presentation.entity.CentralTab
import app.bitcoin.baemail.core.presentation.entity.CentralTabModel
import app.bitcoin.baemail.di.SavedStateViewModelAssistant
import app.bitcoin.baemail.twetch.ui.CoinManagementUseCase
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class CentralViewModel @AssistedInject constructor(
    @Assisted private val state : SavedStateHandle,
    getActivePaymailUseCase: GetActivePaymailUseCase,
    private val deepLinkHelper: DeepLinkHelper, //todo switch to a usecase
    val coinManagementUseCase: CoinManagementUseCase
) : ViewModel() {

    @AssistedFactory
    interface Assistant : SavedStateViewModelAssistant<CentralViewModel> {
        override fun create(savedStateHandle: SavedStateHandle): CentralViewModel
    }

    private val tabList = listOf(
        CentralTab.TWETCH,
        CentralTab.BAEMAIL,
        CentralTab.REPL,
        CentralTab.P2P,
        CentralTab.PAYMENTS,
    )

    private val activeTabIndex = MutableStateFlow(0)

    private val millisTabUpdated = MutableStateFlow(System.currentTimeMillis())

    val tabBarModel = activeTabIndex.combine(millisTabUpdated) { activeTabIndex, millisTabUpdated ->
        //.map { activeTabIndex ->
        tabList.mapIndexed { index, tab ->
            val isSelected = index == activeTabIndex
            CentralTabModel(
                tab,
                if (isSelected) R.color.button_bar_selected_color
                else R.color.button_bar_unselected_color,
                isSelected
            )
        } to millisTabUpdated
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = (emptyList<CentralTabModel>() to 0L)
    )

    val content = getActivePaymailUseCase().map { activePaymail ->
        CentralModel(activePaymail?.paymail ?: "")
    }

    init {
        viewModelScope.launch {
            deepLinkHelper.unsatisfiedLink.collectLatest { link ->
                link ?: return@collectLatest

                when (link.type) {
                    AppDeepLinkType.TWETCH_CHAT,
                    AppDeepLinkType.TWETCH_POST,
                    AppDeepLinkType.TWETCH_USER -> {
                        onTabClicked(CentralTab.TWETCH)
                    }
                    AppDeepLinkType.P2P_STALL_MANAGE -> {
                        onTabClicked(CentralTab.P2P)
                    }
                }
            }
        }
    }

    fun onTabClicked(tab: CentralTab) {
        val index = tabList.indexOf(tab)
        if (index != activeTabIndex.value) {
            activeTabIndex.value = index
        } else {
            millisTabUpdated.value = System.currentTimeMillis()
        }

    }

}