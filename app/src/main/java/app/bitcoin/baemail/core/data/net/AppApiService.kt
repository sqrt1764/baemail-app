package app.bitcoin.baemail.core.data.net

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

// https://bitcoinsv.io/2020/04/03/miner-id-and-merchant-api-beta-release/

// http://bsvalias.org/01-brfc-specifications.html
interface AppApiService {

    @HEAD
    fun fetchHead(@Url url: String): Call<Void>

    @GET
    fun fetch(@Url url: String): Call<ResponseBody>

    @GET
    @Headers(
            "Accept: */*",
            //"Content-type: application/json",
    )
    fun fetch1(@Url url: String, @Header("Cookie") userCookie: String): Call<ResponseBody>

    @POST
    @Headers(
        "Accept: */*",
        "Content-type: application/json",
    )
    fun post(
            @Url url: String,
            @Body body: RequestBody,
            @Header("Cookie") userCookie: String = ""
    ): Call<ResponseBody>

    @POST
    @Headers(
        "Content-type: application/json; charset=utf-8",
        "Accept: application/json; charset=utf-8"
    )
    fun postJson(@Url url: String, @Body body: RequestBody): Call<ResponseBody>

    @GET
    fun fetchWithKey(@Url url: String, @Header("key") key: String): Call<ResponseBody>

    @POST("https://api.whatsonchain.com/v1/bsv/main/tx/raw")
    @Headers(
        "Content-type: application/json"
    )
    fun postHexTx(@Body body: RequestBody): Call<ResponseBody>
}
