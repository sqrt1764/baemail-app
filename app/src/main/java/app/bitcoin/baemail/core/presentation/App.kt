package app.bitcoin.baemail.core.presentation

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.os.Build
import android.os.Bundle
import android.util.Log
import app.bitcoin.baemail.BuildConfig
import coil.ImageLoader
import app.bitcoin.baemail.di.AppComponent
import app.bitcoin.baemail.di.AppModule
import app.bitcoin.baemail.di.DaggerAppComponent
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.data.util.FileLoggingTree
import app.bitcoin.baemail.core.presentation.util.OnCreatedActivityLifecycleListener
import coil.ImageLoaderFactory
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import java.lang.RuntimeException
import java.lang.reflect.InvocationTargetException
import javax.inject.Inject

class App : Application(), ImageLoaderFactory, HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var imageLoader: ImageLoader

    @Inject
    lateinit var coroutineUtil: CoroutineUtil

    lateinit var nodeModel: NodeModel

    @SuppressLint("LogNotTimber")
    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)

        app = this


        val processName = findProcessName()
        if (processName.contains(':')) {
            //do not initialize components used only in the main-process
            Log.d("App", "exiting early from App#onCreate processName:$processName")
            return
        }

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()

        appComponent.inject(this)

        nodeModel = appComponent.provideNodeModel()


        initTimber()

        registerActivityLifecycleCallbacks(
            object : OnCreatedActivityLifecycleListener() {
                override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                    AndroidInjection.inject(activity)
                }
            })


        Timber.d("""
            end of App#onCreate
            ///////////////////
            ///////////////////
            ///////////////////
            ///////////////////
            ///////////////////
            ///////////////////
        """.trimIndent())
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        Timber.plant(FileLoggingTree(this, coroutineUtil, 3))

        Timber.d("App#initTimber")
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    override fun newImageLoader(): ImageLoader {
        return imageLoader
    }


    companion object {

        @SuppressLint("DiscouragedPrivateApi")
        fun findProcessName(): String {
            return if (Build.VERSION.SDK_INT >= 28) {
                getProcessName()
            } else {
                try {
                    @SuppressLint("PrivateApi") val activityThread = Class.forName("android.app.ActivityThread")
                    activityThread.getDeclaredMethod("currentProcessName").invoke(null) as String
                } catch (e: ClassNotFoundException) {
                    throw RuntimeException(e)
                } catch (e: NoSuchMethodException) {
                    throw RuntimeException(e)
                } catch (e: IllegalAccessException) {
                    throw RuntimeException(e)
                } catch (e: InvocationTargetException) {
                    throw RuntimeException(e)
                }
            }
            // Using the same technique as Application.getProcessName() for older devices
            // Using reflection since ActivityThread is an internal API
        }

        private lateinit var app: App

        private lateinit var appComponent: AppComponent

        fun get(): App = app

        fun getComponent(): AppComponent = appComponent
    }
}