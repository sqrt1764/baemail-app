package app.bitcoin.baemail.core.presentation.util

import android.text.TextWatcher

abstract class OnAfterChangeTextWatcher : TextWatcher {
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
}