package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.data.wallet.ExchangeRate
import app.bitcoin.baemail.core.domain.repository.BsvStatusRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GetExchangeRateUseCaseImpl(
    private val bsvStatusRepository: BsvStatusRepository
) : GetExchangeRateUseCase {

    override val exchangeRate: Flow<ExchangeRate>
        get() = bsvStatusRepository.exchangeRate.map { it.rate }

}

interface GetExchangeRateUseCase {
    val exchangeRate: Flow<ExchangeRate> //todo switch to a domain-model
}