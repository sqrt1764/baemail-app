package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.core.domain.entities.ReceivedCoin
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber
import java.lang.Exception

/*
#expecting params
[
    ['seed', 'words', ..],
    [{tx_id:'', sats: 123, index: 0, address: '1FFF..FF', derivation_path:'/0/13'}, {..}]
    [
        {
            type: 'address',
            value: '1FFF..FF',
            sats: 1234
        },
        {
            type: 'destinationScriptHex',
            value: 'F0F0F0F0..F0',
            sats: 2345
        },
        ..
    ]
]

# response
[
    {
        hexTx: '1f1f1f1f1f',
        hash: '0F0F0F'
    }
]
*/

class CreateSimpleOutputScriptTx(
    val seed: List<String>,
    val spendingCoins: Map<String, ReceivedCoin>, //key: derivation-path value: utxo
    val destinationList: List<Destination>,
    val gson: Gson,
    val callback: (TxInfo?)->Unit
) : NodeRuntimeInterface.NodeRequest("CREATE_SIMPLE_OUTPUT_SCRIPT_TX"){

    override fun getReqParams(): JsonArray {
        val jsonSeedArray = JsonArray()
        seed.forEach {
            jsonSeedArray.add(it)
        }

        val jsonSpendingCoinsArray = JsonArray()
        spendingCoins.forEach {
            val jsonObject = gson.toJsonTree(it.value).asJsonObject
            jsonObject.addProperty("derivation_path", it.key)
            jsonSpendingCoinsArray.add(jsonObject)
        }



        val destinationJsonArray = JsonArray()
        destinationList.forEach { destination ->
            val destinationJson = JsonObject()
            destinationJson.addProperty("type", destination.type.value)
            destinationJson.addProperty("value", destination.value)
            destinationJson.addProperty("sats", destination.sats)

            destinationJsonArray.add(destinationJson)
        }



        val wrappingJsonArray = JsonArray()
        wrappingJsonArray.add(jsonSeedArray)
        wrappingJsonArray.add(jsonSpendingCoinsArray)
        wrappingJsonArray.add(destinationJsonArray)



        return wrappingJsonArray
    }


    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "error deriving requested set of addresses; token: $token")
            callback(null)
            return
        }

        try {
            val dataJson = responseData ?: throw RuntimeException("data missing")

            val envelopeJson = dataJson.get(0).asJsonObject
            val hexTx = envelopeJson.get("hexTx").asString
            val txHash = envelopeJson.get("hash").asString

            callback(TxInfo(
                hexTx,
                txHash
            ))

        } catch (e: Exception) {
            Timber.e(e, "token: $token")
            callback(null)
        }
    }

    enum class DestinationType(
        val value: String
    ) {
        ADDRESS("address"),
        OUTPUT_SCRIPT("destinationScriptHex")
    }

    data class Destination(
        val type: DestinationType,
        val value: String,
        val sats: Long
    )

    data class TxInfo(
        val txHex: String,
        val txHash: String
    )
}