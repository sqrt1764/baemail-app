package app.bitcoin.baemail.core.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.bitcoin.baemail.core.data.room.TABLE_BLOCK_SYNC_INFO
import app.bitcoin.baemail.core.data.room.entity.BlockSyncInfo

@Dao
abstract class BlockSyncInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertInfo(info: BlockSyncInfo)

    @Query("select * from $TABLE_BLOCK_SYNC_INFO where paymail = :paymail order by height desc limit 1")
    abstract suspend fun getLatestInfo(paymail: String): BlockSyncInfo?

    @Query("select * from $TABLE_BLOCK_SYNC_INFO where paymail = :paymail order by height desc limit 25")
    abstract suspend fun getRecentSyncInfoList(paymail: String): List<BlockSyncInfo>

    @Query("delete from $TABLE_BLOCK_SYNC_INFO where height >= :includingAndUpHeight and paymail = :paymail")
    abstract suspend fun clearForHeight(includingAndUpHeight: Int, paymail: String)

}