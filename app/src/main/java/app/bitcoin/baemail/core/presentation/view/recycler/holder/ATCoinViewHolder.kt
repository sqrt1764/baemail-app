package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.Color
import android.graphics.drawable.LayerDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.util.formatSatsLabel
import app.bitcoin.baemail.core.presentation.view.recycler.ATCoin
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr

class ATCoinViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATCoinListener

    var model: ATCoin? = null

    val path: TextView = itemView.findViewById(R.id.path)
    val sats: TextView = itemView.findViewById(R.id.sats)
    val isActive: ImageView = itemView.findViewById(R.id.is_active)

    lateinit var strokeDrawable: DrawableStroke

    val strokeColor = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)

    init {
        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(itemView.context, id)
        }

        val strokeWidth = R.dimen.li_filter_seed_word__stroke_width.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        val strokePaddingSides = R.dimen.li_filter_seed_word__stroke_padding_sides.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        strokeDrawable = DrawableStroke().also {
            it.setStrokeColor(strokeColor)
            it.setStrokeWidths(0, 0, 0, strokeWidth)
            it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
        }

        itemView.background = LayerDrawable(arrayOf(
            strokeDrawable,
            DrawableState.getNew(colorSelector)
        ))

        itemView.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onClick(m.coin)
        }

        itemView.setOnLongClickListener {
            val m = model ?: return@setOnLongClickListener false
            listener.onLongClick(m.coin)
            true
        }


        isActive.visibility = View.INVISIBLE

    }






    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATCoin

        this.listener = listener as BaseListAdapter.ATCoinListener

        path.text = model.coin.derivationPath
        sats.text = formatSatsLabel(model.coin.sats.toString())

        if (model.isLast) {
            strokeDrawable.setStrokeColor(Color.TRANSPARENT)
        } else {
            strokeDrawable.setStrokeColor(strokeColor)
        }

        if (model.isSelected) {
            isActive.visibility = View.VISIBLE
        } else {
            isActive.visibility = View.INVISIBLE
        }
    }


    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATCoinViewHolder {
            return ATCoinViewHolder(
                inflater.inflate(
                    R.layout.li_coin,
                    parent,
                    false
                )
            )
        }
    }
}