package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.Color
import android.graphics.drawable.LayerDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.view.recycler.ATUnusedAddress
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr

class ATUnusedAddressViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATUnusedAddressListener

    var model: ATUnusedAddress? = null

    val path: TextView = itemView.findViewById(R.id.path)
    val sats: TextView = itemView.findViewById(R.id.sats)

    var strokeDrawable: DrawableStroke

    val strokeColor = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
//            R.color.li_filter_seed_word__stroke_color.let { id ->
//        ContextCompat.getColor(itemView.context, id)
//    }

    init {
        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(itemView.context, id)
        }

        val strokeWidth = R.dimen.li_filter_seed_word__stroke_width.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        val strokePaddingSides = R.dimen.li_filter_seed_word__stroke_padding_sides.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        strokeDrawable = DrawableStroke().also {
            it.setStrokeColor(strokeColor)
            it.setStrokeWidths(0, 0, 0, strokeWidth)
            it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
        }

        itemView.background = LayerDrawable(arrayOf(
            strokeDrawable,
            DrawableState.getNew(colorSelector)
        ))

        itemView.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onClick(m.coin)
        }



    }






    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.listener = listener!! as BaseListAdapter.ATUnusedAddressListener
        this.model = model as ATUnusedAddress

        path.text = model.coin.path
        sats.text = model.coin.address

        if (model.isLast) {
            strokeDrawable.setStrokeColor(Color.TRANSPARENT)
        } else {
            strokeDrawable.setStrokeColor(strokeColor)
        }
    }






    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATUnusedAddressViewHolder {
            return ATUnusedAddressViewHolder(
                inflater.inflate(
                    R.layout.li_unused_address,
                    parent,
                    false
                )
            )
        }
    }
}