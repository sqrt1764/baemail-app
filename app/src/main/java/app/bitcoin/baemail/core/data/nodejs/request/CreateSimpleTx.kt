package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.core.domain.entities.ReceivedCoin
import com.google.gson.Gson
import com.google.gson.JsonArray
import timber.log.Timber
import java.lang.Exception

/*
#expecting params
[
    ['seed', 'words', ..],
    [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/0/13'}, {..}],
    '1FFF..EE', //destination address
    123, //sats
]

# response
[
    {
        'hexTx': '1f1f1f1f1f',
        'hash': '1f1f',
        'output': {
            address: '1C6BBGPhq57UCA1DxK5HQkT7Essr8zzLXJ',
            outputIndex: 0,
            sats: 123
        }
    }
]
*/
class CreateSimpleTx(
    val seed: List<String>,
    val spendingCoins: Map<String, ReceivedCoin>, //key: derivation-path value: utxo
    val destinationAddress: String,
    val sats: Long,
    val gson: Gson,
    val callback: (TxInfoSendToAddress?)->Unit
) : NodeRuntimeInterface.NodeRequest("CREATE_SIMPLE_TX") {

    override fun getReqParams(): JsonArray {
        val jsonSeedArray = JsonArray()
        seed.forEach {
            jsonSeedArray.add(it)
        }

        val jsonSpendingCoinsArray = JsonArray()
        spendingCoins.forEach {
            val jsonObject = gson.toJsonTree(it.value).asJsonObject
            jsonObject.addProperty("derivation_path", it.key)
            jsonSpendingCoinsArray.add(jsonObject)
        }





        val wrappingJsonArray = JsonArray()
        wrappingJsonArray.add(jsonSeedArray)
        wrappingJsonArray.add(jsonSpendingCoinsArray)
        wrappingJsonArray.add(destinationAddress)
        wrappingJsonArray.add(sats)



        return wrappingJsonArray
    }

    /*
    expecting response
    [{"hexTx":"010000000136ffffccb285df739f397c40df0b842c57d7ae499a9 .. 0387ffffff017662000000000083fd89d8581e603180bed13b6088ac00000000"}]
     */
    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "error deriving requested set of addresses; token: $token")
            callback(null)
            return
        }

        try {
            val dataJson = responseData ?: throw RuntimeException("data missing")

            val envelopeJson = dataJson.get(0).asJsonObject
            val hexTx = envelopeJson.get("hexTx").asString
            val txHash = envelopeJson.get("hash").asString

            val outputObject = envelopeJson.getAsJsonObject("output")
            val address = outputObject.get("address").asString
            val outputIndex = outputObject.get("outputIndex").asInt
            val sats = outputObject.get("sats").asLong

            val output = TxOutput(
                address,
                outputIndex,
                sats
            )

            callback(TxInfoSendToAddress(
                hexTx,
                txHash,
                output
            ))

        } catch (e: Exception) {
            Timber.e(e, "token: $token")
            callback(null)
        }
    }


}


data class TxInfoSendToAddress(
    val txHex: String,
    val txHash: String,
    val output: TxOutput
)