package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.p2p.P2pStallRepository
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber

/*
# expected structure of params
[
    {
        port: 99999,
        pkiKey: "hex",
        pkiPrivateKey: "hex",
        paymail: "",
        title: "",
        forceUsePaymailPki: false,
        customPkiWifPrivate: "hex",
        connectionType: "nearby"  //possible values: `nearby`, `wifi`
    }
]


# expected structure of the response data passed in the callback
[
    true
]
 */
class StartStallSocketServer(
    val port: Int,
    val config: P2pStallRepository.PeerServiceConfig,
    val callback: (Boolean?)->Unit
) : NodeRuntimeInterface.NodeRequest("START_STALL_SOCKET_SERVER") {

    override fun getReqParams(): JsonArray {
        val configJson = JsonObject()
        configJson.addProperty("port", port)
        configJson.addProperty("pkiKey", config.id.pki)
        configJson.addProperty("pkiPrivateKey", config.pkiPriv)
        configJson.addProperty("paymail", config.id.paymail)
        configJson.addProperty("title", config.id.title)
        configJson.addProperty("forceUsePaymailPki", config.forceUsePaymailPki)
        configJson.addProperty("customPkiWifPrivate", config.customPkiWifPrivate)
        configJson.addProperty("connectionType", config.type.const)

        val finalArray = JsonArray()
        finalArray.add(configJson)

        return finalArray
    }

    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "stall server not started; token: $token")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(stackTrace, "unexpected response format; token: $token")
            callback(null)
            return
        }

        try {
            callback(data[0].asBoolean)

        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")
            callback(null)
        }
    }
}