package app.bitcoin.baemail.core.data.wallet

import app.bitcoin.baemail.core.data.nodejs.request.DerivedAddress
import app.bitcoin.baemail.core.data.room.dao.CoinDao
import app.bitcoin.baemail.core.data.room.entity.Coin
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import timber.log.Timber
import kotlin.math.max
import kotlin.math.min

class WocFullSyncHelper(
    private val blockchainDataSource: BlockchainDataSource,
    private val coinDao: CoinDao,
    private val coinAddressDerivationHelper: CoinAddressDerivationHelper,
    private val appUtilDiskDAO: AppUtilDiskDAO
) {

    companion object {
        const val ADDRESS_SET_SIZE = 20
    }

    lateinit var paymail: String
        private set

    private lateinit var seed: List<String>
    private lateinit var path:String

    private val _highestUsedAddressRef = MutableStateFlow<AddressRef?>(null)
    val highestUsedAddressRef: StateFlow<AddressRef?>
        get() = _highestUsedAddressRef.asStateFlow()

    private val _isSyncing = MutableStateFlow(false)
    val isSyncing = _isSyncing.asStateFlow()

    fun setup(
        paymail: String,
        seed: List<String>,
        fundingWalletPath:String
    ) {
        this.paymail = paymail
        this.seed = seed
        this.path = fundingWalletPath
    }

    suspend fun syncFromStart(): Boolean {
        return doSync(false, 0)
    }

    suspend fun syncFromCurrentState(upcomingOnly: Boolean): Boolean {
        val lowestDerivationIndex = if (upcomingOnly) {
            findDerivationIndexOfHighestUnspentCoin()
        } else {
            findDerivationIndexOfLowestUnspentCoin()
        }

        val syncStartIndex = max(0, lowestDerivationIndex - 5) / ADDRESS_SET_SIZE
        Timber.d("syncing coins; recentOnly=$upcomingOnly; index of lowest unspent coin:$lowestDerivationIndex; syncStartIndex:$syncStartIndex")

        return doSync(upcomingOnly, syncStartIndex)
    }


    private suspend fun findDerivationIndexOfLowestUnspentCoin(): Int {
        val coinList = coinDao.getUnspentFundingCoins(paymail)
        val coin = coinList.lastOrNull() ?: return 0
        return coin.pathIndex
    }

    private suspend fun findDerivationIndexOfHighestUnspentCoin(): Int {
        val coinList = coinDao.getUnspentFundingCoins(paymail)
        val coin = coinList.firstOrNull() ?: return 0
        return coin.pathIndex
    }

    private fun onStarted() {
        Timber.d("onStarted paymail:$paymail")
        _isSyncing.value = true
    }

    private fun onStopped() {
        Timber.d("onStopped paymail:$paymail")
        _isSyncing.value = false
    }

    private fun onError(e: Exception) {
        Timber.d(e, "onError")
    }

    private fun onCompleted() {
        Timber.d("onCompleted paymail:$paymail")
    }

    private suspend fun doSync(
        upcomingOnly: Boolean,
        startIndex: Int
    ): Boolean {
        try {
            val extraSetsToCheckForGaps = appUtilDiskDAO
                .readSyncConfigExtraSetsToCheckForGaps(paymail)

            onStarted()

            if (upcomingOnly) syncAddressesOfCurrentlyKnownUnspentCoins()

            var addressSetIndex = startIndex

            var coinsOfLastUsedAddresses = CoinsOfAddresses(listOf(), listOf())

            outer@ while (true) {
                Timber.d("starting unspent coin-sync for set: $addressSetIndex")

                val addressList = coinAddressDerivationHelper.deriveAddressSet(
                    paymail,
                    seed,
                    path,
                    addressSetIndex
                )

                if (addressList.isEmpty()) {
                    throw RuntimeException()
                }

                if (addressSetIndex > 0) {
                    if (!checkIfUpcomingAddressesAreUsed(
                            addressList,
                            addressSetIndex,
                            extraSetsToCheckForGaps
                    )) {

                        Timber.d("stopping address sync; next set appears unused")
                        break@outer
                    }
                }

                val unspentCoins = syncAddressSet(addressList)
                coinsOfLastUsedAddresses = CoinsOfAddresses(addressList, unspentCoins)

                addressSetIndex++
            }


            //inspect the last used address-set to find out the actual first unused derived-address
            // from the coin-address-vector
            refreshFirstUnusedAddressIndex(coinsOfLastUsedAddresses)

            onCompleted()
            return true

        } catch (e: Exception) {
            onError(e)
            return false

        } finally {
            onStopped()
        }
    }

    private suspend fun syncAddressesOfCurrentlyKnownUnspentCoins() {
        coinDao.getFundingCoins(paymail)
            .map { coin ->
                DerivedAddress(coin.derivationPath, coin.address)
            }
            .toSet()
            .toList()
            .let { addressList ->
                var loopStartIndex = 0

                while (true) {
                    if (addressList.size <= loopStartIndex) break

                    val toIndex = min(addressList.size, loopStartIndex + ADDRESS_SET_SIZE)
                    val subList = addressList.subList(loopStartIndex, toIndex)
                    loopStartIndex += ADDRESS_SET_SIZE

                    syncAddressSet(subList)
                }
            }
    }

    private suspend fun syncAddressSet(list: List<DerivedAddress>): List<Coin> {
        if (list.size > ADDRESS_SET_SIZE) throw RuntimeException()

        val simpleList = list.map { address ->
            address.address
        }

        val unspentCoins = blockchainDataSource.findUnspentCoinsForAddress(simpleList)
            ?: throw RuntimeException()


        val coinTruth = unspentCoins.map {
            val matchingDerivedAddress = list.find { derivedAddress ->
                derivedAddress.address == it.address
            } ?: throw RuntimeException()

            Coin(
                paymail,
                matchingDerivedAddress.path,
                matchingDerivedAddress.getIndex(),
                matchingDerivedAddress.address,
                it.txHash,
                it.txPos,
                it.value,
                it.blockHeight
            )
        }

        coinDao.updateUnspent(paymail, simpleList, coinTruth)

        return coinTruth
    }

    private suspend fun checkIfUpcomingAddressesAreUsed(
        currentSet: List<DerivedAddress>,
        currentSetIndex: Int,
        extraSetsToCheck: Int
    ): Boolean {
        if (checkIfAddressesAreUsed(currentSet)) {
            return true
        }

        for (i in 0 until extraSetsToCheck) {
            val addressList = coinAddressDerivationHelper.deriveAddressSet(
                paymail,
                seed,
                path,
                currentSetIndex + 1 + i
            )
            if (checkIfAddressesAreUsed(addressList)) {
                return true
            }
        }
        return false
    }

    private suspend fun checkIfAddressesAreUsed(list: List<DerivedAddress>): Boolean {
        listOf(0, 1, 2, 5, 11, 18).forEach { id ->
            if (blockchainDataSource.checkAddressHasBeenUsed(list[id].address)) {
                return true
            }
        }
        return false
    }

    private suspend fun refreshFirstUnusedAddressIndex(coinsOfLastUsedAddresses: CoinsOfAddresses) {
        val highestAddressCoin = coinsOfLastUsedAddresses.coins.maxByOrNull { it.pathIndex }

        //read the highest used coin-index cached to disk - appUtilDiskDAO
        var highestPathIndexOfCoin = appUtilDiskDAO.readHighestUsedCoin(paymail)

        highestAddressCoin?.pathIndex?.let { index ->
            if (index > highestPathIndexOfCoin) {
                highestPathIndexOfCoin = index
            }
        }

        val addressesListGrowing = coinsOfLastUsedAddresses
            .addressList
            .sortedBy { it.getIndex() }

        for (i in addressesListGrowing.indices) {
            if (i <= highestPathIndexOfCoin) continue
            val derivedAddress = addressesListGrowing[i]

            //query the indexer to find out if this address has been used before
            if (blockchainDataSource.checkAddressHasBeenUsed(derivedAddress.address)) {
                highestPathIndexOfCoin = derivedAddress.getIndex()
            } else {
                break
            }
        }

        _highestUsedAddressRef.value = AddressRef(paymail, highestPathIndexOfCoin)
    }

}

data class CoinsOfAddresses(
    val addressList: List<DerivedAddress>,
    val coins: List<Coin>
)

data class AddressRef(
    val paymail: String,
    val index: Int
)