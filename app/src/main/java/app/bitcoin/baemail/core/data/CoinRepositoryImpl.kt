package app.bitcoin.baemail.core.data

import app.bitcoin.baemail.core.data.wallet.UnusedCoinAddressHelper
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.data.wallet.DynamicChainSync
import app.bitcoin.baemail.core.data.nodejs.request.DerivedAddress
import app.bitcoin.baemail.core.domain.repository.CoinRepository
import app.bitcoin.baemail.core.data.room.dao.CoinDao
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow

class CoinRepositoryImpl(
    private val authRepository: AuthRepository,
    private val coinDao: CoinDao,
    private val dynamicChainSync: DynamicChainSync,
    private val unusedCoinAddressHelper: UnusedCoinAddressHelper
) : CoinRepository {

    override val syncStatus: StateFlow<ChainSyncStatus>
        get() = dynamicChainSync.status

    override fun getCoin(address: String, txId: String, outputIndex: Int): Flow<Coin?> =
        authRepository.activePaymail.flatMapLatest { paymail ->
            paymail?.paymail ?: return@flatMapLatest flow {
                emit(null)
            }
            coinDao.getCoinOfPaymail(paymail.paymail, address, txId, outputIndex)
        }

    override fun getFundingCoins(): Flow<List<Coin>> =
        authRepository.activePaymail.flatMapLatest { paymail ->
            paymail?.paymail ?: return@flatMapLatest flow {
                emit(listOf())
            }
            coinDao.getFundingCoinsFlow(paymail.paymail)
        }

    override suspend fun getUpcomingUnusedAddresses(
        paymail: String,
        addressCount: Int
    ): Flow<List<DerivedAddress>> {
        return unusedCoinAddressHelper.getUpcomingUnusedAddresses(paymail, addressCount)
    }

    override suspend fun saveCoins(ownedCoins: List<Coin>) = coinDao.saveCoins(ownedCoins)

    override suspend fun markCoinsSpent(coins: List<Coin>) = coinDao.markCoinsSpent(coins)

    override suspend fun resyncCoins() {
        authRepository.activePaymail.value!!.paymail!!.let {
            coinDao.deleteAll(it)
            unusedCoinAddressHelper.clearCachedHighestAddress(it)
            dynamicChainSync.fullSyncHelper?.syncFromStart()
        }
    }

    override suspend fun refreshCoins() {
        authRepository.activePaymail.value!!.paymail!!.let {
            dynamicChainSync.fullSyncHelper?.syncFromCurrentState(false)
        }
    }

    override fun getHighestUsedAddressIndex(): Flow<Int> =
        authRepository.activePaymail.flatMapLatest { paymail ->
            paymail?.paymail ?: return@flatMapLatest flow {
                emit(0)
            }
            unusedCoinAddressHelper.getHighestUsedAddressIndex(paymail.paymail)
        }

}

