package app.bitcoin.baemail.core.domain.repository

import app.bitcoin.baemail.core.data.util.AuthenticatedPaymail
import app.bitcoin.baemail.core.domain.entities.ActivePaymail
import kotlinx.coroutines.flow.StateFlow

interface AuthRepository {
    companion object {
        const val FUNDING_WALLET_ROOT_PATH = "m/44'/0'/0'/0"
    }

    val addedPaymails: StateFlow<List<AuthenticatedPaymail>?>
    val activePaymail: StateFlow<ActivePaymail?>

    fun changeActivePaymail(paymail: String)

    suspend fun addPaymail(
        paymail: String,
        paymailPath: String,
        paymailMnemonic: List<String>,
        fundingPath: String,
        fundingMnemonic: List<String>
    )

}