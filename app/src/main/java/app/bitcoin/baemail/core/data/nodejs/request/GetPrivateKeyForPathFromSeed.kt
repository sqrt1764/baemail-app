package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.JsonArray
import timber.log.Timber


class GetPrivateKeyForPathFromSeed(
    val path: String,
    val seed: List<String>,
    val callback: (PrivateKeyHolder?)->Unit
) : NodeRuntimeInterface.NodeRequest("PRIVATE_KEY_FOR_PATH_FROM_SEED") {

    override fun getReqParams(): JsonArray {
        val seedArray = JsonArray()
        seed.forEach {
            seedArray.add(it)
        }

        val finalArray = JsonArray()
        finalArray.add(path)
        finalArray.add(seedArray)

        return finalArray
    }

    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "private key not received; token: $token")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(stackTrace, "unexpected response format; token: $token")
            callback(null)
            return
        }

        try {
            val dataObject = data[0].asJsonObject

            val publicKey = dataObject.get("publicKey")
            val privateKey = dataObject.get("privateKey")

            callback(PrivateKeyHolder(
                path,
                publicKey.asString,
                privateKey.asString
            ))
        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")
            callback(null)
        }
    }


    data class PrivateKeyHolder(
        val path: String,
        val publicKey: String,
        val privateKey: String
    )


}








