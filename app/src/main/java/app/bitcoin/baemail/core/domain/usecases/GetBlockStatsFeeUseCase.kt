package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.data.wallet.BlockStatsFee
import app.bitcoin.baemail.core.domain.repository.BsvStatusRepository
import kotlinx.coroutines.flow.Flow

class GetBlockStatsFeeUseCaseImpl(
    private val bsvStatusRepository: BsvStatusRepository
) : GetBlockStatsFeeUseCase {
    override fun invoke(): Flow<BlockStatsFee> = bsvStatusRepository.txFee

}

interface GetBlockStatsFeeUseCase {
    operator fun invoke(): Flow<BlockStatsFee>
}