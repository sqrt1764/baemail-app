package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.ColorUtils
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableTwetchReplyColumn
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchPostSecondaryLoadingItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr

class ATTwetchPostSecondaryLoadingViewHolder(
    view: View
): BaseViewHolder(view) {

    var model: ATTwetchPostSecondaryLoadingItem? = null

    private val replyColumnDrawable = DrawableTwetchReplyColumn()

    private val colorReplyColumn: Int by lazy {
        val color = itemView.context.getColorFromAttr(R.attr.colorOnSurfaceSubtle)
        ColorUtils.setAlphaComponent(color, 88)
    }

    private val dp = itemView.resources.displayMetrics.density

    init {

        replyColumnDrawable.setColumnWidth((6 * dp).toInt())
        replyColumnDrawable.setColumnPaddingLeft((36.5 * dp).toInt())
        replyColumnDrawable.setColumnColor(colorReplyColumn)

        itemView.background = replyColumnDrawable

    }


    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATTwetchPostSecondaryLoadingItem


        if (model.isLoadingPostParent) {
            replyColumnDrawable.alpha = 255

            if (model.isLoadingFirstOfPostParents) {
                replyColumnDrawable.setColumnPaddingTop((30 * dp).toInt())
            } else {
                replyColumnDrawable.setColumnPaddingTop(0)
            }

        } else {
            replyColumnDrawable.alpha = 0
        }
    }



    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTwetchPostSecondaryLoadingViewHolder {
            return ATTwetchPostSecondaryLoadingViewHolder(
                inflater.inflate(
                    R.layout.li_twetch_post_secondary_loading,
                    parent,
                    false
                )
            )
        }
    }
}