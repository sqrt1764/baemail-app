package app.bitcoin.baemail.core.data.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
//import app.bitcoin.baemail.paymail.PaymailProvider
import app.bitcoin.baemail.core.data.room.TABLE_PAYMAILS

@Entity(tableName = TABLE_PAYMAILS)
data class Paymail(
    @PrimaryKey
    val paymail: String,
    val cypherSeedPhrase: String//,
//    val provider: PaymailProvider
)