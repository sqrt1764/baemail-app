package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.drawable.LayerDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.view.recycler.ATReplInput
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr

class ATTerminalInputViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATTerminalInputListener

    var model: ATReplInput? = null

    val codeText: TextView = itemView.findViewById(R.id.code_text)
    val line: TextView = itemView.findViewById(R.id.line)
    val actionCopy: Button = itemView.findViewById(R.id.action_copy)

    init {

        val strokeColor = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(itemView.context, id)
        }

        val strokeWidth = (0.6 * itemView.resources.displayMetrics.density).toInt()

        val strokePaddingSides = (4 * itemView.resources.displayMetrics.density).toInt()

        itemView.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(strokeColor)
                it.setStrokeWidths(0, 0, 0, strokeWidth)
                it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))


        actionCopy.setOnClickListener {
            val m = model ?: return@setOnClickListener

            listener.onCopyClick(m.code)
        }

    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATReplInput

        this.listener = listener!! as BaseListAdapter.ATTerminalInputListener

        codeText.text = model.code

        line.text = model.id
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTerminalInputViewHolder {
            return ATTerminalInputViewHolder(
                inflater.inflate(
                    R.layout.li_terminal_input,
                    parent,
                    false
                )
            )
        }
    }
}