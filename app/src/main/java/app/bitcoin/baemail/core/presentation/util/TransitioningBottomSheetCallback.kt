package app.bitcoin.baemail.core.presentation.util

import android.view.View
import androidx.activity.OnBackPressedCallback
import com.google.android.material.bottomsheet.BottomSheetBehavior
import timber.log.Timber

class TransitioningBottomSheetCallback(
    private val bottomSheetBehavior: BottomSheetBehavior<View>,
    private val bottomSheetContentExpanded: View,
    private val bottomSheetContentCollapsed: View,
    private val addOnBackPressedCallback: (OnBackPressedCallback)->Unit
) : BottomSheetBehavior.BottomSheetCallback()  {

    private val expandPercentBarrierA = 0.05f
    private val expandPercentBarrierB = 0.10f

    var collapsingBackPressedCallback: CollapsingBackPressedCallback? = null
    inner class CollapsingBackPressedCallback : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            Timber.d("collapsingBackPressedCallback .. handleOnBackPressed")
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

            remove()
            collapsingBackPressedCallback = null
        }

    }

    override fun onStateChanged(bottomSheet: View, newState: Int) {
        if (BottomSheetBehavior.STATE_EXPANDED == newState) {
            collapsingBackPressedCallback.let {
                if (it != null) return@let
                val cb = CollapsingBackPressedCallback()
                collapsingBackPressedCallback = cb
                addOnBackPressedCallback(cb)
            }

        } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
            collapsingBackPressedCallback?.remove()
            collapsingBackPressedCallback = null
        }
    }

    override fun onSlide(bottomSheet: View, slideOffset: Float) {
        //make `bottomSheetContentExpanded` visible as the bottom-sheet is expanded

        if (slideOffset < expandPercentBarrierA) {
            bottomSheetContentExpanded.translationZ = 0f
        } else {
            bottomSheetContentExpanded.translationZ = 10f
        }

        if (slideOffset < expandPercentBarrierA) {
            if (bottomSheetContentExpanded.alpha != 0f) {
                bottomSheetContentExpanded.alpha = 0f
            }
            if (bottomSheetContentCollapsed.alpha != 1f) {
                bottomSheetContentCollapsed.alpha = 1f
            }
        }

        if (slideOffset > expandPercentBarrierB) {
            if (bottomSheetContentExpanded.alpha != 1f) {
                bottomSheetContentExpanded.alpha = 1f
            }
            if (bottomSheetContentCollapsed.alpha != 0f) {
                bottomSheetContentCollapsed.alpha = 0f
            }
        }

        if (slideOffset > expandPercentBarrierB) {
            bottomSheetContentCollapsed.translationZ = 0f
        } else {
            bottomSheetContentCollapsed.translationZ = 10f
        }

        if (slideOffset > expandPercentBarrierA && slideOffset < expandPercentBarrierB) {
            val percentInBarrier = (slideOffset - expandPercentBarrierA) /
                    (expandPercentBarrierB - expandPercentBarrierA)

            bottomSheetContentExpanded.alpha = percentInBarrier
            bottomSheetContentCollapsed.alpha = 1 - percentInBarrier
        }
    }
}