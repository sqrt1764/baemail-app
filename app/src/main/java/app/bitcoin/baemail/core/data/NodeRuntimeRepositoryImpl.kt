package app.bitcoin.baemail.core.data

import android.net.wifi.p2p.WifiP2pInfo
import androidx.tracing.Trace
import app.bitcoin.baemail.core.data.util.Tr
import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.core.data.nodejs.request.CheckMnemonicIsValid
import app.bitcoin.baemail.core.data.nodejs.request.CreateBaemailMessageTx
import app.bitcoin.baemail.core.data.nodejs.request.CreateEqualSplitCoinTx
import app.bitcoin.baemail.core.data.nodejs.request.CreateSimpleOutputScriptTx
import app.bitcoin.baemail.core.data.nodejs.request.CreateSimpleTx
import app.bitcoin.baemail.core.data.nodejs.request.DecryptPkiEncryptedData
import app.bitcoin.baemail.core.data.nodejs.request.DeriveNextAddressSet
import app.bitcoin.baemail.core.data.nodejs.request.DerivedAddress
import app.bitcoin.baemail.core.data.nodejs.request.GetMoneyButtonPublicKeyFromSeed
import app.bitcoin.baemail.core.data.nodejs.request.GetPrivateKeyForPathFromSeed
import app.bitcoin.baemail.core.data.nodejs.request.InformAboutAvailableCoins
import app.bitcoin.baemail.core.data.nodejs.request.InformActivePaymailChanged
import app.bitcoin.baemail.core.data.nodejs.request.InformDiscoveredStallsChanged
import app.bitcoin.baemail.core.data.nodejs.request.InformNearbyP2pConnectionChanged
import app.bitcoin.baemail.core.data.nodejs.request.InformStallConfigChanged
import app.bitcoin.baemail.core.data.nodejs.request.InformWifiDirectP2pConnectionChanged
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.core.data.nodejs.request.SignMessage
import app.bitcoin.baemail.core.data.nodejs.request.SignMessageWithXpriv
import app.bitcoin.baemail.core.data.nodejs.request.StartStallSocketServer
import app.bitcoin.baemail.core.data.nodejs.request.StopStallSocketServer
import app.bitcoin.baemail.core.data.nodejs.request.TwetchAuth
import app.bitcoin.baemail.core.data.nodejs.request.TxInfoEqualSplit
import app.bitcoin.baemail.core.data.nodejs.request.TxInfoSendToAddress
import app.bitcoin.baemail.core.domain.entities.FundingCoinInfo
import app.bitcoin.baemail.core.domain.entities.ReceivedCoin
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.p2p.P2pStallRepository
import app.bitcoin.baemail.twetch.ui.TwetchAuthStore
import app.local.p2p.serviceDiscovery.DiscoveryModel
import app.local.p2p.serviceDiscovery.FoundStall
import app.local.p2p.serviceDiscovery.P2pStallRegistryHelper1
import com.google.gson.Gson
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.random.Random

class NodeRuntimeRepositoryImpl @Inject constructor(
    private val nodeRuntimeInterface: NodeRuntimeInterface,
    private val gson: Gson
) : NodeRuntimeRepository {

    private var isForegroundRequested = false
    private var isForegroundRequestedFromNode = false

    /*private */override fun startForegroundFromNode() {
        isForegroundRequestedFromNode = true

        if (nodeRuntimeInterface.isForeground) return
        nodeRuntimeInterface.startForeground()
    }

    /*private */override fun stopForegroundFromNode() {
        isForegroundRequestedFromNode = false

        if (isForegroundRequested) return
        nodeRuntimeInterface.stopForeground()
    }

    //todo this should be smarter... this API can be used from multiple places in the app &
    // they can end up interfering with each other, unintentionally stopping foreground while some
    // other place in the code still needs it
    override fun startForeground() {
        isForegroundRequested = true

        if (nodeRuntimeInterface.isForeground) return
        nodeRuntimeInterface.startForeground()
    }

    override fun stopForeground() {
        isForegroundRequested = false

        if (isForegroundRequestedFromNode) return
        nodeRuntimeInterface.stopForeground()
    }

    ///////////

    override suspend fun informActivePaymailChanged(
        paymail: String,
        seed: List<String>,
        pkiPath: String
    ) {
        //only proceed if the node-side is already started
        if (!nodeRuntimeInterface.isStarted) return
        if (nodeRuntimeInterface.isKilling) return

        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_INFORM_ACTIVE_PAYMAIL_CHANGED, id)

            val request = InformActivePaymailChanged(paymail, seed, pkiPath) {
                Trace.endAsyncSection(Tr.METHOD_INFORM_ACTIVE_PAYMAIL_CHANGED, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("informActivePaymailChanged got cancelled")
                    return@InformActivePaymailChanged
                }

                cancellableContinuation.resume(Unit)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun informCoinAvailabilityChanged(
        info: FundingCoinInfo
    ) {
        //only proceed if the node-side is already started
        if (!nodeRuntimeInterface.isStarted) return
        if (nodeRuntimeInterface.isKilling) return

        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_INFORM_COIN_AVAILABILITY_CHANGED, id)

            val request = InformAboutAvailableCoins(info) {
                Trace.endAsyncSection(Tr.METHOD_INFORM_COIN_AVAILABILITY_CHANGED, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("InformAboutAvailableCoins got cancelled")
                    return@InformAboutAvailableCoins
                }

                cancellableContinuation.resume(Unit)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun getMoneyButtonPublicKey(seed: List<String>): String? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_GET_MONEY_BUTTON_PUBLIC_KEY, id)

            val request = GetMoneyButtonPublicKeyFromSeed(seed) { publicKey ->
                Trace.endAsyncSection(Tr.METHOD_GET_MONEY_BUTTON_PUBLIC_KEY, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("getMoneyButtonPublicKey got cancelled")
                    return@GetMoneyButtonPublicKeyFromSeed
                }

                cancellableContinuation.resume(publicKey)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun getPrivateKeyForPathFromSeed(
        path: String,
        seed: List<String>
    ): GetPrivateKeyForPathFromSeed.PrivateKeyHolder? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_GET_PRIVATE_KEY_FOR_PATH_FROM_SEED, id)

            val request = GetPrivateKeyForPathFromSeed(path, seed) { holder ->
                Trace.endAsyncSection(Tr.METHOD_GET_PRIVATE_KEY_FOR_PATH_FROM_SEED, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("getPrivateKeyForPathFromSeed got cancelled")
                    return@GetPrivateKeyForPathFromSeed
                }

                cancellableContinuation.resume(holder)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun signMessageWithXpriv(
        xpriv: String,
        path: String,
        message: String
    ): String? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_SIGN_MESSAGE_WITH_XPRIV, id)

            val request = SignMessageWithXpriv(
                xpriv,
                path,
                message
            ) { signature ->
                Trace.endAsyncSection(Tr.METHOD_SIGN_MESSAGE_WITH_XPRIV, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("signMessageWithXpriv got cancelled")
                    return@SignMessageWithXpriv
                }

                cancellableContinuation.resume(signature)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun signMessage(
        message: String,
        wifPrivateKey: String
    ): String? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_SIGN_MESSAGE_WITH_PRIVATE_KEY, id)

            val request = SignMessage(message, wifPrivateKey) { signature ->
                Trace.endAsyncSection(Tr.METHOD_SIGN_MESSAGE_WITH_PRIVATE_KEY, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("signMessage got cancelled")
                    return@SignMessage
                }

                cancellableContinuation.resume(signature)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun checkMnemonic(seed: List<String>): Boolean {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_CHECK_MNEMONIC, id)

            val request = CheckMnemonicIsValid(seed) { isValid ->
                Trace.endAsyncSection(Tr.METHOD_CHECK_MNEMONIC, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("checkMnemonic got cancelled")
                    return@CheckMnemonicIsValid
                }

                cancellableContinuation.resume(isValid)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun deriveNextAddressSet(
        seed: List<String>,
        path: String,
        setIndex: Int
    ): List<DerivedAddress> {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_DERIVE_NEXT_ADDRESS_SET, id)

            val request = DeriveNextAddressSet(seed, path, setIndex, gson) { list ->
                Trace.endAsyncSection(Tr.METHOD_DERIVE_NEXT_ADDRESS_SET, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("deriveNextAddressSet got cancelled")
                    return@DeriveNextAddressSet
                }

                cancellableContinuation.resume(list!!)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun createSplitCoinTx(
        seed: List<String>,
        spendingCoins: Map<String, ReceivedCoin>,
        spendingCoinRootPath: String,
        destinationAddressInfo: List<Pair<String, Long>>
    ): TxInfoEqualSplit? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_CREATE_SPLIT_COIN_TX, id)

            val request = CreateEqualSplitCoinTx(
                seed,
                spendingCoins,
                spendingCoinRootPath,
                destinationAddressInfo,
                gson
            ) { txInfo ->
                Trace.endAsyncSection(Tr.METHOD_CREATE_SPLIT_COIN_TX, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("createSimpleTx got cancelled")
                    return@CreateEqualSplitCoinTx
                }

                cancellableContinuation.resume(txInfo)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun createSimpleTx(
        seed: List<String>,
        spendingCoins: Map<String, ReceivedCoin>,
        destinationAddress: String,
        sats: Long
    ): TxInfoSendToAddress? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_CREATE_SIMPLE_TX, id)

            val request = CreateSimpleTx(
                seed,
                spendingCoins,
                destinationAddress,
                sats,
                gson
            ) { txInfo ->
                Trace.endAsyncSection(Tr.METHOD_CREATE_SIMPLE_TX, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("createSimpleTx got cancelled")
                    return@CreateSimpleTx
                }

                cancellableContinuation.resume(txInfo)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun createSimpleOutputScriptTx(
        seed: List<String>,
        spendingCoins: Map<String, ReceivedCoin>,
        destinationList: List<CreateSimpleOutputScriptTx.Destination>
    ): CreateSimpleOutputScriptTx.TxInfo? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_CREATE_SIMPLE_OUTPUT_SCRIPT_TX, id)

            val request = CreateSimpleOutputScriptTx(
                seed,
                spendingCoins,
                destinationList,
                gson
            ) { txInfo ->
                Trace.endAsyncSection(Tr.METHOD_CREATE_SIMPLE_OUTPUT_SCRIPT_TX, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("createSimpleOutputScriptTx got cancelled")
                    return@CreateSimpleOutputScriptTx
                }

                cancellableContinuation.resume(txInfo)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun createBaemailMessageTx(
        sendingPaymail: CreateBaemailMessageTx.SendingPaymail,
        fundingWallet: CreateBaemailMessageTx.FundingWallet,
        message: CreateBaemailMessageTx.Message
    ): CreateBaemailMessageTx.Tx? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_ASSEMBLE_BAEMAIL_MESSAGE_TX, id)

            val request = CreateBaemailMessageTx(
                sendingPaymail,
                fundingWallet,
                message,
                gson
            ) { tx ->
                Trace.endAsyncSection(Tr.METHOD_ASSEMBLE_BAEMAIL_MESSAGE_TX, id)

                if (cancellableContinuation.isCancelled) {
                    Timber.d("createBaemailMessageTx got cancelled")
                    return@CreateBaemailMessageTx
                }

                cancellableContinuation.resume(tx)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun decryptPkiEncryptedData(
        seed: List<String>,
        pkiPath: String,
        mappedCypherText: Map<String, String>
    ): Map<String, String>? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_DECRYPT_PKI_ENCRYPTED_DATA, id)

            val request = DecryptPkiEncryptedData(
                seed,
                pkiPath,
                mappedCypherText,
                gson
            ) { decrypted ->

                Trace.endAsyncSection(Tr.METHOD_DECRYPT_PKI_ENCRYPTED_DATA, id)

                if (cancellableContinuation.isCancelled) {
                    Timber.d("createBaemailMessageTx got cancelled")
                    return@DecryptPkiEncryptedData
                }

                cancellableContinuation.resume(decrypted)
            }

            nodeRuntimeInterface.request(request)
        }
    }


    override suspend fun twetchAuth(
            paymail: String,
            isForced: Boolean
    ): TwetchAuthStore.AuthInfo? {
        return suspendCancellableCoroutine { cancellableContinuation ->

            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_TWETCH_AUTH, id)

            val request = TwetchAuth(paymail, isForced) { info ->

                Trace.endAsyncSection(Tr.METHOD_TWETCH_AUTH, id)

                if (cancellableContinuation.isCancelled) {
                    Timber.d("twetchAuth got cancelled")
                    return@TwetchAuth
                }

                cancellableContinuation.resume(info)
            }

            nodeRuntimeInterface.request(request)
        }
    }

    /**
     * Content is executed as async-lambda.
     * Use `return` keyword to return a value.
     * Be sure to always serialize the value before returning.
     * The content code has access to variable `context` which is injected in scope.
     *
     *
       val res = walletRepository.runAsyncScript("""
            //console.log("coin info:", context.availableCoins);
            await context.twetch.ensureSetupHasCompleted();
            let twetchMe = JSON.stringify(await context.twetch.instance.me());
            //console.log("twetch-me:", twetchMe);
            return twetchMe;
            """.trimIndent())
     *
     */
    override suspend fun runAsyncScript(content: String): RunScript.Response {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_RUN_ASYNC_SCRIPT, id)

            val request = RunScript(content, gson) { response ->
                Trace.endAsyncSection(Tr.METHOD_RUN_ASYNC_SCRIPT, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("runModule got cancelled")
                    return@RunScript
                }

                cancellableContinuation.resume(response)

            }

            nodeRuntimeInterface.request(request)

        }
    }



    override suspend fun startStallSocketServer(
        port: Int,
        config: P2pStallRepository.PeerServiceConfig
    ): Boolean {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_START_STALL_SOCKET_SERVER, id)

            val request = StartStallSocketServer(
                port,
                config
            ) { response ->
                Trace.endAsyncSection(Tr.METHOD_START_STALL_SOCKET_SERVER, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("startStallSocketServer got cancelled")
                    return@StartStallSocketServer
                }

                response?.let {
                    cancellableContinuation.resume(it)
                } ?: cancellableContinuation.resume(false)

            }

            nodeRuntimeInterface.request(request)

        }
    }

    override suspend fun stopStallSocketServer(): Boolean {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_STOP_STALL_SOCKET_SERVER, id)

            val request = StopStallSocketServer { response ->
                Trace.endAsyncSection(Tr.METHOD_STOP_STALL_SOCKET_SERVER, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("stopStallSocketServer got cancelled")
                    return@StopStallSocketServer
                }

                response?.let {
                    cancellableContinuation.resume(it)
                } ?: cancellableContinuation.resume(false)

            }

            nodeRuntimeInterface.request(request)
        }
    }


    override suspend fun informStallConfigChanged(
        config: P2pStallRepository.PeerServiceConfig
    ): Boolean {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_INFORM_STALL_CONFIG_CHANGED, id)

            val request = InformStallConfigChanged(
                config
            ) { response ->
                Trace.endAsyncSection(Tr.METHOD_INFORM_STALL_CONFIG_CHANGED, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("informStallConfigChanged got cancelled")
                    return@InformStallConfigChanged
                }

                response?.let {
                    cancellableContinuation.resume(it)
                } ?: cancellableContinuation.resume(false)

            }

            nodeRuntimeInterface.request(request)
        }
    }


    override suspend fun informDiscoveredStallsChanged(
        model: DiscoveryModel
    ): Boolean {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_INFORM_DISCOVERED_STALLS_CHANGED, id)

            val request = InformDiscoveredStallsChanged(
                model
            ) { response ->
                Trace.endAsyncSection(Tr.METHOD_INFORM_DISCOVERED_STALLS_CHANGED, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("informDiscoveredStallsChanged got cancelled")
                    return@InformDiscoveredStallsChanged
                }

                response?.let {
                    cancellableContinuation.resume(it)
                } ?: cancellableContinuation.resume(false)

            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun informNearbyP2pConnectionChange(
        isNew: Boolean,
        isOutgoing: Boolean,
        endpointId: String,
        meta: InformNearbyP2pConnectionChanged.EndpointMeta?
    ): Boolean {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_INFORM_NEARBY_P2P_CONNECTION_CHANGE, id)

            val request = InformNearbyP2pConnectionChanged(
                isNew,
                endpointId,
                isOutgoing,
                meta
            ) { response ->
                Trace.endAsyncSection(Tr.METHOD_INFORM_NEARBY_P2P_CONNECTION_CHANGE, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("InformNearbyP2pConnectionChange got cancelled")
                    return@InformNearbyP2pConnectionChanged
                }

                response?.let {
                    cancellableContinuation.resume(it)
                } ?: cancellableContinuation.resume(false)

            }

            nodeRuntimeInterface.request(request)
        }
    }

    override suspend fun informWifiDirectP2pConnectionChange(
        connectionAttempt: FoundStall?,
        groupInfo: WifiP2pInfo?,
        participants: Collection<P2pStallRegistryHelper1.DeviceStatus>?,
    ): Boolean {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            Trace.beginAsyncSection(Tr.METHOD_INFORM_WIFI_DIRECT_P2P_CONNECTION_CHANGE, id)

            val request = InformWifiDirectP2pConnectionChanged(
                connectionAttempt,
                groupInfo,
                participants
            ) { response ->
                Trace.endAsyncSection(Tr.METHOD_INFORM_WIFI_DIRECT_P2P_CONNECTION_CHANGE, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("InformNearbyP2pConnectionChange got cancelled")
                    return@InformWifiDirectP2pConnectionChanged
                }

                response?.let {
                    cancellableContinuation.resume(it)
                } ?: cancellableContinuation.resume(false)

            }

            nodeRuntimeInterface.request(request)
        }
    }





}