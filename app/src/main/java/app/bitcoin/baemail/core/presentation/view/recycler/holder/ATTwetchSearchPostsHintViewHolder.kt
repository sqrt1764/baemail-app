package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.Outline
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.TextView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchSearchPostsHintItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@FlowPreview
@ExperimentalCoroutinesApi
class ATTwetchSearchPostsHintViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    var model: ATTwetchSearchPostsHintItem? = null

    val hint: TextView = itemView.findViewById(R.id.hint)

    init {

        hint.clipToOutline = true
        hint.outlineProvider = object : ViewOutlineProvider() {

            val roundness = 12 * itemView.resources.displayMetrics.density

            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                outline.setRoundRect(0, 0, view.width, view.height, roundness)
            }

        }
    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATTwetchSearchPostsHintItem

        //hint.text = model.value
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTwetchSearchPostsHintViewHolder {
            return ATTwetchSearchPostsHintViewHolder(
                inflater.inflate(
                    R.layout.li_twetch_search_posts_hint,
                    parent,
                    false
                )
            )
        }
    }
}