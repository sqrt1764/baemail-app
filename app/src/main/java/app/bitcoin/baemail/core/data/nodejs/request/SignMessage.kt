package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.JsonArray
import timber.log.Timber
import java.lang.RuntimeException

class SignMessage(
    val message: String,
    val wifPrivateKey: String,
    val callback: (String?)->Unit
) : NodeRuntimeInterface.NodeRequest("SIGN_MESSAGE") {

    override fun getReqParams(): JsonArray {

        val finalArray = JsonArray()
        finalArray.add(message)
        finalArray.add(wifPrivateKey)

        return finalArray
    }


    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "message not signed; token: $token")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(stackTrace, "unexpected response format; token: $token")
            callback(null)
            return
        }

        try {
            callback(data.asString)

        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")
            callback(null)
        }
    }

}