package app.bitcoin.baemail.core.presentation.view.recycler

import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.core.presentation.view.recycler.lifecycle.LifecyclePagingAdapter

class BasePagingAdapter(
    lifecycle: Lifecycle,
    val helper: BaseAdapterHelper
) : LifecyclePagingAdapter<BaseViewHolder>() {

//    private var items: MutableList<AdapterType> = mutableListOf()

    lateinit var viewPool: AdapterTypeViewPool

    init {
        parentLifecycle = lifecycle
    }




    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val item = getItem(position) ?: throw RuntimeException() //todo will crash if placeholders are enabled

        holder.onBindViewHolder(item, helper.getListener(getItemViewType(position)))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return helper.createHolder(parent, viewType)
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position) ?: throw RuntimeException() //todo will crash if placeholders are enabled
        return item.type.ordinal
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
//        Timber.i("onAttachedToRecyclerView $recyclerView")

        if (!::viewPool.isInitialized) {
            viewPool = BaseListAdapter.findSharedViewPool(recyclerView)
        }
        recyclerView.setRecycledViewPool(viewPool)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
//        Timber.i("onDetachedFromRecyclerView $recyclerView")
    }


}