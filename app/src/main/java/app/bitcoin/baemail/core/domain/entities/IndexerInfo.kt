package app.bitcoin.baemail.core.domain.entities

data class IndexerInfo(
    val serverUrl: String = "",
    val account: String = "",
    val password: String = ""
)