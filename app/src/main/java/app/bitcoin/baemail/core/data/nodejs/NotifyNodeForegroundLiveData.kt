package app.bitcoin.baemail.core.data.nodejs

import androidx.lifecycle.MutableLiveData

class NotifyNodeForegroundLiveData : MutableLiveData<Boolean>() {
}