package app.bitcoin.baemail.core.data.wallet

import android.content.Context
import androidx.lifecycle.MutableLiveData
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.lang.RuntimeException

class AppStateLiveData(
    val appContext: Context,
    val coroutineUtil: CoroutineUtil
) : MutableLiveData<AppState>() {

    private val _state = MutableStateFlow<AppState?>(null)
    val state = _state.asStateFlow()

    init {
        coroutineUtil.appScope.launch(Dispatchers.IO) {
            appContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).let { prefs ->
                val activePaymail = prefs.getString(KEY_ACTIVE_PAYMAIL, null)
                val state = AppState(activePaymail)

                withContext(Dispatchers.Main) {
                    Timber.d("app-state initialized; activePaymail:$activePaymail")
                    doSetValue(state)
                }
            }
        }
    }

    override fun setValue(value: AppState?) {
        if (value == null) throw RuntimeException()

        coroutineUtil.appScope.launch(Dispatchers.IO) {
            appContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit().let { editor ->

                editor.putString(KEY_ACTIVE_PAYMAIL, value.activePaymail)

                editor.apply()
                Timber.d("app-state persisted")
            }
        }

        doSetValue(value)
    }

    private fun doSetValue(value: AppState) {
        _state.value = value
        super.setValue(value)
    }

    fun setActivePaymail(paymail: String) {
        val currentValue = value!!
        value = currentValue.copy(
            activePaymail = paymail
        )
    }

    companion object {
        private const val PREFS_NAME = "app-state"
        private const val KEY_ACTIVE_PAYMAIL = "activePaymail"
    }
}