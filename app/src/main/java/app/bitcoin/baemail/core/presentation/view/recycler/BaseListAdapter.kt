package app.bitcoin.baemail.core.presentation.view.recycler

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.nodejs.request.DerivedAddress
import app.bitcoin.baemail.core.presentation.view.recycler.lifecycle.LifecycleRecyclerAdapter
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.twetch.ui.*
import app.bitcoin.baemail.core.presentation.util.Event
import app.bitcoin.baemail.core.presentation.view.VideoClipManager
import app.bitcoin.baemail.core.presentation.view.recycler.holder.TabItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber
import java.io.File

@ExperimentalCoroutinesApi
@FlowPreview
class BaseListAdapter(
    lifecycle: Lifecycle,
    val helper: BaseAdapterHelper
) : LifecycleRecyclerAdapter<BaseViewHolder>() {

    private var inflater: LayoutInflater? = null

    private var items: MutableList<AdapterType> = mutableListOf()

    fun getCopyOfItems(): MutableList<AdapterType> {
        return ArrayList(items)
    }

    lateinit var viewPool: AdapterTypeViewPool

    init {
        parentLifecycle = lifecycle
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBindViewHolder(items[position], helper.getListener(getItemViewType(position)))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return helper.createHolder(parent, viewType)
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].type.ordinal
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun getInflater(context: Context): LayoutInflater {
        inflater?.let {
            return it
        }
        return LayoutInflater.from(context).let {
            inflater = it
            it
        }
    }

    fun setItems(list: List<AdapterType>) {
        val diffCallback = ListAdapterDiffCallback(items, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(list)

        diffResult.dispatchUpdatesTo(this)
    }


    fun swapOutItem(posToRemove: Int, replacementItem: AdapterType) {
        items.removeAt(posToRemove)
        notifyItemRemoved(posToRemove)
        items.add(posToRemove, replacementItem)
        notifyItemInserted(posToRemove)
    }


    fun getItemOfPosition(position: Int): AdapterType {
        return items[position]
    }


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
//        Timber.i("onAttachedToRecyclerView $recyclerView")

        if (!::viewPool.isInitialized) {
            viewPool = findSharedViewPool(recyclerView)
        }
        recyclerView.setRecycledViewPool(viewPool)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
//        Timber.i("onDetachedFromRecyclerView $recyclerView")
    }


    ///todo move to the helper
    interface Listener {}

    abstract class ATMessageListener : Listener {
        companion object {
            val ACTION_START_0 = 0
            val ACTION_START_1 = 1
            val ACTION_END_0 = 2
            val ACTION_END_1 = 3
            val ACTION_END_2 = 4

        }

        abstract fun onItemClicked(adapterPosition: Int, model: ATMessage0)
        abstract fun onActionClicked(adapterPosition: Int, model: ATMessage0, actionId: Int)
    }

    interface ATAuthenticatedPaymailListener : Listener {
        fun onClick(paymail: String)
    }

    interface ATFilterSeedWordListener : Listener {
        fun onClick(word: String)
    }

    interface ATSelectedSeedWordListener : Listener {
        fun onDismissClicked()
    }

    interface ATCoinListener : Listener {
        fun onClick(coin: Coin)
        fun onLongClick(coin: Coin)
    }

    interface ATUnusedAddressListener : Listener {
        fun onClick(coin: DerivedAddress)
    }

    interface ATContentLoadingListener : Listener {
        fun onBound()
    }

    //todo naming
    interface ATPaymentCarouselListener : Listener {
        fun onInfoClick(paymentId: String)
        fun onManageClick(paymentId: String)
    }

    interface ATTwetchChatInfoListener : Listener {
        fun onClick(chat: TwetchChatInfoStore.ChatInfo)
    }

    interface ATTwetchChatMessageMeListener : Listener {
        fun onProfileClicked(participant: TwetchChatInfoStore.Participant)
        fun onContentClicked(
            chatId: String,
            messageId: String,
            content: String,
            participant: TwetchChatInfoStore.Participant,
            dateLabel:String,
            millisCreatedAt: Long
        )
        fun onImageClicked(image: File)
    }

    interface ATTwetchChatMessageOtherListener : Listener {
        fun onProfileClicked(participant: TwetchChatInfoStore.Participant)
        fun onContentClicked(
            chatId: String,
            messageId: String,
            content: String,
            participant: TwetchChatInfoStore.Participant,
            dateLabel:String,
            millisCreatedAt: Long
        )
        fun onImageClicked(image: File)
    }

    interface ATTwetchPostListener : Listener {
        fun onContentClicked(holder: PostHolder, ofPost: PostModel)
        fun onUnloadedPostClicked(holder: PostHolder, ofPost: PostModel, txId: String)
        fun onImageClicked(holder: PostHolder, allImageUrlList: List<String>)
        fun onProfileClicked(holder: PostHolder, ofPost: PostModel)
        fun onBranchedLabelClicked(holder: PostHolder)
        fun onLikeClicked(holder: PostHolder, ofPost: PostModel): Boolean
        fun onCommentClicked(holder: PostHolder, ofPost: PostModel)
        fun onBranchClicked(holder: PostHolder, ofPost: PostModel)
        fun onCopyLinkClicked(holder: PostHolder, ofPost: PostModel)
        fun onCreatedLabelClicked(holder: PostHolder, ofPost: PostModel)
        fun onTweetClicked(holder: PostHolder, ofPost: PostModel)
        fun onUserIdBadgeClicked(holder: PostHolder, userId: String)
        fun getVideoClipManager(): VideoClipManager
        fun onVideoClipClicked(holder: PostHolder, videoTxId: String)
        fun getCoinFlipSuccessSignal(): StateFlow<Event<CoinFlipModel>?>
    }

    interface ATTwetchPostDetailsMainPostListener : Listener {
        fun onContentClicked(holder: PostHolder, ofPost: PostModel)
        fun onUnloadedPostClicked(holder: PostHolder, ofPost: PostModel, txId: String)
        fun onImageClicked(holder: PostHolder, allImageUrlList: List<String>)
        fun onProfileClicked(holder: PostHolder, ofPost: PostModel)
        fun onLikeClicked(holder: PostHolder, ofPost: PostModel): Boolean
        fun onCommentClicked(holder: PostHolder, ofPost: PostModel)
        fun onBranchClicked(holder: PostHolder, ofPost: PostModel)
        fun onCopyLinkClicked(holder: PostHolder, ofPost: PostModel)
        fun onCreatedLabelClicked(holder: PostHolder, ofPost: PostModel)
        fun onTweetClicked(holder: PostHolder, ofPost: PostModel)
        fun onViewLikesClicked(holder: PostHolder, ofPost: PostModel)
        fun onViewBranchesClicked(holder: PostHolder, ofPost: PostModel)
        fun onUserIdBadgeClicked(holder: PostHolder, userId: String)
        fun getVideoClipManager(): VideoClipManager
        fun onVideoClipClicked(holder: PostHolder, videoTxId: String)
        fun getCoinFlipSuccessSignal(): StateFlow<Event<CoinFlipModel>?>
    }

    interface ATTwetchNotificationListener : Listener {
        fun onContentClicked(notification: TwetchNotificationsStore.Notification)
        fun onActorClicked(notification: TwetchNotificationsStore.Notification)
        fun onPostClicked(notification: TwetchNotificationsStore.Notification)
        fun onPostUserClicked(notification: TwetchNotificationsStore.Notification)
        fun onPostTweetClicked(notification: TwetchNotificationsStore.Notification)
        fun onLikeClicked(notification: TwetchNotificationsStore.Notification): Boolean
        fun onCommentClicked(notification: TwetchNotificationsStore.Notification)
        fun onBranchClicked(notification: TwetchNotificationsStore.Notification)
        fun onCopyLinkClicked(notification: TwetchNotificationsStore.Notification)
        fun onUserIdBadgeClicked(notification: TwetchNotificationsStore.Notification, userId: String)
        fun onUnloadedPostClicked(notification: TwetchNotificationsStore.Notification, txId: String)
        fun onImageClicked(notification: TwetchNotificationsStore.Notification, allImageUrlList: List<String>)
        fun getVideoClipManager(): VideoClipManager
        fun onVideoClipClicked(notification: TwetchNotificationsStore.Notification, videoTxId: String)
        fun getCoinFlipSuccessSignal(): StateFlow<Event<CoinFlipModel>?>
    }

    interface ATTwetchChatParticipantsListener : Listener {
        fun onProfileClicked(participant: TwetchChatInfoStore.Participant)
    }

    interface ATTwetchUserProfileBaseListener : Listener {
        fun onUserIconClicked(url: String)
        fun onFollowUserClicked(model: ATTwetchUserProfileRootItem): Boolean
        fun onUnfollowUserClicked(model: ATTwetchUserProfileRootItem): Boolean
    }

    interface ATTwetchTabsListener: Listener {
        fun onBound(id: String): List<TabItem>
        fun onTabClicked(id: String, tab: TabItem): List<TabItem>
        fun onAttached(holder: RecyclerView.ViewHolder)
        fun onDetached()
    }

    interface ATP2pStallListener : Listener {
        fun onItemClick(id: String)
    }

    interface ATTerminalInputListener : Listener {
        fun onCopyClick(code: String)
    }

    interface ATTerminalReturnListener : Listener {
        fun onCopyClick(code: String)
    }

    interface ATTerminalOutputActionListener : Listener {
        fun onToggleCollapsed()
        fun getToggleCollapsedLabel(): String
        fun onClear()
        fun onFullLog()
        fun onFullLogLong()
        fun onSaved()
    }

    interface ATSavedReplInputListener : Listener {
        fun onCopy(model: ATSavedReplInput)
        fun onRemove(model: ATSavedReplInput)
    }

    companion object {

        fun findSharedViewPool(view: View): AdapterTypeViewPool {
            var v: View = view
            while (true) {
                val p = v.parent
                if (p is View) {
                    v = p
                } else {
                    break
                }
            }

            var foundPool: AdapterTypeViewPool? = null
            v.findViewById<View>(R.id.root).let {
                if (it == null) return@let

                if (it.tag is AdapterTypeViewPool) {
                    foundPool = it.tag as AdapterTypeViewPool
                } else {
                    Timber.i("shared-view-pool not found; instantiating a personal one")
                    val pool = AdapterTypeViewPool()
                    it.tag = pool
                    foundPool = pool
                }
            }

            return if (foundPool == null) {
                Timber.e(
                    RuntimeException(),
                    "view with R.id.root not found; instantiating a personal one"
                )
                AdapterTypeViewPool()
            } else {
                foundPool!!
            }
        }
    }

}