package app.bitcoin.baemail.core.presentation.view

import android.content.Context
import android.graphics.Outline
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.view.ViewOutlineProvider
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import app.bitcoin.baemail.R
import com.google.android.material.snackbar.BaseTransientBottomBar


class ErrorSnackBar private constructor(
    context: Context,
    parent: ViewGroup,
    content: View,
    contentValue: String
) : BaseTransientBottomBar<ErrorSnackBar>(
    context,
    parent,
    content,
    object : com.google.android.material.snackbar.ContentViewCallback {
        override fun animateContentIn(delay: Int, duration: Int) {
            content.scaleY = 0f
            ViewCompat.animate(content).scaleY(1f)
                .setDuration(duration.toLong())
                .setStartDelay(delay.toLong())
        }

        override fun animateContentOut(delay: Int, duration: Int) {
            content.scaleY = 1f
            ViewCompat.animate(content).scaleY(0f)
                .setDuration(duration.toLong())
                .setStartDelay(delay.toLong())
        }

    }
) {

    companion object {

        fun make(
            context: Context,
            parent: ViewGroup,
            contentValue: String
        ): ErrorSnackBar {


            val content: View = LayoutInflater.from(context)
                .inflate(R.layout.view_snackbar_content, parent, false)

            content.layoutParams = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            ).also {
                it.gravity = Gravity.BOTTOM
            }

            val dp = context.resources.displayMetrics.density

            content.clipToOutline = true
            content.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    view ?: return
                    outline ?: return

                    outline.setRoundRect(0, 0, view.width, view.height, 12 * dp)
                }

            }


            return ErrorSnackBar(context, parent, content, contentValue)
        }


    }


    private var info: TextView
    private var action: Button


    init {
        val dp = context.resources.displayMetrics.density
        view.setBackgroundColor(
            ContextCompat.getColor(
                view.context,
                android.R.color.transparent
            )
        )
        view.setPadding(
            (16 * dp).toInt(),
            0,
            (16 * dp).toInt(),
            (8 * dp).toInt()
        )
        info = view.findViewById(R.id.info)
        action = view.findViewById(R.id.action)
        action.visibility = View.GONE
        info.text = contentValue
        view.fitsSystemWindows = false
        ViewCompat.setOnApplyWindowInsetsListener(view, null)
        view.layoutParams.let { lp ->
            if (lp is MarginLayoutParams) {
                lp.leftMargin = 0
                lp.topMargin = 0
                lp.rightMargin = 0
                lp.bottomMargin = 0
            }
        }
    }





}