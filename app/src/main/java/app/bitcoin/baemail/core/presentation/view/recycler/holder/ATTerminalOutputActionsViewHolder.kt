package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATReplOutputActions
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder

class ATTerminalOutputActionsViewHolder(
        itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATTerminalOutputActionListener

    var model: ATReplOutputActions? = null

    val toggleCollapse: TextView = itemView.findViewById(R.id.toggle_collapse)
    val clear: TextView = itemView.findViewById(R.id.clear)
    val fullLog: Button = itemView.findViewById(R.id.full_log)
    val saved: Button = itemView.findViewById(R.id.saved)

    init {

//        val strokeColor = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
//        val colorSelector = R.color.selector_on_light.let { id ->
//            ContextCompat.getColor(itemView.context, id)
//        }
//
//        val strokeWidth = R.dimen.li_filter_seed_word__stroke_width.let { id ->
//            itemView.resources.getDimensionPixelSize(id)
//        }
//
//        val strokePaddingSides = R.dimen.li_filter_seed_word__stroke_padding_sides.let { id ->
//            itemView.resources.getDimensionPixelSize(id)
//        }

        toggleCollapse.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onToggleCollapsed()
            toggleCollapse.text = this.listener.getToggleCollapsedLabel()
        }

        clear.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onClear()
        }

        fullLog.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onFullLog()
        }

        fullLog.setOnLongClickListener {
            listener.onFullLogLong()
            true
        }

        saved.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onSaved()
        }


    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATReplOutputActions

        this.listener = listener!! as BaseListAdapter.ATTerminalOutputActionListener

        toggleCollapse.text = this.listener.getToggleCollapsedLabel()
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTerminalOutputActionsViewHolder {
            return ATTerminalOutputActionsViewHolder(
                    inflater.inflate(
                            R.layout.li_terminal_output_actions,
                            parent,
                            false
                    )
            )
        }
    }
}