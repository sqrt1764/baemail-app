package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber

/*
# expected structure of params
[
    (1),
    (2)
]

(1) possible values: 'new', 'lost'

(2) multiple possible structures;
when 'new':
{
    endpointId: 'ABC0',
    isOutgoing: true,
    millisStarted: 1234567890,
    paymail: 'aa@yy.zz',
    pki: '00aaff11',
    title: 'stall 01'
}

when 'lost':
{
    endpointId: 'ABC0',
    isOutgoing: true
}


# expected structure of the response data passed in the callback
[
    true
]
 */
class InformNearbyP2pConnectionChanged(
    val isNew: Boolean,
    val endpointId: String,
    val isOutgoing: Boolean,
    val meta: EndpointMeta?,
    val callback: (Boolean?)->Unit
) : NodeRuntimeInterface.NodeRequest("INFORM_NEARBY_P2P_CONNECTION_CHANGED") {




    override fun getReqParams(): JsonArray {
        val metaJson = JsonObject()
        if (isNew) {
            meta!!
            metaJson.addProperty("name", meta.name)
            metaJson.addProperty("endpointId", endpointId)
            metaJson.addProperty("isOutgoing", isOutgoing)
            metaJson.addProperty("millisStarted", meta.millisStarted)
            metaJson.addProperty("paymail", meta.paymail)
            metaJson.addProperty("pki", meta.pki)
            metaJson.addProperty("title", meta.title)
        } else {
            metaJson.addProperty("endpointId", endpointId)
            metaJson.addProperty("isOutgoing", isOutgoing)
        }


        val finalArray = JsonArray()
        if (isNew) {
            finalArray.add("new")
        } else {
            finalArray.add("lost")
        }
        finalArray.add(metaJson)

        return finalArray
    }

    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "failed to deliver nearby-connection-change; token: $token")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(stackTrace, "unexpected response format; token: $token")
            callback(null)
            return
        }

        try {
            callback(data[0].asBoolean)

        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")
            callback(null)
        }
    }






    data class EndpointMeta(
        val name: String,
        val millisStarted: Long,
        val paymail: String,
        val pki: String,
        val title: String
    )
}