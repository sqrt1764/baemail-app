package app.bitcoin.baemail.core.data.wallet

import androidx.lifecycle.*
import androidx.lifecycle.Observer
import app.bitcoin.baemail.core.data.nodejs.CoinRefreshRequestHelper
import app.bitcoin.baemail.core.data.nodejs.NotifyNodeForegroundLiveData
import app.bitcoin.baemail.core.data.util.AuthenticatedPaymail
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.data.util.AppForegroundLiveData
import app.bitcoin.baemail.core.data.util.ConnectivityLiveData
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber
import java.util.*
import javax.inject.Provider

class DynamicChainSync(
    private val appStateLD: AppStateLiveData,
    private val appForegroundLD: AppForegroundLiveData,
    private val connectivityLD: ConnectivityLiveData,
    private val notifyNodeForegroundLiveData: NotifyNodeForegroundLiveData,
    private val coinRefreshRequestHelper: CoinRefreshRequestHelper,
    private val coroutineUtil: CoroutineUtil,
    private val secureDataSource: SecureDataSource,
    private val chainSyncProvider: Provider<ChainSync>
) {

    companion object {
        private const val MILLIS_KILL_DELAY = 1000 * 45L

        private const val INVALID_PAYMAIL = "invalid"

    }

    //if this is not NULL when observing is requested because either the app is in the foreground, or it is forced
    private var chainSync: MutableStateFlow<ChainSync?> = MutableStateFlow(null)

    val status = chainSync.flatMapLatest { chainSync ->
        chainSync ?: return@flatMapLatest flow { emit(ChainSyncStatus.DISCONNECTED) }
        chainSync.status
    }.stateIn(
        scope = coroutineUtil.appScope,
        started = SharingStarted.Eagerly,
        initialValue = ChainSyncStatus.DISCONNECTED
    )

    val highestUsedAddressRef: StateFlow<AddressRef?> = chainSync.flatMapLatest { chainSync ->
        chainSync ?: return@flatMapLatest flow { emit(null) }
        chainSync.fullSyncHelper.highestUsedAddressRef
    }.stateIn(
        scope = coroutineUtil.appScope,
        started = SharingStarted.Eagerly,
        initialValue = null
    )

    val statusLD: LiveData<ChainSyncStatus>
        get() = status.asLiveData()

//    //////////
//    //todo make sure this gets triggered when when unspent coins changes
//    //todo
//    //todo
//    //todo
//    //todo
//    private val _newCoinsDetectedLD = MediatorLiveData<Unit>()
//    val newCoinsDetectedLD: LiveData<Unit>
//        get() = _newCoinsDetectedLD



    private var shouldIgnoreNotForeground = false

    private var initializedForPaymail = INVALID_PAYMAIL
    val paymail: String?
        get() {
            return initializedForPaymail.let {
                if (INVALID_PAYMAIL == it) null else it
            }
        }


    private val runningLD = MediatorLiveData<Unit>()






    private var killJob: Job? = null


    val fullSyncHelper: WocFullSyncHelper? get() = chainSync.value?.fullSyncHelper

    val liveBlockInfo: Flow<BlockInfo?> = chainSync.flatMapLatest { chainSync ->
        chainSync ?: return@flatMapLatest flow<BlockInfo?> { emit(null) }
        chainSync.liveBlockInfo
    }

    fun getLiveBlockInfo() = chainSync.value?.liveBlockInfo?.value

    fun getRealBlock() = BlockInfo(
            659311,
            1604175885L,
            "0000000000000000054ae6e15c579e3f04dd6d73445878952b28029298b42aa4",
            "533bc72eb1e8a3a9c1eb74b61c45e179deefd52bfc0b2454af87aeb078c2493f",
            "00000000000000000527d29c79cfb00d17eee38503871c1060aed42bc637cf4e"
    )

    fun calculateCurrentBlockHeight(): Int {

        //return from sync
        getLiveBlockInfo()?.let {
            return it.height
        }

        val realBlock = getRealBlock()

        val calculatedCurrentBlockHeight = realBlock.let {
            val c = Calendar.getInstance()
            val millisBlockPeriod = 1000L * 60 * 10

            val millisSinceRealBlock = c.timeInMillis - realBlock.time
            val blocksSinceRealBlock = millisSinceRealBlock / millisBlockPeriod

            realBlock.height + blocksSinceRealBlock
        }.toInt()


        return calculatedCurrentBlockHeight
    }

    fun calculateTimeOfBlockHeight(blockHeight: Int): Long {
        val info = getLiveBlockInfo() ?: getRealBlock()

        val deltaOfHeight = blockHeight - info.height
        val millisBlockPeriod = 1000L * 60 * 10
        val millisOHeightDelta = deltaOfHeight * millisBlockPeriod

        return info.time + millisOHeightDelta
    }

    fun calculateBlockHeightOfTime(millis: Long): Int {
        val info = getLiveBlockInfo() ?: getRealBlock()

        val millisBlockPeriod = 1000L * 60 * 10

        val millisBlockTimeDelta = millis - info.time
        val blocksInDelta = millisBlockTimeDelta / millisBlockPeriod

        return (info.height + blocksInDelta).toInt()
    }


    fun considerRetry() {
        if (ChainSyncStatus.ERROR != status.value) {
            Timber.d("dropped an attempt to #considerRetry; current status:${status.value}")
            return
        }
        refreshRunningState()
    }


    private fun isAppForeground(): Boolean {
        if (shouldIgnoreNotForeground) return true
        return appForegroundLD.value ?: false
    }




    private fun refreshRunningState() {
        coroutineUtil.appScope.launch(coroutineUtil.mainDispatcher) {
            doRefreshRunningState()
        }
    }

    private suspend fun doRefreshRunningState() {
        val activePaymail = appStateLD.value?.activePaymail ?: INVALID_PAYMAIL
        val isConnected = connectivityLD.value?.isConnected ?: false
        val isAppForeground = isAppForeground()

        val isRunning = chainSync.value != null && ChainSyncStatus.ERROR != status.value

        Timber.d("""
            #refreshRunningState
            activePaymail: $activePaymail
            isConnected: $isConnected
            isAppForeground: $isAppForeground
            shouldIgnoreNotForeground: $shouldIgnoreNotForeground
            sync currently running: $isRunning
        """.trimIndent())


        val shouldBeRunning = isConnected && isAppForeground && activePaymail != INVALID_PAYMAIL
        val shouldBeRestarted = activePaymail != initializedForPaymail
        val shouldBeCleanedUp = !isConnected





        if (shouldBeCleanedUp) {
            if (isRunning) {
                if (isDisconnectScheduled()) cancelDisconnect()

                Timber.d("chain-sync is running & should be stopped & cleaned up; doing it now")
                doStop()
            } else {
                Timber.d("chain-sync should be be stopped & cleaned up; it is not running; nothing to do")
            }

        } else if (shouldBeRunning) {
            if (isRunning) {
                if (isDisconnectScheduled()) cancelDisconnect()

                if (shouldBeRestarted) {
                    Timber.d("chain-sync is running & should be restarted; doing it now")
                    doStop()
                    doStart()

                } else {
                    Timber.d("chain-sync should be running and is; nothing to do")
                }

            } else { //not running
                Timber.d("chain-sync should be running and is not; doing it now")
                if (chainSync.value != null) {
                    doStop()
                }
                doStart()
            }

        } else { //should stop
            if (isRunning) {
                Timber.d("chain-sync is running, but should stop; scheduling now")
                scheduleDisconnect()
            } else {
                Timber.d("chain-sync is NOT running and should not; nothing to do")
            }

        }


    }


    fun setup() {

        notifyNodeForegroundLiveData.observeForever {
            it ?: return@observeForever
            shouldIgnoreNotForeground = it

            refreshRunningState()
        }





        runningLD.addSource(appForegroundLD, object : Observer<Boolean> {
            override fun onChanged(value: Boolean) {
                refreshRunningState()
            }
        })

        runningLD.addSource(appStateLD, object : Observer<AppState> {

            private var lastPaymail: String? = "invalid"

            override fun onChanged(value: AppState) {
                val currentPaymail = value.activePaymail

                if (currentPaymail == lastPaymail) {
                    //relevant state not changed
                    return
                }
                lastPaymail = currentPaymail

                refreshRunningState()
            }
        })

        runningLD.addSource(connectivityLD, object : Observer<ConnectivityLiveData.Model> {
            var lastIsConnected = false

            override fun onChanged(value: ConnectivityLiveData.Model) {
                val currentIsConnected = value.isConnected

                if (currentIsConnected == lastIsConnected) {
                    //relevant state not changed
                    return
                }

                lastIsConnected = currentIsConnected

                Timber.d("internet-connectivity status: $currentIsConnected ...refreshing")
                refreshRunningState()

            }

        })


        runningLD.observeForever {
            refreshRunningState()
        }

    }


    private fun isDisconnectScheduled(): Boolean {
        return killJob != null
    }

    private fun cancelDisconnect() {
        Timber.d("cancelDisconnect")
        killJob?.cancel()
        killJob = null
    }

    private fun scheduleDisconnect() {
        Timber.d("scheduleDisconnect")
        killJob ?: let {
            killJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
                Timber.d("scheduleDisconnect kill-job launched")
                delay(MILLIS_KILL_DELAY)
                if (!isActive) {
                    Timber.d("`killJob` job has been cancelled")
                    return@launch
                }

                doStop()
                killJob = null
            }
        }
    }




    private fun doStart() {
        if (chainSync.value != null) {
            Timber.d("dropped #doStart; already exists")
            return
        }

        Timber.d("#doStart")

        val paymail = appStateLD.value?.activePaymail ?: let {
             Timber.e(RuntimeException("cannot sync at this moment"))
            return
        }

        initializedForPaymail = paymail

        val sync = chainSyncProvider.get()
        chainSync.value = sync

        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            val fundingWalletSeed = try {
                secureDataSource.getParallelWalletSeed(AuthenticatedPaymail(paymail))
            } catch (e: Exception) {
                Timber.e(e, "wallet not found; dropping sync-request")
                return@launch
            }
            val fundingWalletPath = try {
                secureDataSource.getPathForFundingMnemonic(AuthenticatedPaymail(paymail))
            } catch (e: Exception) {
                Timber.e(e, "wallet-path not found; dropping sync-request")
                return@launch
            }

            sync.initialize(
                paymail,
                fundingWalletSeed,
                fundingWalletPath,
                coinRefreshRequestHelper.requestChannel.receiveAsFlow()
            )
        }
    }



    private suspend fun doStop() {
        Timber.d("#doStop")


        withContext(coroutineUtil.mainDispatcher) {
            chainSync.value?.disconnect()
            chainSync.value = null
        }

    }





}