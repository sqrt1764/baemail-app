package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATContentLoading0
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@FlowPreview
@ExperimentalCoroutinesApi
class ATContentLoadingViewHolder(
    view: View
): BaseViewHolder(view) {

    private lateinit var listener: BaseListAdapter.ATContentLoadingListener

    var model: ATContentLoading0? = null

    init {
        itemView.setOnClickListener { listener.onBound() }


        itemView.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                itemView.post {
                    listener.onBound()
                }
            }

            override fun onViewDetachedFromWindow(v: View) {
                //do nothing
            }

        })


    }



    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATContentLoading0

        this.listener = listener!! as BaseListAdapter.ATContentLoadingListener

    }



    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATContentLoadingViewHolder {
            return ATContentLoadingViewHolder(
                inflater.inflate(
                    R.layout.li_content_loading,
                    parent,
                    false
                )
            )
        }
    }
}