package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.data.wallet.NotifyCoinsUsedByUserLiveData
import app.bitcoin.baemail.core.domain.entities.FundingCoinInfo
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.domain.repository.BsvStatusRepository
import app.bitcoin.baemail.core.domain.repository.CoinRepository
import app.bitcoin.baemail.core.domain.repository.SecureDataRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull
import timber.log.Timber

class CoinInfoUseCaseImpl(
    private val coinSyncStatusUseCase: CoinSyncStatusUseCase,
    private val secureDataRepository: SecureDataRepository,
    private val getFundingCoinsUseCase: GetFundingCoinsUseCase,
    private val coinRepository: CoinRepository,
    private val notifyCoinsUsedByUserLiveData: NotifyCoinsUsedByUserLiveData,
    private val bsvStatusRepository: BsvStatusRepository, //todo include tx-fee info
    private val authRepository: AuthRepository
): CoinInfoUseCase {
    override suspend fun invoke(): Flow<FundingCoinInfo> =
        authRepository.activePaymail.mapNotNull { it?.paymail }
            .combine(notifyCoinsUsedByUserLiveData.state) { paymail, millisCoinsUsed ->
                paymail to millisCoinsUsed
            }.combine(coinSyncStatusUseCase()) { (paymail, millisCoinsUsed), syncStatus ->
                Triple(paymail, millisCoinsUsed, syncStatus)
            }.mapNotNull { (paymail, _, syncStatus) ->
                val fundingWalletSeed = try {
                    secureDataRepository.getFundingWalletSeed(paymail)
                } catch (e: Exception) {
                    Timber.e(e, "error retrieving seed")
                    return@mapNotNull null
                }

                val unspentFundingCoins = getFundingCoinsUseCase().first()

                val upcomingChangeAddressList = try {
                    coinRepository.getUpcomingUnusedAddresses(paymail, 5).first()
                } catch (e: Exception) {
                    Timber.e(e, "not ready")
                    return@mapNotNull null
                }

                val fundingPath = try {
                    secureDataRepository.getPathForFundingMnemonic(paymail)
                } catch (e: Exception) {
                    Timber.e(e, "error retrieving funding path")
                    return@mapNotNull null
                }

                FundingCoinInfo(
                    paymail,
                    syncStatus,
                    fundingWalletSeed,
                    fundingPath,
                    unspentFundingCoins,
                    upcomingChangeAddressList
                )
            }

}

interface CoinInfoUseCase {
    suspend operator fun invoke(): Flow<FundingCoinInfo>
}