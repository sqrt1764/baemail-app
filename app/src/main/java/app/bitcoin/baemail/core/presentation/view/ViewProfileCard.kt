package app.bitcoin.baemail.core.presentation.view

import android.content.Context
import android.graphics.Outline
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.ViewCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.util.match
import kotlin.math.min

class ViewProfileCard : ConstraintLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private val dp1: Float

    private lateinit var image: ImageView
    private lateinit var name: TextView
    private lateinit var mail: TextView

    private var valueModel = Model(
        "unset-name",
        "unset-mail"
    )

    init {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.view_profile_card, this, false)
        val set = ConstraintSet()
        view.id = ViewCompat.generateViewId()
        addView(view)

        set.clone(this)
        set.match(view, this)

        dp1 = context.resources.displayMetrics.density


        image = view.findViewById(R.id.image)
        name = view.findViewById(R.id.name)
        mail = view.findViewById(R.id.mail)


        image.clipToOutline = true
        image.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val roundness = min(view.width, view.height)

                outline.setRoundRect(0, 0, view.width, view.height, roundness.toFloat())
            }
        }

        refresh()

    }

    private fun refresh() {
        name.text = valueModel.name
        mail.text = valueModel.mail
    }

    fun applyModel(model: Model) {
        valueModel = model

        refresh()
    }






    data class Model(
        val name: String,
        val mail: String
    )


}