package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.domain.entities.InfoModel
import app.bitcoin.baemail.core.domain.repository.SecureDataRepository
import kotlinx.coroutines.flow.Flow

class GetSecretsUseCaseImpl(
    val secureDataRepository: SecureDataRepository
) : GetSecretsUseCase {
    override fun invoke(): Flow<InfoModel?> = secureDataRepository.getWalletInfo()

}

interface GetSecretsUseCase {
    operator fun invoke(): Flow<InfoModel?>
}