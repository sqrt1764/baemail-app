package app.bitcoin.baemail.core.data.wallet

enum class ChainSyncStatus(val const: String) {
    DISCONNECTED("disconnected"),
    INITIALISING("initialising"),
    LIVE("live"),
    ERROR("error")
}