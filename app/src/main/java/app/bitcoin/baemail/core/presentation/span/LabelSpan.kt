package app.bitcoin.baemail.core.presentation.span

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.text.style.LineHeightSpan
import android.text.style.ReplacementSpan
import kotlin.math.roundToInt

class LabelSpan(
        private val labelContent: String,
        private val backgroundColor: Int,
        private val textColor: Int,
        private val sidePadding: Float = 16f,
        private val radius: Float? = null,
        private val backgroundVerticalInsetPx: Float = 0f
) : ReplacementSpan() {

    override fun getSize(paint: Paint, text: CharSequence?, start: Int, end: Int, fm: Paint.FontMetricsInt?): Int {
        return (sidePadding + paint.measureText(labelContent) + sidePadding).toInt()
    }

    override fun draw(
            canvas: Canvas,
            text: CharSequence?,
            start: Int,
            end: Int,
            x: Float,
            top: Int,
            y: Int,
            bottom: Int,
            paint: Paint
    ) {
        paint.color = backgroundColor

        val width = paint.measureText(labelContent)
        val height = paint.fontMetrics.descent - paint.fontMetrics.ascent + 2 * sidePadding

        if(radius != null) {
            val rect = RectF(
                    x,
                    top.toFloat() + backgroundVerticalInsetPx,
                    x + width + 2 * sidePadding,
                    bottom.toFloat() - backgroundVerticalInsetPx
            )
            canvas.drawRoundRect(rect, radius, radius, paint)
        } else {
            canvas.drawCircle(x + ((width + 2 * sidePadding) / 2), ((bottom - top) / 2).toFloat(), height / 2, paint)
        }


        val finalTextY = bottom.toFloat() - ((bottom - top - paint.textSize + paint.descent()) / 2f)


        paint.color = textColor
        canvas.drawText(labelContent, x + sidePadding, finalTextY, paint)
    }

//    override fun chooseHeight(
//            text: CharSequence?,
//            start: Int,
//            end: Int,
//            spanstartv: Int,
//            gotLineHeight: Int,
//            gotFm: Paint.FontMetricsInt?
//    ) {
//        lineHeight ?: return
//        gotFm?.let { fm ->
//            val originHeight = fm.descent - fm.ascent
//            // If original height is not positive, do nothing.
//            if (originHeight <= 0 || originHeight == lineHeight) {
//                return
//            }
//            val ratio: Float = lineHeight * 1.0f / originHeight
//            fm.descent = (fm.descent * ratio).roundToInt()
//            fm.ascent = fm.descent - lineHeight
//        }
//
//    }
}