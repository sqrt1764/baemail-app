package app.bitcoin.baemail.core.data.nodejs

import kotlinx.coroutines.channels.Channel

class CoinRefreshRequestHelper {

    val requestChannel = Channel<Request>()

    suspend fun enqueueRefresh(coinsToCheck: List<Request>) {
        coinsToCheck.forEach {
            requestChannel.send(it)
        }
    }

    data class Request(
        val paymail: String,
        val coinTxId: String,
        val coinAddress: String,
        val coinOutputIndex: Int
    )
}