package app.bitcoin.baemail.core.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import app.bitcoin.baemail.core.data.room.converters.DbConverters
import app.bitcoin.baemail.core.data.room.dao.BlockSyncInfoDao
import app.bitcoin.baemail.core.data.room.dao.CachedDerivedAddressDao
import app.bitcoin.baemail.core.data.room.dao.CoinDao
import app.bitcoin.baemail.core.data.room.dao.DecryptedBaemailMessageDao
import app.bitcoin.baemail.core.data.room.entity.BlockSyncInfo
import app.bitcoin.baemail.core.data.room.entity.CachedDerivedAddress
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage

@Database(entities = [
    Coin::class,
    BlockSyncInfo::class,
    DecryptedBaemailMessage::class,
    CachedDerivedAddress::class
], version = 6)
@TypeConverters(DbConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun coinDao(): CoinDao

    abstract fun blockSyncInfoDao(): BlockSyncInfoDao

    abstract fun decryptedBaemailMessageDao(): DecryptedBaemailMessageDao

    abstract fun cachedDerivedAddressDao(): CachedDerivedAddressDao
}