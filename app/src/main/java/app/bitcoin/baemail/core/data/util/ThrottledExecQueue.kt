package app.bitcoin.baemail.core.data.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class ThrottledExecQueue(
    private val requestsPerSecond: Int,
    private val scope: CoroutineScope
) {

    private val channel = Channel<Model<*>>(Channel.UNLIMITED)

    fun start() {
        scope.launch {
            var millisStart = -1L
            var count = 0
            channel.consumeAsFlow().collect {

                val millisNow = System.currentTimeMillis()

                if (millisStart < 0 || millisStart + 1111L < millisNow) {
                    millisStart = millisNow
                    count = 1
                } else if (count == requestsPerSecond) {
                    delay(millisStart + 1111L - millisNow)
                    millisStart = System.currentTimeMillis()
                    count = 1
                } else {
                    count++
                }


                it.resume()

            }
        }
    }

    suspend fun <T> execute(code: suspend () -> T): T {
        val result = suspendCoroutine { continuation ->
            channel.trySend(Model(code, continuation))
        }

        return result
    }

}

data class Model<T>(
    val code: suspend () -> T,
    val continuation: Continuation<T>
) {
    suspend fun resume() {
        continuation.resume(code())
    }
}