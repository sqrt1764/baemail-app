package app.bitcoin.baemail.core.presentation

import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowInsets
import android.widget.FrameLayout
import android.widget.HorizontalScrollView
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.NavHostFragment
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.entity.CentralTab
import app.bitcoin.baemail.core.presentation.view.ViewButtonBar
import app.bitcoin.baemail.message.data.entity.MessageBucket
import app.bitcoin.baemail.message.data.entity.SortType
import app.bitcoin.baemail.message.presentation.MessagesFragment
import app.bitcoin.baemail.core.presentation.util.FragmentStateHelper
import app.bitcoin.baemail.core.presentation.util.TransitioningBottomSheetCallback
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.util.isDarkModeOn
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.progressindicator.LinearProgressIndicator
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class CentralActivity: AppCompatActivity(), HasAndroidInjector {

    companion object {
        private const val STATE_HELPER = "helper"
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant

    private lateinit var viewModel: CentralViewModel

    private lateinit var root: ConstraintLayout

    private lateinit var statusBarBg: View
    private lateinit var navigationBarBg: View

    private lateinit var coordinator: CoordinatorLayout
    private lateinit var bottomSheet: ConstraintLayout
    private lateinit var bottomSheetContentExpanded: FrameLayout
    private lateinit var bottomSheetContentCollapsed: FrameLayout
    private lateinit var syncingBar: LinearProgressIndicator

    private lateinit var paymailLabel: TextView

    private lateinit var fragmentHelper: FragmentStateHelper
    private var navHost: NavHostFragment? = null

    private lateinit var expandView: ImageView
    private lateinit var tabBar: ViewButtonBar

    private lateinit var bottomSheetCallback: BottomSheetBehavior.BottomSheetCallback


    private val colorPrimary: Int by lazy {
        getColorFromAttr(R.attr.colorPrimary)
    }
    private val colorDanger: Int by lazy {
        getColorFromAttr(R.attr.colorDanger)
    }

    private val drawableTopStroke = DrawableStroke()

    val dp: Float by lazy {
        resources.displayMetrics.density
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        WindowCompat.setDecorFitsSystemWindows(window, false)


        viewModel = ViewModelProvider(
            this,
            savedStateViewModelFactoryAssistant.create(this)
        )[CentralViewModel::class.java]


        fragmentHelper = FragmentStateHelper(supportFragmentManager)

        //

        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE)


        if (!resources.isDarkModeOn()) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        if (!resources.isDarkModeOn() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }

        setContentView(R.layout.activity_central)

        //

        root = findViewById(R.id.root)
        statusBarBg = findViewById(R.id.status_bar_bg)
        navigationBarBg = findViewById(R.id.navigation_bar_bg)
        tabBar = findViewById(R.id.tab_bar)
        expandView = findViewById(R.id.bottom_sheet_content_collapsed_info_hint)
        paymailLabel = findViewById(R.id.bottom_sheet_content_collapsed_info_label)

        coordinator = findViewById(R.id.coordinator)
        bottomSheet = findViewById(R.id.bottom_sheet)
        bottomSheetContentExpanded = findViewById(R.id.bottom_sheet_content_expanded)
        bottomSheetContentCollapsed = findViewById(R.id.bottom_sheet_content_collapsed)
        syncingBar = findViewById(R.id.syncing_bar)


        root.doOnApplyWindowInsets { v, windowInsets, _ ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                v.updatePadding(
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                v.updatePadding(
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight,
                )
            }

            windowInsets
        }
        statusBarBg.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.layoutParams.let {
                it.height = windowInsets.systemWindowInsetTop.let { top ->
                    if (top == 0) {
                        view.visibility = View.GONE
                        1
                    } else {
                        view.visibility = View.VISIBLE
                        top
                    }
                }

                view.layoutParams = it
            }

            windowInsets
        }
        navigationBarBg.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.layoutParams.let {
                it.height = windowInsets.systemWindowInsetBottom.let { bottom ->
                    if (bottom == 0) {
                        view.visibility = View.GONE
                        1
                    } else {
                        view.visibility = View.VISIBLE
                        bottom
                    }
                }

                view.layoutParams = it
            }

            windowInsets
        }

        tabBar.tabClickListener = { view, id ->
            id as CentralTab

            viewModel.onTabClicked(id)

            scrollToViewOfBar(view, tabBar)
        }

        setupBottomSheet()

        if (savedInstanceState != null) {
            val helperState = savedInstanceState.getBundle(STATE_HELPER)!!
            fragmentHelper.restoreHelperState(helperState)

        } else {
            bottomSheetCallback.onSlide(bottomSheet, 0f)
            bottomSheetCallback.onStateChanged(bottomSheet, BottomSheetBehavior.STATE_COLLAPSED)
        }


        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                var activeTab: Int = -1
                var isRestoringState = savedInstanceState != null

                viewModel.tabBarModel.collectLatest { (tabList, millisUpdated) ->
                    if (tabList.isEmpty()) return@collectLatest

                    val previous = activeTab

                    val remapped = tabList.mapIndexed { i, tab ->
                        if (tab.isSelected) {
                            activeTab = i
                        }

                        ViewButtonBar.Model(
                            tab.tab,
                            getString(tab.tab.labelResource),
                            ContextCompat.getDrawable(this@CentralActivity, tab.tab.iconResource)!!,
                            ContextCompat.getColor(this@CentralActivity, tab.colorId),
                            tab.isSelected
                        )
                    }

                    if (isRestoringState) {
                        //allow fragment-restoring to execute normally
                        isRestoringState = false
                        tabBar.init(remapped)
                        return@collectLatest
                    }

                    val host = navHost
                    if (previous == activeTab && host != null) {
                        //todo navHost.withCreated {  }
                        val navController = host.navController
                        //the user is already seeing this tab
                        //bring the user to the start of the nav-graph that is showing
                        val currentDestinationId = navController.currentDestination?.id
                        val startDestinationId = navController.graph.startDestinationId

                        if (currentDestinationId == startDestinationId) {
                            //the user is already looking at the start destination
                            return@collectLatest
                        }

                        navController.popBackStack(startDestinationId, false)

                        return@collectLatest
                    }

                    //
                    //init the tabs & the content of this fragment
                    tabBar.init(remapped)



                    //
                    //
                    //handle the nav-graph
                    //

                    //save the state of the current tab
                    if (previous != -1 && host != null) {
                        fragmentHelper.saveState(host, tabList[previous].tab.name)
                    }

                    //restore the state of the new tab if it had any
                    val tab = tabList[activeTab].tab
                    val graphResource = when (tab) {
                        CentralTab.P2P -> R.navigation.nav_p2p
                        CentralTab.TWETCH -> R.navigation.nav_twetch
                        CentralTab.PAYMENTS -> R.navigation.nav_payments
                        CentralTab.BAEMAIL -> R.navigation.nav_inbox
                        CentralTab.REPL -> R.navigation.nav_repl
                        else -> throw RuntimeException()
                    }


                    //todo when access to this becomes required deeper in the nav-controller;
                    // switch to using nav-graph-view-model in `NavHostFragment`

                    val args = Bundle().also { args ->
                        val bucket = when (tab) {
                            CentralTab.BAEMAIL -> MessageBucket.INBOX
                            else -> null
                        }

                        bucket?.let {
                            args.putString(MessagesFragment.KEY_BUCKET, it.const)

                            if (MessageBucket.SENT == it) {
                                args.putString(MessagesFragment.KEY_INITIAL_SORT, SortType.TIME.const)
                                args.putBoolean(MessagesFragment.KEY_IS_SORT_LOCKED, true)
                            } else {
                                args.putString(MessagesFragment.KEY_INITIAL_SORT, SortType.VALUE.const)
                                args.putBoolean(MessagesFragment.KEY_IS_SORT_LOCKED, false)
                            }
                        }
                    }

                    val f = NavHostFragment.create(graphResource, args)
                    fragmentHelper.restoreState(f, tabList[activeTab].tab.name)

                    supportFragmentManager.beginTransaction()
                        .replace(R.id.content, f)
                        .setPrimaryNavigationFragment(f)
                        .commitNowAllowingStateLoss()

                    navHost = f

                }
            }
        }


        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.content.collectLatest {
                    paymailLabel.text = it.paymail
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.coinManagementUseCase.state.collectLatest { model ->
                    if (ChainSyncStatus.INITIALISING == model.syncStatus) {
                        syncingBar.visibility = View.VISIBLE
                    } else {
                        syncingBar.visibility = View.INVISIBLE
                    }

                    if (ChainSyncStatus.DISCONNECTED == model.syncStatus
                        || ChainSyncStatus.ERROR == model.syncStatus
                    ) {
                        drawableTopStroke.setStrokeColor(colorDanger)
                    } else {
                        drawableTopStroke.setStrokeColor(colorPrimary)
                    }

                }
            }
        }

    }

    private var tabBarScrollingJob: Job? = null

    private fun scrollToViewOfBar(child: View, scrollView: HorizontalScrollView) {
        tabBarScrollingJob?.cancel()

        tabBarScrollingJob = lifecycleScope.launch {
            delay(300)
            val distanceFromLeft = child.left - scrollView.scrollX
            val mustScrollAmount = distanceFromLeft - dp * 150
            scrollView.smoothScrollBy(mustScrollAmount.toInt(), 0)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        val bottomSheetLP = bottomSheet.layoutParams as CoordinatorLayout.LayoutParams
        val bottomSheetBehavior = bottomSheetLP.behavior as BottomSheetBehavior
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetCallback.onSlide(bottomSheet, 1f)
            bottomSheetCallback.onStateChanged(bottomSheet, BottomSheetBehavior.STATE_EXPANDED)

        } else {
            bottomSheetCallback.onSlide(bottomSheet, 0f)
            bottomSheetCallback.onStateChanged(bottomSheet, BottomSheetBehavior.STATE_COLLAPSED)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        // Make sure we save the current tab's state too!


        navHost?.let {
            //save current state
            val tabModel = viewModel.tabBarModel.value.first
                .find { tabModel -> tabModel.isSelected }
            val tab = tabModel?.tab ?: return@let

            fragmentHelper.saveState(it, tab.name)
        }

        if (::fragmentHelper.isInitialized) {
            outState.putBundle(STATE_HELPER, fragmentHelper.saveHelperState())
        }

    }

    private fun setupBottomSheet() {
        val bottomSheetLP = bottomSheet.layoutParams as CoordinatorLayout.LayoutParams
        val bottomSheetBehavior = bottomSheetLP.behavior as BottomSheetBehavior
        bottomSheetBehavior.isGestureInsetBottomIgnored = true

        val dWidth = (3 * dp).toInt()
        drawableTopStroke.setPadding(0, 0, 0, 0)
        drawableTopStroke.setStrokeColor(colorPrimary)
        drawableTopStroke.setStrokeWidths(0, dWidth, 0, 0)
        bottomSheetContentCollapsed.background = drawableTopStroke

        bottomSheetCallback = TransitioningBottomSheetCallback(
            bottomSheetBehavior,
            bottomSheetContentExpanded,
            bottomSheetContentCollapsed
        ) { callback ->
            onBackPressedDispatcher.addCallback(this@CentralActivity, callback)
        }

        bottomSheetBehavior.addBottomSheetCallback(bottomSheetCallback)

        expandView.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }
}