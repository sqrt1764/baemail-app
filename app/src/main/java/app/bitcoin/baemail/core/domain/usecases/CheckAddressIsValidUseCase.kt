package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.data.util.BitcoinAddressUtils

class CheckAddressIsValidUseCaseImpl : CheckAddressIsValidUseCase {
    override suspend fun invoke(value: String): Boolean {
        return BitcoinAddressUtils.isRegularAddress(value)
    }
}

interface CheckAddressIsValidUseCase {
    suspend operator fun invoke(value: String): Boolean
}