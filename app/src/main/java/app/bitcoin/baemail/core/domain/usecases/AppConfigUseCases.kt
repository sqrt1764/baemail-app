package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.BuildConfig
import app.bitcoin.baemail.core.domain.repository.AppConfigRepository
import app.bitcoin.baemail.core.domain.entities.IndexerInfo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AppConfigUseCasesImpl(
    private val appConfigRepository: AppConfigRepository,
    private val scope: CoroutineScope
) : AppConfigUseCases {

    override val defaultIndexerInfo = IndexerInfo(
        BuildConfig.BITCOIN_INDEXER_URL,
        BuildConfig.BITCOIN_INDEXER_ACCOUNT,
        BuildConfig.BITCOIN_INDEXER_PASSWORD
    )



    override suspend fun getIndexerInfoConfig(): IndexerInfo {
        return withContext(scope.coroutineContext) {
            IndexerInfo(
                appConfigRepository.getIndexerUrl() ?: "",
                appConfigRepository.getIndexerAccount() ?: "",
                appConfigRepository.getIndexerPassword() ?: ""
            )
        }
    }

    //TODO it would probably be a good idea to force-restart node runtime so it uses the new config
    override suspend fun updateIndexerInfoConfig(info: IndexerInfo) {
        scope.launch {
            appConfigRepository.updateIndexerUrl(info.serverUrl)
            appConfigRepository.updateIndexerAccount(info.account)
            appConfigRepository.updateIndexerPassword(info.password)
        }.join()
    }

}

interface AppConfigUseCases {
    val defaultIndexerInfo: IndexerInfo
    suspend fun getIndexerInfoConfig(): IndexerInfo
    suspend fun updateIndexerInfoConfig(info: IndexerInfo)
}