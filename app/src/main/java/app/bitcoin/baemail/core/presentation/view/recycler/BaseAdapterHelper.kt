package app.bitcoin.baemail.core.presentation.view.recycler

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATCoinViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATContentLoadingViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATFilterSeedNoMatchViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATFilterSeedWordViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATHeightDecorDynamicViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATMessage0ViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATNoPaymailsAddedViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATP2pStallItemViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATPaymentItemViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATSavedReplInfoViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATSavedReplInputViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATSectionTitleViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATSeedWordHintViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATSelectedSeedWordViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTerminalInputViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTerminalOutputActionsViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTerminalOutputViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTerminalReturnViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchChatMessageMeViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchChatMessageOtherViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchChatParticipantViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchChatViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchNoPostsFoundViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchNotificationViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchPostDetailMainViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchPostLoadingViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchPostSecondaryLoadingViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchPostViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchSearchPostsHintViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchSeparatorViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchTabsViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchUserProfileRootViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATUnusedAddressViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.AuthenticatedPaymailViewHolder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
class BaseAdapterHelper(
    val factory: BaseListAdapterFactory,
    val atMessageListener: BaseListAdapter.ATMessageListener? = null,
    val atFilterSeedWordListener: BaseListAdapter.ATFilterSeedWordListener? = null,
    val atSelectedSeedWordListener: BaseListAdapter.ATSelectedSeedWordListener? = null,
    val atAuthenticatedPaymailListener: BaseListAdapter.ATAuthenticatedPaymailListener? = null,
    val atCoinListener: BaseListAdapter.ATCoinListener? = null,
    val atUnusedAddressListener: BaseListAdapter.ATUnusedAddressListener? = null,
    val atContentLoadingListener: BaseListAdapter.ATContentLoadingListener? = null,
    val atPaymentCarouselListener: BaseListAdapter.ATPaymentCarouselListener? = null,
    val atTwetchChatInfoListener: BaseListAdapter.ATTwetchChatInfoListener? = null,
    val atTerminalInputListener : BaseListAdapter.ATTerminalInputListener? = null,
    val atTerminalReturnListener : BaseListAdapter.ATTerminalReturnListener? = null,
    val atTerminalOutputActionsListener: BaseListAdapter.ATTerminalOutputActionListener? = null,
    val atReplSavedInputListener: BaseListAdapter.ATSavedReplInputListener? = null,
    val atTwetchChatMessageMeListener: BaseListAdapter.ATTwetchChatMessageMeListener? = null,
    val atTwetchChatMessageOtherListener: BaseListAdapter.ATTwetchChatMessageOtherListener? = null,
    val atTwetchChatParticipantsListener: BaseListAdapter.ATTwetchChatParticipantsListener? = null,
    val atTwetchUserProfileBaseListener: BaseListAdapter.ATTwetchUserProfileBaseListener? = null,
    val atTwetchTabsListener: BaseListAdapter.ATTwetchTabsListener? = null,
    val atTwetchPostListener: BaseListAdapter.ATTwetchPostListener? = null,
    val atTwetchPostDetailsMainPostListener: BaseListAdapter.ATTwetchPostDetailsMainPostListener? = null,
    val atTwetchNotificationListener: BaseListAdapter.ATTwetchNotificationListener? = null,
    val atP2pStallListener: BaseListAdapter.ATP2pStallListener? = null
) {

    private var inflater: LayoutInflater? = null

    fun getListener(viewType: Int): BaseListAdapter.Listener? {
        return when (viewType) {
            AdapterTypeEnum.MESSAGE_0.ordinal -> {
                atMessageListener
            }
            AdapterTypeEnum.AUTHENTICATED_PAYMAIL.ordinal -> {
                atAuthenticatedPaymailListener
            }
            AdapterTypeEnum.SELECTED_SEED_WORD.ordinal -> {
                atSelectedSeedWordListener
            }
            AdapterTypeEnum.FILTER_SEED_WORD.ordinal -> {
                atFilterSeedWordListener
            }
            AdapterTypeEnum.COIN_0.ordinal -> {
                atCoinListener
            }
            AdapterTypeEnum.UNUSED_ADDRESS_0.ordinal -> {
                atUnusedAddressListener
            }
            AdapterTypeEnum.CONTENT_LOADING_0.ordinal -> {
                atContentLoadingListener
            }
            AdapterTypeEnum.PAYMENT_ITEM.ordinal -> {
                atPaymentCarouselListener
            }
            AdapterTypeEnum.TWETCH_CHAT_LIST_ITEM.ordinal -> {
                atTwetchChatInfoListener
            }
            AdapterTypeEnum.REPL_INPUT.ordinal -> {
                atTerminalInputListener
            }
            AdapterTypeEnum.REPL_RETURN.ordinal -> {
                atTerminalReturnListener
            }
            AdapterTypeEnum.REPL_OUTPUT_ACTIONS.ordinal -> {
                atTerminalOutputActionsListener
            }
            AdapterTypeEnum.SAVED_REPL_INPUT.ordinal -> {
                atReplSavedInputListener
            }
            AdapterTypeEnum.TWETCH_CHAT_MESSAGE_ME.ordinal -> {
                atTwetchChatMessageMeListener
            }
            AdapterTypeEnum.TWETCH_CHAT_MESSAGE_OTHER.ordinal -> {
                atTwetchChatMessageOtherListener
            }
            AdapterTypeEnum.TWETCH_CHAT_PARTICIPANT.ordinal -> {
                atTwetchChatParticipantsListener
            }
            AdapterTypeEnum.TWETCH_USER_PROFILE_ROOT.ordinal -> {
                atTwetchUserProfileBaseListener
            }
            AdapterTypeEnum.TWETCH_TABS.ordinal -> {
                atTwetchTabsListener
            }
            AdapterTypeEnum.TWETCH_POST.ordinal -> {
                atTwetchPostListener
            }
            AdapterTypeEnum.TWETCH_POST_DETAIL_MAIN.ordinal -> {
                atTwetchPostDetailsMainPostListener
            }
            AdapterTypeEnum.TWETCH_NOTIFICATION.ordinal -> {
                atTwetchNotificationListener
            }
            AdapterTypeEnum.P2P_STALL.ordinal -> {
                atP2pStallListener
            }
            else -> null
        }
    }

    fun createHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            AdapterTypeEnum.MESSAGE_0.ordinal -> {
                ATMessage0ViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.NO_PAYMAILS_ADDED.ordinal -> {
                ATNoPaymailsAddedViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.AUTHENTICATED_PAYMAIL.ordinal -> {
                AuthenticatedPaymailViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.SELECTED_SEED_WORD.ordinal -> {
                ATSelectedSeedWordViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.SEED_WORD_HINT.ordinal -> {
                ATSeedWordHintViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.FILTER_SEED_WORD.ordinal -> {
                ATFilterSeedWordViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.FILTER_SEED_NO_MATCH.ordinal -> {
                ATFilterSeedNoMatchViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.HEIGHT_DECOR_DYNAMIC.ordinal -> {
                ATHeightDecorDynamicViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.COIN_0.ordinal -> {
                ATCoinViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.UNUSED_ADDRESS_0.ordinal -> {
                ATUnusedAddressViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.CONTENT_LOADING_0.ordinal -> {
                ATContentLoadingViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.SECTION_TITLE_0.ordinal -> {
                ATSectionTitleViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.PAYMENT_ITEM.ordinal -> {
                ATPaymentItemViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_CHAT_LIST_ITEM.ordinal -> {
                ATTwetchChatViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_CHAT_PARTICIPANT.ordinal -> {
                ATTwetchChatParticipantViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_CHAT_MESSAGE_ME.ordinal -> {
                ATTwetchChatMessageMeViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_CHAT_MESSAGE_OTHER.ordinal -> {
                ATTwetchChatMessageOtherViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_USER_PROFILE_ROOT.ordinal -> {
                ATTwetchUserProfileRootViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_POST.ordinal -> {
                ATTwetchPostViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_POST_DETAIL_MAIN.ordinal -> {
                ATTwetchPostDetailMainViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_TABS.ordinal -> {
                ATTwetchTabsViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_SEARCH_POSTS_HINT.ordinal -> {
                ATTwetchSearchPostsHintViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_NO_POSTS_FOUND.ordinal -> {
                ATTwetchNoPostsFoundViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_POST_SECONDARY_LOADING.ordinal -> {
                ATTwetchPostSecondaryLoadingViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_POST_LOADING.ordinal -> {
                ATTwetchPostLoadingViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_SEPARATOR_0.ordinal -> {
                ATTwetchSeparatorViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.TWETCH_NOTIFICATION.ordinal -> {
                ATTwetchNotificationViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.REPL_INPUT.ordinal -> {
                ATTerminalInputViewHolder.create(
                        factory,
                        getInflater(parent.context),
                        parent
                )
            }
            AdapterTypeEnum.REPL_OUTPUT.ordinal -> {
                ATTerminalOutputViewHolder.create(
                        factory,
                        getInflater(parent.context),
                        parent
                )
            }
            AdapterTypeEnum.REPL_RETURN.ordinal -> {
                ATTerminalReturnViewHolder.create(
                        factory,
                        getInflater(parent.context),
                        parent
                )
            }
            AdapterTypeEnum.REPL_OUTPUT_ACTIONS.ordinal -> {
                ATTerminalOutputActionsViewHolder.create(
                        factory,
                        getInflater(parent.context),
                        parent
                )
            }
            AdapterTypeEnum.SAVED_REPL_INPUT.ordinal -> {
                ATSavedReplInputViewHolder.create(
                        factory,
                        getInflater(parent.context),
                        parent
                )
            }
            AdapterTypeEnum.SAVED_REPL_INFO.ordinal -> {
                ATSavedReplInfoViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.P2P_STALL.ordinal -> {
                ATP2pStallItemViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            else -> throw RuntimeException()
        }
    }


    private fun getInflater(context: Context): LayoutInflater {
        inflater?.let {
            return it
        }
        return LayoutInflater.from(context).let {
            inflater = it
            it
        }
    }

}