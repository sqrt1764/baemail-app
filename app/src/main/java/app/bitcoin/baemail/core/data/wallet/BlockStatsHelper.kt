package app.bitcoin.baemail.core.data.wallet

import app.bitcoin.baemail.core.data.util.CoroutineUtil
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class BlockStatsHelper(
    private val appUtilDiskDAO: AppUtilDiskDAO,
    private val blockchainDataSource: BlockchainDataSource,
    private val coroutineUtil: CoroutineUtil,
    private val chainSync: DynamicChainSync,
    private val gson: Gson
) {

    companion object {
        val HARDCODED = BlockStatsFee(
            793264,
            13,
            242
        )
    }

    private val _state = MutableStateFlow(
        Model(
            State.CACHED,
            HARDCODED
        )
    )

    val state: StateFlow<Model>
        get() = _state.asStateFlow()


    fun setup() {
        coroutineUtil.appScope.launch {
            delay(1337)

            initializeFromDisk()

            chainSync.status.collectLatest { syncStatus ->
                if (ChainSyncStatus.LIVE != syncStatus) return@collectLatest
                if (State.FRESH == state.value.state) return@collectLatest

                val latestBlockHeight = chainSync.getLiveBlockInfo()!!.height
                val txFee = blockchainDataSource.fetchBlockStatsFee(latestBlockHeight)
                    ?: return@collectLatest

                _state.value = Model(
                    State.FRESH,
                    txFee
                )
                persistToDisk(txFee)
            }
        }
    }

    private suspend fun initializeFromDisk() {
        appUtilDiskDAO.readBlockStatsFee()?.let {
            _state.value = Model(
                State.CACHED,
                it
            )
        }
    }

    private suspend fun persistToDisk(txFee: BlockStatsFee) {
        appUtilDiskDAO.writeBlockStatsFee(txFee)
    }

    enum class State {
        CACHED,
        FRESH
    }

    data class Model(
        val state: State,
        val txFee: BlockStatsFee
    )
}

data class BlockStatsFee(
    @SerializedName("blockHeight") val blockHeight: Int,
    @SerializedName("medianFeeSats") val medianFeeSats: Int,
    @SerializedName("medianTxSizeBytes") val medianTxSizeBytes: Int
) {
    val satsPerByte: Float =
        if (medianTxSizeBytes == 0) 0.1f else medianFeeSats / medianTxSizeBytes.toFloat()
}