package app.bitcoin.baemail.core.domain.repository

import app.bitcoin.baemail.core.domain.entities.InfoModel
import kotlinx.coroutines.flow.Flow

interface SecureDataRepository {
    suspend fun getFundingWalletSeed(paymail: String): List<String>
    suspend fun getPaymailWalletSeed(paymail: String): List<String>

    suspend fun getPathForPaymailMnemonic(paymail: String): String
    suspend fun getPathForFundingMnemonic(paymail: String): String

    fun getWalletInfo(): Flow<InfoModel?>
}