package app.bitcoin.baemail.core.data.wallet.sending

data class PaymailInfo(
    val alias: BsvAlias,
    val supportsProfile: Boolean,
    val supportsP2p: Boolean
)