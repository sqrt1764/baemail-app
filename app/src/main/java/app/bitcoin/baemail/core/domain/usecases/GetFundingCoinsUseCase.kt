package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.domain.repository.CoinRepository
import app.bitcoin.baemail.core.data.room.entity.Coin
import kotlinx.coroutines.flow.Flow

class GetFundingCoinsUseCaseImpl(
    private val coinRepository: CoinRepository
) : GetFundingCoinsUseCase {
    override fun invoke(): Flow<List<Coin>> = coinRepository.getFundingCoins()

}

interface GetFundingCoinsUseCase {
    operator fun invoke(): Flow<List<Coin>>
}