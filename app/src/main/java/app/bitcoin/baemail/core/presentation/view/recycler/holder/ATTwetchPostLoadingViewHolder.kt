package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchPostLoadingItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder

class ATTwetchPostLoadingViewHolder(
    view: View
): BaseViewHolder(view) {

    var model: ATTwetchPostLoadingItem? = null

    init {
    }


    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATTwetchPostLoadingItem

    }



    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTwetchPostLoadingViewHolder {
            return ATTwetchPostLoadingViewHolder(
                inflater.inflate(
                    R.layout.li_twetch_post_loading,
                    parent,
                    false
                )
            )
        }
    }
}