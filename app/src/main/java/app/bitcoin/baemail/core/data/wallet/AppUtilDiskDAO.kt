package app.bitcoin.baemail.core.data.wallet

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first

class AppUtilDiskDAO(
    private val appUtilPrefs: DataStore<Preferences>,
    private val gson: Gson
) {

    suspend fun readHighestUsedCoin(paymail: String): Int {
        val prefs = appUtilPrefs.data.first()
        prefs[stringPreferencesKey("last_used_coin_address_state")]?.let { serializedState ->
            val json = gson.fromJson(serializedState, JsonObject::class.java)
            if (json.has(paymail)) {
                return json.get(paymail).asInt
            } else {
                return 0
            }
        }
        return 0
    }

    suspend fun readHighestUsedCoins(): Map<String, MutableStateFlow<Int>> {
        val parsed: MutableMap<String, MutableStateFlow<Int>> = mutableMapOf()

        val prefs = appUtilPrefs.data.first()
        prefs[stringPreferencesKey("last_used_coin_address_state")]?.let { serializedState ->
            val json = gson.fromJson(serializedState, JsonObject::class.java)
            json.keySet().forEach { paymail ->
                val addressIndex = json.get(paymail).asInt
                parsed[paymail] = MutableStateFlow(addressIndex)
            }
        }

        return parsed
    }

    suspend fun writeHighestUsedCoins(highestUsedAddressMap: Map<String, MutableStateFlow<Int>>) {
        appUtilPrefs.edit { prefs ->
            val json = JsonObject()
            highestUsedAddressMap.entries.forEach { (paymail, addressIndex) ->
                json.addProperty(paymail, addressIndex.value)
            }

            prefs[stringPreferencesKey("last_used_coin_address_state")] = gson.toJson(json)
        }
    }

    suspend fun writeSyncConfigExtraSetsToCheckForGaps(paymail: String, extraSets: Int) {
        appUtilPrefs.edit { prefs ->
            prefs[intPreferencesKey("extra_sets_to_check_for_gaps__$paymail")] = extraSets
        }
    }

    suspend fun readSyncConfigExtraSetsToCheckForGaps(paymail: String): Int {
        return appUtilPrefs.data
            .first()[intPreferencesKey("extra_sets_to_check_for_gaps__$paymail")] ?: 2
    }

    suspend fun readBlockStatsFee(): BlockStatsFee? {
        val prefs = appUtilPrefs.data.first()
        return prefs[stringPreferencesKey("block_stats_fee")]?.let { serializedState ->
            gson.fromJson(serializedState, BlockStatsFee::class.java)
        }
    }

    suspend fun writeBlockStatsFee(txFee: BlockStatsFee) {
        appUtilPrefs.edit { prefs ->
            prefs[stringPreferencesKey("block_stats_fee")] = gson.toJson(txFee)
        }
    }

    suspend fun readExchangeRate(): ExchangeRate? {
        val prefs = appUtilPrefs.data.first()
        val serializedState = prefs[stringPreferencesKey("usd_exchange_rate")]
        return serializedState?.let {
            gson.fromJson(it, ExchangeRate::class.java)
        }
    }

    suspend fun writeExchangeRate(rate: ExchangeRate) {
        appUtilPrefs.edit { prefs ->
            prefs[stringPreferencesKey("usd_exchange_rate")] = gson.toJson(rate)
        }
    }


}