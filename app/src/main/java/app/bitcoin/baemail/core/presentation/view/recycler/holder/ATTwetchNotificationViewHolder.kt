package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.text.util.LinkifyCompat
import androidx.lifecycle.lifecycleScope
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableRoundedBitmap
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.twetch.ui.PostModel
import app.bitcoin.baemail.twetch.ui.TwetchNotificationsStore
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ViewTwetchTweetCard
import app.bitcoin.baemail.core.presentation.view.ViewVideoClip
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchNotificationItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.SpanType
import coil.load
import coil.size.Precision
import coil.target.ImageViewTarget
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.button.MaterialButton
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.textview.MaterialTextView
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.receiveAsFlow
import timber.log.Timber
import kotlin.math.min

class ATTwetchNotificationViewHolder(
    itemView: View,
    private val adapterFactory: BaseListAdapterFactory
) : BaseViewHolder(itemView) {



    private lateinit var listener: BaseListAdapter.ATTwetchNotificationListener

    private var model: ATTwetchNotificationItem? = null



    private val icon: ImageView = itemView.findViewById(R.id.icon)
    private val iconBadge: ImageView = itemView.findViewById(R.id.icon_badge)
    private val created: MaterialTextView = itemView.findViewById(R.id.created)
    private val name: MaterialTextView = itemView.findViewById(R.id.name)
    private val price: MaterialTextView = itemView.findViewById(R.id.price)
    private val userId: MaterialTextView = itemView.findViewById(R.id.user_id)
    private val content: MaterialTextView = itemView.findViewById(R.id.content)

    private val actionLike: MaterialButton = itemView.findViewById(R.id.action_like)
    private val actionComment: MaterialButton = itemView.findViewById(R.id.action_comment)
    private val actionBranch: MaterialButton = itemView.findViewById(R.id.action_branch)
    private val actionLink: MaterialButton = itemView.findViewById(R.id.action_link)


    private val quoteTwetchContainer: ConstraintLayout = itemView
        .findViewById(R.id.quote_twetch_container)
    private val quoteTweet: ViewTwetchTweetCard = itemView.findViewById(R.id.quote_tweet)
    private val quoteReplyingLabel: MaterialTextView = itemView.findViewById(R.id.quote_replying_label)
    private val quoteIcon: ImageView = itemView.findViewById(R.id.quote_icon)
    private val quotePostImage: ImageView = itemView.findViewById(R.id.quote_post_image)
    private val quotePostVideo: ViewVideoClip = itemView.findViewById(R.id.quote_post_video)
    private val quotePostImageLabel: MaterialTextView = itemView.findViewById(R.id.quote_post_image_label)
    private val quoteCreated: MaterialTextView = itemView.findViewById(R.id.quote_created)
    private val quoteName: MaterialTextView = itemView.findViewById(R.id.quote_name)
    private val quoteUserId: MaterialTextView = itemView.findViewById(R.id.quote_user_id)
    private val quoteContent: MaterialTextView = itemView.findViewById(R.id.quote_content)



    private val animLike: LottieAnimationView = itemView.findViewById(R.id.anim_like)
    private val animBranch: LottieAnimationView = itemView.findViewById(R.id.anim_branch)







    private val drawableNpc = ContextCompat.getDrawable(itemView.context, R.drawable.ic_npc3000)

    private val imagePlaceholderDrawable = ColorDrawable(
        itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
    )

    private val dp = itemView.resources.displayMetrics.density
    private val sp = itemView.resources.displayMetrics.scaledDensity


    private val colorPrimary = itemView.context.getColorFromAttr(R.attr.colorPrimary)


    private var strokeDrawable: DrawableStroke


    private val colorSelector = R.color.selector_on_primary.let { id ->
        ContextCompat.getColor(itemView.context, id)
    }



    private val drawableNotificationReply: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_twetch_notification_reply)!!

    private val drawableNotificationLike: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_twetch_notification_like)!!

    private val drawableNotificationFollow: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_twetch_notification_follow)!!

    private val drawableNotificationBranch: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_twetch_notification_branch)!!

    private val drawableNotificationMention: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_twetch_notification_mention)!!

    private val drawableNotificationPay: Drawable by lazy {
        val payDrawable = ContextCompat
            .getDrawable(itemView.context, R.drawable.ic_twetch_notification_pay)!!
        val ld = LayerDrawable(arrayOf(payDrawable))
        ld.setLayerInset(
            0,
            (3 * dp).toInt(),
            (3 * dp).toInt(),
            (3 * dp).toInt(),
            (3 * dp).toInt()
        )

        ld
    }


    private val drawableBranch: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_branch)!!

    private val drawableYouBranched: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_branch_filled)!!

    private val drawableLike: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_like)!!

    private val drawableYouLiked: Drawable = ContextCompat
        .getDrawable(itemView.context, R.drawable.ic_like_filled)!!



    private val colorOnSurface = itemView.context.getColorFromAttr(R.attr.colorOnSurface)
    private val colorTwetchLike = ContextCompat.getColor(itemView.context, R.color.twetch_like)
    private val colorTwetchBranch = ContextCompat.getColor(itemView.context, R.color.twetch_branch)




    private var shouldDropNextContentClickHandlerCall = false


    init {

        quoteTweet.setLifecycleOwner(this)


        val colorTransparentSurface = ColorUtils.setAlphaComponent(
            itemView.context.getColorFromAttr(R.attr.colorSurface),
            123
        )





        quotePostImageLabel.clipToOutline = true
        quotePostImageLabel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }
        quotePostImageLabel.background = ColorDrawable(colorTransparentSurface)







        quotePostVideo.clipToOutline = true
        quotePostVideo.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, 12 * dp)
            }
        }
        quotePostVideo.foreground = DrawableState.getNew(colorSelector)


        quotePostImage.clipToOutline = true
        quotePostImage.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, 12 * dp)
            }
        }
        quotePostImage.foreground = DrawableState.getNew(colorSelector)






        icon.clipToOutline = true
        icon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }
        icon.foreground = DrawableState.getNew(colorSelector)


        iconBadge.clipToOutline = true
        iconBadge.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }


        strokeDrawable = DrawableStroke().also {
            it.setStrokeColor(ColorUtils.setAlphaComponent(colorPrimary, 70))
            it.setStrokeWidths(0, 0, 0, (1.0f * dp).toInt())
            //it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
        }

        itemView.background = strokeDrawable




        val shape = ShapeAppearanceModel.Builder()
            .setAllCornerSizes(16 * dp)
            .build()
        val backgroundDrawable = MaterialShapeDrawable(shape)
        backgroundDrawable.strokeWidth = 1.5f * dp
        backgroundDrawable.fillColor = ColorStateList.valueOf(Color.TRANSPARENT)
        backgroundDrawable.strokeColor = ColorStateList
            .valueOf(ColorUtils.setAlphaComponent(colorPrimary, 180))
        quoteTwetchContainer.background = backgroundDrawable



        quoteIcon.clipToOutline = true
        quoteIcon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }
        quoteIcon.foreground = DrawableState.getNew(colorSelector)




        quotePostImage.addOnLayoutChangeListener { _, left, _, right, _, _, _, _, _ ->
            adapterFactory.intCache[CACHE_KEY_QUOTE_POST_IMAGE_WIDTH] = right - left
        }


        quoteTweet.addOnLayoutChangeListener { _, left, _, right, _, _, _, _, _ ->
            adapterFactory.intCache[CACHE_KEY_QUOTE_TWEET_IMAGE_WIDTH] = right - left
        }








        quoteTwetchContainer.setOnClickListener {
            val m = model ?: return@setOnClickListener

            listener.onPostClicked(m.notification)
        }



        quoteTweet.mediaClickListener = lambda@{
            val m = model ?: return@lambda

            m.notification.post?.tweet?.let { tweet ->
                listener.onImageClicked(m.notification, tweet.media)
            }
        }


        quotePostImage.setOnClickListener {
            val m = model ?: return@setOnClickListener

            m.notification.post?.let { p ->
                listener.onImageClicked(m.notification, p.getFullImageUrlList())
            }
        }

        quotePostVideo.setOnClickListener {
            val m = model ?: return@setOnClickListener

            m.notification.post?.getVideoFileTxId()?.let { videoTxId ->
                listener.onVideoClipClicked(m.notification, videoTxId)
            }
        }


        quoteTweet.clickListener = lambda@{
            val m = model ?: return@lambda

            listener.onPostTweetClicked(m.notification)
        }

        quoteContent.setOnClickListener {
            lifecycleScope.launchWhenStarted {
                delay(150)

                if (shouldDropNextContentClickHandlerCall) {
                    shouldDropNextContentClickHandlerCall = false
                    return@launchWhenStarted
                }

                val m = model ?: return@launchWhenStarted
                listener.onPostClicked(m.notification)
            }
        }
        quoteContent.movementMethod = LinkMovementMethod.getInstance()

        quoteCreated.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onContentClicked(m.notification)
        }

        quoteIcon.setOnClickListener {
            val m = model ?: return@setOnClickListener

            if (m.notification.post?.isRemoved == true) return@setOnClickListener
            listener.onPostUserClicked(m.notification)
        }

        quoteName.setOnClickListener {
            val m = model ?: return@setOnClickListener

            if (m.notification.post?.isRemoved == true) return@setOnClickListener
            listener.onPostUserClicked(m.notification)
        }




        icon.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onActorClicked(m.notification)
        }

        name.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onActorClicked(m.notification)
        }

















        actionLike.setOnClickListener {
            val m = model ?: return@setOnClickListener

            m.notification.post?.let { p ->
                val proceeding = listener.onLikeClicked(m.notification)
                if (proceeding) {
                    //model modified
                    p.onLikedByMe()

                    //force refresh of views
                    onBindViewHolder(m, listener)
                }
            }
        }

        actionComment.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onCommentClicked(m.notification)
        }

        actionBranch.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onBranchClicked(m.notification)
        }

        actionLink.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onCopyLinkClicked(m.notification)
        }







    }


    private var coinFlipChannelJob: Job? = null
    private var spanClickHandlerJob: Job? = null

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATTwetchNotificationItem

        this.listener = listener as BaseListAdapter.ATTwetchNotificationListener

        initBase(
            model.notification,
            model.labelCreatedAtOfNotification,
            model.labelPrice
        )

        model.notification.post?.let { post ->
            quoteTwetchContainer.visibility = View.VISIBLE

            initQuotePostBase(post, model.labelCreatedAtOfReferencedPost!!)
            initQuoteTweet(post)
            initQuotePostImage(post)
            initQuoteReplyingLabel(post)

        } ?: let {
            quoteTwetchContainer.visibility = View.GONE
        }



        spanClickHandlerJob?.cancel()
        spanClickHandlerJob = lifecycleScope.launchWhenStarted {
            model.spanClicks.receiveAsFlow().collect { (type, value) ->
                Timber.d("got type:$type; value:$value")
                shouldDropNextContentClickHandlerCall = true

                //process
                if (SpanType.USER_ID == type) {
                    listener.onUserIdBadgeClicked(model.notification, value)

                } else if (SpanType.REF_POST == type) {
                    listener.onUnloadedPostClicked(model.notification, value)

                }
            }
        }

        coinFlipChannelJob?.cancel()
        coinFlipChannelJob = lifecycleScope.launchWhenStarted {
            this@ATTwetchNotificationViewHolder.listener.getCoinFlipSuccessSignal().collectLatest { event ->
                event ?: return@collectLatest
                val peeking = event.peekContent()
                val m = model

                val postTxId = m.notification.postTxId ?: return@collectLatest
                if (peeking.postTxId != postTxId) return@collectLatest


                val parentId = m.notification.id
                if (parentId != peeking.parentId) return@collectLatest

                val coinFlip = event.getContentIfNotHandled() ?: return@collectLatest

                if (coinFlip.flipBranch && coinFlip.flipLike) {
                    animLike.playAnimation()
                    delay(200)
                    animBranch.playAnimation()

                } else if (coinFlip.flipLike) {
                    animLike.playAnimation()

                } else if (coinFlip.flipBranch) {
                    animBranch.playAnimation()
                }
            }
        }

    }



    private fun initBase(
        notification: TwetchNotificationsStore.Notification,
        createdAtLabel: String,
        priceLabel: String
    ) {

        name.text = notification.actor.name
        userId.text = itemView.resources.getString(R.string.twetch_user_id, notification.actor.id)
        created.text = createdAtLabel
        price.text = priceLabel

        model!!.contentMap["notification_description"]?.let {
            content.setPrecomputedText(it)
        } ?: let {
            content.setText(SpannableStringBuilder(notification.description), TextView.BufferType.SPANNABLE)
        }


        when (notification.type) {
            "reply" -> {
                iconBadge.setImageDrawable(drawableNotificationReply)
            }
            "like" -> {
                iconBadge.setImageDrawable(drawableNotificationLike)
            }
            "branch" -> {
                iconBadge.setImageDrawable(drawableNotificationBranch)
            }
            "follower" -> {
                iconBadge.setImageDrawable(drawableNotificationFollow)
            }
            "payment" -> {
                iconBadge.setImageDrawable(drawableNotificationPay)
            }
            "mention" -> {
                iconBadge.setImageDrawable(drawableNotificationMention)
            }
            else -> {
                iconBadge.setImageDrawable(null)
            }
        }


        notification.actor.icon?.let {
            icon.load(it) {
                placeholder(imagePlaceholderDrawable)
                error(imagePlaceholderDrawable)
                precision(Precision.INEXACT)
                target(object : ImageViewTarget(icon) {
                    override fun onSuccess(result: Drawable) {
                        val d = if (result is BitmapDrawable) {
                            DrawableRoundedBitmap(itemView.context, 0f, result.bitmap)
                        } else {
                            result
                        }
                        super.onSuccess(d)
                    }
                })
            }
        } ?: let {
            icon.load(imagePlaceholderDrawable)
        }
    }





    private fun initQuotePostBase(post: PostModel, createdAtLabel: String) {
        if (post.isRemoved) {
            quoteContent.visibility = View.VISIBLE

        } else if (post.content.isBlank()) {
            quoteContent.visibility = View.GONE

        } else {
            quoteContent.visibility = View.VISIBLE
        }
        //quoteContent.text = post.content
        model!!.contentMap[post.txId]?.let {
            quoteContent.setPrecomputedText(it)
        } ?: let {
            quoteContent.setText(SpannableStringBuilder(post.content), TextView.BufferType.SPANNABLE)
        }

        LinkifyCompat.addLinks(quoteContent, Linkify.WEB_URLS)


        if (post.isRemoved) {
            quoteName.text = itemView.context.getString(R.string.twetch_npc_3000)
            quoteUserId.text = ""

        } else {
            quoteName.text = post.user.name
            quoteUserId.text = itemView.resources.getString(R.string.twetch_user_id, post.user.id)
        }

        quoteCreated.text = createdAtLabel




        actionLike.text = post.numLikes.toString()
        actionComment.text = post.numComments.toString()
        actionBranch.text = post.numBranches.toString()

        if (post.youLiked != 0) {
            actionLike.icon = drawableYouLiked
            actionLike.iconTint = ColorStateList.valueOf(colorTwetchLike)
        } else {
            actionLike.icon = drawableLike
            actionLike.iconTint = ColorStateList.valueOf(colorOnSurface)
        }

        if (post.youBranched != 0) {
            actionBranch.icon = drawableYouBranched
            actionBranch.iconTint = ColorStateList.valueOf(colorTwetchBranch)
        } else {
            actionBranch.icon = drawableBranch
            actionBranch.iconTint = ColorStateList.valueOf(colorOnSurface)
        }



        if (post.isRemoved) {
            quoteIcon.load(drawableNpc)
        } else {
            post.user.icon?.let {
                quoteIcon.load(it) {
                    placeholder(imagePlaceholderDrawable)
                    error(imagePlaceholderDrawable)
                    precision(Precision.INEXACT)
                    target(object : ImageViewTarget(quoteIcon) {
                        override fun onSuccess(result: Drawable) {
                            val d = if (result is BitmapDrawable) {
                                DrawableRoundedBitmap(itemView.context, 0f, result.bitmap)
                            } else {
                                result
                            }
                            super.onSuccess(d)
                        }
                    })
                }
            } ?: let {
                quoteIcon.load(imagePlaceholderDrawable)
            }
        }
    }


    private fun initQuoteTweet(post: PostModel) {
        if (post.isRemoved) {
            quoteTweet.visibility = View.GONE
            return
        }

        post.tweet?.let {
            if (it.media.isNotEmpty()) {
                adapterFactory.imageAspectRatioCache.applyMinHeightFromCachedRatio(
                    quoteTweet.mediaImage,
                    it.media[0],
                    adapterFactory.intCache[CACHE_KEY_QUOTE_TWEET_IMAGE_WIDTH]
                )
            }

            quoteTweet.applyModel(it) { d ->
                adapterFactory.imageAspectRatioCache.cacheRatio(d, it.media[0])
            }
            quoteTweet.visibility = View.VISIBLE
        } ?: let {
            quoteTweet.visibility = View.GONE
        }
    }


    private fun initQuotePostImage(post: PostModel) {
        if (post.isRemoved) {
            quotePostImage.visibility = View.GONE
            quotePostImageLabel.visibility = View.GONE

            return
        }


        post.getFullImageUrlList().let { list ->
            if (list.isEmpty()) {
                quotePostImage.visibility = View.GONE
                quotePostImageLabel.visibility = View.GONE
            } else {
                quotePostImage.visibility = View.VISIBLE

                adapterFactory.imageAspectRatioCache.applyMinHeightFromCachedRatio(
                    quotePostImage,
                    list[0],
                    adapterFactory.intCache[CACHE_KEY_QUOTE_POST_IMAGE_WIDTH]
                )

                quotePostImage.load(list[0]) {
                    placeholder(imagePlaceholderDrawable)
                    error(imagePlaceholderDrawable)
                    precision(Precision.INEXACT)
                    target(object : ImageViewTarget(quotePostImage) {
                        override fun onSuccess(result: Drawable) {
                            adapterFactory.imageAspectRatioCache.cacheRatio(result, list[0])
                            super.onSuccess(result)
                        }
//                        override fun onSuccess(result: Drawable) {
//                            val d = if (result is BitmapDrawable) {
//                                DrawableRoundedBitmap(itemView.context, 0f, result.bitmap)
//                            } else {
//                                result
//                            }
//                            super.onSuccess(d)
//                        }
                    })
                }

                if (list.size > 1) {
                    quotePostImageLabel.visibility = View.VISIBLE
                    quotePostImageLabel.text = itemView.context.getString(
                        R.string.one_of_label,
                        list.size
                    )
                } else {
                    quotePostImageLabel.visibility = View.GONE
                }
            }
        }
    }





    private fun initQuoteReplyingLabel(post: PostModel) {
        if (post.parents.isNotEmpty()) {
            val list = post.parents
            val namesList = list
                .map { it.user.name }
                .asReversed()
                .fold(mutableListOf<String>()) { acc, item ->
                    if (acc.contains(item)) {
                        return@fold acc
                    } else {
                        acc.add(item)
                        return@fold acc
                    }
                }
            quoteReplyingLabel.visibility = View.VISIBLE
            val sb = StringBuilder(itemView.resources.getString(R.string.replying_to))
            val maxIndex = min(namesList.size, 2)
            for (i in 0 until maxIndex) {
                sb.append(" @").append(namesList[i])
            }
            if (namesList.size > maxIndex) {
                sb.append(" ...")
            }
            quoteReplyingLabel.text = sb.toString()
        } else {
            quoteReplyingLabel.visibility = View.GONE
        }
    }







    companion object {

        const val LOG = false

        private const val CACHE_KEY_QUOTE_POST_IMAGE_WIDTH = "twetch_post__notif_quote_post_image_width"
        private const val CACHE_KEY_QUOTE_TWEET_IMAGE_WIDTH = "twetch_post__notif_quote_tweet_image_width"

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTwetchNotificationViewHolder {
            return ATTwetchNotificationViewHolder(
                inflater.inflate(
                    R.layout.li_twetch_notification,
                    parent,
                    false
                ),
                factory
            )
        }
    }
}