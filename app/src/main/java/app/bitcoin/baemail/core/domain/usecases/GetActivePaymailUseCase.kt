package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.domain.entities.ActivePaymail
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import kotlinx.coroutines.flow.Flow

class GetActivePaymailUseCaseImpl(
    private val authRepository: AuthRepository
): GetActivePaymailUseCase {
    override fun invoke(): Flow<ActivePaymail?> = authRepository.activePaymail

}

interface GetActivePaymailUseCase {
    operator fun invoke(): Flow<ActivePaymail?>
}