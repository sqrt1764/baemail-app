package app.bitcoin.baemail.core.data.room.entity

import androidx.room.Entity
import app.bitcoin.baemail.message.data.entity.MessageBucket
import app.bitcoin.baemail.core.data.room.TABLE_DECRYPTED_MESSAGE

@Entity(tableName = TABLE_DECRYPTED_MESSAGE, primaryKeys = ["txId", "ownerPaymail"])
data class DecryptedBaemailMessage(
    val txId: String,
    val ownerPaymail: String,
    val createdAt: Long,
    val updatedAt: Long,

    val replyToTxId : String?,

    val amountUsd: String,
    val numAmountUsd: Float,
    val to: String,
    val cc: List<String>,
    val subject: String,
    val salesPitch: String?,
    val fromName: String?,
    val from: String,


    val decryptedMessage: String,

    val destinationSeen: Boolean,
    val destinationPaid: Boolean,

    val bucket: MessageBucket
)