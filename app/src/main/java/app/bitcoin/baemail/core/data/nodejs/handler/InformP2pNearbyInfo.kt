package app.bitcoin.baemail.core.data.nodejs.handler

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.p2p.InformP2pChangeHelper
import app.local.p2p.serviceDiscovery.P2pStallRegistryHelper4
import com.google.gson.JsonArray
import com.google.gson.JsonObject

/*
expected structure
[
    {
        type: 'COMMS_SERVER_META',
        model: {..}
    }

    Possible values of `type`: 'COMMS_SERVER_META', 'REQUEST_MESSAGE_SEND', 'REQUEST_RECEIVE_FILE', 'REQUEST_SEND_SHORT', 'REQUEST_DISCONNECT'

    Possible structures of `model`;
    When 'COMMS_SERVER_META':
    {
        port: 12345
    }

    When 'REQUEST_MESSAGE_SEND':
    {
        endpointId: 'ABC0',
        token: '0011ffaa',
        hasStreamPayload: false,
        bytePayload: ''
    }

    When 'REQUEST_RECEIVE_FILE':
    {
        endpointId: 'ABC0',
        token: '0011ffaa',
        salt: '0011ffaa',
        data: '{fileResourceUri: 'abc01ABC!!'}' [salt-masked]
    }


    When 'REQUEST_SEND_SHORT':
    {
        endpointId: 'ABC0',
        token: '0011ffaa',
        salt: '0011ffaa',
        data: '0011ffaa'
    }

    When 'REQUEST_DISCONNECT':
    {
        endpointId: 'ABC0'
    }
]
*/
class InformP2pNearbyInfo(
    private val informP2pChangeHelper: InformP2pChangeHelper
) : NodeRuntimeInterface.WorkHandler("INFORM_P2P_NEARBY_INFO") {

    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<NodeRuntimeInterface.Result, JsonArray> {

        val obj = params[0].asJsonObject
        val type = obj.get("type").asString

        when (type) {
            "COMMS_SERVER_META" -> handleCommsServerMeta(obj.getAsJsonObject("model"))
            "REQUEST_MESSAGE_SEND" -> handleSendRequest(obj.getAsJsonObject("model"))
            "REQUEST_RECEIVE_FILE" -> handleReceiveFileRequest(obj.getAsJsonObject("model"))
            "REQUEST_SEND_SHORT" -> handleSendShortRequest(obj.getAsJsonObject("model"))
            "REQUEST_DISCONNECT" -> handleRequestDisconnect(obj.getAsJsonObject("model"))
            else -> throw Exception()
        }

        return NodeRuntimeInterface.Result.SUCCESS to JsonArray()
    }

    private fun handleCommsServerMeta(json: JsonObject) {
        val port = json.get("port").asInt
        informP2pChangeHelper.postNearbyMeta(InformP2pChangeHelper.P2pNearbyMeta(port))
    }

    private fun handleSendRequest(json: JsonObject) {
        val endpointId = json.get("endpointId").asString
        val token = json.get("token").asString
        val salt = json.get("salt").asString

        informP2pChangeHelper.postSendRequest(P2pStallRegistryHelper4.SendEvent(
            endpointId,
            token,
            salt
        ))

    }

    private fun handleReceiveFileRequest(json: JsonObject) {
        val endpointId = json.get("endpointId").asString
        val token = json.get("token").asString
        val salt = json.get("salt").asString
        val data = json.get("data").asString

        informP2pChangeHelper.postSendRequest(P2pStallRegistryHelper4.FileReceiveEvent(
            endpointId,
            token,
            salt,
            data
        ))
    }

    private fun handleSendShortRequest(json: JsonObject) {
        val endpointId = json.get("endpointId").asString
        val token = json.get("token").asString
        val salt = json.get("salt").asString
        val data = json.get("data").asString

        informP2pChangeHelper.postSendRequest(P2pStallRegistryHelper4.ShortSendEvent(
            endpointId,
            token,
            salt,
            data
        ))
    }

    private fun handleRequestDisconnect(json: JsonObject) {
        val endpointId = json.get("endpointId").asString
        informP2pChangeHelper.postDisconnectRequest(endpointId)
    }

}