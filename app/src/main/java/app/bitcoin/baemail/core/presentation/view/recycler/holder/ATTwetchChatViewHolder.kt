package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.Outline
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableRoundedBitmap
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchChatListItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import coil.load
import coil.target.ImageViewTarget
import kotlin.math.min

class ATTwetchChatViewHolder(
        itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATTwetchChatInfoListener

    var model: ATTwetchChatListItem? = null

    val icon: ImageView = itemView.findViewById(R.id.icon)
    val unreadCount: TextView = itemView.findViewById(R.id.unread_count)
    val name: TextView = itemView.findViewById(R.id.name)
    val lastMessageTime: TextView = itemView.findViewById(R.id.last_message_time)
    val isGroup: TextView = itemView.findViewById(R.id.is_group)


    private val imagePlaceholderDrawable = ColorDrawable(itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant))

    init {

        val strokeColor = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(itemView.context, id)
        }

        val strokeWidth = R.dimen.li_filter_seed_word__stroke_width.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        val strokePaddingSides = R.dimen.li_filter_seed_word__stroke_padding_sides.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        icon.clipToOutline = true
        icon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }

        }

        unreadCount.clipToOutline = true
        unreadCount.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }

        }

        itemView.background = LayerDrawable(arrayOf(
                DrawableStroke().also {
                    it.setStrokeColor(strokeColor)
                    it.setStrokeWidths(0, 0, 0, strokeWidth)
                    it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
                },
                DrawableState.getNew(colorSelector)
        ))

        itemView.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onClick(m.chat)
        }



    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATTwetchChatListItem

        this.listener = listener!! as BaseListAdapter.ATTwetchChatInfoListener

        name.text = model.name
        lastMessageTime.text = model.labelLastMessageTime

        if (model.chat.isGroup) {
            isGroup.visibility = View.VISIBLE
        } else {
            isGroup.visibility = View.INVISIBLE
        }

        model.icon?.let {
            icon.load(it) {
                placeholder(imagePlaceholderDrawable)
                error(imagePlaceholderDrawable)

                target(object : ImageViewTarget(icon) {
                    override fun onSuccess(result: Drawable) {
                        val d = if (result is BitmapDrawable) {
                            DrawableRoundedBitmap(itemView.context, 0f, result.bitmap)
                        } else {
                            result
                        }
                        super.onSuccess(d)
                    }
                })
            }
        } ?: icon.load(imagePlaceholderDrawable)


        if (model.chat.unreadCount < 1) {
            unreadCount.visibility = View.INVISIBLE
        } else {
            unreadCount.text = model.chat.unreadCount.toString()
            unreadCount.visibility = View.VISIBLE
        }

    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTwetchChatViewHolder {
            return ATTwetchChatViewHolder(
                    inflater.inflate(
                            R.layout.li_twetch_chat,
                            parent,
                            false
                    )
            )
        }
    }
}