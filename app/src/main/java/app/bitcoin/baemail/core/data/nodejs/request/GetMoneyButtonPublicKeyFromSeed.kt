package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.JsonArray
import timber.log.Timber
import java.lang.RuntimeException

class GetMoneyButtonPublicKeyFromSeed(
    val seed: List<String>,
    val callback: (String?)->Unit
) : NodeRuntimeInterface.NodeRequest("MONEY_BUTTON_PUBLIC_KEY_FROM_SEED") {


    override fun getReqParams(): JsonArray {
        val array = JsonArray()
        seed.forEach {
            array.add(it)
        }
        return array
    }

    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "public key not received; token: $token")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(RuntimeException(), "unexpected response format; token: $token")
            callback(null)
            return
        }

        try {
            callback(data.asString)
        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")
            callback(null)
        }
    }

}