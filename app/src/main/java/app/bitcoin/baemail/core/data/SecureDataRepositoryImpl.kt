package app.bitcoin.baemail.core.data

import app.bitcoin.baemail.core.data.util.AuthenticatedPaymail
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.domain.entities.InfoModel
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.domain.repository.SecureDataRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import timber.log.Timber

class SecureDataRepositoryImpl(
    val authRepository: AuthRepository,
    val secureDataSource: SecureDataSource
) : SecureDataRepository {

    override suspend fun getFundingWalletSeed(paymail: String): List<String> =
        secureDataSource.getParallelWalletSeed(AuthenticatedPaymail(paymail))

    override suspend fun getPaymailWalletSeed(paymail: String): List<String> =
        secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(paymail))

    override suspend fun getPathForPaymailMnemonic(paymail: String): String =
        secureDataSource.getPathForPaymailMnemonic(AuthenticatedPaymail(paymail))

    override suspend fun getPathForFundingMnemonic(paymail: String): String =
        secureDataSource.getPathForFundingMnemonic(AuthenticatedPaymail(paymail))

    override fun getWalletInfo(): Flow<InfoModel?> =
        authRepository.activePaymail.flatMapLatest { activePaymail ->
            val paymail = activePaymail?.paymail ?: return@flatMapLatest flow<InfoModel?> {
                emit(null)
            }

            val paymailMnemonic = secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(paymail))
            val paymailPath = secureDataSource.getPathForPaymailMnemonic(AuthenticatedPaymail(paymail))
            val fundingMnemonic = secureDataSource.getParallelWalletSeed(AuthenticatedPaymail(paymail))
            val fundingPath = secureDataSource.getPathForFundingMnemonic(AuthenticatedPaymail(paymail))

            MutableStateFlow(InfoModel(
                paymail,
                paymailPath,
                paymailMnemonic,
                fundingPath,
                fundingMnemonic
            ))
        }
}

