package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.drawable.LayerDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.view.recycler.ATReplReturn
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr

class ATTerminalReturnViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATTerminalReturnListener

    var model: ATReplReturn? = null

    val infoText: TextView = itemView.findViewById(R.id.info_text)
    val outputText: TextView = itemView.findViewById(R.id.code_text)
    val outputLine: TextView = itemView.findViewById(R.id.line)
    val actionCopy: Button = itemView.findViewById(R.id.action_copy)

    val colorSuccess = ContextCompat.getColor(itemView.context, R.color.green2)
    val colorFail = itemView.context.getColorFromAttr(R.attr.colorDanger)

    init {

        val strokeColor = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(itemView.context, id)
        }

        val strokeWidth = (0.6 * itemView.resources.displayMetrics.density).toInt()

        val strokePaddingSides = (4 * itemView.resources.displayMetrics.density).toInt()

        itemView.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(strokeColor)
                it.setStrokeWidths(0, 0, 0, strokeWidth)
                it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))


        actionCopy.setOnClickListener {
            val m = model ?: return@setOnClickListener

            listener.onCopyClick(m.data)
        }

    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATReplReturn

        this.listener = listener!! as BaseListAdapter.ATTerminalReturnListener

        outputText.text = model.data

        outputLine.text = model.id


        if (model.success) {
            infoText.setText(R.string.returned)
            infoText.setTextColor(colorSuccess)
        } else {
            infoText.setText(R.string.crashed)
            infoText.setTextColor(colorFail)
        }
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTerminalReturnViewHolder {
            return ATTerminalReturnViewHolder(
                inflater.inflate(
                    R.layout.li_terminal_return,
                    parent,
                    false
                )
            )
        }
    }
}