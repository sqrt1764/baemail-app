package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.twetch.ui.TwetchAuthStore
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber
import java.lang.RuntimeException




/*

# expected structure of params
[
    {
        isForced: false
    }
]


# expected structure of the response data passed in the callback
`me` will only be filled when keyAdded==TRUE
`signingAddress` will only be filled when keyAdded==FALSE
[
    {
        token: 'abc',
        keyAdded: true,
        data: {
            "me": {
                "id": "1470",
                "name": "yaanis",
                "publicKey": "03827d631df08ba2e4f177a433ac7aed8ac114bf404024f0c3d959c5483b3322a3",
                "description": "doing Android native programming",
                "dmConversationId": "3ad40c2f-7e91-4686-b754-20314eae2937",
                "followerCount": "45",
                "followingCount": "105",
                "icon": "https://cimg.twetch.com/avatars/2020/07/09/a78511334b7142810d6e3b904389b1aa402fd0ba.jpg",
                "invitesRemaining": "3",
                "myConversationId": "8f704efa-593a-41a5-a0a0-f179582e9bc8",
                "notificationsCount": "0",
                "numPosts": "645",
                "numLikes": "176",
                "numReferrals": "0",
                "paidNotificationsCount": "0",
                "unreadMessagesCount": "1"
            }
        },
        signingAddress: {
            address: '1af..ff',
            message: 'twetch-api-rocks',
            signature: 'ff..00'
        }
    }
]



*/
class TwetchAuth(
        val paymail: String,
        val isForced: Boolean,
        val callback: (TwetchAuthStore.AuthInfo?)->Unit
) : NodeRuntimeInterface.NodeRequest("TWETCH_AUTH") {

    override fun getReqParams(): JsonArray {
        val configJson = JsonObject()
        configJson.addProperty("isForced", isForced)

        val finalArray = JsonArray()
        finalArray.add(configJson)

        return finalArray
    }


    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "twetch authentication failed; token: $token")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(stackTrace, "unexpected response format; token: $token")
            callback(null)
            return
        }

        try {
            val responseObj = data[0].asJsonObject

            val info = TwetchAuthStore.AuthInfo.parse(responseObj)

            callback(info)

        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")
            callback(null)
        }
    }

}