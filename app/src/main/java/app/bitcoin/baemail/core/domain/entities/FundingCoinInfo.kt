package app.bitcoin.baemail.core.domain.entities

import app.bitcoin.baemail.core.data.nodejs.request.DerivedAddress
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus

data class FundingCoinInfo(
    val activePaymail: String,
    val syncStatus: ChainSyncStatus,
    val fundingWalletSeed: List<String>,
    val fundingRootPath: String,
    val unspentCoins: List<Coin>,
    val upcomingChangeAddressList: List<DerivedAddress>
)