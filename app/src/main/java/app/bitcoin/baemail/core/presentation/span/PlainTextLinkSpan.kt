package app.bitcoin.baemail.core.presentation.span

import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View

class PlainTextLinkSpan(
    private val ref: String,
    private val onRefClicked: (String) -> Unit
) : ClickableSpan() {

    override fun onClick(widget: View) {
        onRefClicked(ref)
    }

    override fun updateDrawState(ds: TextPaint) {}
}