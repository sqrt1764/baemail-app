package app.bitcoin.baemail.core.presentation.drawable

import android.graphics.*
import android.graphics.drawable.Drawable
import android.text.TextPaint
import timber.log.Timber

class DrawableNumberBadge(
    private val dp: Float,
    private var value: String,
    private val labelTextSize: Float = 50f,
    private var labelColor: Int = Color.WHITE,
    private var backgroundColor: Int = Color.RED
) : Drawable() {


    private val bp = Paint(Paint.ANTI_ALIAS_FLAG)
    private val tp = TextPaint(Paint.ANTI_ALIAS_FLAG)

    init {
        tp.color = labelColor
        tp.textSize = labelTextSize
        tp.textAlign = Paint.Align.RIGHT


        bp.style = Paint.Style.FILL
        bp.color = backgroundColor
    }

    var textTop: Float = 0f
    var textRight: Float = 0f
    var textBottom: Float = 0f
    var textLeft: Float = 0f
    var roundness: Float = 0f

    private fun refreshTextBounds() {
        textBottom = (bounds.top + labelTextSize + 0 * dp)
        textTop = textBottom - labelTextSize
        textRight = (bounds.right - 16 * dp)
        textLeft = textRight - tp.measureText(value)
    }

    override fun onBoundsChange(bounds: Rect) {
        bounds ?: return

        Timber.d("...new bounds: $bounds")
        refreshTextBounds()
    }

    override fun draw(canvas: Canvas) {
        if (value.isBlank()) return


        //val token = canvas.save()
        roundness = (textBottom + 4 * dp) - (textTop - 0 * dp)

        canvas.drawRoundRect(
            textLeft - 8 * dp,
            textTop - 0 * dp,
            textRight + 8 * dp,
            textBottom + 4 * dp,
            roundness,
            roundness,
            bp
        )

        canvas.drawText(
            value,
            textRight,
            textBottom,
            tp
        )


        //canvas.restoreToCount(token)

    }

    override fun setAlpha(alpha: Int) {
        //not supported
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        //not supported
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

    fun updateLabel(content: String) {
        value = content
        refreshTextBounds()
        invalidateSelf()
    }


}