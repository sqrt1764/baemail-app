package app.bitcoin.baemail.core.presentation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import app.bitcoin.baemail.paymail.presentation.PaymailActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashActivity : AppCompatActivity(), HasAndroidInjector {

    companion object {
        const val KEY_IS_CHANGING_ACTIVE_PAYMAIL = "changing_active_paymail"
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant

    private lateinit var authViewModel: AuthViewModel

    private var isNavigating = false

    private var isChangingActivePaymail = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent.hasExtra(KEY_IS_CHANGING_ACTIVE_PAYMAIL)) {
            isChangingActivePaymail = intent.getBooleanExtra(KEY_IS_CHANGING_ACTIVE_PAYMAIL, false)
        }

        authViewModel = ViewModelProvider(
            this,
            savedStateViewModelFactoryAssistant.create(this)
        )[AuthViewModel::class.java]

//        authViewModel.authLD.observe(this, Observer { isAuthorized ->
//            isAuthorized ?: return@Observer
//
//            if (isAuthorized) {
//                //go to the central screen
//                navigateToApp()
//            } else {
//                //go to auth screen
//                navigateToPaymailScreen()
//            }
//        })

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                authViewModel.activePaymail.collect { activePaymail ->
                    activePaymail ?: return@collect

                    if (activePaymail.paymail != null) {
                        //go to the central screen
                        navigateToApp()
                    } else {
                        //go to auth screen
                        navigateToPaymailScreen()
                    }

                }
            }
        }
    }

    private fun navigateToApp() {
        if (isNavigating) return
        isNavigating = true

        val intent = Intent(this, CentralActivity::class.java)
        if (isChangingActivePaymail) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }

        startActivity(intent)
        finish()
    }

    private fun navigateToPaymailScreen() {
        if (isNavigating) return
        isNavigating = true

        startActivity(Intent(this, PaymailActivity::class.java), null)
        finish()
    }



    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }
}
