package app.bitcoin.baemail.core.data

import androidx.tracing.Trace
import app.bitcoin.baemail.core.data.util.Tr
import app.bitcoin.baemail.core.data.wallet.sending.PaymailExistsHelper
import app.bitcoin.baemail.core.data.net.AppApiService
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.data.wallet.sending.BsvAliasCapabilitiesHelper
import app.bitcoin.baemail.fundingWallet.core.domain.entity.P2pDestinationInfo
import app.bitcoin.baemail.fundingWallet.core.domain.entity.P2pTxMetadata
import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import app.bitcoin.baemail.core.domain.repository.PaymailInfoRepository
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random

class PaymailInfoRepositoryImpl(
    private val apiService: AppApiService,
    private val bsvAliasCapabilitiesHelper: BsvAliasCapabilitiesHelper,
    paymailHelper: PaymailExistsHelper,
    private val gson: Gson,
    private val coroutineUtil: CoroutineUtil
) : PaymailInfoRepository {

    override val paymailExistsHelper: PaymailExistsHelper = paymailHelper

    override fun checkExistence(paymail: String): Flow<PaymailExistence> =
        paymailExistsHelper.checkExistence(paymail).map {
            when (it.first) {
                PaymailExistsHelper.State.CHECKING -> PaymailExistence.Checking(paymail)
                PaymailExistsHelper.State.INVALID -> PaymailExistence.Invalid(paymail)
                PaymailExistsHelper.State.VALID -> PaymailExistence.Valid(
                    paymail,
                    it.second!!.alias.pubkey,
                    it.second!!.supportsProfile,
                    it.second!!.supportsP2p
                )
            }
        }

    /**
     * @return hex encoded Bitcoin script for the destination output
     */
    override suspend fun getPaymailDestinationOutput(
        sendingPaymail: String,
        receivingPaymail: String,
        signMessageWithSendingPaymailPki: suspend (sendingPaymail: String, message: String)->String? //todo wut?
    ): String? {

        val id = Random.nextInt()
        try {
            Trace.beginAsyncSection(Tr.METHOD_GET_PAYMAIL_DESTINATION_OUTPUT, id)

            val addressResolver = PaymailAddressResolver(sendingPaymail, null, null)

            val signature = signMessageWithSendingPaymailPki(
                sendingPaymail,
                addressResolver.getSignedString()
            ) ?: return null


            val reqJson = addressResolver.getReqJson(signature)

            Timber.d("paymail-address-resolution json: $reqJson")

            return getPaymailDestination(receivingPaymail, reqJson.toString())
        } finally {
            Trace.endAsyncSection(Tr.METHOD_GET_PAYMAIL_DESTINATION_OUTPUT, id)
        }
    }

    /**
     * http://web.archive.org/web/20200808115636/https://docs.moneybutton.com/docs/paymail-07-p2p-payment-destination.html
     */
    override suspend fun getP2pPaymentDestinationInfo(
        receivingPaymail: String,
        satsToSend: Long
    ): P2pDestinationInfo = withContext(coroutineUtil.ioDispatcher) {
        val handle = receivingPaymail.substring(0, receivingPaymail.indexOf("@"))
        val domain = receivingPaymail.substring(receivingPaymail.indexOf("@") + 1)

        val capabilities = bsvAliasCapabilitiesHelper.getBsvAliasCapabilities(domain)
            ?: throw RuntimeException()

        val assembled = capabilities.p2pPaymentDestinationEndpoint!!
            .replace("{alias}", handle)
            .replace("{domain.tld}", domain)

        val jsonReq = JsonObject()
        jsonReq.addProperty("satoshis", satsToSend)

        val reqBody = jsonReq.toString().toRequestBody()

        val response = apiService.postJson(assembled, reqBody).execute()
        if (response.isSuccessful) {
            response.body()?.let {
                return@withContext gson.fromJson(it.charStream(), P2pDestinationInfo::class.java)
            }
        }

        throw RuntimeException()
    }

    /**
     * http://web.archive.org/web/20200808115844/https://docs.moneybutton.com/docs/paymail-06-p2p-transactions.html
     */
    override suspend fun sendP2pPayment(
        receivingPaymail: String,
        hexTx: String,
        metadata: P2pTxMetadata,
        reference: String
    ) = withContext(coroutineUtil.ioDispatcher) {
        val handle = receivingPaymail.substring(0, receivingPaymail.indexOf("@"))
        val domain = receivingPaymail.substring(receivingPaymail.indexOf("@") + 1)

        val capabilities = bsvAliasCapabilitiesHelper.getBsvAliasCapabilities(domain)
            ?: throw RuntimeException()

        val assembled = capabilities.submitP2pTransactionEndpoint!!
            .replace("{alias}", handle)
            .replace("{domain.tld}", domain)

        val jsonReq = JsonObject()
        jsonReq.addProperty("hex", hexTx)
        jsonReq.addProperty("reference", reference)
        val jsonMetadata = gson.toJsonTree(metadata, P2pTxMetadata::class.java)
        jsonReq.add("metadata", jsonMetadata)

        val reqBody = jsonReq.toString().toRequestBody()

        val response = apiService.postJson(assembled, reqBody).execute()
        if (response.isSuccessful) {
            response.body()?.let {
                val json = gson.fromJson(it.charStream(), JsonObject::class.java)
                val txid = json.get("txid").asString

                Timber.d("response $txid $json")
                return@withContext
            }
        }

        throw RuntimeException()
    }


    /**
     * @return hex encoded Bitcoin script for the destination output
     */
    private suspend fun getPaymailDestination(
        paymail: String,
        sendingPaymailAuth: String
    ): String? {
        return withContext(Dispatchers.IO) {
            val id = Random.nextInt()
            try {
                Trace.beginAsyncSection(Tr.METHOD_REQUEST_PAYMENT_DESTINATION_OUTPUT, id)


                val handle = paymail.substring(0, paymail.indexOf("@"))
                val domain = paymail.substring(paymail.indexOf("@") + 1)


                val capabilities = bsvAliasCapabilitiesHelper.getBsvAliasCapabilities(domain)
                    ?: return@withContext null



                //got the payment destination querying endpoint

                val assembled = capabilities.paymentDestinationEndpoint
                    .replace("{alias}", handle)
                    .replace("{domain.tld}", domain)

                val reqBody = sendingPaymailAuth.toRequestBody()

                val call = apiService.postJson(assembled, reqBody)
                val response = call.execute()
                if (response.isSuccessful) {
                    response.body()?.let {
                        try {
                            //got the destination output

                            val json = gson.fromJson(it.charStream(), JsonObject::class.java)
                            val hexScript = json.get("output").asString

                            Timber.d("output hex script; destination: $paymail; script: $hexScript")
                            return@withContext hexScript

                        } catch (e: Exception) {
                            Timber.e(e)
                        }
                    }
                }

                return@withContext null
            } finally {
                Trace.endAsyncSection(Tr.METHOD_REQUEST_PAYMENT_DESTINATION_OUTPUT, id)

            }
        }
    }



    class PaymailAddressResolver(
        val senderHandle: String,
        val amount: Int?,
        val purpose: String?
    ) {

        val timestamp: String

        init {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'", Locale.US).apply {
                timeZone = TimeZone.getTimeZone("UTC")
            }

            timestamp = df.format(Date())
        }

        fun getSignedString(): String {
            val stringAmount = (amount ?: 0).toString()
            val finalPurpose = purpose ?: ""

            return senderHandle + timestamp + stringAmount + finalPurpose
        }


        fun getReqJson(signature: String): JsonObject {
            val json = JsonObject()

            json.addProperty("senderName", "")
            json.addProperty("senderHandle", senderHandle)
            json.addProperty("dt", timestamp)

            val stringAmount = (amount ?: 0).toString()
            json.addProperty("amount", stringAmount)

            val finalPurpose = purpose ?: ""
            json.addProperty("purpose", finalPurpose)

            json.addProperty("signature", signature)

            return json
        }

    }

}