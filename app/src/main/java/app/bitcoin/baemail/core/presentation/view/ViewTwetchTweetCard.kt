package app.bitcoin.baemail.core.presentation.view

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Outline
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.view.ViewCompat
import androidx.lifecycle.LifecycleOwner
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableRoundedBitmap
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.twetch.ui.TweetModel
import app.bitcoin.baemail.twetch.ui.TweetUser
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.data.util.match
import coil.load
import coil.size.Precision
import coil.target.ImageViewTarget
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.textview.MaterialTextView
import timber.log.Timber
import kotlin.math.min

class ViewTwetchTweetCard : ConstraintLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private var lifecycleOwner: LifecycleOwner? = null

    private val dp1: Float

    private val tweetCard: ConstraintLayout
    private val image: ImageView
    val mediaImage: ImageView
    private val mediaImageLabel: MaterialTextView
    private val content: MaterialTextView
    private val name: MaterialTextView
    private val handle: MaterialTextView

    private var valueModel = TweetModel(
        "Wed Jan 27 17:31:13 +0000 2021",
        "1354482105355444226",
        "tweet content",
        emptyList(),
        "2349056444",
        TweetUser(
            "Charlie Kirk",
            "charliekirk11",
            "Wed May 04 13:37:25 +0000 2011",
            "292929271",
            null
        )
    )

    private val imagePlaceholderDrawable = ColorDrawable(
        context.getColorFromAttr(R.attr.colorSurfaceVariant)
    )

    private val colorSelector = R.color.selector_on_primary.let { id ->
        ContextCompat.getColor(context, id)
    }


    private var _onMediaImageLoaded: (Drawable)->Unit = lambda@{
        onMediaImageLoaded?.let { onLoaded ->
            onLoaded(it)
        }
    }
    private var onMediaImageLoaded: ((Drawable)->Unit)? = null

    var clickListener: (TweetModel) -> Unit = {
        Timber.d("tweet clicked: $it")
    }

    var mediaClickListener: (TweetModel) -> Unit = {
        Timber.d("tweet-media clicked: $it")
    }

    init {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.view_twetch_tweet_card, this, false)
        val set = ConstraintSet()
        view.id = ViewCompat.generateViewId()
        addView(view)
        tweetCard = view as ConstraintLayout

        set.clone(this)
        set.match(view, this)

        dp1 = context.resources.displayMetrics.density


        image = view.findViewById(R.id.image)
        mediaImage = view.findViewById(R.id.media_image)
        content = view.findViewById(R.id.content)
        mediaImageLabel = view.findViewById(R.id.media_image_label)
        name = view.findViewById(R.id.name)
        handle = view.findViewById(R.id.handle)


        image.clipToOutline = true
        image.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val roundness = min(view.width, view.height)

                outline.setRoundRect(0, 0, view.width, view.height, roundness.toFloat())
            }
        }



        val colorTransparentSurface = ColorUtils.setAlphaComponent(
            context.getColorFromAttr(R.attr.colorSurface),
            123
        )

        mediaImageLabel.clipToOutline = true
        mediaImageLabel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }
        mediaImageLabel.background = ColorDrawable(colorTransparentSurface)





        mediaImage.foreground = DrawableState.getNew(colorSelector)

        mediaImage.setOnClickListener {
            mediaClickListener(valueModel)
        }










        val colorPrimary = context.getColorFromAttr(R.attr.colorPrimary)

        val shape = ShapeAppearanceModel.Builder()
//            .setAllCornerSizes(12 * dp1)
            .build()
        val backgroundDrawable = MaterialShapeDrawable(shape)
        backgroundDrawable.fillColor = ColorStateList
            .valueOf(ColorUtils.setAlphaComponent(colorPrimary, 90))
        tweetCard.background = backgroundDrawable

        tweetCard.clipToOutline = true
        tweetCard.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, 12 * dp1)
            }
        }



        tweetCard.setOnClickListener {
            clickListener(valueModel)
        }

        refresh()

    }

    private fun refresh() {
        /////

        name.text = valueModel.user.name
        handle.text = resources.getString(R.string.twetch_user_id, valueModel.user.screenName)

        content.text = valueModel.text.trim()
        if (valueModel.text.trim().isEmpty()) {
            content.visibility = View.GONE
        } else {
            content.visibility = View.VISIBLE
        }


        if (valueModel.media.isNotEmpty()) {
            mediaImage.visibility = View.VISIBLE

            if (valueModel.media.size > 1) {
                mediaImageLabel.visibility = View.VISIBLE
                mediaImageLabel.text = context.getString(
                    R.string.one_of_label,
                    valueModel.media.size
                )
            } else {
                mediaImageLabel.visibility = View.GONE
            }

            mediaImage.load(valueModel.media[0]) {
                placeholder(imagePlaceholderDrawable)
                error(imagePlaceholderDrawable)
                precision(Precision.INEXACT)
                target(object : ImageViewTarget(mediaImage) {
                    override fun onSuccess(result: Drawable) {
                        _onMediaImageLoaded.invoke(result)
                        super.onSuccess(result)
                    }
                    //                    override fun onSuccess(result: Drawable) {
//                        val d = if (result is BitmapDrawable) {
//                            DrawableRoundedBitmap(context, 0f, result.bitmap)
//                        } else {
//                            result
//                        }
//                        super.onSuccess(d)
//                    }
                })
            }



        } else {
            mediaImage.visibility = View.GONE
            mediaImageLabel.visibility = View.GONE
        }


        valueModel.user.imageUrl?.let {
            image.load(it) {
                placeholder(imagePlaceholderDrawable)
                error(imagePlaceholderDrawable)
                precision(Precision.INEXACT)
                target(object : ImageViewTarget(image) {
                    override fun onSuccess(result: Drawable) {
                        val d = if (result is BitmapDrawable) {
                            DrawableRoundedBitmap(context, 0f, result.bitmap)
                        } else {
                            result
                        }
                        super.onSuccess(d)
                    }
                })
            }
        } ?: let {
            image.load(imagePlaceholderDrawable)
        }
    }

    fun applyModel(model: TweetModel, onMediaImageLoaded: ((Drawable)->Unit)? = null) {
        val prevModel = valueModel
        valueModel = model
        this.onMediaImageLoaded = onMediaImageLoaded

        if (prevModel == model) return
        refresh()
    }

    fun setLifecycleOwner(owner: LifecycleOwner) {
        lifecycleOwner = owner
    }






}