package app.bitcoin.baemail.core.data.wallet.sending

import app.bitcoin.baemail.core.data.net.AppApiService
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.minidns.AbstractDnsClient
import org.minidns.hla.ResolverApi
import org.minidns.hla.SrvResolverResult
import org.minidns.record.A
import org.minidns.record.AAAA
import org.minidns.util.SrvUtil
import timber.log.Timber

/**
 * https://bsvalias.org/02-01-host-discovery.html
 */
class BsvAliasCapabilitiesHelper(
    private val apiService: AppApiService,
    private val gson: Gson
) {

    val cache = mutableMapOf<String, BsvAliasCapabilities>()


    //todo what about not retrying fails for the 99 time?
    //todo
    suspend fun getBsvAliasCapabilities(domain: String): BsvAliasCapabilities? =
            withContext(Dispatchers.IO) {
                cache[domain] ?: let {
                    queryBsvAliasCapabilities(domain)?.let {
                        cache[domain] = it
                        it
                    }
                }
            }


    private suspend fun queryBsvAliasCapabilities(domain: String): BsvAliasCapabilities? {

        val hostDomain = discoverBsvCapabilityHost(domain)

        //get bsvalias endpoints for the domain

        val endpointsMetaResponse = try {
            apiService.fetch("https://$hostDomain/.well-known/bsvalias").execute()
        } catch (e: Exception) {
            Timber.e(e, ">>>>>>>>>")
            return null
        }

        val metaResponseBody = endpointsMetaResponse.body()
        return if (endpointsMetaResponse.isSuccessful && metaResponseBody != null) {
            val jsonString = String(metaResponseBody.bytes())
            try {
                val json = gson.fromJson(jsonString, JsonObject::class.java)

                val capabilitiesJson: JsonObject = json.getAsJsonObject("capabilities")

                val pkiEndpoint = capabilitiesJson.getAsJsonPrimitive("pki").asString

                val paymentDestinationEndpoint = capabilitiesJson
                    .getAsJsonPrimitive("paymentDestination").asString

                val publicProfile = if (capabilitiesJson.has("f12f968c92d6")) {
                    capabilitiesJson.getAsJsonPrimitive("f12f968c92d6").asString
                } else null

                val p2pPaymentDestination = if (capabilitiesJson.has("2a40af698840")) {
                    capabilitiesJson.getAsJsonPrimitive("2a40af698840").asString
                } else null

                val submitP2pTransaction = if (capabilitiesJson.has("5f1323cddf31")) {
                    capabilitiesJson.getAsJsonPrimitive("5f1323cddf31").asString
                } else null

                BsvAliasCapabilities(
                    domain,
                    pkiEndpoint,
                    paymentDestinationEndpoint,
                    publicProfile,
                    p2pPaymentDestination,
                    submitP2pTransaction
                )
            } catch (e: Exception) {
                Timber.e(e, "response... length:${jsonString.length} content:$jsonString")
                return null
            }

        } else {
            Timber.e(RuntimeException("response-code: ${endpointsMetaResponse.code()}"))
            null
        }
    }

    private fun discoverBsvCapabilityHost(domain: String): String {
        return try {
            val serviceName = "_bsvalias._tcp.$domain"

            val resolvedResult = ResolverApi.INSTANCE.resolveSrv(serviceName)
            if (!resolvedResult.wasSuccessful()) {
                throw RuntimeException()
            }

            extractSrvTarget(resolvedResult) ?: domain


        } catch (e: Exception) {
            Timber.e(e)

            domain
        }
    }

    /**
     * SrvResolverResult#getSortedSrvResolvedAddresses() is too strict & returning empty results.
     * This is a modified version of that method.
     *
     * According to the protocol there should be either zero or one entries for `_bsvalias` SRV.
     */
    private fun extractSrvTarget(result: SrvResolverResult): String? {

        if (result.isServiceDecidedlyNotAvailableAtThisDomain) {
            return null
        }

        val resolver = ResolverApi.INSTANCE
        val ipVersion: AbstractDnsClient.IpVersionSetting = resolver.client.preferedIpVersion

        val srvRecords = SrvUtil.sortSrvRecords(result.answers)

        for (srvRecord in srvRecords) {
            if (ipVersion.v4) {
                val aRecordsResult = resolver.resolve(srvRecord.target, A::class.java)
                if (aRecordsResult.wasSuccessful()/* && !aRecordsResult.hasUnverifiedReasons()*/) {

                    return aRecordsResult.rawAnswer.question.name.toString()
                }
            }
            if (ipVersion.v6) {
                val aaaaRecordsResult = resolver.resolve(srvRecord.target, AAAA::class.java)
                if (aaaaRecordsResult.wasSuccessful()/* && !aaaaRecordsResult.hasUnverifiedReasons()*/) {

                    return aaaaRecordsResult.rawAnswer.question.name.toString()
                }
            }
        }


        return null
    }


}



data class BsvAliasCapabilities(
    val domain: String,
    val pkiEndpoint: String,
    val paymentDestinationEndpoint: String,
    val publicProfileEndpoint: String?,
    val p2pPaymentDestinationEndpoint: String?,
    val submitP2pTransactionEndpoint: String?
)