package app.bitcoin.baemail.core.data

import app.bitcoin.baemail.core.data.wallet.BlockInfo
import app.bitcoin.baemail.core.data.wallet.BlockStatsFee
import app.bitcoin.baemail.core.data.wallet.BlockStatsHelper
import app.bitcoin.baemail.core.data.wallet.BlockchainDataSource
import app.bitcoin.baemail.core.data.wallet.DynamicChainSync
import app.bitcoin.baemail.core.data.wallet.ExchangeRateHelper
import app.bitcoin.baemail.core.domain.repository.BsvStatusRepository
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class BsvStatusRepositoryImpl(
    private val blockStatsHelper: BlockStatsHelper,
    private val exchangeRateHelper: ExchangeRateHelper,
    private val dynamicChainSync: DynamicChainSync,
    private val blockchainDataSource: BlockchainDataSource,
    private val coroutineUtil: CoroutineUtil
): BsvStatusRepository {

    override val liveBlockInfo: Flow<BlockInfo?>
        get() = dynamicChainSync.liveBlockInfo

    override val txFee: Flow<BlockStatsFee>
        get() = blockStatsHelper.state.map { it.txFee }

    override val exchangeRate: Flow<ExchangeRateHelper.Model>
        get() = exchangeRateHelper.state

    override suspend fun sendTx(hexTx: String): Boolean =
        withContext(coroutineUtil.defaultDispatcher) {
            blockchainDataSource.sendHexTx(hexTx) != null
        }

    override suspend fun sendTxToBaemail(hexTx: String): Boolean =
        withContext(coroutineUtil.defaultDispatcher) {
            blockchainDataSource.sendTxToBaemail(hexTx)
        }
}