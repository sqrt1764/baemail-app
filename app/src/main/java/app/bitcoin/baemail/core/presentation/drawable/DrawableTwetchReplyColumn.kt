package app.bitcoin.baemail.core.presentation.drawable

import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.annotation.IntRange
import kotlin.math.min

class DrawableTwetchReplyColumn : Drawable() {


    private var columnColor = Color.RED
    private var columnWidthPx = 20
    private var columnPaddingLeft = 150
    private var columnPaddingTop = 0

    private var limitColumnHeightFromTop = false
    private var columnHeightFromTop = 0

    private var _alpha = 255

    private val strokePaint = Paint(Paint.ANTI_ALIAS_FLAG)


    fun setColumnColor(color: Int) {
        columnColor = color

        val finalAlpha = (((Color.alpha(color) / 255f) * (_alpha / 255f)) * 255).toInt()

        val finalColor = Color.argb(
            finalAlpha, Color.red(columnColor), Color.green(columnColor), Color.blue(columnColor)
        )

        strokePaint.color = finalColor

        invalidateSelf()
    }

    fun setColumnWidth(widthPx: Int) {
        columnWidthPx = widthPx

        invalidateSelf()
    }

    fun setColumnPaddingLeft(px: Int) {
        columnPaddingLeft = px

        invalidateSelf()
    }

    fun setColumnPaddingTop(px: Int) {
        columnPaddingTop = px

        invalidateSelf()
    }


    fun setColumnHeightLimitation(limitHeightFromTop: Boolean, maxHeightFromTop: Int) {
        limitColumnHeightFromTop = limitHeightFromTop
        columnHeightFromTop = maxHeightFromTop

        invalidateSelf()
    }


    override fun draw(canvas: Canvas) {
        if (_alpha == 0) return

        if (limitColumnHeightFromTop) {
            val finalColumnBottom = min(columnHeightFromTop, bounds.bottom)
            canvas.drawRect(
                bounds.left + columnPaddingLeft.toFloat(),
                bounds.top + columnPaddingTop.toFloat(),
                bounds.left + columnPaddingLeft.toFloat() + columnWidthPx,
                finalColumnBottom.toFloat(),
                strokePaint
            )


        } else {
            canvas.drawRect(
                bounds.left + columnPaddingLeft.toFloat(),
                bounds.top + columnPaddingTop.toFloat(),
                bounds.left + columnPaddingLeft.toFloat() + columnWidthPx,
                bounds.bottom.toFloat(),
                strokePaint
            )

        }
    }

    override fun setAlpha(@IntRange(from = 0, to = 255) alpha: Int) {
        _alpha = alpha

        setColumnColor(columnColor)
    }

    override fun getAlpha(): Int {
        return _alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        // not supported
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }
}