package app.bitcoin.baemail.core.data.wallet.sending

import app.bitcoin.baemail.core.data.util.AuthenticatedPaymail
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.domain.entities.SigningResponse
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import timber.log.Timber

class SignMessageHelper {
    suspend fun signMessageWithPaymailPki(
        sendingPaymail: String,
        message: String,
        secureDataSource: SecureDataSource,
        nodeRuntimeRepository: NodeRuntimeRepository
    ): SigningResponse? {
        val paymailSeed = try {
            secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(sendingPaymail))
        } catch (e: Exception) {
            Timber.d("failed to retrieve active-paymail wallet-seed")
            return null
        }

        val pkiPath = try {
            secureDataSource.getPathForPaymailMnemonic(AuthenticatedPaymail(sendingPaymail))
        } catch (e: Exception) {
            Timber.d("failed to retrieve active-paymail pki-path")
            return null
        }

        return signMessageWithPaymailPki(
            paymailSeed,
            pkiPath,
            message,
            nodeRuntimeRepository
        )
    }

    suspend fun signMessageWithPaymailPki(
        paymailSeed: List<String>,
        pkiPath: String,
        message: String,
        nodeRuntimeRepository: NodeRuntimeRepository
    ): SigningResponse? {
        val keyHolder = nodeRuntimeRepository.getPrivateKeyForPathFromSeed(
            pkiPath,
            paymailSeed
        ) ?: let {
            Timber.d("error retrieving the wif-private-key for path")
            return null
        }


        val signature = nodeRuntimeRepository.signMessage(
            message,
            keyHolder.privateKey
        ) ?: let {
            Timber.d("error signing paymail-address-resolution message")
            return null
        }

        return SigningResponse(
            signature,
            keyHolder.publicKey
        )
    }
}