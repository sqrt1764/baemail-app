package app.bitcoin.baemail.core.domain.usecases

import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import app.bitcoin.baemail.core.domain.repository.PaymailInfoRepository
import kotlinx.coroutines.flow.Flow

class CheckPaymailExistsUseCaseImpl(
    private val paymailInfoRepository: PaymailInfoRepository
) : CheckPaymailExistsUseCase {
    override fun invoke(paymail: String): Flow<PaymailExistence> =
        paymailInfoRepository.checkExistence(paymail)

}

interface CheckPaymailExistsUseCase {
    operator fun invoke(paymail: String): Flow<PaymailExistence>
}