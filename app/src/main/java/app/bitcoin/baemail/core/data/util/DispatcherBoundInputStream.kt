package app.bitcoin.baemail.core.data.util

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.runBlocking
import java.io.InputStream

class DispatcherBoundInputStream(
    val inStream: InputStream,
    val dispatcher: CoroutineDispatcher
) : InputStream() {

    override fun read(): Int {
        return runBlocking(dispatcher) {
            inStream.read()
        }
    }

    override fun read(b: ByteArray?): Int {
        return runBlocking(dispatcher) {
            inStream.read(b)
        }
    }

    override fun read(b: ByteArray?, off: Int, len: Int): Int {
        return runBlocking(dispatcher) {
            inStream.read(b, off, len)
        }
    }

    override fun skip(n: Long): Long {
        return runBlocking(dispatcher) {
            inStream.skip(n)
        }
    }

    override fun available(): Int {
        return runBlocking(dispatcher) {
            inStream.available()
        }
    }

    override fun close() {
        runBlocking(dispatcher) {
            inStream.close()
        }
    }

    override fun mark(readlimit: Int) {
        runBlocking(dispatcher) {
            inStream.mark(readlimit)
        }
    }

    override fun reset() {
        runBlocking(dispatcher) {
            inStream.reset()
        }
    }

    override fun markSupported(): Boolean {
        return runBlocking(dispatcher) {
            inStream.markSupported()
        }
    }

}