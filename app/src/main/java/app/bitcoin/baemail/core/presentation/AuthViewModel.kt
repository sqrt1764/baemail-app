package app.bitcoin.baemail.core.presentation

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.di.SavedStateViewModelAssistant
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class AuthViewModel @AssistedInject constructor(
    @Assisted private val state : SavedStateHandle,
    getActivePaymailUseCase: GetActivePaymailUseCase
) : ViewModel() {

    @AssistedFactory
    interface Assistant : SavedStateViewModelAssistant<AuthViewModel> {
        override fun create(savedStateHandle: SavedStateHandle): AuthViewModel
    }

    val activePaymail = getActivePaymailUseCase()

}