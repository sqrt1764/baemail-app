package app.bitcoin.baemail.core.domain.usecases

import android.util.Patterns

class CheckIsValidEmailUseCaseImpl : CheckIsValidEmailUseCase {
    override fun invoke(paymail: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(paymail).matches()
    }

}

interface CheckIsValidEmailUseCase {
    operator fun invoke(paymail: String): Boolean
}