package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.core.domain.entities.ReceivedCoin
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber
import java.lang.Exception

/*
#expecting params
[
    ['seed', 'words', ..],
    [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/0/13'}, {..}],
    "m/44'/0'/0'", //spendingCoinRootPath
    [
        {
            address: '1FFF..EE',
            sats: 123
        },
        {
            address: '1F99..EE',
            sats: 1234
        },
        ..
    ] //destination address info list
]

# response
[
    {
        'hexTx': '1f1f1f1f1f',
        'hash': '1f1f',
        'outputs': [
            { address: '1C6BBGPhq57UCA1DxK5HQkT7Essr8zzLXJ' outputIndex: 3, sats: 123},
            ..
        ]
    }
]
*/
class CreateEqualSplitCoinTx( //todo rename & get rid of 'equal'
    val seed: List<String>,
    val spendingCoins: Map<String, ReceivedCoin>, //key: derivation-path value: utxo
    val spendingCoinRootPath: String,
    val destinationAddressInfo: List<Pair<String, Long>>,
    val gson: Gson,
    val callback: (TxInfoEqualSplit?)->Unit
) : NodeRuntimeInterface.NodeRequest("CREATE_EQUAL_SPLIT_COIN_TX") {

    override fun getReqParams(): JsonArray {
        val jsonSeedArray = JsonArray()
        seed.forEach {
            jsonSeedArray.add(it)
        }

        val jsonSpendingCoinsArray = JsonArray()
        spendingCoins.forEach {
            val jsonObject = gson.toJsonTree(it.value).asJsonObject
            jsonObject.addProperty("derivation_path", it.key)
            jsonSpendingCoinsArray.add(jsonObject)
        }

        val jsonDestinationAddresses = JsonArray()
        destinationAddressInfo.forEach {
            val infoObject = JsonObject()
            infoObject.addProperty("address", it.first)
            infoObject.addProperty("sats", it.second)
            jsonDestinationAddresses.add(infoObject)
        }

        val wrappingJsonArray = JsonArray()
        wrappingJsonArray.add(jsonSeedArray)
        wrappingJsonArray.add(jsonSpendingCoinsArray)
        wrappingJsonArray.add(spendingCoinRootPath)
        wrappingJsonArray.add(jsonDestinationAddresses)

        return wrappingJsonArray
    }

    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "error creating equal-split-tx; token: $token")
            callback(null)
            return
        }

        try {
            val dataJson = responseData ?: throw RuntimeException("data missing")

            val envelopeJson = dataJson.get(0).asJsonObject
            val txHex = envelopeJson.get("hexTx").asString
            val txHash = envelopeJson.get("hash").asString

            val outputArray = envelopeJson.getAsJsonArray("outputs")
            val outputs = mutableListOf<TxOutput>()
            outputArray.forEach { element ->
                val address = element.asJsonObject.get("address").asString
                val outputIndex = element.asJsonObject.get("outputIndex").asInt
                val sats = element.asJsonObject.get("sats").asLong
                outputs.add(TxOutput(
                    address,
                    outputIndex,
                    sats
                ))
            }

            callback(TxInfoEqualSplit(
                txHex,
                txHash,
                outputs
            ))

        } catch (e: Exception) {
            Timber.e(e, "token: $token")
            callback(null)
        }
    }

}

data class TxOutput(
    val address: String,
    val outputIndex: Int,
    val sats: Long
)

data class TxInfoEqualSplit(
    val txHex: String,
    val txHash: String,
    val outputs: List<TxOutput>
)