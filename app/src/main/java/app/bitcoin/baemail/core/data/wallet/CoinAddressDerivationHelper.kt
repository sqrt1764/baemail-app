package app.bitcoin.baemail.core.data.wallet

import app.bitcoin.baemail.core.data.nodejs.request.DerivedAddress
import app.bitcoin.baemail.core.data.room.dao.CachedDerivedAddressDao
import app.bitcoin.baemail.core.data.room.entity.CachedDerivedAddress
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository

class CoinAddressDerivationHelper(
    private val cachedDerivedAddressDao: CachedDerivedAddressDao,
    private val nodeRuntimeRepository: NodeRuntimeRepository
) {
    suspend fun deriveAddressSet(
        paymail: String,
        seed: List<String>,
        path: String,
        setIndex: Int
    ): List<DerivedAddress> {

        //first check the addresses among cached
        val listFromCached = let {
            val lower = 20 * setIndex
            val upper = 20 * (setIndex + 1)
            val gotList = cachedDerivedAddressDao.getCachedAddresses(paymail, lower, upper)
            if (gotList.size != 20) {
                return@let null
            }

            gotList.map {
                DerivedAddress(
                    DerivedAddress.getPathOfIndex(it.pathIndex),
                    it.address
                )
            }
        }

        listFromCached?.let {
            return it
        }



        val generatedList = nodeRuntimeRepository.deriveNextAddressSet(seed, path, setIndex)
        //cache the results
        generatedList.map { CachedDerivedAddress(
            paymail,
            it.getIndex(),
            it.address
        ) }.let {
            cachedDerivedAddressDao.cacheThis(it)
        }

        return generatedList
    }
}