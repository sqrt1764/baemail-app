package app.bitcoin.baemail.core.presentation.compose

import androidx.compose.runtime.Composable
import com.google.android.material.composethemeadapter.MdcTheme

@Composable
fun ComposeAppTheme(
    content: @Composable () -> Unit) {
    MdcTheme(
        content = content
    )
}