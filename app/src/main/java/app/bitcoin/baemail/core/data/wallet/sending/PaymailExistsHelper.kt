package app.bitcoin.baemail.core.data.wallet.sending

import android.util.Patterns
import app.bitcoin.baemail.core.data.net.AppApiService
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class PaymailExistsHelper(
    private val apiService: AppApiService,
    private val bsvAliasCapabilitiesHelper: BsvAliasCapabilitiesHelper,
    private val gson: Gson,
    private val coroutineUtil: CoroutineUtil
) {

    private val invalidPaymailFormatResponse = MutableStateFlow<Pair<State, PaymailInfo?>>(
        Pair(State.INVALID, null)
    ).asStateFlow()

    private val checkedPaymailCache =
        mutableMapOf<String, MutableStateFlow<Pair<State, PaymailInfo?>>>()


    //todo return Pair<State, PaymailInfo?> instead
    fun checkExistence(paymailToCheck: String): StateFlow<Pair<State, PaymailInfo?>> {
        val isValidAddress = Patterns.EMAIL_ADDRESS.matcher(paymailToCheck).matches()

        if (!isValidAddress) {
            return invalidPaymailFormatResponse
        }

        //return from cache if mapping exists
        checkedPaymailCache[paymailToCheck]?.let {
            return it.asStateFlow()
        }

        //start checking
        return doCheckExistence(paymailToCheck)
    }

    private fun doCheckExistence(paymailToCheck: String): StateFlow<Pair<State, PaymailInfo?>> {

        val flow = MutableStateFlow<Pair<State, PaymailInfo?>>(Pair(State.CHECKING, null))

        checkedPaymailCache[paymailToCheck] = flow

        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            val aliasResp = checkPaymailExists(paymailToCheck)
            if (aliasResp.first != null) {
                flow.value = Pair(State.VALID, aliasResp.first)
            } else {
                flow.value = Pair(State.INVALID, null)
            }
        }

        return flow.asStateFlow()
    }

    suspend fun checkPaymailExists(
        paymail: String
    ): Pair<PaymailInfo?, Exception?> {
        return withContext(Dispatchers.IO) {

            val handle = paymail.substring(0, paymail.indexOf("@"))
            val domain = paymail.substring(paymail.indexOf("@") + 1)

            val capabilities = bsvAliasCapabilitiesHelper.getBsvAliasCapabilities(domain)
                ?: return@withContext Pair(null, RuntimeException())

            var assembled = capabilities.pkiEndpoint.replace("{alias}", handle)
            assembled = assembled.replace("{domain.tld}", domain)

            val response = try {
                apiService.fetch(assembled).execute()
            } catch (e: Exception) {
                return@withContext Pair(null, e)
            }
            if (response.isSuccessful) {
                response.body()?.let {
                    try {
                        val alias = gson.fromJson(it.charStream(), BsvAlias::class.java)
                        return@withContext Pair(
                            PaymailInfo(
                                alias,
                                capabilities.publicProfileEndpoint != null,
                                capabilities.p2pPaymentDestinationEndpoint != null &&
                                        capabilities.submitP2pTransactionEndpoint != null
                            ),
                            null
                        )
                    } catch (e: Exception) {
                        Timber.e(e)

                        return@withContext Pair(null, e)
                    }
                }
            }

            return@withContext Pair(null, null)
        }
    }

    enum class State {
        CHECKING,
        VALID,
        INVALID
    }
}