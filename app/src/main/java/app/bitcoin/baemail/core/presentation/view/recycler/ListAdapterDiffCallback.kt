package app.bitcoin.baemail.core.presentation.view.recycler

import androidx.recyclerview.widget.DiffUtil

class ListAdapterDiffCallback(
    val oldList: List<AdapterType>,
    val newList: List<AdapterType>
) : DiffUtil.Callback() {

    private val diffCallback = AdapterTypeDiffCallback()

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val itemOld = oldList[oldItemPosition]
        val itemNew = newList[newItemPosition]

        return diffCallback.areItemsTheSame(itemOld, itemNew)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val itemOld = oldList[oldItemPosition]
        val itemNew = newList[newItemPosition]

        return diffCallback.areContentsTheSame(itemOld, itemNew)
    }
}