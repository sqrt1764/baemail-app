package app.bitcoin.baemail.core.data.nodejs.handler

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.p2p.InformP2pChangeHelper
import com.google.gson.JsonArray

/*
expected structure
[
    {
        type: type,
        model: {..}
    }
]
*/
class InformP2pEventHandler(
    private val informP2pChangeHelper: InformP2pChangeHelper
) : NodeRuntimeInterface.WorkHandler("INFORM_P2P_EVENT") {

    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<NodeRuntimeInterface.Result, JsonArray> {

        val obj = params[0].asJsonObject
        val type = obj.get("type").asString

        informP2pChangeHelper.postEvent(
            InformP2pChangeHelper.P2pEvent(
            type
        ))

        return NodeRuntimeInterface.Result.SUCCESS to JsonArray()
    }

}