package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.core.domain.entities.FundingCoinInfo
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber
import kotlin.RuntimeException

/*

#expecting params
[
    {
        activePaymail: 'aa@moneybutton.com',
        syncStatus: 'disconnected'
        fundingWalletSeed: ['seed', 'words', ..],
        fundingRootPath: 'm/44'/0'/ ..',
        coins: [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/0/13'}, {..}],
        upcomingChangeAddressList: [{'address': '1abcedf..', 'derivation_path':'/0/13'}, {'address': '1abcedf..', 'derivation_path':'/0/14'}, ..]
    }
]

syncStatus {
    disconnected,
    initialising,
    live,
    error
}



# response
[
    {
        informed: true
    }
]

 */
class InformAboutAvailableCoins(
    val info: FundingCoinInfo,
    val callback: (Boolean)->Unit
) : NodeRuntimeInterface.NodeRequest("INFORM_ABOUT_AVAILABLE_COINS") {

    override fun getReqParams(): JsonArray {

        val stringSyncStatus = when (info.syncStatus) {
            ChainSyncStatus.DISCONNECTED -> "disconnected"
            ChainSyncStatus.INITIALISING -> "initialising"
            ChainSyncStatus.LIVE -> "live"
            ChainSyncStatus.ERROR -> "error"
            else -> throw RuntimeException()
        }

        val fundingSeedArray = JsonArray().let { jsonArr ->
            info.fundingWalletSeed.forEach {
                jsonArr.add(it)
            }
            jsonArr
        }

        val fundingCoinsArray = JsonArray().let { jsonArr ->
            info.unspentCoins.forEach { coin ->
                val jsonObj = JsonObject()
                jsonObj.addProperty("tx_id", coin.txId)
                jsonObj.addProperty("sats", coin.sats)
                jsonObj.addProperty("index", coin.outputIndex)
                jsonObj.addProperty("address", coin.address)
                jsonObj.addProperty("derivation_path", coin.derivationPath)

                jsonArr.add(jsonObj)
            }
            jsonArr
        }

        val upcomingChangeAddressList = JsonArray().let { jsonArr ->
            info.upcomingChangeAddressList.forEach { derivedAddress ->
                val jsonObj = JsonObject()
                jsonObj.addProperty("address", derivedAddress.address)
                jsonObj.addProperty("derivation_path", derivedAddress.path)

                jsonArr.add(jsonObj)
            }
            jsonArr
        }

        val jsonObj = JsonObject()
        jsonObj.addProperty("activePaymail", info.activePaymail)
        jsonObj.addProperty("syncStatus", stringSyncStatus)
        jsonObj.add("fundingWalletSeed", fundingSeedArray)
        jsonObj.addProperty("fundingRootPath", info.fundingRootPath)
        jsonObj.add("coins", fundingCoinsArray)
        jsonObj.add("upcomingChangeAddressList", upcomingChangeAddressList)





        val finalArray = JsonArray()
        finalArray.add(jsonObj)

        return finalArray
    }


    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "available coins changes not propagated; token: $token")
            callback(false)
            return
        }


        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(stackTrace, "unexpected response format; token: $token")
            callback(false)
            return
        }



        try {
            callback(data[0].asJsonObject.get("informed").asBoolean)

        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")
            callback(false)
        }
    }

}