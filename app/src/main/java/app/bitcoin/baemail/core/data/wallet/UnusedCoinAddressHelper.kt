package app.bitcoin.baemail.core.data.wallet

import app.bitcoin.baemail.core.data.nodejs.request.DerivedAddress
import app.bitcoin.baemail.core.data.util.AuthenticatedPaymail
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.data.room.dao.CoinDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlin.jvm.Throws


class UnusedCoinAddressHelper(
    private val appUtilDiskDAO: AppUtilDiskDAO,
    private val chainSync: DynamicChainSync,
    private val coinDao: CoinDao,
    private val secureDataSource: SecureDataSource,
    private val addressDerivationHelper: CoinAddressDerivationHelper,
    private val scope: CoroutineScope
) {

    private val highestUsedAddressMap: MutableMap<String, MutableStateFlow<Int>> = mutableMapOf()


    fun setup() {
        scope.launch {

            restoreFromDiskCache()

            chainSync.highestUsedAddressRef
                .combine(
                    coinDao.getHighestCoinAddressByPaymail()
                ) { highestUsedAddressRef, highestCoinAddressByPaymail ->
                    highestUsedAddressRef to highestCoinAddressByPaymail
                }.collect { (syncedHighestUsedAddressRef, highestCoinAddressByPaymail) ->
                    syncedHighestUsedAddressRef ?: return@collect

                    val synchronizedPaymail = syncedHighestUsedAddressRef.paymail

                    var highestAddressIndex = syncedHighestUsedAddressRef.index
                    highestCoinAddressByPaymail.find {
                        it.paymail == synchronizedPaymail
                    }?.addressPathIndex?.let { index ->
                        if (highestAddressIndex < index) {
                            highestAddressIndex = index
                        }
                    }

                    //first unused address is the next one after the highest used address
                    val stateFlow = highestUsedAddressMap[synchronizedPaymail] ?: let {
                        val f = MutableStateFlow(0)
                        highestUsedAddressMap[synchronizedPaymail] = f
                        f
                    }
                    if (stateFlow.value < highestAddressIndex) {
                        stateFlow.value = highestAddressIndex
                    }

                    persistToDiskCache()
                }
        }
    }

    private suspend fun restoreFromDiskCache() {
        appUtilDiskDAO.readHighestUsedCoins().forEach { (paymail, state) ->
            highestUsedAddressMap[paymail]?.let { stateFlow ->
                stateFlow.value = state.value
            } ?: let {
                highestUsedAddressMap[paymail] = state
            }

        }
        highestUsedAddressMap.putAll(appUtilDiskDAO.readHighestUsedCoins())
    }

    private suspend fun persistToDiskCache() {
        appUtilDiskDAO.writeHighestUsedCoins(highestUsedAddressMap)
    }

    fun getHighestUsedAddressIndex(paymail: String): StateFlow<Int> {
        val stateFlow = highestUsedAddressMap[paymail] ?: let {
            val f = MutableStateFlow(0)
            highestUsedAddressMap[paymail] = f
            f
        }

        return stateFlow
    }

    @Throws(RuntimeException::class)
    fun getUpcomingUnusedAddresses(paymail: String, count: Int = 20): Flow<List<DerivedAddress>> {
        val highestUsedAddressIndex = highestUsedAddressMap[paymail]
            ?: throw RuntimeException("not initialized for paymail: $paymail")

        return highestUsedAddressIndex.flatMapLatest { addressIndex ->
            flow {
                val seed = secureDataSource.getParallelWalletSeed(AuthenticatedPaymail(paymail))
                val path = secureDataSource.getPathForFundingMnemonic(AuthenticatedPaymail(paymail))

                val firstUnusedCoinAddressIndex = addressIndex + 1

                val setIndex = firstUnusedCoinAddressIndex / 20
                val offset = firstUnusedCoinAddressIndex % 20

                val finalList = mutableListOf<DerivedAddress>()

                while (true) {
                    addressDerivationHelper.deriveAddressSet(
                        paymail,
                        seed,
                        path,
                        setIndex
                    ).let {
                        finalList.addAll(it)
                    }

                    if(finalList.size - offset >= count) break
                }

                finalList.subList(offset, offset + count).let {
                    if (it[0].getIndex() != firstUnusedCoinAddressIndex)
                        throw RuntimeException("expected: $firstUnusedCoinAddressIndex; got:${it[0].getIndex()}") //todo
                    emit(it)
                }

            }
        }
    }

    suspend fun clearCachedHighestAddress(paymail: String) {
        highestUsedAddressMap.remove(paymail)
        persistToDiskCache()
    }


}