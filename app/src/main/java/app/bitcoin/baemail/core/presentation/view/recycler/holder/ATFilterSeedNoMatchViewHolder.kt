package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATFilterSeedNoMatch
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder

class ATFilterSeedNoMatchViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    var model: ATFilterSeedNoMatch? = null

    init {
        //todo
        //todo
        //todo
    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATFilterSeedNoMatch
    }


    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATFilterSeedNoMatchViewHolder {
            return ATFilterSeedNoMatchViewHolder(
                inflater.inflate(
                    R.layout.li_filter_seed_no_match,
                    parent,
                    false
                )
            )
        }
    }
}