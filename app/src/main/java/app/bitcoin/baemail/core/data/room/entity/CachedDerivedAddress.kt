package app.bitcoin.baemail.core.data.room.entity

import androidx.room.Entity
import app.bitcoin.baemail.core.data.room.TABLE_CACHED_DERIVED_ADDRESS

@Entity(tableName = TABLE_CACHED_DERIVED_ADDRESS, primaryKeys = ["paymail", "pathIndex"])
data class CachedDerivedAddress(
    val paymail: String,
    val pathIndex: Int,
    val address: String,
)