package app.bitcoin.baemail.core.data.nodejs.request


import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber
import java.lang.RuntimeException

/*
# expected structure of params
[
{
    "content": ";"
}
]


# expected structure of the response data passed in the callback

# success
[
{
    content: "content",
    logs: "logs",
    success: true
}
]

# error
[
"stack-trace of exception"
]
*/
class RunScript(
        val content: String,
        val gson: Gson,
        val callback: (Response)->Unit
) : NodeRuntimeInterface.NodeRequest("RUN_SCRIPT") {



    override fun getReqParams(): JsonArray {

        val jsonReq = JsonObject()
        jsonReq.addProperty("content", content)

        val finalArray = JsonArray()
        finalArray.add(jsonReq)

        return finalArray
    }




    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            val e = RuntimeException(responseData?.toString())
            Timber.e(e, "script crashed; token: $token")
//            Timber.e(e, "script crashed; token: $token\n\n$content") //todo undo printing of content
            callback(Response.Fail(
                    e,
                    responseData!!.toString()
            ))

            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            val e = RuntimeException()
            Timber.e(e, "unexpected response format; token: $token")
            callback(Response.Fail(
                    e,
                    responseData!!.toString()
            ))

            return
        }


        try {
            val response = data[0].asJsonObject

            if (responseResult == NodeRuntimeInterface.Result.SUCCESS) {


                val parsedLogs = mutableListOf<String>()
                try {
                    val jsonArray = response.get("logs").asJsonArray //gson.fromJson(result.logs, JsonArray::class.java)
                    jsonArray.map {
                        it.asString.trim()
                    }.toCollection(parsedLogs)

                } catch (e: Exception) {
                    Timber.e(e)
                }

                val content = if (response.has("content")) {
                    response.get("content").asString
                } else {
                    "undefined"
                }

                val success = if (response.has("success")) {
                    response.get("success").asBoolean
                } else {
                    false
                }

                if (success) {
                    callback(
                        Response.Success(
                            content,
                            parsedLogs
                        )
                    )
                } else {
                    callback(
                        Response.Fail(
                            RuntimeException(content),
                            parsedLogs.toString() //todo
                        )
                    )
                }
            } else {

                callback(Response.Fail(
                        RuntimeException(),
                        responseData!!.toString()
                ))
            }

        } catch (e: Exception) {
            Timber.e(e, "not expected formatting; token: $token")

            callback(Response.Fail(
                    e,
                    responseData?.toString() ?: ";"
            ))
        }
    }


    sealed class Response {
        data class Success(
                val content: String,
                val logs: List<String>
        ) : Response()
        data class Fail(
                val throwable: Throwable,
                val responseData: String
        ) : Response()
    }

}