package app.bitcoin.baemail.core.data.nodejs.request

import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.core.domain.entities.ReceivedCoin
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber
import java.lang.Exception

/*
#expecting params
[
    {
        sendingPaymail: {
            paymail: 'aa@moneybutton.com',
            seed: ['seed', 'words', ..],
            pkiPath: 'm/44'/0'/ ..'
        },
        fundingWallet: {
            seed: ['seed', 'words', ..],
            rootPath: 'm/44'/0'/ ..',
            coins: [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/0/13'}, {..}],
            changeAddress: '1abcedf..'
        },
        message: {
            destination: 'receiver@moneybutton.com',
            destinationPkiPublicKey: '00abc',
            subject: 'subject01',
            content: 'content 01',
            amountToPaymailUsd: 0.123,
            satsInACent: 2345,
            satsPerByteFee: 0.12,
            payToPaymailOutputScript: '0a010203..',
            amountToBaemailSats: 2345
        }
    }
]

# response
[
    {
        'hexTx': '1f1f1f1f1f',
        'spentCoins': [
            {
                tx_id: '00..ee',
                address: '1eee..ee',
                index: 2
            },
            {
                tx_id: '00..ee',
                address: '1eee..ee',
                index: 2
            },
            {
                tx_id: '00..ee',
                address: '1eee..ee',
                index: 2
            },
            ..
        ],
        'id': 'aa..ff'
    }
]
*/
class CreateBaemailMessageTx(
    val sendingPaymail: SendingPaymail,
    val fundingWallet: FundingWallet,
    val message: Message,
    val gson: Gson,
    val callback: (Tx?)->Unit
) : NodeRuntimeInterface.NodeRequest("CREATE_BAEMAIL_MESSAGE_TX") {

    override fun getReqParams(): JsonArray {
        //sendingPaymail
        val jsonSendingPaymail = JsonObject().also {

            it.addProperty("paymail", sendingPaymail.paymail)
            it.addProperty("pkiPath", sendingPaymail.pkiPath) // .. mb pki path: "m/44'/0'/0'/0/0/0"

            val jsonSeedArray = JsonArray()
            sendingPaymail.seed.forEach {
                jsonSeedArray.add(it)
            }
            it.add("seed", jsonSeedArray)
        }

        val jsonFundingWallet = JsonObject().also {

            val jsonSeedArray = JsonArray()
            fundingWallet.seed.forEach {
                jsonSeedArray.add(it)
            }
            it.add("seed", jsonSeedArray)

            it.addProperty("rootPath", fundingWallet.rootPath)
            it.addProperty("changeAddress", fundingWallet.changeAddress)

            val jsonSpendingCoinsArray = JsonArray()
            fundingWallet.coins.forEach {
                val jsonObject = gson.toJsonTree(it.value).asJsonObject
                jsonObject.addProperty("derivation_path", it.key)
                jsonSpendingCoinsArray.add(jsonObject)
            }

            it.add("coins", jsonSpendingCoinsArray)
        }

        val jsonMessage = JsonObject().also {
            it.addProperty("destination", message.destination)
            it.addProperty("destinationPkiPublicKey", message.destinationPkiPublicKey)
            it.addProperty("subject", message.subject)
            it.addProperty("content", message.content)
            it.addProperty("amountToPaymailUsd", message.amountToPaymailUsd)
            it.addProperty("satsInACent", message.satsInACent)
            it.addProperty("satsPerByteFee", message.satsPerByteFee)
            it.addProperty("payToPaymailOutputScript", message.payToPaymailOutputScript)
            it.addProperty("amountToBaemailSats", message.amountToBaemailSats)

            if (message.thread != null) {
                it.addProperty("thread", message.thread)
            }
        }


        val finalJson = JsonObject().also {
            it.add("sendingPaymail", jsonSendingPaymail)
            it.add("fundingWallet", jsonFundingWallet)
            it.add("message", jsonMessage)
        }




        val wrappingJsonArray = JsonArray()
        wrappingJsonArray.add(finalJson)

        return wrappingJsonArray
    }


    override fun onResponse() {
        if (responseResult != NodeRuntimeInterface.Result.SUCCESS) {
            Timber.e(stackTrace, "error creating baemail-message tx; token: $token")
            callback(null)
            return
        }

        try {
            val dataJson = responseData ?: throw RuntimeException("data missing")

            val envelopeJson = dataJson.get(0).asJsonObject
            val hexTx = envelopeJson.get("hexTx").asString
            val id = envelopeJson.get("id").asString
            val spentCoinList = envelopeJson.getAsJsonArray("spentCoins").let {
                val parsedList = mutableListOf<SpentOutput>()

                it.forEach { item ->
                    val outputObj = item.asJsonObject

                    parsedList.add(SpentOutput(
                        outputObj.get("tx_id").asString,
                        outputObj.get("address").asString,
                        outputObj.get("index").asInt
                    ))

                }

                parsedList
            }


            callback(
                Tx(
                    hexTx,
                    spentCoinList,
                    id
                )
            )

        } catch (e: Exception) {
            Timber.e(e, "token: $token")
            callback(null)
        }
    }







    data class SendingPaymail(
        val paymail: String,
        val seed: List<String>,
        val pkiPath: String
    )

    data class FundingWallet(
        val seed: List<String>,
        val rootPath: String,
        val coins: Map<String, ReceivedCoin>, //key: derivation-path value: utxo
        val changeAddress: String
    )

    data class Message(
        val thread: String?,
        val destination: String,
        val destinationPkiPublicKey: String,
        val subject: String,
        val content: String,
        val amountToPaymailUsd: Float,
        val satsInACent: Int,
        val satsPerByteFee: Float,
        val payToPaymailOutputScript: String,
        val amountToBaemailSats: Int,
    )




    data class SpentOutput(
        val txId: String,
        val address: String,
        val index: Int
    )

    data class Tx(
        val hexTx: String,
        val spentCoinList: List<SpentOutput>,
        val id: String
    )

}