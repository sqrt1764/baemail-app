package app.bitcoin.baemail.core.domain.entities

data class InfoModel(
    val paymail: String,
    val paymailPath: String,
    val paymailMnemonic: List<String>,
    val fundingPath: String,
    val fundingMnemonic: List<String>
)