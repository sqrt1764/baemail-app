package app.bitcoin.baemail.core.presentation.view

import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.core.os.postDelayed
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.*
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.TransferListener
import com.google.android.exoplayer2.util.Util
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collectLatest
import timber.log.Timber

import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.upstream.cache.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.File


class ViewVideoClip : FrameLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    val playerView: PlayerView// = PlayerView(context)

    private var lifecycleOwner: LifecycleOwner? = null
    private var sourceObservationJob: Job? = null
    private var delayedShowingJob: Job? = null

    private var requestedSoundOn = false

    init {
        View.inflate(context, R.layout.view_video_clip, this)

        playerView = findViewById(R.id.player)

        playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
        playerView.useController = false

        playerView.alpha = 0f
        playerView.translationZ = -1 * resources.displayMetrics.density

        playerView.setKeepContentOnPlayerReset(true)

        //addView(playerView, LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT))
    }

    fun hideImmediately() {
        delayedShowingJob?.cancel()

        visibility = View.INVISIBLE
        playerView.alpha = 0f
        playerView.translationZ = -1 * resources.displayMetrics.density
    }

    fun showWithDelay() {
        delayedShowingJob?.cancel()

        playerView.alpha = 0f
        playerView.translationZ = 0f
        visibility = View.VISIBLE
        lifecycleOwner?.let {
            delayedShowingJob = it.lifecycleScope.launchWhenStarted {
                delay(600)

//                playerView.translationZ = 0f
                playerView.alpha = 1f
            }
        } ?: let {
            //no delay
            playerView.alpha = 1f
//            playerView.translationZ = 0f
        }
    }


    fun setup(
        lifecycleOwner: LifecycleOwner,
        source: Flow<VideoClip>,
        showController: Boolean = false,
        fitContent: Boolean = false,
        soundOn: Boolean = false
    ) {
        playerView.useController = showController
        requestedSoundOn = soundOn

        if (fitContent) {
            playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
        } else {
            playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
        }

        if (lifecycleObserver.attachedLifecycle == lifecycleOwner.lifecycle) return
        this.lifecycleOwner = lifecycleOwner

        sourceObservationJob?.cancel()
        sourceObservationJob = lifecycleOwner.lifecycleScope.launchWhenStarted {
            try {
                source.collect { clip ->
                    Timber.d("player-view is now observing source-flow")
                    //playerView.player = clip.player
                    lifecycleObserver.initPlayingState(clip.player)
                    clip.handlePlayerSwap(this@ViewVideoClip)

                }

            } finally {
                Timber.d("player-view cleanup")
                playerView.player = null
            }
        }

        lifecycleObserver.attachToLifecycle(lifecycleOwner.lifecycle)
    }

    private var isPlayerViewResumed = false

    private val lifecycleObserver = object : DefaultLifecycleObserver {

        var attachedLifecycle: Lifecycle? = null
        var shouldVideoBePlaying = false

        fun initPlayingState(gotPlayer: Player?) {
            if (shouldVideoBePlaying) {
                Timber.d("initPlayingState shouldVideoBePlaying:$shouldVideoBePlaying ...has player:${playerView.player != null}")
                gotPlayer?.let {
//                    it.play()

                    if (!isPlayerViewResumed) {
                        isPlayerViewResumed = true
                        playerView.onResume()
                    }

                    if (requestedSoundOn) {
                        it.volume = 1f
                    } else {
                        it.volume = 0f
                    }

                } ?: let {
                    if (isPlayerViewResumed) {
                        isPlayerViewResumed = false
                        playerView.onPause()
                    }

                }
            } else {
                Timber.d("initPlayingState shouldVideoBePlaying:$shouldVideoBePlaying")
//                playerView.player?.pause()
                if (isPlayerViewResumed) {
                    isPlayerViewResumed = false
                    playerView.onPause()

                }
            }
        }

        fun attachToLifecycle(lifecycle: Lifecycle) {
            if (attachedLifecycle == lifecycle) return
            if (attachedLifecycle != null) throw RuntimeException()
            attachedLifecycle = lifecycle
            shouldVideoBePlaying = false
            lifecycle.addObserver(this)
        }

        private fun detach() {
            attachedLifecycle?.removeObserver(this)
            attachedLifecycle = null
            lifecycleOwner = null
            shouldVideoBePlaying = false
            initPlayingState(playerView.player)
        }

        override fun onStart(owner: LifecycleOwner) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                shouldVideoBePlaying = true
                initPlayingState(playerView.player)
            }
        }

        override fun onResume(owner: LifecycleOwner) {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                shouldVideoBePlaying = true
                initPlayingState(playerView.player)
            }
        }

        override fun onPause(owner: LifecycleOwner) {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                shouldVideoBePlaying = false
                initPlayingState(playerView.player)
            }
        }

        override fun onStop(owner: LifecycleOwner) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                shouldVideoBePlaying = false
                initPlayingState(playerView.player)
            }
        }

        override fun onDestroy(owner: LifecycleOwner) {
            detach()
        }
    }

}






data class VideoClip(
    val uri: String,
    val player: Player?,
    val handlePlayerSwap: suspend (ViewVideoClip)->Unit
)





class VideoClipManager(
    val factory: ExoFactory,
    val coroutineUtil: CoroutineUtil
) {

    companion object {
        private const val CLEANUP_DELAY = 30 * 1000L
    }

    private val cache = HashMap<String, MutableSharedFlow<VideoClip>>()

    private val handler = Handler(Looper.getMainLooper())



    fun requestClip(url: String, isFull: Boolean = false): Flow<VideoClip> {
        val cacheKey = "${url}_${isFull}"

        cache[cacheKey]?.let {
            return it
        }

        val flow = initClip(cacheKey, url)
        cache[cacheKey] = flow

        return flow
    }

    private fun initClip(
        cacheKey: String,
        url: String
    ): MutableSharedFlow<VideoClip> {
        Timber.d("new clip ...cacheKey:$cacheKey url:$url")
        val flow = MutableSharedFlow<VideoClip>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_OLDEST
        )


        //track the usage of this & cleanup after a time...
        coroutineUtil.appScope.launch(coroutineUtil.mainDispatcher) {
            val thisJob = coroutineContext[Job]!!

            var delayedCleanupJob: Job? = null

            flow.subscriptionCount.collectLatest { count ->
                delay(1500)
                if (count == 0) {
                    flow.replayCache.lastOrNull()?.player?.let { player ->
                        if (player.playWhenReady) {
                            player.pause()
                        }
                    }

                    if (delayedCleanupJob == null) {
                        Timber.d("create a job for player-clean-up after a delay")
                        delayedCleanupJob = coroutineUtil.appScope.launch {
                            delay(CLEANUP_DELAY)

                            Timber.d("cleaning up player for cacheKey:$cacheKey")
                            flow.replayCache.last().player?.let { player ->
                                handler.post {
                                    player.release()
                                }
                            }
                            cache.remove(cacheKey)

                            thisJob.cancel()
                        }


                    } else {
                        Timber.d("player-clean-up already exists")
                    }

                } else {
                    flow.replayCache.lastOrNull()?.player?.let { player ->
                        if (!player.playWhenReady) {
                            player.play()
                        }
                    }

                    if (delayedCleanupJob != null) {
                        Timber.d("player-clean-up job cancelled")
                        delayedCleanupJob?.cancel()
                        delayedCleanupJob = null

                    } else {
                        Timber.d("player-clean-up == NULL")
                    }
                }
            }
        }


        flow.tryEmit(VideoClip(url, null) {
            Timber.d("dummy implementation")
        })

        handler.postDelayed(250) { //TODO why post delayed? check if undoing affects recycler smoothness
            val player = factory.providePlayer()

            var lastAttachedView: ViewVideoClip? = null

            val mediaSource = factory.provideMediaSource(url)

            player.playWhenReady = false
            player.repeatMode = Player.REPEAT_MODE_ONE
            player.setMediaSource(mediaSource)
            player.prepare()

            player.addListener(object : Player.EventListener {
                override fun onPlaybackStateChanged(state: Int) {
                    val stateName = when (state) {
                        Player.STATE_BUFFERING -> "STATE_BUFFERING"
                        Player.STATE_IDLE -> "STATE_IDLE"
                        Player.STATE_READY -> "STATE_READY"
                        Player.STATE_ENDED -> "STATE_ENDED"
                        else -> "unknown"
                    }
                    Timber.d("onPlaybackStateChanged $stateName")

                    if (Player.STATE_READY == state) {
                        lastAttachedView?.showWithDelay()
                    } else {
                        lastAttachedView?.hideImmediately()
                    }
                }

                override fun onPlayWhenReadyChanged(playWhenReady: Boolean, reason: Int) {
                    Timber.d("onPlayWhenReadyChanged $playWhenReady $reason")
                }

            })


            val handlePlayerSwap: suspend (ViewVideoClip)->Unit = handleSwap@{ viewToUse ->
                if (lastAttachedView == viewToUse) {
                    Timber.d("player-view unchanged; doing nothing")
                    return@handleSwap
                }
                lastAttachedView?.hideImmediately()
                delay(300)
                PlayerView.switchTargetView(player, lastAttachedView?.playerView, viewToUse.playerView)
                lastAttachedView = viewToUse
                if (Player.STATE_READY == player.playbackState) {
                    viewToUse.showWithDelay()
                }

                Timber.d("player-views switched")
            }


            flow.tryEmit(VideoClip(url, player, handlePlayerSwap))

        }


        return flow
    }

}





class ExoFactory(
    val appContext: Context
) {

    companion object {
        private const val CACHE_SIZE_MB = 100L
        private const val CACHE_DIR = "video_clips"
    }

    private val cache = SimpleCache(
        File(appContext.cacheDir, CACHE_DIR),
        LeastRecentlyUsedCacheEvictor(CACHE_SIZE_MB * (1024 * 1024)),
        ExoDatabaseProvider(appContext)
    )

    private var mediaSourceFactory: ProgressiveMediaSource.Factory? = null


    fun providePlayer(): SimpleExoPlayer {
        val trackSelector = DefaultTrackSelector(appContext, AdaptiveTrackSelection.Factory())

        return SimpleExoPlayer.Builder(appContext)
            .setTrackSelector(trackSelector)
            .build()
            .also {
                it.volume = 0f
            }
    }

    fun provideMediaSource(url: String): ProgressiveMediaSource {
        val mediaItem = MediaItem.fromUri(Uri.parse(url))
        return ensureMediaSourceFactory().createMediaSource(mediaItem)
    }

    private fun ensureMediaSourceFactory(): ProgressiveMediaSource.Factory {
        mediaSourceFactory?.let {
            return it
        }

        val transferListener = object : TransferListener {
            override fun onTransferInitializing(
                source: DataSource,
                dataSpec: DataSpec,
                isNetwork: Boolean
            ) {
                Timber.d("transferListener onTransferInitializing isNetwork:$isNetwork")
            }

            override fun onTransferStart(
                source: DataSource,
                dataSpec: DataSpec,
                isNetwork: Boolean
            ) {
                Timber.d("transferListener onTransferStart isNetwork:$isNetwork")
            }

            override fun onBytesTransferred(
                source: DataSource,
                dataSpec: DataSpec,
                isNetwork: Boolean,
                bytesTransferred: Int
            ) {
//                Timber.d("transferListener onBytesTransferred isNetwork:$isNetwork bytesTransferred:$bytesTransferred")
            }

            override fun onTransferEnd(source: DataSource, dataSpec: DataSpec, isNetwork: Boolean) {
                Timber.d("transferListener onTransferEnd isNetwork:$isNetwork")
            }

        }


        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(
            appContext,
            Util.getUserAgent(appContext, appContext.getString(R.string.app_name)),
            transferListener
        )

        val cachingDataSourceFactory = CacheDataSource.Factory()
            .setCache(cache)
            .setUpstreamDataSourceFactory(dataSourceFactory)
            .setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)


        val mediaSourceFactory = ProgressiveMediaSource.Factory(cachingDataSourceFactory)
        this.mediaSourceFactory = mediaSourceFactory

        return mediaSourceFactory
    }
}
