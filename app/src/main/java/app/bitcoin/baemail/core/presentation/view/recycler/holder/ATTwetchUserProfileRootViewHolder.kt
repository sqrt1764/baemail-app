package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.Outline
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.util.LinkifyCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableRoundedBitmap
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchUserProfileRootItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import coil.load
import coil.target.ImageViewTarget
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlin.math.min

@FlowPreview
@ExperimentalCoroutinesApi
class ATTwetchUserProfileRootViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    private lateinit var listener: BaseListAdapter.ATTwetchUserProfileBaseListener

    private var model: ATTwetchUserProfileRootItem? = null

    private val userIcon: ImageView = itemView.findViewById(R.id.user_logo)
    private val name: TextView = itemView.findViewById(R.id.user_name)
    private val userId: TextView = itemView.findViewById(R.id.user_id)
    private val buttonFollow: Button = itemView.findViewById(R.id.follow_user)
    private val followsYou: TextView = itemView.findViewById(R.id.twetch_follows_you)
    private val followersValue: TextView = itemView.findViewById(R.id.twetch_followers_value)
    private val followingValue: TextView = itemView.findViewById(R.id.twetch_following_value)
    private val description: TextView = itemView.findViewById(R.id.twetch_bio)


    private val imagePlaceholderDrawable = ColorDrawable(itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant))

    private val dp = itemView.resources.displayMetrics.density

    val strokeColor = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)

    var didImageLoad = false

    init {



        userIcon.clipToOutline = true
        userIcon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val rounding: Float = min(p0.width / 2, p0.height / 2).toFloat()
                p1.setRoundRect(
                    0,
                    0,
                    p0.width,
                    p0.height,
                    rounding
                )
            }

        }

        //todo userIcon.foreground = DrawableState.getNew()

        val strokeWidth = (itemView.resources.displayMetrics.density * 1).toInt()

        val strokeDrawable = DrawableStroke().also {
            it.setStrokeColor(strokeColor)
            it.setStrokeWidths(0, strokeWidth, 0, 0)
            it.setPadding(0, 0, 0, 0)
        }

        description.background = strokeDrawable

        buttonFollow.setOnClickListener {
            val m = model ?: return@setOnClickListener

            if (m.profileBase.youFollow) {
                val proceeding = listener.onUnfollowUserClicked(m)

                if (proceeding) {
                    m.profileBase.onUnfollowedByMe()
                    onBindViewHolder(m, listener)
                }



            } else {
                val proceeding = listener.onFollowUserClicked(m)

                if (proceeding) {
                    m.profileBase.onFollowedByMe()
                    onBindViewHolder(m, listener)
                }
            }

        }

        userIcon.setOnClickListener {
            val m = model ?: return@setOnClickListener
            if (!didImageLoad) return@setOnClickListener

            listener.onUserIconClicked(m.profileBase.icon!!)
        }

        description.movementMethod = LinkMovementMethod.getInstance()

    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATTwetchUserProfileRootItem

        this.listener = listener!! as BaseListAdapter.ATTwetchUserProfileBaseListener

        if (model.showFollowButton) {
            buttonFollow.visibility = View.VISIBLE

            if (model.profileBase.youFollow) {
                buttonFollow.setText(R.string.following)
            } else {
                buttonFollow.setText(R.string.follow_10c)
            }

        } else {
            buttonFollow.visibility = View.GONE
        }

        if (!model.showFollowButton) {
            followsYou.visibility = View.GONE
        } else if (model.profileBase.followsYou) {
            followsYou.visibility = View.VISIBLE
        } else {
            followsYou.visibility = View.GONE
        }

        userId.text = "@${model.profileBase.userId}"
        name.text = model.profileBase.name

        didImageLoad = false

        if (model.profileBase.icon.isNullOrBlank()) {
            userIcon.load(imagePlaceholderDrawable)
        } else {
            userIcon.load(model.profileBase.icon) {
                placeholder(imagePlaceholderDrawable)
                error(imagePlaceholderDrawable)

                target(object : ImageViewTarget(userIcon) {
                    override fun onSuccess(result: Drawable) {
                        didImageLoad = true

                        val d = if (result is BitmapDrawable) {
                            DrawableRoundedBitmap(itemView.context, 0f, result.bitmap)
                        } else {
                            result
                        }
                        super.onSuccess(d)
                    }
                })
            }
        }


        //todo... if (isFollowing) {... style `buttonFollow`

        followersValue.text = model.profileBase.followerCount.toString()
        followingValue.text = model.profileBase.followingCount.toString()

        description.text = model.profileBase.description ?: "-"

        LinkifyCompat.addLinks(description, Linkify.WEB_URLS)
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTwetchUserProfileRootViewHolder {
            return ATTwetchUserProfileRootViewHolder(
                inflater.inflate(
                    R.layout.li_twetch_user_profile_root,
                    parent,
                    false
                )
            )
        }
    }
}