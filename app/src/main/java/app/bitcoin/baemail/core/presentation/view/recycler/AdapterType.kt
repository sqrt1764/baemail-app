package app.bitcoin.baemail.core.presentation.view.recycler

import androidx.core.text.PrecomputedTextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.bitcoin.baemail.core.data.nodejs.request.DerivedAddress
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.twetch.db.entity.UserProfileBaseModel
import app.bitcoin.baemail.twetch.ui.PostHolder
import app.bitcoin.baemail.twetch.ui.TwetchChatInfoStore
import app.bitcoin.baemail.twetch.ui.TwetchNotificationsStore
import kotlinx.coroutines.channels.Channel
import java.io.File

sealed class AdapterType(val type: AdapterTypeEnum)

data class ATAccount0(
    val isAuthorised: Boolean
) : AdapterType(AdapterTypeEnum.ACCOUNT_0)

data class ATMessage0(
    val id: String,
    val from: String,
    val subject: String,
    val preview: String,
    val dateLabel: String,
    val hasAttachment: Boolean,
    val isUnread: Boolean,
    var isRightActionExpanded: Boolean = false,
    var isLeftActionExpanded: Boolean = false,
    var message: DecryptedBaemailMessage? = null
) : AdapterType(AdapterTypeEnum.MESSAGE_0)

data class ATActivity0(
    val memoryId: String,
    val actionId: String
) : AdapterType(AdapterTypeEnum.ACTIVITY_0)

data class ATNoPaymailsAdded(
    val id: String
) : AdapterType(AdapterTypeEnum.NO_PAYMAILS_ADDED)

data class ATAuthenticatedPaymail(
    val paymail: String,
    val isActive: Boolean,
    val isLast: Boolean
) : AdapterType(AdapterTypeEnum.AUTHENTICATED_PAYMAIL)

data class ATSelectedSeedWord(
    val value: String,
    val isDismissible: Boolean
) : AdapterType(AdapterTypeEnum.SELECTED_SEED_WORD)

data class ATSeedWordHint(
    val value: String
) : AdapterType(AdapterTypeEnum.SEED_WORD_HINT)

data class ATFilterSeedWord(
    val value: String
) : AdapterType(AdapterTypeEnum.FILTER_SEED_WORD)

data class ATFilterSeedNoMatch(
    val value: String
) : AdapterType(AdapterTypeEnum.FILTER_SEED_NO_MATCH)

data class ATHeightDecorDynamic(
    val id: String,
    val heightLD: MutableLiveData<Int>
) : AdapterType(AdapterTypeEnum.HEIGHT_DECOR_DYNAMIC)

data class ATCoin(
    val coin: Coin,
    val isSelected: Boolean,
    val isLast: Boolean
) : AdapterType(AdapterTypeEnum.COIN_0)

data class ATUnusedAddress(
    val coin: DerivedAddress,
    val isLast: Boolean
) : AdapterType(AdapterTypeEnum.UNUSED_ADDRESS_0)

data class ATContentLoading0(
    val id: Int = 0
) : AdapterType(AdapterTypeEnum.CONTENT_LOADING_0)

data class ATSectionTitle(
    val id: Int = 0
) : AdapterType(AdapterTypeEnum.SECTION_TITLE_0)

data class ATPaymentItem(
    val id: String
) : AdapterType(AdapterTypeEnum.PAYMENT_ITEM)


data class ATP2pStallItem(
    val name: String,
    val title: String,
    val paymail: String,
    val host: String,
    val port: Int
) : AdapterType(AdapterTypeEnum.P2P_STALL)

data class ATTwetchChatListItem(
    val id: String,
    val name: String,
    val icon: String?,
    val labelLastMessageTime: String,
    val chat: TwetchChatInfoStore.ChatInfo
) : AdapterType(AdapterTypeEnum.TWETCH_CHAT_LIST_ITEM)

data class ATTwetchChatParticipantItem(
        val participant: TwetchChatInfoStore.Participant
) : AdapterType(AdapterTypeEnum.TWETCH_CHAT_PARTICIPANT)

data class ATTwetchChatMessageMeItem(
    val id: String,
    val chatId: String,
    val participant: TwetchChatInfoStore.Participant,
    val content: String,
    val image: File?,
    val imageFileFound: Boolean,
    val dateLabel:String,
    val millisCreatedAt: Long,
    var showDate: Boolean,
    var showIcon: Boolean,
    var inSending: Boolean = false,
    var inError: Boolean = false
) : AdapterType(AdapterTypeEnum.TWETCH_CHAT_MESSAGE_ME)

data class ATTwetchChatMessageOtherItem(
    val id: String,
    val chatId: String,
    val participant: TwetchChatInfoStore.Participant?,
    val content: String,
    val image: File?,
    val imageFileFound: Boolean,
    val dateLabel:String,
    val millisCreatedAt: Long,
    var showDate: Boolean,
    var showIcon: Boolean
) : AdapterType(AdapterTypeEnum.TWETCH_CHAT_MESSAGE_OTHER)

data class ATTwetchUserProfileRootItem(
    val id: String,
    val profileBase: UserProfileBaseModel,
    val showFollowButton: Boolean
) : AdapterType(AdapterTypeEnum.TWETCH_USER_PROFILE_ROOT)

data class ATTwetchTabsItem(
    val id: String
) : AdapterType(AdapterTypeEnum.TWETCH_TABS)




//////

enum class SpanType {
    USER_ID,
    REF_POST
}

//////




data class ATTwetchPostItem(
    val id: String,
    val holder: PostHolder,
    val labelCreatedAtOfPost: String,
    val labelCreatedAtOfReferenced: String?,
    val labelCreatedAtOfDoubleReferenced: String?,
    val contentMap: HashMap<String, PrecomputedTextCompat> = HashMap(),
    val isParentOfPostDetails: Boolean = false,
    val isFirstOfParentsOfPostDetails: Boolean = false,
    val isReplyOfPostDetails: Boolean = false
) : AdapterType(AdapterTypeEnum.TWETCH_POST) {

    lateinit var spanClicks: Channel<Pair<SpanType, String>>

}

data class ATTwetchPostDetailMainItem(
    val id: String,
    val holder: PostHolder,
    val labelCreatedAtOfPost: String,
    val labelCreatedAtOfReferenced: String?,
    val contentMap: HashMap<String, PrecomputedTextCompat>
) : AdapterType(AdapterTypeEnum.TWETCH_POST_DETAIL_MAIN) {

    lateinit var spanClicks: Channel<Pair<SpanType, String>>

}

data class ATTwetchNotificationItem(
    val id: String,
    val notification: TwetchNotificationsStore.Notification,
    val labelCreatedAtOfNotification: String,
    val labelCreatedAtOfReferencedPost: String?,
    val labelPrice: String,
    val contentMap: HashMap<String, PrecomputedTextCompat>
) : AdapterType(AdapterTypeEnum.TWETCH_NOTIFICATION) {

    lateinit var spanClicks: Channel<Pair<SpanType, String>>

}

data class ATTwetchSearchPostsHintItem(
    val id: String
) : AdapterType(AdapterTypeEnum.TWETCH_SEARCH_POSTS_HINT)

data class ATTwetchNoPostsFoundItem(
    val id: String
) : AdapterType(AdapterTypeEnum.TWETCH_NO_POSTS_FOUND)

data class ATTwetchPostSecondaryLoadingItem(
    val id: String,
    val isLoadingPostParent: Boolean = false,
    val isLoadingFirstOfPostParents: Boolean = false
) : AdapterType(AdapterTypeEnum.TWETCH_POST_SECONDARY_LOADING)

data class ATTwetchPostLoadingItem(
    val id: String
) : AdapterType(AdapterTypeEnum.TWETCH_POST_LOADING)

data class ATTwetchSeparatorItem(
    val id: String,
    val color: Int,
    val heightPx: Int
) : AdapterType(AdapterTypeEnum.TWETCH_SEPARATOR_0)

data class ATReplInput(
        val id: String,
        val code: String
) : AdapterType(AdapterTypeEnum.REPL_INPUT)

data class ATReplOutput(
        val id: String,
        val output: String
) : AdapterType(AdapterTypeEnum.REPL_OUTPUT)

data class ATReplReturn(
        val id: String,
        val success: Boolean,
        val data: String
) : AdapterType(AdapterTypeEnum.REPL_RETURN)

data class ATReplOutputActions(
        val id: String
) : AdapterType(AdapterTypeEnum.REPL_OUTPUT_ACTIONS)

data class ATSavedReplInput(
    val id: String,
    val millisSaved: Long,
    val content: String
) : AdapterType(AdapterTypeEnum.SAVED_REPL_INPUT)

data class ATSavedReplInfo(
    val id: String
) : AdapterType(AdapterTypeEnum.SAVED_REPL_INFO)

enum class AdapterTypeEnum {
    ACCOUNT_0,
    MESSAGE_0,
    ACTIVITY_0,
    NO_PAYMAILS_ADDED,
    AUTHENTICATED_PAYMAIL,
    SELECTED_SEED_WORD,
    SEED_WORD_HINT,
    FILTER_SEED_WORD,
    FILTER_SEED_NO_MATCH,
    HEIGHT_DECOR_DYNAMIC,
    COIN_0,
    UNUSED_ADDRESS_0,
    CONTENT_LOADING_0,
    SECTION_TITLE_0,
    PAYMENT_ITEM,
    TWETCH_CHAT_LIST_ITEM,
    TWETCH_CHAT_PARTICIPANT,
    TWETCH_CHAT_MESSAGE_ME,
    TWETCH_CHAT_MESSAGE_OTHER,
    TWETCH_USER_PROFILE_ROOT,
    TWETCH_TABS,
    TWETCH_POST,
    TWETCH_POST_DETAIL_MAIN,
    TWETCH_SEARCH_POSTS_HINT,
    TWETCH_NO_POSTS_FOUND,
    TWETCH_POST_SECONDARY_LOADING,
    TWETCH_POST_LOADING,
    TWETCH_SEPARATOR_0,
    TWETCH_NOTIFICATION,
    REPL_INPUT,
    REPL_OUTPUT,
    REPL_RETURN,
    REPL_OUTPUT_ACTIONS,
    SAVED_REPL_INPUT,
    SAVED_REPL_INFO,
    P2P_STALL
}