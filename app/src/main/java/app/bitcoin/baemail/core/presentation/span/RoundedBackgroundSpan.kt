package app.bitcoin.baemail.core.presentation.span

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.FontMetricsInt
import android.graphics.RectF
import android.text.style.LineHeightSpan
import android.text.style.ReplacementSpan
import kotlin.math.roundToInt

/**
 * Modified version of this -- https://gist.github.com/anacicconi/39469b975db3edb81adce3a7d199133b#file-rounded_background_span-kt-L7
 *
 * A span to create a rounded background on a text.
 */
class RoundedBackgroundSpan (
    private val backgroundColor: Int,
    private val textColor: Int,
    private val lineHeight: Int? = null,
    private val sidePadding: Float = 16f,
    private val radius: Float? = null,
    private val backgroundVerticalInsetPx: Float = 0f
) : ReplacementSpan(), LineHeightSpan {

    override fun getSize(paint: Paint, text: CharSequence?, start: Int, end: Int, fm: FontMetricsInt?): Int {
        return (sidePadding + paint.measureText(text, start, end) + sidePadding).toInt()
    }

    override fun draw(
        canvas: Canvas,
        text: CharSequence?,
        start: Int,
        end: Int,
        x: Float,
        top: Int,
        y: Int,
        bottom: Int,
        paint: Paint
    ) {
        paint.color = backgroundColor

        val width = paint.measureText(text, start, end)
        val height = paint.fontMetrics.descent - paint.fontMetrics.ascent + 2 * sidePadding

        if(radius != null) {
            val rect = RectF(
                x,
                top.toFloat() + backgroundVerticalInsetPx,
                x + width + 2 * sidePadding,
                bottom.toFloat() - backgroundVerticalInsetPx
            )
            canvas.drawRoundRect(rect, radius, radius, paint)
        } else {
            canvas.drawCircle(x + ((width + 2 * sidePadding) / 2), ((bottom - top) / 2).toFloat(), height / 2, paint)
        }


        val finalTextY = bottom.toFloat() - ((bottom - top - paint.textSize + paint.descent()) / 2f)


        paint.color = textColor
        canvas.drawText(text ?: "", start, end, x + sidePadding, finalTextY, paint)
    }

    override fun chooseHeight(
        text: CharSequence?,
        start: Int,
        end: Int,
        spanstartv: Int,
        lineHeight: Int,
        fm: Paint.FontMetricsInt?
    ) {
    }

//    override fun chooseHeight(
//        text: CharSequence?,
//        start: Int,
//        end: Int,
//        spanstartv: Int,
//        gotLineHeight: Int,
//        gotFm: FontMetricsInt?
//    ) {
//        lineHeight ?: return
//        gotFm?.let { fm ->
//            val originHeight = fm.descent - fm.ascent
//            // If original height is not positive, do nothing.
//            if (originHeight <= 0 || originHeight == lineHeight) {
//                return
//            }
//            val ratio: Float = lineHeight * 1.0f / originHeight
//            fm.descent = (fm.descent * ratio).roundToInt()
//            fm.ascent = fm.descent - lineHeight
//        }
//
//    }
}