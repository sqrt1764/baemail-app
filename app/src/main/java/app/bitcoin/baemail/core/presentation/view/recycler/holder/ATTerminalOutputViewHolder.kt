package app.bitcoin.baemail.core.presentation.view.recycler.holder

import android.graphics.drawable.LayerDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.view.recycler.ATReplOutput
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.view.recycler.BaseViewHolder
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import timber.log.Timber

class ATTerminalOutputViewHolder(
        itemView: View
) : BaseViewHolder(itemView) {

//    lateinit var listener: BaseListAdapter.ATFilterSeedWordListener

    var model: ATReplOutput? = null

    val outputText: TextView = itemView.findViewById(R.id.output_text)
    val outputLine: TextView = itemView.findViewById(R.id.output_line)

    init {

        val strokeColor = itemView.context.getColorFromAttr(R.attr.colorSurfaceVariant)
        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(itemView.context, id)
        }

        val strokeWidth = (0.6 * itemView.resources.displayMetrics.density).toInt()

        val strokePaddingSides = (4 * itemView.resources.displayMetrics.density).toInt()

        itemView.background = LayerDrawable(arrayOf(
                DrawableStroke().also {
                    it.setStrokeColor(strokeColor)
                    it.setStrokeWidths(0, 0, 0, strokeWidth)
                    it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
                },
                DrawableState.getNew(colorSelector)
        ))


//        itemView.setOnClickListener {
//            Toast.makeText(itemView.context, "Line copied.", Toast.LENGTH_SHORT).show()
//            //todo
//            //todo
//            //todo
//            //todo
//        }

    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATReplOutput

//        this.listener = listener!! as BaseListAdapter.ATFilterSeedWordListener

        outputText.text = model.output

        outputLine.text = model.id

        //make sure that the output can be selected
        outputText.setTextIsSelectable(false)
        outputText.post {
            try {
                outputText.setTextIsSelectable(true)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATTerminalOutputViewHolder {
            return ATTerminalOutputViewHolder(
                    inflater.inflate(
                            R.layout.li_terminal_output,
                            parent,
                            false
                    )
            )
        }
    }
}