package app.bitcoin.baemail.p2p

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.bitcoin.baemail.core.data.util.AppForegroundLiveData
import app.bitcoin.baemail.core.presentation.util.PermissionStateHelper
import app.local.p2p.serviceDiscovery.P2pStallManager
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class P2pManageStallViewModel @Inject constructor(
    val p2pRepository: P2pStallRepository,
    val appForegroundLiveData: AppForegroundLiveData
) : ViewModel() {

    private val _p2pPermissionState = MutableStateFlow(P2pPermissionModel())
    val p2pPermissionState: StateFlow<P2pPermissionModel>
        get() = _p2pPermissionState.asStateFlow()

    private val _content = MutableStateFlow(ManageModel(
        isRegistryActive = false,
        title = P2pStallRepository.VALUE_UNSET,
        pki = P2pStallRepository.VALUE_UNSET,
        info = P2pStallRepository.VALUE_UNSET,
        forcePaymailPki = true
    ))

    val content: StateFlow<ManageModel>
        get() = _content

    val playStateHolder: StateFlow<PermissionStateHelper.PlayServicesStateHolder>
        get() = p2pRepository.permissionStateHelper.playStateHolder



    private val _contentOfEdit = MutableStateFlow(EditModel(
        P2pStallRepository.VALUE_UNSET,
        P2pStallRepository.VALUE_UNSET,
        P2pStallRepository.VALUE_UNSET,
        forcePaymailPki = true,
        isModified = false
    ))
    val contentOfEdit: StateFlow<EditModel>
        get() = _contentOfEdit





    init {

        viewModelScope.launch {
            p2pRepository.stallManager.stallRunningState.collect { model ->
                if (P2pStallManager.State.OFF == model.state) {
                    _content.value = _content.value.copy(
                        isRegistryActive = false
                    )
                } else {
                    _content.value = _content.value.copy(
                        isRegistryActive = true
                    )
                }




            }
        }

        viewModelScope.launch {

            p2pRepository.flowOfInfo().combine(p2pRepository.currentPeerServiceConfigFlow) { p0, p1 ->
                p0 to p1
            }.collect { (peerInfo, config) ->
                _content.value = _content.value.copy(
                    title = config.id.title,
                    pki = config.id.pki,
                    info = peerInfo,
                    forcePaymailPki = config.forceUsePaymailPki
                )


                val editContent = _contentOfEdit.value
                if (!editContent.isModified) {
                    _contentOfEdit.value = _contentOfEdit.value.copy(
                        title = config.id.title,
                        pki = config.customPkiWifPrivate,
                        info = peerInfo,
                        forcePaymailPki = config.forceUsePaymailPki
                    )
                }

            }
        }



    }



    fun onPlayServicesAvailabilityChanged(
        availability: PermissionStateHelper.PlayServicesAvailability

    ) {
        p2pRepository.permissionStateHelper.onPlayServicesAvailabilityChanged(availability)
    }

    fun resetPlayServicesAvailabilityChecking() {
        p2pRepository.permissionStateHelper.resetPlayServicesAvailabilityChecking()
    }


    fun getRequiredP2pPermissions(): Array<String> = p2pRepository.getAppropriatePermissionsArray()

    fun onP2pPermissionCheckResults(
        hasPermissionResults: Map<String, Boolean>,
        shouldShowRequestPermissionRationaleResults: Map<String, Boolean>
    ) {
        _p2pPermissionState.value = P2pPermissionModel(
            mustCheckLocPermission = false,
            hasPermissionResults = hasPermissionResults,
            shouldShowRequestPermissionRationaleResults = shouldShowRequestPermissionRationaleResults
        )
    }

    fun onRequestP2pPermissionsResults(results: Map<String, Boolean>) {
        val state = p2pPermissionState.value
        val permissionResults = state.hasPermissionResults?.toMutableMap()
            ?: mutableMapOf()

        permissionResults.putAll(results)

        val requireRationaleResults = state.shouldShowRequestPermissionRationaleResults?.let {
            val mutable = it.toMutableMap()

            results.entries.forEach { (permission, granted) ->
                if (granted) mutable.remove(permission)
            }

            mutable
        }

        _p2pPermissionState.value = state.copy(
            hasPermissionResults = permissionResults,
            shouldShowRequestPermissionRationaleResults = requireRationaleResults
        )
    }


    fun onClickedServiceRegistryToggle() {
        Timber.d("#onClickedServiceRegistryToggle")

        val stallRegistryModel = p2pRepository.stallManager.stallRunningState.value

        if (P2pStallManager.State.OFF == stallRegistryModel.state) {
            p2pRepository.stallManager.requestStartAdvertisingService()
        } else {
            p2pRepository.stallManager.stopAdvertisingService()
        }

    }












    fun onTitleInputOfEditChanged(value: String) {
        val newModel = _contentOfEdit.value.copy(
            title = value
        )

        val isModified = !(_content.value.title == newModel.title &&
                _content.value.pki == newModel.pki &&
                _content.value.info == newModel.info &&
                _content.value.forcePaymailPki == newModel.forcePaymailPki)

        _contentOfEdit.value = newModel.copy(
            isModified = isModified
        )
    }


    fun onPkiInputOfEditChanged(value: String) {
        Timber.d("onPkiInputOfEditChanged $value")
        if (_contentOfEdit.value.pki == value) {
            Timber.d("same...")
            return
        }

        val newModel = _contentOfEdit.value.copy(
            pki = value
        )

        val isModified = !(_content.value.title == newModel.title &&
                _content.value.pki == newModel.pki &&
                _content.value.info == newModel.info &&
                _content.value.forcePaymailPki == newModel.forcePaymailPki)

        _contentOfEdit.value = newModel.copy(
            isModified = isModified
        )
    }


    fun onInfoInputOfEditChanged(value: String) {
        val newModel = _contentOfEdit.value.copy(
            info = value
        )

        val isModified = !(_content.value.title == newModel.title &&
                _content.value.pki == newModel.pki &&
                _content.value.info == newModel.info &&
                _content.value.forcePaymailPki == newModel.forcePaymailPki)

        _contentOfEdit.value = newModel.copy(
            isModified = isModified
        )
    }

    fun onTogglePkiVisibilityInEdit(isHidden: Boolean) {
        val newModel = _contentOfEdit.value.copy(
            isPkiHidden = isHidden
        )
//        val isModified = !(_content.value.title == newModel.title &&
//                _content.value.pki == newModel.pki &&
//                _content.value.info == newModel.info &&
//                _content.value.forcePaymailPki == newModel.forcePaymailPki)

        _contentOfEdit.value = newModel.copy(
//            isModified = isModified
        )
    }

    fun onRadioTogglePki(forceUsePaymailPki: Boolean) {
        val newModel = _contentOfEdit.value.copy(
            forcePaymailPki = forceUsePaymailPki
        )

        val isModified = !(_content.value.title == newModel.title &&
                _content.value.pki == newModel.pki &&
                _content.value.info == newModel.info &&
                _content.value.forcePaymailPki == newModel.forcePaymailPki)

        _contentOfEdit.value = newModel.copy(
            isModified = isModified
        )
    }

    fun onClickedCancelEditOfStall() {
        viewModelScope.launch {
            val info = p2pRepository.flowOfInfo().first()
            val peerConfig = p2pRepository.currentPeerServiceConfigFlow.value
            _contentOfEdit.value = _contentOfEdit.value.copy(
                title = peerConfig.id.title,
                pki = peerConfig.customPkiWifPrivate,
                info = info,
                isModified = false,
                isPkiHidden = true
            )
        }
    }

    fun onClickedApplyEditOfStall() {
        val model = _contentOfEdit.value
        _contentOfEdit.value = _contentOfEdit.value.copy(
            isModified = false
        )

        viewModelScope.launch {
            val info = p2pRepository.flowOfInfo().first()
            if (model.info != info){
                p2pRepository.updateInfo(model.info)
            }

            val peerConfig = p2pRepository.currentPeerServiceConfigFlow.value

            val finalPki = model.pki.let { pki ->
                if (pki != peerConfig.customPkiWifPrivate) {
                    pki
                } else null
            }
            val finalTitle = model.title.let { title ->
                if (title != peerConfig.id.title) {
                    title
                } else null
            }

            val finalForceUsePaymailPki = if (model.forcePaymailPki == peerConfig.forceUsePaymailPki) {
                null
            } else model.forcePaymailPki

            p2pRepository.updateStall(
                customPkiWifPrivate = finalPki,
                title = finalTitle,
                forceUsePaymailPki = finalForceUsePaymailPki
            )
        }
    }



    data class P2pPermissionModel(
        val mustCheckLocPermission: Boolean = true,
        val hasPermissionResults: Map<String, Boolean>? = null,
        val shouldShowRequestPermissionRationaleResults: Map<String, Boolean>? = null,
    ) {
        fun hasMissingPermissions(): Boolean {
            if (hasPermissionResults == null) return true

            val missingPermissions = hasPermissionResults.entries.mapNotNull {
                if (it.value) return@mapNotNull null

                //the following permission is allowed to remain 'denied'
                if ("android.permission.POST_NOTIFICATIONS" == it.key) return@mapNotNull null

                it.key
            }

            return missingPermissions.isNotEmpty()
        }
    }



    data class ManageModel(
        val isRegistryActive: Boolean,
        val title: String,
        val pki: String,
        val info: String,
        val forcePaymailPki: Boolean
    )

    data class EditModel(
        val title: String,
        val pki: String,
        val info: String,
        val forcePaymailPki: Boolean,
        val isModified: Boolean,
        var isPkiHidden: Boolean = true
    )
}