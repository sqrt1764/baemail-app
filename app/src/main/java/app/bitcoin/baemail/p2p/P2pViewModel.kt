package app.bitcoin.baemail.p2p

import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.bitcoin.baemail.core.data.util.DeepLinkHelper
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.core.data.util.AppForegroundLiveData
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.local.p2p.serviceDiscovery.ConnectionType
import app.local.p2p.serviceDiscovery.DiscoveryModel
import app.local.p2p.serviceDiscovery.FoundStall
import app.local.p2p.serviceDiscovery.P2pStallManager
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class P2pViewModel @Inject constructor(
    val p2pRepository: P2pStallRepository,
    val appForegroundLiveData: AppForegroundLiveData,
    val deepLinkHelper: DeepLinkHelper,
    val nodeRuntimeRepository: NodeRuntimeRepository
) : ViewModel() {


    private val _content = MutableStateFlow(emptyList<P2pItem>())

    val content: StateFlow<List<P2pItem>>
        get() = _content


    init {


        viewModelScope.launch {

            val currentConnectionTypeState: Flow<ConnectionTypeState> =
                p2pRepository.serviceRegistryManager.supportedStatus
                    .combine(p2pRepository.connectionTypeFlow) { p0, p1 ->
                        val statusCode = if (ConnectionType.NEARBY == p1) {
                            p2pRepository.permissionStateHelper.playStateHolder.value.availability.statusCode
                        } else -1
                        ConnectionTypeState(p1, p0, statusCode)
                    }

            val discoveredStallsState: Flow<DiscoveredStallsState> =
                p2pRepository.stallManager.serviceRegistryHelper.discoveredServices
                    .combine(p2pRepository.discoveryFlow) { discoveredServices, discoveryRequested ->
                        discoveredServices to discoveryRequested
                    }.combine(p2pRepository.informP2pChangeHelper.stallSessionState) {
                            (discoveredServices, discoveryRequested), sessionState ->
                        DiscoveredStallsState(
                            discoveryRequested,
                            discoveredServices,
                            sessionState
                        )
                    }


            currentConnectionTypeState.combine(
                p2pRepository.stallManager.stallRunningState

            ) { connectionTypeState, myStallState ->
                val isMyStallActive = myStallState.state != P2pStallManager.State.OFF
                connectionTypeState to isMyStallActive

            }.combine(discoveredStallsState) { (connectionTypeState, isMyStallActive), discoveryState ->
                buildDiscoveredStallListState(
                    connectionTypeState,
                    isMyStallActive,
                    discoveryState.isDiscoveryRequested,
                    discoveryState.discoveredStalls.peers,
                    discoveryState.discoveredStallSessions
                )

            }.collect { content ->
                _content.value = content
            }


        }

    }


    override fun onCleared() {
        super.onCleared()
    }



    private var currentStopDiscoveryJob: Job? = null

    fun onClickedServiceRegistryToggle() {
        val stallModel = p2pRepository.stallManager.stallRunningState.value

        if (P2pStallManager.State.OFF == stallModel.state) {
            p2pRepository.stallManager.requestStartAdvertisingService()
        } else {
            p2pRepository.stallManager.stopAdvertisingService()
        }
    }


    fun getP2pConnectionType(): ConnectionType {
        return p2pRepository.connectionTypeFlow.value
    }

    private fun isDiscoveryRequested(): Boolean {
        return p2pRepository.discoveryFlow.value
    }

    fun onClickedDiscoveryToggle() {
        if (!isDiscoveryRequested()) {
            viewModelScope.launch {
                try {
                    p2pRepository.requestStartDiscoveryOfPeers()

                } catch (e: Exception) {
                    Timber.e(e, "!!!!")
                }
            }

        } else {
            currentStopDiscoveryJob?.let {
                if (it.isActive) {
                    //drop this `click`
                    return
                }
            }

            currentStopDiscoveryJob = viewModelScope.launch {
                p2pRepository.stopDiscoveryOfPeers()
            }
        }

    }







    fun onClickedBrowseStall(stallName: String) {
        val gotStall = p2pRepository.stallManager.serviceRegistryHelper.discoveredServices
            .value.peers[stallName] ?: let {
            Timber.d("there are no stalls with the specified name: $stallName")
            return
        }

        viewModelScope.launch {
            p2pRepository.connectToStall(gotStall)
        }
    }


    //todo move to the p2p-repository
    //todo
    //todo
    //todo
    //todo
    //todo
    fun onClickedDisconnectFromStall(stallName: String) {
        viewModelScope.launch {
            val response = nodeRuntimeRepository.runAsyncScript("""
                var stallInterface = context.stall.peers.connected['${stallName}'];
                
                if (stallInterface == null) {
                    console.log('....matching stall not found');
                    return;
                }
                
                stallInterface.disconnectNow();
                
            """.trimIndent())


            if (response is RunScript.Response.Success) {
//                Timber.d("success; content: ${response.content}")
            } else {
                response as RunScript.Response.Fail
                Timber.d("success; content: ${response.responseData}")
            }


        }
    }


    fun runNodeScript(script: String) {
        viewModelScope.launch {
            Timber.d("executing stall-button-click-script\n\n$script")

            val response = nodeRuntimeRepository.runAsyncScript(script)

            if (response is RunScript.Response.Success) {
//                Timber.d("success; content: ${response.content}")
            } else {
                response as RunScript.Response.Fail
                Timber.d("success; content: ${response.responseData}")
            }
        }
    }


    fun onChangeConnectionType(type: ConnectionType) {
        viewModelScope.launch {
            p2pRepository.setConnectionType(type)
        }
    }

    fun buildDiscoveredStallListState(
        connectionTypeState: ConnectionTypeState,
        isRegistryOfMyStallActive: Boolean,
        isDiscoveryRequested: Boolean,
        discoveredPeers: Map<String, FoundStall>,
        sessionState: InformP2pChangeHelper.StallSessionChangeEvent?
    ): List<P2pItem> {

        val assembledList = mutableListOf<P2pItem>()


        assembledList.add(P2pInfo(
            P2pDiscoverySectionMy(
                isStallStarted = isRegistryOfMyStallActive,
                connectionTypeState = connectionTypeState
            ),
            P2pDiscoverSectionFound(
                isDiscoveryActive = isDiscoveryRequested,
                connectionTypeState = connectionTypeState
            )
        ))


        //add layout states for all connected stalls
        sessionState?.sessionLayouts?.entries?.mapTo(assembledList) { stallSessionEntry ->
            val id = stallSessionEntry.key
            val layout = stallSessionEntry.value

            val layoutJsonArray = layout.getAsJsonArray("items")

            P2pStall(
                P2pDiscoverSectionStallBrowsing(
                    stallName = id.name,
                    layoutJson = layoutJsonArray
                )
            )
        }


        //add layout states for discovered/unconnected stalls
        val stallsNotConnected = discoveredPeers.entries.mapNotNull { entry ->
            val name = entry.key
            val stall = entry.value

            if (stall.host == null) return@mapNotNull null

            val matchingSessionEntry = sessionState?.let { stallSessions ->
                stallSessions.sessionLayouts.entries.find { entry ->
                    val id = entry.key
                    val layout = entry.value

                    if (id.name == name) return@find true else false
                }
            }

            if (matchingSessionEntry != null) return@mapNotNull null

            entry.value
        }

        stallsNotConnected.mapTo(assembledList) { stall ->
            P2pStall(
                P2pDiscoverSectionStallFront(
                    stallName = stall.name,
                    pki = stall.pki,
                    title = stall.title,
                    paymail = stall.paymail,
                    host = stall.host ?: "-",
                    port = stall.port.toString() ?: "-"
                )
            )
        }


        assembledList.add(P2pGapItem(550.dp))

        return assembledList
    }




    data class ConnectionTypeState(
        val currentType: ConnectionType,
        val isSupported: Boolean,
        val statusCode: Int
    )

    data class DiscoveredStallsState(
        val isDiscoveryRequested: Boolean,
        val discoveredStalls: DiscoveryModel,
        val discoveredStallSessions: InformP2pChangeHelper.StallSessionChangeEvent?
    )

}