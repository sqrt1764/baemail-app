package app.bitcoin.baemail.p2p

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import app.local.p2p.serviceDiscovery.ConnectionType


sealed class P2pItem

data class P2pInfo(
    val sectionMy: P2pDiscoverySectionMy,
    val sectionFound: P2pDiscoverSectionFound,
) : P2pItem()

data class P2pStall(
    val stall: P2pDiscoverSectionStall
) : P2pItem()

data class P2pGapItem(
    val height: Dp
) : P2pItem()





/////////////////////


//@Preview(
//    uiMode = Configuration.UI_MODE_NIGHT_YES,
//    showBackground = true,
//    name = "Dark Mode of Discover"
//)
//@Composable
//fun P2pDiscoverPart1Preview() {
//    ComposeP2pDiscover(listOf(
//        P2pInfo(),
//        P2pStall("stall 0")
//    ))
//}




/////////////////////

//todo fix
//@Preview(
//    uiMode = Configuration.UI_MODE_NIGHT_YES,
//    showBackground = true,
//    name = "Dark Mode of Discover"
//)
//@Composable
//fun P2pDiscoverPreview() {
//    val holder = DropDownStateHolder(
//        remember { mutableStateOf(false) },
//        remember { mutableStateOf(0) },
//        listOf("nearby" to "Nearby connections", "wifidirect" to "WiFi Direct", "wifi" to "WiFi")
//    )
//    val connectionTypeState = StallConnectionStateHolder(
//        holder,
//        "Stall discovery & communication will happen through bluetooth",
//        false
//    )
//    ComposeAppTheme {
//        ComposeP2pDiscover(
//            listOf(
//                P2pInfo(
//                    P2pDiscoverySectionMy(),
//                    P2pDiscoverSectionFound()
//                ),
//                P2pStall(P2pDiscoverSectionStallFront(title = "Fruity stall")),
//                P2pStall(P2pDiscoverSectionStallFront(title = "Cheapo")),
//                P2pStall(P2pDiscoverSectionStallFront(title = "Strawberries everywhere!")),
//                P2pStall(P2pDiscoverSectionStallFront(title = "Test stall")),
//                P2pGapItem(24.dp)
//            ),
//            connectionTypeState = connectionTypeState
//        )
//    }
//}





/////////////////////




@Composable
fun ComposeP2pDiscover(
    items: List<P2pItem>,
    connectionTypes: List<ConnectionType>,
    onSelectionChanged: (ConnectionType)->Unit,
    onAttemptResolveConnectionIssue: ()->Unit,
    onGoToMyStall: ()->Unit = {},
    onStallStartToggled: ()->Unit = {},
    onStartDiscoveryToggled: ()->Unit = {},
    onBrowseClicked: (String)->Unit = {},
    onDisconnectClicked: (String)->Unit = {},
    onButtonClicked: (String)->Unit = {}
) {
    val isConnectionDropDownExpanded = remember { mutableStateOf(false) }

    LazyColumn {
        items(items) { item ->
            when (item) {
                is P2pInfo -> {
                    Column {
                        ComposableP2pDiscoverSectionMy(
                            item.sectionMy,
                            isConnectionDropDownExpanded,
                            connectionTypes,
                            onSelectionChanged,
                            onAttemptResolveConnectionIssue,
                            onGoToMyStall,
                            onStallStartToggled
                        )
                        Spacer(modifier = Modifier.size(24.dp))
                        ComposableP2pDiscoverSectionFound(
                            item.sectionFound,
                            onStartDiscoveryToggled
                        )
                        Spacer(modifier = Modifier.size(12.dp))
                    }
                }
                is P2pStall -> {
                    ComposableP2pDiscoverSectionStall(
                        item.stall,
                        onBrowseClicked,
                        onDisconnectClicked,
                        onButtonClicked
                    )
                }
                is P2pGapItem -> {
                    Spacer(modifier = Modifier.size(item.height))
                }
                else -> {
                    throw RuntimeException()
                }
            }
        }
    }
}