package app.bitcoin.baemail.p2p

import android.content.res.Configuration
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import androidx.core.graphics.ColorUtils
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import app.local.p2p.serviceDiscovery.ConnectionType
import coil.compose.rememberImagePainter
import coil.size.OriginalSize
import com.google.android.gms.common.GoogleApiAvailability
import com.google.gson.JsonArray
import java.io.File


//todo fix
//@Preview(
//    uiMode = Configuration.UI_MODE_NIGHT_YES,
//    showBackground = true,
//    name = "Dark Mode"
//)
//@Composable
//fun PreviewComposableP2pDiscoverSectionMy() {
//    val holder = DropDownStateHolder(
//        remember { mutableStateOf(false) },
//        remember { mutableStateOf(0) },
//        listOf("nearby" to "Nearby connections", "wifidirect" to "WiFi Direct", "wifi" to "WiFi")
//    )
//    val connectionTypeState = StallConnectionStateHolder(
//        holder,
//        "Stall discovery & communication will happen through bluetooth",
//        false
//    )
//
//    ComposeAppTheme {
//        ComposableP2pDiscoverSectionMy(connectionTypeState = connectionTypeState)
//    }
//}

data class DropDownStateHolder(
    val expanded: MutableState<Boolean>,
    val selectedIndex: MutableState<Int>,
    val items: List<Pair<String, String>>
)

data class StallConnectionStateHolder(
    val dropDownHolder: DropDownStateHolder,
    val connectionInfoLabel: String,
    val isSupported: Boolean
)

/////////////////////////


data class P2pDiscoverySectionMy(
    val isStallStarted: Boolean = false,
    val connectionTypeState: P2pViewModel.ConnectionTypeState = P2pViewModel.ConnectionTypeState(ConnectionType.NEARBY, false, -1)
)


@Composable
fun ComposableP2pDiscoverSectionMy(
    model: P2pDiscoverySectionMy = P2pDiscoverySectionMy(),
    isConnectionDropDownExpanded: MutableState<Boolean>,
    connectionTypes: List<ConnectionType>,
    onSelectionChanged: (ConnectionType)->Unit,
    onAttemptResolveConnectionIssue: ()->Unit,
    onGoToMyStall: ()->Unit = {},
    onStallStartToggled: ()->Unit = {},
) {

    val startTitle = if (model.isStallStarted) {
        stringResource(R.string.stop)
    } else {
        stringResource(R.string.start)
    }

    val selectedConnectionTypeIndex = connectionTypes.indexOf(model.connectionTypeState.currentType)

    val formattedConnectionTypes = connectionTypes.map {
        val typeLabel = when (it) {
            ConnectionType.NEARBY -> stringResource(R.string.p2p_type_nearby_label)
            ConnectionType.WIFI_DIRECT -> stringResource(R.string.p2p_type_wifidirect_label)
            ConnectionType.NSD -> stringResource(R.string.p2p_type_nsd_label)
        }
        Pair(it.const, typeLabel)
    }

    val formattedOnSelectionChanged: (String) -> Unit = { const ->
        onSelectionChanged(ConnectionType.parse(const))
    }

    val connectionTypeInfoLabel = when (model.connectionTypeState.currentType) {
        ConnectionType.NEARBY -> stringResource(R.string.p2p_type_nearby_info)
        ConnectionType.NSD -> stringResource(R.string.p2p_type_nsd_info)
        ConnectionType.WIFI_DIRECT -> stringResource(R.string.p2p_type_wifidirect_info)
    }

    Column(
        modifier = Modifier
            .padding(PaddingValues(16.dp, 12.dp, 16.dp, 2.dp))
    ) {
        Text(
            "P2P",
            modifier = Modifier,
            style = MaterialTheme.typography.h1
        )
        Text(stringResource(R.string.p2p_info_0),
            modifier = Modifier.padding(top = 8.dp),
            style = MaterialTheme.typography.body1
        )


        Box(
            Modifier
                .padding(top = 16.dp)
                .border(BorderStroke(1.dp, MaterialTheme.colors.primary), RoundedCornerShape(16.dp))
        ) {
            Column(
                Modifier
                    .padding(start = 12.dp, top = 8.dp, end = 12.dp, bottom = 16.dp)
            ) {
                ComposeDropDown(
                    isConnectionDropDownExpanded,
                    selectedConnectionTypeIndex,
                    formattedConnectionTypes,
                    formattedOnSelectionChanged
                )
                Text(
                    connectionTypeInfoLabel,
                    modifier = Modifier.padding(top = 8.dp),
                    style = MaterialTheme.typography.caption
                )
                if (!model.connectionTypeState.isSupported) {
                    //todo bugfix the error-label
                    var shouldShowResolveIssueButton = false
                    val errorLabel = if (ConnectionType.NEARBY == model.connectionTypeState.currentType) {
                        if  (GoogleApiAvailability.getInstance().isUserResolvableError(
                                model.connectionTypeState.statusCode
                        )) {
                            shouldShowResolveIssueButton = true
                            stringResource(R.string.nearby_requires_play_service)
                        } else stringResource(R.string.device_not_supported)
                    } else stringResource(R.string.device_not_supported)

                    Text(
                        errorLabel,
                        modifier = Modifier.padding(top = 8.dp),
                        style = MaterialTheme.typography.caption,
                        color = MaterialTheme.colors.error
                    )

                    if (shouldShowResolveIssueButton) {
                        Button(
                            onClick = {
                                onAttemptResolveConnectionIssue()
                            }
                        ) {
                            Text(
                                stringResource(R.string.install_play_service),
                                style = MaterialTheme.typography.button
                            )
                        }
                    }

                }
            }
        }



        Text(
            stringResource(R.string.my_stall),
            modifier = Modifier
                .padding(top = 16.dp),
            style = MaterialTheme.typography.h2
        )

        Text(stringResource(R.string.p2p_info_1),
            modifier = Modifier
                .padding(top = 8.dp),
            style = MaterialTheme.typography.caption
        )
        Row(
            modifier = Modifier.padding(top = 8.dp)
        ) {
            Button(
                onClick = {
                    onGoToMyStall()
                }
            ) {
                Text(stringResource(R.string.manage),
                    style = MaterialTheme.typography.button
                )
            }

            Spacer(modifier = Modifier.padding(start = 8.dp))

            Button(
                onClick = {
                    onStallStartToggled()
                },
                enabled = model.connectionTypeState.isSupported
            ) {
                Text(
                    startTitle,
                    style = MaterialTheme.typography.button
                )
            }
        }
    }


}



@Composable
fun ComposeDropDown(
    expanded: MutableState<Boolean>,
    selectedIndex: Int,
    items: List<Pair<String, String>>,
    onSelectionChanged: (String)->Unit,
    modifier: Modifier = Modifier
) {

    Box(modifier) {
        OutlinedButton(
            onClick = { expanded.value = true },
            shape = CircleShape,
            modifier = Modifier
                .height(60.dp)
                .padding(
                    start = 0.dp,
                    end = 0.dp,
                    top = 8.dp,
                    bottom = 8.dp
                ),
            border = BorderStroke(1.5.dp, MaterialTheme.colors.primary),
            contentPadding = PaddingValues(
                start = 16.dp,
                end = 16.dp,
                top = 0.dp,
                bottom = 0.dp
            ),
            colors = ButtonDefaults.outlinedButtonColors(contentColor = MaterialTheme.colors.primary)
        ) {
            Text(
                items[selectedIndex].second,
                style = MaterialTheme.typography.button
            )
        }

        DropdownMenu(
            expanded = expanded.value,
            onDismissRequest = { expanded.value = false },
            modifier = Modifier
                .width(300.dp),
            DpOffset(0.dp, 0.dp)
        ) {
            items.forEachIndexed { index, s ->
                DropdownMenuItem(onClick = {
                    onSelectionChanged(s.first)
                    expanded.value = false
                }) {
                    Text(text = s.second)
                }
            }
        }
    }
}







/////////////////////////


//@Preview(
//    uiMode = Configuration.UI_MODE_NIGHT_YES,
//    showBackground = true,
//    name = "Dark Mode of Found-section"
//)
//@Composable
//fun Preview1() {
//    ComposableP2pDiscoverSectionFound()
//}

/////////////////////////


data class P2pDiscoverSectionFound(
    val isDiscoveryActive: Boolean = false,
    val connectionTypeState: P2pViewModel.ConnectionTypeState = P2pViewModel.ConnectionTypeState(ConnectionType.NEARBY, false, -1)
)

@Composable
fun ComposableP2pDiscoverSectionFound(
    model: P2pDiscoverSectionFound = P2pDiscoverSectionFound(),
    onStartDiscoveryToggled: ()->Unit = {}
) {
    ComposeAppTheme {
        Column(
            modifier = Modifier
                .padding(PaddingValues(16.dp, 2.dp, 16.dp, 2.dp))
        ) {
            Text(
                "Nearby",
                modifier = Modifier,
                style = MaterialTheme.typography.h2
            )
            Text(stringResource(R.string.p2p_info_2),
                modifier = Modifier
                    .padding(top = 8.dp),
                style = MaterialTheme.typography.caption
            )
            Row(
                modifier = Modifier.padding(top = 8.dp)
            ) {
                Button(
                    onClick = {
                        onStartDiscoveryToggled()
                    },
                    modifier = Modifier.align(Alignment.Bottom),
                    enabled = model.connectionTypeState.isSupported
                ) {
                    val title = if (model.isDiscoveryActive) {
                        stringResource(R.string.p2p_stop_stall_discovery)
                    } else {
                        stringResource(R.string.p2p_start_stall_discovery)
                    }
                    Text(
                        title,
                        style = MaterialTheme.typography.button
                    )
                }

                if (model.isDiscoveryActive) {
                    Spacer(modifier = Modifier.padding(start = 16.dp))
                    CircularProgressIndicator(modifier = Modifier.align(Alignment.CenterVertically))
                }
            }
        }
    }
}





/////////////////////////


//@Preview(
//    uiMode = Configuration.UI_MODE_NIGHT_YES,
//    showBackground = true,
//    name = "Dark Mode of Stall-item"
//)
//@Composable
//fun Preview2() {
//    ComposableP2pDiscoverSectionStall()
//}


/////////////////////////

sealed class P2pDiscoverSectionStall

data class P2pDiscoverSectionStallFront(
    val stallName: String = "NsdPeer;dummyName",
    val pki: String = "abcef0123456789",
    val title: String = "Fruity stall",
    val paymail: String = "dummy@paymail.sv",
    val host: String = "81.168.0.1",
    val port: String = "12345"
) : P2pDiscoverSectionStall()

data class P2pDiscoverSectionStallBrowsing(
    val stallName: String = "NsdPeer;dummyName",
    val layoutJson: JsonArray
) : P2pDiscoverSectionStall()



@Composable
fun ComposableP2pDiscoverSectionStallFront(
    model: P2pDiscoverSectionStallFront = P2pDiscoverSectionStallFront(),
    onBrowseClicked: (String)->Unit
) {
    Column(
        modifier = Modifier
            .padding(start = 16.dp, top = 8.dp, end = 16.dp, bottom = 8.dp)
            .fillMaxWidth(1f)
    ) {
        Text(
            model.title,
            modifier = Modifier,
            style = MaterialTheme.typography.h2
        )
        Row(
            modifier = Modifier
                .padding(top = 8.dp)
        ) {
            Text(
                stringResource(R.string.paymail),
                modifier = Modifier
                    .align(Alignment.CenterVertically),
                style = MaterialTheme.typography.caption
            )
            Spacer(modifier = Modifier.padding(start = 12.dp))
            Text(
                model.paymail,
                modifier = Modifier
                    .align(Alignment.CenterVertically),
                style = MaterialTheme.typography.body1
            )
        }
        Row(
            modifier = Modifier
                .padding(top = 4.dp)
        ) {
            Text(
                stringResource(R.string.host),
                modifier = Modifier
                    .align(Alignment.CenterVertically),
                style = MaterialTheme.typography.caption
            )
            Spacer(modifier = Modifier.padding(start = 12.dp))
            Text(
                model.host,
                modifier = Modifier
                    .align(Alignment.CenterVertically),
                style = MaterialTheme.typography.body1
            )
        }
        Row(
            modifier = Modifier
                .padding(top = 4.dp)
        ) {
            Text(
                stringResource(R.string.port),
                modifier = Modifier
                    .align(Alignment.CenterVertically),
                style = MaterialTheme.typography.caption
            )
            Spacer(modifier = Modifier.padding(start = 12.dp))
            Text(
                model.port,
                modifier = Modifier
                    .align(Alignment.CenterVertically),
                style = MaterialTheme.typography.body1
            )
        }

        Spacer(modifier = Modifier.height(4.dp))

        OutlinedButton(
            onClick = {
                onBrowseClicked(model.stallName)
//                model.onBrowseClicked(model.stallName) todo
            }
        ) {
            Text(
                stringResource(R.string.browse),
                style = MaterialTheme.typography.button,
                modifier = Modifier.fillMaxWidth(1f),
                textAlign = TextAlign.Center

            )
        }
    }
}











@Composable
fun ComposableP2pDiscoverSectionStallBrowsing(
    model: P2pDiscoverSectionStallBrowsing,
    onDisconnectClicked: (String)->Unit,
    onButtonClicked: (String)->Unit
) {
    Column(
        modifier = Modifier
            .padding(start = 16.dp, top = 8.dp, end = 16.dp, bottom = 8.dp)
            .fillMaxWidth(1f)
    ) {

        model.layoutJson.forEach { jsonElement ->
            val itemObject = jsonElement.asJsonObject
            val typeValue = itemObject.get("type").asString

            when (typeValue) {
                "toolbar" -> {
                    val label = if (itemObject.has("label") && !itemObject.get("label").isJsonNull) {
                        itemObject.get("label").asString
                    } else {
                        stringResource(R.string.error)
                    }

                    val labelStyle = if (itemObject.has("labelStyle") && !itemObject.get("labelStyle").isJsonNull) {
                        itemObject.get("labelStyle").asString
                    } else {
                        "h2"
                    }

                    val buttonValue = if (itemObject.has("buttonValue") && !itemObject.get("buttonValue").isJsonNull) {
                        itemObject.get("buttonValue").asString
                    } else {
                        "console.log('`back` button clicked; set `buttonValue` to execute your node-script on click');"
                    }



                    ComposableP2pStallItemToolbar(
                        label,
                        labelStyle
                    ) {
                        onButtonClicked(buttonValue)
                    }
                }
                "button_disconnect_from_stall" -> {
                    val label = if (itemObject.has("label") && !itemObject.get("label").isJsonNull) {
                        itemObject.get("label").asString
                    } else {
                        stringResource(R.string.disconnect)
                    }

                    val labelStyle = if (itemObject.has("labelStyle") && !itemObject.get("labelStyle").isJsonNull) {
                        itemObject.get("labelStyle").asString
                    } else {
                        "body"
                    }

                    val buttonLabel = if (itemObject.has("buttonLabel") && !itemObject.get("buttonLabel").isJsonNull) {
                        itemObject.get("buttonLabel").asString
                    } else {
                        stringResource(R.string.disconnect)
                    }

                    ComposableP2pStallItemDisconnect1(
                        label,
                        labelStyle,
                        buttonLabel,
                        onDisconnect = {
                            onDisconnectClicked(model.stallName)
                        }
                    )

                }
                "text" -> {
                    val style = if (itemObject.has("style") && !itemObject.get("style").isJsonNull) {
                        itemObject.get("style").asString
                    } else {
                        "body"
                    }

                    val value = if (itemObject.has("value") && !itemObject.get("value").isJsonNull) {
                        itemObject.get("value").asString
                    } else {
                        "ERROR UNSET TEXT CONTENT"
                    }

                    ComposableP2pStallItemText(
                        value,
                        style
                    )

                }
                "button" -> {
                    val value = if (itemObject.has("value") && !itemObject.get("value").isJsonNull) {
                        itemObject.get("value").asString
                    } else {
                        "console.log('ERROR UNSET BUTTON VALUE CONTENT');"
                    }

                    val label = if (itemObject.has("label") && !itemObject.get("label").isJsonNull) {
                        itemObject.get("label").asString
                    } else {
                        stringResource(R.string.button)
                    }

                    ComposableP2pStallItemButton(
                        label
                    ) {
                        onButtonClicked(value)
                    }

                }
                "image" -> {
                    val filePath = if (itemObject.has("filePath") && !itemObject.get("filePath").isJsonNull) {
                        itemObject.get("filePath").asString
                    } else {
                        "/dummy" //todo
                    }
                    val caption = if (itemObject.has("caption") && !itemObject.get("caption").isJsonNull) {
                        itemObject.get("caption").asString
                    } else {
                        "^unset caption^" //todo
                    }
                    val showCaption = if (itemObject.has("showCaption") && !itemObject.get("showCaption").isJsonNull) {
                        itemObject.get("showCaption").asBoolean
                    } else {
                        false
                    }

                    val file  = File(filePath)
                    
                    ComposableP2pStallItemImage(
                        file,
                        caption,
                        showCaption
                    )
                    
                    
                }
                "gap_0" -> {
                    val size = if (itemObject.has("size") && !itemObject.get("size").isJsonNull) {
                        itemObject.get("size").asFloat
                    } else {
                        1f
                    }

                    ComposableP2pStallItemGap0(size)

                }
                "progress_0" -> {
                    val size = if (itemObject.has("size") && !itemObject.get("size").isJsonNull) {
                        itemObject.get("size").asFloat
                    } else {
                        1f
                    }

                    ComposableP2pStallItemProgress0(size)

                }
                else -> {
                    //todo
                    //todo
                    //todo
                }
            }


        }

    }
}



@Composable
fun ComposableP2pStallItemDisconnect1(
    label: String,
    labelStyle: String,
    buttonLabel: String,
    onDisconnect: ()->Unit
) {
    val finalStyle = when (labelStyle) {
        "h1" -> MaterialTheme.typography.h1
        "h2" -> MaterialTheme.typography.h2
        "caption" -> MaterialTheme.typography.caption
        else -> MaterialTheme.typography.body1
    }


    val stroke = BorderStroke(
        1.5.dp,
        MaterialTheme.colors.primary.copy(alpha = 0.5f)
    )

    Row(
        verticalAlignment = Alignment.Top
    ) {
        Text(
            label,
            style = finalStyle,
            modifier = Modifier
                .weight(1f)
                .padding(top = 7.dp)
        )
        OutlinedButton(
            onClick = {
                onDisconnect()
            },
            modifier = Modifier
                .align(Alignment.Top)
                .padding(0.dp),
            border = stroke
        ) {
            Text(
                buttonLabel,
                style = MaterialTheme.typography.button,
                textAlign = TextAlign.Center

            )
        }
    }

}

@Composable
fun ComposableP2pStallItemText(
    label: String,
    style: String
) {
    val finalStyle = when (style) {
        "h1" -> MaterialTheme.typography.h1
        "h2" -> MaterialTheme.typography.h2
        "caption" -> MaterialTheme.typography.caption
        else -> MaterialTheme.typography.body1
    }

    Text(
        label,
        style = finalStyle,
        modifier = Modifier.fillMaxWidth(1f),
    )
}



@Composable
fun ComposableP2pStallItemButton(
    label: String,
    onClick: ()->Unit
) {
    val stroke = BorderStroke(
        1.5.dp,
        MaterialTheme.colors.primary.copy(alpha = 0.5f)
    )
    OutlinedButton(
        onClick = {
            onClick()
        },
        border = stroke
    ) {
        Text(
            label,
            style = MaterialTheme.typography.button,
            modifier = Modifier.fillMaxWidth(1f),
            textAlign = TextAlign.Center

        )
    }
}


@Composable
fun ComposableP2pStallItemGap0(heightCoefficient: Float) {
    val coefficient = when {
        heightCoefficient < 0.2f -> 0.2f
        heightCoefficient > 10 -> 10f
        else -> heightCoefficient
    }
    val height = 8.dp * coefficient
    Spacer(Modifier.height(height))
}

@Composable
fun ComposableP2pStallItemImage(
    file: File,
    caption: String,
    showCaption: Boolean
) {
    Column(
        modifier = Modifier
            //.padding(PaddingValues(16.dp, 12.dp, 16.dp, 2.dp))
    ) {
        Image(
            painter = rememberImagePainter(
                file,
                builder = {
                    size(OriginalSize)
                }),
            contentDescription = caption,
            contentScale = ContentScale.FillWidth,
            modifier = Modifier.fillMaxWidth()
        )
        if (showCaption) {
            Text(
                caption,
                modifier = Modifier
                    .padding(PaddingValues(7.dp, 4.dp, 0.dp, 0.dp)),
                style = MaterialTheme.typography.caption
            )
        }
    }
}

@Composable
fun ComposableP2pStallItemProgress0(heightCoefficient: Float) {
    val coefficient = when {
        heightCoefficient < 0.2f -> 0.2f
        heightCoefficient > 3 -> 3f
        else -> heightCoefficient
    }
    val height = (45 * coefficient).dp
    CircularProgressIndicator(Modifier.size(height))
}






@Composable
fun ComposableP2pStallItemToolbar(
    label: String,
    labelStyle: String,
    buttonResourceId: Int? = null,
    onButton: ()->Unit
) {
    val finalStyle = when (labelStyle) {
        "h1" -> MaterialTheme.typography.h1
        "h2" -> MaterialTheme.typography.h2
        "caption" -> MaterialTheme.typography.caption
        else -> MaterialTheme.typography.body1
    }


    val stroke = BorderStroke(
        1.5.dp,
        MaterialTheme.colors.primary.copy(alpha = 0.5f)
    )

    Row(
        verticalAlignment = Alignment.CenterVertically
    ) {
        OutlinedButton(
            onClick = {
                onButton()
            },
            modifier = Modifier
                .padding(0.dp)
                .defaultMinSize(minWidth = 1.dp, minHeight = 1.dp),
            border = null,
            shape = CircleShape,
            contentPadding = PaddingValues(0.dp)
        ) {
            Icon(
                painter = painterResource(R.drawable.round_arrow_back_ios_24),
                contentDescription = stringResource(id = R.string.back),
                modifier = Modifier
                    .border(stroke, CircleShape)
                    .padding(8.dp)
            )
        }
        Spacer(Modifier.size(8.dp))
        Text(
            label,
            style = finalStyle,
            modifier = Modifier
        )
    }

}


/////////////////////////


@Composable
fun ComposableP2pDiscoverSectionStall(
    model: P2pDiscoverSectionStall = P2pDiscoverSectionStallFront(),
    onBrowseClicked: (String)->Unit = {},
    onDisconnectClicked: (String)->Unit = {},
    onButtonClicked: (String)->Unit = {}
) {
    ComposeAppTheme {
        val dilutedPrimaryColor =
            ColorUtils.setAlphaComponent(MaterialTheme.colors.primary.toArgb(), 100)
        val finalTintedSurfaceColor = ColorUtils.compositeColors(
            dilutedPrimaryColor,
            MaterialTheme.colors.surface.toArgb()
        )

        Surface(
            shape = MaterialTheme.shapes.medium,
            color = MaterialTheme.colors.surface,
            border = BorderStroke(1.4.dp, Color(finalTintedSurfaceColor)),
            elevation = 0.dp,
            modifier = Modifier
                .animateContentSize()
                .padding(start = 16.dp, top = 3.dp, end = 16.dp, bottom = 4.dp)
        ) {
            if (model is P2pDiscoverSectionStallFront) {
                ComposableP2pDiscoverSectionStallFront(
                    model,
                    onBrowseClicked
                )
            } else if (model is P2pDiscoverSectionStallBrowsing) {
                ComposableP2pDiscoverSectionStallBrowsing(
                    model,
                    onDisconnectClicked,
                    onButtonClicked
                )
            }
        }
    }

}


//todo fix
//@Preview(
//    uiMode = Configuration.UI_MODE_NIGHT_YES,
//    showBackground = true,
//    name = "Dark Mode of Stall"
//)
//@Composable
//fun Preview0() {
//    val gson = Gson()
//
//    ComposableP2pDiscoverSectionStall(
//        P2pDiscoverSectionStallBrowsing(
//            layoutJson = gson.fromJson("""
//                [
//                    {
//                        type: "toolbar",
//                        label: "Welcome",
//                        buttonValue: ``
//                    },
//                    {
//                        type: "gap_0",
//                        size: 0.8
//                    },
//                    {
//                        type: "text",
//                        style: "body",
//                        value: "Some content.......... ...... .... .. ... .. . . ."
//                    },
//                    {
//                        type: "gap_0",
//                        size: 1.4
//                    },
//                    {
//                        type: "text",
//                        style: "h2",
//                        value: "A dummy title"
//                    },
//                    {
//                        type: "gap_0",
//                        size: 1.7
//                    },
//                    {
//                        type: "text",
//                        style: "body",
//                        value: "Greetings, you are now directly communicating with a nearby device using WiFiDirect. On top of the communication is encrypted. Communicate with this peer by sending prepared/supported commands to it. Maybe you could think of this as like a chat-bot-like communication, or interaction with a smart-watch (trying to get that vibe)."
//                    },
//                    {
//                        type: 'gap_0',
//                        size: 3.7
//                    },
//                    {
//                        type: "button",
//                        value: "nodejs-eval",
//                        label: "Info"
//                    },
//                    {
//                        type: "button",
//                        value: "nodejs-eval",
//                        label: "Request service"
//                    },
//                    {
//                        type: 'gap_0',
//                        size: 3.7
//                    },
//                    {
//                        type: "text",
//                        style: "body",
//                        value: "Loading info; requesting it from a 'stall' hosted by this peer device."
//                    },
//                    {
//                        type: "gap_0",
//                        size: 0.7
//                    },
//                    {
//                        type: "progress_0",
//                        size: 0.9
//                    },
//                    {
//                        type: "gap_0",
//                        size: 1.7
//                    },
//                    {
//                        type: "text",
//                        style: "body",
//                        value: "Time session connected: 123354345"
//                    },
//                    {
//                         type: "gap_0",
//                         size: 1.7
//                    }
//                ]
//                """.trimIndent(), JsonArray::class.java)
//        )
//    )
//}