package app.bitcoin.baemail.p2p

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.ViewOutlineProvider
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.NontouchableCoordinatorLayout
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.android.material.radiobutton.MaterialRadioButton
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.max

class P2pEditStallFragment : Fragment(R.layout.fragment_p2p_edit_stall) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: P2pManageStallViewModel

    @Inject
    lateinit var adapterFactory: BaseListAdapterFactory
    lateinit var adapter: BaseListAdapter




    private lateinit var container: NontouchableCoordinatorLayout
    private lateinit var contentContainer: ConstraintLayout

    lateinit var close: Button
    lateinit var apply: Button
    lateinit var cancel: Button
    lateinit var actionTop0: Button

    private lateinit var inputTitle: EditText
    private lateinit var inputPki: EditText
    private lateinit var inputInfo: EditText

    lateinit var appBar: AppBarLayout
    lateinit var scroller: NestedScrollView
    lateinit var hoverHolder: ConstraintLayout

    lateinit var pkiRadio0: MaterialRadioButton
    lateinit var pkiRadio1: MaterialRadioButton

    lateinit var pkiShowCheck: MaterialCheckBox





    private val appliedInsets = AppliedInsets()

    private val availableHeightHelper = AvailableScreenHeightHelper()





    var modelObservationJob: Job? = null

    lateinit var radio0ChangeListener: SilentChangeApplyingListener
    lateinit var radio1ChangeListener: SilentChangeApplyingListener





    private val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }
    private val colorSelectorOnLight: Int by lazy {
        R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorApplyBackground: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }


    private val colorApplyBackgroundDisabled: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimaryVariant)
    }


    private val drawableApplyBackground: ColorDrawable by lazy {
        ColorDrawable(colorApplyBackgroundDisabled)
    }

    private val drawableApplySelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }






    private val dp: Float by lazy {
        resources.displayMetrics.density
    }



    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(P2pManageStallViewModel::class.java)






    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        container = requireView().findViewById(R.id.container)
        contentContainer = requireView().findViewById(R.id.content_container)

        close = requireView().findViewById(R.id.close)
        apply = requireView().findViewById(R.id.apply)
        cancel = requireView().findViewById(R.id.cancel)
        actionTop0 = requireView().findViewById(R.id.top_action_0)

        inputTitle = requireView().findViewById(R.id.input_title)
        inputPki = requireView().findViewById(R.id.input_pki)
        inputInfo = requireView().findViewById(R.id.input_info)

        appBar = requireView().findViewById(R.id.app_bar)
        scroller = requireView().findViewById(R.id.scroller)
        hoverHolder = requireView().findViewById(R.id.hover_holder)

        pkiRadio0 = requireView().findViewById(R.id.radio_0)
        pkiRadio1 = requireView().findViewById(R.id.radio_1)

        pkiShowCheck = requireView().findViewById(R.id.pki_show_password)






        val topBarElevation = resources.getDimension(R.dimen.added_paymails_fragment__top_bar_elevation)




        contentContainer.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop//,
                //,
                //bottom = windowInsets.systemWindowInsetBottom
            )

            appliedInsets.top = windowInsets.systemWindowInsetTop
            appliedInsets.bottom = windowInsets.systemWindowInsetBottom

            windowInsets
        }






        availableHeightHelper.setup(
            requireContext(),
            requireActivity().window,
            viewLifecycleOwner,
            { requireView() }
        )

        availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
            height ?: return@Observer

            val maxHeight = container.height// - appliedInsets.top// - appliedInsets.bottom

            val keyboardHeight = max(maxHeight - height, 0)
            Timber.d("keyboardHeight: $keyboardHeight")


            hoverHolder.translationY = -1f * keyboardHeight

            //this view touches the bottom of the layout

            val canScrollDown = scroller.canScrollVertically(1)

            scroller.updatePadding(
                bottom = /*(dp * 75).toInt() + */keyboardHeight
            )

//            if (!canScrollDown && inputContent.isFocused) {
//                Timber.d(">>> doing extra scroll")
//                scroller.scrollBy(0, keyboardHeight)
//            }


        })







        let { //avoid drawing the shadow appBar automatically lays out with at first
            var cleanupListener: () -> Unit = {
                Timber.d("EMPTY!!!")
            }

            val listener = object : ViewTreeObserver.OnPreDrawListener {

                override fun onPreDraw(): Boolean {
                    appBar.elevation = 0f

                    cleanupListener()

                    return true
                }

            }

            cleanupListener = {
                appBar.viewTreeObserver.removeOnPreDrawListener(listener)
            }
            appBar.viewTreeObserver.addOnPreDrawListener(listener)
        }






        scroller.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener {
            override fun onScrollChange(
                v: NestedScrollView,
                scrollX: Int,
                scrollY: Int,
                oldScrollX: Int,
                oldScrollY: Int
            ) {
                if (scrollY < 8 * resources.displayMetrics.density) {
                    appBar.elevation = 0f
                } else {
                    appBar.elevation = topBarElevation
                }
            }

        })

        scroller.overScrollMode = View.OVER_SCROLL_NEVER











        let {
            val colorSelector = R.color.selector_on_surface.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }
//
//            close.foreground = DrawableState.getNew(colorSelector)

            close.setOnClickListener {
                imm.hideSoftInputFromWindow(
                    requireView().windowToken,
                    0
                )

                findNavController().popBackStack()
            }

            close.clipToOutline = true
            close.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    view ?: return
                    outline ?: return

                    outline.setOval(0, 0, view.width, view.height)
                }

            }
        }












        hoverHolder.visibility = View.INVISIBLE



        apply.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                modelObservationJob?.cancel()

                imm.hideSoftInputFromWindow(
                    requireView().windowToken,
                    0
                )
                close.requestFocus()

                delay(100)

                viewModel.onClickedApplyEditOfStall()
                findNavController().popBackStack()
            }
        }

        cancel.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                modelObservationJob?.cancel()

                imm.hideSoftInputFromWindow(
                    requireView().windowToken,
                    0
                )
                close.requestFocus()

                delay(100)

                viewModel.onClickedCancelEditOfStall()
                findNavController().popBackStack()
            }

        }





        radio0ChangeListener = SilentChangeApplyingListener({
            pkiRadio1.setOnCheckedChangeListener(null)
            pkiRadio1.isChecked = false
            pkiRadio1.setOnCheckedChangeListener(radio1ChangeListener)
        }) { isChecked ->
            if (isChecked) viewModel.onRadioTogglePki(true)
        }

        radio1ChangeListener = SilentChangeApplyingListener({
            pkiRadio0.setOnCheckedChangeListener(null)
            pkiRadio0.isChecked = false
            pkiRadio0.setOnCheckedChangeListener(radio0ChangeListener)
        }) { isChecked ->
            if (isChecked) viewModel.onRadioTogglePki(false)
        }

        pkiRadio0.setOnCheckedChangeListener(radio0ChangeListener)
        pkiRadio1.setOnCheckedChangeListener(radio1ChangeListener)
        radio0ChangeListener.button = pkiRadio0
        radio1ChangeListener.button = pkiRadio1







        inputPki.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                pkiRadio1.isChecked = true
            }
        }

        pkiShowCheck.setOnClickListener {
            Timber.d("do toggle new:${!viewModel.contentOfEdit.value.isPkiHidden}")
            viewModel.onTogglePkiVisibilityInEdit(!viewModel.contentOfEdit.value.isPkiHidden)
        }


        val inputPkiBackground = MaterialShapeDrawable()
        inputPkiBackground.fillColor = ColorStateList.valueOf(Color.TRANSPARENT)
        inputPkiBackground.strokeColor = ColorStateList.valueOf(requireContext().getColorFromAttr(R.attr.colorSurfaceVariant))
        inputPkiBackground.strokeWidth = dp * 0.6f
        inputPkiBackground.setCornerSize(dp * 8)

        inputPki.background = inputPkiBackground



        val inputTitleBackground = MaterialShapeDrawable()
        inputTitleBackground.fillColor = ColorStateList.valueOf(Color.TRANSPARENT)
        inputTitleBackground.strokeColor = ColorStateList.valueOf(requireContext().getColorFromAttr(R.attr.colorSurfaceVariant))
        inputTitleBackground.strokeWidth = dp * 0.6f
        inputTitleBackground.setCornerSize(dp * 8)

        inputTitle.background = inputTitleBackground

        val inputInfoBackground = MaterialShapeDrawable()
        inputInfoBackground.fillColor = ColorStateList.valueOf(Color.TRANSPARENT)
        inputInfoBackground.strokeColor = ColorStateList.valueOf(requireContext().getColorFromAttr(R.attr.colorSurfaceVariant))
        inputInfoBackground.strokeWidth = dp * 0.6f
        inputInfoBackground.setCornerSize(dp * 8)

        inputInfo.background = inputInfoBackground




        modelObservationJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.contentOfEdit.collect { model ->

                inputTitle.removeTextChangedListener(inputTitleWatcher)
                if (inputTitle.text.toString() != model.title) {
                    inputTitle.setText(model.title)
                }
                inputTitle.addTextChangedListener(inputTitleWatcher)


                inputPki.removeTextChangedListener(inputPkiWatcher)
                if (inputPki.text.toString() != model.pki) {
                    inputPki.setText(model.pki)
                }
                inputPki.addTextChangedListener(inputPkiWatcher)

                initPasswordVisibility(model.isPkiHidden)



                inputInfo.removeTextChangedListener(inputInfoWatcher)
                if (inputInfo.text.toString() != model.info) {
                    inputInfo.setText(model.info)
                }
                inputInfo.addTextChangedListener(inputInfoWatcher)

                if (model.forcePaymailPki) {
                    if (!pkiRadio0.isChecked || pkiRadio1.isChecked) {
                        pkiRadio0.setOnCheckedChangeListener(null)
                        pkiRadio1.setOnCheckedChangeListener(null)
                        pkiRadio0.isChecked = true
                        pkiRadio1.isChecked = false
                        pkiRadio0.setOnCheckedChangeListener(radio0ChangeListener)
                        pkiRadio1.setOnCheckedChangeListener(radio1ChangeListener)
                    }
                } else {
                    if (pkiRadio0.isChecked || !pkiRadio1.isChecked) {
                        pkiRadio0.setOnCheckedChangeListener(null)
                        pkiRadio1.setOnCheckedChangeListener(null)
                        pkiRadio0.isChecked = false
                        pkiRadio1.isChecked = true
                        pkiRadio0.setOnCheckedChangeListener(radio0ChangeListener)
                        pkiRadio1.setOnCheckedChangeListener(radio1ChangeListener)
                    }
                }


                //manages the showing/hiding of cancel/apply buttons
                if (model.isModified) {
                    hoverHolder.visibility = View.VISIBLE
                } else {
                    hoverHolder.visibility = View.INVISIBLE
                }

            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            focusOnInfoInput()
        }

    }

    private val passwordTransformationMethod: PasswordTransformationMethod by lazy {
        PasswordTransformationMethod.getInstance()
    }

    private fun initPasswordVisibility(hidePass: Boolean) {
        if (inputPki.transformationMethod != null && hidePass) {
            return
        } else if (inputPki.transformationMethod == null && !hidePass) {
            return
        }

        inputPki.removeTextChangedListener(inputPkiWatcher)

        // store the current cursor position
        val selection: Int = inputPki.getSelectionEnd()
        if (!hidePass) {
            inputPki.transformationMethod = null
        } else {
            inputPki.transformationMethod = passwordTransformationMethod
        }

        // restore cursor position
        inputPki.setSelection(selection)
        // update checkbox
        pkiShowCheck.isChecked = !hidePass


        inputPki.addTextChangedListener(inputPkiWatcher)
    }


    private fun focusOnInfoInput() {
        if (inputTitle.requestFocus()) {
            Timber.d("onStart got focus")

            if (/*!close.isFocused && */!imm.isActive(inputTitle)) {
                imm.restartInput(inputTitle)
            }
        }
    }


    private val inputTitleWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            s ?: return
            viewModel.onTitleInputOfEditChanged(s.toString())
        }

    }

    private val inputPkiWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            s ?: return
            viewModel.onPkiInputOfEditChanged(s.toString())

            pkiRadio0.isChecked = false
            pkiRadio1.isChecked = true
        }

    }

    private val inputInfoWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            s ?: return
            viewModel.onInfoInputOfEditChanged(s.toString())
        }

    }


    class SilentChangeApplyingListener(
        val uncheckOthers: ()->Unit,
        val onChanged: (Boolean)->Unit
    ) : CompoundButton.OnCheckedChangeListener {
        var button: CompoundButton? = null

        override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
            if (isChecked) {
                uncheckOthers()
            }

            onChanged(isChecked)
        }

    }


}