package app.bitcoin.baemail.p2p

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.doOnPreDraw
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import com.google.android.material.appbar.AppBarLayout
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

//todo this is missing drop-down for the connection-type & also UI for connection-type-error-resolution
class P2pManageStallFragment : Fragment(R.layout.fragment_p2p_manage_stall) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: P2pManageStallViewModel


    lateinit var buttonActive: Button
    lateinit var buttonClose: Button
    lateinit var buttonEdit: Button
    lateinit var valueName: TextView
    lateinit var valuePki: TextView
    lateinit var valueInfo: TextView


    lateinit var appBar: AppBarLayout
    lateinit var scroller: NestedScrollView

    private lateinit var requestPermissionLauncher: ActivityResultLauncher<Array<String>>

    private var runOnP2pPermissionsGranted: Runnable? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactory
        )[P2pManageStallViewModel::class.java]

        requestPermissionLauncher = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { result ->
            viewModel.onRequestP2pPermissionsResults(result)

            if (viewModel.p2pPermissionState.value.hasMissingPermissions()) {
                //todo inform the user why the action cannot proceed
                Toast.makeText(requireContext(), "missing permissions", Toast.LENGTH_SHORT).show()

            } else {
                runOnP2pPermissionsGranted?.run()
            }

            runOnP2pPermissionsGranted = null
        }

        setFragmentResultListener(LocPermissionRequestRationaleFragment.KEY_PERMISSION_REQUEST_RATIONALE) { requestKey, bundle ->
            if (bundle.getBoolean(LocPermissionRequestRationaleFragment.KEY_YES_REQUEST)) {
                //can proceed with requesting of permissions
                requestPermissionLauncher.launch(viewModel.getRequiredP2pPermissions())
            } else {
                runOnP2pPermissionsGranted = null
            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonActive = view.findViewById(R.id.button_active)
        buttonClose = view.findViewById(R.id.close_stall)
        buttonEdit = view.findViewById(R.id.edit_stall)
        valueName = view.findViewById(R.id.value_name)
        valuePki = view.findViewById(R.id.value_pki)
        valueInfo = view.findViewById(R.id.value_info)

        appBar = requireView().findViewById(R.id.app_bar)
        scroller = requireView().findViewById(R.id.scroller)

        val topBarElevation = resources.getDimension(R.dimen.added_paymails_fragment__top_bar_elevation)

        appBar.doOnPreDraw {
            appBar.elevation = 0f
        }

        scroller.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener {
            override fun onScrollChange(
                v: NestedScrollView,
                scrollX: Int,
                scrollY: Int,
                oldScrollX: Int,
                oldScrollY: Int
            ) {
                if (scrollY < 8 * resources.displayMetrics.density) {
                    appBar.elevation = 0f
                } else {
                    appBar.elevation = topBarElevation
                }
            }

        })

        scroller.overScrollMode = View.OVER_SCROLL_NEVER

        buttonClose.setOnClickListener {
            findNavController().popBackStack()
        }

        buttonActive.setOnClickListener {
            onStartStallClicked()
        }

        buttonEdit.setOnClickListener {
            findNavController().navigate(R.id.action_p2pManageStallFragment_to_p2pEditStallFragment)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.p2pPermissionState.collect { permissionState ->
                    if (permissionState.mustCheckLocPermission) {
                        handleMustCheckLocPermission()
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.content.collect { model ->

                    valueName.text = model.title
                    if (model.pki.isBlank()) {
                        valuePki.text = "-"
                    } else {
                        //TODO
                        valuePki.text = "${model.pki.substring(0,5)}◉◉${model.pki.substring(model.pki.length - 5)}"
                    }
                    valueInfo.text = model.info


                    if (model.isRegistryActive) {
                        buttonActive.setText(R.string.stop)
                    } else {
                        buttonActive.setText(R.string.start)
                    }
                }
            }
        }
    }

    private fun handleMustCheckLocPermission() {
        val permissionsToCheck = viewModel.getRequiredP2pPermissions()

        val hasPermissionResults = mutableMapOf<String, Boolean>()
        permissionsToCheck.forEach { permission ->
            hasPermissionResults[permission] = PackageManager.PERMISSION_GRANTED ==
                    ContextCompat.checkSelfPermission(
                        requireContext(),
                        permission
                    )
        }

        val requirePermissionRationaleResults = mutableMapOf<String, Boolean>()
        hasPermissionResults.forEach { permissionResultEntry ->
            if (permissionResultEntry.value) return@forEach

            requirePermissionRationaleResults[permissionResultEntry.key] =
                shouldShowRequestPermissionRationale(permissionResultEntry.key)
        }

        viewModel.onP2pPermissionCheckResults(
            hasPermissionResults,
            requirePermissionRationaleResults
        )
    }

    private fun onStartStallClicked() {
        val onP2pPermissionsGranted = Runnable {
            viewModel.onClickedServiceRegistryToggle()
        }

        val p2pPermissionState = viewModel.p2pPermissionState.value
        if (p2pPermissionState.mustCheckLocPermission) return

        if (p2pPermissionState.hasMissingPermissions()) {
            runOnP2pPermissionsGranted = onP2pPermissionsGranted
            doRequestP2pPermissions()
            return
        }

        onP2pPermissionsGranted.run()
    }

    private fun doRequestP2pPermissions() {
        val p2pPermissionState = viewModel.p2pPermissionState.value

        val permissionsShouldShowRequestRationale =
            p2pPermissionState.shouldShowRequestPermissionRationaleResults.let {
                if (it == null) throw RuntimeException("unexpected usage")
                it.mapNotNull { permissionEntry ->
                    if (!permissionEntry.value) return@mapNotNull null
                    permissionEntry.key
                }
            }

        if(permissionsShouldShowRequestRationale.isNotEmpty()) {
            //todo consider passing permissionsShouldShowRequestRationale to the destination-fragment
            Timber.d("...should show rational for the following permissions: $permissionsShouldShowRequestRationale")

            findNavController().navigate(
                R.id.action_p2pFragment_to_locPermissionRequestRationaleFragment
            )
            return
        }

        requestPermissionLauncher.launch(viewModel.getRequiredP2pPermissions())
    }

}