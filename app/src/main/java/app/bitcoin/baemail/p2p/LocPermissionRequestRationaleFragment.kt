package app.bitcoin.baemail.p2p

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.util.RoundedBottomSheetDialogFragment

class LocPermissionRequestRationaleFragment : RoundedBottomSheetDialogFragment() {

    companion object {
        const val KEY_PERMISSION_REQUEST_RATIONALE = "location_request_rationale"
        const val KEY_YES_REQUEST = "yes_request"
    }



    lateinit var requestPermission: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_loc_permission_req_rationale, container, false)
    }





    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        requestPermission = view.findViewById(R.id.request_permission)

        requestPermission.setOnClickListener {
            setFragmentResult(KEY_PERMISSION_REQUEST_RATIONALE, bundleOf(KEY_YES_REQUEST to true))
            dismiss()
        }

    }


    override fun getPeekHeight(): Int {
        return CONST_UNSET
    }

    override fun shouldFitToContents(): Boolean {
        return true
    }
}