package app.bitcoin.baemail.p2p

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import app.bitcoin.baemail.intercept.InterceptActivity
import app.bitcoin.baemail.core.data.util.AuthenticatedPaymail
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.data.nodejs.NodeConnectedLiveData
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.presentation.util.PermissionStateHelper
import app.local.p2p.serviceDiscovery.*
import com.google.gson.Gson
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber


class P2pStallRepository(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil,
    private val gson: Gson,
    private val authRepository: AuthRepository,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val nearbyConnectionsHelper: NearbyConnectionsHelper,
    val permissionStateHelper: PermissionStateHelper,
    private val nodeConnectedLiveData: NodeConnectedLiveData,
    private val secureDataSource: SecureDataSource,
    val informP2pChangeHelper: InformP2pChangeHelper,
    private val p2pPrefs: DataStore<Preferences>,
    private val scope: CoroutineScope,
    private val appForegroundFlow: StateFlow<Boolean>
) {

    companion object {
        const val VALUE_UNSET = "INVALID_UNSET"
        const val KEY_PREFIX_PKI = "stall_pki_"
        const val KEY_PREFIX_TITLE = "stall_title_"
        const val KEY_PREFIX_INFO = "stall_info_"
        const val KEY_PREFIX_FORCE_USE_PAYMAIL_PKI = "stall_force_use_paymail_pki"
        const val KEY_PREFIX_CUSTOM_PKI_WIF_PRIVATE = "stall_custom_pki_wif_private"
    }

    /**
     * source of truth for the configuration of user's stall
     */
    val currentPeerServiceConfigFlow = MutableStateFlow(PeerServiceConfig(
        P2pStallManager.PeerServiceId(
                VALUE_UNSET,
                VALUE_UNSET,
                VALUE_UNSET
        ),
        null,
        true,
        "",
        ConnectionType.WIFI_DIRECT
    ))

    /**
     * source of truth for the identification of the currently active user's stall
     */
    private val currentPeerFlow = MutableStateFlow(P2pStallManager.PeerServiceId(
        VALUE_UNSET,
        VALUE_UNSET,
        VALUE_UNSET
    ))

    /**
     * source of truth for the user-chosen communication type
     */
    private val _connectionTypeFlow = MutableStateFlow(ConnectionType.WIFI_DIRECT)
    val connectionTypeFlow: StateFlow<ConnectionType>
        get() = _connectionTypeFlow

    /**
     * responsible for interfacing data parsed out of nearby-connections-lib and the prepared
     * communication setup on the node-side
     */
    private val nearbyConnectionNodeDelegate = NearbyConnectionNodeDelegate(
        scope,
        nodeRuntimeRepository
    )

    private val registryHelperOfNearby = P2pStallRegistryHelper4(
        nearbyConnectionsHelper,
        gson,
        scope,
        permissionStateHelper.playStateHolder.map { it.availability.isAvailable() },
        nearbyConnectionNodeDelegate::onNewConnection,
        nearbyConnectionNodeDelegate::onLostConnection,
        informP2pChangeHelper.sendRequest,
        nearbyConnectionNodeDelegate::getStreamDataInputToSend,
        nearbyConnectionNodeDelegate::delegateIncomingMessageHandling,
        nearbyConnectionNodeDelegate::delegateIncomingByteMessageHandling,
        nearbyConnectionNodeDelegate::delegateIncomingFileRequestHandling,
        nearbyConnectionNodeDelegate::delegateIncomingFileResponseHandling,
        nearbyConnectionNodeDelegate::delegateIncomingErrorResponseHandling
    )

    private val registryOfWifiDirect = P2pStallRegistryHelper1(
        appContext,
        scope,
        onConnectionAttempt = { foundStall ->
            scope.launch {
                nodeRuntimeRepository.informWifiDirectP2pConnectionChange(
                    foundStall,
                    null,
                    null
                )
            }
        },
        onGroupInfoChange = { wifiP2pInfo ->
            scope.launch {
                nodeRuntimeRepository.informWifiDirectP2pConnectionChange(
                    null,
                    wifiP2pInfo,
                    null
                )
            }
        },
        onParticipantChange = { peers ->
            scope.launch {
                nodeRuntimeRepository.informWifiDirectP2pConnectionChange(
                    null,
                    null,
                    peers
                )
            }
        }
    )

    /**
     * holder of all supported stall-communication types
     */
    val serviceRegistryManager = P2pStallRegistryManager(
        registryHelperOfNearby,
        P2pStallRegistryHelper0(appContext, scope),
        registryOfWifiDirect,
        scope
    )

    val stallManager: P2pStallManager by lazy {
        //action-intent for the system-notification representing the stall-service
        val goToManageStall = Intent(appContext, InterceptActivity::class.java)
        goToManageStall.putExtra(
            InterceptActivity.KEY_DESTINATION,
            InterceptActivity.DESTINATION_P2P_STALL_MANAGEMENT
        )

        P2pStallManager.setup(
            appContext,
            scope,
            goToManageStall,
            currentPeerFlow,
            serviceRegistryManager,
            startServer = { port, peerId ->
                nodeRuntimeRepository.startStallSocketServer(
                    port,
                    currentPeerServiceConfigFlow.value
                )
            },
            stopServer = {
                nodeRuntimeRepository.stopStallSocketServer()
            }
        )
    }

    /**
     * source of truth for user's desired discovery-status
     */
    private val _discoveryFlow = MutableStateFlow(false)
    val discoveryFlow: StateFlow<Boolean>
        get() = _discoveryFlow

    /**
     * helper that is responsible for putting stall-discovery to sleep while the app is not
     * in foreground for the browsing user
     */
    private val discoveryReleaseHelper = DelayedReleaseHelper(
        discoveryFlow,
        appForegroundFlow,
        scope,
        20 * 1000L,
        startNow = {
            stallManager.serviceRegistryHelper.beginDiscovery(currentPeerFlow.value)
        },
        stopNow = {
            stallManager.serviceRegistryHelper.stopDiscovery()
        }
    )


    /**
     * used for signalling the need to reinitialize the usage of stall-preferences
     */
    private val prefsInitializedState = MutableStateFlow(0)

    private var prefsObservationJob: Job? = null

    private var informNodeVmOfStallInfoJob: Job? = null


    init {

        //observe the active paymail & shut the stall down on active-paymail-change
        coroutineUtil.appScope.launch {
            authRepository.activePaymail.collectLatest { activePaymail ->
                activePaymail?.paymail ?: return@collectLatest

                if (currentPeerFlow.value.paymail == activePaymail.paymail) return@collectLatest

                if (P2pStallManager.State.OFF != stallManager.stallRunningState.value.state) {
                    Timber.d("stall-manager was shut down because of the active paymail change")
                    stopDiscoveryOfPeers()
                    stallManager.stopAdvertisingService() //todo maybe calling this instead? #stopAdvertisingServiceAndAwait
                }

                Timber.d("#currentPeerFlow.paymail initialized from active-paymail; proceeding with setting up propper observation using this value")

                currentPeerFlow.value = currentPeerFlow.value.copy(
                    paymail = activePaymail.paymail
                )

                setupPrefsObservation(activePaymail.paymail)
            }
        }

        //node should be guaranteed to be available for use while the stall is active
        scope.launch {
            stallManager.stallRunningState
                .onEach { state ->
                    if (P2pStallManager.State.RUNNING == state.state) {
                        nodeRuntimeRepository.startForeground()
                    } else if (P2pStallManager.State.OFF == state.state) {
                        //todo improvement... #stop call prob only needs to be called if no other part of the app needs this thing to be started
                        nodeRuntimeRepository.stopForeground()
                    }
                }
                .catch { e ->
                    Timber.e(e, "crashed when observing stall-running-state")
                }
                .collect()

//            stallManager.stallRunningState.collect { state ->
//                if (P2pStallManager.State.RUNNING == state.state) {
//                    walletRepository.startForeground()
//                } else if (P2pStallManager.State.OFF == state.state) {
//                    //todo improvement... #stop call prob only needs to be called if no other part of the app needs this thing to be started
//                    walletRepository.stopForeground()
//                }
//            }
        }


        // react to starting/stopping of the managed node-vm
        // ...force-stop the stall if node crashes for whatever reason - sanity cleanup
        // ...start informing node-vm of stall-info value as it changes
        nodeConnectedLiveData.observeForever { isConnected ->
            isConnected ?: return@observeForever

            informNodeVmOfStallInfoJob?.cancel()

            if (!isConnected) {
                if (P2pStallManager.State.RUNNING == stallManager.stallRunningState.value.state) {
                    Timber.d("Detected a termination of the node-process during the active functioning of the p2p-stall; shutting the p2p-stall down")

                    stallManager.stopAdvertisingService()

                }

                stopDiscoveryOfPeers()

            } else {
                informNodeVmOfStallInfoJob = scope.launch {

                    //passes stall-config to node-vm-side
                    launch {
                        currentPeerServiceConfigFlow.collect { config ->
                            nodeRuntimeRepository.informStallConfigChanged(config)
                        }
                    }

                    //keep the node-side up to date with info on stalls that have been discovered
                    launch {
                        stallManager.serviceRegistryHelper.discoveredServices.collectLatest { model ->
                            nodeRuntimeRepository.informDiscoveredStallsChanged(model)
                        }
                    }

                    //passes user-composed stall-description to the node-vm-side
                    launch {
                        flowOfInfo().collect { info ->
                            val result = nodeRuntimeRepository.runAsyncScript(
                                """
                                    try {
                                        context.stall.my.util.onStallInfoChanged(`${info}`);
                                    } catch (e) {console.log(e);}
                                """.trimIndent()
                            )

                            if (result is RunScript.Response.Success) {
                                //do nothing
                            } else {
                                result as RunScript.Response.Fail
                                Timber.d(result.throwable)
                            }

                        }
                    }

                }
            }
        }

        //handle p2p-stall specific information updates originating from node-side
        scope.launch {

            //following is only used when p2p is set to rely on `nearby connections`;
            // messages received via nearby-connections-library are redirected to a http-server
            // running inside of node-vm; this handles necessary incoming meta-info about the server
            launch {
                informP2pChangeHelper.nearbyMeta.receiveAsFlow().collect { model ->
                    nearbyConnectionNodeDelegate.updateSendingMeta(model.port)
                }
            }

            //following is only used when p2p is set to rely on `nearby connections`;
            // this handles the requests to disconnect from p2p-stalls
            launch {
                informP2pChangeHelper.disconnectRequest.receiveAsFlow().collect { endpointId ->
                    registryHelperOfNearby.disconnectFromEndpoint(endpointId)
                }
            }
        }


    }

    /**
     * responsible for initialization of the user's stall-state
     */
    private fun setupPrefsObservation(paymail: String) {
        prefsObservationJob?.cancel()

        //restore currentPeerFlow from disk
        prefsObservationJob = scope.launch {
            prefsInitializedState.combine(p2pPrefs.data) { p0, p1 ->
                p1
            }.combine(_connectionTypeFlow) { p0, p1 ->
                p0 to p1
            }.collectLatest { (prefs, connectionType) ->

                Timber.d("triggered rebuild of p2p-config-state")

                val pki = prefs[stringPreferencesKey(KEY_PREFIX_PKI + paymail)] ?: VALUE_UNSET
                val title = prefs[stringPreferencesKey(KEY_PREFIX_TITLE + paymail)] ?: VALUE_UNSET

                var serviceId = P2pStallManager.PeerServiceId(
                        pki,
                        paymail,
                        title
                )


                val forceUsePaymailPki = prefs[booleanPreferencesKey(KEY_PREFIX_FORCE_USE_PAYMAIL_PKI + paymail)] ?: true
                val customPkiWifPrivate = prefs[stringPreferencesKey(KEY_PREFIX_CUSTOM_PKI_WIF_PRIVATE + paymail)] ?: ""
                val valueCustomPkiWifPrivate = secureDataSource.getCustom(customPkiWifPrivate) ?: ""
                var valuePkiWifPrivate: String? = null


                if (forceUsePaymailPki) {
                    val paymailWalletSeed = try {
                        secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(paymail))
                    } catch (e: Exception) {
                        Timber.e(e, "error retrieving seed")
                        return@collectLatest
                    }

                    val joinedPaymailWalletSeed = paymailWalletSeed.fold("") { acc, item ->
                        if (acc.isEmpty()) {
                            item
                        } else {
                            "$acc $item"
                        }
                    }

                    val pkiPath = try {
                        secureDataSource.getPathForPaymailMnemonic(AuthenticatedPaymail(paymail))
                    } catch (e: Exception) {
                        Timber.e(e, "error retrieving pki-path matching the sending-paymail")
                        return@collectLatest
                    }

                    val result = nodeRuntimeRepository.runAsyncScript("""
                        try {
                            var joinedPaymailSeed = "$joinedPaymailWalletSeed";
                            var paymailMnemonic = context.mnemonic.fromString(joinedPaymailSeed);
                            var paymailHdPrivateKey = context.bsv.HDPrivateKey.fromSeed(paymailMnemonic.toSeed());
                            var paymailPkiHdPrivateKey = paymailHdPrivateKey.deriveChild("$pkiPath");
                            var paymailPkiPublicKey = context.bsv.PublicKey.fromPrivateKey(paymailPkiHdPrivateKey.privateKey);
                            
                            return paymailPkiHdPrivateKey.privateKey.toWIF() + ';' + paymailPkiPublicKey.toString();
                        } catch (e) {
                            console.log(e, 'wut?');
                            return ';'
                        }
                    """.trimIndent())

                    if (result is RunScript.Response.Success) {
                        val parts = result.content.split(";")
                        val privateKey = parts[0]
                        val publicKey = parts[1]

                        serviceId = serviceId.copy(
                                pki = publicKey
                        )
                        valuePkiWifPrivate = privateKey


                    } else {
                        result as RunScript.Response.Fail
                        Timber.e(result.throwable) //todo

                        serviceId = serviceId.copy(
                                pki = ""
                        )
                        valuePkiWifPrivate = ""


                        //todo set an error-flag which is used to stop usage when it is TRUE

                    }


                } else {
                    //get public key of the provided custom private key wif
                    val result = nodeRuntimeRepository.runAsyncScript("""
                        try {
                            var privateKey = context.bsv.PrivateKey.fromWIF('$valueCustomPkiWifPrivate');
                            var publicKey = context.bsv.PublicKey.fromPrivateKey(privateKey);
                            
                            return publicKey.toString();
                        } catch (e) {
                            console.log(e);
                            return '';
                        }
                    """.trimIndent())

                    if (result is RunScript.Response.Success) {
                        val publicKey = result.content

                        serviceId = serviceId.copy(
                                pki = publicKey
                        )
                        valuePkiWifPrivate = valueCustomPkiWifPrivate


                    } else {
                        result as RunScript.Response.Fail
                        Timber.d(result.throwable)

                        serviceId = serviceId.copy(
                                pki = ""
                        )
                        valuePkiWifPrivate = valueCustomPkiWifPrivate

                        //todo set an error-flag which is used to stop usage when it is TRUE

                    }
                }


                Timber.d("updating `currentPeerServiceConfigFlow` ... title:${serviceId.title}; pki:${serviceId.pki}") //todo

                currentPeerServiceConfigFlow.value = PeerServiceConfig(
                    serviceId,
                    valuePkiWifPrivate,
                    forceUsePaymailPki,
                    valueCustomPkiWifPrivate,
                    connectionType
                )

                serviceRegistryManager.setConnectionType(connectionType)

                currentPeerFlow.value = currentPeerFlow.value.copy(
                    pki = serviceId.pki,
                    title = serviceId.title
                )

                if (VALUE_UNSET == serviceId.title) {
                    launch {
                        onDetectedUninitializedStallConfig()
                    }
                }
            }
        }


    }

    private suspend fun onDetectedUninitializedStallConfig() {

        //todo
        updateStall(
                title = "Fruity stall",
                forceUsePaymailPki = true
        )
        //todo
        //todo
        //todo
        //todo
    }




    fun requestStartDiscoveryOfPeers() {
        _discoveryFlow.value = true
    }


    fun stopDiscoveryOfPeers() {
        _discoveryFlow.value = false
    }


    suspend fun connectToStall(stall: FoundStall) {
        if (ConnectionType.NEARBY == _connectionTypeFlow.value) {
            try {
                registryHelperOfNearby.connectToEndpoint(stall.host!!)

            } catch (e: Exception) {
                Timber.e(e)
                informP2pChangeHelper.postEvent(InformP2pChangeHelper.P2pEvent("NEARBY_CONNECT_ERROR"))

            }
            return
        }

        if (ConnectionType.WIFI_DIRECT == _connectionTypeFlow.value) {
            try {
                registryOfWifiDirect.connectPeer(stall.name)

            } catch (e: Exception) {
                Timber.e(e)
                informP2pChangeHelper.postEvent(InformP2pChangeHelper.P2pEvent("WIFI_DIRECT_CONNECT_ERROR"))

            }
            return
        }


        val response = nodeRuntimeRepository.runAsyncScript("""
            if (context.stall.peers.connected.hasOwnProperty('${stall.name}')) {
                console.log(new Error('this peer is already connected'));
                return;
            }
            
            context.stall.peers.connectToStall(
                '${stall.name}',
                '${stall.title}',
                '${stall.paymail}',
                '${stall.host}',
                '${stall.port}',
                '${stall.pki}'
            );
            
            
        """.trimIndent())


        if (response is RunScript.Response.Success) {
//                Timber.d("success; content: ${response.content}")
        } else {
            response as RunScript.Response.Fail
            Timber.d("success; content: ${response.responseData}")
        }
    }


    suspend fun updateStall(
        pki: String? = null,
        title: String? = null,
        forceUsePaymailPki: Boolean? = null,
        customPkiWifPrivate: String? = null
    ) {
        val currentPaymail = currentPeerFlow.value.paymail
        p2pPrefs.edit { prefs ->
            pki?.let {
                prefs[stringPreferencesKey(KEY_PREFIX_PKI + currentPaymail)] = it
            }
            title?.let {
                prefs[stringPreferencesKey(KEY_PREFIX_TITLE + currentPaymail)] = it
            }
            forceUsePaymailPki?.let {
                prefs[booleanPreferencesKey(KEY_PREFIX_FORCE_USE_PAYMAIL_PKI + currentPaymail)] = it
            }
            customPkiWifPrivate?.let {
                val key = stringPreferencesKey(KEY_PREFIX_CUSTOM_PKI_WIF_PRIVATE + currentPaymail)

                var currentKeyId = prefs[key]
                if (!currentKeyId.isNullOrBlank()) {
                    secureDataSource.updateCustom(currentKeyId, it)

                    prefsInitializedState.value = prefsInitializedState.value + 1

                } else {
                    currentKeyId = secureDataSource.addCustom(it)
                    prefs[key] = currentKeyId
                }

            }
        }
    }





    fun flowOfInfo(): Flow<String> {
        return prefsInitializedState.combine(p2pPrefs.data) { p0, p1 ->
            p1
        }.mapLatest { prefs ->
            prefs[stringPreferencesKey(KEY_PREFIX_INFO + currentPeerFlow.value.paymail)]
                ?: VALUE_UNSET
        }
    }

    suspend fun updateInfo(info: String) {
        val paymail = currentPeerFlow.value.paymail
        p2pPrefs.edit { prefs ->
            prefs[stringPreferencesKey(KEY_PREFIX_INFO + paymail)] = info
        }
    }



    suspend fun setConnectionType(type: ConnectionType) {
        if (_connectionTypeFlow.value == type) return

        //must stop the stall beforehand
        stopDiscoveryOfPeers()
        stallManager.stopAdvertisingServiceAndAwait()

        //apply config-change
        _connectionTypeFlow.value = type
    }

    fun getAppropriatePermissionsArray(): Array<String> {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            arrayOf(
                Manifest.permission.POST_NOTIFICATIONS,
                Manifest.permission.NEARBY_WIFI_DEVICES,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.CHANGE_WIFI_STATE,
                Manifest.permission.BLUETOOTH_ADVERTISE,
                Manifest.permission.BLUETOOTH_CONNECT,
                Manifest.permission.BLUETOOTH_SCAN,
                Manifest.permission.NFC
            )

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            arrayOf(
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.CHANGE_WIFI_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.BLUETOOTH_ADVERTISE,
                Manifest.permission.BLUETOOTH_CONNECT,
                Manifest.permission.BLUETOOTH_SCAN,
                Manifest.permission.NFC
            )
        } else {
            arrayOf(
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.CHANGE_WIFI_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.NFC
            )
        }
    }


    data class PeerServiceConfig(
        val id: P2pStallManager.PeerServiceId,
        val pkiPriv: String?,
        val forceUsePaymailPki: Boolean,
        val customPkiWifPrivate: String,
        val type: ConnectionType
    )
}