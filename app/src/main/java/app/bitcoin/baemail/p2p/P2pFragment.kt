package app.bitcoin.baemail.p2p

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts.*
import androidx.compose.runtime.getValue
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import androidx.compose.ui.platform.ComposeView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.intercept.AppDeepLinkType
import app.bitcoin.baemail.core.presentation.util.PermissionStateHelper
import app.local.p2p.serviceDiscovery.ConnectionType
import com.google.android.gms.common.GoogleApiAvailability
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class P2pFragment : Fragment() {

    companion object {
        private const val REQUEST_RESOLVE_PLAY_SERVICES = 12345
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: P2pViewModel
    lateinit var manageStallViewModel: P2pManageStallViewModel

    private lateinit var requestPermissionLauncher: ActivityResultLauncher<Array<String>>

    private var runOnP2pPermissionsGranted: Runnable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactory
        )[P2pViewModel::class.java]

        manageStallViewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactory
        )[P2pManageStallViewModel::class.java]


        requestPermissionLauncher = registerForActivityResult(
            RequestMultiplePermissions()
        ) { result ->
            manageStallViewModel.onRequestP2pPermissionsResults(result)

            if (manageStallViewModel.p2pPermissionState.value.hasMissingPermissions()) {
                //todo inform the user why the action cannot proceed
                Toast.makeText(requireContext(), "missing permissions", Toast.LENGTH_SHORT).show()

            } else {
                runOnP2pPermissionsGranted?.run()
            }

            runOnP2pPermissionsGranted = null
        }

        setFragmentResultListener(LocPermissionRequestRationaleFragment.KEY_PERMISSION_REQUEST_RATIONALE) { requestKey, bundle ->
            if (bundle.getBoolean(LocPermissionRequestRationaleFragment.KEY_YES_REQUEST)) {
                //can proceed with requesting of permissions
                requestPermissionLauncher.launch(manageStallViewModel.getRequiredP2pPermissions())
            } else {
                runOnP2pPermissionsGranted = null
            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            val p2pItems: List<P2pItem> by viewModel.content.collectAsStateWithLifecycle(listOf())

            val connectionTypes = listOf(
                ConnectionType.NEARBY,
                ConnectionType.WIFI_DIRECT,
                ConnectionType.NSD,
            )

            ComposeAppTheme {
                ComposeP2pDiscover(
                    items = p2pItems,
                    connectionTypes = connectionTypes,
                    onSelectionChanged = viewModel::onChangeConnectionType,
                    onAttemptResolveConnectionIssue = {
                        if (ConnectionType.NEARBY == viewModel.getP2pConnectionType()) {
                            openPlayServicesResolveDialog()
                        } else {
                            Timber.d("click dropped... unhandled")
                        }
                    },
                    onGoToMyStall = ::navigateToManageStallFragment,
                    onStallStartToggled = ::onStartStallClicked,
                    onStartDiscoveryToggled = ::onStartDiscoveryClicked,
                    onBrowseClicked = viewModel::onClickedBrowseStall,
                    onDisconnectClicked = viewModel::onClickedDisconnectFromStall,
                    onButtonClicked = viewModel::runNodeScript
                )
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                manageStallViewModel.p2pPermissionState.collect { permissionState ->
                    if (permissionState.mustCheckLocPermission) {
                        handleMustCheckLocPermission()
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                val supported = listOf(
                    AppDeepLinkType.P2P_STALL_MANAGE
                )
                viewModel.deepLinkHelper.unsatisfiedLink.collectLatest { link ->
                    link ?: return@collectLatest

                    if (!supported.contains(link.type))return@collectLatest

                    when (link.type) {
                        AppDeepLinkType.P2P_STALL_MANAGE -> {
                            navigateToManageStallFragment()

                            viewModel.deepLinkHelper.informLinkNavigationSatisfied() //todo move & execute in the destination fragment

                        }
                        else -> {
                            //do nothing
                        }
                    }

                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                manageStallViewModel.playStateHolder.collect { state ->
                    if (state.mustCheckAvailability) {
                        checkGooglePlayServicesAvailability()
                    }
                }
            }
        }

        //observe p2p-events & display them
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.p2pRepository.informP2pChangeHelper.events.receiveAsFlow().collect { model ->
                    Toast.makeText(requireContext(), "Event: ${model.code}", Toast.LENGTH_SHORT).show()
                    ///todo
                    ///todo
                    ///todo
                    ///todo
                    ///todo
                    ///todo

//                //Timber.d("receiving model: $model")
//
//                utilViewModel.onChangeSnackBarSpaceReservation(true)
//                delay(150)
//                try {
//                    TwetchSnackbarHelper.displaySnackbar(model, errorSnackbarContainer)
//                    delay(150)
//
//                } finally {
//                    utilViewModel.onChangeSnackBarSpaceReservation(false)
//                }


                }
            }
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (REQUEST_RESOLVE_PLAY_SERVICES == requestCode) {
            Timber.d("user returned from resolving play-services issues; refresh availability-state")
            checkGooglePlayServicesAvailability()
        }
    }

    private fun checkGooglePlayServicesAvailability() {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val status = googleApiAvailability.isGooglePlayServicesAvailable(requireContext())
        manageStallViewModel.onPlayServicesAvailabilityChanged(
            PermissionStateHelper.PlayServicesAvailability(status)
        )
    }

    private fun openPlayServicesResolveDialog() {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        googleApiAvailability.getErrorDialog(
            requireActivity(),
            manageStallViewModel.playStateHolder.value.availability.statusCode,
            REQUEST_RESOLVE_PLAY_SERVICES
        )?.show()
        manageStallViewModel.resetPlayServicesAvailabilityChecking()
    }


    private fun navigateToManageStallFragment() {
        findNavController().navigate(R.id.action_p2pFragment_to_p2pManageStallFragment)
    }

    private fun handleMustCheckLocPermission() {
        val permissionsToCheck = manageStallViewModel.getRequiredP2pPermissions()

        val hasPermissionResults = mutableMapOf<String, Boolean>()
        permissionsToCheck.forEach { permission ->
            hasPermissionResults[permission] = PackageManager.PERMISSION_GRANTED ==
                    ContextCompat.checkSelfPermission(
                        requireContext(),
                        permission
                    )
        }

        val requirePermissionRationaleResults = mutableMapOf<String, Boolean>()
        hasPermissionResults.forEach { permissionResultEntry ->
            if (permissionResultEntry.value) return@forEach

            requirePermissionRationaleResults[permissionResultEntry.key] =
                shouldShowRequestPermissionRationale(permissionResultEntry.key)
        }

        manageStallViewModel.onP2pPermissionCheckResults(
            hasPermissionResults,
            requirePermissionRationaleResults
        )
    }

    private fun onStartStallClicked() {
        val onP2pPermissionsGranted = Runnable {
            viewModel.onClickedServiceRegistryToggle()
        }

        val p2pPermissionState = manageStallViewModel.p2pPermissionState.value
        if (p2pPermissionState.mustCheckLocPermission) return
        if (p2pPermissionState.hasMissingPermissions()) {
            runOnP2pPermissionsGranted = onP2pPermissionsGranted
            doRequestP2pPermissions()
            return
        }

        onP2pPermissionsGranted.run()
    }

    private fun onStartDiscoveryClicked() {
        val onP2pPermissionsGranted = Runnable {
            viewModel.onClickedDiscoveryToggle()
        }

        val p2pPermissionState = manageStallViewModel.p2pPermissionState.value
        if (p2pPermissionState.mustCheckLocPermission) return
        if (p2pPermissionState.hasMissingPermissions()) {
            runOnP2pPermissionsGranted = onP2pPermissionsGranted
            doRequestP2pPermissions()
            return
        }

        onP2pPermissionsGranted.run()
    }

    private fun doRequestP2pPermissions() {
        val p2pPermissionState = manageStallViewModel.p2pPermissionState.value

        val permissionsShouldShowRequestRationale =
            p2pPermissionState.shouldShowRequestPermissionRationaleResults.let {
                if (it == null) throw RuntimeException("unexpected usage")
                it.mapNotNull { permissionEntry ->
                    if (!permissionEntry.value) return@mapNotNull null
                    permissionEntry.key
                }
            }

        if(permissionsShouldShowRequestRationale.isNotEmpty()) {
            //todo consider passing permissionsShouldShowRequestRationale to the destination-fragment
            Timber.d("...should show rational for the following permissions: $permissionsShouldShowRequestRationale")

            findNavController().navigate(
                R.id.action_p2pFragment_to_locPermissionRequestRationaleFragment
            )
            return
        }

        requestPermissionLauncher.launch(manageStallViewModel.getRequiredP2pPermissions())
    }

}