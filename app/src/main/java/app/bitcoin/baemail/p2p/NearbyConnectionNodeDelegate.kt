package app.bitcoin.baemail.p2p

import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.data.nodejs.request.InformNearbyP2pConnectionChanged
import app.local.p2p.serviceDiscovery.P2pStallRegistryHelper4
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.IOException
import java.io.InputStream
import java.io.PrintWriter
import java.net.HttpURLConnection
import java.net.URL
import kotlin.coroutines.coroutineContext

class NearbyConnectionNodeDelegate(
    private val scope: CoroutineScope,
    private val nodeRuntimeRepository: NodeRuntimeRepository
) {

    private var serverPort: Int = -1

    fun updateSendingMeta(port: Int) {
        serverPort = port
    }

    fun onNewConnection(endpointId: String, endpointName: String, isOutgoing: Boolean): Unit {
        val millisStarted = P2pStallRegistryHelper4.RegisteredNameHelper
            .extractStartMillis(endpointName)
        val paymail = P2pStallRegistryHelper4.RegisteredNameHelper
            .extractPaymail(endpointName)
        val pki = P2pStallRegistryHelper4.RegisteredNameHelper
            .extractPki(endpointName)
        val title = P2pStallRegistryHelper4.RegisteredNameHelper
            .extractTitle(endpointName)

        Timber.d(".....pki: $pki; endpointName:$endpointName")

        scope.launch {
            nodeRuntimeRepository.informNearbyP2pConnectionChange(
                true,
                isOutgoing,
                endpointId,
                InformNearbyP2pConnectionChanged.EndpointMeta(
                    endpointName,
                    millisStarted,
                    paymail,
                    pki,
                    title
                )
            )
        }
    }

    fun onLostConnection(endpointId: String, isOutgoing: Boolean) {
        scope.launch {
            nodeRuntimeRepository.informNearbyP2pConnectionChange(
                false,
                isOutgoing,
                endpointId,
                null
            )
        }
    }

    fun getStreamDataInputToSend(event: P2pStallRegistryHelper4.SendEvent): InputStream {
        Timber.d("wired up input-stream for message-send-request; endpointId:${event.endpointId}; token:${event.token}")
        val url = URL("http://localhost:${serverPort}/messagePayload?token=${event.token}")
        with(url.openConnection() as HttpURLConnection) {
            requestMethod = "GET"  // optional default is GET
            return inputStream
        }
    }

    suspend fun delegateIncomingMessageHandling(holder: P2pStallRegistryHelper4.StreamResponseHolder) {
        Timber.d("streaming the message to node-side; meta: ${holder.meta}")

        val url = URL("http://localhost:${serverPort}/messageRedirect?endpoint=${holder.meta.endpoint.id}&token=${holder.meta.token}&salt=${holder.meta.salt}")

        val didFail = { -1 == holder.getStreamStatus() }
        val didSucceed = { 1 == holder.getStreamStatus() }

        if (didFail()) {
            throw Exception()
        }

        val connection = with(url.openConnection() as HttpURLConnection) {
            requestMethod = "POST"
            doOutput = true
            doInput = false
            setRequestProperty("content-type", "application/octet-stream")
            this
        }

        var _writer: PrintWriter? = PrintWriter(connection.outputStream)

        val BUFFER_SIZE = 2048

        try {
            val buffer = ByteArray(BUFFER_SIZE)
            var bytesRead: Int
            var totalTransferred = 0
            val writer = _writer!!

            outerLoop@ while (true) {
                bytesRead = holder.stream.read(buffer)

                if (bytesRead == 0) {
                    //Timber.d("..................... 0 bytes read")
                    innerLoop@ while(coroutineContext.isActive) {
                        if (didSucceed()) break@innerLoop
                        if (didFail()) {
                            //todo
                            throw Exception()
                        }

                        if (0 <= holder.stream.available()) {
                            //Timber.d("....sleeping")
                            delay(50)
                        } else {
                            break@innerLoop
                        }
                    }

                } else if (bytesRead == -1) {
                    //Timber.d("end of stream; payloadId:${holder.streamPayloadId}")
                    //if (didSucceed()) break@outerLoop
                    break@outerLoop

                } else {
                    writer.write(String(buffer, 0, bytesRead))
                    totalTransferred += bytesRead

                }
            }

            writer.flush()
            writer.close()
            _writer = null

            Timber.d("streaming of large-message to node has completed; payloadId:${holder.streamPayloadId}; transferred:$totalTransferred")

            if (connection.responseCode != HttpURLConnection.HTTP_OK) {
                throw Exception("response-code:${connection.responseCode}")
            }

        } finally {
            holder.stream.close()
            _writer?.close()
        }
    }

    fun delegateIncomingByteMessageHandling(message: P2pStallRegistryHelper4.IncomingByteMessage) {
        Timber.d("streaming the short-message-payload to node-side; endpointId: ${message.endpointId}; token: ${message.token}")

        val url = URL("http://localhost:${serverPort}/shortMessageRedirect?endpoint=${message.endpointId}&token=${message.token}&salt=${message.salt}")

        val connection = with(url.openConnection() as HttpURLConnection) {
            requestMethod = "POST"
            doInput = false
            doOutput = true
            setRequestProperty("Content-Type", "application/octet-stream")
            this
        }

        val BUFFER_SIZE = 2048
        val inputStream = message.data.byteInputStream()

        var _writer: PrintWriter? = PrintWriter(connection.outputStream)

        try {
            val buffer = ByteArray(BUFFER_SIZE)
            var bytesRead: Int
            var totalTransferred = 0
            val writer = _writer!!

            outerLoop@ while (
                true
            ) {
                bytesRead = inputStream.read(buffer)

                if (bytesRead == 0) {
                    //Timber.d("..................... 0 bytes read")

                } else if (bytesRead == -1) {
                    //Timber.d("..................... end of stream?")
                    break@outerLoop

                } else {
                    writer.write(String(buffer, 0, bytesRead))
                    totalTransferred += bytesRead
                }
            }

            writer.flush()
            writer.close()
            _writer = null

            Timber.d("streaming of short-message to node has completed; token:${message.token}; transferred:$totalTransferred")

            if (connection.responseCode != HttpURLConnection.HTTP_OK) {
                throw IOException("unexpected response-code:${connection.responseCode}")
            }

        } finally {
            inputStream.close()
            _writer?.close()
        }
    }

    fun delegateIncomingFileRequestHandling(event: P2pStallRegistryHelper4.FileReceiveEvent): P2pStallRegistryHelper4.FileResponseHolder {
        Timber.d("streaming file-request to node-side; token:${event.token}")

        val url = URL("http://localhost:${serverPort}/fileRequest?endpoint=${event.endpointId}&salt=${event.salt}")

        val connection = with(url.openConnection() as HttpURLConnection) {
            requestMethod = "POST"
            doOutput = true
            doInput = true

            setRequestProperty("content-type", "application/octet-stream")

            this
        }

        val BUFFER_SIZE = 2048
        val inputStream = event.data.byteInputStream()

        var _writer: PrintWriter? = PrintWriter(connection.outputStream)

        try {
            val buffer = ByteArray(BUFFER_SIZE)
            var bytesRead: Int
            var totalTransferred = 0
            val writer = _writer!!

            while (true) {
                bytesRead = inputStream.read(buffer)

                if (bytesRead == 0) {
                    //Timber.d("..................... 0 bytes read")

                } else if (bytesRead == -1) {
                    //Timber.d("..................... end of stream?")
                    break

                } else {
                    writer.write(String(buffer, 0, bytesRead))
                    totalTransferred += bytesRead

                }
            }

            writer.flush()
            writer.close()
            _writer = null

            Timber.d("streaming to node completed; token:${event.token}; transferred:$totalTransferred")

            if (connection.responseCode != HttpURLConnection.HTTP_OK) {
                throw IOException("unexpected response-code:${connection.responseCode}")
            }

            //pass the node-response back to nearby-connections-lib
            val saltOfMeta = connection.getHeaderField("salt-of-meta")
            val saltOfStream = connection.getHeaderField("salt-of-stream")
            val meta = connection.getHeaderField("meta")
            //Timber.d("file-request got response saltOfMeta:$saltOfMeta saltOfStream:$saltOfStream")

            return P2pStallRegistryHelper4.FileResponseHolder(
                saltOfMeta,
                saltOfStream,
                meta,
                connection.inputStream
            )

        } finally {
            inputStream.close()
            _writer?.close()
        }
    }

    suspend fun delegateIncomingFileResponseHandling(holder: P2pStallRegistryHelper4.FileResponseRedirectingHolder) {
        Timber.d("streaming the file-message to node-side; token: ${holder.token}")

        val url = URL("http://localhost:${serverPort}/fileRedirect?endpoint=${holder.endpointId}&token=${holder.token}")

        val didFail = { -1 == holder.getStreamStatus() }
        val didSucceed = { 1 == holder.getStreamStatus() }

        if (didFail()) {
            throw Exception()
        }

        val connection = with(url.openConnection() as HttpURLConnection) {
            requestMethod = "POST"
            doOutput = true
            doInput = false

            setRequestProperty("content-type", "application/octet-stream")
            setRequestProperty("salt-of-stream", holder.saltOfStream)
            setRequestProperty("salt-of-meta", holder.saltOfMeta)
            setRequestProperty("meta", holder.meta)

            this
        }

        var _writer: PrintWriter? = PrintWriter(connection.outputStream)

        val BUFFER_SIZE = 2048

        try {
            val buffer = ByteArray(BUFFER_SIZE)
            var bytesRead: Int
            var totalTransferred = 0
            val writer = _writer!!

            outerLoop@ while (
                true
            ) {
                bytesRead = holder.stream.read(buffer)

                if (bytesRead == 0) {
                    //Timber.d("..................... 0 bytes read")
                    innerLoop@ while(coroutineContext.isActive) {
                        if (didSucceed()) break@innerLoop
                        if (didFail()) {
                            //todo
                            throw Exception()
                        }

                        if (0 <= holder.stream.available()) {
                            Timber.d("....sleeping")
                            delay(50)
                        } else {
                            break@innerLoop
                        }
                    }

                } else if (bytesRead == -1) {
                    break@outerLoop

                } else {
                    writer.write(String(buffer, 0, bytesRead))
                    totalTransferred += bytesRead

                }
            }

            writer.flush()
            writer.close()
            _writer = null
            holder.stream.close()

            Timber.d("streaming completed; token:${holder.token}; transferred:$totalTransferred")
            //todo cleanup the maps


            if (connection.responseCode != HttpURLConnection.HTTP_OK) {
                throw Exception("response-code:${connection.responseCode}")
            }

        } finally {
            holder.stream.close()
            _writer?.close()
        }
    }

    fun delegateIncomingErrorResponseHandling(holder: P2pStallRegistryHelper4.ErrorResponseHolder) {
        Timber.d("sending the error-response to node-side; token: ${holder.token}")

        val url = URL("http://localhost:${serverPort}/errorResponse?endpoint=${holder.endpoint}&token=${holder.token}&type=${holder.type}")

        val connection = with(url.openConnection() as HttpURLConnection) {
            requestMethod = "GET"
            this
        }

        try {
            if (connection.responseCode != HttpURLConnection.HTTP_OK) {
                throw Exception("response-code:${connection.responseCode}")
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

}