package app.bitcoin.baemail.p2p

import app.local.p2p.serviceDiscovery.P2pStallRegistryHelper4
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber

class InformP2pChangeHelper {

    private val _stallSessionState = MutableStateFlow<StallSessionChangeEvent?>(null)
    val stallSessionState: StateFlow<StallSessionChangeEvent?>
        get() = _stallSessionState


    private val _events = Channel<P2pEvent>()
    val events: ReceiveChannel<P2pEvent>
        get() = _events


    private val _nearbyMeta = Channel<P2pNearbyMeta>()
    val nearbyMeta: ReceiveChannel<P2pNearbyMeta>
        get() = _nearbyMeta

    private val _sendRequest = Channel<P2pStallRegistryHelper4.SendEvent>(Channel.UNLIMITED)
    val sendRequest: ReceiveChannel<P2pStallRegistryHelper4.SendEvent>
        get() = _sendRequest

    private val _disconnectRequest = Channel<String>(Channel.UNLIMITED)
    val disconnectRequest: ReceiveChannel<String>
        get() = _disconnectRequest


    suspend fun postEvent(event: P2pEvent) {
        _events.send(event)
    }

    fun postNearbyMeta(model: P2pNearbyMeta) {
        Timber.d("model: $model")
        _nearbyMeta.trySend(model)
    }

    fun postSendRequest(model: P2pStallRegistryHelper4.SendEvent) {
        _sendRequest.trySend(model)
    }

    fun postDisconnectRequest(endpointId: String) {
        _disconnectRequest.trySend(endpointId)
    }

    fun informStallSessionChange(
        activeSessionsJson: JsonArray,
        sessionLayoutsJson: JsonArray
    ) {
        val activeSessionList = mutableListOf<StallSessionId>()
        activeSessionsJson.mapTo(activeSessionList) { json ->
            json.asJsonObject.let { jsonStall ->
                StallSessionId(
                    jsonStall.get("name").asString,
                    jsonStall.get("paymail").asString,
                    jsonStall.get("pki").asString
                )

            }
        }

        val layoutMap= mutableMapOf<StallSessionId, JsonObject>()
        sessionLayoutsJson.forEach { jsonElement ->
            val json = jsonElement.asJsonObject
            val sessionId = StallSessionId(
                json.get("name").asString,
                json.get("paymail").asString,
                json.get("pki").asString
            )

            val layoutJson = json.getAsJsonObject("layout")

            layoutMap[sessionId] = layoutJson
        }

        _stallSessionState.value = StallSessionChangeEvent(
            activeSessionList,
            layoutMap
        )

    }


    data class StallSessionId(
        val name: String,
        val paymail: String,
        val pki: String
    )

    data class StallSessionChangeEvent(
        val activeSessions: List<StallSessionId>,
        val sessionLayouts: Map<StallSessionId, JsonObject>
    )



    data class P2pEvent(
        val code: String
    )


    data class P2pNearbyMeta(
        val port: Int
    )


}