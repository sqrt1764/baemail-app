package app.bitcoin.baemail.logs.presentation

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.isDarkModeOn
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class LogsActivity: AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>


    private lateinit var statusBarBg: View
    private lateinit var navigationBarBg: View


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE)

        if (!resources.isDarkModeOn() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        if (!resources.isDarkModeOn() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }

        setContentView(R.layout.activity_logs)

        statusBarBg = findViewById(R.id.status_bar_bg)
        navigationBarBg = findViewById(R.id.navigation_bar_bg)


        statusBarBg.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.layoutParams.let {
                it.height = windowInsets.systemWindowInsetTop.let { top ->
                    if (top == 0) {
                        view.visibility = View.GONE
                        1
                    } else {
                        view.visibility = View.VISIBLE
                        top
                    }
                }

                view.layoutParams = it
            }

            windowInsets
        }
        navigationBarBg.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.layoutParams.let {
                it.height = windowInsets.systemWindowInsetBottom.let { bottom ->
                    if (bottom == 0) {
                        view.visibility = View.GONE
                        1
                    } else {
                        view.visibility = View.VISIBLE
                        bottom
                    }
                }

                view.layoutParams = it
            }

            windowInsets
        }

    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }




}