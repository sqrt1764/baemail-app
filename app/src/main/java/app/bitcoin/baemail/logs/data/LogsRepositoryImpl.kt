package app.bitcoin.baemail.logs.data

import android.content.Context
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.data.util.FileLoggingTree
import app.bitcoin.baemail.logs.domain.LogsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.File
import javax.inject.Inject

class LogsRepositoryImpl @Inject constructor(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil
): LogsRepository {

    override suspend fun deleteAllLogs() {
        FileLoggingTree.deleteAllLogs(appContext, coroutineUtil)
    }

    override fun getLogFiles(): Flow<List<File>> = flow {
        val logsDir = File(appContext.cacheDir, "logging")
        if (!logsDir.exists()) {
            emit(listOf())
            return@flow
        }

        val foundLogs = logsDir.listFiles { dir, name ->
            name.endsWith(".html")
        } ?: let {
            emit(listOf())
            return@flow
        }

        if (foundLogs.isEmpty()) {
            emit(listOf())
            return@flow
        }

        emit(foundLogs.sortedBy { it.lastModified() })
    }

}