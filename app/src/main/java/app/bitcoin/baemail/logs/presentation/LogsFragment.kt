package app.bitcoin.baemail.logs.presentation

import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowInsets
import android.webkit.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.webkit.WebViewFeature
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.data.util.DispatcherBoundInputStream
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.isDarkModeOn
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.io.InputStream
import java.io.SequenceInputStream
import java.util.*
import javax.inject.Inject

class LogsFragment : Fragment(R.layout.fragment_logs) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var coroutineUtil: CoroutineUtil

    private lateinit var viewModel: LogsViewModel

    private lateinit var container: ConstraintLayout
    private lateinit var webView: WebView
    private lateinit var dismissFab: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_logs)

        viewModel = ViewModelProvider(
            viewModelStoreOwner,
            viewModelFactory
        )[LogsViewModel::class.java]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        container = requireView().findViewById(R.id.container)
        webView = requireView().findViewById(R.id.webview)
        dismissFab = requireView().findViewById(R.id.dismiss)

        container.doOnApplyWindowInsets { v, windowInsets, _ ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                v.updatePadding(
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )

            } else {
                v.updatePadding(
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight,
                )
            }

            windowInsets
        }

        dismissFab.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        val settings = webView.settings
        settings.javaScriptEnabled = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && resources.isDarkModeOn()) {
            if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK_STRATEGY)) {
                webView.parent.let {
                    it as View
                    it.isForceDarkAllowed = true
                }
                settings.forceDark = WebSettings.FORCE_DARK_ON
            }
        }

        webView.webViewClient = WebClient()


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.foundLogs.collectLatest {
                    if (it.isEmpty()) return@collectLatest

                    //get webview to load something
                    webView.loadUrl("https://www.digitalBrick.lol")
                }
            }
        }

    }


    inner class WebClient : WebViewClient() {

        override fun shouldInterceptRequest(
            view: WebView?,
            request: WebResourceRequest?
        ): WebResourceResponse {

            val contentStream = viewModel.foundLogs.value.let {
                if (it.isEmpty()) {
                    return@let  """
                        <div>No logs found</div>
                    """.trimIndent().byteInputStream()
                }

                DispatcherBoundInputStream(
                    it.last().inputStream(),
                    coroutineUtil.fileLoggingDispatcher
                )

            }

            val pre = """
                <!DOCTYPE html>
                <html>
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
                    <meta name="color-scheme" content="dark light">
                    <style>
                    
                        :root {
                            color-scheme: dark light;
                            word-break: break-all;
                            overflow-wrap: anywhere;
                            hyphens: none;
                            padding-left: 1em;
                            padding-right: 1em;
                            scroll-behavior: smooth;
                        }
                    
                        body {
                            font-family: monospace;
                            margin: 0px;
                            padding: 0px;
                            padding-top: 3em;
                        }
                        
                        .log {
                            padding-bottom: 2.5em;
                        }
                        
                        .timestamp {
                            display: inline-block;
                            background: rgba(0, 255, 255, 0.2);
                            font-size: 80%;
                            padding-top: 0.2em;
                            padding-bottom: 0.2em;
                            padding-right: 6em;
                        }
                        
                        .tag {
                            padding-top: 0.1em;
                            padding-bottom: 0.5em;
                            font-size: 80%;
                        }
                        
                        .message {
                            overflow: auto;
                        }
                        
                        #gapElement {
                            height: 20em;
                        }
                        
                        #bottomElement {
                            height: 1em;
                        }
                                                                
                                               
                        @media (prefers-color-scheme: dark) {
                            body {
                                background: rgba(0, 0, 0, 1);
                                color: rgba(240, 240, 240, 1);
                            }
                        }
                        
                    </style>
                    <script>
                        var scrollDownIntervalToken = setInterval(function() {
                            var body = document.body;
                            var html = document.documentElement;

                            var height = Math.max(
                                body.scrollHeight,
                                body.offsetHeight, 
                                html.clientHeight,
                                html.scrollHeight,
                                html.offsetHeight
                            );
                            document.scrollingElement.scrollBy(0, height);
                            
                        }, 500);
                    </script>
                </head>
                <body>
            """.trimIndent().byteInputStream()

            val post = """
                <div id="gapElement"></div>
                <div id="bottomElement"></div>
                </body>
                <script>
                    setTimeout(function() {
                        clearInterval(scrollDownIntervalToken);
                    }, 600);
                    
                </script>
                </html>
            """.trimIndent().byteInputStream()

            val streamsVector = Vector<InputStream>()
            streamsVector.add(pre)
            streamsVector.add(contentStream)
            streamsVector.add(post)

            val finalStream = SequenceInputStream(streamsVector.elements())

            return WebResourceResponse(
                "text/html",
                "utf-8",
                finalStream
            )
        }

        override fun onPageFinished(view: WebView?, url: String?) {
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        }
    }

}