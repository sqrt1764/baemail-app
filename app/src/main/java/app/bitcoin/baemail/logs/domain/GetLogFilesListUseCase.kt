package app.bitcoin.baemail.logs.domain

import kotlinx.coroutines.flow.Flow
import java.io.File

class GetLogFilesListUseCaseImpl(
    private val logsRepository: LogsRepository
): GetLogFilesListUseCase {
    override fun invoke(): Flow<List<File>> = logsRepository.getLogFiles()

}

interface GetLogFilesListUseCase {
    operator fun invoke(): Flow<List<File>>
}