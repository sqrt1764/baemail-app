package app.bitcoin.baemail.logs.domain

import kotlinx.coroutines.flow.Flow
import java.io.File


interface LogsRepository {
    suspend fun deleteAllLogs()
    fun getLogFiles(): Flow<List<File>>
}