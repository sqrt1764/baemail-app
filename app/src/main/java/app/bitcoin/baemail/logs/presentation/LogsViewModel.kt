package app.bitcoin.baemail.logs.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.bitcoin.baemail.logs.domain.GetLogFilesListUseCase
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import java.io.File
import javax.inject.Inject

class LogsViewModel @Inject constructor(
    getLogFilesListUseCase: GetLogFilesListUseCase
) : ViewModel() {

    val foundLogs: StateFlow<List<File>> = getLogFilesListUseCase()
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = listOf()
        )

}