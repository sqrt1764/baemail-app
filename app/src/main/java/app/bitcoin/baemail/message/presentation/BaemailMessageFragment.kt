package app.bitcoin.baemail.message.presentation

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.view.doOnPreDraw
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.CircleOutlineProvider
import app.bitcoin.baemail.core.presentation.drawable.DrawableCircleProgress
import app.bitcoin.baemail.core.presentation.drawable.DrawableOval
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.message.data.entity.MessageBucket
import app.bitcoin.baemail.core.presentation.util.NoSwipeBehavior
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ErrorSnackBar
import app.bitcoin.baemail.core.presentation.view.NontouchableCoordinatorLayout
import app.bitcoin.baemail.message.presentation.entity.BaemailMessageUiEvent
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class BaemailMessageFragment : Fragment(R.layout.fragment_baemail_message)  {

    companion object {
        const val KEY_TX_ID = "tx_id"
        const val KEY_SCROLL_POS = "scroll_pos"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: BaemailMessageViewModel


    private lateinit var container: NontouchableCoordinatorLayout
    private lateinit var contentContainer: ConstraintLayout

    private lateinit var close: ImageView
    private lateinit var reply: Button
    private lateinit var archive: Button
    private lateinit var unarchive: Button
    private lateinit var delete: Button


    private lateinit var valueFrom: TextView
    private lateinit var valueTo: TextView
    private lateinit var valueSubject: TextView
    private lateinit var valueContent: TextView
    private lateinit var valueDate: TextView
    private lateinit var valueAmount: TextView
    private lateinit var valueTxId: TextView

    private lateinit var labelTitle: TextView

    private lateinit var labelReplyTo: TextView
    private lateinit var valueReplyTo: TextView


    private lateinit var appBar: AppBarLayout
    private lateinit var scroller: NestedScrollView

    private lateinit var loadingIndicator: ImageView

    private lateinit var headerContainer: ConstraintLayout
    private lateinit var footerContainer: ConstraintLayout

    private lateinit var bottomGap: View


    private lateinit var loadingDrawable: DrawableCircleProgress


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val txId = requireArguments().getString(KEY_TX_ID)!!

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            this,
            viewModelFactory
        )[BaemailMessageViewModel::class.java]

        viewModel.init(txId)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        container = view.findViewById(R.id.container)
        contentContainer = view.findViewById(R.id.content_container)

        appBar = view.findViewById(R.id.app_bar)
        scroller = view.findViewById(R.id.scroller)

        loadingIndicator = view.findViewById(R.id.loading_indicator)

        close = view.findViewById(R.id.close)

        valueFrom = view.findViewById(R.id.value_from)
        valueTo = view.findViewById(R.id.value_to)
        valueSubject = view.findViewById(R.id.value_subject)
        valueContent = view.findViewById(R.id.value_content)
        valueDate = view.findViewById(R.id.value_date)
        valueAmount = view.findViewById(R.id.value_amount)
        valueTxId = view.findViewById(R.id.value_tx_id)

        labelTitle = view.findViewById(R.id.title)

        headerContainer = view.findViewById(R.id.header_container)
        footerContainer = view.findViewById(R.id.footer_container)

        labelReplyTo = view.findViewById(R.id.label_reply_to)
        valueReplyTo = view.findViewById(R.id.value_reply_to)

        reply = view.findViewById(R.id.reply)
        archive = view.findViewById(R.id.archive)
        unarchive = view.findViewById(R.id.unarchive)
        delete = view.findViewById(R.id.delete)

        bottomGap = view.findViewById(R.id.bottom_gap)


        val topBarElevation = resources.getDimension(
            R.dimen.added_paymails_fragment__top_bar_elevation
        )


        appBar.doOnPreDraw {
            appBar.elevation = 0f
        }

        val ovalStrokeWidth = resources
            .getDimension(R.dimen.button_bar_item__oval_stroke_width)

        val softPrimary = requireContext().getColorFromAttr(R.attr.colorPrimary).let {
            ColorUtils.setAlphaComponent(it, 50)
        }

        headerContainer.background = DrawableOval(requireContext()).also {
            it.setBgColor(Color.TRANSPARENT)
            it.setStrokeWidth(ovalStrokeWidth)
            it.setStrokeColor(softPrimary)
            it.setCornerRadius(TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                12f,
                resources.displayMetrics
            ))
            it.initPaints()
        }

        footerContainer.background = DrawableOval(requireContext()).also {
            it.setBgColor(Color.TRANSPARENT)
            it.setStrokeWidth(ovalStrokeWidth)
            it.setStrokeColor(softPrimary)
            it.setCornerRadius(TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                12f,
                resources.displayMetrics
            ))
            it.initPaints()
        }


        scroller.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener {
                _, _, scrollY, _, _ ->
            if (scrollY < 8 * resources.displayMetrics.density) {
                appBar.elevation = 0f
            } else {
                appBar.elevation = topBarElevation
            }
        })

        scroller.overScrollMode = View.OVER_SCROLL_NEVER



        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        let {
            close.foreground = DrawableState.getNew(colorSelector)

            close.setOnClickListener {
                findNavController().popBackStack()
            }

            close.clipToOutline = true
            close.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    view ?: return
                    outline ?: return

                    outline.setOval(0, 0, view.width, view.height)
                }
            }
        }


        valueFrom.setOnClickListener {
            copyFieldValueToClipBoard(
                getString(R.string.field_from),
                valueFrom.text.toString()
            )
        }

        valueTo.setOnClickListener {
            copyFieldValueToClipBoard(
                getString(R.string.field_to),
                valueTo.text.toString()
            )
        }

        valueSubject.setOnClickListener {
            copyFieldValueToClipBoard(
                getString(R.string.field_subject),
                valueSubject.text.toString()
            )
        }

        valueDate.setOnClickListener {
            copyFieldValueToClipBoard(
                getString(R.string.field_date),
                valueDate.text.toString()
            )
        }

        valueAmount.setOnClickListener {
            copyFieldValueToClipBoard(
                getString(R.string.field_amount),
                valueAmount.text.toString()
            )
        }

        valueTxId.setOnClickListener {
            copyFieldValueToClipBoard(
                getString(R.string.field_tx_hash),
                valueTxId.text.toString()
            )
        }

        loadingDrawable = DrawableCircleProgress(requireContext()).also {
            it.setStyle(CircularProgressDrawable.LARGE)
            it.setColorSchemeColors(requireContext().getColorFromAttr(R.attr.colorOnBackground))
        }
        loadingIndicator.setImageDrawable(loadingDrawable)
        loadingIndicator.background =
            ColorDrawable(requireContext().getColorFromAttr(R.attr.colorSurface))
        loadingIndicator.clipToOutline = true
        loadingIndicator.outlineProvider = CircleOutlineProvider()


        valueReplyTo.background = DrawableState.getNew(colorSelector)
        valueReplyTo.clipToOutline = true
        valueReplyTo.outlineProvider = object : ViewOutlineProvider() {

            val paddingPx = (8 * resources.displayMetrics.density).toInt()
            val radius = 8 * resources.displayMetrics.density

            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                outline.setRoundRect(
                    paddingPx,
                    paddingPx,
                    view.width - paddingPx,
                    view.height - paddingPx,
                    radius
                )
            }

        }
        valueReplyTo.setOnClickListener {
            val replyToTxId = viewModel.state.value?.message?.replyToTxId
                ?: return@setOnClickListener

            val args = Bundle().also {
                it.putString(KEY_TX_ID, replyToTxId)
            }

            findNavController().navigate(R.id.action_baemailMessageFragment_self, args)
        }


        reply.setOnClickListener {
            val txId = viewModel.state.value?.message?.txId ?: let {
                Timber.d("txId not found")
                return@setOnClickListener
            }

            val model = viewModel.state.value ?: let {
                Timber.d("message not found")
                return@setOnClickListener
            }

            val destinationPaymail = if (model.message.to == model.ownerPaymail) {
                model.message.from
            } else {
                model.message.to
            }

            val args = Bundle().also {
                it.putBoolean(NewBaemailFragment.KEY_IS_NEW_REPLY, true)
                it.putString(NewBaemailFragment.KEY_THREAD, txId)
                it.putString(NewBaemailFragment.KEY_DESTINATION_PAYMAIL, destinationPaymail)
                it.putString(NewBaemailFragment.KEY_SUBJECT, model.message.subject)
                it.putLong(NewBaemailFragment.KEY_MILLIS_CREATED, System.currentTimeMillis())
            }

            findNavController().navigate(
                R.id.action_baemailMessageFragment_to_newMessageFragment,
                args
            )
        }

        archive.setOnClickListener {
            viewModel.archiveMessage()
        }

        unarchive.setOnClickListener {
            viewModel.unarchiveMessage()
        }

        delete.setOnClickListener {
            viewModel.deleteMessage()
        }


        viewLifecycleOwner.lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onStop(owner: LifecycleOwner) {
                loadingIndicator.visibility = View.GONE
                loadingDrawable.stop()
                container.setTouchBroken(false)
            }
        })

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.eventFlow.collectLatest { event ->
                    when(event) {
                        is BaemailMessageUiEvent.ShowSnackbar -> {
                            loadingIndicator.visibility = View.GONE
                            loadingDrawable.stop()
                            container.setTouchBroken(false)

                            ErrorSnackBar.make(
                                requireContext(),
                                container,
                                getString(event.message)
                            ).let {
                                it.behavior = NoSwipeBehavior()
                                it.duration = Snackbar.LENGTH_SHORT
                                it.show()
                            }
                        }
                        is BaemailMessageUiEvent.Processing -> {
                            loadingIndicator.visibility = View.VISIBLE
                            loadingDrawable.start()
                            container.setTouchBroken(true)
                        }
                        is BaemailMessageUiEvent.Success -> {
                            findNavController().popBackStack()
                        }
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                var hasBeenRestored = false
                viewModel.state.collectLatest { model ->
                    model ?: return@collectLatest

                    //init the content
                    valueFrom.text = model.message.from
                    valueTo.text = model.message.to
                    valueSubject.text = model.message.subject.trim()
                    valueDate.text = getString(
                        R.string.value_date,
                        model.formattedMessageDate,
                        model.formattedMessageTime
                    )
                    valueAmount.text = getString(R.string.value_amount_usd, model.message.amountUsd)
                    valueTxId.text = model.message.txId

                    valueContent.text = model.parsedContent


                    if (model.message.replyToTxId == null) {
                        labelReplyTo.visibility = View.GONE
                        valueReplyTo.visibility = View.GONE
                    } else {
                        labelReplyTo.visibility = View.VISIBLE
                        valueReplyTo.visibility = View.VISIBLE

                        valueReplyTo.text = model.message.replyToTxId
                    }

                    when (model.message.bucket) {
                        MessageBucket.INBOX -> {
                            labelTitle.setText(R.string.messages_inbox_open_title)
                        }
                        MessageBucket.SENT -> {
                            labelTitle.setText(R.string.messages_sent_open_title)
                        }
                        MessageBucket.ARCHIVE -> {
                            labelTitle.setText(R.string.messages_archive_open_title)
                        }
                    }

                    when (model.message.bucket) {
                        MessageBucket.INBOX -> {
                            reply.visibility = View.VISIBLE
                            archive.visibility = View.VISIBLE
                            unarchive.visibility = View.GONE
                            delete.visibility = View.GONE
                        }
                        MessageBucket.SENT -> {
                            reply.visibility = View.VISIBLE
                            archive.visibility = View.GONE
                            unarchive.visibility = View.GONE
                            delete.visibility = View.VISIBLE
                        }
                        MessageBucket.ARCHIVE -> {
                            reply.visibility = View.VISIBLE
                            archive.visibility = View.GONE
                            unarchive.visibility = View.VISIBLE
                            delete.visibility = View.VISIBLE
                        }
                    }


                    if (!hasBeenRestored) {
                        hasBeenRestored = true
                        savedInstanceState?.getInt(KEY_SCROLL_POS)?.let { scrollY ->
                            scroller.doOnPreDraw {
                                scroller.scrollY = scrollY
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(KEY_SCROLL_POS, scroller.scrollY)
    }

    private val cbm: ClipboardManager by lazy {
        requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }

    private fun copyFieldValueToClipBoard(field: String, value: String) {
        cbm.let { manager ->
            val clip = ClipData.newPlainText(getString(R.string.x_value, field), value)

            manager.setPrimaryClip(clip)
            ErrorSnackBar.make(
                requireContext(),
                container,
                getString(R.string.x_copied, field)
            ).let {
                it.behavior = NoSwipeBehavior()
                it.duration = Snackbar.LENGTH_SHORT
                it.show()
            }
        }
    }



}