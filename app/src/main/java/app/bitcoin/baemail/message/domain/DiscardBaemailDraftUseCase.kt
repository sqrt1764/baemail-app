package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull

class DiscardBaemailDraftUseCaseImpl(
    private val getActivePaymailUseCase: GetActivePaymailUseCase,
    private val baemailRepository: BaemailRepository
) : DiscardBaemailDraftUseCase {

    override suspend fun invoke() {
        val paymail = getActivePaymailUseCase().mapNotNull { it?.paymail }.first()
        baemailRepository.messageDraftHelper.discardDraft(paymail)
    }

}

interface DiscardBaemailDraftUseCase {
    suspend operator fun invoke()
}