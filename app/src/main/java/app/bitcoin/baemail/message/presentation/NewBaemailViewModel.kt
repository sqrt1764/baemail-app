package app.bitcoin.baemail.message.presentation

import androidx.lifecycle.*
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import app.bitcoin.baemail.core.domain.usecases.CheckPaymailExistsUseCase
import app.bitcoin.baemail.core.domain.usecases.CoinSyncStatusUseCase
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.core.domain.usecases.GetBlockStatsFeeUseCase
import app.bitcoin.baemail.core.domain.usecases.GetExchangeRateUseCase
import app.bitcoin.baemail.core.domain.usecases.GetFundingCoinsUseCase
import app.bitcoin.baemail.core.presentation.view.ViewBaePayOptions
import app.bitcoin.baemail.message.data.entity.Draft
import app.bitcoin.baemail.message.domain.ApplyNewReplyDraftUseCase
import app.bitcoin.baemail.message.domain.DiscardBaemailDraftUseCase
import app.bitcoin.baemail.message.domain.ErrorConstructingTx
import app.bitcoin.baemail.message.domain.ErrorGettingDestinationOutput
import app.bitcoin.baemail.message.domain.ErrorNotReady
import app.bitcoin.baemail.message.domain.ErrorPaymailDoesNotExist
import app.bitcoin.baemail.message.domain.EstimateBaemailMessageTxSize
import app.bitcoin.baemail.message.domain.GetSavedMessageDraftUseCase
import app.bitcoin.baemail.message.domain.SaveBaemailDraftUseCase
import app.bitcoin.baemail.message.domain.SendBaemailMessageUseCase
import app.bitcoin.baemail.message.domain.TxNotAcceptedByBaemail
import app.bitcoin.baemail.message.domain.TxNotAcceptedByMiner
import app.bitcoin.baemail.message.presentation.entity.NewBaemailUi
import app.bitcoin.baemail.message.presentation.entity.NewBaemailUiEvent
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.stateIn
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.nextUp

class NewBaemailViewModel @Inject constructor(
    private val getActivePaymailUseCase: GetActivePaymailUseCase,
    private val getFundingCoinsUseCase: GetFundingCoinsUseCase,
    private val coinSyncStatusUseCase: CoinSyncStatusUseCase,
    private val checkPaymailExistsUseCase: CheckPaymailExistsUseCase,
    private val getBlockStatsFeeUseCase: GetBlockStatsFeeUseCase,
    private val getExchangeRateUseCase: GetExchangeRateUseCase,
    private val estimateBaemailMessageTxSize: EstimateBaemailMessageTxSize,
    private val getSavedMessageDraftUseCase: GetSavedMessageDraftUseCase,
    private val saveBaemailDraftUseCase: SaveBaemailDraftUseCase,
    private val discardBaemailDraftUseCase: DiscardBaemailDraftUseCase,
    private val applyNewReplyDraftUseCase: ApplyNewReplyDraftUseCase,
    private val sendBaemailMessageUseCase: SendBaemailMessageUseCase
) : ViewModel() {

    private val defaultPayOptions = ViewBaePayOptions.Model(
        0.01f,
        0.1f,
        0.01f,
        "USD"
    )

    private val destinationPaymail = MutableStateFlow("")
    private val subject = MutableStateFlow("")
    private val content = MutableStateFlow("")
    private val thread = MutableStateFlow<String?>(null)
    private val payOptions = MutableStateFlow(defaultPayOptions)
    private val paymailExistence: Flow<PaymailExistence> = destinationPaymail
        .flatMapLatest { value ->
            checkPaymailExistsUseCase(value)
        }

    val state: StateFlow<NewBaemailUi> = thread
        .combine(destinationPaymail) { thread, destinationPaymail ->
            thread to destinationPaymail
        }.combine(subject) { (thread, destinationPaymail), subject ->
            Triple(thread, destinationPaymail, subject)
        }.combine(content) { (thread, destinationPaymail, subject), content ->
            thread to Triple(destinationPaymail, subject, content)
        }.combine(paymailExistence) { (thread, triple), paymailExistence ->
            Triple(
                triple,
                thread,
                paymailExistence
            )
        }.combine(payOptions) { (triple, thread, paymailExistence), payOptions ->
            triple to Triple(
                thread,
                paymailExistence,
                payOptions
            )
        }.combine(
            getFundingCoinsUseCase().combine(coinSyncStatusUseCase()) { coins, syncStatus ->
                coins to syncStatus
            }.combine(getBlockStatsFeeUseCase()) { (coins, syncStatus), fee ->
                Triple(coins, syncStatus, fee)
            }.combine(getExchangeRateUseCase.exchangeRate) { triple, exchangeRate ->
                val (coins, syncStatus, fee) = triple
                Pair(coins, syncStatus) to Pair(fee, exchangeRate)
            }
        ) { (triple0, triple1), other ->
            val (destinationPaymail, subject, content) = triple0
            val (thread, paymailExistence, payOptions) = triple1
            val (coins, syncStatus) = other.first
            val (fee, exchangeRate) = other.second

            val activePaymail = getActivePaymailUseCase().mapNotNull { it?.paymail }.first()

            val estimatedTxSize = estimateBaemailMessageTxSize(
                sendingPaymail = activePaymail,
                destinationPaymail = destinationPaymail,
                subject = subject,
                content = content
            )
            val estimatedFee = (estimatedTxSize * fee.satsPerByte).nextUp().toInt()
            val satsToPay = (payOptions.valueCurrent * 100 * exchangeRate.satsInACent).toLong()
            val estimatedTxOutputSats = exchangeRate.satsInACent + //amount to baemail
                    satsToPay //amount to user

            val estimatedSatsRequired = estimatedFee + estimatedTxOutputSats

            val satsAvailable = if (syncStatus == ChainSyncStatus.LIVE) {
                coins.fold(0L) { acc, coin -> acc + coin.sats }
            } else 0L

            var isMessageValid = paymailExistence is PaymailExistence.Valid
            if (subject.length < 3 || content.length < 3) {
                isMessageValid = false
            }
            if (satsAvailable < estimatedSatsRequired) {
                isMessageValid = false
            }

            NewBaemailUi(
                destinationPaymail,
                subject,
                thread,
                content,
                isMessageValid,
                paymailExistence,
                payOptions,
                satsAvailable,
                estimatedSatsRequired
            )

        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = NewBaemailUi(
                paymail = "",
                subject = "",
                replyThreadId = null,
                content = "",
                isMessageValid = false,
                paymailExistenceState = PaymailExistence.Checking(),
                payOptions = defaultPayOptions,
                satsAvailable = 0,
                satsEstimatedRequired = 1
            )
        )

    private val _eventFlow = MutableSharedFlow<NewBaemailUiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var millisLastReplyInit = 0L

    init {
        viewModelScope.launch {
            getSavedMessageDraftUseCase().collectLatest { draft ->
                restoreFromDraft(draft)
            }
        }
    }

    fun initNewReply(paymail:String, subject: String, threadId: String, millisCreated: Long) {
        if (threadId == thread.value) return
        if (millisCreated <= millisLastReplyInit) return

        viewModelScope.launch {
            thread.value = threadId
            millisLastReplyInit = millisCreated
            destinationPaymail.value = paymail
            this@NewBaemailViewModel.subject.value = subject

            applyNewReplyDraftUseCase(
                paymail,
                subject,
                threadId,
                millisCreated
            )
        }
    }

    private fun restoreFromDraft(draft: Draft) {
        if (draft.thread != null && draft.thread == thread.value) return
        if (draft.millisCreated <= millisLastReplyInit) return

        val payOptions = ViewBaePayOptions.Model(
            draft.payMax / 10,
            draft.payMax,
            draft.payAmount,
            draft.payCurrency
        )

        destinationPaymail.value = draft.destination ?: ""
        subject.value = draft.subject ?: ""
        content.value = draft.content ?: ""
        thread.value = draft.thread
        this.payOptions.value = payOptions
        millisLastReplyInit = draft.millisCreated
    }

    fun onDestinationInputChanged(paymail: String) {
        destinationPaymail.value = paymail.trim()
        onDraftContentChanged()
    }

    fun onSubjectInputChanged(subject: String) {
        this.subject.value = subject
        onDraftContentChanged()
    }

    fun onContentInputChanged(content: String) {
        this.content.value = content
        onDraftContentChanged()
    }

    fun onClearReplyTo() {
        viewModelScope.launch {
            thread.value = null
            onDraftContentChanged()
        }
    }

    fun onPayOptionsMinus() {
        val payModel = payOptions.value

        val newMax = payModel.valueMax / 10
        if (newMax < 0.1f) return

        val newMin = newMax / 10
        val newOptions = ViewBaePayOptions.Model(
            newMin,
            newMax,
            newMin,
            payModel.currency
        )

        payOptions.value = newOptions
        onDraftContentChanged()
    }

    fun onPayOptionsPlus() {
        val payModel = payOptions.value

        val newMax = payModel.valueMax * 10
        if (newMax > 1_000_000) return

        val newMin = newMax / 10
        val newOptions = ViewBaePayOptions.Model(
            newMin,
            newMax,
            newMin,
            payModel.currency
        )

        payOptions.value = newOptions
        onDraftContentChanged()
    }

    fun onPayOptionsValueChange(value: Float) {
        val payModel = payOptions.value

        if (value < payModel.valueMin) return
        if (value > payModel.valueMax) return

        payOptions.value = payModel.copy(
            valueCurrent = value
        )
        onDraftContentChanged()
    }

    private fun onDraftContentChanged() {
        viewModelScope.launch {
            val payModel = payOptions.value
            saveBaemailDraftUseCase(
                destinationPaymail.value,
                subject.value,
                content.value,
                thread.value,
                payModel.currency,
                payModel.valueMax,
                payModel.valueCurrent
            )
        }
    }

    fun onDraftDiscarded() {
        viewModelScope.launch {
            discardDraft()
            _eventFlow.emit(NewBaemailUiEvent.DiscardSuccess)
        }
    }

    private suspend fun discardDraft() {
        discardBaemailDraftUseCase()

        destinationPaymail.value = ""
        subject.value = ""
        content.value = ""
        payOptions.value = defaultPayOptions
        thread.value = null
    }

    fun onSendMessage() {
        viewModelScope.launch {
            _eventFlow.emit(NewBaemailUiEvent.Processing)
            try {
                sendBaemailMessageUseCase(
                    destinationPaymail.value.trim(),
                    subject.value.trim(),
                    content.value.trim(),
                    thread.value,
                    payOptions.value.valueCurrent
                )
                discardDraft()
                _eventFlow.emit(NewBaemailUiEvent.SendSuccess)

            } catch (e: ErrorNotReady) {
                Timber.e(e)
                _eventFlow.emit(NewBaemailUiEvent.ShowSnackbar(R.string.error_no_internet))

            } catch (e: ErrorPaymailDoesNotExist) {
                Timber.e(e)
                _eventFlow.emit(
                    NewBaemailUiEvent.ShowSnackbar(R.string.error_paymail_does_not_exist)
                )

            } catch (e: ErrorGettingDestinationOutput) {
                Timber.e(e)
                _eventFlow.emit(NewBaemailUiEvent.ShowSnackbar(
                    R.string.error_finding_paymail_destination_output
                ))

            } catch (e: ErrorConstructingTx) {
                Timber.e(e)
                _eventFlow.emit(
                    NewBaemailUiEvent.ShowSnackbar(R.string.error_failed_constructing_tx)
                )

            } catch (e: TxNotAcceptedByBaemail) {
                Timber.e(e)
                _eventFlow.emit(
                    NewBaemailUiEvent.ShowSnackbar(R.string.error_tx_not_accepted_by_baemail)
                )

            } catch (e: TxNotAcceptedByMiner) {
                Timber.e(e)
                _eventFlow.emit(NewBaemailUiEvent.ShowSnackbar(R.string.error_tx_not_accepted))

            } catch (e: Exception) {
                Timber.e(e)
                _eventFlow.emit(NewBaemailUiEvent.ShowSnackbar(R.string.error))

            }
        }
    }
}