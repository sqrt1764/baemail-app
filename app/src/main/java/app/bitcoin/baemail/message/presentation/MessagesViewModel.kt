package app.bitcoin.baemail.message.presentation

import androidx.lifecycle.*
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.di.SavedStateViewModelAssistant
import app.bitcoin.baemail.message.data.entity.Filter
import app.bitcoin.baemail.message.data.entity.MessageBucket
import app.bitcoin.baemail.message.data.entity.SortType
import app.bitcoin.baemail.message.domain.MapMessagesToAdapterTypesUseCase
import app.bitcoin.baemail.message.domain.SearchMessagesUseCase
import app.bitcoin.baemail.message.presentation.entity.ListCacheKey
import app.bitcoin.baemail.message.presentation.entity.MessagesStateUi
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.stateIn
import timber.log.Timber


class MessagesViewModel @AssistedInject constructor(
    @Assisted private val state : SavedStateHandle,
    private val getActivePaymailUseCase: GetActivePaymailUseCase,
    private val searchMessagesUseCase: SearchMessagesUseCase,
    private val mapMessagesToAdapterTypesUseCase: MapMessagesToAdapterTypesUseCase
): ViewModel() {

    @AssistedFactory
    interface Assistant : SavedStateViewModelAssistant<MessagesViewModel> {
        override fun create(savedStateHandle: SavedStateHandle): MessagesViewModel
    }

    companion object {
        private const val KEY_LIST_STATE = "list_state"

        private const val KEY_SORT_TYPE = "sort_type"
        private const val KEY_SEARCH_QUERY = "query"
        private const val KEY_BUCKET = "bucket"
        private const val KEY_INITIALIZED = "initialized"
        private const val KEY_SIGNAL_REQUEST_NEXT_PAGE = "signal_request_next"
        private const val KEY_SIGNAL_REQUEST_REFRESH = "signal_request_refresh"
    }

    private val stateHandler = StateHandler(state, viewModelScope)

    private val messageParam: SharedFlow<Filter?> = channelFlow {
        var lastPageRequest = 0L
        var lastRequestRefresh = 0L

        val pageCountCache = mutableMapOf<ListCacheKey, Int>()

        getActivePaymailUseCase()
            .mapNotNull { it?.paymail }
            .combine(stateHandler.bucket) { paymail, bucket ->
                paymail to MessageBucket.parse(bucket)
            }.combine(stateHandler.sort) { (paymail, bucket), sort ->
                Triple(paymail, bucket, SortType.parse(sort))
            }.combine(stateHandler.query) { (paymail, bucket, sort), query ->
                Triple(paymail, bucket, sort) to query
            }.combine(
                stateHandler.signalRequestNextPage.combine(stateHandler.signalRequestRefresh) {
                        nextPageSignal, refreshSignal ->
                    nextPageSignal to refreshSignal
                }
            ) { (triple, query), (nextPageSignal, refreshSignal) ->
                val (paymail, bucket, sort) = triple

                val pageCountCacheKey = ListCacheKey(
                    paymail,
                    query,
                    sort,
                    bucket
                )

                //look up the page-count in the cache
                val cachedPageCount = pageCountCache[pageCountCacheKey] ?: 0

                val finalPage = if (refreshSignal > lastRequestRefresh) {
                    //reset page-count to 0
                    lastRequestRefresh = refreshSignal
                    0
                } else if (nextPageSignal > lastPageRequest) {
                    //next page
                    lastPageRequest = nextPageSignal
                    cachedPageCount + 1
                } else cachedPageCount //same page

                //update cache with the latest page-count
                pageCountCache[pageCountCacheKey] = finalPage

                Filter(
                    refreshSignal,
                    paymail,
                    query,
                    sort,
                    bucket,
                    finalPage
                )

            }.collectLatest {
                send(it)
            }

    }.shareIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        replay = 1
    )

    val searchResults: StateFlow<MessagesStateUi?> = messageParam.flatMapLatest { param ->
        Timber.d("....searchResults param: $param")
        if (param == null) return@flatMapLatest flow<MessagesStateUi?> {
            emit(null)
        }
        searchMessagesUseCase(param).map {
            MessagesStateUi(
                it.filter,
                mapMessagesToAdapterTypesUseCase(it.filter.paymail, it.items),
                it.hasNextPage,
                it.isRefreshing
            )

        }

    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = null
    )

    fun init(bucket: MessageBucket, initialSort: SortType) {
        if (stateHandler.isInitialized()) return
        stateHandler.setInitialized()

        stateHandler.setNewQuery("")
        stateHandler.setNewBucket(bucket)
        stateHandler.setNewSort(initialSort)
    }

    fun onSwipeToRefresh() {
        stateHandler.signalRefresh()
    }

    fun onSignalRequestNextPage() {
        searchResults.value?.let {
            if (it.isRefreshing) {
                Timber.d(">>>>>>> onSignalRequestNextPage dropped; still fetching the current page")
                //todo do recursion?
                Timber.d(">>>>>>> !!!! doing recursion!!!!!! #onSignalRequestNextPage()")
                return
            }

            if (!it.hasNextPage) {
                return
            }

            stateHandler.signalNextPage()
        }
    }

    fun onSortTypeToggled() {
        val newValue = when (SortType.parse(stateHandler.sort.value)) {
            SortType.VALUE -> SortType.TIME
            SortType.TIME -> SortType.VALUE
        }
        stateHandler.setNewSort(newValue)
    }

    fun onQueryTextChange(query: String): Boolean {
        if (stateHandler.query.value == query) return false
        stateHandler.setNewQuery(query)
        return true
    }

    fun onQueryTextSubmit(query: String): Boolean {
        return onQueryTextChange(query)
    }

    fun onRequestClearSearchResults() {
        val query = stateHandler.query.value
        if (query.isBlank()) return
        stateHandler.setNewQuery(null)
    }

    fun onBuckedChanged(bucket: MessageBucket) {
        if (stateHandler.bucket.value == bucket.const) return
        stateHandler.setNewBucket(bucket)
    }

    fun persistListScrollState(listState: Any) {
        state[KEY_LIST_STATE] = listState
    }

    fun getPersistedListScrollState(): Any? {
        return state[KEY_LIST_STATE]
    }

    class StateHandler(
        private val savedState: SavedStateHandle,
        scope: CoroutineScope
    ) {

        val bucket = savedState.getStateFlow(KEY_BUCKET, MessageBucket.INBOX.const)
        val query = savedState.getStateFlow(KEY_SEARCH_QUERY, "")
        val sort = savedState.getStateFlow(KEY_SORT_TYPE, SortType.VALUE.const)

        val signalRequestNextPage = savedState
            .getStateFlow(KEY_SIGNAL_REQUEST_NEXT_PAGE,0L)
            .shareIn(
                scope = scope,
                started = SharingStarted.WhileSubscribed(5000),
                replay = 1
            )

        val signalRequestRefresh = savedState
            .getStateFlow(KEY_SIGNAL_REQUEST_REFRESH,0L)
            .shareIn(
                scope = scope,
                started = SharingStarted.WhileSubscribed(5000),
                replay = 1
            )

        fun isInitialized(): Boolean {
            return savedState.get<Boolean>(KEY_INITIALIZED) ?: false
        }

        fun setInitialized() {
            savedState[KEY_INITIALIZED] = true
        }

        fun signalNextPage() {
            savedState[KEY_SIGNAL_REQUEST_NEXT_PAGE] = System.currentTimeMillis()
        }

        fun signalRefresh() {
            savedState[KEY_SIGNAL_REQUEST_REFRESH] = System.currentTimeMillis()
        }

        fun setNewBucket(bucket: MessageBucket) {
            savedState[KEY_BUCKET] = bucket.const
        }

        fun setNewSort(type: SortType) {
            savedState[KEY_SORT_TYPE] = type.const
        }

        fun setNewQuery(query: String?) {
            savedState[KEY_SEARCH_QUERY] = query ?: ""
        }
    }
}

