package app.bitcoin.baemail.message.data.entity

enum class SortType(val const: String) {
    VALUE("value"),
    TIME("time");

    companion object {
        fun parse(serialized: String): SortType {
            return values().find { it.const == serialized }!!
        }
    }
}