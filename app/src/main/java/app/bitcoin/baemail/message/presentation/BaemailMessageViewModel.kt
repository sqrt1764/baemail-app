package app.bitcoin.baemail.message.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.bitcoin.baemail.R
import app.bitcoin.baemail.message.domain.ArchiveBaemailMessageUseCase
import app.bitcoin.baemail.message.domain.DeleteBaemailMessageUseCase
import app.bitcoin.baemail.message.domain.FormatMessageDateUseCase
import app.bitcoin.baemail.message.domain.FormatMessageTimeUseCase
import app.bitcoin.baemail.message.domain.GetMessageUseCase
import app.bitcoin.baemail.message.domain.HtmlToSpannedStringUseCase
import app.bitcoin.baemail.message.domain.ParseDecryptedMessageContent
import app.bitcoin.baemail.message.domain.PostBaemailReadReceiptUseCase
import app.bitcoin.baemail.message.domain.UnarchiveBaemailMessageUseCase
import app.bitcoin.baemail.message.presentation.entity.BaemailMessageUi
import app.bitcoin.baemail.message.presentation.entity.BaemailMessageUiEvent
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

//TODO a bug that needs resolving...
// the user can navigate to a message (by tapping on a 'replied to' link) that cannot be found in
// the local database... this message needs to be explicitly fetched from the backend
class BaemailMessageViewModel @Inject constructor(
    private val getMessageUseCase: GetMessageUseCase,
    private val formatMessageTimeUseCase: FormatMessageTimeUseCase,
    private val formatMessageDateUseCase: FormatMessageDateUseCase,
    private val htmlToSpannedStringUseCase: HtmlToSpannedStringUseCase,
    private val parseDecryptedMessageContent: ParseDecryptedMessageContent,
    private val archiveBaemailMessageUseCase: ArchiveBaemailMessageUseCase,
    private val unarchiveBaemailMessageUseCase: UnarchiveBaemailMessageUseCase,
    private val deleteBaemailMessageUseCase: DeleteBaemailMessageUseCase,
    private val postBaemailReadReceiptUseCase: PostBaemailReadReceiptUseCase
) : ViewModel()  {

    private val messageRef = MutableStateFlow<String?>(null)

    val state: StateFlow<BaemailMessageUi?> = messageRef
        .mapNotNull { it }
        .flatMapLatest { txId ->
            getMessageUseCase(txId)
        }.mapNotNull { message ->
            message ?: return@mapNotNull null

            val blocksContent = parseDecryptedMessageContent(message.decryptedMessage)

            BaemailMessageUi(
                message.ownerPaymail,
                message,
                htmlToSpannedStringUseCase(blocksContent),
                formatMessageDateUseCase(message.createdAt),
                formatMessageTimeUseCase(message.createdAt)
            )

        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = null
        )


    private val _eventFlow = MutableSharedFlow<BaemailMessageUiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        viewModelScope.launch {
            state.mapNotNull { it }.first().message.let {
                postBaemailReadReceiptUseCase(it)
            }
        }
    }

    fun init(txId: String) {
        messageRef.value = txId
    }

    fun archiveMessage() {
        viewModelScope.launch {
            _eventFlow.emit(BaemailMessageUiEvent.Processing)

            try {
                val message = state.value?.message ?: throw RuntimeException()
                archiveBaemailMessageUseCase(message)
                _eventFlow.emit(BaemailMessageUiEvent.Success)

            } catch (e: Exception) {
                _eventFlow.emit(BaemailMessageUiEvent.ShowSnackbar(R.string.error))
            }
        }
    }

    fun unarchiveMessage() {
        viewModelScope.launch {
            _eventFlow.emit(BaemailMessageUiEvent.Processing)

            try {
                val message = state.value?.message ?: throw RuntimeException()
                unarchiveBaemailMessageUseCase(message)
                _eventFlow.emit(BaemailMessageUiEvent.Success)

            } catch (e: Exception) {
                _eventFlow.emit(BaemailMessageUiEvent.ShowSnackbar(R.string.error))
            }
        }
    }

    fun deleteMessage() {
        viewModelScope.launch {
            _eventFlow.emit(BaemailMessageUiEvent.Processing)

            try {
                val message = state.value?.message ?: throw RuntimeException()
                deleteBaemailMessageUseCase(message)
                _eventFlow.emit(BaemailMessageUiEvent.Success)

            } catch (e: Exception) {
                _eventFlow.emit(BaemailMessageUiEvent.ShowSnackbar(R.string.error))
            }
        }
    }

}