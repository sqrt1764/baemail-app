package app.bitcoin.baemail.message.data.entity

import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage

data class FilteredResults(
    val messages: List<DecryptedBaemailMessage>,
    val hasMorePages: Boolean,
    val isOfflineResult: Boolean
)