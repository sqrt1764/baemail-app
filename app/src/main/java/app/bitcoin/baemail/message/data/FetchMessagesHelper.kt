package app.bitcoin.baemail.message.data

import app.bitcoin.baemail.core.data.room.dao.DecryptedBaemailMessageDao
import app.bitcoin.baemail.message.data.entity.Filter
import app.bitcoin.baemail.message.data.entity.FilteredResults
import app.bitcoin.baemail.message.data.entity.SortType
import com.dropbox.android.external.store4.Fetcher
import com.dropbox.android.external.store4.SourceOfTruth
import com.dropbox.android.external.store4.Store
import com.dropbox.android.external.store4.StoreBuilder
import com.dropbox.android.external.store4.StoreRequest
import com.dropbox.android.external.store4.StoreResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import timber.log.Timber

class FetchMessagesHelper(
    private val dao: DecryptedBaemailMessageDao,
    private val index: MessageIndex,
    private val baemailDataSource: BaemailDataSource
) {

    private val pageSize: Int = index.pageSize

    fun getMessages(filter: Filter): Flow<StoreResponse<FilteredResults>> {
        val shouldRefresh = filter.id != index.getFilterId(filter)
        return messageStore.stream(StoreRequest.cached(filter, refresh = shouldRefresh))
    }

    private val messageStore: Store<Filter, FilteredResults> = StoreBuilder.from(
        fetcher = Fetcher.ofFlow { filter ->
            flow {
                var didError = false
                val gotList = try {
                    //todo there is a bug where performing actions on messages & then fetching
                    // further pages in message-bucket leads to 'holes' in result-sets,
                    // app should rely on local-result-set-size instead of page-number when
                    // fetching results
                    baemailDataSource.fetchAndHandlePageOfBaemailMessages(filter)

                } catch (e: Exception) {
                    Timber.e(e)
                    didError = true
                    emptyList()
                }

                val hasMoreResults = gotList.size == pageSize

                emit(
                    FilteredResults(
                        gotList,
                        hasMoreResults,
                        didError
                    )
                )
            }
        },
        sourceOfTruth = SourceOfTruth.of(
            reader = ::query,
            writer = ::insert,
            delete = ::delete,
            deleteAll = ::deleteAll
        )
    ).build()



    private fun query(
        key: Filter
    ): Flow<FilteredResults> {
        Timber.d("...querying  now; $key")
        val resultsInfoFlow = index.getList(key)

        return resultsInfoFlow.map { resultsInfo ->
            return@map if (resultsInfo == null) {
                val byTimeNotValue = key.sort == SortType.TIME
                val list = dao.queryByBucketAndOwner(
                    key.bucket,
                    key.paymail,
                    "%${key.query}%",
                    byTimeNotValue,
                    index.pageSize
                ).firstOrNull()

                FilteredResults(
                    list ?: emptyList(),
                    false,
                    true
                )

            } else {
                val (messageOrder, hasMorePages) = resultsInfo
                val messages = dao.queryMessagesByOwnerAndTxId(key.paymail, messageOrder)

                val ordered = messageOrder.mapNotNull { txId ->
                    messages.find { it.txId == txId }
                }

                FilteredResults(
                    ordered,
                    hasMorePages,
                    false
                )
            }
        }
    }

    private suspend fun insert(
        key: Filter,
        results: FilteredResults
    ) {
        if (results.isOfflineResult) return

        dao.insertList(results.messages)

        results.messages.map { it.txId }.let { list ->
            index.update(key, list, results.hasMorePages)
        }
    }

    private suspend fun delete(filter: Filter) {
        index.clear(filter)
    }

    private suspend fun deleteAll() {
        throw RuntimeException()
    }
}

