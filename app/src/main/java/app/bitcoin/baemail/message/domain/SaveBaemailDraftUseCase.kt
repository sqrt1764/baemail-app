package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull

class SaveBaemailDraftUseCaseImpl(
    private val getActivePaymailUseCase: GetActivePaymailUseCase,
    private val baemailRepository: BaemailRepository
): SaveBaemailDraftUseCase {
    override suspend fun invoke(
        destinationPaymail: String,
        subject: String,
        content: String,
        threadId: String?,
        payCurrency: String,
        payMax: Float,
        payAmount: Float
    ) {
        val paymail = getActivePaymailUseCase().mapNotNull { it?.paymail }.first()

        baemailRepository.messageDraftHelper.applyNewDraftContent(
            paymail,
            destinationPaymail,
            subject,
            content,
            threadId,
            payCurrency,
            payMax,
            payAmount
        )
    }

}


interface SaveBaemailDraftUseCase {
    suspend operator fun invoke(
        destinationPaymail: String,
        subject: String,
        content: String,
        threadId: String?,
        payCurrency: String,
        payMax: Float,
        payAmount: Float
    )
}