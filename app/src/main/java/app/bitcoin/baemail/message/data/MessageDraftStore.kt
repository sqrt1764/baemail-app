package app.bitcoin.baemail.message.data

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.floatPreferencesKey
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import app.bitcoin.baemail.message.data.entity.Draft
import kotlinx.coroutines.flow.first

class MessageDraftStore(
    private val ds: DataStore<Preferences>
) {
    suspend fun getDraft(paymail: String): Draft {
        val keyMillisCreated = longPreferencesKey("millis_created__$paymail")
        val keyDestination = stringPreferencesKey("destination__$paymail")
        val keySubject = stringPreferencesKey("subject__$paymail")
        val keyContent = stringPreferencesKey("content__$paymail")
        val keyThread = stringPreferencesKey("thread__$paymail")
        val keyPayCurrency = stringPreferencesKey("pay_currency__$paymail")
        val keyPayMax = floatPreferencesKey("pay_max__$paymail")
        val keyPayAmount = floatPreferencesKey("pay_amount__$paymail")

        val prefs = ds.data.first()

        var millisCreated: Long? = null
        if (prefs.contains(keyMillisCreated)) {
            millisCreated = prefs[keyMillisCreated]
        }
        var destination: String? = null
        if (prefs.contains(keyDestination)) {
            destination = prefs[keyDestination]
        }
        var subject: String? = null
        if (prefs.contains(keySubject)) {
            subject = prefs[keySubject]
        }
        var content: String? = null
        if (prefs.contains(keyContent)) {
            content = prefs[keyContent]
        }
        var thread: String? = null
        if (prefs.contains(keyThread)) {
            thread = prefs[keyThread]
        }
        var payCurrency: String? = null
        if (prefs.contains(keyPayCurrency)) {
            payCurrency = prefs[keyPayCurrency]
        }
        var payMax: Float? = null
        if (prefs.contains(keyPayMax)) {
            payMax = prefs[keyPayMax]
        }
        var payAmount: Float? = null
        if (prefs.contains(keyPayAmount)) {
            payAmount = prefs[keyPayAmount]
        }

        payMax = payMax ?: 0.1f

        return Draft(
            paymail,
            millisCreated ?: 0L,
            destination,
            subject,
            content,
            thread,
            payCurrency ?: "USD",
            payMax,
            payAmount ?: (payMax / 10)
        )
    }


    suspend fun applyNewReply(
        owner: String,
        destinationPaymail: String,
        subject: String,
        threadId: String,
        millisCreated: Long
    ) {
        val keyMillisCreated = longPreferencesKey("millis_created__$owner")
        val keyDestination = stringPreferencesKey("destination__$owner")
        val keySubject = stringPreferencesKey("subject__$owner")
        val keyThread = stringPreferencesKey("thread__$owner")


        ds.edit { prefs ->
            prefs[keyMillisCreated] = millisCreated
            prefs[keyDestination] = destinationPaymail
            prefs[keySubject] = subject
            prefs[keyThread] = threadId
        }

    }

    suspend fun applyNewDraftContent(
        owner: String,
        destinationPaymail: String,
        subject: String,
        content: String,
        threadId: String?,
        payCurrency: String,
        payMax: Float,
        payAmount: Float
    ) {


        val keyDestination = stringPreferencesKey("destination__$owner")
        val keySubject = stringPreferencesKey("subject__$owner")
        val keyContent = stringPreferencesKey("content__$owner")
        val keyThread = stringPreferencesKey("thread__$owner")
        val keyPayCurrency = stringPreferencesKey("pay_currency__$owner")
        val keyPayMax = floatPreferencesKey("pay_max__$owner")
        val keyPayAmount = floatPreferencesKey("pay_amount__$owner")

        ds.edit { prefs ->
            prefs[keyDestination] = destinationPaymail
            prefs[keySubject] = subject
            prefs[keyContent] = content

            threadId?.let {
                prefs[keyThread] = it
            } ?: let {
                prefs.remove(keyThread)
            }

            prefs[keyPayCurrency] = payCurrency
            prefs[keyPayMax] = payMax
            prefs[keyPayAmount] = payAmount

        }
    }

    suspend fun discardDraft(owner: String) {

        val keyDestination = stringPreferencesKey("destination__$owner")
        val keySubject = stringPreferencesKey("subject__$owner")
        val keyContent = stringPreferencesKey("content__$owner")
        val keyThread = stringPreferencesKey("thread__$owner")
        val keyPayCurrency = stringPreferencesKey("pay_currency__$owner")
        val keyPayMax = floatPreferencesKey("pay_max__$owner")
        val keyPayAmount = floatPreferencesKey("pay_amount__$owner")


        ds.edit { prefs ->
            prefs.remove(keyDestination)
            prefs.remove(keySubject)
            prefs.remove(keyContent)
            prefs.remove(keyThread)
            prefs.remove(keyPayCurrency)
            prefs.remove(keyPayMax)
            prefs.remove(keyPayAmount)

        }
    }


    suspend fun clearReplyTo(owner: String) {

        val keyThread = stringPreferencesKey("thread__$owner")

        ds.edit { prefs ->
            prefs.remove(keyThread)

        }
    }

}