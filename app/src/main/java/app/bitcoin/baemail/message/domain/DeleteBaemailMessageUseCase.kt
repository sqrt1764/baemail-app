package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.message.data.entity.MessageBucket
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull

class DeleteBaemailMessageUseCaseImpl(
    val baemailRepository: BaemailRepository,
    val getActivePaymailUseCase: GetActivePaymailUseCase
): DeleteBaemailMessageUseCase {
    override suspend fun invoke(message: DecryptedBaemailMessage) {
        if (message.bucket != MessageBucket.ARCHIVE &&
            message.bucket != MessageBucket.SENT) throw RuntimeException()

        val paymail = getActivePaymailUseCase().mapNotNull { it }.first().paymail
            ?: throw RuntimeException()

        baemailRepository.deleteMessage(paymail, message.txId)
    }

}

interface DeleteBaemailMessageUseCase {
    suspend operator fun invoke(message: DecryptedBaemailMessage)
}