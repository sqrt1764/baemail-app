package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.mapNotNull

class GetMessageUseCaseImpl(
    private val getActivePaymailUseCase: GetActivePaymailUseCase,
    private val baemailRepository: BaemailRepository
) : GetMessageUseCase {
    override fun invoke(txId: String): Flow<DecryptedBaemailMessage?>  {
        return getActivePaymailUseCase().mapNotNull { it?.paymail }.flatMapLatest { paymail ->
            baemailRepository.getMessage(paymail, txId)
        }
    }

}

interface GetMessageUseCase {
    operator fun invoke(txId: String): Flow<DecryptedBaemailMessage?>
}