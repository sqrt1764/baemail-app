package app.bitcoin.baemail.message.domain

import android.content.Context
import android.text.format.DateFormat

class FormatMessageDateUseCaseImpl(
    val context: Context
) : FormatMessageDateUseCase {

    private val messageDateFormat = DateFormat.getMediumDateFormat(context)

    override fun invoke(createdAtMillis: Long): String {
        return messageDateFormat.format(createdAtMillis)
    }

}

interface FormatMessageDateUseCase {
    operator fun invoke(createdAtMillis: Long): String
}