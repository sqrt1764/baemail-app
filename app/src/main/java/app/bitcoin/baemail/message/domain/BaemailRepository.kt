package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.message.data.MessageDraftStore
import app.bitcoin.baemail.message.data.entity.Filter
import app.bitcoin.baemail.message.data.entity.FilteredResults
import com.dropbox.android.external.store4.StoreResponse
import kotlinx.coroutines.flow.Flow

interface BaemailRepository {

    val messageDraftHelper: MessageDraftStore

    fun getMessages(filter: Filter): Flow<StoreResponse<FilteredResults>>

    fun getMessage(ownerPaymail: String, txHash: String): Flow<DecryptedBaemailMessage?>

    suspend fun archiveMessage(activePaymail: String, txHash: String)

    suspend fun unarchiveMessage(activePaymail: String, txHash: String)

    suspend fun deleteMessage(activePaymail: String, txHash: String)

    suspend fun requestPostReadReceipt(
        txHash: String,
        seenByPaymail: String
    )

}