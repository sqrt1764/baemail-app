package app.bitcoin.baemail.message.presentation.entity

import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import timber.log.Timber

class BaemailRecyclerStateCache(
    val map: MutableMap<ListCacheKey, Parcelable>
) : Parcelable {

    constructor(parcel: Parcel) : this(mutableMapOf()) {
        val count = parcel.readInt()

        for (i in 0 until count) {
            val key = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                parcel.readSerializable(
                    ListCacheKey::class.java.classLoader,
                    ListCacheKey::class.java
                )
            } else {
                parcel.readSerializable() as ListCacheKey
            }
            val layoutManagerSavedState = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                parcel.readParcelable(
                    androidx.recyclerview.widget.LinearLayoutManager::class.java.classLoader,
                    Parcelable::class.java
                )
            } else {
                parcel.readParcelable(Parcelable::class.java.classLoader)
            }
            if (key == null || layoutManagerSavedState == null) {
                Timber.d("...failed to parse the parcel")
            } else {
                map[key] = layoutManagerSavedState
            }
        }

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(map.size)
        map.entries.forEach { (key, state) ->
            parcel.writeSerializable(key)
            parcel.writeParcelable(state, flags)
        }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BaemailRecyclerStateCache> {
        override fun createFromParcel(parcel: Parcel): BaemailRecyclerStateCache {
            return BaemailRecyclerStateCache(parcel)
        }

        override fun newArray(size: Int): Array<BaemailRecyclerStateCache?> {
            return arrayOfNulls(size)
        }
    }
}