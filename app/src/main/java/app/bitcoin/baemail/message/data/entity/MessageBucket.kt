package app.bitcoin.baemail.message.data.entity

enum class MessageBucket(val const: String) {
    INBOX("inbox"),
    ARCHIVE("archive"),
    SENT("sent");

    companion object {
        fun parse(serialized: String): MessageBucket {
            return values().find { it.const == serialized }!!
        }
    }
}