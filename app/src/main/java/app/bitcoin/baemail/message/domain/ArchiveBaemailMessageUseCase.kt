package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.message.data.entity.MessageBucket
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull

class ArchiveBaemailMessageUseCaseImpl(
    val baemailRepository: BaemailRepository,
    val getActivePaymailUseCase: GetActivePaymailUseCase
) : ArchiveBaemailMessageUseCase {

    override suspend fun invoke(message: DecryptedBaemailMessage) {
        if (message.bucket != MessageBucket.INBOX) throw RuntimeException()

        val paymail = getActivePaymailUseCase().mapNotNull { it }.first().paymail
            ?: throw RuntimeException()

        baemailRepository.archiveMessage(paymail, message.txId)
    }
}

interface ArchiveBaemailMessageUseCase {
    suspend operator fun invoke(message: DecryptedBaemailMessage)
}