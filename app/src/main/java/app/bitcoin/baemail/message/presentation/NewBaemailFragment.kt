package app.bitcoin.baemail.message.presentation

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Outline
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.doOnPreDraw
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import app.bitcoin.baemail.message.presentation.util.DrawableRevealSend
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.message.presentation.util.DrawableStyledLabel
import app.bitcoin.baemail.core.presentation.util.NoSwipeBehavior
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ErrorSnackBar
import app.bitcoin.baemail.core.presentation.view.NontouchableCoordinatorLayout
import app.bitcoin.baemail.core.presentation.view.ViewBaePayOptions
import app.bitcoin.baemail.message.presentation.entity.NewBaemailUiEvent
import app.bitcoin.baemail.message.presentation.util.SendBaemailGesture
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.min

class NewBaemailFragment : Fragment(R.layout.fragment_new_message) {

    companion object {
        const val KEY_IS_NEW_REPLY = "new_reply"
        const val KEY_DESTINATION_PAYMAIL = "destination_paymail"
        const val KEY_SUBJECT = "subject"
        const val KEY_THREAD = "thread"
        const val KEY_MILLIS_CREATED = "millis_created"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: NewBaemailViewModel


    private lateinit var discard: Button

    private lateinit var container: NontouchableCoordinatorLayout
    private lateinit var contentContainer: ConstraintLayout
    private lateinit var moneyOptions: ConstraintLayout

    private lateinit var moneyOptionsDimmingView: View
    private lateinit var moneyOptionsContent: ViewBaePayOptions

    private lateinit var messageType: TextView
    private lateinit var send: TextView
    private lateinit var inputTo: EditText
    private lateinit var inputSubject: EditText
    private lateinit var inputContent: EditText
    private lateinit var validIndicator: ImageView

    private lateinit var labelTitle: TextView

    private lateinit var replyToLabel: TextView
    private lateinit var replyToValue: TextView
    private lateinit var replyToClear: ImageView
    private lateinit var replyToSeparator: View

    private lateinit var coordinator: CoordinatorLayout

    lateinit var appBar: AppBarLayout
    lateinit var scroller: NestedScrollView

    lateinit var bottomGap: View
    lateinit var hoverHolder: ConstraintLayout

    lateinit var close: ImageView


    private var currentSnackBar: ErrorSnackBar? = null

    private lateinit var optionsSheetBehavior: BottomSheetBehavior<View>

    private lateinit var sendBaemailGesture: SendBaemailGesture

    private val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }
    private val colorSelectorOnLight: Int by lazy {
        R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorSendBackground: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }

    private val colorBlue2: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimaryVariant)
    }

    private val colorSendBackgroundDisabled: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimaryVariant)
    }

    private val drawableSendBackground: ColorDrawable by lazy {
        ColorDrawable(colorSendBackgroundDisabled)
    }

    private val drawableSendSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }

    private val colorSuccess: Int by lazy {
        R.color.send_coin_to_paymail__input_error_color_success.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorChecking: Int by lazy {
        R.color.send_coin_to_paymail__input_error_color_processing.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorFail: Int by lazy {
        R.color.send_coin_to_paymail__input_error_color_fail.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val drawablePaymailExists: Drawable by lazy {
        ContextCompat.getDrawable(
            requireContext(),
            R.drawable.baseline_check_circle_24
        )!!.mutate().also {
            it.setTint(colorSuccess)
        }
    }
    private val drawablePaymailNonExistent: Drawable by lazy {
        ContextCompat.getDrawable(
            requireContext(),
            R.drawable.baseline_cancel_24
        )!!.mutate().also {
            it.setTint(colorFail)
        }
    }
    private val drawablePaymailExistenceChecking: Drawable by lazy {
        ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_round_hourglass_top_24
        )!!.mutate().also {
            it.setTint(colorChecking)
        }
    }

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private var isCreatingNewReply = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        isCreatingNewReply = requireArguments().getBoolean(KEY_IS_NEW_REPLY)

        val destinationPaymail = if (isCreatingNewReply) {
            requireArguments().getString(KEY_DESTINATION_PAYMAIL)
        } else null

        val subject = if (isCreatingNewReply) {
            requireArguments().getString(KEY_SUBJECT)
        } else null

        val replyThreadId = if (isCreatingNewReply) {
            requireArguments().getString(KEY_THREAD)
        } else null

        val millisReplyCreated = if (isCreatingNewReply) {
            requireArguments().getLong(KEY_MILLIS_CREATED)
        } else null


        viewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactory
        )[NewBaemailViewModel::class.java]

        if (isCreatingNewReply) {
            viewModel.initNewReply(
                destinationPaymail!!,
                subject!!,
                replyThreadId!!,
                millisReplyCreated!!
            )
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        container = requireView().findViewById(R.id.container)
        contentContainer = requireView().findViewById(R.id.content_container)
        moneyOptions = requireView().findViewById(R.id.money_options)

        moneyOptionsDimmingView = requireView().findViewById(R.id.dimming_view)
        moneyOptionsContent = requireView().findViewById(R.id.money_options_content)

        coordinator = requireView().findViewById(R.id.coordinator)

        appBar = requireView().findViewById(R.id.app_bar)
        scroller = requireView().findViewById(R.id.scroller)

        close = requireView().findViewById(R.id.close)

        bottomGap = requireView().findViewById(R.id.bottom_gap)
        hoverHolder = requireView().findViewById(R.id.hover_holder)

        discard = requireView().findViewById(R.id.discard)

        messageType = requireView().findViewById(R.id.message_type)
        send = requireView().findViewById(R.id.send)
        inputTo = requireView().findViewById(R.id.input_to)
        inputSubject = requireView().findViewById(R.id.input_subject)
        inputContent = requireView().findViewById(R.id.input_content)
        validIndicator = requireView().findViewById(R.id.valid_indicator)

        labelTitle = requireView().findViewById(R.id.title)

        replyToLabel = requireView().findViewById(R.id.label_reply_to)
        replyToValue = requireView().findViewById(R.id.value_reply_to)
        replyToClear = requireView().findViewById(R.id.clear_reply_to)
        replyToSeparator = requireView().findViewById(R.id.reply_to_bottom_separator)

        val topBarElevation = resources.getDimension(R.dimen.added_paymails_fragment__top_bar_elevation)


        container.doOnApplyWindowInsets { v, windowInsets, _ ->
            val keyboardInset = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                max(windowInsets.getInsets(WindowInsets.Type.ime()).bottom, 0)
            } else {
                windowInsets.systemWindowInsetBottom
            }

            val keyboardHeight = if (v.height != 0 && keyboardInset != 0) {
                val decorViewHeight = requireActivity().window.decorView.height

                val location = IntArray(2)
                view.getLocationOnScreen(location)
                val yPosOfView = location[1]

                val bottomHeightOutsideView = decorViewHeight - (v.height + yPosOfView)
                keyboardInset - bottomHeightOutsideView
            } else {
                0
            }

            hoverHolder.translationY = -1f * keyboardHeight

            scroller.updatePadding(
                bottom = keyboardHeight
            )

            windowInsets
        }


        appBar.doOnPreDraw {
            appBar.elevation = 0f
        }

        scroller.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener {
            override fun onScrollChange(
                v: NestedScrollView,
                scrollX: Int,
                scrollY: Int,
                oldScrollX: Int,
                oldScrollY: Int
            ) {
                if (scrollY < 8 * resources.displayMetrics.density) {
                    appBar.elevation = 0f
                } else {
                    appBar.elevation = topBarElevation
                }
            }
        })

        scroller.overScrollMode = View.OVER_SCROLL_NEVER


        let {
            close.foreground = DrawableState.getNew(
                ContextCompat.getColor(requireContext(), R.color.selector_on_surface)
            )

            close.setOnClickListener {
                imm.hideSoftInputFromWindow(
                    requireView().windowToken,
                    0
                )

                findNavController().popBackStack()
            }

            close.clipToOutline = true
            close.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    view ?: return
                    outline ?: return
                    outline.setOval(0, 0, view.width, view.height)
                }

            }
        }


        val optionsSheetLP = moneyOptions.layoutParams as CoordinatorLayout.LayoutParams
        optionsSheetBehavior = optionsSheetLP.behavior as BottomSheetBehavior

        optionsSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        optionsSheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {

            var collapsingBackPressedCallback: CollapsingBackPressedCallback? = null
            inner class CollapsingBackPressedCallback : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Timber.d("collapsingBackPressedCallback .. handleOnBackPressed")
                    optionsSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

                    remove()
                    collapsingBackPressedCallback = null
                }

            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                Timber.d("collapsingBackPressedCallback .. onStateChanged newState:$newState")

                if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                    collapsingBackPressedCallback.let {
                        if (it != null) return@let
                        val cb = CollapsingBackPressedCallback()
                        collapsingBackPressedCallback = cb
                        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, cb)
                        Timber.d("collapsingBackPressedCallback .. added")
                    }

                } else if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                    collapsingBackPressedCallback?.remove()
                    collapsingBackPressedCallback = null
                    Timber.d("collapsingBackPressedCallback .. removed")
                }

            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                //do nothing
            }
        })


        replyToValue.background = DrawableState.getNew(colorSelectorOnLight)
        replyToValue.clipToOutline = true
        replyToValue.outlineProvider = object : ViewOutlineProvider() {

            val paddingPx = (6 * resources.displayMetrics.density).toInt()
            val radius = 8 * resources.displayMetrics.density

            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                outline.setRoundRect(paddingPx, paddingPx, view.width - paddingPx, view.height - paddingPx, radius)
            }
        }


        replyToValue.setOnClickListener {
            if (isCreatingNewReply) {
                findNavController().popBackStack()
                return@setOnClickListener
            }

            //go to message
            val txId = viewModel.state.value.replyThreadId ?: let {
                Timber.d("id not found")
                return@setOnClickListener
            }
            val args = Bundle().also {
                it.putString(BaemailMessageFragment.KEY_TX_ID, txId)
            }
            findNavController().navigate(
                R.id.action_newMessageFragment_to_baemailMessageFragment,
                args
            )
        }


        replyToClear.background = DrawableState.getNew(colorSelectorOnLight)
        replyToClear.clipToOutline = true
        replyToClear.outlineProvider = object : ViewOutlineProvider() {

            val paddingPx = (6 * resources.displayMetrics.density).toInt()

            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val finalHeight = view.height - 2 * paddingPx
                val left = ((view.width / 2f) - (finalHeight / 2)).toInt()
                outline.setOval(left, paddingPx, left + finalHeight, paddingPx + finalHeight)
            }
        }

        replyToClear.setOnClickListener {
            viewModel.onClearReplyTo()
        }

        messageType.clipToOutline = true
        messageType.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }
        }

        val colorCancelBackground = R.color.green2.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        messageType.background = LayerDrawable(arrayOf(
            ColorDrawable(colorCancelBackground),
            DrawableState.getNew(colorSelectorOnAccent)
        ))

        send.clipToOutline = true
        send.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        send.background = LayerDrawable(arrayOf(
            drawableSendBackground,
            drawableSendSelector
        ))

        moneyOptionsContent.onMinusClicked = {
            viewModel.onPayOptionsMinus()
        }

        moneyOptionsContent.onPlusClicked = {
            viewModel.onPayOptionsPlus()
        }

        moneyOptionsContent.onValueChanged = { value ->
            viewModel.onPayOptionsValueChange(value)
        }

        moneyOptionsDimmingView.setOnClickListener {
            optionsSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }

        messageType.setOnClickListener {
            imm.hideSoftInputFromWindow(
                requireView().windowToken,
                0
            )
            optionsSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        send.setOnClickListener {
            imm.hideSoftInputFromWindow(
                requireView().windowToken,
                0
            )

            viewModel.onSendMessage()
        }



        val dp = resources.displayMetrics.density

        sendBaemailGesture = SendBaemailGesture(
            requireView(),
            viewLifecycleOwner.lifecycleScope,
            DrawableRevealSend(
                dp,
                getString(R.string.baemail_send_gesture_swipe_up_hint),
                getString(R.string.baemail_send_gesture_swipe_up_done),
                600 * dp,
                colorSendBackground,
                requireContext().getColorFromAttr(R.attr.colorOnPrimary),
                colorBlue2,
                requireContext().getColorFromAttr(R.attr.colorError)
            ),
            DrawableStyledLabel(
                dp,
                getString(R.string.baemail_send_gesture_sent),
                labelColor = requireContext().getColorFromAttr(R.attr.colorError),
                labelAlign = Paint.Align.CENTER
            ),
            DrawableStyledLabel(
                dp,
                getString(R.string.baemail_send_gesture_swipe_up),
                labelColor = colorSendBackground,
                labelAlign = Paint.Align.RIGHT
            ),
            dp,
            {
                val locArrayOfSend = IntArray(2)
                send.getLocationOnScreen(locArrayOfSend)
                locArrayOfSend
            },
            {
                val locArrayOfView = IntArray(2)
                requireView().getLocationOnScreen(locArrayOfView)
                locArrayOfView
            },
            100 * dp
        )

        //this controls the animated overlays of the send-button
        send.setOnTouchListener(sendBaemailGesture)

        discard.setOnClickListener {
            imm.hideSoftInputFromWindow(
                requireView().windowToken,
                0
            )

            viewModel.onDraftDiscarded()
        }


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.eventFlow.collectLatest { event ->
                    when(event) {
                        is NewBaemailUiEvent.ShowSnackbar -> {
                            container.setTouchBroken(false)
                            sendBaemailGesture.dismiss()

                            ErrorSnackBar.make(
                                requireContext(),
                                coordinator,
                                getString(event.message)
                            ).let {
                                it.behavior = NoSwipeBehavior()
                                it.duration = Snackbar.LENGTH_SHORT
                                it.show()
                            }

                        }
                        is NewBaemailUiEvent.Processing -> {
                            //break touch input of the whole fragment
                            container.setTouchBroken(true)

                        }
                        is NewBaemailUiEvent.SendSuccess -> {
                            findNavController().popBackStack()
                        }
                        is NewBaemailUiEvent.DiscardSuccess -> {
                            findNavController().popBackStack()
                        }
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            var lastReplyInitState: Boolean? = null
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.state.collect { state ->

                    if (state.paymail.length < 3) {
                        validIndicator.visibility = View.INVISIBLE
                    } else {
                        validIndicator.visibility = View.VISIBLE
                    }

                    when (state.paymailExistenceState) {
                        is PaymailExistence.Valid -> {
                            validIndicator.setImageDrawable(drawablePaymailExists)
                        }
                        is PaymailExistence.Invalid -> {
                            validIndicator.setImageDrawable(drawablePaymailNonExistent)
                        }
                        is PaymailExistence.Checking -> {
                            validIndicator.setImageDrawable(drawablePaymailExistenceChecking)
                        }
                    }

                    inputTo.removeTextChangedListener(inputToWatcher)
                    inputSubject.removeTextChangedListener(inputSubjectWatcher)
                    inputContent.removeTextChangedListener(inputContentWatcher)

                    if (inputTo.text.toString() != state.paymail && !inputTo.isFocused) {
                        inputTo.setText(state.paymail)
                    }
                    if (inputSubject.text.toString() != state.subject && !inputSubject.isFocused) {
                        inputSubject.setText(state.subject)
                    }
                    if (inputContent.text.toString() != state.content/*&& !inputContent.isFocused*/) {
                        val currentPos = inputContent.selectionStart

                        inputContent.setText(state.content)

                        val finalSelection: Int = min(state.content.length, currentPos)
                        inputContent.setSelection(finalSelection)
                    }

                    inputTo.addTextChangedListener(inputToWatcher)
                    inputSubject.addTextChangedListener(inputSubjectWatcher)
                    inputContent.addTextChangedListener(inputContentWatcher)


                    if (!state.isMessageValid) {
                        send.isEnabled = false
                        drawableSendBackground.color = colorSendBackgroundDisabled
                    } else {
                        send.isEnabled = true
                        drawableSendBackground.color = colorSendBackground
                    }

                    moneyOptionsContent.applyModel(state.payOptions)

                    val messageTypeLabel = String.format(
                        Locale.US,
                        "Pay %.2f %s",
                        state.payOptions.valueCurrent,
                        state.payOptions.currency
                    )
                    if (messageTypeLabel != messageType.text.toString()) {
                        messageType.text = messageTypeLabel
                    }


                    if (state.satsAvailable < state.satsEstimatedRequired) {
                        currentSnackBar?.dismiss()

                        ErrorSnackBar.make(
                            requireContext(),
                            coordinator,
                            "Funding wallet needs more juice"//todo localize
                        ).let {
                            it.behavior = NoSwipeBehavior()
                            it.duration = Snackbar.LENGTH_INDEFINITE
                            it.show()
                            currentSnackBar = it
                        }

                    } else {
                        currentSnackBar?.dismiss()
                        currentSnackBar = null
                    }

                    state.replyThreadId?.let {
                        labelTitle.setText(R.string.new_baemail_reply_title)
                    } ?: let {
                        labelTitle.setText(R.string.new_baemail_message_title)
                    }

                    state.replyThreadId?.let { id ->
                        //must be visible
                        if (replyToValue.tag == id) return@let
                        lastReplyInitState = true

                        replyToLabel.visibility = View.VISIBLE
                        replyToValue.visibility = View.VISIBLE
                        replyToClear.visibility = View.VISIBLE
                        replyToSeparator.visibility = View.VISIBLE

                        replyToValue.text = id
                        replyToValue.tag = id

                    } ?: let {
                        //must be hidden
                        if (lastReplyInitState == false) return@let
                        lastReplyInitState = false

                        replyToLabel.visibility = View.GONE
                        replyToValue.visibility = View.GONE
                        replyToClear.visibility = View.GONE
                        replyToSeparator.visibility = View.GONE

                        replyToValue.tag = null

                    }

                    if (state.replyThreadId == null) {
                        if (!inputTo.isEnabled) {
                            inputTo.isEnabled = true
                        }
                        if (!inputSubject.isEnabled) {
                            inputSubject.isEnabled = true
                        }
                    } else {
                        if (inputTo.isEnabled) {
                            inputTo.isEnabled = false
                        }
                        if (inputSubject.isEnabled) {
                            inputSubject.isEnabled = false
                        }
                    }

                }
            }
        }
    }

    private val inputToWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            s ?: return
            viewModel.onDestinationInputChanged(s.toString())
        }

    }

    private val inputSubjectWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            s ?: return
            viewModel.onSubjectInputChanged(s.toString())
        }

    }

    private val inputContentWatcher = object : TextWatcher {

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            s ?: return

            val value = s.toString()

            run { //scroll the view when the user presses `enter` so the cursor does not end up
                // behind the keyboard
                val countOfEnters = value.filter { c -> c == '\n' }.length
                val lastValue = viewModel.state.value.content
                val countOfEntersBefore = lastValue.filter { c -> c == '\n' }.length


                if (countOfEnters > countOfEntersBefore) {
                    Timber.d("new enters detected, scroll the view")
                    val count = countOfEnters - countOfEntersBefore
                    viewLifecycleOwner.lifecycleScope.launch {
                        delay(50L)
                        scroller.smoothScrollBy(0, inputContent.lineHeight * count)
                    }
                }
            }

            viewModel.onContentInputChanged(value)
        }
    }
}






