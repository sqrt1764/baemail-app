package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.message.data.entity.Draft
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapNotNull

class GetSavedMessageDraftUseCaseImpl(
    private val getActivePaymailUseCase: GetActivePaymailUseCase,
    private val baemailRepository: BaemailRepository
) : GetSavedMessageDraftUseCase {

    override suspend fun invoke(): Flow<Draft> {
        return getActivePaymailUseCase().mapNotNull { it?.paymail }.flatMapLatest { paymail ->
            flow {
                baemailRepository.messageDraftHelper.getDraft(paymail).let { draft ->
                    emit(draft)
                }
            }
        }
    }

}

interface GetSavedMessageDraftUseCase {
    suspend operator fun invoke(): Flow<Draft>
}