package app.bitcoin.baemail.message.presentation.entity

import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage

data class BaemailMessageUi(
    val ownerPaymail: String,
    val message: DecryptedBaemailMessage,
    val parsedContent: String,
    val formattedMessageDate: String,
    val formattedMessageTime: String
)