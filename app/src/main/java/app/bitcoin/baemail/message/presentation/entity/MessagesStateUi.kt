package app.bitcoin.baemail.message.presentation.entity

import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.message.data.entity.Filter

data class MessagesStateUi(
    val filter: Filter,
    val items: List<AdapterType>,
    val hasNextPage: Boolean,
    val isRefreshing: Boolean
)