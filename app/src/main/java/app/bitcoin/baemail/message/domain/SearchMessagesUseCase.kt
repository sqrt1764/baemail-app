package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.message.data.entity.Filter
import app.bitcoin.baemail.message.domain.entity.MessagesState
import com.dropbox.android.external.store4.ResponseOrigin
import com.dropbox.android.external.store4.StoreResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber

class SearchMessagesUseCaseImpl(
    private val baemailRepository: BaemailRepository,
    private val dispatcher: CoroutineDispatcher
): SearchMessagesUseCase {

    override fun invoke(filter: Filter): Flow<MessagesState> {
        val results = baemailRepository.getMessages(filter)

        return flow {
            var currentItems = listOf<DecryptedBaemailMessage>()
            var isRefreshing = false
            var hasNextPage = false

            results.flowOn(dispatcher)
                .collect {
                    when (it) {
                        is StoreResponse.Loading -> {
                            isRefreshing = true
                        }
                        is StoreResponse.Data<*> -> {
                            if (it.origin == ResponseOrigin.Fetcher) {
                                isRefreshing = false
                            } else if (it.origin == ResponseOrigin.SourceOfTruth
                                || it.origin == ResponseOrigin.Cache) {
                                isRefreshing = false
                            }

                            val data = it.requireData()

                            currentItems = data.messages
                            hasNextPage = data.hasMorePages

                        }
                        is StoreResponse.Error -> {
                            if (it.origin == ResponseOrigin.Fetcher) {
                                isRefreshing = false
                            }
                            Timber.e(it.errorMessageOrNull())
                        }
                        is StoreResponse.NoNewData -> {
                        }
                    }

                    emit(
                        MessagesState(
                            filter,
                            currentItems,
                            hasNextPage,
                            isRefreshing
                        )
                    )
                }
        }.flowOn(dispatcher)
    }
}

interface SearchMessagesUseCase {
    operator fun invoke(filter: Filter): Flow<MessagesState>
}