package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.message.data.entity.MessageBucket
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull


class UnarchiveBaemailMessageUseCaseImpl(
    val baemailRepository: BaemailRepository,
    val getActivePaymailUseCase: GetActivePaymailUseCase
) : UnarchiveBaemailMessageUseCase {

    override suspend fun invoke(message: DecryptedBaemailMessage) {
        if (message.bucket != MessageBucket.ARCHIVE) throw RuntimeException()

        val paymail = getActivePaymailUseCase().mapNotNull { it }.first().paymail
            ?: throw RuntimeException()

        baemailRepository.unarchiveMessage(paymail, message.txId)
    }
}

interface UnarchiveBaemailMessageUseCase {
    operator suspend fun invoke(message: DecryptedBaemailMessage)
}