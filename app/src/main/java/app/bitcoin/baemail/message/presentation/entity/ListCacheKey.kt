package app.bitcoin.baemail.message.presentation.entity

import app.bitcoin.baemail.message.data.entity.MessageBucket
import app.bitcoin.baemail.message.data.entity.SortType
import java.io.Serializable

data class ListCacheKey(
    val paymail: String,
    val query: String?,
    val sort: SortType,
    val bucket: MessageBucket
): Serializable