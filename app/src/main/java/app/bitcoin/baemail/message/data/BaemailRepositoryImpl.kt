package app.bitcoin.baemail.message.data

import app.bitcoin.baemail.core.data.room.dao.DecryptedBaemailMessageDao
import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.message.data.entity.Filter
import app.bitcoin.baemail.message.data.entity.FilteredResults
import app.bitcoin.baemail.message.data.entity.MessageBucket
import app.bitcoin.baemail.message.domain.BaemailRepository
import com.dropbox.android.external.store4.StoreResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class BaemailRepositoryImpl(
    private val coroutineUtil: CoroutineUtil,
    private val dao: DecryptedBaemailMessageDao,
    draftHelper: MessageDraftStore,
    private val baemailDataSource: BaemailDataSource,
    private val fetchMessagesHelper: FetchMessagesHelper,
    private val index: MessageIndex
) : BaemailRepository {

    override val messageDraftHelper: MessageDraftStore = draftHelper

    private val activeRequestsPostReadReceipt = mutableMapOf<Pair<String, String>, Job>()

    override fun getMessages(filter: Filter): Flow<StoreResponse<FilteredResults>> {
        return fetchMessagesHelper.getMessages(filter)
    }

    override fun getMessage(ownerPaymail: String, txHash: String): Flow<DecryptedBaemailMessage?> {
        return dao.getMessageByOwnerAndTxId(ownerPaymail, txHash)
    }

    override suspend fun archiveMessage(activePaymail: String, txHash: String) = withContext(Dispatchers.IO) {
        val status = baemailDataSource.ensureBaemailLogin(activePaymail)
            ?: throw RuntimeException("failed to authenticate")

        baemailDataSource.archiveMessage(status, txHash)

        //update the db-entry
        dao.updateMessageBucket(
            status.paymail,
            txHash,
            MessageBucket.ARCHIVE
        )

        index.onArchived(txHash)
    }

    override suspend fun unarchiveMessage(activePaymail: String, txHash: String) = withContext(Dispatchers.IO) {
        val status = baemailDataSource.ensureBaemailLogin(activePaymail)
            ?: throw RuntimeException("failed to authenticate")

        baemailDataSource.unarchiveMessage(status,  txHash)

        //update the db-entry
        dao.updateMessageBucket(
            status.paymail,
            txHash,
            MessageBucket.INBOX
        )

        index.onUnarchived(txHash)
    }

    override suspend fun deleteMessage(activePaymail: String, txHash: String) = withContext(Dispatchers.IO) {
        val status = baemailDataSource.ensureBaemailLogin(activePaymail)
            ?: throw RuntimeException("failed to authenticate")

        baemailDataSource.deleteMessage(status, txHash)

        //remove the db-entry
        dao.deleteMessageForOwner(
            status.paymail,
            txHash
        )

        index.onDeleted(txHash)
    }

    override suspend fun requestPostReadReceipt(
        txHash: String,
        seenByPaymail: String
    ) {
        Timber.d("...requestPostReadReceipt txHash:$txHash")

        activeRequestsPostReadReceipt[Pair(txHash, seenByPaymail)]?.let {
            //the app is already in the process of doing this
            return
        }

        val postReceiptJob = coroutineUtil.appScope.launch(Dispatchers.IO) {
            try {
                val status = baemailDataSource.ensureBaemailLogin(seenByPaymail)
                    ?: throw RuntimeException("failed to authenticate")

                baemailDataSource.postMessageReadReceipt(status, txHash)

                dao.updateMessageAsSeen(
                    seenByPaymail,
                    txHash
                )
                Timber.d("Read receipt updated ... txId:$txHash seenByPaymail:$seenByPaymail")


            } finally {
                //assuming success
                activeRequestsPostReadReceipt.remove(Pair(txHash, seenByPaymail))
                Timber.d("Read receipt job cleaned-up... txId:$txHash seenByPaymail:$seenByPaymail")

            }
        }

        activeRequestsPostReadReceipt[Pair(txHash, seenByPaymail)] = postReceiptJob
    }


}