package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.data.nodejs.request.CreateBaemailMessageTx
import app.bitcoin.baemail.core.domain.entities.ReceivedCoin
import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.domain.repository.BsvStatusRepository
import app.bitcoin.baemail.core.domain.repository.CoinRepository
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.domain.repository.PaymailInfoRepository
import app.bitcoin.baemail.core.domain.repository.SecureDataRepository
import app.bitcoin.baemail.core.domain.usecases.CheckPaymailExistsUseCase
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.core.domain.usecases.GetBlockStatsFeeUseCase
import app.bitcoin.baemail.core.domain.usecases.GetExchangeRateUseCase
import app.bitcoin.baemail.core.domain.usecases.GetFundingCoinsUseCase
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.SignMessageUseCase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull
import timber.log.Timber
import java.util.Locale

class SendBaemailMessageUseCaseImpl(
    private val coinRepository: CoinRepository,
    private val secureDataRepository: SecureDataRepository,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val bsvStatusRepository: BsvStatusRepository,
    private val paymailInfoRepository: PaymailInfoRepository,
    private val signMessageUseCase: SignMessageUseCase,
    private val getActivePaymailUseCase: GetActivePaymailUseCase,
    private val getFundingCoinsUseCase: GetFundingCoinsUseCase,
    private val checkPaymailExistsUseCase: CheckPaymailExistsUseCase,
    private val getBlockStatsFeeUseCase: GetBlockStatsFeeUseCase,
    private val getExchangeRateUseCase: GetExchangeRateUseCase,
): SendBaemailMessageUseCase {

    override suspend fun invoke(
        destinationPaymail: String,
        subject: String,
        content: String,
        threadId: String?,
        payAmountUsd: Float
    ) {
        bsvStatusRepository.liveBlockInfo.first() ?: throw ErrorNotReady()

        val paymailExistence = checkPaymailExistsUseCase(destinationPaymail).mapNotNull {
            if (it is PaymailExistence.Checking) return@mapNotNull null
            else it
        }.first()
        if (paymailExistence !is PaymailExistence.Valid) throw ErrorPaymailDoesNotExist()


        //wrap in html
        val formattedContent = content.let {
            val built = it.split("\n").map { line ->
                val sb = StringBuilder()
                    .append("<div>")
                    .append(line)
                    .append("<br></div>")
                sb
            }

            StringBuilder().let { sb ->
                built.forEach { line ->
                    sb.append(line)
                }

                sb.toString()
            }
        }

        val sendingPaymail = getActivePaymailUseCase().mapNotNull { it?.paymail }.first()
        val sendingPaymailWalletSeed = secureDataRepository.getPaymailWalletSeed(sendingPaymail)

        val fundingCoins = getFundingCoinsUseCase().first()

        val fee = getBlockStatsFeeUseCase().first()

        val exchangeRate = getExchangeRateUseCase.exchangeRate.first()

        val pkiPath = secureDataRepository.getPathForPaymailMnemonic(sendingPaymail)

        val sendingPaymailModel = CreateBaemailMessageTx.SendingPaymail(
            sendingPaymail,
            sendingPaymailWalletSeed,
            pkiPath
        )


        val availableCoinsMap: Map<String, ReceivedCoin> = fundingCoins.let {
            val map = mutableMapOf<String, ReceivedCoin>()

            it.forEach { c ->
                map[c.derivationPath] = ReceivedCoin(
                    c.txId,
                    c.sats,
                    c.outputIndex,
                    c.address
                )
            }

            map
        }

        val fundingWalletSeed = secureDataRepository.getFundingWalletSeed(sendingPaymail)

        val fundingPath = secureDataRepository.getPathForFundingMnemonic(sendingPaymail)

        val destinationAddressList = coinRepository
            .getUpcomingUnusedAddresses(sendingPaymail, 5)
            .first()


        val fundingWalletModel = CreateBaemailMessageTx.FundingWallet(
            fundingWalletSeed,
            fundingPath,
            availableCoinsMap,
            destinationAddressList[0].address
        )

        //need to drop the extra decimals after the relevant ones
        val usdToPaymailString = String.format(Locale.US, "%.2f", payAmountUsd)
        val usdToPaymail = usdToPaymailString.toFloat()

        val feeUsd = 0.01f
        val feeSats = (feeUsd * 100 * exchangeRate.satsInACent).toInt()



        val destinationPaymailOutputScript = paymailInfoRepository.getPaymailDestinationOutput(
            sendingPaymail,
            destinationPaymail
        ) { paymail, message ->
            signMessageUseCase(
                paymail,
                message
            )?.signature
        } ?: throw ErrorGettingDestinationOutput()


        val messageModel = CreateBaemailMessageTx.Message(
            threadId,
            destinationPaymail,
            paymailExistence.pubKey,
            subject,
            formattedContent,
            usdToPaymail,
            exchangeRate.satsInACent,
            fee.satsPerByte,
            destinationPaymailOutputScript,
            feeSats // fee to baemail
        )


        val tx = nodeRuntimeRepository.createBaemailMessageTx(
            sendingPaymailModel,
            fundingWalletModel,
            messageModel
        ) ?: throw ErrorConstructingTx()
        Timber.d("tx created; hex: $tx")


        //give the tx to baemail-indexing-serverside as an optimisation;
        // it takes a few seconds for it to intercept it and process it
        val success = bsvStatusRepository.sendTxToBaemail(tx.hexTx)
        if (!success) {
            throw TxNotAcceptedByBaemail()
        }

        //give the tx to miners
        val txIsValid = bsvStatusRepository.sendTx(tx.hexTx)
        if (!txIsValid) {
            throw TxNotAcceptedByMiner()
        }

        // mark coin as spent by updating the db
        val spentCoins = tx.spentCoinList.map { spent ->
            fundingCoins.find { coin ->
                spent.address == coin.address &&
                        spent.txId == coin.txId &&
                        spent.index == coin.outputIndex
            }!!
        }

        coinRepository.markCoinsSpent(spentCoins)

    }

}

interface SendBaemailMessageUseCase {
    suspend operator fun invoke(
        destinationPaymail: String,
        subject: String,
        content: String,
        threadId: String?,
        payAmountUsd: Float
    )
}

class ErrorNotReady : Exception()
class ErrorPaymailDoesNotExist : Exception()
class ErrorGettingDestinationOutput : Exception()
class ErrorConstructingTx : Exception()
class TxNotAcceptedByBaemail : Exception()
class TxNotAcceptedByMiner : Exception()