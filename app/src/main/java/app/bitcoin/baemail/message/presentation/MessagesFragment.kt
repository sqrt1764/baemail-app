package app.bitcoin.baemail.message.presentation

import android.graphics.Color
import android.graphics.Outline
import android.graphics.Rect
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.os.Parcelable
import android.text.TextPaint
import android.view.*
import android.widget.*
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableOval
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.view.recycler.ATMessage0
import app.bitcoin.baemail.core.presentation.view.recycler.ATSectionTitle
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.message.data.entity.MessageBucket
import app.bitcoin.baemail.message.data.entity.SortType
import app.bitcoin.baemail.twetch.ui.TwetchFilesMimeTypeHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.recycler.ATContentLoading0
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import app.bitcoin.baemail.message.presentation.entity.BaemailRecyclerStateCache
import app.bitcoin.baemail.message.presentation.entity.ListCacheKey
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.math.max
import kotlin.math.min


class MessagesFragment : Fragment(R.layout.fragment_messages) {

    companion object {
//        const val KEY_RECYCLER = "recycler"

        //TODO LOCALIZE
        private val LABEL_VALUE_SORT_TYPE_VALUE = "Value"
        private val LABEL_VALUE_SORT_TYPE_TIME = "Time"

        const val KEY_BUCKET = "bucket"
        const val KEY_INITIAL_SORT = "initial_sort"
        const val KEY_IS_SORT_LOCKED = "is_sort_locked"
    }

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant

    private lateinit var messagesViewModel: MessagesViewModel

    private lateinit var swipeRefresh: SwipeRefreshLayout

    private lateinit var recycler: RecyclerView
    private lateinit var appBar: AppBarLayout

    private lateinit var searchLayout: FrameLayout
    private lateinit var searchView: SearchView
    private lateinit var cancelSearch: TextView
    private lateinit var clearSearch: ImageView

    private lateinit var searchSort: TextView

    private lateinit var newMessage: FloatingActionButton

    private lateinit var tabInbox: TextView
    private lateinit var tabSent: TextView
    private lateinit var tabArchive: TextView

    private lateinit var layoutManager: LinearLayoutManager

    private val recyclerStateCache = mutableMapOf<ListCacheKey, Parcelable>()
    private var lastListCacheKey: ListCacheKey? = null

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    private lateinit var adapter: BaseListAdapter

//    //list-item side-swipe
//    private lateinit var swipeController: SwipeController
//    private lateinit var itemTouchHelper: ItemTouchHelper

    private val colorTabActive by lazy {
        requireContext().getColorFromAttr(R.attr.colorDanger)
    }
    private val colorTabInactive by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }

    private lateinit var drawableBackgroundInbox: DrawableOval
    private lateinit var drawableBackgroundSent: DrawableOval
    private lateinit var drawableBackgroundArchive: DrawableOval

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bucket = requireArguments().getString(KEY_BUCKET)?.let {
            MessageBucket.parse(it)
        } ?: let {
            Timber.d("MESSAGE FRAGMENT WAS MISSING INIT PARAM")
            MessageBucket.INBOX
        }

        val initialSort = requireArguments().getString(KEY_INITIAL_SORT)?.let {
            SortType.parse(it)
        } ?: let {
            Timber.d("MESSAGE FRAGMENT WAS MISSING INIT PARAM")
            SortType.VALUE
        }

        AndroidSupportInjection.inject(this)

//        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_memories)


        messagesViewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[MessagesViewModel::class.java]

        messagesViewModel.init(bucket, initialSort)

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (view == null) return

        //outState.putParcelable(KEY_RECYCLER, BaemailRecyclerStateCache(recyclerStateCache))
        messagesViewModel.persistListScrollState(BaemailRecyclerStateCache(recyclerStateCache))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


//        savedInstanceState?.let {bundle ->
//            val cache = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
//                bundle.getParcelable(KEY_RECYCLER, BaemailRecyclerStateCache::class.java)
//            } else {
//                bundle.getParcelable(KEY_RECYCLER)
//            }
//
//            cache?.let {
//                Timber.d("...restoring recycler state")
//                recyclerStateCache.clear()
//                recyclerStateCache.putAll(it.map)
//            }
//        }

        messagesViewModel.getPersistedListScrollState()?.let {
            if (it is BaemailRecyclerStateCache) {
                recyclerStateCache.clear()
                recyclerStateCache.putAll(it.map)
            }
        }

        val isFirstCreate = savedInstanceState == null

        swipeRefresh = requireView().findViewById(R.id.swipe_refresh)
        recycler = requireView().findViewById(R.id.recycler)
        appBar = requireView().findViewById(R.id.app_bar)
        newMessage = requireView().findViewById(R.id.new_message)
        searchView = requireView().findViewById(R.id.search_view)
        searchLayout = requireView().findViewById(R.id.search_layout)
        cancelSearch = requireView().findViewById(R.id.cancel_search)
        clearSearch = requireView().findViewById(R.id.clear_search)
        searchSort = requireView().findViewById(R.id.search_sort)
        tabInbox = requireView().findViewById(R.id.tab_inbox)
        tabSent = requireView().findViewById(R.id.tab_sent)
        tabArchive = requireView().findViewById(R.id.tab_archive)


        val adapterTypeMessageListener = object : BaseListAdapter.ATMessageListener() {
            override fun onItemClicked(adapterPosition: Int, model: ATMessage0) {
                val txId = model.message?.txId ?: return

                val args = Bundle().also {
                    it.putString(BaemailMessageFragment.KEY_TX_ID, txId)
                }
                findNavController().navigate(
                    R.id.action_memoriesFragment_to_baemailMessageFragment,
                    args
                )
            }

            override fun onActionClicked(
                adapterPosition: Int,
                model: ATMessage0,
                actionId: Int
            ) {
                when (actionId) {
                    ACTION_START_0 -> {
                        Toast.makeText(requireContext(), "A1", Toast.LENGTH_SHORT).show()
                    }
                    ACTION_START_1 -> {
                        Toast.makeText(requireContext(), "A2", Toast.LENGTH_SHORT).show()
                    }
                    ACTION_END_0 -> {
                        Toast.makeText(requireContext(), "B1", Toast.LENGTH_SHORT).show()
                    }
                    ACTION_END_1 -> {
                        Toast.makeText(requireContext(), "B2", Toast.LENGTH_SHORT).show()
                    }
                    ACTION_END_2 -> {
                        Toast.makeText(requireContext(), "B3", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        val contentLoadingListener = object : BaseListAdapter.ATContentLoadingListener {
            override fun onBound() {
                Timber.d("onBound of ATContentLoadingListener")
                messagesViewModel.onSignalRequestNextPage()
            }

        }

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atMessageListener = adapterTypeMessageListener,
                atContentLoadingListener = contentLoadingListener
            )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER

        val dp = resources.displayMetrics.density
        val topBarElevation = resources.getDimension(R.dimen.messages_fragment__top_bar_elevation)

        if (isFirstCreate) {
            appBar.doOnPreDraw {
                appBar.elevation = 0f
            }
        }

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                when (layoutManager.findFirstCompletelyVisibleItemPosition()) {
                    RecyclerView.NO_POSITION -> false
                    0 -> false
                    else -> true
                }.let { showShadow ->
                    if (showShadow) {
                        appBar.elevation = topBarElevation
                    } else {
                        appBar.elevation = 0f
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            }
        })

        appBar.descendantFocusability = ViewGroup.FOCUS_BLOCK_DESCENDANTS
        appBar.doOnPreDraw {
            appBar.descendantFocusability = ViewGroup.FOCUS_AFTER_DESCENDANTS
        }

//        swipeController = provideSwipeController()
//
//        itemTouchHelper = ItemTouchHelper(swipeController)
//        itemTouchHelper.attachToRecyclerView(recycler)

        val colorSelector = ContextCompat.getColor(requireContext(), R.color.selector_on_surface)

        drawableBackgroundInbox = DrawableOval(requireContext()).also {
            it.setBgColor(Color.TRANSPARENT)
            it.setStrokeColor(colorTabActive)
            it.setStrokeWidth(dp * 1.5f)
            it.setPaddings(0f, 0f, 0f, 0f)
        }
        val drawableTabInboxSelector = DrawableState.getNew(colorTabActive)

        tabInbox.clipToOutline = true
        tabInbox.outlineProvider = drawableBackgroundInbox.getOutlineProvider()
        tabInbox.background = LayerDrawable(arrayOf(
            drawableBackgroundInbox,
            drawableTabInboxSelector
        ))


        drawableBackgroundSent = DrawableOval(requireContext()).also {
            it.setBgColor(Color.TRANSPARENT)
            it.setStrokeColor(colorTabInactive)
            it.setStrokeWidth(dp * 1.5f)
            it.setPaddings(0f, 0f, 0f, 0f)
        }
        val drawableTabSentSelector = DrawableState.getNew(colorTabActive)

        tabSent.clipToOutline = true
        tabSent.outlineProvider = drawableBackgroundSent.getOutlineProvider()
        tabSent.background = LayerDrawable(arrayOf(
                drawableBackgroundSent,
                drawableTabSentSelector
        ))


        drawableBackgroundArchive = DrawableOval(requireContext()).also {
            it.setBgColor(Color.TRANSPARENT)
            it.setStrokeColor(colorTabInactive)
            it.setStrokeWidth(dp * 1.5f)
            it.setPaddings(0f, 0f, 0f, 0f)
        }
        val drawableTabArchiveSelector = DrawableState.getNew(colorTabActive)

        tabArchive.clipToOutline = true
        tabArchive.outlineProvider = drawableBackgroundArchive.getOutlineProvider()
        tabArchive.background = LayerDrawable(arrayOf(
                drawableBackgroundArchive,
                drawableTabArchiveSelector
        ))


        tabInbox.setOnClickListener {
            messagesViewModel.onBuckedChanged(MessageBucket.INBOX)
        }

        tabSent.setOnClickListener {
            messagesViewModel.onBuckedChanged(MessageBucket.SENT)
        }

        tabArchive.setOnClickListener {
            messagesViewModel.onBuckedChanged(MessageBucket.ARCHIVE)
        }


        swipeRefresh.setProgressViewOffset(true, (5 * dp).toInt(), (45 * dp).toInt())
        swipeRefresh.setOnRefreshListener {
            messagesViewModel.onSwipeToRefresh()
        }
        swipeRefresh.setColorSchemeColors(requireContext().getColorFromAttr(R.attr.colorPrimaryVariant))
        swipeRefresh.setProgressBackgroundColorSchemeColor(requireContext().getColorFromAttr(R.attr.colorSurfaceVariant))


        val pressedColor = ContextCompat
            .getColor(requireContext(), R.color.selector_on_surface)

        val colorSortBackground = requireContext().getColorFromAttr(R.attr.colorPrimary)
        //val colorSortLockedBackground = requireContext().getColorFromAttr(R.attr.colorPrimaryVariant)
        val colorSelectorOnPrimary = ContextCompat.getColor(requireContext(), R.color.selector_on_primary)


        val searchBackground = DrawableOval(
                colorSortBackground,
                0f * dp,
                requireContext().getColorFromAttr(R.attr.colorSurfaceVariant)
        )
        searchBackground.setPaddings(0f, 0f, 0f, 0f)
        searchLayout.background = LayerDrawable(arrayOf(
            searchBackground,
            DrawableState.getNew(pressedColor)
        ))
        searchLayout.clipToOutline = true
        searchLayout.outlineProvider = searchBackground.getOutlineProvider()

        searchView.isSaveEnabled = false
        searchView.setIconifiedByDefault(false)


        cancelSearch.setOnClickListener {
            messagesViewModel.onRequestClearSearchResults()
            searchView.clearFocus()
        }

        clearSearch.setOnClickListener {

            if (searchView.hasFocus()) {
                searchView.setQuery("", false)
            } else {
                messagesViewModel.onRequestClearSearchResults()
            }
        }

        searchView.setOnQueryTextListener(searchInputQueryTextListener)

        searchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
            Timber.d("searchView focus-change hasFocus:$hasFocus")

            val currentInputValue = searchView.query.toString()

            if (!hasFocus) {
                if (currentInputValue.trim().isEmpty()) {
                    initSearchBarButtons(
                        true,
                        true
                    )
                } else {
                    initSearchBarButtons(
                        true,
                        false
                    )
                }
                return@setOnQueryTextFocusChangeListener
            }


            initSearchBarButtons(
                currentInputValue.trim().isNotEmpty(),
                false
            )
        }


        val drawableBackgroundSearchSort = DrawableOval(requireContext()).also {
            it.setBgColor(colorSortBackground)
            it.setStrokeWidth(0f)
            it.setPaddings(0f, 0f, 0f, 0f)
        }
        searchSort.background = LayerDrawable(arrayOf(
                drawableBackgroundSearchSort,
                DrawableState.getNew(colorSelectorOnPrimary)
        ))
        searchSort.clipToOutline = true
        searchSort.outlineProvider = drawableBackgroundSearchSort.getOutlineProvider()

        searchSort.setOnClickListener {
            messagesViewModel.onSortTypeToggled()
        }


        //find the final width of the sort-text-view
        searchSort.layoutParams.let {
            val tp = TextPaint()
            tp.textSize = searchSort.textSize
            val widthOfLongestLabel = max(
                tp.measureText(LABEL_VALUE_SORT_TYPE_VALUE),
                tp.measureText(LABEL_VALUE_SORT_TYPE_TIME)
            )

            it.width = (widthOfLongestLabel + searchSort.paddingStart + searchSort.paddingEnd).toInt()
            searchSort.updatePadding(
                right = 0
            )

            searchSort.layoutParams = it
        }


        newMessage.setOnClickListener {
            try {
                val args = Bundle().also {
                    it.putBoolean(NewBaemailFragment.KEY_IS_NEW_REPLY, false)
                }

                findNavController().navigate(R.id.action_memoriesFragment_to_newMessageFragment, args)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }


        cancelSearch.background = DrawableState.getNew(colorSelector, chipShape = true)

        cancelSearch.clipToOutline = true
        cancelSearch.outlineProvider = object : ViewOutlineProvider() {
            val r = Rect()
            override fun getOutline(v: View?, o: Outline?) {
                if (v == null || o == null) return

                r.set(0, 0, v.width, v.height)
                o.setRoundRect(r, (min(r.height(), r.width()) / 2).toFloat())
            }
        }


        clearSearch.background = DrawableState.getNew(colorSelector, ovalShape = true)

        val colorClearTint = Color.parseColor("#777777")
        clearSearch.drawable.mutate().setTint(colorClearTint)

        clearSearch.clipToOutline = true
        clearSearch.outlineProvider = object : ViewOutlineProvider() {
            val r = Rect()
            override fun getOutline(v: View?, o: Outline?) {
                if (v == null || o == null) return

                r.set(0, 0, v.width, v.height)
                o.setOval(r)
            }
        }


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                messagesViewModel.searchResults.mapNotNull { it }.collect { messagesState ->
                    val finalItems = ArrayList(messagesState.items)

                    if (!messagesState.isRefreshing && messagesState.hasNextPage) {
                        finalItems.add(ATContentLoading0(0))
                    }

                    //todo if the message-list is empty, then show card talking about it
                    //todo
                    //todo


                    //add decor
                    finalItems.add(ATHeightDecorDynamic("0", MutableLiveData<Int>().also {
                        it.value = (resources.displayMetrics.density * 95).toInt()
                    }))



//                    swipeStateHelper.updateExpands(finalItems)

                    swipeRefresh.isRefreshing = messagesState.isRefreshing


                    //todo should title be visible when when interacting with search?
                    when (messagesState.filter.bucket) {
                        MessageBucket.INBOX -> {
                            finalItems.add(0, ATSectionTitle(R.string.messages_inbox_title))
                        }
                        MessageBucket.SENT -> {
                            finalItems.add(0, ATSectionTitle(R.string.messages_sent_title))
                        }
                        MessageBucket.ARCHIVE -> {
                            finalItems.add(0, ATSectionTitle(R.string.messages_archive_title))
                        }
                    }

                    initSort(messagesState.filter.sort)
                    initTabs(messagesState.filter.bucket)
                    initSearchView(messagesState.filter.query)

                    awaitRecyclerAnimationsFinish()

                    val newListCacheKey = ListCacheKey(
                        messagesState.filter.paymail,
                        messagesState.filter.query,
                        messagesState.filter.sort,
                        messagesState.filter.bucket
                    )
                    val lastKey = lastListCacheKey
                    val lastRecyclerState = layoutManager.onSaveInstanceState()
                    if (lastKey != null && lastRecyclerState != null) {
                        recyclerStateCache[lastKey] = lastRecyclerState
                    }
                    lastListCacheKey = newListCacheKey

                    adapter.setItems(finalItems)


                    val cacheKeyMappedRecyclerState = recyclerStateCache[newListCacheKey]
                    if (cacheKeyMappedRecyclerState != null) {
                        layoutManager.onRestoreInstanceState(cacheKeyMappedRecyclerState)
                    } else {
                        layoutManager.scrollToPosition(0)
                    }


                    if (!newMessage.isShown) {
                        newMessage.show()
                    }
                }
            }
        }

    }

    private suspend fun awaitRecyclerAnimationsFinish() {
        recycler.itemAnimator?.let { animator ->
            //making sure that the any previous animation get completed
            if (!animator.isRunning) {
                return@let
            }

            suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                animator.isRunning { cancellableContinuation.resume(Unit) }
            }
        }

        suspendCancellableCoroutine<Unit> { continuation ->
            if (recycler.isComputingLayout) {
                recycler.post {
                    continuation.resume(Unit)
                }
            } else {
                continuation.resume(Unit)
            }

        }
    }


    private fun initSort(sort: SortType?) {
        sort ?: return

        when (sort) {
            SortType.VALUE -> {
                searchSort.text = LABEL_VALUE_SORT_TYPE_VALUE
            }
            SortType.TIME -> {
                searchSort.text = LABEL_VALUE_SORT_TYPE_TIME
            }
        }
    }

    private fun initTabs(bucket: MessageBucket?) {
        bucket ?: return

        when (bucket) {
            MessageBucket.INBOX -> {
                tabInbox.setTextColor(colorTabActive)
                drawableBackgroundInbox.setStrokeColor(colorTabActive)

                tabSent.setTextColor(colorTabInactive)
                drawableBackgroundSent.setStrokeColor(colorTabInactive)

                tabArchive.setTextColor(colorTabInactive)
                drawableBackgroundArchive.setStrokeColor(colorTabInactive)
            }

            MessageBucket.ARCHIVE -> {
                tabArchive.setTextColor(colorTabActive)
                drawableBackgroundArchive.setStrokeColor(colorTabActive)

                tabInbox.setTextColor(colorTabInactive)
                drawableBackgroundInbox.setStrokeColor(colorTabInactive)

                tabSent.setTextColor(colorTabInactive)
                drawableBackgroundSent.setStrokeColor(colorTabInactive)
            }
            MessageBucket.SENT -> {
                tabSent.setTextColor(colorTabActive)
                drawableBackgroundSent.setStrokeColor(colorTabActive)

                tabInbox.setTextColor(colorTabInactive)
                drawableBackgroundInbox.setStrokeColor(colorTabInactive)

                tabArchive.setTextColor(colorTabInactive)
                drawableBackgroundArchive.setStrokeColor(colorTabInactive)
            }
        }
    }

    private fun initSearchView(query: String?) {
        val currentInputValue = searchView.query.toString()

        if (query == null) {
            initSearchBarButtons(false, true)

            searchView.setOnQueryTextListener(null)
            searchView.setQuery("", false)
            searchView.setOnQueryTextListener(searchInputQueryTextListener)

            return
        }

        initSearchBarButtons(query.isNotEmpty(), query.isEmpty())

        if (currentInputValue != query) {
            Timber.d("searchView setQuery from observer")
            searchView.setQuery(query, false)
        }
    }

    private fun initSearchBarButtons(
        nonEmptyQuery: Boolean,
        isClear: Boolean
    ) {
        if (isClear) {
            cancelSearch.visibility = View.INVISIBLE
            clearSearch.visibility = View.INVISIBLE
            return
        }

        if (!nonEmptyQuery) {
            cancelSearch.visibility = View.VISIBLE
            clearSearch.visibility = View.INVISIBLE
        } else {
            cancelSearch.visibility = View.INVISIBLE
            clearSearch.visibility = View.VISIBLE
        }
    }

//    private fun provideSwipeController(): SwipeController {
//        val swipeController = SwipeController()
//        swipeController.init(
//            allowSwipeLeft = lambda@{ holder ->
//                if (holder !is ATMessage0ViewHolder) return@lambda false
//
//                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
//                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false
//
//                if (isRightActionExpanded) {
//                    return@lambda false
//                }
//                if (isLeftActionExpanded) {
//                    return@lambda true
//                }
//
//                true
//            },
//            allowSwipeRight = lambda@{ holder ->
//                if (holder !is ATMessage0ViewHolder) return@lambda false
//
//                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
//                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false
//
//                if (isRightActionExpanded) {
//                    return@lambda true
//                }
//                if (isLeftActionExpanded) {
//                    return@lambda false
//                }
//
//                true
//            },
//            onSwiped = { holder, direction, maxNotMin ->
//                val adapterPosition = holder.bindingAdapterPosition
//
//                if (ItemTouchHelper.LEFT == direction) {
//                    swipeStateHelper.onItemSwiped(adapterPosition, true, maxNotMin)
//                } else if (ItemTouchHelper.RIGHT == direction) {
//                    swipeStateHelper.onItemSwiped(adapterPosition, false, maxNotMin)
//                }
//            },
//            swipeRightMinWidth = lambda@{ holder ->
//                if (holder !is ATMessage0ViewHolder) return@lambda SwipeController.WIDTH_MAX
//
//                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
//                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false
//
//                return@lambda if (isLeftActionExpanded) {
//                    SwipeController.WIDTH_MAX
//                } else if (isRightActionExpanded) {
//                    holder.getRightSwipeActionWidth()
//                } else {
//                    holder.getLeftSwipeActionWidth()
//                }
//
//            },
//            swipeRightMaxWidth = lambda@{ holder ->
//                if (holder !is ATMessage0ViewHolder) return@lambda SwipeController.WIDTH_MAX
//
//                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
//                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false
//
//                return@lambda if (isLeftActionExpanded) {
//                    SwipeController.WIDTH_MAX
//                } else if (isRightActionExpanded) {
//                    holder.getRightSwipeActionWidth() + holder.getLeftSwipeActionWidth()
//                } else {
//                    holder.getLeftSwipeActionWidth()
//                }
//
//            },
//            swipeLeftMinWidth = lambda@{ holder ->
//                if (holder !is ATMessage0ViewHolder) return@lambda SwipeController.WIDTH_MAX
//
//                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
//                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false
//
//                return@lambda if (isLeftActionExpanded) {
//                    holder.getLeftSwipeActionWidth()
//                } else if (isRightActionExpanded) {
//                    SwipeController.WIDTH_MAX
//                } else {
//                    holder.getRightSwipeActionWidth()
//                }
//
//            },
//            swipeLeftMaxWidth = lambda@{ holder ->
//                if (holder !is ATMessage0ViewHolder) return@lambda SwipeController.WIDTH_MAX
//
//                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
//                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false
//
//                return@lambda if (isLeftActionExpanded) {
//                    holder.getRightSwipeActionWidth() + holder.getLeftSwipeActionWidth()
//                } else if (isRightActionExpanded) {
//                    SwipeController.WIDTH_MAX
//                } else {
//                    holder.getRightSwipeActionWidth()
//                }
//
//            },
//            onSwipedAmount = lambda@{ holder, dX, dY ->
//                if (holder !is ATMessage0ViewHolder) return@lambda
//
//                holder.onSwipedAmount(dX, dY)
//            }
//        )
//
//        return swipeController
//    }

    private val searchInputQueryTextListener = object : SearchView.OnQueryTextListener {

        private fun showCancelButton() {
            cancelSearch.visibility = View.VISIBLE
        }

        private fun hideCancelButton() {
            cancelSearch.visibility = View.INVISIBLE
        }

        override fun onQueryTextChange(newText: String): Boolean {
            val wasDropped = !messagesViewModel.onQueryTextChange(newText)

            val isEmptyQuery = newText.isBlank()

            if (isEmptyQuery) {
                showCancelButton()
                clearSearch.visibility = View.INVISIBLE
            } else {
                hideCancelButton()
                clearSearch.visibility = View.VISIBLE
            }

            if (!wasDropped) {
                recycler.scrollToPosition(0)
            }

            return false
        }

        override fun onQueryTextSubmit(query: String): Boolean {
            Timber.d("onQueryTextSubmit: $query")
            searchView.clearFocus()

            val wasDropped = !messagesViewModel.onQueryTextSubmit(query)

            if (!wasDropped) {
                recycler.scrollToPosition(0)
            }

            return false
        }

    }

//    private val swipeStateHelper = SwipeStateHelper()
//
//    private inner class SwipeStateHelper {
//        private val rightExpandedMessages = HashSet<String>()
//        private val leftExpandedMessages = HashSet<String>()
//
//        fun onItemSwiped(adapterPosition: Int, leftNotRight: Boolean, maxNotMin: Boolean) {
//            val isRightExpanded = rightExpandedMessages.contains("$adapterPosition")
//            val isLeftExpanded = leftExpandedMessages.contains("$adapterPosition")
//
//            Timber.d("#onItemSwiped adapterPosition:$adapterPosition leftNotRight:$leftNotRight maxNotMin:$maxNotMin")
//
//            if (leftNotRight) {
//                if (isRightExpanded) {
//                    //do nothing
//                } else if (isLeftExpanded) {
//                    if (maxNotMin) {
//                        leftExpandedMessages.remove("$adapterPosition")
//                        rightExpandedMessages.add("$adapterPosition")
//                    } else {
//                        leftExpandedMessages.remove("$adapterPosition")
//                        rightExpandedMessages.remove("$adapterPosition")
//                    }
//                } else {
//                    leftExpandedMessages.remove("$adapterPosition")
//                    rightExpandedMessages.add("$adapterPosition")
//                }
//            } else {
//                if (isRightExpanded) {
//                    if (maxNotMin) {
//                        leftExpandedMessages.add("$adapterPosition")
//                        rightExpandedMessages.remove("$adapterPosition")
//                    } else {
//                        leftExpandedMessages.remove("$adapterPosition")
//                        rightExpandedMessages.remove("$adapterPosition")
//                    }
//                } else if (isLeftExpanded) {
//                    //do nothing
//                } else {
//                    leftExpandedMessages.add("$adapterPosition")
//                    rightExpandedMessages.remove("$adapterPosition")
//                }
//            }
//
//
//            invalidateItem(adapterPosition)
//        }
//
//        fun invalidateItem(adapterPosition: Int) {
//            val model = adapter.getItemOfPosition(adapterPosition)
//            if (model !is ATMessage0) return
//
//            model.isLeftActionExpanded = leftExpandedMessages.contains("$adapterPosition")
//            model.isRightActionExpanded = rightExpandedMessages.contains("$adapterPosition")
//
//            val holder = recycler.findViewHolderForAdapterPosition(adapterPosition) as ATMessage0ViewHolder
//                ?: return
//
//            //update the views of the itemView so they apply the correct expand modifiers
//            holder.onBindViewHolder(model, holder.listener)
//
//            //alternative to the following .. adapter.notifyItemChanged(adapterPosition)
//            //hack .. force clearing of the swiping-touch-helper
//            recycler.findViewHolderForAdapterPosition(adapterPosition)?.itemView?.let {
//                itemTouchHelper.onChildViewDetachedFromWindow(it)
//                itemTouchHelper.onChildViewAttachedToWindow(it)
//            }
//        }
//
//        fun updateExpands(list: List<AdapterType>) {
//            list.forEachIndexed { i, item ->
//                if (item is ATMessage0) {
//                    item.isLeftActionExpanded = leftExpandedMessages.contains("$i")
//                    item.isRightActionExpanded = rightExpandedMessages.contains("$i")
//                }
//            }
//        }
//    }

}