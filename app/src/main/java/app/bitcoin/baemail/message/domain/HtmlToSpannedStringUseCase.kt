package app.bitcoin.baemail.message.domain

import android.os.Build
import android.text.Html

class HtmlToSpannedStringUseCaseImpl() : HtmlToSpannedStringUseCase {
    override fun invoke(html: String): String {
        val spannedContent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(html)
        }
        return spannedContent.toString().replace("\n", " ").trim()
    }

}

interface HtmlToSpannedStringUseCase {
    operator fun invoke(html: String): String
}