package app.bitcoin.baemail.message.domain

class EstimateBaemailMessageTxSizeImpl() : EstimateBaemailMessageTxSize {
    override fun invoke(
        sendingPaymail: String,
        destinationPaymail: String,
        subject: String,
        content: String
    ): Int {
        val lengthOfCcPaymails = 0 //todo
        val headerChunkSize = 60 + destinationPaymail.length +
                lengthOfCcPaymails +
                10 + subject.length +
                10 + sendingPaymail.length + //for name of sender
                10 + sendingPaymail.length + //for paymail of sender
                50

        val countOfCcs = 0
        val messageChunkCount = 2 + countOfCcs
        val messageChunkSize = (content.length + 20) * messageChunkCount

        return 50 + headerChunkSize + messageChunkSize +
                50 +
                50 + //hash of header
                50 + //sender-signed hash of header
                50 + //sendingPaymailPkiPublicKey
                sendingPaymail.length +
                100 + //baemail pay output
                100 + //app pay output
                100 + //destination pay output
                200 + //tx-inputs
                50 //general tx-decorations
    }

}

interface EstimateBaemailMessageTxSize {
    operator fun invoke(
        sendingPaymail: String,
        destinationPaymail: String,
        subject: String,
        content: String
    ): Int
}