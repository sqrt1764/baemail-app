package app.bitcoin.baemail.message.domain

import android.content.Context
import android.text.format.DateFormat

class FormatMessageTimeUseCaseImpl(
    val context: Context
) : FormatMessageTimeUseCase {

    private val messageTimeFormat = DateFormat.getTimeFormat(context)

    override fun invoke(createdAtMillis: Long): String {
        return messageTimeFormat.format(createdAtMillis)
    }

}

interface FormatMessageTimeUseCase {
    operator fun invoke(createdAtMillis: Long): String
}