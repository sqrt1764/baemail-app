package app.bitcoin.baemail.message.domain

import com.google.gson.Gson
import com.google.gson.JsonObject
import java.lang.Exception

class ParseDecryptedMessageContentImpl(
    private val gson: Gson
) : ParseDecryptedMessageContent {
    override fun invoke(decryptedMessage: String): String {
        return try {
            val json = gson.fromJson(decryptedMessage, JsonObject::class.java)
            val bodyJson = json.getAsJsonObject("body")
            val blocksJson = bodyJson.get("blocks")
            val blocks = if (!blocksJson.isJsonPrimitive) {
                blocksJson.toString()
            } else {
                blocksJson.asString
            }

            blocks
        } catch (e: Exception) {
            "-----------------"
        }
    }
}

interface ParseDecryptedMessageContent {
    operator fun invoke(decryptedMessage: String): String
}