package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.message.data.entity.MessageBucket
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull

class PostBaemailReadReceiptUseCaseImpl(
    private val baemailRepository: BaemailRepository,
    val getActivePaymailUseCase: GetActivePaymailUseCase
) : PostBaemailReadReceiptUseCase {

    override suspend fun invoke(message: DecryptedBaemailMessage) {
        if (!message.destinationSeen && MessageBucket.INBOX == message.bucket) {
            val paymail = getActivePaymailUseCase().mapNotNull { it }.first().paymail
                ?: throw RuntimeException()

            baemailRepository.requestPostReadReceipt(message.txId, paymail)
        }
    }

}

interface PostBaemailReadReceiptUseCase {
    suspend operator fun invoke(message: DecryptedBaemailMessage)
}