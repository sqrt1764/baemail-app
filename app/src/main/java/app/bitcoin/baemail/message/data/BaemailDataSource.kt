package app.bitcoin.baemail.message.data

import app.bitcoin.baemail.core.data.net.AppApiService
import app.bitcoin.baemail.core.data.room.dao.DecryptedBaemailMessageDao
import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.core.data.util.AuthenticatedPaymail
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.message.data.entity.BaemailState
import app.bitcoin.baemail.message.data.entity.MessageBucket
import app.bitcoin.baemail.message.data.entity.QueriedBaemailMessage
import app.bitcoin.baemail.message.data.entity.SortType
import app.bitcoin.baemail.message.data.entity.Filter
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withContext
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import java.net.HttpURLConnection
import kotlin.coroutines.resume

class BaemailDataSource(
    private val gson: Gson,
    private val apiService: AppApiService,
    private val pageSize: Int,
    private val dao: DecryptedBaemailMessageDao,
    private val secureDataSource: SecureDataSource,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val baemailStateStore: BaemailStateStore,
) {

    suspend fun ensureBaemailLogin(paymail: String?): BaemailState? {
        Timber.d("ensureBaemailLogin: $paymail")

        paymail ?: return null

        if (baemailStateStore.hasState(paymail)) {
            Timber.d("read persisted state")
            return baemailStateStore.requireStateForPaymail(paymail)
        }


        val state = executeLogin(paymail) ?: let {
            Timber.d("error logging in")
            return null
        }

        baemailStateStore.writeState(state)
        return state
    }


    private suspend fun clearBaemailLogin(paymail: String) {
        baemailStateStore.clearState(paymail)
    }


    fun archiveMessage(status: BaemailState, txHash: String) {
        val reqJson: JsonObject = JsonObject().let {
            it.addProperty("code", status.token)
            it.addProperty("pki", status.paymailPkiPublicKey)
            it.addProperty("paymail", status.paymail)
            it.addProperty("txid", txHash)

            it
        }

        var isUpdated = false

        try {
            val call = apiService.post(
                "https://baemail.me/api/set/archived",
                reqJson.toString().toRequestBody(),
                status.session
            )
            val response = call.execute()
            val responseBody = response.body()

            if (response.isSuccessful && responseBody != null) {
                //parse the body
                val jsonResponse = gson.fromJson(
                    String(responseBody.bytes()),
                    JsonObject::class.java
                )

                isUpdated = jsonResponse.get("updated").asBoolean

            }

        } catch (e: Exception) {
            Timber.e(e)
            isUpdated = false
        }

        if (!isUpdated) throw RuntimeException()
    }

    fun unarchiveMessage(status: BaemailState, txHash: String) {
        val reqJson: JsonObject = JsonObject().let {
            it.addProperty("code", status.token)
            it.addProperty("pki", status.paymailPkiPublicKey)
            it.addProperty("paymail", status.paymail)
            it.addProperty("txid", txHash)

            it
        }

        var isUpdated = false

        try {
            val call = apiService.post(
                "https://baemail.me/api/set/unarchived",
                reqJson.toString().toRequestBody(),
                status.session
            )
            val response = call.execute()
            val responseBody = response.body()

            if (response.isSuccessful && responseBody != null) {
                //parse the body
                val jsonResponse = gson.fromJson(
                    String(responseBody.bytes()),
                    JsonObject::class.java
                )
                isUpdated = jsonResponse.get("updated").asBoolean
            }

        } catch (e: Exception) {
            Timber.e(e)
            isUpdated = false
        }

        if (!isUpdated) throw RuntimeException()
    }

    fun deleteMessage(status: BaemailState, txHash: String) {
        val reqJson: JsonObject = JsonObject().let {
            it.addProperty("code", status.token)
            it.addProperty("pki", status.paymailPkiPublicKey)
            it.addProperty("paymail", status.paymail)
            it.addProperty("txid", txHash)

            it
        }

        var isUpdated = false

        try {
            val call = apiService.post(
                "https://baemail.me/api/set/deleted",
                reqJson.toString().toRequestBody(),
                status.session
            )
            val response = call.execute()
            val responseBody = response.body()

            if (response.isSuccessful && responseBody != null) {
                //parse the body
                val bodyString = String(responseBody.bytes())
                val jsonResponse = gson.fromJson(
                    bodyString,
                    JsonObject::class.java
                )

                isUpdated = jsonResponse.get("updated").asBoolean //true

            }

        } catch (e: Exception) {
            Timber.e(e)
            isUpdated = false
        }

        if (!isUpdated) throw RuntimeException()
    }


    fun postMessageReadReceipt(status: BaemailState, txHash: String) {
        val jsonReq = JsonObject()
        jsonReq.addProperty("code", status.token)
        jsonReq.addProperty("txid", txHash)
        jsonReq.addProperty("paymail", status.paymail)
        jsonReq.addProperty("pki", status.paymailPkiPublicKey)

        var isUpdated = false

        try {
            val call = apiService.post(
                "https://baemail.me/api/set/seen",
                jsonReq.toString().toRequestBody(),
                status.session
            )
            val response = call.execute()

            val responseBody = response.body()

            if (response.isSuccessful && responseBody != null) {

                //parse the body

                val jsonResponse = gson.fromJson(
                    String(responseBody.bytes()),
                    JsonObject::class.java
                )

                if (jsonResponse.get("updated").asBoolean) {
                    isUpdated = true
                }

            }

        } catch (e: Exception) {
            Timber.e(e)
            isUpdated = false
        }

        if (!isUpdated) throw RuntimeException()
    }


    suspend fun fetchAndHandlePageOfBaemailMessages(
        key: Filter
    ): List<DecryptedBaemailMessage> {

        val fetched = try {
            requestPageOfBaemailMessages(key)
        } catch (e: HttpUnauthorizedException) {
            clearBaemailLogin(key.paymail)
            requestPageOfBaemailMessages(key)
        }

        if (fetched.isEmpty()) return emptyList()

        return processQueriedMessages(key, fetched)
    }

    private suspend fun requestPageOfBaemailMessages(
        key: Filter
    ): List<QueriedBaemailMessage> {


        val status = ensureBaemailLogin(key.paymail)

        status ?: let {
            Timber.d("status == null")
            return emptyList()
        }



        val (endpoint, reqJson) = buildReqJsonOfBaemailMessagesPage(
            status.token,
            status.paymail,
            key
        )


        Timber.d(/*RuntimeException(), */"pre request to:$endpoint bucket:${key.bucket} json:$reqJson")

        val inboxPage: List<QueriedBaemailMessage>? = suspendCancellableCoroutine { cancellableContinuation ->
            try {
                val call = apiService.post(
                    endpoint,
                    reqJson.toString().toRequestBody(),
                    status.session
                )
                val response = call.execute()

                val responseBody = response.body()

                if (response.isSuccessful && responseBody != null) {

                    //parse the body
                    val payload = String(responseBody.bytes())
                    val jsonElement = gson.fromJson(
                        payload,
                        JsonElement::class.java
                    )

                    val jsonMessageArray = if (jsonElement.isJsonArray) {
                        jsonElement.asJsonArray
                    } else {
                        Timber.d("... $payload")
                        throw Exception()
                    }

                    val list = mutableListOf<QueriedBaemailMessage>()
                    (0 until jsonMessageArray.size()).map { i ->
                        QueriedBaemailMessage.parse(gson, jsonMessageArray[i].asJsonObject)
                    }.toCollection(list)

                    cancellableContinuation.resume(list)

                    return@suspendCancellableCoroutine
                } else if (HttpURLConnection.HTTP_UNAUTHORIZED == response.code()) {
                    throw HttpUnauthorizedException()
                }

            } catch (e: HttpUnauthorizedException) {
                throw e
            } catch (e: Exception) {
                Timber.e(e)
            }

            cancellableContinuation.resume(null)
        }



        inboxPage?.let {
            Timber.d("page: ${key.pages} ..messages: ${it.size}")
        } ?: let {
            return emptyList()
        }



        return inboxPage
    }

    private fun buildReqJsonOfBaemailMessagesPage(
        token: String,
        paymail: String,
        key: Filter
    ): Pair<String, JsonObject> {

        val limit = pageSize
        val skip = key.pages * limit

        val reqJson = JsonObject()
        reqJson.addProperty("code", token)
        reqJson.addProperty("paymail", paymail)
        reqJson.addProperty("limit", limit)
        reqJson.addProperty("skip", skip)
        reqJson.addProperty("search", key.query ?: "")

        var endpoint = ""

        if (!key.query.isNullOrBlank()) {
            // search endpoint
            endpoint = "https://baemail.me/api/get/search"


            //todo something is wrong in web-baemail
            when (key.bucket) {
                MessageBucket.INBOX -> {
                    //searching in inbox
                    when (key.sort) {
                        SortType.VALUE -> {
                            val sortJson = JsonObject()
                            sortJson.addProperty("status.0.seen", 1)
                            sortJson.addProperty("baemailData.amountUsd", -1)
                            sortJson.addProperty("createdAt", -1)

                            reqJson.add("sort", sortJson)
                        }
                        SortType.TIME -> {
                            val sortJson = JsonObject()
                            sortJson.addProperty("createdAt", -1)

                            reqJson.add("sort", sortJson)
                        }
                    }


                }
                MessageBucket.SENT -> {
                    //searching in sent

                    //ignores sort
                    val sortJson = JsonObject()
                    sortJson.addProperty("createdAt", -1)

                    reqJson.add("sort", sortJson)

                }
                MessageBucket.ARCHIVE -> {
                    //searching in archive

                    when (key.sort) {
                        SortType.VALUE -> {
                            val sortJson = JsonObject()
                            sortJson.addProperty("status.0.seen", 1)
                            sortJson.addProperty("baemailData.amountUsd", -1)
                            sortJson.addProperty("createdAt", -1)

                            reqJson.add("sort", sortJson)
                        }
                        SortType.TIME -> {
                            val sortJson = JsonObject()
                            sortJson.addProperty("createdAt", -1)

                            reqJson.add("sort", sortJson)
                        }
                    }

                }
            }


            //todo
            //todo
            //todo
            //todo


        } else if (MessageBucket.INBOX == key.bucket) {

            endpoint = "https://baemail.me/api/get/inbox"


            when (key.sort) {
                SortType.VALUE -> {
                    val sortJson = JsonObject()
                    sortJson.addProperty("status.0.seen", 1)
                    sortJson.addProperty("baemailData.amountUsd", -1)
                    sortJson.addProperty("createdAt", -1)

                    reqJson.add("sort", sortJson)
                }
                SortType.TIME -> {
                    val sortJson = JsonObject()
                    sortJson.addProperty("createdAt", -1)

                    reqJson.add("sort", sortJson)
                }
            }


        } else if (MessageBucket.SENT == key.bucket) {
            endpoint = "https://baemail.me/api/get/sent"


            val sortJson = JsonObject()
            sortJson.addProperty("createdAt", -1)

            reqJson.add("sort", sortJson)


        } else if (MessageBucket.ARCHIVE == key.bucket) {
            endpoint = "https://baemail.me/api/get/archive"

            when (key.sort) {
                SortType.VALUE -> {
                    val sortJson = JsonObject()
                    sortJson.addProperty("status.0.seen", 1)
                    sortJson.addProperty("baemailData.amountUsd", -1)
                    sortJson.addProperty("createdAt", -1)

                    reqJson.add("sort", sortJson)
                }
                SortType.TIME -> {
                    val sortJson = JsonObject()
                    sortJson.addProperty("createdAt", -1)

                    reqJson.add("sort", sortJson)
                }
            }
        }


        return endpoint to reqJson
    }

    private suspend fun processQueriedMessages(
        key: Filter,
        fetched: List<QueriedBaemailMessage>
    ): List<DecryptedBaemailMessage> {

        //the content of the retrieved messages must be decrypted
        val toBeDecryptedMessageMap = mutableMapOf<String, String>()
        fetched.forEach {
            toBeDecryptedMessageMap[it.txId] = it.getMessageCypherText(key.paymail)
        }
        //avoid re-decrypting messages
        val messageTxIdList = fetched.map { it.txId }
        val foundMatchingMessages = dao.queryMessagesByOwnerAndTxId(key.paymail, messageTxIdList)
            .let { list ->
                val map = mutableMapOf<String, DecryptedBaemailMessage>()

                list.forEach {
                    map[it.txId] = it
                }

                map
            }
        foundMatchingMessages.forEach {
            toBeDecryptedMessageMap.remove(it.key)
        }

        val decryptedMessageContent = decryptBaemailMessages(toBeDecryptedMessageMap, key.paymail)
        //normalize decrypted-message-map by re-adding messages that were already decrypted
        foundMatchingMessages.forEach {
            decryptedMessageContent[it.key] = it.value.decryptedMessage
        }

        return fetched.map {
            val decryptedContent = decryptedMessageContent[it.txId]!!

            val numAmountUsd = try {
                it.head.amountUsd.toFloat()
            } catch (e: Exception) {
                0f
            }

            DecryptedBaemailMessage(
                it.txId,
                key.paymail,
                it.createdAt,
                it.updatedAt,
                it.head.thread,
                it.head.amountUsd,
                numAmountUsd,
                it.head.to,
                it.head.cc,
                it.head.subject,
                it.head.salesPitch,
                it.head.from.name,
                it.head.from.primaryPaymail,
                decryptedContent,
                it.hasDestinationSeen(),
                it.hasDestinationPaid(),
                key.bucket
            )
        }
    }

    private suspend fun decryptBaemailMessages(
        mappedCypherText: Map<String, String>,
        paymail: String
    ): MutableMap<String, String> {


        val activePaymailWalletSeed = try {
            secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(paymail))
        } catch (e: Exception) {
            throw e
        }

        val pkiPath = try {
            secureDataSource.getPathForPaymailMnemonic(AuthenticatedPaymail(paymail))
        } catch (e: Exception) {
            throw e
        }

        val decrypted = nodeRuntimeRepository.decryptPkiEncryptedData(
            activePaymailWalletSeed,
            pkiPath,
            mappedCypherText
        ) ?: throw RuntimeException()

        return HashMap(decrypted)
    }


    private suspend fun executeLogin(paymail: String): BaemailState? = withContext(Dispatchers.IO) {

        val loginToken: String? = suspendCancellableCoroutine { cancellableContinuation ->
            try {
                val call = apiService.post(
                    "https://baemail.me/api/login/request",
                    "".toRequestBody()
                )
                val response = call.execute()

                val responseBody = response.body()

                if (response.isSuccessful && responseBody != null) {
                    String(responseBody.bytes()).let {
                        Timber.d("login-response-content-string: $it")
                        cancellableContinuation.resume(it)
                    }
                    return@suspendCancellableCoroutine
                }

            } catch (e: Exception) {
                Timber.e(e)
            }

            cancellableContinuation.resume(null)
        }

        loginToken ?: let {
            Timber.d("failed to retrieve login-token")
            return@withContext null
        }


        val paymailSeed = try {
            secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(paymail))
        } catch (e: Exception) {
            Timber.d("failed to retrieve active-paymail-wallet-seed")
            return@withContext null
        }

        val pkiPath = try {
            secureDataSource.getPathForPaymailMnemonic(AuthenticatedPaymail(paymail))
        } catch (e: Exception) {
            Timber.d("failed to retrieve active-paymail  pki-path")
            return@withContext null
        }

        //keys for pki-path
        val keyHolder = nodeRuntimeRepository.getPrivateKeyForPathFromSeed(
            pkiPath,
            paymailSeed
        ) ?: let {
            Timber.d("error retrieving the wif-private-key for path")
            return@withContext null
        }

        val loginTokenJson = gson.fromJson(loginToken, JsonObject::class.java)
        val loginCode = loginTokenJson.get("code").asString

        val signature = nodeRuntimeRepository.signMessage(
            loginCode,
            keyHolder.privateKey
        ) ?: let {
            Timber.d("error signing the login token with pki-key")
            return@withContext null
        }


        var sess: String? = null
        val loginResponse: String? = suspendCancellableCoroutine { cancellableContinuation ->
            try {

                val jsonReq = JsonObject()
                jsonReq.addProperty("paymail", paymail)
                jsonReq.addProperty("username", keyHolder.publicKey)
                jsonReq.addProperty("code", loginCode)
                jsonReq.addProperty("password", signature)
                jsonReq.addProperty("name", paymail)

                val call = apiService.post(
                    "https://baemail.me/api/login",
                    jsonReq.toString().toRequestBody()
                )
                val response = call.execute()

                val responseBody = response.body()

                if (response.isSuccessful && responseBody != null) {

                    sess = response.headers()["set-cookie"]

                    String(responseBody.bytes()).let {
                        Timber.d("login-attempt-response-content-string: $it")
                        cancellableContinuation.resume(it)
                    }
                    return@suspendCancellableCoroutine
                }


            } catch (e: Exception) {
                Timber.e(e)
            }

            cancellableContinuation.resume(null)
        }

        val isLoginSuccessful = loginResponse.let {
            var isSuccess = false
            try {
                if (sess == null) throw RuntimeException()

                val responseJson = gson.fromJson(it, JsonObject::class.java)
                val value = responseJson.getAsJsonPrimitive("added").asBoolean
                if (value) {
                    isSuccess = true
                }
            } catch (e: Exception) {
                Timber.e(e)
            }

            isSuccess
        }

        if (!isLoginSuccessful) {
            Timber.d("Baemail login failed !!!!!!!")
            return@withContext null
        }

        val loginConfirmResponse: String? = suspendCancellableCoroutine { cancellableContinuation ->
            try {

                val call = apiService.fetch1(
                    "https://baemail.me/api/login/confirm",
                    sess!!
                )
                val response = call.execute()

                val responseBody = response.body()

                if (response.isSuccessful && responseBody != null) {
                    String(responseBody.bytes()).let {
                        Timber.d("login-confirmation-response-content-string: $it")
                        cancellableContinuation.resume(it)
                    }
                    return@suspendCancellableCoroutine
                }

            } catch (e: Exception) {
                Timber.e(e)
            }

            cancellableContinuation.resume(null)
        }


        return@withContext BaemailState(
            loginCode,
            signature,
            System.currentTimeMillis(),
            paymail,
            keyHolder.publicKey,
            sess!!
        )
    }


    class HttpUnauthorizedException : RuntimeException()

}