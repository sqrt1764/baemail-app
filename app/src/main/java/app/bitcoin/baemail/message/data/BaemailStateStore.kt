package app.bitcoin.baemail.message.data

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.core.Preferences
import app.bitcoin.baemail.message.data.entity.BaemailState
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.first

class BaemailStateStore(
    private val ds: DataStore<Preferences>,
    private val gson: Gson
) {
    suspend fun writeState(state: BaemailState) {
        ds.edit { prefs ->
            prefs[stringPreferencesKey("state_${state.paymail}")] = state.serialize()
        }
    }

    suspend fun getStateForPaymail(paymail: String): BaemailState? {
        val key = stringPreferencesKey("state_$paymail")
        val prefs = ds.data.first()

        if (prefs.contains(key)) {
            val serializedState = prefs[key]
            return BaemailState.parse(gson.fromJson(serializedState, JsonObject::class.java))
        }

        return null
    }

    suspend fun requireStateForPaymail(paymail: String): BaemailState {
        val key = stringPreferencesKey("state_$paymail")
        val prefs = ds.data.first()

        if (prefs.contains(key)) {
            val serializedState = prefs[key]
            return BaemailState.parse(gson.fromJson(serializedState, JsonObject::class.java))
        }

        throw RuntimeException()
    }

    suspend fun hasState(paymail: String): Boolean {
        val prefs = ds.data.first()

        return prefs.contains(stringPreferencesKey("state_$paymail"))
    }

    suspend fun clearState(paymail: String) {
        ds.edit { prefs ->
            prefs.remove(stringPreferencesKey("state_${paymail}"))
        }
    }

}