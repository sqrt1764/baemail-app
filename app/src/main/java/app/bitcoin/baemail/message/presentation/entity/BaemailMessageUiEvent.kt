package app.bitcoin.baemail.message.presentation.entity

import androidx.annotation.StringRes

sealed class BaemailMessageUiEvent {
    object Processing: BaemailMessageUiEvent()
    object Success: BaemailMessageUiEvent()
    data class ShowSnackbar(@StringRes val message: Int): BaemailMessageUiEvent()
}