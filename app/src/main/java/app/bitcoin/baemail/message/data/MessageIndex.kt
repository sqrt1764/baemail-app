package app.bitcoin.baemail.message.data

import app.bitcoin.baemail.message.data.entity.MessageBucket
import app.bitcoin.baemail.message.data.entity.SortType
import app.bitcoin.baemail.message.data.entity.Filter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow

class MessageIndex(
    val pageSize: Int
) {

    private val map = mutableMapOf<Key, MutableList<String>>()

    private val hasMoreResults = mutableMapOf<Key, Boolean>()

    private var filterRefreshId = mutableMapOf<Key, Long>()

    private val version = MutableStateFlow(0)

    fun getFilterId(filter: Filter): Long {
        val key = Key(
            filter.paymail,
            filter.query,
            filter.sort,
            filter.bucket
        )

        return filterRefreshId[key] ?: Long.MIN_VALUE
    }

    fun getList(filter: Filter): Flow<Pair<List<String>, Boolean>?> {
        val key = Key(
            filter.paymail,
            filter.query,
            filter.sort,
            filter.bucket
        )

        return version.flatMapLatest {
            flow {
                val list = map[key] ?: let {
                    emit(null)
                    return@flow
                }
                val hasMorePages = hasMoreResults[key] ?: false

                val totalCount = pageSize * (filter.pages + 1)

                val result =  if (totalCount <= list.size) {
                    list.subList(0, totalCount) to (totalCount < list.size || hasMorePages)
                } else {
                    list to false
                }

                emit(result)
            }

        }
    }

    fun clear(filter: Filter) {
        val key = Key(
            filter.paymail,
            filter.query,
            filter.sort,
            filter.bucket
        )

        map.remove(key)
        hasMoreResults.remove(key)
    }

    fun update(filter: Filter, messages: List<String>, hasMorePages: Boolean) {
        val key = Key(
            filter.paymail,
            filter.query,
            filter.sort,
            filter.bucket
        )

        if (filterRefreshId[key] != filter.id) {
            //stuff cached up until now must be dropped
            map.remove(key)
            hasMoreResults.remove(key)

            filterRefreshId[key] = filter.id
        }

        val list = map[key] ?: let {
            val l = ArrayList<String>()
            map[key] = l
            l
        }
        list.addAll(messages)

        hasMoreResults[key] = hasMorePages
    }

    fun onArchived(txId: String) {
        //todo after this the length of the list will not be a multiple of pageSize
        map.entries.forEach { (key, list) ->
            if (MessageBucket.INBOX != key.bucket) return@forEach
            list.remove(txId)
        }

        version.value = version.value + 1
    }

    fun onUnarchived(txId: String) {
        //todo after this the length of the list will not be a multiple of pageSize
        map.entries.forEach { (key, list) ->
            if (MessageBucket.ARCHIVE != key.bucket) return@forEach
            list.remove(txId)
        }

        version.value = version.value + 1
    }

    fun onDeleted(txId: String) {
        //todo after this the length of the list will not be a multiple of pageSize
        map.entries.forEach { (key, list) ->
            if (MessageBucket.INBOX == key.bucket) return@forEach
            list.remove(txId)
        }

        version.value = version.value + 1
    }

    data class Key(
        val paymail: String,
        val query: String?,
        val sort: SortType,
        val bucket: MessageBucket
    )
}