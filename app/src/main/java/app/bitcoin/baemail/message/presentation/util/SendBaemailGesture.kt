package app.bitcoin.baemail.message.presentation.util

import android.annotation.SuppressLint
import android.os.SystemClock
import android.view.MotionEvent
import android.view.View
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.max

class SendBaemailGesture(
    private val rootView: View,
    private val coroutineScope: CoroutineScope,
    private val drawableProgressAnimation: DrawableRevealSend,
    private val drawableSent: DrawableStyledLabel,
    private val drawableHint: DrawableStyledLabel,
    private val dp: Float,
    private val getScreenLocationOfActionView: () -> IntArray,
    private val getScreenLocationOfRootView: () -> IntArray,
    private val dpToMaxProgress: Float = 100 * dp
) : View.OnTouchListener  {

    private val millisShortHoldPeriod = 1000 * 1L
    private var millisStart = -1L

    private var yOfActionDown = -1f

    private var drawableMessageSent: DrawableStyledLabel? = null
    private var drawableSwipeHint: DrawableStyledLabel? = null
    private var jobAnimateHideHint: Job? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        event ?: return false
        v ?: return false

        return when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                onStart(event)

                true
            }
            MotionEvent.ACTION_MOVE -> {

                val y = event.y

                val distanceY = yOfActionDown - y
                val progress = max(distanceY / dpToMaxProgress, 0f)

                drawableProgressAnimation.setProgress(progress)

                false
            }
            MotionEvent.ACTION_UP -> {
                onStop(v, event)

                false
            }
            else -> false
        }
    }

    fun dismiss() {
        dismissNow()
    }

    private fun dismissWithDelay(delay: Long = 5000) {
        //do not bother going through with this if the gesture is not a state that needs dismissing
        if (drawableMessageSent == null) {
            return
        }

        coroutineScope.launch {
            try {
                delay(delay)
            } catch (e: Exception) {
                //got cancelled
                return@launch
            }

            dismissNow()
        }
    }

    private fun dismissNow() {
        //do not bother going through with this if the gesture is not a state that needs dismissing
        if (drawableMessageSent == null) {
            return
        }

        rootView.overlay.remove(drawableSent)
        if (drawableSent == drawableMessageSent) {
            drawableMessageSent = null
        }
    }

    private fun onStart(event: MotionEvent) {
        //Timber.d("onStart ....")

        val locArrayOfSend = getScreenLocationOfActionView()
        val locArrayOfView = getScreenLocationOfRootView()

        drawableProgressAnimation.setProgress(0f)
        drawableProgressAnimation.setBounds(0, 0, rootView.width, rootView.height)

        drawableProgressAnimation.setStartLoc(
            locArrayOfSend[0] - locArrayOfView[0] + event.x,
            locArrayOfSend[1] - locArrayOfView[1] + event.y
        )

        millisStart = SystemClock.elapsedRealtime()
        yOfActionDown = event.y

        drawableSwipeHint?.let {
            rootView.overlay.remove(it)
            drawableSwipeHint = null
        }
        jobAnimateHideHint?.let {
            it.cancel()
            jobAnimateHideHint = null
        }

        rootView.overlay.add(drawableProgressAnimation)
    }

    private fun onSent(v: View) {
        v.performClick()
    }

    private fun onStop(v: View, event: MotionEvent) {
        val now = SystemClock.elapsedRealtime()

        if (drawableProgressAnimation.getLastProgress() > 1.7f) {

            //trigger the event officially
            onSent(v)

            //craft the style of the drawable
            val pos = drawableProgressAnimation.getDrawPositionOfSend()

            drawableSent.init(pos.x, pos.y, drawableProgressAnimation.getSizeOfSend())

            drawableSent.setBounds(0, 0, rootView.width, rootView.height)


            drawableMessageSent = drawableSent
            rootView.overlay.add(drawableSent)

            dismissWithDelay()

        } else if (now - millisStart < millisShortHoldPeriod) {
            //Timber.d(">>>>>>>> show hint that teaches sending ... event.x:${event.x} event.y:${event.y}")

            if (event.x > 0f && event.x < v.width &&
                event.y > 0f && event.y < v.height) {
                //Timber.d(">>>>>>>> the touch is on the button")

                //show the swipe-hint

                //remove an existing hint if any
                drawableSwipeHint?.let {
                    rootView.overlay.remove(it)
                    drawableSwipeHint = null
                }
                jobAnimateHideHint?.let {
                    it.cancel()
                    jobAnimateHideHint = null
                }

                //craft the style of the drawable
                val locArrayOfSend = getScreenLocationOfActionView()
                val locArrayOfView = getScreenLocationOfRootView()

                val xOfStart = locArrayOfSend[0] - locArrayOfView[0]
                val yOfBottom = locArrayOfSend[1] - locArrayOfView[1] - 1 * dp * 12

                drawableHint.init(
                    xOfStart.toInt(),
                    yOfBottom.toInt(),
                    dp * 42
                )
                
                drawableHint.setBounds(0, 0, rootView.width, rootView.height)


                drawableSwipeHint = drawableHint
                rootView.overlay.add(drawableHint)

                jobAnimateHideHint = coroutineScope.launch {
                    try {
                        delay((1.6f * 1000).toLong())
                    } catch (e: Exception) {
                        //got cancelled
                        return@launch
                    }
                    drawableSwipeHint?.let {
                        rootView.overlay.remove(it)
                        drawableSwipeHint = null
                    }
                }
            }



        } else {
            //Timber.d(">>>>>>>> end of LONG HOLD")
        }

        //todo rm

        rootView.overlay.remove(drawableProgressAnimation)
    }
}