package app.bitcoin.baemail.message.data.entity

data class Filter(
    val id: Long,
    val paymail: String,
    val query: String?,
    val sort: SortType,
    val bucket: MessageBucket,
    val pages: Int
)