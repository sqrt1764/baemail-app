package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull

class ApplyNewReplyDraftUseCaseImpl(
    private val getActivePaymailUseCase: GetActivePaymailUseCase,
    private val baemailRepository: BaemailRepository
) : ApplyNewReplyDraftUseCase {
    override suspend fun invoke(
        destinationPaymail: String,
        subject: String,
        threadId: String,
        millisCreated: Long
    ) {
        val paymail = getActivePaymailUseCase().mapNotNull { it?.paymail }.first()

        baemailRepository.messageDraftHelper.applyNewReply(
            paymail,
            destinationPaymail,
            subject,
            threadId,
            millisCreated
        )
    }

}

interface ApplyNewReplyDraftUseCase {
    suspend operator fun invoke(
        destinationPaymail: String,
        subject: String,
        threadId: String,
        millisCreated: Long
    )
}