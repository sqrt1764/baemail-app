package app.bitcoin.baemail.message.data.entity

data class Draft(
    val owner: String,
    val millisCreated: Long,
    val destination: String?,
    val subject: String?,
    val content: String?,
    val thread: String?,
    val payCurrency: String,
    val payMax: Float,
    val payAmount: Float
)