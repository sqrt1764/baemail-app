package app.bitcoin.baemail.message.domain

import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.core.presentation.view.recycler.ATMessage0
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import com.google.gson.Gson
import com.google.gson.JsonObject
import timber.log.Timber
import java.lang.Exception

class MapMessagesToAdapterTypesUseCaseImpl(
    private val gson: Gson,
    private val formatMessageDateUseCase: FormatMessageDateUseCase,
    private val htmlToSpannedStringUseCase: HtmlToSpannedStringUseCase,
    private val parseDecryptedMessageContent: ParseDecryptedMessageContent
): MapMessagesToAdapterTypesUseCase {
    override fun invoke(activePaymail: String, list: List<DecryptedBaemailMessage>): List<AdapterType> {
        val adapterList = mutableListOf<AdapterType>()

        list.map { message ->
            val blocksContent = parseDecryptedMessageContent(message.decryptedMessage)

            val stringContent = htmlToSpannedStringUseCase(blocksContent)

            val content = if (stringContent.length <= 200) {
                stringContent
            } else {
                stringContent.substring(0, 200)
            }

            val createdOnLabel = formatMessageDateUseCase(message.createdAt)

            val finalFrom = if (message.fromName == null) {
                message.from
            } else if (message.fromName != message.from) {
                "${message.fromName} (${message.from})"
            } else {
                message.fromName
            }

            ATMessage0(
                message.txId,
                finalFrom,
                message.subject,
                content,
                createdOnLabel,
                hasAttachment = false,
                isUnread = !message.destinationSeen && message.to == activePaymail,
                message = message
            )
        }.toCollection(adapterList)

        return adapterList
    }

}

interface MapMessagesToAdapterTypesUseCase {
    operator fun invoke(activePaymail: String, list: List<DecryptedBaemailMessage>): List<AdapterType>
}