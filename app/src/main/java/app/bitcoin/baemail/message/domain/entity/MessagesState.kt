package app.bitcoin.baemail.message.domain.entity

import app.bitcoin.baemail.core.data.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.message.data.entity.Filter

data class MessagesState(
    val filter: Filter,
    val items: List<DecryptedBaemailMessage>,
    val hasNextPage: Boolean,
    val isRefreshing: Boolean
)