package app.bitcoin.baemail.message.presentation.entity

import androidx.annotation.StringRes

sealed class NewBaemailUiEvent {
    object Processing: NewBaemailUiEvent()
    object SendSuccess: NewBaemailUiEvent()
    object DiscardSuccess: NewBaemailUiEvent()
    data class ShowSnackbar(@StringRes val message: Int): NewBaemailUiEvent()
}