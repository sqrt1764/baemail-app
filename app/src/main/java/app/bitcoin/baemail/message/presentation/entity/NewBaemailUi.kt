package app.bitcoin.baemail.message.presentation.entity

import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import app.bitcoin.baemail.core.presentation.view.ViewBaePayOptions

data class NewBaemailUi(
    val paymail: String,
    val subject: String,
    val replyThreadId: String?,
    val content: String,
    val isMessageValid: Boolean,
    val paymailExistenceState: PaymailExistence,
    val payOptions: ViewBaePayOptions.Model,
    val satsAvailable: Long = -1,
    val satsEstimatedRequired: Long = -1
)