package app.bitcoin.baemail.paymail.presentation

import androidx.annotation.StringRes
import androidx.lifecycle.*
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.domain.usecases.CheckIsValidEmailUseCase
import app.bitcoin.baemail.core.domain.usecases.CheckIsValidPkiPathUseCase
import app.bitcoin.baemail.core.domain.usecases.CheckPaymailExistsUseCase
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.core.domain.usecases.GetSeedWordListUseCase
import app.bitcoin.baemail.core.domain.usecases.NormalizePkiPathUseCase
import app.bitcoin.baemail.paymail.domain.usecases.AddAuthUseCase
import app.bitcoin.baemail.paymail.domain.usecases.ErrorAliasMnemonicMismatch
import app.bitcoin.baemail.paymail.domain.usecases.ErrorAliasNotFound
import app.bitcoin.baemail.paymail.domain.usecases.ErrorInvalidMnemonic
import app.bitcoin.baemail.paymail.domain.usecases.GetAuthenticatedPaymailsUseCase
import app.bitcoin.baemail.paymail.domain.usecases.GetPkiPathUseCase
import app.bitcoin.baemail.paymail.domain.usecases.SwitchActivePaymailUseCase
import app.bitcoin.baemail.core.presentation.view.recycler.ATAuthenticatedPaymail
import app.bitcoin.baemail.core.presentation.view.recycler.ATFilterSeedWord
import app.bitcoin.baemail.core.presentation.view.recycler.ATNoPaymailsAdded
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.paymail.domain.entity.PaymailValidation
import app.bitcoin.baemail.paymail.domain.usecases.CheckIsValidMnemonicUseCase
import app.bitcoin.baemail.paymail.domain.usecases.FindFundingWalletSeedUseCase
import app.bitcoin.baemail.paymail.domain.usecases.ValidatePaymailUseCase
import app.bitcoin.baemail.paymail.domain.usecases.ValidateWalletInfoUseCase
import app.bitcoin.baemail.paymail.presentation.compose.FundingWallet
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class PaymailManagementViewModel @Inject constructor(
    getAuthenticatedPaymailsUseCase: GetAuthenticatedPaymailsUseCase,
    getActivePaymailUseCase: GetActivePaymailUseCase,
    private val switchActivePaymailUseCase: SwitchActivePaymailUseCase,
    getSeedWordListUseCase: GetSeedWordListUseCase,
    private val getPkiPathUseCase: GetPkiPathUseCase,
    private val checkPaymailExistsUseCase: CheckPaymailExistsUseCase,
    private val checkIsValidEmailUseCase: CheckIsValidEmailUseCase,
    val checkIsValidPkiPathUseCase: CheckIsValidPkiPathUseCase,
    private val normalizePkiPathUseCase: NormalizePkiPathUseCase,
    private val validatePaymailUseCase: ValidatePaymailUseCase,
    private val checkIsValidMnemonicUseCase: CheckIsValidMnemonicUseCase,
    private val validateWalletInfoUseCase: ValidateWalletInfoUseCase,
    private val findFundingWalletSeedUseCase: FindFundingWalletSeedUseCase,
    private val addAuthUseCase: AddAuthUseCase
) : ViewModel() {

    val seedWordList: StateFlow<List<ATFilterSeedWord>> = getSeedWordListUseCase().map { words ->
        return@map words.map {
            ATFilterSeedWord(it)
        }
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.Eagerly,
        initialValue = listOf()
    )

    val addedPaymails: StateFlow<List<AdapterType>> = getAuthenticatedPaymailsUseCase()
        .combine(getActivePaymailUseCase().mapNotNull { it }) { added, active ->
            if (added.isEmpty()) {
                return@combine listOf(
                    ATNoPaymailsAdded("0")
                )
            }

            //show added paymails
            return@combine added.mapIndexed { index, authenticatedPaymail ->
                ATAuthenticatedPaymail(
                    authenticatedPaymail.paymail,
                    authenticatedPaymail.paymail == active.paymail,
                    index + 1 == added.size
                )
            }

        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = listOf(
                ATNoPaymailsAdded("0")
            )
        )


    private val _paymailId = MutableStateFlow("")

    private val _paymailIdExistence: Flow<PaymailExistence> = _paymailId
        .flatMapLatest { value ->
            val isValidAddress = checkIsValidEmailUseCase(value)
            if (!isValidAddress) return@flatMapLatest flow<PaymailExistence> {
                emit(PaymailExistence.Checking(value))
            }
            checkPaymailExistsUseCase(value)
        }

    val paymailIdModel = _paymailId.combine(_paymailIdExistence) { paymail, idExistence ->
        paymail to idExistence
    }.combine(getAuthenticatedPaymailsUseCase().mapNotNull { added ->
        added.map { it.paymail }
    }) { (paymail, idExistence), addedPaymails ->
        val isValidAddress = checkIsValidEmailUseCase(paymail)

        val shouldShowInvalidAddressError = isValidAddress.let showError@{
            if (it) return@showError false

            val indexOfAt = paymail.indexOf("@")
            (indexOfAt != -1).let { containsAt ->
                if (!containsAt) return@showError false
            }

            val indexOfDot = paymail.substring(indexOfAt).indexOf(".")
            (indexOfDot != -1).let { containsDot ->
                if (!containsDot) return@showError false
            }

            if (indexOfDot + 1 == paymail.substring(indexOfAt).length) {
                return@showError false
            }

            return@showError true
        }

        PaymailIdModel(
            paymail,
            isValidAddress,
            shouldShowInvalidAddressError,
            idExistence,
            addedPaymails.contains(paymail)
        )

    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = PaymailIdModel(
            input = "",
            isValidAddress = false,
            shouldShowInvalidAddressError = false,
            paymailExistence = PaymailExistence.Checking(),
            isAlreadyAdded = false
        )
    )


    private val _paymailPath = MutableStateFlow<String?>(null)

    private val _paymailMnemonic = MutableStateFlow<List<String>?>(null)

    val fundingWalletType = MutableStateFlow(FundingWallet.DETERMINISTIC)

    private val _fundingPath = MutableStateFlow(AuthRepository.FUNDING_WALLET_ROOT_PATH)

    private val _fundingMnemonic = MutableStateFlow<List<String>?>(null)

    val fundingPathInfo: StateFlow<FundingPathModel> = _fundingPath.map { path ->
        FundingPathModel(
            path,
            checkIsValidPkiPathUseCase(path, true),
            checkIsValidPkiPathUseCase(path, false)
        )
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = FundingPathModel(
            path = AuthRepository.FUNDING_WALLET_ROOT_PATH,
            isPathValid = true,
            isPathValidStrict = true
        )
    )

    val paymailPath: StateFlow<PaymailPkiPathModel> = paymailIdModel.mapNotNull {
        if (it.shouldShowInvalidAddressError ||
            it.isAlreadyAdded ||
            it.paymailExistence is PaymailExistence.Invalid
        ) {
            return@mapNotNull null
        }

        it.input to getPkiPathUseCase(it.input)

    }.combine(_paymailPath) { (paymail, defaultPkiPath), path ->
        val defP = defaultPkiPath ?: ""
        val p = if (path.isNullOrBlank()) defP else path
        PaymailPkiPathModel(
            paymail,
            defP,
            p,
            checkIsValidPkiPathUseCase(path)
        )

    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = PaymailPkiPathModel(
            paymail = "",
            pkiPathDefault = null,
            pkiPath = null,
            isPkiPathValid = false
        )
    )

    val overview: StateFlow<OverviewModel> = _paymailId.combine(_paymailPath) { id, path ->
        id to path
    }.combine(_paymailMnemonic) { (id, path), mnemonic ->
        Triple(id, path, mnemonic)
    }.combine(_fundingPath) { (id, path, mnemonic), fundingPath ->
        Triple(id, path, mnemonic) to fundingPath
    }.combine(_fundingMnemonic) { (paymailInfo, fundingPath), fundingMnemonic ->
        val (id, path, mnemonic) = paymailInfo

        val isFundingInfoValid = if (fundingMnemonic != null) {
            validateWalletInfoUseCase(fundingMnemonic, fundingPath)
        } else false

        OverviewModel(
            paymail = id,
            paymailPath = path ?: "",
            paymailMnemonic = mnemonic ?: emptyList(),
            fundingPath = fundingPath,
            fundingMnemonic = fundingMnemonic ?: emptyList(),
            isFundingInfoValid = isFundingInfoValid
        )

    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = OverviewModel(
            paymail = "",
            paymailPath = "",
            paymailMnemonic = emptyList(),
            fundingPath = "",
            fundingMnemonic = emptyList(),
            isFundingInfoValid = false
        )
    )


    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()


    fun onChangeActivePaymail(paymail: String) {
        switchActivePaymailUseCase(paymail)
    }

    fun onAddingCancelled() {
        _paymailId.value = ""
        _paymailPath.value = null
        _paymailMnemonic.value = null
        fundingWalletType.value = FundingWallet.DETERMINISTIC
        _fundingPath.value = AuthRepository.FUNDING_WALLET_ROOT_PATH
        _fundingMnemonic.value = null
    }

    fun onPaymailIdChanged(input: String) {
        _paymailId.value = input
    }

    fun onPaymailPkiPathChanged(path: String) {
        if (checkIsValidPkiPathUseCase(path, true)) {
            _paymailPath.value = normalizePkiPathUseCase(path)
        } else {
            val current = _paymailPath.value
            _paymailPath.value = "$current "
            _paymailPath.value = current
        }
    }

    fun onPaymailMnemonicAdded(seedList: List<String>) {
        _paymailMnemonic.value = seedList

        viewModelScope.launch {
            _eventFlow.emit(UiEvent.ProcessingPaymailInfoValidity)

            try {
                val isPaymailInfoValid = validatePaymailUseCase(
                    paymailIdModel.value.input,
                    paymailPath.value.pkiPath!!,
                    seedList
                )

                when (isPaymailInfoValid) {
                    PaymailValidation.VALID -> {
                        _eventFlow.emit(UiEvent.SuccessPaymailInfoValid)
                    }
                    PaymailValidation.ALIAS_NOT_FOUND -> {
                        _eventFlow.emit(
                            UiEvent.FailPaymailInfoInvalid(
                                R.string.paymail_not_found,
                                ErrorAliasNotFound()
                            )
                        )
                    }
                    PaymailValidation.INVALID_MNEMONIC -> {
                        _eventFlow.emit(
                            UiEvent.FailPaymailInfoInvalid(
                                R.string.invalid_mnemonic,
                                ErrorInvalidMnemonic()
                            )
                        )
                    }
                    PaymailValidation.ALIAS_MNEMONIC_MISMATCH -> {
                        _eventFlow.emit(
                            UiEvent.FailPaymailInfoInvalid(
                                R.string.paymail_adding_error__mnemonic_mismatch,
                                ErrorAliasMnemonicMismatch()
                            )
                        )
                    }
                }

            } catch (e: Exception) {
                _eventFlow.emit(
                    UiEvent.FailPaymailInfoInvalid(
                        R.string.error,
                        e
                    )
                )
            }

        }
    }

    fun onFundingWalletTypeChange(type: FundingWallet) {
        fundingWalletType.value = type
    }

    fun onFundingPathSubmit() {
        viewModelScope.launch {
            when (fundingWalletType.value) {
                FundingWallet.DETERMINISTIC -> {
                    _fundingMnemonic.value = findFundingWalletSeedUseCase(_paymailMnemonic.value!!)
                    _eventFlow.emit(UiEvent.ProceedToAuthorization)
                }
                FundingWallet.PAYMAIL -> {
                    _fundingMnemonic.value = _paymailMnemonic.value
                    _eventFlow.emit(UiEvent.ProceedToAuthorization)
                }
                FundingWallet.CUSTOM -> {
                    _eventFlow.emit(UiEvent.ProceedToFundingMnemonicInput)
                }
            }
        }
    }

    fun onFundingPathChanged(path: String) {
        if (checkIsValidPkiPathUseCase(path, true)) {
            _fundingPath.value = normalizePkiPathUseCase(path)
        }
    }

    fun onFundingMnemonicAdded(seedList: List<String>) {
        _fundingMnemonic.value = seedList

        viewModelScope.launch {
            if (checkIsValidMnemonicUseCase(seedList)) {
                _eventFlow.emit(UiEvent.ProceedToFundingMnemonicInput)
            } else {
                _eventFlow.emit(UiEvent.FailFundingMnemonicNotValid)
            }
        }
    }

    fun onConfirmAuthorizePaymail() {
        viewModelScope.launch {
            val overview = overview.value
            if (!overview.isFundingInfoValid) {
                _eventFlow.emit(
                    UiEvent.FailPaymailInfoInvalid(
                        R.string.account_info_invalid
                    )
                )
                return@launch
            }

            try {
                addAuthUseCase(
                    overview.paymail,
                    overview.paymailPath,
                    overview.paymailMnemonic,
                    overview.fundingPath,
                    overview.fundingMnemonic
                )

                onAddingCancelled()

                _eventFlow.emit(UiEvent.SuccessPaymailAdded)
            } catch (e: Exception) {
                _eventFlow.emit(
                    UiEvent.FailPaymailInfoInvalid(
                        R.string.account_adding_failed,
                        e
                    )
                )
            }
        }
    }

    data class PaymailIdModel(
        val input: String,
        val isValidAddress: Boolean,
        val shouldShowInvalidAddressError: Boolean,
        val paymailExistence: PaymailExistence,
        val isAlreadyAdded: Boolean
    )

    data class PaymailPkiPathModel(
        val paymail: String? = null,
        val pkiPathDefault: String? = null,
        val pkiPath: String? = null,
        val isPkiPathValid: Boolean = false,
    )

    data class FundingPathModel(
        val path: String,
        val isPathValid: Boolean,
        val isPathValidStrict: Boolean
    )

    data class OverviewModel(
        val paymail: String,
        val paymailPath: String,
        val paymailMnemonic: List<String>,
        val fundingPath: String,
        val fundingMnemonic: List<String>,
        val isFundingInfoValid: Boolean
    )

    sealed class UiEvent {
        object ProcessingPaymailInfoValidity : UiEvent()
        object SuccessPaymailInfoValid : UiEvent()
        object SuccessPaymailAdded : UiEvent()
        data class FailPaymailInfoInvalid(
            @StringRes val message: Int,
            val error: Exception = RuntimeException()
        ) : UiEvent()

        object ProceedToAuthorization : UiEvent()
        object ProceedToFundingMnemonicInput : UiEvent()

        object FailFundingMnemonicNotValid : UiEvent()
    }
}