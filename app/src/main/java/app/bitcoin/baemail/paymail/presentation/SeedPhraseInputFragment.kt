package app.bitcoin.baemail.paymail.presentation

import android.content.Context
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.ViewOutlineProvider
import android.view.WindowInsets
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.doOnPreDraw
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.util.OnAfterChangeTextWatcher
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.twetch.ui.TwetchFilesMimeTypeHelper
import app.bitcoin.baemail.core.presentation.util.NoSwipeBehavior
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.util.isDarkModeOn
import app.bitcoin.baemail.core.presentation.view.ErrorSnackBar
import app.bitcoin.baemail.core.presentation.view.recycler.ATFilterSeedNoMatch
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATSeedWordHint
import app.bitcoin.baemail.core.presentation.view.recycler.ATSelectedSeedWord
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.max

class SeedPhraseInputFragment : Fragment(R.layout.fragment_seed_phrase_input) {

    companion object {
        const val KEY_SCENARIO = "mnemonic_scenario"
        const val SCENARIO_PAYMAIL = "paymail"
        const val SCENARIO_FUNDING = "funding"

        fun argsForPaymailInput() = Bundle().apply {
            putString(KEY_SCENARIO, SCENARIO_PAYMAIL)
        }
        fun argsForFundingInput() = Bundle().apply {
            putString(KEY_SCENARIO, SCENARIO_FUNDING)
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PaymailManagementViewModel


    private lateinit var container: ConstraintLayout

    private lateinit var coordinator: CoordinatorLayout

    private lateinit var titleText: TextView

    private lateinit var selectedWordsRecycler: RecyclerView
    private lateinit var filterWordsRecycler: RecyclerView
    private lateinit var input: EditText

    private lateinit var cancel: TextView
    private lateinit var done: TextView

    private lateinit var clearInput: ImageView

    private lateinit var selectedWordsAdapter: BaseListAdapter

    private lateinit var filterWordsAdapter: BaseListAdapter

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }


    private val filterBottomExtraLD = MutableLiveData<Int>()
    private val bottomExtraLiModel = ATHeightDecorDynamic("0", filterBottomExtraLD)


    private val dp: Float by lazy {
        resources.displayMetrics.density
    }

    private var maxSeedWordCount = 12
    private val selectedSeedWords = mutableListOf<String>()

    private val elevatedTopBarHeight: Float by lazy {
        dp * 6
    }

    private val drawableStrokeSeparator: DrawableStroke by lazy {
        val strokeColor = requireContext().getColorFromAttr(R.attr.colorSurfaceVariant)

        val strokeWidth = (dp * 2).toInt()

        val d = DrawableStroke()
        d.setStrokeColor(strokeColor)
        d.setStrokeWidths(0, strokeWidth, 0, 0)

        d
    }

    private val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorSelectorOnLight: Int by lazy {
        R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorDoneBackground: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }

    private val colorDoneBackgroundDisabled: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimaryVariant)
    }

    private val drawableDoneBackground: ColorDrawable by lazy {
        ColorDrawable(colorDoneBackgroundDisabled)
    }

    private val drawableDoneSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }

    private val labelDoneWhenComplete: String by lazy {
        resources.getString(R.string.seed_phrase_input_fragment__done_value)
    }

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private lateinit var refreshTopBarElevation: ()->Unit

    private lateinit var inputType: String
    private lateinit var scenario: MnemonicTypeScenario

    private val availableHeightHelper = AvailableScreenHeightHelper()
    private val keyboardHeightState = MutableStateFlow(0)
    private val appliedInsetsState = MutableStateFlow(AppliedInsets())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        inputType = arguments?.getString(KEY_SCENARIO) ?: SCENARIO_PAYMAIL
        scenario = when (inputType) {
            SCENARIO_PAYMAIL -> PaymailMnemonicScenario()
            SCENARIO_FUNDING -> FundingMnemonicScenario()
            else -> throw RuntimeException()
        }

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(
            viewModelStoreOwner,
            viewModelFactory
        )[PaymailManagementViewModel::class.java]
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        container = requireView().findViewById(R.id.container)
        coordinator = requireView().findViewById(R.id.coordinator)
        titleText = requireView().findViewById(R.id.title_text)
        selectedWordsRecycler = requireView().findViewById(R.id.selected_words)
        filterWordsRecycler = requireView().findViewById(R.id.filter_list)
        input = requireView().findViewById(R.id.input)
        cancel = requireView().findViewById(R.id.cancel)
        done = requireView().findViewById(R.id.done)
        clearInput = requireView().findViewById(R.id.clear_input)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            availableHeightHelper.setup(
                requireContext(),
                requireActivity().window,
                viewLifecycleOwner,
                { requireView() }
            )

            availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
                height ?: return@Observer

                val maxHeight = container.height - appliedInsetsState.value.top - appliedInsetsState.value.bottom
                val keyboardHeight = max(maxHeight - height, 0)

                keyboardHeightState.value = keyboardHeight
            })

            viewLifecycleOwner.lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    keyboardHeightState.collectLatest { keyboardHeight ->
                        onKeyboardHeightChange(keyboardHeight)
                    }
                }
            }
        }

        container.doOnApplyWindowInsets { v, windowInsets, _ ->
            val appliedInsets = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                AppliedInsets(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                AppliedInsets(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight
                )
            }

            v.updatePadding(
                top = appliedInsets.top,
                bottom = appliedInsets.bottom,
                left = appliedInsets.left,
                right = appliedInsets.right
            )

            appliedInsetsState.value = appliedInsets

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val keyboardHeight = max(
                    windowInsets.getInsets(WindowInsets.Type.ime()).bottom -
                            windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    0
                )

                keyboardHeightState.value = keyboardHeight

                onKeyboardHeightChange(keyboardHeight)
            }

            windowInsets
        }


        filterBottomExtraLD.value = (resources.displayMetrics.density * 130).toInt()

        titleText.text = scenario.getTitle()

        cancel.clipToOutline = true
        cancel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }


        val colorCancelBackground = requireContext().getColorFromAttr(R.attr.colorPrimary)

        cancel.background = LayerDrawable(arrayOf(
            ColorDrawable(colorCancelBackground),
            DrawableState.getNew(colorSelectorOnAccent)
        ))

        cancel.setOnClickListener {
            imm.hideSoftInputFromWindow(cancel.windowToken, 0)
            viewModel.onAddingCancelled()
            findNavController().popBackStack(R.id.addedPaymailsFragment, false)
        }


        done.clipToOutline = true
        done.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        done.background = LayerDrawable(arrayOf(
            drawableDoneBackground,
            drawableDoneSelector
        ))

        done.setOnClickListener {
            imm.hideSoftInputFromWindow(cancel.windowToken, 0)
            scenario.onDone()
        }


        clearInput.setOnClickListener {
            input.setText("")
        }

        clearInput.foreground = DrawableState.getNew(colorSelectorOnLight)

        clearInput.clipToOutline = true
        clearInput.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }


        selectedWordsAdapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atSelectedSeedWordListener = object : BaseListAdapter.ATSelectedSeedWordListener {
                    override fun onDismissClicked() {
                        selectedSeedWords.removeAt(selectedSeedWords.size - 1)
                        refreshSeedWordRecycler()
                        refreshDoneButton()
                    }

                }
            )
        )

        val selectedWordsLayoutManager = LinearLayoutManager(context)
        selectedWordsLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        selectedWordsRecycler.layoutManager = selectedWordsLayoutManager
        selectedWordsRecycler.adapter = selectedWordsAdapter


        filterWordsAdapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atFilterSeedWordListener = object : BaseListAdapter.ATFilterSeedWordListener {
                    override fun onClick(word: String) {
                        if (selectedSeedWords.size == maxSeedWordCount) return

                        selectedSeedWords.add(word)

                        refreshSeedWordRecycler()
                        refreshDoneButton()

                        input.setText("")
                    }
                }
            )
        )

        val filterWordsLayoutManager = LinearLayoutManager(context)
        filterWordsRecycler.layoutManager = filterWordsLayoutManager
        filterWordsRecycler.adapter = filterWordsAdapter

        filterWordsRecycler.background = drawableStrokeSeparator


        if (resources.isDarkModeOn()) {
            refreshTopBarElevation = lambda@{
                //do nothing
            }
        } else {
            refreshTopBarElevation = lambda@{
                when (filterWordsLayoutManager.findFirstCompletelyVisibleItemPosition()) {
                    RecyclerView.NO_POSITION -> false
                    0 -> false
                    else -> true
                }.let { showShadow ->
                    if (showShadow) {
                        if (drawableStrokeSeparator.alpha == 0) return@lambda

                        titleText.elevation = elevatedTopBarHeight
                        selectedWordsRecycler.elevation = elevatedTopBarHeight
                        input.elevation = elevatedTopBarHeight
                        clearInput.elevation = elevatedTopBarHeight

                        drawableStrokeSeparator.alpha = 0
                    } else {
                        if (drawableStrokeSeparator.alpha == 255) return@lambda

                        titleText.elevation = 0f
                        selectedWordsRecycler.elevation = 0f
                        input.elevation = 0f
                        clearInput.elevation = 0f

                        drawableStrokeSeparator.alpha = 255
                    }
                }
            }
        }

        filterWordsRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                refreshTopBarElevation()
            }
        })

        if (savedInstanceState == null) {
            //avoid drawing the shadow top-bar automatically lays out with at first
            view.doOnPreDraw {
                selectedWordsRecycler.elevation = 0f
                input.elevation = 0f
                clearInput.elevation = 0f

                drawableStrokeSeparator.alpha = 255
            }
        }

        selectedWordsAdapter.setItems(listOf(
            ATSeedWordHint(resources.getString(R.string.li_seed_word_hint__value_of_start))
        ))

        input.addTextChangedListener(object : OnAfterChangeTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                filterSeedWords(s.toString())
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    viewModel.onAddingCancelled()

                    isEnabled = false
                    findNavController().popBackStack(R.id.addedPaymailsFragment, false)
                }

            }
        )

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.eventFlow.collectLatest (scenario.eventHandler())
            }
        }

        filterSeedWords("")
        refreshSeedWordRecycler()
        refreshDoneButton()
    }

    private fun onKeyboardHeightChange(keyboardHeight: Int) {
        filterBottomExtraLD.value = keyboardHeight

        cancel.translationY = -1f * keyboardHeight
        done.translationY = -1f * keyboardHeight
        coordinator.translationY = -1f * keyboardHeight
    }

    private var filterSeedWordsJob: Job? = null

    private fun filterSeedWords(filter: String) {
        filterSeedWordsJob?.cancel()

        if (filter.isEmpty()) {
            val list = ArrayList<AdapterType>(viewModel.seedWordList.value)
            list.add(bottomExtraLiModel)
            filterWordsAdapter.setItems(list)

            filterWordsRecycler.scrollToPosition(0)
            filterWordsRecycler.post {
                refreshTopBarElevation()
            }

            return
        }

        filterSeedWordsJob = viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Default) {
            val filtered = ArrayList<AdapterType>()

            viewModel.seedWordList.value.filter {
                ensureActive()
                it.value.startsWith(filter)
            }.toCollection(filtered)

            if (filtered.isEmpty()) {
                filtered.add(ATFilterSeedNoMatch(""))
            }

            filtered.add(bottomExtraLiModel)


            withContext(Dispatchers.Main) {
                filterWordsAdapter.setItems(filtered)

                filterWordsRecycler.scrollToPosition(0)
                filterWordsRecycler.post {
                    refreshTopBarElevation()
                }
            }
        }
    }

    private fun refreshSeedWordRecycler() {
        val contentList = mutableListOf<AdapterType>()

        selectedSeedWords.forEachIndexed { index, s ->
            val isLast = index + 1 == selectedSeedWords.size
            contentList.add(ATSelectedSeedWord(s, isLast))
        }

        if (selectedSeedWords.size == 0) {
            contentList.add(
                ATSeedWordHint(resources.getString(R.string.li_seed_word_hint__value_of_start))
            )
        }

        selectedWordsAdapter.setItems(contentList)

        //scroll to the end
        val scrollAmount = (selectedSeedWords.size * (dp * 150)).toInt()
        selectedWordsRecycler.scrollBy(scrollAmount, 0)
    }

    private fun refreshDoneButton() {
        if (selectedSeedWords.size == 12) {
            done.isEnabled = true
            done.text = labelDoneWhenComplete
            drawableDoneBackground.color = colorDoneBackground
            return
        }
        done.isEnabled = false

        val missingCount = 12 - selectedSeedWords.size

        done.text = resources.getString(
            R.string.seed_phrase_input_fragment__done_incomplete, missingCount)

        drawableDoneBackground.color = colorDoneBackgroundDisabled
    }


    override fun onStart() {
        super.onStart()

        Timber.d("onStart $imm")
        if (input.requestFocus()) {
            Timber.d("onStart got focus")

            if (!imm.isActive(input)) imm.restartInput(input)
        }
    }

    override fun onStop() {
        filterSeedWordsJob?.cancel()
        super.onStop()
    }

    //////

    interface MnemonicTypeScenario {
        fun getTitle(): String
        fun onDone()
        fun eventHandler(): suspend (PaymailManagementViewModel.UiEvent) -> Unit
    }

    inner class PaymailMnemonicScenario : MnemonicTypeScenario {
        override fun getTitle(): String =
            requireContext().getString(R.string.paymail_seed_input_title_for_paymail)

        override fun onDone() {
            viewModel.onPaymailMnemonicAdded(selectedSeedWords)
        }

        override fun eventHandler(): suspend (PaymailManagementViewModel.UiEvent) -> Unit = { event ->
            when (event) {
                is PaymailManagementViewModel.UiEvent.ProcessingPaymailInfoValidity -> {
                    findNavController().navigate(
                        R.id.action_seedPhraseInputFragment_to_paymailVerifyingFragment
                    )

                }
                is PaymailManagementViewModel.UiEvent.FailPaymailInfoInvalid -> {
                    ErrorSnackBar.make(
                        requireContext(),
                        coordinator,
                        getString(event.message)
                    ).let {
                        it.behavior = NoSwipeBehavior()
                        it.duration = Snackbar.LENGTH_SHORT
                        it.show()
                    }

                }
                else -> {
                    Timber.d("event not handled: $event")
                }
            }

        }

    }

    inner class FundingMnemonicScenario : MnemonicTypeScenario {
        override fun getTitle(): String =
            requireContext().getString(R.string.paymail_seed_input_title_for_funding)

        override fun onDone() {
            viewModel.onFundingMnemonicAdded(selectedSeedWords)
        }

        override fun eventHandler(): suspend (PaymailManagementViewModel.UiEvent) -> Unit = { event ->
            when (event) {
                is PaymailManagementViewModel.UiEvent.ProceedToFundingMnemonicInput -> {
                    findNavController().navigate(
                        R.id.action_seedPhraseInputFragment_to_paymailAuthorizationFragment
                    )

                }
                is PaymailManagementViewModel.UiEvent.FailFundingMnemonicNotValid -> {
                    ErrorSnackBar.make(
                        requireContext(),
                        coordinator,
                        getString(R.string.invalid_funding_mnemonic)
                    ).let {
                        it.behavior = NoSwipeBehavior()
                        it.duration = Snackbar.LENGTH_SHORT
                        it.show()
                    }

                }
                else -> {
                    Timber.d("event not handled: $event")
                }
            }
        }

    }

}

