package app.bitcoin.baemail.paymail.domain.entity

enum class PaymailValidation {
    VALID,
    ALIAS_NOT_FOUND,
    INVALID_MNEMONIC,
    ALIAS_MNEMONIC_MISMATCH
}