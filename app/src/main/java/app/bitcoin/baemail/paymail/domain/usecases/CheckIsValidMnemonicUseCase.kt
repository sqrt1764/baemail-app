package app.bitcoin.baemail.paymail.domain.usecases

import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository

class CheckIsValidMnemonicUseCaseImpl(
    private val nodeRuntimeRepository: NodeRuntimeRepository
) : CheckIsValidMnemonicUseCase {
    override suspend fun invoke(seedList: List<String>): Boolean =
        nodeRuntimeRepository.checkMnemonic(seedList)

}

interface CheckIsValidMnemonicUseCase {
    suspend operator fun invoke(seedList: List<String>): Boolean
}