package app.bitcoin.baemail.paymail.domain.usecases

class GetPkiPathUseCaseImpl: GetPkiPathUseCase {

    override fun invoke(paymail: String): String? {
        return paymail.substring(paymail.indexOf("@") + 1).let { domain ->
            when (domain) {
                "moneybutton.com" -> "m/44'/0'/0'/0/0/0"
                "relayx.io" -> "m/0'/236'/0'/0/0"
                "simply.cash" -> "m/44'/145'/0'/2/0"
                "twetch.me" -> "m/0/0"
                else -> null
            }
        }
    }
}

interface GetPkiPathUseCase {
    operator fun invoke(paymail: String): String?
}