package app.bitcoin.baemail.paymail.presentation

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import androidx.activity.OnBackPressedCallback
import androidx.compose.foundation.layout.imePadding
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.paymail.presentation.compose.ComposeFundingDerivPathScreen
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.max

class FundingDerivPathFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PaymailManagementViewModel

    private val availableHeightHelper = AvailableScreenHeightHelper()
    private val keyboardHeightState = MutableStateFlow(0)
    private val appliedInsetsState = MutableStateFlow(AppliedInsets())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(
            viewModelStoreOwner,
            viewModelFactory
        )[PaymailManagementViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            val pathInfo = viewModel.fundingPathInfo.collectAsStateWithLifecycle()

            val errorLabel = if (pathInfo.value.isPathValid) "" else
                "the path format is invalid... rip" //todo

            val derivPathValue = TextFieldValue(
                text = pathInfo.value.path,
                selection = TextRange(pathInfo.value.path.length)
            )

            val keyboardHeight = keyboardHeightState.collectAsStateWithLifecycle()

            ComposeAppTheme {
                val bottomReservedHeight = with(LocalDensity.current) { keyboardHeight.value.toDp() }

                ComposeFundingDerivPathScreen(
                    derivPathValue,
                    ::onDerivPathValueChange,
                    AuthRepository.FUNDING_WALLET_ROOT_PATH,
                    errorLabel,
                    pathInfo.value.isPathValidStrict,
                    ::cancelAdding,
                    ::onPathChosenSelected,
                    bottomReservedHeight
                )
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    viewModel.onAddingCancelled()

                    isEnabled = false
                    findNavController().popBackStack(R.id.addedPaymailsFragment, false)
                }

            }
        )

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            availableHeightHelper.setup(
                requireContext(),
                requireActivity().window,
                viewLifecycleOwner,
                { requireView() }
            )

            availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
                height ?: return@Observer

                val maxHeight = view.height - appliedInsetsState.value.top - appliedInsetsState.value.bottom
                val keyboardHeight = max(maxHeight - height, 0)

                keyboardHeightState.value = keyboardHeight
            })

//            viewLifecycleOwner.lifecycleScope.launch {
//                repeatOnLifecycle(Lifecycle.State.STARTED) {
//                    keyboardHeightState.collectLatest { keyboardHeight ->
//                        onKeyboardHeightChange(keyboardHeight)
//                    }
//                }
//            }
        }

        view.doOnApplyWindowInsets { v, windowInsets, _ ->
            val appliedInsets = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                AppliedInsets(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                AppliedInsets(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight
                )
            }

            v.updatePadding(
                top = appliedInsets.top,
                bottom = appliedInsets.bottom,
                left = appliedInsets.left,
                right = appliedInsets.right
            )

            appliedInsetsState.value = appliedInsets

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val keyboardHeight = max(
                    windowInsets.getInsets(WindowInsets.Type.ime()).bottom -
                            windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    0
                )

                keyboardHeightState.value = keyboardHeight

//                onKeyboardHeightChange(keyboardHeight)
            }

            windowInsets
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.eventFlow.collectLatest { event ->
                    when (event) {
                        is PaymailManagementViewModel.UiEvent.ProceedToAuthorization -> {
                            findNavController().navigate(
                                R.id.action_fundingDerivPathFragment_to_paymailAuthorizationFragment
                            )
                        }
                        is PaymailManagementViewModel.UiEvent.ProceedToFundingMnemonicInput -> {
                            findNavController().navigate(
                                R.id.action_fundingDerivPathFragment_to_seedPhraseInputFragment,
                                SeedPhraseInputFragment.argsForFundingInput()
                            )
                        }
                        else -> {
                            Timber.d("event not handled: $event")
                        }
                    }
                }
            }
        }
    }

//    private fun onKeyboardHeightChange(keyboardHeight: Int) {
//        buttonContainer.translationY = -1f * keyboardHeight
//        bottomGap.layoutParams.let { lp ->
//            lp.height = keyboardHeight
//            bottomGap.layoutParams = lp
//        }
//    }

    private fun onDerivPathValueChange(value: TextFieldValue) {
        viewModel.onFundingPathChanged(value.text)
    }

    private fun cancelAdding() {
        viewModel.onAddingCancelled()
        findNavController().popBackStack(R.id.addedPaymailsFragment, false)
    }

    private fun onPathChosenSelected() {
        viewModel.onFundingPathSubmit()
    }

}