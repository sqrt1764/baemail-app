package app.bitcoin.baemail.paymail.presentation

import android.content.Context
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.ViewOutlineProvider
import android.view.WindowInsets
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import app.bitcoin.baemail.core.presentation.util.OnAfterChangeTextWatcher
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.max

class PaymailIdInputFragment : Fragment(R.layout.fragment_paymail_id_input) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PaymailManagementViewModel


    private lateinit var container: CoordinatorLayout
    private lateinit var buttonContainer: ConstraintLayout

    private lateinit var coordinator: CoordinatorLayout

    private lateinit var bottomGap: View
    private lateinit var input: EditText
    private lateinit var cancel: TextView
    private lateinit var next: TextView
    private lateinit var inputHint: TextView
    private lateinit var errorLabel: TextView


    private val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorNextBackground: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }

    private val colorNextBackgroundDisabled: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimaryVariant)
    }

    private val drawableNextBackground: ColorDrawable by lazy {
        ColorDrawable(colorNextBackgroundDisabled)
    }

    private val drawableNextSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private val availableHeightHelper = AvailableScreenHeightHelper()
    private val keyboardHeightState = MutableStateFlow(0)
    private val appliedInsetsState = MutableStateFlow(AppliedInsets())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(
            viewModelStoreOwner,
            viewModelFactory
        )[PaymailManagementViewModel::class.java]

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        container = requireView().findViewById(R.id.container2)
        buttonContainer = requireView().findViewById(R.id.button_container)
        coordinator = requireView().findViewById(R.id.coordinator)
        bottomGap = requireView().findViewById(R.id.bottom_gap)
        input = requireView().findViewById(R.id.input)
        cancel = requireView().findViewById(R.id.cancel)
        next = requireView().findViewById(R.id.next)
        inputHint = requireView().findViewById(R.id.input_hint)
        errorLabel = requireView().findViewById(R.id.error_label)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            availableHeightHelper.setup(
                requireContext(),
                requireActivity().window,
                viewLifecycleOwner,
                { requireView() }
            )

            availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
                height ?: return@Observer

                val maxHeight = container.height - appliedInsetsState.value.top - appliedInsetsState.value.bottom
                val keyboardHeight = max(maxHeight - height, 0)

                keyboardHeightState.value = keyboardHeight
            })

            viewLifecycleOwner.lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    keyboardHeightState.collectLatest { keyboardHeight ->
                        onKeyboardHeightChange(keyboardHeight)
                    }
                }
            }
        }

        container.doOnApplyWindowInsets { v, windowInsets, _ ->
            val appliedInsets = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                AppliedInsets(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                AppliedInsets(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight
                )
            }

            v.updatePadding(
                top = appliedInsets.top,
                bottom = appliedInsets.bottom,
                left = appliedInsets.left,
                right = appliedInsets.right
            )

            appliedInsetsState.value = appliedInsets

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val keyboardHeight = max(
                    windowInsets.getInsets(WindowInsets.Type.ime()).bottom -
                            windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    0
                )

                keyboardHeightState.value = keyboardHeight

                onKeyboardHeightChange(keyboardHeight)
            }

            windowInsets
        }



        cancel.clipToOutline = true
        cancel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }
        cancel.background = LayerDrawable(arrayOf(
            ColorDrawable(requireContext().getColorFromAttr(R.attr.colorPrimary)),
            DrawableState.getNew(colorSelectorOnAccent)
        ))


        next.clipToOutline = true
        next.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        next.background = LayerDrawable(arrayOf(
            drawableNextBackground,
            drawableNextSelector
        ))


        input.addTextChangedListener(object : OnAfterChangeTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                viewModel.onPaymailIdChanged(s.toString())
            }
        })

        cancel.setOnClickListener {
            imm.hideSoftInputFromWindow(cancel.windowToken, 0)
            viewModel.onAddingCancelled()
            findNavController().popBackStack(R.id.addedPaymailsFragment, false)
        }

        next.setOnClickListener {
            findNavController()
                .navigate(R.id.action_paymailIdInputFragment_to_paymailDerivPathFragment)
        }


        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {

                    viewModel.onAddingCancelled()

                    isEnabled = false
                    findNavController().popBackStack(R.id.addedPaymailsFragment, false)
                }

            }
        )

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.paymailIdModel.collect { model ->

                    if (model.isAlreadyAdded || !model.isValidAddress ||
                        model.paymailExistence !is PaymailExistence.Valid) {
                        next.isEnabled = false
                        drawableNextBackground.color = colorNextBackgroundDisabled
                    } else {
                        next.isEnabled = true
                        drawableNextBackground.color = colorNextBackground
                    }

                    if (model.isAlreadyAdded) {
                        errorLabel.visibility = View.VISIBLE
                        errorLabel.text = getString(R.string.paymail_id_input_fragment__error_label_id_taken)
                    } else if (model.shouldShowInvalidAddressError) {
                        errorLabel.visibility = View.VISIBLE
                        errorLabel.text = getString(R.string.paymail_id_input_fragment__error_label_invalid_paymail)
                    } else if (model.paymailExistence is PaymailExistence.Checking) {
                        if (model.paymailExistence.paymail.isBlank()) {
                            errorLabel.visibility = View.INVISIBLE
                        } else {
                            errorLabel.visibility = View.VISIBLE
                            errorLabel.text = getString(R.string.paymail_checking_existence)
                        }

                    } else if (model.paymailExistence is PaymailExistence.Invalid) {
                        errorLabel.visibility = View.VISIBLE
                        errorLabel.text = getString(R.string.paymail_adding_error__paymail_not_found)

                    } else {
                        errorLabel.visibility = View.INVISIBLE
                    }

                    if (model.input.isEmpty()) {
                        inputHint.visibility = View.VISIBLE
                    } else {
                        inputHint.visibility = View.INVISIBLE
                    }

                }
            }
        }

    }

    private fun onKeyboardHeightChange(keyboardHeight: Int) {
        buttonContainer.translationY = -1f * keyboardHeight
        bottomGap.layoutParams.let { lp ->
            lp.height = keyboardHeight
            bottomGap.layoutParams = lp
        }
    }

    private fun focusOnInput() {
        if (input.requestFocus()) {
            Timber.d("onStart got focus")

            if (!imm.isActive(input)) imm.restartInput(input)
        }
    }

    override fun onStart() {
        super.onStart()

        Timber.d("onStart $imm")
        focusOnInput()
    }

}