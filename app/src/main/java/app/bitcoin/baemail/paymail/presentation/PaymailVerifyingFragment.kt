package app.bitcoin.baemail.paymail.presentation

import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.paymail.domain.usecases.ErrorAliasMnemonicMismatch
import app.bitcoin.baemail.paymail.domain.usecases.ErrorAliasNotFound
import app.bitcoin.baemail.paymail.domain.usecases.ErrorInvalidFundingMnemonic
import app.bitcoin.baemail.paymail.domain.usecases.ErrorInvalidMnemonic
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class PaymailVerifyingFragment : DialogFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: PaymailManagementViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(
            viewModelStoreOwner,
            viewModelFactory
        )[PaymailManagementViewModel::class.java]

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_verify_paymail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        requireView().clipToOutline = true
        requireView().outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = resources.displayMetrics.density * 13;

                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        isCancelable = false
        dialog?.setCanceledOnTouchOutside(false)


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.eventFlow.collectLatest { event ->
                    when (event) {
                        is PaymailManagementViewModel.UiEvent.SuccessPaymailInfoValid -> {
                            dismiss()
                            findNavController().navigate(
                                R.id.action_paymailVerifyingFragment_to_fundingWalletConfigFragment
                            )
                        }
                        is PaymailManagementViewModel.UiEvent.FailPaymailInfoInvalid -> {

                            when (event.error) {
                                is ErrorAliasNotFound -> {
                                    findNavController().popBackStack(
                                        R.id.paymailIdInputFragment,
                                        false
                                    )
                                }
                                is ErrorInvalidMnemonic, is ErrorAliasMnemonicMismatch -> {
                                    dismiss()
                                }
                                is ErrorInvalidFundingMnemonic -> {
                                    dismiss()
                                }
                                else -> {
                                    dismiss()
                                }
                            }

                        }
                        else -> {
                            dismiss()
                        }
                    }
                }
            }
        }


    }
}