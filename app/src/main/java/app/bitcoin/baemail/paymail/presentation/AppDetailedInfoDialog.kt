package app.bitcoin.baemail.paymail.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.util.RoundedBottomSheetDialogFragment
import dagger.android.support.AndroidSupportInjection

class AppDetailedInfoDialog : RoundedBottomSheetDialogFragment() {

//    @Inject
//    lateinit var viewModelFactory: ViewModelProvider.Factory


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detailed_app_info, container, false)
    }


    override fun getPeekHeight(): Int {
        return CONST_UNSET
    }

    override fun shouldFitToContents(): Boolean {
        return true
    }
}