package app.bitcoin.baemail.paymail.presentation

import android.content.Context
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.ViewOutlineProvider
import android.view.WindowInsets
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.util.OnAfterChangeTextWatcher
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.core.presentation.util.DerivationPathInputFilter
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.max

class PaymailDerivPathFragment : Fragment(R.layout.fragment_paymail_deriv_path)  {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PaymailManagementViewModel


    private lateinit var container: CoordinatorLayout
    private lateinit var buttonContainer: ConstraintLayout

    private lateinit var bottomGap: View
    private lateinit var input: EditText
    private lateinit var cancel: TextView
    private lateinit var next: TextView

    private lateinit var info0a: TextView
    private lateinit var info1: TextView

    private val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorNextBackground: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }

    private val colorNextBackgroundDisabled: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimaryVariant)
    }

    private val drawableNextBackground: ColorDrawable by lazy {
        ColorDrawable(colorNextBackgroundDisabled)
    }

    private val drawableNextSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private val availableHeightHelper = AvailableScreenHeightHelper()
    private val keyboardHeightState = MutableStateFlow(0)
    private val appliedInsetsState = MutableStateFlow(AppliedInsets())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(
            viewModelStoreOwner,
            viewModelFactory
        )[PaymailManagementViewModel::class.java]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        container = requireView().findViewById(R.id.container)
        buttonContainer = requireView().findViewById(R.id.button_container)
        bottomGap = requireView().findViewById(R.id.bottom_gap)
        input = requireView().findViewById(R.id.input)
        cancel = requireView().findViewById(R.id.cancel)
        next = requireView().findViewById(R.id.next)
        info0a = requireView().findViewById(R.id.info_0a)
        info1 = requireView().findViewById(R.id.info_1)



        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            availableHeightHelper.setup(
                requireContext(),
                requireActivity().window,
                viewLifecycleOwner,
                { requireView() }
            )

            availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
                height ?: return@Observer

                val maxHeight = container.height - appliedInsetsState.value.top - appliedInsetsState.value.bottom
                val keyboardHeight = max(maxHeight - height, 0)

                keyboardHeightState.value = keyboardHeight
            })

            viewLifecycleOwner.lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    keyboardHeightState.collectLatest { keyboardHeight ->
                        onKeyboardHeightChange(keyboardHeight)
                    }
                }
            }
        }

        container.doOnApplyWindowInsets { v, windowInsets, _ ->
            val appliedInsets = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                AppliedInsets(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                AppliedInsets(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight
                )
            }

            v.updatePadding(
                top = appliedInsets.top,
                bottom = appliedInsets.bottom,
                left = appliedInsets.left,
                right = appliedInsets.right
            )

            appliedInsetsState.value = appliedInsets

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val keyboardHeight = max(
                    windowInsets.getInsets(WindowInsets.Type.ime()).bottom -
                            windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    0
                )

                keyboardHeightState.value = keyboardHeight

                onKeyboardHeightChange(keyboardHeight)
            }

            windowInsets
        }


        cancel.clipToOutline = true
        cancel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }


        cancel.background = LayerDrawable(arrayOf(
            ColorDrawable(colorNextBackground),
            DrawableState.getNew(colorSelectorOnAccent)
        ))


        next.clipToOutline = true
        next.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        next.background = LayerDrawable(arrayOf(
            drawableNextBackground,
            drawableNextSelector
        ))


        input.addTextChangedListener(object : OnAfterChangeTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                viewModel.onPaymailPkiPathChanged(s.toString())
            }
        })
        input.filters = arrayOf(DerivationPathInputFilter(viewModel.checkIsValidPkiPathUseCase))

        cancel.setOnClickListener {
            imm.hideSoftInputFromWindow(cancel.windowToken, 0)
            viewModel.onAddingCancelled()
            findNavController().popBackStack(R.id.addedPaymailsFragment, false)
        }

        next.setOnClickListener {
            findNavController()
                .navigate(
                    R.id.action_paymailDerivPathFragment_to_seedPhraseInputFragment,
                    SeedPhraseInputFragment.argsForPaymailInput()
                )
        }


        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {

                    viewModel.onAddingCancelled()

                    isEnabled = false
                    findNavController().popBackStack(R.id.addedPaymailsFragment, false)
                }

            }
        )


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.paymailPath.collectLatest { model ->

                    Timber.d("....pkiPath:${model.pkiPath}")
                    if (model.pkiPath == null) {
                        input.setText("")
                    } else if (input.text.toString() != model.pkiPath) {
                        input.setText(model.pkiPath)
                        if (model.pkiPath.isNotBlank())
                            input.setSelection(model.pkiPath.length - 1)
                    }

                    if (model.pkiPathDefault.isNullOrBlank()) {
                        info0a.text = getString(R.string.paymail_deriv_path_info_1_0)
                    } else {
                        info0a.text = getString(R.string.paymail_deriv_path_info_0a1, model.pkiPathDefault)
                    }

                    if (!model.isPkiPathValid) {
                        next.isEnabled = false
                        drawableNextBackground.color = colorNextBackgroundDisabled
                    } else {
                        next.isEnabled = true
                        drawableNextBackground.color = colorNextBackground
                    }


                }
            }
        }

    }

    private fun onKeyboardHeightChange(keyboardHeight: Int) {
        buttonContainer.translationY = -1f * keyboardHeight
        bottomGap.layoutParams.let { lp ->
            lp.height = keyboardHeight
            bottomGap.layoutParams = lp
        }
    }

    private fun focusOnInput() {
        if (input.requestFocus()) {
            Timber.d("onStart got focus")

            if (!imm.isActive(input)) imm.restartInput(input)
        }
    }

    override fun onStart() {
        super.onStart()

        Timber.d("onStart $imm")
        focusOnInput()
    }
}