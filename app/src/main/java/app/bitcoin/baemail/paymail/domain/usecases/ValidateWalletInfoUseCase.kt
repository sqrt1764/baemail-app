package app.bitcoin.baemail.paymail.domain.usecases

import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository

class ValidateWalletInfoUseCaseImpl(
    private val nodeRuntimeRepository: NodeRuntimeRepository
) : ValidateWalletInfoUseCase {
    override suspend fun invoke(mnemonic: List<String>, derivationPath: String): Boolean {
        val keyHolder = nodeRuntimeRepository.getPrivateKeyForPathFromSeed(
            derivationPath + "/0",
            mnemonic
        )

        return keyHolder != null
    }

}

interface ValidateWalletInfoUseCase {
    suspend operator fun invoke(mnemonic: List<String>, derivationPath: String): Boolean
}