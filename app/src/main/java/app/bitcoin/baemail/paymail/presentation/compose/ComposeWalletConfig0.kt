package app.bitcoin.baemail.paymail.presentation.compose

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme


@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode of ComposeWalletConfig"
)
@Composable
fun ComposeWalletConfigPreview0() {

    val fundingWalletType = remember {
        mutableStateOf(FundingWallet.DETERMINISTIC)
    }

    val defaultDeterministicTypePath = "m/44'/0'/0'"
    val deterministicTypePath = remember {
        mutableStateOf(defaultDeterministicTypePath)
    }
    val deterministicTypeErrorMessage = stringResource(R.string.key_path_error_info)

    val defaultPaymailTypePath = "m/44'/0'/0'"
    val paymailTypePath = remember {
        mutableStateOf(defaultDeterministicTypePath)
    }
    val paymailTypeErrorMessage = ""

    val defaultCustomTypePath = "m/44'/0'/0'"
    val customTypePath = remember {
        mutableStateOf(defaultDeterministicTypePath)
    }
    val customTypeErrorMessage = null

    ComposeAppTheme {
        ComposeWalletConfig0(
            fundingWalletType,
            defaultDeterministicTypePath,
            deterministicTypePath,
            deterministicTypeErrorMessage,
            defaultPaymailTypePath,
            paymailTypePath,
            paymailTypeErrorMessage,
            defaultCustomTypePath,
            customTypePath,
            customTypeErrorMessage
        )
    }
}


@Composable
fun ComposeWalletConfig0(
    fundingWalletType: MutableState<FundingWallet>,
    defaultDeterministicTypePath: String,
    deterministicTypePath: MutableState<String>,
    deterministicTypeErrorMessage: String?,
    defaultPaymailTypePath: String,
    paymailTypePath: MutableState<String>,
    paymailTypeErrorMessage: String?,
    defaultCustomTypePath: String,
    customTypePath: MutableState<String>,
    customTypeErrorMessage: String?
) {
    val selectionTitleSize = MaterialTheme.typography.body1.fontSize.times(1.5f)
    val selectedBorderColor = MaterialTheme.colors.primary
    val unselectedBorderColor = MaterialTheme.colors.onBackground.copy(alpha = 0.3f)

    val deterministicTypeStrokeColor =
        if (FundingWallet.DETERMINISTIC == fundingWalletType.value) {
            selectedBorderColor
        } else {
            unselectedBorderColor
        }

    val paymailTypeStrokeColor = if (FundingWallet.PAYMAIL == fundingWalletType.value) {
        selectedBorderColor
    } else {
        unselectedBorderColor
    }

    val customTypeStrokeColor = if (FundingWallet.CUSTOM == fundingWalletType.value) {
        selectedBorderColor
    } else {
        unselectedBorderColor
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp)
    ) {
        Spacer(Modifier.height(24.dp))
        Text(
            stringResource(R.string.funding_wallet),
            style = MaterialTheme.typography.h1
        )
        Spacer(Modifier.height(16.dp))
        Text(
            stringResource(R.string.funding_wallet_info),
            style = MaterialTheme.typography.body1
        )
        Spacer(Modifier.height(16.dp))
        Box(
            Modifier
                .border(
                    BorderStroke(2.5.dp, deterministicTypeStrokeColor),
                    MaterialTheme.shapes.medium
                )
                .clip(RoundedCornerShape(2.5.dp))
                .clickable {
                    fundingWalletType.value = FundingWallet.DETERMINISTIC
                }) {
            Column(modifier = Modifier.padding(horizontal = 12.dp, vertical = 8.dp)) {
                Text(
                    stringResource(R.string.deterministic),
                    style = MaterialTheme.typography.body1,
                    fontSize = selectionTitleSize
                )
                Text(
                    stringResource(R.string.deterministic_info),
                    style = MaterialTheme.typography.caption
                )
                Spacer(Modifier.height(10.dp))
                DerivationPathInput0(
                    deterministicTypePath.value,
                    {
                        deterministicTypePath.value = it
                    },
                    defaultDeterministicTypePath,
                    stringResource(R.string.path)
                )
                if (!deterministicTypeErrorMessage.isNullOrBlank()) {
                    Text(
                        deterministicTypeErrorMessage,
                        color = MaterialTheme.colors.error,
                        style = MaterialTheme.typography.caption
                    )
                }
                Spacer(Modifier.height(5.dp))
            }
        }
        Spacer(Modifier.height(8.dp))
        Box(modifier = Modifier
            .border(
                BorderStroke(2.5.dp, paymailTypeStrokeColor),
                MaterialTheme.shapes.medium
            )
            .clip(RoundedCornerShape(2.5.dp))
            .clickable {
                fundingWalletType.value = FundingWallet.PAYMAIL
            }) {
            Column(modifier = Modifier.padding(horizontal = 12.dp, vertical = 8.dp)) {
                Text(
                    stringResource(R.string.paymail_mnemonic),
                    style = MaterialTheme.typography.body1,
                    fontSize = selectionTitleSize
                )
                Text(
                    stringResource(R.string.paymail_mnemonic_info),
                    style = MaterialTheme.typography.caption
                )
                Spacer(Modifier.height(10.dp))
                DerivationPathInput0(
                    paymailTypePath.value,
                    {
                        paymailTypePath.value = it
                    },
                    defaultPaymailTypePath,
                    stringResource(R.string.path)
                )
                if (!paymailTypeErrorMessage.isNullOrBlank()) {
                    Text(
                        paymailTypeErrorMessage,
                        color = MaterialTheme.colors.error,
                        style = MaterialTheme.typography.caption
                    )
                }
                Spacer(Modifier.height(5.dp))
            }
        }
        Spacer(Modifier.height(8.dp))
        Box(modifier = Modifier
            .border(
                BorderStroke(2.5.dp, customTypeStrokeColor),
                MaterialTheme.shapes.medium
            )
            .clip(RoundedCornerShape(2.5.dp))
            .clickable {
                fundingWalletType.value = FundingWallet.CUSTOM
            }) {
            Column(modifier = Modifier.padding(horizontal = 12.dp, vertical = 8.dp)) {
                Text(
                    stringResource(R.string.custom),
                    style = MaterialTheme.typography.body1,
                    fontSize = selectionTitleSize
                )
                Text(
                    stringResource(R.string.custom_info),
                    style = MaterialTheme.typography.caption
                )
                Spacer(Modifier.height(10.dp))
                DerivationPathInput0(
                    customTypePath.value,
                    {
                        customTypePath.value = it
                    },
                    defaultCustomTypePath,
                    stringResource(R.string.path)
                )
                if (!customTypeErrorMessage.isNullOrBlank()) {
                    Text(
                        customTypeErrorMessage,
                        color = MaterialTheme.colors.error,
                        style = MaterialTheme.typography.caption
                    )
                }
                Spacer(Modifier.height(5.dp))
            }
        }
    }
}


@Composable
fun DerivationPathInput0(
    value: String,
    onValueChanged: (String) -> Unit,
    defaultValue: String,
    description: String
) {
    Surface(
        shape = MaterialTheme.shapes.small,
        color = MaterialTheme.colors.onSurface.copy(alpha = 0.18f),
    ) {
        Text(
            description,
            style = MaterialTheme.typography.caption,
            modifier = Modifier.padding(start = 16.dp, top = 8.dp)
        )
        TextField(
            value = value,
            onValueChange = onValueChanged,
            modifier = Modifier
                .semantics { contentDescription = description }
                .fillMaxWidth()
                .padding(top = Dp(MaterialTheme.typography.caption.fontSize.value).plus(4.dp)),
            singleLine = true,
            keyboardOptions = KeyboardOptions.Default.copy(
                autoCorrect = false,
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Done
            ),
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Color.Transparent,
                unfocusedBorderColor = Color.Transparent,
            ),
            placeholder = {
                Text(
                    defaultValue,
                    style = MaterialTheme.typography.subtitle1
                )
            }
        )
    }
}
