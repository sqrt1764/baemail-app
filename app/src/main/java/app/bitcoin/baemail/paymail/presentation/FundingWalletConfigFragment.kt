package app.bitcoin.baemail.paymail.presentation

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import androidx.activity.OnBackPressedCallback
import androidx.compose.ui.platform.ComposeView
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.paymail.presentation.compose.ComposeWalletConfigScreen
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class FundingWalletConfigFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PaymailManagementViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(
            viewModelStoreOwner,
            viewModelFactory
        )[PaymailManagementViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            val fundingWalletType = viewModel.fundingWalletType.collectAsStateWithLifecycle()

            ComposeAppTheme {
                ComposeWalletConfigScreen(
                    fundingWalletType,
                    viewModel::onFundingWalletTypeChange,
                    this@FundingWalletConfigFragment::cancelAdding,
                    this@FundingWalletConfigFragment::onTypeSelected
                )
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    viewModel.onAddingCancelled()

                    isEnabled = false
                    findNavController().popBackStack(R.id.addedPaymailsFragment, false)
                }

            }
        )

        view.doOnApplyWindowInsets { v, windowInsets, _ ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                v.updatePadding(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                v.updatePadding(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight,
                )
            }

            windowInsets
        }
    }

    private fun cancelAdding() {
        viewModel.onAddingCancelled()
        findNavController().popBackStack(R.id.addedPaymailsFragment, false)
    }

    private fun onTypeSelected() {
        findNavController().navigate(
            R.id.action_fundingWalletConfigFragment_to_fundingDerivPathFragment
        )
    }
}