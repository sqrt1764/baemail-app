package app.bitcoin.baemail.paymail.domain.usecases

import app.bitcoin.baemail.core.domain.repository.AuthRepository


class SwitchActivePaymailUseCaseImpl(
    private val authRepository: AuthRepository
) : SwitchActivePaymailUseCase {

    override fun invoke(paymail: String) = authRepository.changeActivePaymail(paymail)

}

interface SwitchActivePaymailUseCase {
    operator fun invoke(paymail: String)
}