package app.bitcoin.baemail.paymail.domain.usecases

import app.bitcoin.baemail.core.data.wallet.sending.PaymailInfo
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.domain.repository.PaymailInfoRepository
import app.bitcoin.baemail.paymail.domain.entity.PaymailValidation
import kotlinx.coroutines.async
import kotlinx.coroutines.supervisorScope
import timber.log.Timber


class ValidatePaymailUseCaseImpl(
    private val paymailInfoRepository: PaymailInfoRepository,
    private val nodeRuntimeRepository: NodeRuntimeRepository
) : ValidatePaymailUseCase {
    override suspend fun invoke(
        paymail: String,
        pkiPath: String,
        seedList: List<String>
    ): PaymailValidation {
        var bsvAliasResult: Pair<PaymailInfo?, Exception?>? = null
        var publicKeyForSeed: String? = null

        supervisorScope {
            val deferredAlias = async {
                try {
                    paymailInfoRepository.paymailExistsHelper.checkPaymailExists(paymail)
                } catch (e: Exception) {
                    Timber.e(e, "crash retrieving alias")
                    null
                }
            }

            val deferredGetPublicKeyForSeed = async {
                try {
                    //keys for the specified path
                    nodeRuntimeRepository.getPrivateKeyForPathFromSeed(
                        pkiPath,
                        seedList
                    ) ?: let {
                        Timber.d("error retrieving keys for path")
                        null
                    }
                } catch (e: Exception) {
                    Timber.e(e, "crash getting pub-key from seed")
                    null
                }

            }

            bsvAliasResult = deferredAlias.await()
            Timber.d("validatePaymail ... pki-publicKey: ${bsvAliasResult?.first?.alias?.pubkey}")
            publicKeyForSeed = deferredGetPublicKeyForSeed.await()?.publicKey
        }

        val alias = bsvAliasResult?.first?.alias ?: return PaymailValidation.ALIAS_NOT_FOUND
        publicKeyForSeed ?: return PaymailValidation.INVALID_MNEMONIC

        return if (alias.pubkey == publicKeyForSeed) {
            PaymailValidation.VALID
        } else {
            PaymailValidation.ALIAS_MNEMONIC_MISMATCH
        }
    }

}

interface ValidatePaymailUseCase {
    suspend operator fun invoke(paymail: String, pkiPath: String, seedList: List<String>): PaymailValidation
}