package app.bitcoin.baemail.paymail.presentation.compose

import android.content.res.Configuration
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import kotlinx.coroutines.android.awaitFrame
import timber.log.Timber


@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode of ComposeFundingDerivPath"
)
@Composable
fun ComposeFundingDerivPathPreview() {
    ComposeAppTheme {
        ComposeFundingDerivPathScreen(
            TextFieldValue("m/"),
            {},
            "m/0/256'/0",
            "path pattern is not valid",
            true,
            {},
            {},
            0.dp
        )
    }
}

@Composable
fun ComposeFundingDerivPathScreen(
    value: TextFieldValue,
    onValueChanged: (TextFieldValue) -> Unit,
    defaultDerivValue: String,
    errorLabel: String?,
    isPathValid: Boolean,
    onClickCancel: ()->Unit,
    onClickNext: ()->Unit,
    reservedBottomHeight: Dp,
    modifier: Modifier = Modifier
) {
    val buttonTextFontStyle = MaterialTheme.typography.h2.copy(
        fontWeight = FontWeight.Normal,
        fontSize = 30.sp
    )

    Box(modifier) {
        ComposeFundingDerivPath(value, onValueChanged, defaultDerivValue, errorLabel)
        Row(
            Modifier
                .fillMaxWidth()
                .align(Alignment.BottomStart)
                .padding(start = 16.dp, end = 16.dp, bottom = reservedBottomHeight.plus(16.dp))
        ) {
            Button(
                modifier = Modifier
                    .weight(1f, true)
                    .clip(RoundedCornerShape(50)),
                onClick = onClickCancel
            ) {
                Text(
                    stringResource(id = R.string.cancel),
                    modifier = Modifier.padding(vertical = 6.dp),
                    style = buttonTextFontStyle
                )
            }
            Spacer(modifier = Modifier.size(16.dp))
            Button(
                modifier = Modifier
                    .weight(1f, true)
                    .clip(RoundedCornerShape(50)),
                onClick = onClickNext,
                enabled = isPathValid
            ) {
                Text(
                    stringResource(id = R.string.next),
                    modifier = Modifier.padding(vertical = 6.dp),
                    style = buttonTextFontStyle
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ComposeFundingDerivPath(
    value: TextFieldValue,
    onValueChanged: (TextFieldValue) -> Unit,
    defaultDerivValue: String,
    errorLabel: String?
) {
    val scrollState = rememberScrollState()

    val info1FontStyle = MaterialTheme.typography.body1.copy(
        fontWeight = FontWeight.Normal,
        fontSize = 17.sp,
        color = Color(LocalContext.current.getColorFromAttr(R.attr.colorOnSurfaceSubtle))
    )

    val errorFontStyle = MaterialTheme.typography.body1.copy(
        fontWeight = FontWeight.Normal,
        fontSize = 17.sp,
        color = Color(LocalContext.current.getColorFromAttr(R.attr.colorError))
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp)
            .verticalScroll(scrollState)
    ) {
        Spacer(Modifier.height(24.dp))
        Text(
            stringResource(R.string.funding_path_title),
            style = MaterialTheme.typography.h1
        )
        Text(
            stringResource(R.string.funding_path_subtitle),
            style = MaterialTheme.typography.body1
        )
        Spacer(Modifier.height(24.dp))
        Text(
            stringResource(R.string.funding_path_info),
            style = MaterialTheme.typography.body1
        )
        Spacer(Modifier.height(24.dp))
        DerivationPathInput(
            value,
            onValueChanged,
            defaultDerivValue,
            stringResource(R.string.funding_path_label)
        )
        Spacer(Modifier.height(8.dp))
        Surface(onClick = {
            onValueChanged(TextFieldValue(defaultDerivValue))
        }) {
            Text(
                stringResource(R.string.funding_path_info1, defaultDerivValue),
                style = info1FontStyle,
            )
        }
        Spacer(Modifier.height(8.dp))
        Text(
            errorLabel ?: "",
            style = errorFontStyle
        )
    }
}


@Composable
fun DerivationPathInput(
    value: TextFieldValue,
    onValueChanged: (TextFieldValue) -> Unit,
    defaultValue: String,
    description: String
) {
    val focusRequester = remember { FocusRequester() }

    Surface(
        shape = MaterialTheme.shapes.small,
        color = MaterialTheme.colors.onSurface.copy(alpha = 0.18f),
    ) {
        Text(
            description,
            style = MaterialTheme.typography.caption,
            modifier = Modifier.padding(start = 16.dp, top = 8.dp)
        )
        TextField(
            value = value,
            onValueChange = onValueChanged,
            modifier = Modifier
                .semantics { contentDescription = description }
                .fillMaxWidth()
                .padding(top = Dp(MaterialTheme.typography.caption.fontSize.value).plus(4.dp))
                .focusRequester(focusRequester),
            singleLine = true,
            keyboardOptions = KeyboardOptions.Default.copy(
                autoCorrect = false,
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done
            ),
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Color.Transparent,
                unfocusedBorderColor = Color.Transparent,
            ),
            placeholder = {
                Text(
                    defaultValue,
                    style = MaterialTheme.typography.subtitle1
                )
            }
        )
    }

    LaunchedEffect(focusRequester) {
        awaitFrame()
        focusRequester.requestFocus()
    }
}
