package app.bitcoin.baemail.paymail.presentation

import android.content.Intent
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.view.WindowInsets
import android.widget.TextView
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.SplashActivity
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.twetch.ui.TwetchFilesMimeTypeHelper
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.logs.presentation.LogsActivity
import app.bitcoin.baemail.paymail.presentation.compose.ComposeAddedPaymailsHeader
import com.google.android.material.appbar.AppBarLayout
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class AddedPaymailsFragment : Fragment(R.layout.fragment_added_paymails) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: PaymailManagementViewModel


    lateinit var container: ConstraintLayout
    lateinit var recycler: RecyclerView
    lateinit var appBar: AppBarLayout
    lateinit var addPaymail: TextView
    lateinit var headerComposable: ComposeView

    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(
            viewModelStoreOwner,
            viewModelFactory
        )[PaymailManagementViewModel::class.java]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        container = requireView().findViewById(R.id.container)
        recycler = requireView().findViewById(R.id.recycler)
        appBar = requireView().findViewById(R.id.app_bar)
        addPaymail = requireView().findViewById(R.id.add_paymail)
        headerComposable = requireView().findViewById(R.id.header_composable)

        val topBarElevation = resources.getDimension(R.dimen.added_paymails_fragment__top_bar_elevation)

        container.doOnApplyWindowInsets { v, windowInsets, _ ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                v.updatePadding(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                v.updatePadding(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight,
                )
            }

            windowInsets
        }

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atAuthenticatedPaymailListener = object : BaseListAdapter.ATAuthenticatedPaymailListener {
                    override fun onClick(paymail: String) {
                        viewLifecycleOwner.lifecycleScope.launch {
                            viewModel.onChangeActivePaymail(paymail)

                            delay(300)

                            val intent = Intent(requireActivity(), SplashActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            intent.putExtra(SplashActivity.KEY_IS_CHANGING_ACTIVE_PAYMAIL, true)
                            startActivity(intent)
                            requireActivity().finishAffinity()
                        }

                    }

                }
            )
        )

        layoutManager = LinearLayoutManager(context)
        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER



        appBar.elevation = 0f
        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (adapter.itemCount == 0) {
                    appBar.elevation = 0f
                    return
                }
                when (layoutManager.findFirstCompletelyVisibleItemPosition()) {
                    RecyclerView.NO_POSITION -> false
                    0 -> false
                    else -> true
                }.let { showShadow ->
                    if (showShadow) {
                        appBar.elevation = topBarElevation
                    } else {
                        appBar.elevation = 0f
                    }
                }
            }
        })

        val colorAddPaymailBackground = requireContext().getColorFromAttr(R.attr.colorPrimary)
        val colorSelectorAddPaymailBackground = R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        addPaymail.background = LayerDrawable(arrayOf(
            ColorDrawable(colorAddPaymailBackground),
            DrawableState.getNew(colorSelectorAddPaymailBackground)
        ))

        addPaymail.clipToOutline = true
        addPaymail.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val cornerRoundness = view.height / 2f

                outline.setRoundRect(0, 0, view.width, view.height, cornerRoundness)
            }

        }

        addPaymail.setOnClickListener {
            findNavController().navigate(R.id.action_addedPaymailsFragment_to_appIntroFragment)
        }


        headerComposable.setContent {
            val availableOptions = listOf(
                "config" to stringResource(id = R.string.app_config_title),
                "logs" to stringResource(id = R.string.logs),
            )
            val onItemSelected: (String) -> Unit = { key ->
                when (key) {
                    "config" -> {
                        findNavController().navigate(R.id.action_addedPaymailsFragment_to_appConfigFragment)
                    }
                    "logs" -> {
                        startActivity(Intent(requireActivity(), LogsActivity::class.java), null)
                    }
                }
            }

            ComposeAppTheme {
                ComposeAddedPaymailsHeader(
                    items = availableOptions,
                    onItemSelected = onItemSelected
                )
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.addedPaymails.collect {
                    adapter.setItems(it)
                }
            }
        }

    }

}