package app.bitcoin.baemail.paymail.presentation.compose

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.ButtonColors
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import java.lang.StringBuilder

@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode of ComposePaymailAuthorization"
)
@Composable
fun ComposePaymailAuthorizationPreview() {
    ComposeAppTheme {
        ComposePaymailAuthorizationScreen(
            "satoshi@paymail.com",
            "m/0",
            listOf(
                "achieve",
                "address",
                "venture",
                "valley",
                "utility",
                "trust",
                "travel",
                "switch",
                "summer",
                "sponsor",
                "snap",
                "scan",
            ),
            "m/0/0",
            listOf(
                "achieve",
                "address",
                "venture",
                "valley",
                "utility",
                "trust",
                "travel",
                "switch",
                "summer",
                "sponsor",
                "snap",
                "scan",
            ),
            true,
            {},
            {},
            {}
        )
    }
}

@Composable
fun ComposePaymailAuthorizationScreen (
    paymail: String,
    paymailPath: String,
    paymailMnemonic: List<String>,
    fundingPath: String,
    fundingMnemonic: List<String>,
    isFundingInfoValid: Boolean,
    onClickCancel: () -> Unit,
    onClickNext: () -> Unit,
    copyMnemonic: (Boolean) -> Unit
) {
    val buttonTextFontStyle = MaterialTheme.typography.h2.copy(
        fontWeight = FontWeight.Normal,
        fontSize = 30.sp
    )

    Box {
        ComposePaymailAuthorization(
            paymail,
            paymailPath,
            paymailMnemonic,
            fundingPath,
            fundingMnemonic,
            copyMnemonic
        )
        Row(
            Modifier
                .fillMaxWidth()
                .align(Alignment.BottomStart)
                .padding(start = 16.dp, end = 16.dp, bottom = 8.dp)
        ) {
            Button(
                modifier = Modifier
                    .weight(1f, true)
                    .clip(RoundedCornerShape(50)),
                onClick = onClickCancel
            ) {
                Text(
                    stringResource(id = R.string.cancel),
                    modifier = Modifier.padding(vertical = 6.dp),
                    style = buttonTextFontStyle
                )
            }
            Spacer(modifier = Modifier.size(16.dp))
            Button(
                modifier = Modifier
                    .weight(1f, true)
                    .clip(RoundedCornerShape(50)),
                onClick = onClickNext,
                enabled = isFundingInfoValid
            ) {
                Text(
                    stringResource(id = R.string.finish),
                    modifier = Modifier.padding(vertical = 6.dp),
                    style = buttonTextFontStyle
                )
            }
        }
    }
}

@Composable
fun ComposePaymailAuthorization(
    paymail: String,
    paymailPath: String,
    paymailMnemonic: List<String>,
    fundingPath: String,
    fundingMnemonic: List<String>,
    copyMnemonic: (Boolean) -> Unit
) {

    val scrollState = rememberScrollState()

    val paymailMnemonicShowingState = remember {
        mutableStateOf(false)
    }

    val fundingMnemonicShowingState = remember {
        mutableStateOf(false)
    }

    val sectionTitleSize = MaterialTheme.typography.body1.fontSize.times(1.5f)

    val backgroundColor = MaterialTheme.colors.onBackground.copy(alpha = 0.15f)
    val borderColor = MaterialTheme.colors.onBackground.copy(alpha = 0.3f)

    val buttonColors = ButtonDefaults.outlinedButtonColors(
        backgroundColor = backgroundColor
    )

    val paymailMnemonicString = StringBuilder()
    paymailMnemonic.forEachIndexed { i, item ->
        if (paymailMnemonicShowingState.value) {
            paymailMnemonicString.append(item)
        } else {
            paymailMnemonicString.append(
                String(CharArray(item.length) { "●"[0] })
            )
        }
        if (i + 1 != paymailMnemonic.size)
            paymailMnemonicString.append("  ")
    }

    val fundingMnemonicString = StringBuilder()
    fundingMnemonic.forEachIndexed { i, item ->
        if (fundingMnemonicShowingState.value) {
            fundingMnemonicString.append(item)
        } else {
            fundingMnemonicString.append(
                String(CharArray(item.length) { "●"[0] })
            )
        }
        if (i + 1 != fundingMnemonic.size)
            fundingMnemonicString.append("  ")
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp)
            .verticalScroll(scrollState)
    ) {
        Spacer(Modifier.height(24.dp))
        Text(
            stringResource(R.string.account_overview_title),
            style = MaterialTheme.typography.h1
        )
        Spacer(Modifier.height(24.dp))
        Text(
            stringResource(R.string.account_overview_info0),
            style = MaterialTheme.typography.body1
        )
        Spacer(Modifier.height(24.dp))
        Box(modifier = Modifier
            .fillMaxWidth()
            .background(backgroundColor, MaterialTheme.shapes.large)
        ) {
            Column(modifier = Modifier.padding(horizontal = 12.dp, vertical = 8.dp)) {
                Spacer(Modifier.height(4.dp))
                Text(
                    paymail,
                    style = MaterialTheme.typography.body1,
                    fontSize = sectionTitleSize
                )
                Spacer(Modifier.height(6.dp))
                Text(
                    paymailPath,
                    style = MaterialTheme.typography.caption,
                    fontFamily = FontFamily.Monospace
                )
                Spacer(Modifier.height(8.dp))
                Text(
                    paymailMnemonicString.toString(),
                    style = MaterialTheme.typography.body1,
                    fontFamily = FontFamily.Monospace,
                    lineHeight = MaterialTheme.typography.body1.fontSize * 1.3
                )
                Row {
                    OutlinedButton(
                        onClick = {
                            paymailMnemonicShowingState.value = !paymailMnemonicShowingState.value
                        },
                        border = BorderStroke(2.dp, borderColor),
                        shape = RoundedCornerShape(18.dp),
                        colors = buttonColors
                    ) {
                        val painter = if (paymailMnemonicShowingState.value) {
                            painterResource(R.drawable.vector_ic_password_visible)
                        } else {
                            painterResource(R.drawable.vector_ic_password_masked)
                        }
                        Icon(
                            painter,
                            "Show password",
                            tint = MaterialTheme.colors.onBackground
                        )
                    }
                    Spacer(Modifier.width(5.dp))
                    OutlinedButton(
                        onClick = { copyMnemonic(true) },
                        border = BorderStroke(2.dp, borderColor),
                        shape = RoundedCornerShape(18.dp),
                        colors = buttonColors
                    ) {
                        Icon(
                            painterResource(R.drawable.ic_copy_link),
                            "Copy",
                            tint = MaterialTheme.colors.onBackground
                        )
                    }
                }
                Spacer(Modifier.height(5.dp))
            }
        }
        Spacer(Modifier.height(24.dp))
        Box(modifier = Modifier
            .fillMaxWidth()
            .background(backgroundColor, MaterialTheme.shapes.large)
        ) {
            Column(modifier = Modifier.padding(horizontal = 12.dp, vertical = 8.dp)) {
                Spacer(Modifier.height(4.dp))
                Text(
                    stringResource(R.string.funding_wallet),
                    style = MaterialTheme.typography.body1,
                    fontSize = sectionTitleSize
                )
                Spacer(Modifier.height(6.dp))
                Text(
                    fundingPath,
                    style = MaterialTheme.typography.caption,
                    fontFamily = FontFamily.Monospace
                )
                Spacer(Modifier.height(8.dp))
                Text(
                    fundingMnemonicString.toString(),
                    style = MaterialTheme.typography.body1,
                    fontFamily = FontFamily.Monospace,
                    lineHeight = MaterialTheme.typography.body1.fontSize * 1.3
                )
                Row {
                    OutlinedButton(
                        onClick = {
                            fundingMnemonicShowingState.value = !fundingMnemonicShowingState.value
                        },
                        border = BorderStroke(2.dp, borderColor),
                        shape = RoundedCornerShape(18.dp),
                        colors = buttonColors
                    ) {
                        val painter = if (fundingMnemonicShowingState.value) {
                            painterResource(R.drawable.vector_ic_password_visible)
                        } else {
                            painterResource(R.drawable.vector_ic_password_masked)
                        }
                        Icon(
                            painter,
                            "Show password",
                            tint = MaterialTheme.colors.onBackground
                        )
                    }
                    Spacer(Modifier.width(5.dp))
                    OutlinedButton(
                        onClick = { copyMnemonic(false) },
                        border = BorderStroke(2.dp, borderColor),
                        shape = RoundedCornerShape(18.dp),
                        colors = buttonColors
                    ) {
                        Icon(
                            painterResource(R.drawable.ic_copy_link),
                            "Copy",
                            tint = MaterialTheme.colors.onBackground
                        )
                    }
                }
                Spacer(Modifier.height(5.dp))
            }
        }
        Spacer(Modifier.height(90.dp))
    }

}