package app.bitcoin.baemail.paymail.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.bitcoin.baemail.core.domain.usecases.AppConfigUseCases
import app.bitcoin.baemail.core.domain.entities.IndexerInfo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class AppConfigViewModel @Inject constructor(
    private val appConfigUseCases: AppConfigUseCases
) : ViewModel() {


    val defaultIndexerInfo: IndexerInfo = appConfigUseCases.defaultIndexerInfo

    private val _indexerInfo = MutableStateFlow(IndexerInfo())
    val indexerInfo: StateFlow<IndexerInfo> = _indexerInfo

    val modifiedIndexerInfo = MutableStateFlow(IndexerInfo())

    init {
        viewModelScope.launch {
            initializeIndexerInfo()
        }
    }

    private suspend fun initializeIndexerInfo() {
        _indexerInfo.value = appConfigUseCases.getIndexerInfoConfig()
        modifiedIndexerInfo.value = _indexerInfo.value
    }

    fun applyIndexerInfoChanges() {
        if (indexerInfo.value == modifiedIndexerInfo.value) return

        viewModelScope.launch {
            appConfigUseCases.updateIndexerInfoConfig(modifiedIndexerInfo.value)
            initializeIndexerInfo()
        }
    }

    fun onIndexerUrlChanged(url: String){
        modifiedIndexerInfo.value.let { current ->
            modifiedIndexerInfo.value = current.copy(serverUrl = url)
        }
    }

    fun onIndexerAccountChanged(account: String) {
        modifiedIndexerInfo.value.let { current ->
            modifiedIndexerInfo.value = current.copy(account = account)
        }
    }

    fun onIndexerPasswordChanged(password: String) {
        modifiedIndexerInfo.value.let { current ->
            modifiedIndexerInfo.value = current.copy(password = password)
        }
    }

}