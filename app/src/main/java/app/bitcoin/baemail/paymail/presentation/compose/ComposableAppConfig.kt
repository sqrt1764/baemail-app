package app.bitcoin.baemail.paymail.presentation.compose

import android.content.res.Configuration
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.domain.entities.IndexerInfo
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme


@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode of AppConfig"
)
@Composable
fun ComposableAppConfigPreview() {
    val indexerInfo = IndexerInfo(password = "")
    //val indexerInfo by indexerInfoFlow.collectAsStateWithLifecycle(IndexerInfo())

    val defaultIndexerInfo = IndexerInfo(
        "https://default.server.url",
        "default-account",
        "default-password"
    )

    ComposeAppTheme {
        ComposableAppConfig(indexerInfo, defaultIndexerInfo)
    }
}





@Composable
fun ComposableAppConfig(
    indexerInfo: IndexerInfo,
    defaultIndexerInfo: IndexerInfo,
    modifier: Modifier = Modifier,
    onBackClicked: () -> Unit = {},
    onIndexerUrlChanged: (String) -> Unit = {},
    onIndexerAccountChanged: (String) -> Unit = {},
    onIndexerPasswordChanged: (String) -> Unit = {},
    bottomContent: @Composable () -> Unit = {}
) {

    val scrollState = rememberScrollState()

    Box(modifier = modifier.verticalScroll(scrollState)) {
        Column(
            modifier = Modifier
                .padding(PaddingValues(16.dp, 12.dp, 16.dp, 2.dp))
                .fillMaxWidth()
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                IconButton(onClick = onBackClicked, Modifier.size(60.dp)) {
                    Icon(painterResource(id = R.drawable.round_arrow_back_ios_24), stringResource(R.string.back))
                }
                Spacer(Modifier.size(12.dp))
                Text(
                    stringResource(id = R.string.app_config_title),
                    style = MaterialTheme.typography.h1
                )
            }
            Spacer(Modifier.size(16.dp))
            Text(
                stringResource(id = R.string.app_config_info0),
                style = MaterialTheme.typography.body1
            )
            Spacer(Modifier.size(16.dp))
            Text(
                stringResource(id = R.string.app_config_indexer_section),
                style = MaterialTheme.typography.h2
            )
            Text(
                stringResource(id = R.string.app_config_indexer_info),
                style = MaterialTheme.typography.body1
            )
            Spacer(Modifier.size(16.dp))
            ConfigInput(
                value = indexerInfo.serverUrl,
                onValueChanged = onIndexerUrlChanged,
                label = stringResource(id = R.string.app_config_indexer_url),
                default = defaultIndexerInfo.serverUrl
            )
            Spacer(Modifier.size(16.dp))
            ConfigInput(
                value = indexerInfo.account,
                onValueChanged = onIndexerAccountChanged,
                label = stringResource(id = R.string.app_config_indexer_acc),
                default = defaultIndexerInfo.account
            )
            Spacer(Modifier.size(16.dp))
            ConfigInput(
                value = indexerInfo.password,
                onValueChanged = onIndexerPasswordChanged,
                label = stringResource(id = R.string.app_config_indexer_psw),
                default = defaultIndexerInfo.password,
                isPassword = true
            )
            Spacer(Modifier.size(16.dp))
            bottomContent()
        }
    }
}

@Composable
fun ConfigInput(
    value: String,
    onValueChanged: (String) -> Unit,
    modifier: Modifier = Modifier,
    label: String = "Unspecified",
    default: String = "placeholder",
    isPassword: Boolean = false
) {
    Surface(
        shape = MaterialTheme.shapes.small,
        color = MaterialTheme.colors.onSurface.copy(alpha = 0.1f),
        modifier = modifier
    ) {
        Text(
            label,
            style = MaterialTheme.typography.caption,
            modifier = Modifier.padding(start = 16.dp, top = 8.dp)
        )
        if (!isPassword) {
            TextField(
                value = value,
                onValueChange = onValueChanged,
                modifier = Modifier
                    .semantics { contentDescription = label }
                    .fillMaxWidth()
                    .padding(top = Dp(MaterialTheme.typography.caption.fontSize.value).plus(8.dp)),
                singleLine = true,
                keyboardOptions = KeyboardOptions.Default.copy(
                    autoCorrect = false,
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Done
                ),
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = Color.Transparent,
                    unfocusedBorderColor = Color.Transparent,
                ),
                placeholder = {
                    Text(
                        default,
                        style = MaterialTheme.typography.subtitle1
                    )
                }
            )
        } else {
            PasswordTextField(
                value = value,
                onValueChange = onValueChanged,
                modifier = Modifier
                    .semantics { contentDescription = label }
                    .fillMaxWidth()
                    .padding(top = Dp(MaterialTheme.typography.caption.fontSize.value).plus(8.dp)),
                placeholder = default
            )
        }
    }
}


//simplified version of the implementation share here - https://medium.com/google-developer-experts/how-to-create-a-composable-password-with-jetpack-compose-f1be2d48d9f0
@Composable
fun PasswordTextField(
    value: String,
    onValueChange: (text: String) -> Unit,
    modifier: Modifier = Modifier,
    placeholder: String = "-",
) {
    val focusManager = LocalFocusManager.current
    val showPassword = remember { mutableStateOf(false) }

    OutlinedTextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier.fillMaxWidth(),
            placeholder = {
                val placeholderText = if (showPassword.value)
                    placeholder
                else
                    String(CharArray(placeholder.length) {'\u2022'})

                Text(
                    text = placeholderText,
                    style = MaterialTheme.typography.subtitle1
                )
            },
        keyboardOptions = KeyboardOptions.Default.copy(
            autoCorrect = false,
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Done
        ),
        keyboardActions = KeyboardActions(
            onDone = {
                focusManager.clearFocus()
            }
        ),
        singleLine = true,
        visualTransformation = if (showPassword.value) VisualTransformation.None else
            PasswordVisualTransformation(),
        trailingIcon = {
            val icon  = if (showPassword.value) {
                painterResource(id = R.drawable.vector_ic_password_visible)
            } else {
                painterResource(id = R.drawable.vector_ic_password_masked)
            }

            IconButton(onClick = { showPassword.value = !showPassword.value }) {
                Icon(
                    icon,
                    contentDescription = stringResource(id = R.string.visibility),
                    tint = MaterialTheme.colors.onSurface
                )
            }
        },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Color.Transparent,
                unfocusedBorderColor = Color.Transparent,
            )
    )
}


