package app.bitcoin.baemail.paymail.domain.usecases

import app.bitcoin.baemail.core.domain.repository.AuthRepository


class AddAuthUseCaseImpl(
    val authRepository: AuthRepository
) : AddAuthUseCase {
    override suspend fun invoke(
        paymail: String,
        paymailPath: String,
        paymailMnemonic: List<String>,
        fundingPath: String,
        fundingMnemonic: List<String>
    ) {
        authRepository.addPaymail(paymail, paymailPath, paymailMnemonic,
            fundingPath, fundingMnemonic)
    }

}

interface AddAuthUseCase {
    suspend operator fun invoke(
        paymail: String,
        paymailPath: String,
        paymailMnemonic: List<String>,
        fundingPath: String,
        fundingMnemonic: List<String>,
    )
}

class ErrorAliasNotFound : Exception()
class ErrorInvalidMnemonic : Exception()
class ErrorInvalidFundingMnemonic : Exception()
class ErrorAliasMnemonicMismatch : Exception()