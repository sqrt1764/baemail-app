package app.bitcoin.baemail.paymail.domain.usecases

import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.domain.usecases.GetSeedWordListUseCase
import kotlinx.coroutines.flow.first

class FindFundingWalletSeedUseCaseImpl(
    val seedWordListUseCase: GetSeedWordListUseCase,
    val nodeRuntimeRepository: NodeRuntimeRepository
) : FindFundingWalletSeedUseCase {
    override suspend fun invoke(seedList: List<String>): List<String> {
        val seedWordArray = seedWordListUseCase().first()

        var potentialSeed: List<String> = ArrayList(seedList.asReversed())

        //check validity of seed
        (0 until 111).forEach {
            potentialSeed = getNextCandidate(seedWordArray, potentialSeed)

            val isValidMnemonic = nodeRuntimeRepository.checkMnemonic(potentialSeed)
            if (isValidMnemonic) {
                return potentialSeed
            }
        }

        throw RuntimeException("failed finding a valid seed")
    }

    private fun getNextCandidate(seedWords: Array<String>, mnemonic: List<String>): List<String> {
        fun getPrev(word: String): String {
            val index = seedWords.indexOf(word)
            return if (index == 0) {
                seedWords[seedWords.size - 1]
            } else {
                seedWords[index - 1]
            }
        }
        fun getNext(word: String): String {
            val index = seedWords.indexOf(word)
            return if (index + 1 == seedWords.size) {
                seedWords[0]
            } else {
                seedWords[index + 1]
            }
        }

        return listOf(
            getNext(mnemonic[0]),
            getPrev(mnemonic[1]),
            getNext(mnemonic[2]),
            getPrev(mnemonic[3]),
            getNext(mnemonic[4]),
            getPrev(mnemonic[5]),
            getNext(mnemonic[6]),
            getPrev(mnemonic[7]),
            getNext(mnemonic[8]),
            getPrev(mnemonic[9]),
            getNext(mnemonic[10]),
            getPrev(mnemonic[11])
        )
    }

}

interface FindFundingWalletSeedUseCase {
    suspend operator fun invoke(seedList: List<String>): List<String>
}