package app.bitcoin.baemail.paymail.presentation.compose

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme

@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode of AppConfig",
    widthDp = 320
)
@Composable
fun ComposeAddedPaymailsHeaderPreview() {
    val items = listOf(
        "config" to "Config",
        "logs" to "Logs",
    )
    ComposeAppTheme {
        ComposeAddedPaymailsHeader(items = items) {}
    }
}


@Composable
fun ComposeAddedPaymailsHeader(
    modifier: Modifier = Modifier,
    items: List<Pair<String, String>>,
    onItemSelected: (String) -> Unit
) {
    val isConnectionDropDownExpanded = remember { mutableStateOf(false) }

    Box(modifier) {
        Column(Modifier.align(Alignment.Center),
        horizontalAlignment = Alignment.CenterHorizontally) {
            Spacer(Modifier.size(24.dp))
            Image(
                painter = painterResource(id = R.drawable.added_paymails_fragment__logo),
                contentDescription = null,
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.height(60.dp)
            )
            Spacer(Modifier.size(8.dp))
            Text(
                text = stringResource(id = R.string.added_paymails_fragment__top_title_value),
                style = MaterialTheme.typography.h2,
            )
            Spacer(Modifier.size(18.dp))
        }
        ComposableMoreActionsDropDown(
            isConnectionDropDownExpanded,
            items,
            onItemSelected,
            Modifier
                .align(Alignment.TopEnd)
                .padding(end = 16.dp, top = 8.dp)
        )
    }
}

@Composable
fun ComposableMoreActionsDropDown(
    expanded: MutableState<Boolean>,
    items: List<Pair<String, String>>,
    onItemSelected: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    Box(modifier) {
        IconButton(
            onClick = { expanded.value = true },
            modifier = Modifier
                .height(45.dp)
                .clip(CircleShape)
                .border(
                    border = BorderStroke(2.dp, MaterialTheme.colors.primary),
                    shape = CircleShape
                )
        ) {
            Image(
                painterResource(id = R.drawable.ic_more_vert_48),
                contentDescription = stringResource(id = R.string.more),
                colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
                modifier = Modifier.size(32.dp)
            )
        }
        Box(Modifier.align(Alignment.TopEnd)) {
            DropdownMenu(
                expanded = expanded.value,
                onDismissRequest = { expanded.value = false }
            ) {
                items.forEach { s ->
                    DropdownMenuItem(onClick = {
                        onItemSelected(s.first)
                        expanded.value = false
                    }) {
                        Text(
                            text = s.second,
                            modifier = Modifier.padding(start = 16.dp, end = 24.dp),
                            style = MaterialTheme.typography.body1
                        )
                    }
                }
            }
        }

    }

}