package app.bitcoin.baemail.paymail.presentation

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Scaffold
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.SnackbarResult
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.domain.entities.IndexerInfo
import app.bitcoin.baemail.paymail.presentation.compose.ComposableAppConfig
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject

class AppConfigFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: AppConfigViewModel

    private val availableHeightHelper = AvailableScreenHeightHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)
        viewModel = ViewModelProvider(
            viewModelStoreOwner, viewModelFactory)[AppConfigViewModel::class.java]

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val view = ComposeView(requireContext())
        view.setContent {

            val modifiedIndexerInfo = viewModel.modifiedIndexerInfo
                .collectAsStateWithLifecycle(IndexerInfo())
            val indexerInfo = viewModel.indexerInfo.collectAsStateWithLifecycle(IndexerInfo())

            val hasConfigBeenModified = remember {
                derivedStateOf { modifiedIndexerInfo.value != indexerInfo.value }
            }

            val availableHeight = availableHeightHelper.availableHeightLD.observeAsState(0)
            val keyboardDpHeight = availableHeight.value.let { availableHeightPx ->
                val imeHeight = LocalView.current.height - availableHeightPx
                val screenPixelDensity = LocalContext.current.resources.displayMetrics.density
                Dp(imeHeight.toFloat() / screenPixelDensity)
            }

            val focusManager = LocalFocusManager.current

            val applyChangesNow: () -> Unit = {
                doApplyChangedNow()
                focusManager.clearFocus(true)
                //imm.hideSoftInputFromWindow(requireView().windowToken, 0)
            }

            ComposeAppTheme {
                AppConfigScreen(
                    keyboardDpHeight,
                    modifiedIndexerInfo.value,
                    viewModel.defaultIndexerInfo,
                    hasConfigBeenModified.value,
                    onBackClicked,
                    viewModel::onIndexerUrlChanged,
                    viewModel::onIndexerAccountChanged,
                    viewModel::onIndexerPasswordChanged,
                    applyChangesNow
                )
            }
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                view.updatePadding(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                view.updatePadding(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight,
                )
            }

            windowInsets
        }

        availableHeightHelper.setup(
            requireContext(),
            requireActivity().window,
            viewLifecycleOwner,
        ) { view }



//        viewLifecycleOwner.lifecycleScope.launch {
//            repeatOnLifecycle(Lifecycle.State.STARTED) {
//                //TODO flow.collect {}
//            }
//        }
    }

    private val onBackClicked: () -> Unit = {
        Timber.d("onApplyChangesSnackbarDismissed")
        findNavController().navigateUp()
    }

    private val doApplyChangedNow: () -> Unit = {
        Timber.d("do apply changes now")
        viewModel.applyIndexerInfoChanges()
    }

}

@Composable
fun AppConfigScreen(
    imeHeight: Dp,
    info: IndexerInfo,
    defaultInfo: IndexerInfo,
    hasConfigBeenModified: Boolean,
    onBackClicked: () -> Unit = {},
    onIndexerUrlChanged: (String) -> Unit = {},
    onIndexerAccountChanged: (String) -> Unit = {},
    onIndexerPasswordChanged: (String) -> Unit = {},
    doApplyChangedNow: () -> Unit = {}
) {

    val snackbarHostState = remember { SnackbarHostState() }

    val labelConfigHasChanged = stringResource(R.string.config_has_changed)
    val labelApply = stringResource(R.string.apply)

    if (hasConfigBeenModified) {
        LaunchedEffect(snackbarHostState) {
            val result = snackbarHostState.showSnackbar(
                message = labelConfigHasChanged,
                actionLabel = labelApply,
                duration = SnackbarDuration.Indefinite
            )
            when (result) {
                SnackbarResult.ActionPerformed -> {
                    doApplyChangedNow()
                }
                SnackbarResult.Dismissed -> {
                    Timber.d("...on dismissed")
                }
            }
        }
    }



    Scaffold(
        modifier = Modifier.padding(bottom = imeHeight),
        snackbarHost = { SnackbarHost(snackbarHostState) },
    ) { padding ->
        ComposableAppConfig(
            info,
            defaultInfo,
            Modifier.padding(padding),
            onBackClicked = onBackClicked,
            onIndexerUrlChanged = {
                onIndexerUrlChanged(it)
            },
            onIndexerAccountChanged = {
                onIndexerAccountChanged(it)
            },
            onIndexerPasswordChanged = {
                onIndexerPasswordChanged(it)
            },
            bottomContent = {
                Spacer(Modifier.size(80.dp))
            }
        )
    }

}