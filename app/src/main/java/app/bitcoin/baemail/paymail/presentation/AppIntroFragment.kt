package app.bitcoin.baemail.paymail.presentation

import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.text.util.Linkify
import android.view.View
import android.view.ViewOutlineProvider
import android.view.WindowInsets
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr

class AppIntroFragment : Fragment(R.layout.fragment_app_intro)  {

    private lateinit var container: NestedScrollView
    private lateinit var info0: TextView
    private lateinit var next: TextView
    private lateinit var howToFindMySeed: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        container = requireView().findViewById(R.id.container)
        info0 = requireView().findViewById(R.id.info_0)
        next = requireView().findViewById(R.id.next)
        howToFindMySeed = requireView().findViewById(R.id.how_to_find_my_seed)

        Linkify.addLinks(info0, Linkify.WEB_URLS);


        container.doOnApplyWindowInsets { v, windowInsets, _ ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                v.updatePadding(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                v.updatePadding(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight,
                )
            }

            windowInsets
        }






        val colorAddPaymailBackground = requireContext().getColorFromAttr(R.attr.colorPrimary)
        val colorSelectorAddPaymailBackground = R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        next.background = LayerDrawable(arrayOf(
            ColorDrawable(colorAddPaymailBackground),
            DrawableState.getNew(colorSelectorAddPaymailBackground)
        ))

        next.clipToOutline = true
        next.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val cornerRoundness = view.height / 2f

                outline.setRoundRect(0, 0, view.width, view.height, cornerRoundness)
            }

        }

        next.setOnClickListener {
            findNavController().navigate(R.id.action_appIntroFragment_to_paymailIdInputFragment)
//            findNavController().navigate(R.id.action_addedPaymailsFragment_to_appIntroFragment)
        }


        howToFindMySeed.setOnClickListener {
            findNavController().navigate(R.id.action_appIntroFragment_to_appDetailedInfoDialog)
        }


    }
}