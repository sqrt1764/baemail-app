package app.bitcoin.baemail.paymail.domain.usecases

import app.bitcoin.baemail.core.data.util.AuthenticatedPaymail
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapNotNull

class GetAuthenticatedPaymailsUseCaseImpl(
    private val authRepository: AuthRepository
) : GetAuthenticatedPaymailsUseCase {
    override fun invoke(): Flow<List<AuthenticatedPaymail>> =
        authRepository.addedPaymails.mapNotNull { it }

}

interface GetAuthenticatedPaymailsUseCase {
    operator fun invoke(): Flow<List<AuthenticatedPaymail>>
}