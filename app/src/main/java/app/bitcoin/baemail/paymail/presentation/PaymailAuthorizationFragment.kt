package app.bitcoin.baemail.paymail.presentation

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import androidx.activity.OnBackPressedCallback
import androidx.compose.ui.platform.ComposeView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import app.bitcoin.baemail.core.presentation.util.NoSwipeBehavior
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.view.ErrorSnackBar
import app.bitcoin.baemail.paymail.presentation.compose.ComposePaymailAuthorizationScreen
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.StringBuilder
import javax.inject.Inject

class PaymailAuthorizationFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: PaymailManagementViewModel

    private val cbm: ClipboardManager by lazy {
        requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(
            viewModelStoreOwner,
            viewModelFactory
        )[PaymailManagementViewModel::class.java]

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val content = ComposeView(requireContext()).apply {
            setContent {
                val overviewStateFlow = viewModel.overview.collectAsStateWithLifecycle()
                val overviewState = overviewStateFlow.value

                val copyMnemonic: (Boolean) -> Unit = { paymailNotFunding ->
                    val mnemonic = if (paymailNotFunding) {
                        overviewState.paymailMnemonic
                    } else {
                        overviewState.fundingMnemonic
                    }

                    val typeLabel = if (paymailNotFunding) {
                        getString(R.string.paymail)
                    } else {
                        getString(R.string.funding)
                    }

                    copyMnemonicToClipBoard(
                        typeLabel,
                        mnemonic
                    )
                }

                ComposeAppTheme {
                    ComposePaymailAuthorizationScreen(
                        overviewState.paymail,
                        overviewState.paymailPath,
                        overviewState.paymailMnemonic,
                        overviewState.fundingPath,
                        overviewState.fundingMnemonic,
                        overviewState.isFundingInfoValid,
                        ::cancelAdding,
                        viewModel::onConfirmAuthorizePaymail,
                        copyMnemonic
                    )
                }
            }
        }

        val coordinatorLayout = CoordinatorLayout(requireContext())
        coordinatorLayout.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        content.layoutParams = CoordinatorLayout.LayoutParams(
            CoordinatorLayout.LayoutParams.MATCH_PARENT,
            CoordinatorLayout.LayoutParams.MATCH_PARENT
        )
        coordinatorLayout.addView(content)

        return coordinatorLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    viewModel.onAddingCancelled()

                    isEnabled = false
                    findNavController().popBackStack(R.id.addedPaymailsFragment, false)
                }

            }
        )

        view.doOnApplyWindowInsets { v, windowInsets, _ ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                v.updatePadding(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                v.updatePadding(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight,
                )
            }

            windowInsets
        }


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.eventFlow.collectLatest { event ->
                    when (event) {
                        is PaymailManagementViewModel.UiEvent.SuccessPaymailAdded -> {
                            findNavController().popBackStack(R.id.addedPaymailsFragment, false)
                        }
                        is PaymailManagementViewModel.UiEvent.FailPaymailInfoInvalid -> {
                            Timber.d(event.error)

                            ErrorSnackBar.make(
                                requireContext(),
                                requireView() as ViewGroup,
                                getString(event.message)
                            ).let {
                                it.behavior = NoSwipeBehavior()
                                it.duration = Snackbar.LENGTH_SHORT
                                it.show()
                            }

                        }
                        else -> {}
                    }
                }
            }
        }

    }

    private fun cancelAdding() {
        viewModel.onAddingCancelled()
        findNavController().popBackStack(R.id.addedPaymailsFragment, false)
    }

    private fun copyMnemonicToClipBoard(field: String, mnemonic: List<String>) {
        cbm.let { manager ->
            val mnemonicString = StringBuilder()
            mnemonic.forEachIndexed { i, item ->
                mnemonicString.append(item)
                if (i + 1 != mnemonic.size)
                    mnemonicString.append(" ")
            }

            val clip = ClipData.newPlainText(
                getString(R.string.mnemonic, field),
                mnemonicString.toString()
            )

            manager.setPrimaryClip(clip)
            ErrorSnackBar.make(
                requireContext(),
                requireView() as ViewGroup,
                getString(R.string.x_copied, field)
            ).let {
                it.behavior = NoSwipeBehavior()
                it.duration = Snackbar.LENGTH_SHORT
                it.show()
            }
        }
    }

}