package app.bitcoin.baemail.paymail.presentation.compose

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme


@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode of ComposeWalletConfig"
)
@Composable
fun ComposeWalletConfigPreview() {

    val fundingWalletType = remember {
        mutableStateOf(FundingWallet.DETERMINISTIC)
    }

    ComposeAppTheme {
        ComposeWalletConfigScreen(
            fundingWalletType,
            {},
            {},
            {}
        )
    }
}


@Composable
fun ComposeWalletConfigScreen(
    fundingWalletType: State<FundingWallet>,
    onTypeChanged: (FundingWallet) -> Unit,
    onClickCancel: ()->Unit,
    onClickNext: ()->Unit
) {

    val buttonTextFontStyle = MaterialTheme.typography.h2.copy(
        fontWeight = FontWeight.Normal,
        fontSize = 30.sp
    )

    Box() {
        ComposeWalletConfig(fundingWalletType, onTypeChanged)
        Row(
            Modifier
                .fillMaxWidth()
                .align(Alignment.BottomStart)
                .padding(16.dp)
        ) {
            Button(
                modifier = Modifier.weight(1f, true)
                    .clip(RoundedCornerShape(50)),
                onClick = onClickCancel
            ) {
                Text(
                    stringResource(id = R.string.cancel),
                    modifier = Modifier.padding(vertical = 6.dp),
                    style = buttonTextFontStyle
                )
            }
            Spacer(modifier = Modifier.size(16.dp))
            Button(
                modifier = Modifier.weight(1f, true)
                    .clip(RoundedCornerShape(50)),
                onClick = onClickNext
            ) {
                Text(
                    stringResource(id = R.string.next),
                    modifier = Modifier.padding(vertical = 6.dp),
                    style = buttonTextFontStyle
                )
            }
        }
    }

}

enum class FundingWallet {
    DETERMINISTIC,
    PAYMAIL,
    CUSTOM
}

@Composable
fun ComposeWalletConfig(
    fundingWalletType: State<FundingWallet>,
    onTypeChanged: (FundingWallet)->Unit
) {
    val selectionTitleSize = MaterialTheme.typography.body1.fontSize.times(1.5f)
    val selectedBorderColor = MaterialTheme.colors.primary
    val unselectedBorderColor = MaterialTheme.colors.onBackground.copy(alpha = 0.3f)

    val deterministicTypeStrokeColor =
        if (FundingWallet.DETERMINISTIC == fundingWalletType.value) {
            selectedBorderColor
        } else {
            unselectedBorderColor
        }

    val paymailTypeStrokeColor = if (FundingWallet.PAYMAIL == fundingWalletType.value) {
        selectedBorderColor
    } else {
        unselectedBorderColor
    }

    val customTypeStrokeColor = if (FundingWallet.CUSTOM == fundingWalletType.value) {
        selectedBorderColor
    } else {
        unselectedBorderColor
    }
    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp)
            .verticalScroll(scrollState)
    ) {
        Spacer(Modifier.height(24.dp))
        Text(
            stringResource(R.string.funding_wallet),
            style = MaterialTheme.typography.h1
        )
        Spacer(Modifier.height(16.dp))
        Text(
            stringResource(R.string.funding_wallet_info),
            style = MaterialTheme.typography.body1
        )
        Spacer(Modifier.height(16.dp))
        Box(
            Modifier
                .border(
                    BorderStroke(2.5.dp, deterministicTypeStrokeColor),
                    MaterialTheme.shapes.medium
                )
                .clip(RoundedCornerShape(6.dp))
                .clickable {
                    onTypeChanged(FundingWallet.DETERMINISTIC)
                }) {
            Column(modifier = Modifier.padding(horizontal = 12.dp, vertical = 8.dp)) {
                Text(
                    stringResource(R.string.deterministic),
                    style = MaterialTheme.typography.body1,
                    fontSize = selectionTitleSize
                )
                Text(
                    stringResource(R.string.deterministic_info),
                    style = MaterialTheme.typography.caption
                )
                Spacer(Modifier.height(5.dp))
            }
        }
        Spacer(Modifier.height(8.dp))
        Box(modifier = Modifier
            .border(
                BorderStroke(2.5.dp, paymailTypeStrokeColor),
                MaterialTheme.shapes.medium
            )
            .clip(RoundedCornerShape(6.dp))
            .clickable {
                onTypeChanged(FundingWallet.PAYMAIL)
            }) {
            Column(modifier = Modifier.padding(horizontal = 12.dp, vertical = 8.dp)) {
                Text(
                    stringResource(R.string.paymail_mnemonic),
                    style = MaterialTheme.typography.body1,
                    fontSize = selectionTitleSize
                )
                Text(
                    stringResource(R.string.paymail_mnemonic_info),
                    style = MaterialTheme.typography.caption
                )
                Spacer(Modifier.height(5.dp))
            }
        }
        Spacer(Modifier.height(8.dp))
        Box(modifier = Modifier
            .border(
                BorderStroke(2.5.dp, customTypeStrokeColor),
                MaterialTheme.shapes.medium
            )
            .clip(RoundedCornerShape(6.dp))
            .clickable {
                onTypeChanged(FundingWallet.CUSTOM)
            }) {
            Column(modifier = Modifier.padding(horizontal = 12.dp, vertical = 8.dp)) {
                Text(
                    stringResource(R.string.custom),
                    style = MaterialTheme.typography.body1,
                    fontSize = selectionTitleSize
                )
                Text(
                    stringResource(R.string.custom_info),
                    style = MaterialTheme.typography.caption
                )
                Spacer(Modifier.height(5.dp))
            }
        }
    }
}
