package app.bitcoin.baemail.accountMeta

import androidx.lifecycle.*
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.domain.usecases.CoinSyncStatusUseCase
import app.bitcoin.baemail.core.domain.usecases.DeleteAllLogsUseCase
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.core.domain.usecases.GetFundingCoinsUseCase
import app.bitcoin.baemail.core.domain.usecases.RestartCoinSyncUseCase
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import javax.inject.Inject

class AccountMetaViewModel @Inject constructor(
    private val restartCoinSyncUseCase: RestartCoinSyncUseCase,
    private val deleteAllLogsUseCase: DeleteAllLogsUseCase,
    getActivePaymailUseCase: GetActivePaymailUseCase,
    getFundingCoinsUseCase: GetFundingCoinsUseCase,
    coinSyncStatusUseCase: CoinSyncStatusUseCase
) : ViewModel() {

    val model = getActivePaymailUseCase()
        .combine(getFundingCoinsUseCase()) { activePaymail, fundingCoins ->
            activePaymail to fundingCoins
        }.combine(coinSyncStatusUseCase()) { (activePaymail, fundingCoins), syncStatus ->
            val name = activePaymail?.paymail?.let { paymail ->
                paymail.substring(0, paymail.indexOf("@"))
            } ?: ""

            val sats = fundingCoins.foldRight(0L) { i, acc ->
                acc + i.sats
            }

            Model(
                name,
                activePaymail?.paymail ?: "",
                sats,
                syncStatus
            )
        }

    fun retryReconnect() {
        restartCoinSyncUseCase()
    }

    fun deleteAllLogs() {
        viewModelScope.launch {
            deleteAllLogsUseCase()
        }
    }

    data class Model(
        val name: String,
        val paymail: String,
        val sats: Long,
        val syncStatus: ChainSyncStatus
    )
}