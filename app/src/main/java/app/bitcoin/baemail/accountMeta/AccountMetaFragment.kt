package app.bitcoin.baemail.accountMeta

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.presentation.view.ChainSyncView
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.util.formatSatsLabel
import app.bitcoin.baemail.fundingWallet.core.presentation.CoinActivity
import app.bitcoin.baemail.fundingWallet.SweepActivity
import app.bitcoin.baemail.fundingWallet.TopUpActivity
import app.bitcoin.baemail.logs.presentation.LogsActivity
import app.bitcoin.baemail.paymail.presentation.PaymailActivity
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ViewProfileCard
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class AccountMetaFragment : Fragment(R.layout.fragment_account_meta) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: AccountMetaViewModel

    private lateinit var profileCard: ViewProfileCard
    private lateinit var switchPaymail: TextView

    private lateinit var topUpFundingWallet: TextView
    private lateinit var sweepFundingWallet: TextView
    private lateinit var fundingWalletFunds: TextView
    private lateinit var fundingWalletCoins: TextView
    private lateinit var logs: TextView
    private lateinit var chainSyncState: ChainSyncView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            this,
            viewModelFactory
        )[AccountMetaViewModel::class.java]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        profileCard = requireView().findViewById(R.id.profile_card)
        switchPaymail = requireView().findViewById(R.id.switch_paymail)
        topUpFundingWallet = requireView().findViewById(R.id.top_up_funding_wallet)
        sweepFundingWallet = requireView().findViewById(R.id.sweep_funding_wallet)
        fundingWalletFunds = requireView().findViewById(R.id.available_funds)
        fundingWalletCoins = requireView().findViewById(R.id.funding_wallet_coins)
        logs = requireView().findViewById(R.id.logs_viewer)
        chainSyncState = requireView().findViewById(R.id.chain_sync_state)


        val separatorColor = requireContext().getColorFromAttr(R.attr.colorSurfaceVariant)

        val separatorPaddingSides = R.dimen.setup_fragment__item_separator_padding_sides.let { id ->
            resources.getDimensionPixelSize(id)
        }

        val separatorWidth = R.dimen.setup_fragment__item_separator_width.let { id ->
            resources.getDimensionPixelSize(id)
        }

        val colorSelector = R.color.setup_fragment__selector_color.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }


        val drawableProfileSeparator = DrawableStroke().also {
            val color = requireContext().getColorFromAttr(R.attr.colorPrimary)

            val paddingSides = R.dimen.setup_fragment__profile_separator_padding_sides.let { id ->
                resources.getDimensionPixelSize(id)
            }

            val width = R.dimen.setup_fragment__profile_separator_width.let { id ->
                resources.getDimensionPixelSize(id)
            }

            it.setPadding(paddingSides, 0, paddingSides, 0)
            it.setStrokeColor(color)
            it.setStrokeWidths(0, 0, 0, width)
        }
        profileCard.background = drawableProfileSeparator


        switchPaymail.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(separatorColor)
                it.setStrokeWidths(0, 0, 0, separatorWidth)
                it.setPadding(separatorPaddingSides, 0, separatorPaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))

        switchPaymail.setOnClickListener {
            startActivity(Intent(requireActivity(), PaymailActivity::class.java))
        }


        topUpFundingWallet.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(separatorColor)
                it.setStrokeWidths(0, 0, 0, separatorWidth)
                it.setPadding(separatorPaddingSides, 0, separatorPaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))

        topUpFundingWallet.setOnClickListener {
            startActivity(Intent(requireActivity(), TopUpActivity::class.java))
        }


        sweepFundingWallet.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(separatorColor)
                it.setStrokeWidths(0, 0, 0, separatorWidth)
                it.setPadding(separatorPaddingSides, 0, separatorPaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))

        sweepFundingWallet.setOnClickListener {
            startActivity(Intent(requireActivity(), SweepActivity::class.java), null)
        }

        fundingWalletCoins.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(separatorColor)
                it.setStrokeWidths(0, 0, 0, separatorWidth)
                it.setPadding(separatorPaddingSides, 0, separatorPaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))

        fundingWalletCoins.setOnClickListener {
            startActivity(Intent(requireActivity(), CoinActivity::class.java))
        }

        logs.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(separatorColor)
                it.setStrokeWidths(0, 0, 0, separatorWidth)
                it.setPadding(separatorPaddingSides, 0, separatorPaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))

        logs.setOnClickListener {
            startActivity(Intent(requireActivity(), LogsActivity::class.java), null)
        }

        logs.setOnLongClickListener {
            viewModel.deleteAllLogs()
            Toast.makeText(requireContext(), "Logs have been deleted.", Toast.LENGTH_SHORT).show()

            true
        }

        chainSyncState.onRetryClicked = {
            viewModel.retryReconnect()
        }


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.model.collectLatest { model ->
                    profileCard.applyModel(
                        ViewProfileCard.Model(
                        model.name,
                        model.paymail
                    ))

                    fundingWalletFunds.text = getString(
                        R.string.x_sats,
                        formatSatsLabel(model.sats.toString())
                    )



                    when (model.syncStatus) {
                        ChainSyncStatus.ERROR -> {
                            chainSyncState.applyModel(
                                ChainSyncView.Model(
                                    ColorDrawable(Color.RED),
                                    resources.getString(R.string.error),
                                    resources.getString(R.string.wallet_sync_failed),
                                    true
                                )
                            )
                        }
                        ChainSyncStatus.DISCONNECTED -> {
                            chainSyncState.applyModel(
                                ChainSyncView.Model(
                                    ColorDrawable(ContextCompat.getColor(requireContext(), R.color.yellow1)),
                                    resources.getString(R.string.disconnected),
                                    resources.getString(R.string.wallet_sync_failed),
                                    false
                                )
                            )
                        }
                        ChainSyncStatus.INITIALISING -> {
                            chainSyncState.applyModel(
                                ChainSyncView.Model(
                                    ColorDrawable(Color.BLUE),
                                    resources.getString(R.string.connecting),
                                    resources.getString(R.string.wallet_doing_sync),
                                    false
                                )
                            )
                        }
                        ChainSyncStatus.LIVE -> {
                            chainSyncState.applyModel(
                                ChainSyncView.Model(
                                    ColorDrawable(Color.GREEN),
                                    resources.getString(R.string.connected),
                                    resources.getString(R.string.wallet_in_sync),
                                    false
                                )
                            )
                        }
                    }

                }
            }
        }
    }
}