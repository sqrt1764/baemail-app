package app.bitcoin.baemail.payments

import android.content.Context
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateFormat
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableOval
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import kotlin.math.max


class PaymentManageFragment : Fragment(R.layout.fragment_payment_manage) {

    companion object {
        private val ALPHA_BUTTON_DIM = 0.6f
        private val ALPHA_BUTTON_ACTIVE = 1f

    }


    private lateinit var paymentId: String





    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PaymentsViewModel






    private lateinit var container: CoordinatorLayout

    private lateinit var contentContainer: ConstraintLayout
    private lateinit var hoverHolder: ConstraintLayout
    private lateinit var destinationBottomSeparator: View
    private lateinit var scroller: NestedScrollView
    private lateinit var amountOptions: ConstraintLayout

    private lateinit var amountOptionsDimmingView: View
    private lateinit var amountOptionsContent: ViewValueAmountInput

    private lateinit var periodContainer: View
    private lateinit var startTimeContainer: View
    private lateinit var amountContainer: View
    private lateinit var loopsContainer: View


    private lateinit var close: ImageView

    private lateinit var reset: TextView

    private lateinit var buttonPeriodDec: ImageView
    private lateinit var buttonPeriodInc: ImageView
    private lateinit var buttonStartTimeDec: ImageView
    private lateinit var buttonStartTimeInc: ImageView
    private lateinit var buttonLoopsDec: ImageView
    private lateinit var buttonLoopsInc: ImageView

    private lateinit var inputTitle: EditText
    private lateinit var inputDestination: EditText
    private lateinit var destinationLayout: DestinationLayout
    private lateinit var destinationAdd: Button




    private lateinit var buttonPeriodBlock: Button
    private lateinit var buttonPeriodHour: Button
    private lateinit var buttonPeriodDay: Button
    private lateinit var buttonStartTimeCurrentBlock: Button
    private lateinit var buttonStartTimePick: Button
    private lateinit var buttonAmountUsd: Button
    private lateinit var buttonAmountSats: Button
    private lateinit var buttonLoopOne: Button
    private lateinit var buttonLoopInfinite: Button

    private lateinit var valuePeriod: TextView
    private lateinit var valueStartTime: TextView
    private lateinit var valueAmount: TextView
    private lateinit var valueLoops: TextView




    private lateinit var optionsSheetBehavior: BottomSheetBehavior<View>




    private val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }
    private val colorSelectorOnLight: Int by lazy {
        R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorSendBackground: Int by lazy {
        R.color.paymail_id_input_fragment__next_background.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }


    private val colorSendBackgroundDisabled: Int by lazy {
        R.color.paymail_id_input_fragment__next_background_disabled.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val drawableSendBackground: ColorDrawable by lazy {
        ColorDrawable(colorSendBackgroundDisabled)
    }

    private val drawableSendSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }



    private val appliedInsets = AppliedInsets()

    private val availableHeightHelper = AvailableScreenHeightHelper()


    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)



        ///todo
        paymentId = "dummy"






        AndroidSupportInjection.inject(this)



        viewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(PaymentsViewModel::class.java)



//        viewModel.init(paymentId)
        //todo
        //todo
        //todo









        container = requireView().findViewById(R.id.container)

        periodContainer = requireView().findViewById(R.id.period_container)
        startTimeContainer = requireView().findViewById(R.id.start_time_container)
        amountContainer = requireView().findViewById(R.id.amount_container)
        loopsContainer = requireView().findViewById(R.id.loops_container)


        close = requireView().findViewById(R.id.close)


        reset = requireView().findViewById(R.id.reset)



        buttonPeriodDec = requireView().findViewById(R.id.button_period_dec)
        buttonPeriodInc = requireView().findViewById(R.id.button_period_inc)
        buttonStartTimeDec = requireView().findViewById(R.id.button_start_time_dec)
        buttonStartTimeInc = requireView().findViewById(R.id.button_start_time_inc)
        buttonLoopsDec = requireView().findViewById(R.id.button_loops_dec)
        buttonLoopsInc = requireView().findViewById(R.id.button_loops_inc)

        buttonPeriodBlock = requireView().findViewById(R.id.button_period_block)
        buttonPeriodHour = requireView().findViewById(R.id.button_period_hour)
        buttonPeriodDay = requireView().findViewById(R.id.button_period_day)
        buttonStartTimeCurrentBlock = requireView().findViewById(R.id.button_start_time_current)
        buttonStartTimePick = requireView().findViewById(R.id.button_start_time_pick)
        buttonAmountUsd = requireView().findViewById(R.id.button_amount_usd)
        buttonAmountSats = requireView().findViewById(R.id.button_amount_sats)
        buttonLoopOne = requireView().findViewById(R.id.button_loops_one)
        buttonLoopInfinite = requireView().findViewById(R.id.button_loops_infinite)


        valuePeriod = requireView().findViewById(R.id.value_period)
        valueStartTime = requireView().findViewById(R.id.value_start_time)
        valueAmount = requireView().findViewById(R.id.value_amount)
        valueLoops = requireView().findViewById(R.id.value_loops)

        contentContainer = requireView().findViewById(R.id.content_container)
        scroller = requireView().findViewById(R.id.scroller)
        amountOptions = requireView().findViewById(R.id.amount_options)

        amountOptionsDimmingView = requireView().findViewById(R.id.dimming_view)
        amountOptionsContent = requireView().findViewById(R.id.amount_options_content)

        inputTitle = requireView().findViewById(R.id.input_title)
        inputDestination = requireView().findViewById(R.id.input_destination)
        destinationLayout = requireView().findViewById(R.id.destination_container)
        destinationAdd = requireView().findViewById(R.id.button_add_destination)

        hoverHolder = requireView().findViewById(R.id.hover_holder)
        destinationBottomSeparator = requireView().findViewById(R.id.destination_bottom_separator)




        contentContainer.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
//            view.updatePadding(
//                top = windowInsets.systemWindowInsetTop//,
//                //,
//                //bottom = windowInsets.systemWindowInsetBottom
//            )

            appliedInsets.top = windowInsets.systemWindowInsetTop
            appliedInsets.bottom = windowInsets.systemWindowInsetBottom

            windowInsets
        }




        availableHeightHelper.setup(
                requireContext(),
                requireActivity().window,
                viewLifecycleOwner,
                { requireView() }
        )

        availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, object : Observer<Int> {

            var lastKeyboardHeight = 0

            override fun onChanged(height: Int) {
                height ?: return

                val maxHeight = getFragmentHeight()

                val keyboardHeight = max(maxHeight - height, 0)

                if (lastKeyboardHeight == 0 && keyboardHeight != 0) {
                    onKeyboardShowingChanged(true)

                } else if (lastKeyboardHeight != 0 && keyboardHeight == 0) {
                    onKeyboardShowingChanged(false)
                }

                lastKeyboardHeight = keyboardHeight
                Timber.d("keyboardHeight: $keyboardHeight")


                destinationBottomSeparator.layoutParams.let {
                    it as ViewGroup.MarginLayoutParams

                    val defaultGap = getDefaultBottomHeight()

                    it.bottomMargin = keyboardHeight + defaultGap

                    destinationBottomSeparator.requestLayout()

                }

                hoverHolder.translationY = -1f * keyboardHeight

            }

            fun getDefaultBottomHeight(): Int {
                return (120 * resources.displayMetrics.density).toInt()
            }

            fun getFragmentHeight(): Int {
                return container.height// - appliedInsets.top// - appliedInsets.bottom
            }

            fun onKeyboardShowingChanged(isShowing: Boolean) {
                if (!isShowing) return

                //scroll the content so that the focused input is not covered by the keyboard
                viewLifecycleOwner.lifecycleScope.launch {
                    delay(50L)

                    val focusedInput = when {
                        inputTitle.isFocused -> {
                            inputTitle
                        }
                        inputDestination.isFocused -> {
                            inputDestination
                        }
                        else -> {
                            return@launch
                        }
                    }

                    val pos = IntArray(2)
                    focusedInput.getLocationInWindow(pos)

                    var bottomOfInput = pos[1] + focusedInput.height
                    //add extra height so that the input is not covered by a floating button
                    bottomOfInput += getDefaultBottomHeight()


                    if (getFragmentHeight() - lastKeyboardHeight < bottomOfInput) {
                        val scrollHeight = bottomOfInput - (getFragmentHeight() - lastKeyboardHeight)
                        scroller.smoothScrollBy(0, scrollHeight)
                    }
                }

            }
        })









        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }






        let {
//            val colorAccent = R.color.message_thread__subject_text_color.let { id ->
//                ContextCompat.getColor(requireContext(), id)
//            }


//            val colorSelectorOnAccent = R.color.selector_on_accent.let { id ->
//                ContextCompat.getColor(requireContext(), id)
//            }

            close.foreground = DrawableState.getNew(colorSelector)

//            val drawableCloseIcon = R.drawable.ic_clear_24.let { id ->
//                val d = ContextCompat.getDrawable(requireContext(), id)!!.mutate()
//                d.setTint(colorAccent)
//                d
//            }
//            close.setImageDrawable(drawableCloseIcon)

            close.setOnClickListener {
                imm.hideSoftInputFromWindow(
                        requireView().windowToken,
                        0
                )

                findNavController().popBackStack()
            }

            close.clipToOutline = true
            close.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    view ?: return
                    outline ?: return

                    outline.setOval(0, 0, view.width, view.height)
                }

            }
        }










        val dp = resources.displayMetrics.density
        val cardStrokeWidth = R.dimen.manage_payment__period_container_stroke_width.let { id ->
            resources.getDimension(id)
        }

        val drawableOvalForPeriod = DrawableOval(requireContext()).also {
            val color = R.color.manage_payment__period_container_background_color.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }

            it.setBgColor(color)
            it.setStrokeColor(Color.TRANSPARENT)
            it.setStrokeWidth(cardStrokeWidth)
            it.setCornerRadius(12f * dp)
            it.setPaddings(0 * dp, 0 * dp, 0 * dp, 0 * dp)
        }
        periodContainer.background = drawableOvalForPeriod


        ////////////////

        val drawableOvalForStartTime = DrawableOval(requireContext()).also {
            val color = R.color.manage_payment__start_time_container_background_color.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }

            it.setBgColor(color)
            it.setStrokeColor(Color.TRANSPARENT)
            it.setStrokeWidth(cardStrokeWidth)
            it.setCornerRadius(12f * dp)
            it.setPaddings(0 * dp, 0 * dp, 0 * dp, 0 * dp)
        }
        startTimeContainer.background = drawableOvalForStartTime


        ////////////////


        val drawableOvalForAmount = DrawableOval(requireContext()).also {
            val color = R.color.manage_payment__amount_container_background_color.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }

            it.setBgColor(color)
            it.setStrokeColor(Color.TRANSPARENT)
            it.setStrokeWidth(cardStrokeWidth)
            it.setCornerRadius(12f * dp)
            it.setPaddings(0 * dp, 0 * dp, 0 * dp, 0 * dp)
        }
        amountContainer.background = drawableOvalForAmount


        ////////////////

        val drawableOvalForLoops = DrawableOval(requireContext()).also {
            val color = R.color.manage_payment__loops_container_background_color.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }

            it.setBgColor(color)
            it.setStrokeColor(Color.TRANSPARENT)
            it.setStrokeWidth(cardStrokeWidth)
            it.setCornerRadius(12f * dp)
            it.setPaddings(0 * dp, 0 * dp, 0 * dp, 0 * dp)
        }
        loopsContainer.background = drawableOvalForLoops


        /////////////////



        reset.clipToOutline = true
        reset.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        reset.background = LayerDrawable(arrayOf(
                drawableSendBackground,
                drawableSendSelector
        ))



        /////////////////





        optionsSheetBehavior = amountOptions.layoutParams.let {
            it as CoordinatorLayout.LayoutParams
            it.behavior as BottomSheetBehavior
        }

        optionsSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        optionsSheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {

            var collapsingBackPressedCallback: CollapsingBackPressedCallback? = null

            inner class CollapsingBackPressedCallback : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Timber.d("collapsingBackPressedCallback .. handleOnBackPressed")
                    optionsSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

                    remove()
                    collapsingBackPressedCallback = null
                }

            }


            override fun onStateChanged(bottomSheet: View, newState: Int) {
                Timber.d("collapsingBackPressedCallback .. onStateChanged newState:$newState")

                if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                    collapsingBackPressedCallback.let {
                        if (it != null) return@let
                        val cb = CollapsingBackPressedCallback()
                        collapsingBackPressedCallback = cb
                        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, cb)
                        Timber.d("collapsingBackPressedCallback .. added")
                    }

                } else if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                    collapsingBackPressedCallback?.remove()
                    collapsingBackPressedCallback = null
                    Timber.d("collapsingBackPressedCallback .. removed")
                }

            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                //do nothing
            }

        })






        amountOptionsContent.onMinusClicked = {
            viewModel.ensurePaymentHelper(paymentId).onAmountChangeDec()
        }

        amountOptionsContent.onPlusClicked = {
            viewModel.ensurePaymentHelper(paymentId).onAmountChangeInc()
        }

        amountOptionsContent.onValueChanged = { value ->
            viewModel.ensurePaymentHelper(paymentId).onAmountChangeValue(value)
        }




        amountOptionsDimmingView.setOnClickListener {
            optionsSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }



        valueAmount.setOnClickListener {
            Timber.d("valueAmount clicked")

            imm.hideSoftInputFromWindow(
                    requireView().windowToken,
                    0
            )

            optionsSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }



        /////////////////



        listOf(
                buttonPeriodDec,
                buttonPeriodInc,
                buttonStartTimeDec,
                buttonStartTimeInc,
                buttonLoopsDec,
                buttonLoopsInc
        ).forEach { button ->
            button.foreground = DrawableState.getNew(colorSelector)
            button.clipToOutline = true
            button.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    view ?: return
                    outline ?: return

                    outline.setOval(0, 0, view.width, view.height)
                }

            }
        }




        buttonPeriodDec.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId).onPeriodChangeDec()
        }
        buttonPeriodInc.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId).onPeriodChangeInc()
        }
        buttonStartTimeDec.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId).onStartTimeChangeDec()
        }
        buttonStartTimeInc.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId).onStartTimeChangeInc()
        }
        buttonLoopsDec.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId).onLoopChangeDec()
        }
        buttonLoopsInc.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId).onLoopChangeInc()
        }

        buttonPeriodBlock.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId)
                .onPeriodChangeType(PaymentsViewModel.PeriodType.BLOCK)
        }

        buttonPeriodHour.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId)
                .onPeriodChangeType(PaymentsViewModel.PeriodType.HOUR)
        }

        buttonPeriodDay.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId)
                .onPeriodChangeType(PaymentsViewModel.PeriodType.DAY)
        }

        buttonStartTimeCurrentBlock.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId).onStartTimeCurrentBlock()
        }

        buttonStartTimePick.setOnClickListener {

            //todo
            //todo
            //todo
            //todo
            val model = viewModel.ensurePaymentHelper(paymentId).contentLD.value
                    ?: return@setOnClickListener

            val currentStartTimeCalendar = Calendar.getInstance()
            currentStartTimeCalendar.timeInMillis = model.millisStartBlock

            val dateBuilder = MaterialDatePicker.Builder.datePicker()
            dateBuilder.setTheme(R.style.AppThemeDateTimePicker1)
            dateBuilder.setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
            dateBuilder.setSelection(currentStartTimeCalendar.timeInMillis)
            val datePicker = dateBuilder.build()
            datePicker.addOnPositiveButtonClickListener {
                Timber.d("date selection: $it")

                val isSystem24Hour: Boolean = DateFormat.is24HourFormat(context)

                val clockFormat = if (isSystem24Hour) TimeFormat.CLOCK_24H else TimeFormat.CLOCK_12H

                val selectedDateCalendar = Calendar.getInstance()
                selectedDateCalendar.timeInMillis = it

//                val year = selectedDateCalendar.get(Calendar.YEAR)
//                val month = selectedDateCalendar.get(Calendar.MONTH)
//                val dayOfMonth = selectedDateCalendar.get(Calendar.DAY_OF_MONTH)


                val hour = currentStartTimeCalendar.get(Calendar.HOUR_OF_DAY)
                val minute = currentStartTimeCalendar.get(Calendar.MINUTE)

                val timeBuilder = MaterialTimePicker.Builder()
                timeBuilder.setInputMode(MaterialTimePicker.INPUT_MODE_CLOCK)
                timeBuilder.setTimeFormat(clockFormat)
                timeBuilder.setHour(hour)
                timeBuilder.setMinute(minute)

                val timePicker = timeBuilder.build()
                timePicker.addOnPositiveButtonClickListener {
                    Timber.d("time selection: $it")

                    val newHour: Int = timePicker.hour
                    val newMinute: Int = timePicker.minute

                    selectedDateCalendar.set(Calendar.HOUR_OF_DAY, newHour)
                    selectedDateCalendar.set(Calendar.MINUTE, newMinute)

                    viewModel.ensurePaymentHelper(paymentId)
                            .onStartTimePicked(selectedDateCalendar.timeInMillis)
                }

                timePicker.show(childFragmentManager, "datetime1")
            }

            datePicker.show(childFragmentManager, "datetime0")

        }

        buttonAmountUsd.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId)
                .onAmountChangeType(PaymentsViewModel.AmountType.USD)
        }

        buttonAmountSats.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId)
                .onAmountChangeType(PaymentsViewModel.AmountType.SATS)
        }

        buttonLoopOne.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId).onLoopChangeOne()
        }

        buttonLoopInfinite.setOnClickListener {
            viewModel.ensurePaymentHelper(paymentId).onLoopChangeInfinity()
        }





        attachHoldRepeater(buttonPeriodDec) {
            viewModel.ensurePaymentHelper(paymentId).onPeriodChangeDec()
        }
        attachHoldRepeater(buttonPeriodInc) {
            viewModel.ensurePaymentHelper(paymentId).onPeriodChangeInc()
        }
        attachHoldRepeater(buttonStartTimeDec) {
            viewModel.ensurePaymentHelper(paymentId).onStartTimeChangeDec()
        }
        attachHoldRepeater(buttonStartTimeInc) {
            viewModel.ensurePaymentHelper(paymentId).onStartTimeChangeInc()
        }
        attachHoldRepeater(buttonLoopsInc) {
            viewModel.ensurePaymentHelper(paymentId).onLoopChangeInc()
        }
        attachHoldRepeater(buttonLoopsDec) {
            viewModel.ensurePaymentHelper(paymentId).onLoopChangeDec()
        }



        inputTitle.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                s ?: return
                viewModel.ensurePaymentHelper(paymentId).onTitleChange(s.toString())
            }

        })





        destinationAdd.setOnClickListener {
            onAddDestinationClicked()
        }


        destinationLayout.removeFromListListener = {
            val value = it.item as String
            viewModel.ensurePaymentHelper(paymentId).onDestinationRemoved(value)
        }

        destinationLayout.scope = viewLifecycleOwner.lifecycleScope


        //todo
        //todo
        //todo
        //todo
        //todo
        //todo


        
        
        viewModel.ensurePaymentHelper(paymentId).contentLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            initFormFromModel(it)
            //todo
            //todo
            //todo
            //todo
            //todo

        })




        
        
        
        
        
        
    }


    private fun onAddDestinationClicked() {
        val value = inputDestination.text.toString().trim()
        if (value.isEmpty()) return

        //validate the input
        val isAddress = viewModel.isValidBitcoinAddress(value)
        val isValidAddress = viewModel.isValidEmailAddress(value)

        if (!isAddress && !isValidAddress) {
            return
        }


        //add destination

        viewModel.ensurePaymentHelper(paymentId).onDestinationAdded(value)

        inputDestination.setText("")

    }

    private fun attachHoldRepeater(view: View, onRepeat: () -> Unit) {

        view.setOnTouchListener(object : View.OnTouchListener {
            var repeatJob: Job? = null

            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                event ?: return false

                if (MotionEvent.ACTION_DOWN == event.action) {
                    startRepeater()

                } else if (MotionEvent.ACTION_UP == event.action ||
                        MotionEvent.ACTION_CANCEL == event.action) {
                    stopRepeater()

                }

                return false
            }


            fun startRepeater() {
                repeatJob?.cancel()

                repeatJob = viewLifecycleOwner.lifecycleScope.launch {
                    delay(333L)

                    while (true) {
                        onRepeat()
                        delay(50L)
                    }
                }
            }

            fun stopRepeater() {
                repeatJob?.cancel()
                repeatJob = null
            }

        })





    }


    private fun initFormFromModel(content: PaymentsViewModel.ManagePaymentModel) {
        if (valuePeriod.text.toString() != content.period.toString()) {
            valuePeriod.text = "${content.period}"
        }

        if (valueStartTime.text.toString() != content.startHeight.toString()) {
            valueStartTime.text = "${content.startHeight}"
        }

        val amountValue = when (content.amount.currency) {
            PaymentsViewModel.AmountType.USD -> {
                String.format(Locale.US, "%.2f %s", content.amount.valueUsd, "USD")
            }
            PaymentsViewModel.AmountType.SATS -> {
                String.format(Locale.US, "%d %s", content.amount.valueSats, "sats")
            }
        }
        if (valueAmount.text.toString() != amountValue) {
            valueAmount.text = amountValue
        }

        amountOptionsContent.applyModel(content.amount)



        if (valueLoops.tag != content.loops) {
            valueLoops.tag = content.loops

            if (PaymentsViewModel.COUNT_INFINITE == content.loops) {
                valueLoops.text = "Inf"
            } else {
                valueLoops.text = "${content.loops}"
            }
        }

        when (content.periodType) {
            PaymentsViewModel.PeriodType.BLOCK -> {
                buttonPeriodBlock.alpha = ALPHA_BUTTON_ACTIVE
                buttonPeriodHour.alpha = ALPHA_BUTTON_DIM
                buttonPeriodDay.alpha = ALPHA_BUTTON_DIM
            }
            PaymentsViewModel.PeriodType.HOUR -> {
                buttonPeriodBlock.alpha = ALPHA_BUTTON_DIM
                buttonPeriodHour.alpha = ALPHA_BUTTON_ACTIVE
                buttonPeriodDay.alpha = ALPHA_BUTTON_DIM
            }
            PaymentsViewModel.PeriodType.DAY -> {
                buttonPeriodBlock.alpha = ALPHA_BUTTON_DIM
                buttonPeriodHour.alpha = ALPHA_BUTTON_DIM
                buttonPeriodDay.alpha = ALPHA_BUTTON_ACTIVE
            }
        }

        when (content.amount.currency) {
            PaymentsViewModel.AmountType.USD -> {
                buttonAmountUsd.alpha = ALPHA_BUTTON_ACTIVE
                buttonAmountSats.alpha = ALPHA_BUTTON_DIM
            }
            PaymentsViewModel.AmountType.SATS -> {
                buttonAmountUsd.alpha = ALPHA_BUTTON_DIM
                buttonAmountSats.alpha = ALPHA_BUTTON_ACTIVE
            }
        }


        if (inputTitle.text.toString() != content.title) {
            inputTitle.setText(content.title)
        }


        val destinationModel = content.destinations.map {
            DestinationLayout.Model<String>(
                    it,
                    it,
                    true
            )
        }

        destinationLayout.init(destinationModel)


    }
    
    
    
    
}