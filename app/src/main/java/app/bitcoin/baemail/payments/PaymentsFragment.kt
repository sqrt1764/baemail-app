package app.bitcoin.baemail.payments

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.core.presentation.view.recycler.ATPaymentItem
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.twetch.ui.TwetchFilesMimeTypeHelper
import app.bitcoin.baemail.core.presentation.view.viewPager2.PeakingPageTransformer
import com.google.gson.Gson
import com.google.gson.JsonObject
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class PaymentsFragment : Fragment(R.layout.fragment_payments) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PaymentsViewModel








    private lateinit var viewPager: ViewPager2

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    private lateinit var paymentAdapter: BaseListAdapter

    private lateinit var buttonNew: Button
    private lateinit var button1: Button





    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

//        ///todo
//        val paymentId = "dummy"






        AndroidSupportInjection.inject(this)



        viewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(PaymentsViewModel::class.java)



//        viewModel.init(paymentId)






        buttonNew = requireView().findViewById(R.id.button0)
        button1 = requireView().findViewById(R.id.button1)
        viewPager = requireView().findViewById(R.id.pager)





        button1.setOnClickListener {

            viewLifecycleOwner.lifecycleScope.launch {

                val n0 = System.currentTimeMillis()
                val res = viewModel.nodeRuntimeRepository.runAsyncScript("""
                    await context.twetch.ensureSetupHasCompleted();
                    return JSON.stringify(await context.twetch.instance.me());
                """.trimIndent())
                val n1 = System.currentTimeMillis()

                try {
                    res as RunScript.Response.Success
                    val json = Gson().fromJson(res.content, JsonObject::class.java)

                    Timber.d("delta-millis:${n1 - n0} ..... >>>${json.get("me")}")
                } catch (e: Exception) {
                    Timber.e(e, "delta-millis:${n1 - n0}")
                }

            }
        }









        val atPaymentCarouselListener = object : BaseListAdapter.ATPaymentCarouselListener {

            override fun onInfoClick(paymentId: String) {
                val args = Bundle().also {
                    it.putString("id", paymentId)
                }
                findNavController().navigate(
                    R.id.action_paymentsFragment_to_paymentDetailsFragment,
                    args
                )
            }

            override fun onManageClick(paymentId: String) {
                val args = Bundle().also {
                    it.putString("id", paymentId)
                }
                findNavController().navigate(
                    R.id.action_paymentsFragment_to_paymentManageFragment,
                    args
                )
            }

        }


        paymentAdapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atPaymentCarouselListener = atPaymentCarouselListener
            )
        )

        viewPager.adapter = paymentAdapter
        viewPager.offscreenPageLimit = 2

        val peakSpaceWidthPx = R.dimen.payment_item__card_peak_space_width.let {
            resources.getDimensionPixelSize(it)
        }
        val peakGapPx = R.dimen.payment_item__card_peak_gap_width.let {
            resources.getDimensionPixelSize(it)
        }
        viewPager.setPageTransformer(PeakingPageTransformer(peakSpaceWidthPx.toFloat(), peakGapPx.toFloat()))

        viewPager.overScrollMode = View.OVER_SCROLL_NEVER









        buttonNew.setOnClickListener {
            //todo
            //todo
            //todo
            //todo

//            viewModel.deploy()


        }



        //todo
        //todo
        //todo
        val paymentList = listOf(
            ATPaymentItem("a0"),
            ATPaymentItem("a1"),
            ATPaymentItem("a2"),
            ATPaymentItem("a3"),
            ATPaymentItem("a4"),
            ATPaymentItem("a5"),
            ATPaymentItem("a6")
        )
        paymentAdapter.setItems(paymentList)


    }

}