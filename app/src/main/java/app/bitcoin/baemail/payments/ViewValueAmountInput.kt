package app.bitcoin.baemail.payments

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.data.util.match
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

class ViewValueAmountInput : ConstraintLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private val dp1: Float


    private val valueToPayee: TextView
    private val buttonMinus: Button
    private val buttonPlus: Button
    private val slider: Slider


    private val positionListTouchListener = PositionSliderTouchListener()



    private var valueModel: PaymentsViewModel.AmountModel = PaymentsViewModel.AmountModel.DEFAULT


    var onMinusClicked: ()->Unit = {
        Timber.d("onMinusClicked")
    }

    var onPlusClicked: ()->Unit = {
        Timber.d("onPlusClicked")
    }

    var onValueChanged: (Float)->Unit = {
        Timber.d("onValueChanged")
    }


    init {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.view_value_amount_input, this, false)
        val set = ConstraintSet()
        view.id = ViewCompat.generateViewId()
        addView(view)

        set.clone(this)
        set.match(view, this)

        dp1 = context.resources.displayMetrics.density


        valueToPayee = view.findViewById(R.id.value_to_payee)
        buttonMinus = view.findViewById(R.id.button_minus)
        buttonPlus = view.findViewById(R.id.button_plus)
        slider = view.findViewById(R.id.slider)


        val colorStroke = ContextCompat.getColor(context, R.color.manage_payment__amount_container_background_color1)
        val colorBackground = ContextCompat.getColor(context, R.color.manage_payment__amount_container_background_color)

        val drawableStroke = DrawableStroke().also {
            it.setStrokeColor(colorStroke)
            it.setStrokeWidths(0, (8 * dp1).toInt(), 0, 0)
            it.setPadding(0, 0, 0, 0)
        }

        background = LayerDrawable(arrayOf(
            ColorDrawable(context.getColorFromAttr(R.attr.colorSurface)),
            ColorDrawable(colorBackground),
            drawableStroke
        ))




        slider.isSaveEnabled = false
        slider.setOnTouchListener(positionListTouchListener)
        slider.addOnChangeListener { slider, value, fromUser ->
            if (!fromUser) return@addOnChangeListener

            refreshLabelToPayee(value)
        }
        slider.labelBehavior = LabelFormatter.LABEL_GONE


        buttonMinus.setOnClickListener {
            onMinusClicked()
        }

        buttonPlus.setOnClickListener {
            onPlusClicked()
        }


        refresh()
    }

    private fun refresh() {

        when (valueModel.currency) {
            PaymentsViewModel.AmountType.USD -> {
                refreshLabelToPayee(valueModel.valueUsd)

                slider.apply {
                    valueFrom = 0f

                    valueTo = valueModel.valueMaxUsd
                    valueFrom = valueModel.valueMinUsd
                    stepSize = 0f

                    value = valueModel.valueUsd
                    invalidate()
                }
            }
            PaymentsViewModel.AmountType.SATS -> {
                refreshLabelToPayee(valueModel.valueSats.toFloat())

                slider.apply {
                    valueFrom = 0f

                    valueTo = valueModel.valueMaxSats.toFloat()
                    valueFrom = valueModel.valueMinSats.toFloat()
                    stepSize = 0f

                    value = valueModel.valueSats.toFloat()
                    invalidate()
                }
            }
        }

    }

    private fun refreshLabelToPayee(value: Float) {
        val currency = valueModel.currency

        if (PaymentsViewModel.AmountType.USD == currency) {
            valueToPayee.text = String.format(Locale.US, "%.2f %s", value, "USD")
        } else {
            valueToPayee.text = String.format(Locale.US, "%.0f %s", value, "sats")
        }
    }

    fun applyModel(model: PaymentsViewModel.AmountModel) {
        if (model == valueModel) return
        valueModel = model

        refresh()
    }






    inner class PositionSliderTouchListener : OnTouchListener {
        private val scope = CoroutineScope(Dispatchers.Main)

        @SuppressLint("ClickableViewAccessibility")
        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            event ?: return false
            v ?: return false

            when (event.actionMasked) {
                MotionEvent.ACTION_UP -> {
                    onActionUp(v)
                }
            }

            return false
        }

        private fun onActionUp(view: View) {
            view as Slider

            scope.launch {
                delay(50)

                onValueChanged(view.value)


            }
        }

    }

}