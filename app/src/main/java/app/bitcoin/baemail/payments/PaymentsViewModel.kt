package app.bitcoin.baemail.payments

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.data.wallet.BlockchainDataSource
import app.bitcoin.baemail.core.data.wallet.DynamicChainSync
import app.bitcoin.baemail.core.data.room.AppDatabase
import app.bitcoin.baemail.core.data.util.BitcoinAddressUtils
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PaymentsViewModel @Inject constructor(
    val nodeRuntimeRepository: NodeRuntimeRepository,
//    private val baemailRepository: BaemailRepository,
    private val coroutineUtil: CoroutineUtil,
    private val appDatabase: AppDatabase,
    private val secureDataSource: SecureDataSource,
    private val blockchainDataSource: BlockchainDataSource,
//    private val exchangeRateHelper: ExchangeRateHelper,
    private val dynamicChainSync: DynamicChainSync
) : ViewModel() {

//    lateinit var paymentId: String

//    fun init(paymentId: String) {
//        this.paymentId = paymentId
//    }



//
//    fun deploy() {
//        viewModelScope.launch {
//
//            val activePaymail = paymailRepository.activePaymailLD.value?.paymail ?: let {
//                Timber.e("active-payamail not found")
//                return@launch
//            }
//
//            if (dynamicChainSync.statusLD.value != ChainSync.Status.LIVE) {
//                Timber.e("coins are still syncing")
//                return@launch
//            }
//
//            val fundingWalletSeed = try {
//                secureDataSource.getParallelWalletSeed(AuthenticatedPaymail(activePaymail))
//            } catch (e: Exception) {
//                Timber.e(e, "error retrieving seed")
//                return@launch
//            }
//
//
//
//            val keyHolder = walletRepository
//                .getPrivateKeyForPathFromSeed("m/44'/0'/0'/1/0", fundingWalletSeed) ?: let {
//                Timber.e("error looking up key")
//                return@launch
//            }
//
//
////            val f = File("a")
////            f.
//
//
//
//
//            val unspentCoinsList = appDatabase.coinDao().getFundingCoins(activePaymail)
//                .mapNotNull {
//                    if (it.spendingTxId != null) return@mapNotNull null
//                    it
//                }
//
//
//            if (unspentCoinsList.isEmpty()) {
//                Timber.e("there are no unspent coins")
//                return@launch
//            }
//
//            val unspentCoinsMap: Map<String, ReceivedCoins> = unspentCoinsList.let {
//                val map = mutableMapOf<String, ReceivedCoins>()
//
//                it.forEach { c ->
//                    map[c.derivationPath] = ReceivedCoins(
//                        c.txId,
//                        c.sats,
//                        c.outputIndex,
//                        c.address
//                    )
//                }
//
//                map
//            }
//
//
//
//
//            val upcomingUnusedFundingWalletAddresses = unusedAddressHelper
//                .getUnusedAddressesOfFundingWallet(activePaymail) // will be used for a change-address
//
//            val changeAddress = upcomingUnusedFundingWalletAddresses[0]
//            Timber.d("provided changeAddress for the deploy message: $changeAddress")
//
//
//
//
//
//
//
//            val response = walletRepository.superAssetDeploy(
//                SuperAssetDeployNft.Owner(
//                    keyHolder.path,
//                    keyHolder.publicKey,
//                    keyHolder.privateKey
//                ),
//                SuperAssetDeployNft.Funds(
//                    fundingWalletSeed,
//                    PaymailRepository.FUNDING_WALLET_ROOT_PATH,
//                    unspentCoinsMap,
//                    changeAddress.address
//                )
//            )
//
//
//
//
//
//            Timber.d("depoly response:$response")
//        }
//    }


    private val paymentHelperCache = HashMap<String, ManagePaymentHelper>()

    fun ensurePaymentHelper(paymentId: String): ManagePaymentHelper {
        return paymentHelperCache[paymentId] ?: let {
            val helper = ManagePaymentHelper(paymentId, dynamicChainSync)

            paymentHelperCache[paymentId] = helper

            return@let helper
        }
    }




    fun isValidBitcoinAddress(value: String): Boolean {
        return BitcoinAddressUtils.isRegularAddress(value)
    }

    fun isValidEmailAddress(value: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(value).matches()
    }


    class ManagePaymentHelper(
        val paymentId: String,
        val dynamicChainSync: DynamicChainSync
    ) {

        private val _contentLD = MutableLiveData<ManagePaymentModel>()
        val contentLD: LiveData<ManagePaymentModel>
            get() = _contentLD


        init {

            val amount = AmountModel.DEFAULT

            val currentHeight = dynamicChainSync.calculateCurrentBlockHeight()

            _contentLD.value = ManagePaymentModel(
                    UUID.randomUUID().toString(),
                    "dummy title",
                    6,
                    PeriodType.BLOCK,
                    currentHeight,
                    dynamicChainSync.calculateTimeOfBlockHeight(currentHeight),
                    amount,
                    7,
                    listOf(
                        "aa@aa.aa",
                        "bb@aa.aa",
                        "cc@aa.aa",
                        "dd@aa.aa",
                        "ff@aa.aa"
                    ),
                    false
            )
        }

        fun onPeriodChangeInc() {
            val model = contentLD.value ?: return
            _contentLD.setValue(model.copy(
                period = model.period + 1
            ))
        }

        fun onPeriodChangeDec() {
            val model = contentLD.value ?: return
            if (model.period == 1) return
            _contentLD.setValue(model.copy(
                period = model.period - 1
            ))
        }

        fun onPeriodChangeType(type: PeriodType) {
            val model = contentLD.value ?: return
            val currentType = model.periodType
            if (type == currentType) return

            //todo maybe also update the period itself?

            _contentLD.setValue(model.copy(
                periodType = type
            ))
        }

        fun onStartTimeChangeInc() {
            val model = contentLD.value ?: return

            val newHeight = model.startHeight + 1
            _contentLD.setValue(model.copy(
                    startHeight = newHeight,
                    millisStartBlock = dynamicChainSync.calculateTimeOfBlockHeight(newHeight)
            ))
        }

        fun onStartTimeChangeDec() {
            val model = contentLD.value ?: return
            if (model.startHeight == 0) return

            val newHeight = model.startHeight - 1
            _contentLD.setValue(model.copy(
                    startHeight = newHeight,
                    millisStartBlock = dynamicChainSync.calculateTimeOfBlockHeight(newHeight)
            ))
        }

        fun onStartTimeCurrentBlock() {
            val model = contentLD.value ?: return
            val currentBlockHeight = dynamicChainSync.calculateCurrentBlockHeight()
            if (model.startHeight == currentBlockHeight) return

            _contentLD.setValue(model.copy(
                    startHeight = currentBlockHeight,
                    millisStartBlock = dynamicChainSync.calculateTimeOfBlockHeight(currentBlockHeight)
            ))
        }

        fun onStartTimePicked(millis: Long) {
            Timber.d("onStartTimePicked:$millis")
            val model = contentLD.value ?: return


            val height = dynamicChainSync.calculateBlockHeightOfTime(millis)

            if (model.startHeight == height) return

            _contentLD.setValue(model.copy(
                    startHeight = height,
                    millisStartBlock = dynamicChainSync.calculateTimeOfBlockHeight(height)
            ))

        }

        fun onAmountChangeInc() {
            val model = contentLD.value ?: return
            val modelAmount = model.amount

            when (modelAmount.currency) {
                AmountType.SATS -> {

                    val newMax = modelAmount.valueMaxSats * 10
                    if (newMax > AmountModel.MAX_SATS) return

                    val newMin = newMax / 10
                    val newModelAmount = AmountModel(
                        modelAmount.valueMinUsd,
                        modelAmount.valueMaxUsd,
                        modelAmount.valueUsd,
                        newMin,
                        newMax,
                        newMin,
                        modelAmount.currency
                    )

                    _contentLD.setValue(model.copy(
                        amount = newModelAmount
                    ))

                }
                AmountType.USD -> {

                    val newMax = modelAmount.valueMaxUsd * 10
                    if (newMax > AmountModel.MAX_USD) return

                    val newMin = newMax / 10

                    val newModelAmount = AmountModel(
                        newMin,
                        newMax,
                        newMin,
                        modelAmount.valueMinSats,
                        modelAmount.valueMaxSats,
                        modelAmount.valueSats,
                        modelAmount.currency
                    )

                    _contentLD.setValue(model.copy(
                        amount = newModelAmount
                    ))

                }
            }
        }

        fun onAmountChangeDec() {
            val model = contentLD.value ?: return
            val modelAmount = model.amount

            when (modelAmount.currency) {
                AmountType.SATS -> {

                    val newMax = modelAmount.valueMaxSats / 10
                    if (newMax < AmountModel.MIN_SATS) return

                    val newMin = newMax / 10
                    val newModelAmount = AmountModel(
                            modelAmount.valueMinUsd,
                            modelAmount.valueMaxUsd,
                            modelAmount.valueUsd,
                            newMin,
                            newMax,
                            newMin,
                            modelAmount.currency
                    )

                    _contentLD.setValue(model.copy(
                            amount = newModelAmount
                    ))

                }
                AmountType.USD -> {

                    val newMax = modelAmount.valueMaxUsd / 10
                    if (newMax < AmountModel.MIN_USD) return

                    val newMin = newMax / 10

                    val newModelAmount = AmountModel(
                            newMin,
                            newMax,
                            newMin,
                            modelAmount.valueMinSats,
                            modelAmount.valueMaxSats,
                            modelAmount.valueSats,
                            modelAmount.currency
                    )

                    _contentLD.setValue(model.copy(
                            amount = newModelAmount
                    ))

                }
            }
        }

        fun onAmountChangeValue(value: Float) {
            val model = contentLD.value ?: return
            val modelAmount = model.amount

            when (modelAmount.currency) {
                AmountType.SATS -> {

                    if (value < modelAmount.valueMinSats) return
                    if (value > modelAmount.valueMaxSats) return

                    val newModelAmount = modelAmount.copy(
                            valueSats = value.toInt()
                    )

                    _contentLD.setValue(model.copy(
                            amount = newModelAmount
                    ))

                }
                AmountType.USD -> {

                    if (value < modelAmount.valueMinUsd) return
                    if (value > modelAmount.valueMaxUsd) return

                    val newModelAmount = modelAmount.copy(
                            valueUsd = value
                    )

                    _contentLD.setValue(model.copy(
                            amount = newModelAmount
                    ))

                }
            }
        }

        fun onAmountChangeType(type: AmountType) {
            val model = contentLD.value ?: return
            val currentAmountType = model.amount.currency
            if (currentAmountType == type) return

            _contentLD.setValue(model.copy(
                amount = model.amount.copy(currency = type)
            ))
        }

        fun onLoopChangeInc() {
            val model = contentLD.value ?: return

            val loops = if (model.loops == COUNT_INFINITE) {
                1
            } else {
                model.loops + 1
            }

            _contentLD.setValue(model.copy(
                loops = loops
            ))
        }

        fun onLoopChangeDec() {
            val model = contentLD.value ?: return
            val currentLoops = model.loops

            val loops = if (currentLoops == COUNT_INFINITE) {
                1
            } else if (currentLoops == 1) {
                return
            } else {
                currentLoops - 1
            }

            _contentLD.setValue(model.copy(
                loops = loops
            ))
        }

        fun onLoopChangeOne() {
            val model = contentLD.value ?: return
            if (model.loops == 1) return

            _contentLD.postValue(model.copy(
                loops = 1
            ))
        }

        fun onLoopChangeInfinity() {
            val model = contentLD.value ?: return
            if (model.loops == COUNT_INFINITE) return

            _contentLD.setValue(model.copy(
                loops = COUNT_INFINITE
            ))
        }


        fun onTitleChange(value: String) {
            val p0 = value.trim()
            val model = contentLD.value ?: return
            if (p0 == model.title) return

            _contentLD.setValue(model.copy(
                title = p0
            ))

//            /////
//            onDraftContentChanged()
//
//            /////
//
//            refreshPaymailExistenceCheck()
        }

        fun onDestinationAdded(value: String) {
            val model = contentLD.value ?: return

            if (model.destinations.contains(value)) return

            val newList = ArrayList(model.destinations)
            newList.add(value)

            _contentLD.setValue(model.copy(
                destinations = newList
            ))

            //todo validate that paymail exists if it is a paymail
        }

        fun onDestinationRemoved(value: String) {
            val model = contentLD.value ?: return
            val newList = ArrayList(model.destinations)
            newList.remove(value)

            _contentLD.setValue(model.copy(
                destinations = newList
            ))
        }

    }


    enum class PeriodType {
        BLOCK,
        HOUR,
        DAY
    }

    enum class AmountType {
        USD,
        SATS
    }

    data class AmountModel(
        val valueMinUsd: Float,
        val valueMaxUsd: Float,
        val valueUsd: Float,
        val valueMinSats: Int,
        val valueMaxSats: Int,
        val valueSats: Int,
        val currency: AmountType
    ) {
        companion object {
            val MIN_USD = 0.1f
            val MAX_USD = 100f
            val MIN_SATS = 1000
            val MAX_SATS = 100_000_000

            val DEFAULT = AmountModel(
                    MIN_USD,
                    MIN_USD * 10,
                    MIN_USD,
                    MIN_SATS,
                    MIN_SATS * 10,
                    MIN_SATS,
                AmountType.SATS
            )
        }
    }

    data class ManagePaymentModel(
        val token: String,
        val title: String?,
        val period: Int,
        val periodType: PeriodType,
        val startHeight: Int,
        val millisStartBlock: Long,
        val amount: AmountModel,
        val loops: Int,
        val destinations: List<String>,
        val isValid: Boolean
    )


    companion object {
        const val COUNT_INFINITE = -1
    }
}