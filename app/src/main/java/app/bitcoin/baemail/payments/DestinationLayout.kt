package app.bitcoin.baemail.payments

import android.content.Context
import android.graphics.Outline
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.math.min


class DestinationLayout : LinearLayout {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    private lateinit var noDestinationsView: ViewGroup

    private val currentItems = ArrayList<Model<*>>()

    var removeFromListListener: (Model<*>)->Unit = {
        Timber.d("remove-from-list not implemented")
    }

    private var _removeFromListListener: (Model<*>)->Unit = {
        removeFromListListener(it)
    }

    lateinit var scope: CoroutineScope

    override fun onFinishInflate() {
        super.onFinishInflate()

        noDestinationsView = inflater.inflate(
            R.layout.view_payment_destination_empty,
            this,
            false
        ) as ViewGroup

        addView(noDestinationsView)

    }



    fun init(items: List<Model<*>>) {
        //todo return if equals

        val wasListEmpty = currentItems.isEmpty()
        val isNewListEmpty = items.isEmpty()

        currentItems.clear()
        currentItems.addAll(items)


        if (wasListEmpty && !isNewListEmpty) {
            removeView(noDestinationsView)
        }

        if (wasListEmpty && isNewListEmpty) {
            return
        }

        initItems()

        if (!wasListEmpty && isNewListEmpty) {
            addView(noDestinationsView)
        }
    }


    private fun initItems() {
        val extraViews = childCount - currentItems.size

        if (extraViews > 0) {
            //remove the extra views that will not be necessary further down
            removeViews(currentItems.size, extraViews)
        }

        currentItems.forEachIndexed { index, model ->
            val matchingView = getChildAt(index)

            val holder: ItemHolder = if (matchingView == null) {
                val h = ItemHolder.inflate(inflater, this, _removeFromListListener, scope)
                addView(h.view, index)

                h
            } else {
                matchingView.tag as ItemHolder
            }

            holder.init(model)
        }

    }



    data class Model<T>(
        val item: T,
        val label: String,
        val isValid: Boolean?
    )


    data class ItemHolder(
        val view: ViewGroup,
        val clear: ImageButton,
        val isValid: ImageView,
        val label: TextView,
        var item: Model<*>? = null
    ) {


        fun init(model: Model<*>) {

            if (item?.label != model.label) {
                label.text = model.label
            }

            this.item = model
        }



        companion object {

            fun inflate(
                inflater: LayoutInflater,
                parent: DestinationLayout,
                removeFromList: (Model<*>)->Unit,
                scope: CoroutineScope
            ): ItemHolder {
                val view: ViewGroup = inflater.inflate(R.layout.view_payment_destination_li, parent, false)
                        as ViewGroup

                val clear: ImageButton = view.findViewById(R.id.destination_clear)
                val isValid: ImageView = view.findViewById(R.id.destination_is_valid)
                val value: TextView = view.findViewById(R.id.destination_value)

                val holder = ItemHolder(view, clear, isValid, value)
                view.tag = holder


                val colorSelector = R.color.selector_on_surface.let { id ->
                    ContextCompat.getColor(view.context, id)
                }
                clear.background = DrawableState.getNew(colorSelector)


                clear.clipToOutline = true
                clear.outlineProvider = object : ViewOutlineProvider() {
                    override fun getOutline(view: View?, outline: Outline?) {
                        view ?: return
                        outline ?: return

                        val roundness = min(view.width / 2f, view.height / 2f)

                        outline.setRoundRect(0, 0, view.width, view.height, roundness)
                    }

                }

                clear.setOnClickListener {

                    scope.launch {
                        delay(200L)

                        val item = holder.item
                        if (item == null) {
                            Timber.i("remove-from-list clicked; doing nothing because the model is missing")
                            return@launch
                        }
                        removeFromList(item)
                        clear.background.jumpToCurrentState()
                    }
                }

                return holder
            }

        }
    }


}