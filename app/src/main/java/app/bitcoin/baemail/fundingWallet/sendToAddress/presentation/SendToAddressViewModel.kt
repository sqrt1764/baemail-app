package app.bitcoin.baemail.fundingWallet.sendToAddress.presentation

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.domain.entities.CoinRef
import app.bitcoin.baemail.core.domain.usecases.CheckAddressIsValidUseCase
import app.bitcoin.baemail.core.domain.usecases.CoinSyncStatusUseCase
import app.bitcoin.baemail.core.domain.usecases.GetCoinUseCase
import app.bitcoin.baemail.fundingWallet.equalSplit.domain.ErrorConstructingTx
import app.bitcoin.baemail.fundingWallet.equalSplit.domain.ErrorNotReady
import app.bitcoin.baemail.fundingWallet.equalSplit.domain.TxNotAcceptedByMiner
import app.bitcoin.baemail.fundingWallet.sendToAddress.domain.SendCoinToAddressUseCase
import app.bitcoin.baemail.core.data.room.entity.Coin
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

class SendToAddressViewModel @Inject constructor(
    private val getCoinUseCase: GetCoinUseCase,
    coinSyncStatusUseCase: CoinSyncStatusUseCase,
    checkAddressIsValidUseCase: CheckAddressIsValidUseCase,
    private val sendCoinToAddressUseCase: SendCoinToAddressUseCase
) : ViewModel() {

    private val coinRef = MutableStateFlow<CoinRef?>(null)
    private val destinationAddress = MutableStateFlow("")

    private val coin: StateFlow<Coin?> = coinRef.flatMapLatest { ref ->
        ref ?: return@flatMapLatest flow { emit(null) }
        getCoinUseCase(ref)
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = null
    )

    val state: StateFlow<SendToAddressModel> =
        coin.combine(coinSyncStatusUseCase()) { coin, syncStatus ->
            coin to syncStatus

        }.combine(destinationAddress) { (coin, syncStatus), address ->

            val sats = coin?.sats ?: 0

            val state = if (coin == null) {
                SendToAddressState.ERROR_COIN_INVALID
            } else if (ChainSyncStatus.DISCONNECTED == syncStatus) {
                SendToAddressState.ERROR_NO_INTERNET
            } else if (!checkAddressIsValidUseCase(address)) {
                SendToAddressState.NOT_AN_ADDRESS
            } else {
                SendToAddressState.VALID
            }

            SendToAddressModel(
                input = address,
                value = sats,
                state = state
            )

        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = SendToAddressModel(
                input = "",
                value = 0,
                state = SendToAddressState.ERROR_COIN_INVALID
            )
        )

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()


    fun setCoin(
        txHash: String,
        address: String,
        outputIndex: Int,
    ) {
        coinRef.value = CoinRef(address, txHash, outputIndex)
    }


    fun onDestinationAddressChanged(address: String) {
        destinationAddress.value = address.trim()
    }

    fun onRequestedSend() {
        viewModelScope.launch {
            _eventFlow.emit(UiEvent.Processing)

            try {
                val coin = coin.value ?: throw RuntimeException()
                sendCoinToAddressUseCase(coin, destinationAddress.value)
                _eventFlow.emit(UiEvent.SendSuccess)

            } catch (e: ErrorNotReady) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_no_internet))

            } catch (e: ErrorConstructingTx) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_failed_constructing_tx))

            } catch (e: TxNotAcceptedByMiner) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_tx_not_accepted))

            } catch (e: Exception) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error))
            }
        }
    }

    sealed class UiEvent {
        object Processing: UiEvent()
        object SendSuccess: UiEvent()
        data class ShowSnackbar(@StringRes val message: Int): UiEvent()
    }

    data class SendToAddressModel(
        val input: String,
        val value: Long,
        val state: SendToAddressState
    )

    enum class SendToAddressState {
        NOT_AN_ADDRESS,
        VALID,
        ERROR_COIN_INVALID,
        ERROR_NO_INTERNET
    }

}