package app.bitcoin.baemail.fundingWallet.core.domain.entity

import com.google.gson.annotations.SerializedName

data class P2pTxMetadata(
    @SerializedName("sender") val sender: String,
    @SerializedName("pubkey") val pubkey: String,
    @SerializedName("signature") val signature: String,
    @SerializedName("note") val note: String
)