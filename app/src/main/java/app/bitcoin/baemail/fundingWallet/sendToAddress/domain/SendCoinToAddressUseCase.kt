package app.bitcoin.baemail.fundingWallet.sendToAddress.domain

import app.bitcoin.baemail.core.domain.entities.ReceivedCoin
import app.bitcoin.baemail.core.domain.repository.BsvStatusRepository
import app.bitcoin.baemail.core.domain.repository.CoinRepository
import app.bitcoin.baemail.core.domain.usecases.CalculateBasicTxSizeUseCase
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.domain.repository.SecureDataRepository
import kotlinx.coroutines.flow.first
import kotlin.math.nextUp

class SendCoinToAddressUseCaseImpl(
    private val coinRepository: CoinRepository,
    private val secureDataRepository: SecureDataRepository,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val bsvStatusRepository: BsvStatusRepository,
    private val calculateBasicTxSizeUseCase: CalculateBasicTxSizeUseCase
): SendCoinToAddressUseCase {
    override suspend fun invoke(coin: Coin, address: String) {
        val liveBsvBlockInfo = bsvStatusRepository.liveBlockInfo.first()
            ?: throw ErrorNotReady()

        val fundingWalletSeed = secureDataRepository.getFundingWalletSeed(coin.paymail)


        val txSize = calculateBasicTxSizeUseCase(1, 1)

        val txFee = bsvStatusRepository.txFee.first()
        val estimatedFee = (txSize * txFee.satsPerByte).nextUp().toInt()

        val txInfo = nodeRuntimeRepository.createSimpleTx(
            fundingWalletSeed,
            mapOf(coin.derivationPath to ReceivedCoin(
                coin.txId,
                coin.sats,
                coin.outputIndex,
                coin.address
            )
            ),
            address,
            coin.sats - estimatedFee
        )
        txInfo ?: throw ErrorConstructingTx()


        val success = bsvStatusRepository.sendTx(txInfo.txHex)
        if (!success) {
            throw TxNotAcceptedByMiner()
        }

        //check if the user is sending the coin to one of his own addresses
        val destinationAddressList = coinRepository
            .getUpcomingUnusedAddresses(coin.paymail, 20)
            .first()

        val createdUtxo = txInfo.output
        val matchingAddress = destinationAddressList.find { it.address == createdUtxo.address }
        if (matchingAddress != null) {

            val receivedCoin = Coin(
                paymail = coin.paymail,
                derivationPath = matchingAddress.path,
                pathIndex = matchingAddress.getIndex(),
                address = address,
                txId = txInfo.txHash,
                outputIndex = createdUtxo.outputIndex,
                sats = createdUtxo.sats,
                txHeight = liveBsvBlockInfo.height + 1
            )

            coinRepository.saveCoins(listOf(receivedCoin))
        }


        coinRepository.markCoinsSpent(listOf(coin))
    }
}

interface SendCoinToAddressUseCase {
    suspend operator fun invoke(coin: Coin, address: String)
}

class ErrorNotReady : Exception()
class ErrorConstructingTx : Exception()
class TxNotAcceptedByMiner : Exception()