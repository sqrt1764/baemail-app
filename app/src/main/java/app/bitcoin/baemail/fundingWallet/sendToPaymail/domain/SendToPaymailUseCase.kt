package app.bitcoin.baemail.fundingWallet.sendToPaymail.domain

import app.bitcoin.baemail.core.domain.repository.BsvStatusRepository
import app.bitcoin.baemail.core.domain.repository.CoinRepository
import app.bitcoin.baemail.core.domain.usecases.CalculateBasicTxSizeUseCase
import app.bitcoin.baemail.core.domain.entities.ReceivedCoin
import app.bitcoin.baemail.core.data.nodejs.request.CreateSimpleOutputScriptTx
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.fundingWallet.core.domain.entity.P2pTxMetadata
import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.domain.repository.PaymailInfoRepository
import app.bitcoin.baemail.core.domain.repository.SecureDataRepository
import app.bitcoin.baemail.core.domain.usecases.CheckPaymailExistsUseCase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull
import kotlin.math.max
import kotlin.math.nextUp

class SendToPaymailUseCaseImpl(
    private val coinRepository: CoinRepository,
    private val secureDataRepository: SecureDataRepository,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val bsvStatusRepository: BsvStatusRepository,
    private val checkPaymailExistsUseCase: CheckPaymailExistsUseCase,
    private val calculateBasicTxSizeUseCase: CalculateBasicTxSizeUseCase,
    private val signMessageUseCase: SignMessageUseCase,
    private val paymailInfoRepository: PaymailInfoRepository
) : SendToPaymailUseCase {

    override suspend fun invoke(coin: Coin, paymail: String, note: String) {

        bsvStatusRepository.liveBlockInfo.first() ?: throw ErrorNotReady()

        val paymailExistence = checkPaymailExistsUseCase(paymail).mapNotNull {
            if (it is PaymailExistence.Checking) return@mapNotNull null
            else it
        }.first()
        if (paymailExistence !is PaymailExistence.Valid) throw ErrorPaymailDoesNotExist()

        val fundingWalletSeed = secureDataRepository.getFundingWalletSeed(coin.paymail)

        if (paymailExistence.supportsP2p) {
            sendToPaymailP2p(coin, fundingWalletSeed, paymail, note)
        } else {
            sendToPaymailFallback(coin, fundingWalletSeed, paymail)
        }

        coinRepository.markCoinsSpent(listOf(coin))

    }

    private suspend fun sendToPaymailP2p(
        coin: Coin,
        fundingWalletSeed: List<String>,
        paymail: String,
        note: String
    ) {
        val destinationInfo = paymailInfoRepository.getP2pPaymentDestinationInfo(
            paymail,
            coin.sats
        ) ?: throw ErrorGettingDestinationOutput()

        val txSize = calculateBasicTxSizeUseCase(1, destinationInfo.outputs.size)
        val txFeeInfo = bsvStatusRepository.txFee.first()
        val estimatedFee = max(
            (txSize * txFeeInfo.satsPerByte).nextUp().toInt(),
            destinationInfo.outputs.size
        )
        val feePerOutput = estimatedFee / destinationInfo.outputs.size

        val destinationList = destinationInfo.outputs.map { output ->
            CreateSimpleOutputScriptTx.Destination(
                CreateSimpleOutputScriptTx.DestinationType.OUTPUT_SCRIPT,
                output.script,
                output.satoshis - feePerOutput
            )
        }

        val txInfo = nodeRuntimeRepository.createSimpleOutputScriptTx(
            fundingWalletSeed,
            mapOf(coin.derivationPath to ReceivedCoin(
                coin.txId,
                coin.sats,
                coin.outputIndex,
                coin.address
            )),
            destinationList
        )

        txInfo ?: throw ErrorConstructingTx()

        try {
            paymailInfoRepository.sendP2pPayment(
                paymail,
                txInfo.txHex,
                buildP2pMetadata(
                    coin.paymail,
                    txInfo.txHash,
                    note.trim()
                ),
                destinationInfo.reference
            )
        } catch (e: Exception) {
            throw TxNotAcceptedByServer(e)
        }
    }

    private suspend fun sendToPaymailFallback(
        coin: Coin,
        fundingWalletSeed: List<String>,
        paymail: String
    ) {
        val paymailOutputScript = paymailInfoRepository.getPaymailDestinationOutput(
            coin.paymail,
            paymail
        ) { sendingPaymail, message ->
            signMessageUseCase(
                sendingPaymail,
                message
            )?.signature
        } ?: let {
            throw ErrorGettingDestinationOutput()
        }

        val txSize = calculateBasicTxSizeUseCase(1, 1)
        val txFeeInfo = bsvStatusRepository.txFee.first()
        val estimatedFee = (txSize * txFeeInfo.satsPerByte).nextUp().toInt()

        val txInfo = nodeRuntimeRepository.createSimpleOutputScriptTx(
            fundingWalletSeed,
            mapOf(coin.derivationPath to ReceivedCoin(
                coin.txId,
                coin.sats,
                coin.outputIndex,
                coin.address
            )
            ),
            listOf(CreateSimpleOutputScriptTx.Destination(
                CreateSimpleOutputScriptTx.DestinationType.OUTPUT_SCRIPT,
                paymailOutputScript,
                coin.sats - estimatedFee
            ))
        )

        txInfo ?: throw ErrorConstructingTx()

        val success = bsvStatusRepository.sendTx(txInfo.txHex)
        if (!success) {
            throw TxNotAcceptedByServer()
        }
    }

    private suspend fun buildP2pMetadata(
        senderPaymail: String,
        txId: String,
        note: String
    ): P2pTxMetadata {
        val signingResponse = signMessageUseCase(senderPaymail, txId) ?: throw ErrorSubmittingTx()
        return P2pTxMetadata(
            senderPaymail,
            signingResponse.pubkey,
            signingResponse.signature,
            note
        )
    }

}

interface SendToPaymailUseCase {
    suspend operator fun invoke(coin: Coin, paymail: String, note: String)
}

class ErrorNotReady : Exception()
class ErrorPaymailDoesNotExist : Exception()
class ErrorGettingDestinationOutput : Exception()
class ErrorConstructingTx : Exception()
class ErrorSubmittingTx : Exception()
class TxNotAcceptedByServer(e: Exception? = null) : Exception(e)