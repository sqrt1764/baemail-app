package app.bitcoin.baemail.fundingWallet.core.presentation.compose

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import java.util.Locale

@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode of ComposeFundingCoinsOverview"
)
@Composable
fun ComposeFundingCoinsOverviewPreview() {
    ComposeAppTheme {
        ComposeFundingCoinsOverview(
            "yaanis@moneybutton.com",
            13,
            "49 86 759 423",
            3210,
            {},
            {}
        )
    }
}


@Composable
fun ComposeFundingCoinsOverview(
    paymail: String,
    unspentCoins: Int,
    satsAvailable: String,
    highestUsedAddressIndex: Int,
    onClickSyncConfig: ()->Unit,
    onClickMnemonics: ()->Unit
) {
    val scrollState = rememberScrollState()

    val borderColor = MaterialTheme.colors.onBackground.copy(alpha = 0.3f)
    val labelColor = MaterialTheme.colors.onBackground.copy(alpha = 0.7f)

    val backgroundColor = MaterialTheme.colors.onBackground.copy(alpha = 0.15f)
    val buttonColors = ButtonDefaults.outlinedButtonColors(
        backgroundColor = backgroundColor
    )

    val labelTextSize = MaterialTheme.typography.caption.fontSize.times(0.8f)
    
    Column(
        Modifier
            .fillMaxWidth()
            .padding(start = 16.dp, top = 12.dp, end = 16.dp, bottom = 12.dp)
    ) {
        Text(
            text = stringResource(R.string.funding_wallet_for).uppercase(Locale.ROOT),
            style = MaterialTheme.typography.caption,
            fontSize = labelTextSize,
            color = labelColor,
            fontWeight = FontWeight.ExtraBold
        )
        Text(
            paymail,
            style = MaterialTheme.typography.h2,
            color = MaterialTheme.colors.primary,
            lineHeight = MaterialTheme.typography.h2.fontSize.times(0.6f)
        )
        Spacer(Modifier.height(9.dp))
        Text(
            stringResource(R.string.x_unspent_coins, unspentCoins),
            style = MaterialTheme.typography.caption
        )
        Text(
            stringResource(R.string.x_sats, satsAvailable),
            style = MaterialTheme.typography.caption
        )
        Text(
            stringResource(R.string.highest_used_address_index, highestUsedAddressIndex),
            style = MaterialTheme.typography.caption
        )
        Spacer(Modifier.height(5.dp))
        Row(Modifier.horizontalScroll(scrollState)) {
            OutlinedButton(
                onClick = onClickSyncConfig,
                border = BorderStroke(1.dp, borderColor),
                shape = CircleShape,
                colors = buttonColors
            ) {
                Text(
                    text = stringResource(R.string.sync_config),
                    style = MaterialTheme.typography.button
                )
            }
            Spacer(Modifier.width(8.dp))
            OutlinedButton(
                onClick = onClickMnemonics,
                border = BorderStroke(1.dp, borderColor),
                shape = CircleShape,
                colors = buttonColors
            ) {
                Text(
                    text = stringResource(R.string.mnemonics),
                    style = MaterialTheme.typography.button
                )
            }
        }
    }
}