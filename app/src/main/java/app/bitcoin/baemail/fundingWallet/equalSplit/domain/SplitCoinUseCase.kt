package app.bitcoin.baemail.fundingWallet.equalSplit.domain

import app.bitcoin.baemail.core.domain.repository.BsvStatusRepository
import app.bitcoin.baemail.core.domain.repository.CoinRepository
import app.bitcoin.baemail.core.domain.usecases.CalculateBasicTxSizeUseCase
import app.bitcoin.baemail.core.domain.entities.ReceivedCoin
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.domain.repository.SecureDataRepository
import kotlinx.coroutines.flow.first
import kotlin.jvm.Throws
import kotlin.math.nextUp

class SplitCoinUseCaseImpl(
    private val coinRepository: CoinRepository,
    private val secureDataRepository: SecureDataRepository,
    private val bsvStatusRepository: BsvStatusRepository,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val calculateBasicTxSizeUseCase: CalculateBasicTxSizeUseCase,
) : SplitCoinUseCase {

    override suspend fun invoke(coin: Coin, parts: Int) {
        val liveBsvBlockInfo = bsvStatusRepository.liveBlockInfo.first()
            ?: throw ErrorNotReady()

        val destinationAddressList = coinRepository
            .getUpcomingUnusedAddresses(coin.paymail, parts)
            .first()

        val txSize = calculateBasicTxSizeUseCase(1, parts)

        val txFee = bsvStatusRepository.txFee.first()
        val estimatedFee = (txSize * txFee.satsPerByte).nextUp().toInt()

        val partSize = coin.sats.let { sats ->
            if (sats <= 0) return@let 0
            (sats - estimatedFee) / parts
        }

        val destinationAddressInfoList = destinationAddressList.map { it.address to partSize }

        val fundingWalletSeed = secureDataRepository.getFundingWalletSeed(coin.paymail)

        val fundingPath = secureDataRepository.getPathForFundingMnemonic(coin.paymail)

        val txInfo = nodeRuntimeRepository.createSplitCoinTx(
            fundingWalletSeed,
            mapOf(coin.derivationPath to ReceivedCoin(
                coin.txId,
                coin.sats,
                coin.outputIndex,
                coin.address
            )
            ),
            fundingPath,
            destinationAddressInfoList
        )

        txInfo ?: throw ErrorConstructingTx()

        val success = bsvStatusRepository.sendTx(txInfo.txHex)

        if (!success) {
            throw TxNotAcceptedByMiner()
        }

        //tx has been accepted by the miner;
        // update the coin-info in the local database

        val ownedOutputCoins = txInfo.outputs.mapNotNull { (address, outputIndex, sats) ->
            val matchingDerivedAddress = destinationAddressList.find { it.address == address }
                ?: return@mapNotNull null

            Coin(
                paymail = coin.paymail,
                derivationPath = matchingDerivedAddress.path,
                pathIndex = matchingDerivedAddress.getIndex(),
                address = address,
                txId = txInfo.txHash,
                outputIndex = outputIndex,
                sats = sats,
                txHeight = liveBsvBlockInfo.height + 1
            )
        }

        coinRepository.saveCoins(ownedOutputCoins)
        coinRepository.markCoinsSpent(listOf(coin))
    }

}

interface SplitCoinUseCase {
    @Throws(ErrorNotReady::class, ErrorConstructingTx::class, TxNotAcceptedByMiner::class)
    suspend operator fun invoke(coin: Coin, parts: Int)
}

class ErrorNotReady : Exception()
class ErrorConstructingTx : Exception()
class TxNotAcceptedByMiner : Exception()