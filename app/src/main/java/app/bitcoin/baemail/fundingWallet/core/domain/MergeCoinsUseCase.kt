package app.bitcoin.baemail.fundingWallet.core.domain

import app.bitcoin.baemail.core.domain.repository.BsvStatusRepository
import app.bitcoin.baemail.core.domain.repository.CoinRepository
import app.bitcoin.baemail.core.domain.usecases.CalculateBasicTxSizeUseCase
import app.bitcoin.baemail.core.domain.entities.ReceivedCoin
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.domain.repository.SecureDataRepository
import kotlinx.coroutines.flow.first
import timber.log.Timber
import kotlin.math.max
import kotlin.math.nextUp

class MergeCoinsUseCaseImpl(
    private val coinRepository: CoinRepository,
    private val secureDataRepository: SecureDataRepository,
    private val bsvStatusRepository: BsvStatusRepository,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val calculateBasicTxSizeUseCase: CalculateBasicTxSizeUseCase,
) : MergeCoinsUseCase {

    override suspend fun invoke(list: List<Coin>) {
        val sendingPaymail = list.first().paymail

        val liveBsvBlockInfo = bsvStatusRepository.liveBlockInfo.first()
            ?: throw ErrorNotReady()

        val destinationAddress = coinRepository
            .getUpcomingUnusedAddresses(sendingPaymail, 1)
            .first().first()

        val txSize = calculateBasicTxSizeUseCase(list.size, 1)

        val txFee = bsvStatusRepository.txFee.first()
        val estimatedFee = max(1, (txSize * txFee.satsPerByte).nextUp().toInt())

        val totalSats = list.fold(0L) { acc, i ->
            acc + i.sats
        }

        val partSize = totalSats - estimatedFee

        val fundingWalletSeed = secureDataRepository.getFundingWalletSeed(sendingPaymail)

        val fundingPath = secureDataRepository.getPathForFundingMnemonic(sendingPaymail)

        val spendingCoinsList = list.let {
            val map = HashMap<String, ReceivedCoin>()

            list.forEach { coin ->
                map[coin.derivationPath] = ReceivedCoin(
                    coin.txId,
                    coin.sats,
                    coin.outputIndex,
                    coin.address
                )
            }

            map
        }


        val txInfo = nodeRuntimeRepository.createSplitCoinTx(
            fundingWalletSeed,
            spendingCoinsList,
            fundingPath,
            listOf(destinationAddress.address to partSize)
        )

        txInfo ?: throw ErrorConstructingTx()

        val success = bsvStatusRepository.sendTx(txInfo.txHex)

        if (!success) {
            throw TxNotAcceptedByMiner()
        }

        //tx has been accepted by the miner;
        // update the coin-info in the local database

        val ownedOutputCoins = txInfo.outputs.mapNotNull { (address, outputIndex, sats) ->
            if (destinationAddress.address != address) {
                return@mapNotNull null
            }

            Coin(
                paymail = sendingPaymail,
                derivationPath = destinationAddress.path,
                pathIndex = destinationAddress.getIndex(),
                address = address,
                txId = txInfo.txHash,
                outputIndex = outputIndex,
                sats = sats,
                txHeight = liveBsvBlockInfo.height + 1
            )
        }

        coinRepository.saveCoins(ownedOutputCoins)
        coinRepository.markCoinsSpent(list)

    }

}

interface MergeCoinsUseCase {
    suspend operator fun invoke(list: List<Coin>)
}

class ErrorNotReady : Exception()
class ErrorConstructingTx : Exception()
class TxNotAcceptedByMiner : Exception()