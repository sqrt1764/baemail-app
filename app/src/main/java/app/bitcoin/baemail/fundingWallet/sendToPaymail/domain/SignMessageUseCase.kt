package app.bitcoin.baemail.fundingWallet.sendToPaymail.domain

import app.bitcoin.baemail.core.data.wallet.sending.SignMessageHelper
import app.bitcoin.baemail.core.data.util.AuthenticatedPaymail
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.domain.entities.SigningResponse
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import timber.log.Timber

class SignMessageUseCaseImpl(
    private val signMessageHelper: SignMessageHelper, //todo not an interface
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val secureDataSource: SecureDataSource, //todo not an interface
): SignMessageUseCase {

    override suspend fun invoke(sendingPaymail: String, message: String): SigningResponse? {
        val paymailSeed = try {
            secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(sendingPaymail))
        } catch (e: Exception) {
            Timber.d("failed to retrieve active-paymail wallet-seed")
            return null
        }

        val pkiPath = try {
            secureDataSource.getPathForPaymailMnemonic(AuthenticatedPaymail(sendingPaymail))
        } catch (e: Exception) {
            Timber.d("failed to retrieve active-paymail pki-path")
            return null
        }

        return signMessageHelper.signMessageWithPaymailPki(
            paymailSeed,
            pkiPath,
            message,
            nodeRuntimeRepository
        )
    }

}

interface SignMessageUseCase {
    suspend operator fun invoke(sendingPaymail: String, message: String): SigningResponse?
}