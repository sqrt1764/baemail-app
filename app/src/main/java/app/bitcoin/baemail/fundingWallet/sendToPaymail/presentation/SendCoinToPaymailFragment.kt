package app.bitcoin.baemail.fundingWallet.sendToPaymail.presentation

import android.content.Context
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.view.WindowInsets
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.TextView
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.Dp
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.core.presentation.util.NoSwipeBehavior
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ErrorSnackBar
import app.bitcoin.baemail.fundingWallet.sendToPaymail.presentation.compose.ComposableSendToPaymail
import app.bitcoin.baemail.fundingWallet.sendToPaymail.presentation.compose.FoundDestination
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.max

class SendCoinToPaymailFragment : Fragment() {

    companion object {
        const val KEY_HASH_TX = "tx_hash"
        const val KEY_ADDRESS = "address"
        const val KEY_OUTPUT_INDEX = "output_index"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: SendToPaymailViewModel

    private lateinit var container: FrameLayout
    private lateinit var processingContainer: ConstraintLayout
    private lateinit var buttonContainer: ConstraintLayout
    private lateinit var snackbarContainer: CoordinatorLayout

    private lateinit var compose: ComposeView

    private val keyboardCoveredHeight = MutableStateFlow(0)

    private lateinit var cancel: TextView
    private lateinit var next: TextView

    private var errorSnackBar: ErrorSnackBar? = null

    private val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorNextBackground: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }

    private val colorNextBackgroundDisabled: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimaryVariant)
    }

    private val drawableNextBackground: ColorDrawable by lazy {
        ColorDrawable(colorNextBackgroundDisabled)
    }

    private val drawableNextSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private val availableHeightHelper = AvailableScreenHeightHelper()
    private val keyboardHeightState = MutableStateFlow(0)
    private val appliedInsetsState = MutableStateFlow(AppliedInsets())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val txHash = requireArguments().getString(KEY_HASH_TX)!!
        val address = requireArguments().getString(KEY_ADDRESS)!!
        val outputIndex = requireArguments().getInt(KEY_OUTPUT_INDEX)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            this,
            viewModelFactory
        )[SendToPaymailViewModel::class.java]

        viewModel.setCoin(
            txHash,
            address,
            outputIndex
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_send_coin_to_paymail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        container = requireView().findViewById(R.id.container)
        processingContainer = requireView().findViewById(R.id.processing_container)
        buttonContainer = requireView().findViewById(R.id.button_container)
        snackbarContainer = requireView().findViewById(R.id.snackbar_container)
        compose = requireView().findViewById(R.id.compose)
        cancel = requireView().findViewById(R.id.cancel)
        next = requireView().findViewById(R.id.next)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            availableHeightHelper.setup(
                requireContext(),
                requireActivity().window,
                viewLifecycleOwner,
                { requireView() }
            )

            availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
                height ?: return@Observer

                val maxHeight = container.height - appliedInsetsState.value.top - appliedInsetsState.value.bottom
                val keyboardHeight = max(maxHeight - height, 0)

                keyboardHeightState.value = keyboardHeight
            })

            viewLifecycleOwner.lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    keyboardHeightState.collectLatest { keyboardHeight ->
                        onKeyboardHeightChange(keyboardHeight)
                    }
                }
            }
        }

        container.doOnApplyWindowInsets { v, windowInsets, _ ->
            val appliedInsets = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                AppliedInsets(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                AppliedInsets(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight
                )
            }

            v.updatePadding(
                top = appliedInsets.top,
                bottom = appliedInsets.bottom,
                left = appliedInsets.left,
                right = appliedInsets.right
            )

            appliedInsetsState.value = appliedInsets

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val keyboardHeight = max(
                    windowInsets.getInsets(WindowInsets.Type.ime()).bottom -
                            windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    0
                )

                keyboardHeightState.value = keyboardHeight

                onKeyboardHeightChange(keyboardHeight)
            }

            windowInsets
        }

        cancel.clipToOutline = true
        cancel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        cancel.background = let {
            val colorCancelBackground = ContextCompat.getColor(
                requireContext(),
                R.color.paymail_id_input_fragment__cancel_background
            )

            LayerDrawable(arrayOf(
                ColorDrawable(colorCancelBackground),
                DrawableState.getNew(colorSelectorOnAccent)
            ))
        }

        next.clipToOutline = true
        next.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        next.background = LayerDrawable(arrayOf(
            drawableNextBackground,
            drawableNextSelector
        ))

        cancel.setOnClickListener {
            imm.hideSoftInputFromWindow(requireView().windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            findNavController().popBackStack()
        }

        next.setOnClickListener {
            imm.hideSoftInputFromWindow(requireView().windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            viewModel.onRequestedSend()
        }

        compose.setContent {
            val destination = viewModel.destinationPaymail.collectAsStateWithLifecycle()
            val onDestinationChanged: (String) -> Unit = { value ->
                viewModel.onDestinationPaymailChanged(value)
            }

            val note = viewModel.note.collectAsStateWithLifecycle()
            val onNoteChanged: (String) -> Unit = { value ->
                viewModel.onNoteChanged(value)
            }

            val coinInfo = viewModel.coinInfo.collectAsStateWithLifecycle(CoinInfo(0, 0f))

            val paymailExistence = viewModel.paymailExistence
                .collectAsStateWithLifecycle(PaymailExistence.Checking())

            val foundDestination = when (paymailExistence.value) {
                is PaymailExistence.Checking -> FoundDestination.Checking
                is PaymailExistence.Invalid -> FoundDestination.NotFound
                is PaymailExistence.Valid -> FoundDestination.Success
            }

            val isNoteInputEnabled = paymailExistence.value.let {
                it is PaymailExistence.Valid && it.supportsProfile
            }

            val keyboardCoveredHeight = keyboardCoveredHeight.collectAsStateWithLifecycle()

            ComposeAppTheme {
                ComposableSendToPaymail(
                    coinInfo.value.sats,
                    coinInfo.value.usd,
                    destination.value,
                    onDestinationChanged,
                    foundDestination,
                    isNoteInputEnabled,
                    note.value,
                    onNoteChanged,
                    Dp(keyboardCoveredHeight.value.toFloat())
                )
            }
        }


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.eventFlow.collectLatest { event ->
                    when(event) {
                        is SendToPaymailViewModel.UiEvent.ShowSnackbar -> {
                            processingContainer.visibility = View.GONE

                            ErrorSnackBar.make(
                                requireContext(),
                                snackbarContainer,
                                getString(event.message)
                            ).let {
                                it.behavior = NoSwipeBehavior()
                                it.duration = Snackbar.LENGTH_SHORT
                                it.show()
                            }
                        }
                        is SendToPaymailViewModel.UiEvent.Processing -> {
                            processingContainer.visibility = View.VISIBLE
                        }
                        is SendToPaymailViewModel.UiEvent.SendSuccess -> {
                            findNavController().popBackStack()
                        }
                    }
                }
            }
        }


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.state.collect { state ->
                    when (state) {
                        SendToPaymailViewModel.State.ERROR_COIN_INVALID -> {
                            errorSnackBar?.dismiss()
                            errorSnackBar = ErrorSnackBar.make(
                                requireContext(),
                                snackbarContainer,
                                getString(R.string.coin_is_missing)
                            ).let {
                                it.behavior = NoSwipeBehavior()
                                it.duration = Snackbar.LENGTH_INDEFINITE
                                it.show()
                                it
                            }

                        }
                        SendToPaymailViewModel.State.ERROR_NO_INTERNET -> {
                            errorSnackBar?.dismiss()
                            errorSnackBar = ErrorSnackBar.make(
                                requireContext(),
                                snackbarContainer,
                                getString(R.string.error_no_internet)
                            ).let {
                                it.behavior = NoSwipeBehavior()
                                it.duration = Snackbar.LENGTH_INDEFINITE
                                it.show()
                                it
                            }

                        }
                        else -> {
                            errorSnackBar?.dismiss()
                        }
                    }

                    if (SendToPaymailViewModel.State.VALID == state) {
                        next.isEnabled = true
                        drawableNextBackground.color = colorNextBackground
                    } else {
                        next.isEnabled = false
                        drawableNextBackground.color = colorNextBackgroundDisabled
                    }
                }
            }
        }


    }

    private fun onKeyboardHeightChange(keyboardHeight: Int) {
        keyboardCoveredHeight.value = keyboardHeight
        buttonContainer.translationY = -1f * keyboardHeight
    }

}