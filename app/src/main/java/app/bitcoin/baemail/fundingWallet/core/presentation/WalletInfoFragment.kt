package app.bitcoin.baemail.fundingWallet.core.presentation

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import androidx.compose.ui.platform.ComposeView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import app.bitcoin.baemail.core.presentation.util.NoSwipeBehavior
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.view.ErrorSnackBar
import app.bitcoin.baemail.fundingWallet.core.presentation.compose.ComposeWalletInfo
import app.bitcoin.baemail.paymail.presentation.compose.ComposePaymailAuthorization
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import java.lang.StringBuilder
import javax.inject.Inject

class WalletInfoFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: CoinViewModel

    private val cbm: ClipboardManager by lazy {
        requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_coins)

        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)[CoinViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val content = ComposeView(requireContext()).apply {
            setContent {
                val infoState = viewModel.infoState.collectAsStateWithLifecycle()
                val info = infoState.value

                val copyMnemonic: (Boolean) -> Unit = { paymailNotFunding ->
                    val mnemonic = if (paymailNotFunding) {
                        info.paymailMnemonic
                    } else {
                        info.fundingMnemonic
                    }

                    val typeLabel = if (paymailNotFunding) {
                        getString(R.string.paymail)
                    } else {
                        getString(R.string.funding)
                    }

                    copyMnemonicToClipBoard(
                        typeLabel,
                        mnemonic
                    )
                }

                ComposeAppTheme {
                    ComposeWalletInfo(
                        info.paymail,
                        info.paymailPath,
                        info.paymailMnemonic,
                        info.fundingPath,
                        info.fundingMnemonic,
                        copyMnemonic,
                        ::onBackPressed
                    )
                }
            }
        }

        val coordinatorLayout = CoordinatorLayout(requireContext())
        coordinatorLayout.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        content.layoutParams = CoordinatorLayout.LayoutParams(
            CoordinatorLayout.LayoutParams.MATCH_PARENT,
            CoordinatorLayout.LayoutParams.MATCH_PARENT
        )
        coordinatorLayout.addView(content)

        return coordinatorLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        view.doOnApplyWindowInsets { v, windowInsets, initialPadding ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                v.updatePadding(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                v.updatePadding(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight,
                )
            }

            windowInsets
        }


    }

    private fun copyMnemonicToClipBoard(field: String, mnemonic: List<String>) {
        cbm.let { manager ->
            val mnemonicString = StringBuilder()
            mnemonic.forEachIndexed { i, item ->
                mnemonicString.append(item)
                if (i + 1 != mnemonic.size)
                    mnemonicString.append(" ")
            }

            val clip = ClipData.newPlainText(
                getString(R.string.mnemonic, field),
                mnemonicString.toString()
            )

            manager.setPrimaryClip(clip)
            ErrorSnackBar.make(
                requireContext(),
                requireView() as ViewGroup,
                getString(R.string.x_copied, field)
            ).let {
                it.behavior = NoSwipeBehavior()
                it.duration = Snackbar.LENGTH_SHORT
                it.show()
            }
        }
    }

    private fun onBackPressed() {
        findNavController().popBackStack()
    }
}