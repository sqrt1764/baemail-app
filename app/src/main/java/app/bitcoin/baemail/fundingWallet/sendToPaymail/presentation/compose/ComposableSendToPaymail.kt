package app.bitcoin.baemail.fundingWallet.sendToPaymail.presentation.compose

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import app.bitcoin.baemail.core.presentation.util.formatSatsLabel
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import java.util.Locale

@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode of SendToPaymail"
)
@Composable
fun PreviewComposableSendToPaymail() {

    ComposeAppTheme {
        ComposableSendToPaymail(
            12345,
            1.54321f,
            "",
            {},
            FoundDestination.Profile(""),
            false,
            "",
            {},
            0.dp
        )
    }
}



@Composable
fun ComposableSendToPaymail(
    coinSats: Long,
    coinUsd: Float,
    destinationValue: String,
    onDestinationChanged: (String) -> Unit,
    destinationFound: FoundDestination,
    isNoteInputEnabled: Boolean,
    noteValue: String,
    onNoteValueChanged: (String) -> Unit,
    bottomGap: Dp,
    modifier: Modifier = Modifier,
) {
    val scrollState = rememberScrollState()

    Box(modifier = modifier.verticalScroll(scrollState)) {
        Column(
            modifier = Modifier
                .padding(PaddingValues(16.dp, 16.dp, 16.dp, 16.dp))
                .fillMaxWidth()
        ) {
            Text(
                stringResource(id = R.string.send_coin_to_paymail),
                modifier = Modifier.padding(start = 12.dp),
                style = MaterialTheme.typography.h1
            )
            Spacer(
                modifier = Modifier.size(24.dp)
            )
            CoinInfo(
                coinSats,
                coinUsd,
                Modifier.padding(start = 8.dp)
            )
            Spacer(
                modifier = Modifier.size(24.dp)
            )
            DestinationInput(
                destinationValue,
                onDestinationChanged,
                destinationFound
            )
            Spacer(
                modifier = Modifier.size(16.dp)
            )
            NoteInput(
                isNoteInputEnabled,
                noteValue,
                onNoteValueChanged
            )
            Spacer(
                modifier = Modifier.size(10.dp, 10.dp.plus(bottomGap))
            )
        }
    }
}

@Composable
fun CoinInfo(
    sats: Long,
    usd: Float,
    modifier: Modifier = Modifier
) {
    val usdString = String.format(Locale.US, "%.3f", usd)
    val coinValueStyle = MaterialTheme.typography.body1.copy(
        fontSize = MaterialTheme.typography.body1.fontSize * 1.1,
        fontWeight = FontWeight.Bold
    )

    Surface(
        modifier = modifier,
        shape = CircleShape,
        color = MaterialTheme.colors.secondary.copy(alpha = 0.25f),
        border = BorderStroke(
            2.dp,
            MaterialTheme.colors.secondary.copy(alpha = 0.4f)
        )
    ) {
        Text(
            stringResource(R.string.coin_info, formatSatsLabel(sats.toString()), usdString),
            modifier = Modifier.padding(
                start = 16.dp,
                top = 12.dp,
                end = 16.dp,
                bottom = 12.dp
            ),
            style = coinValueStyle
        )
    }
}



sealed class FoundDestination {
    object Checking : FoundDestination()
    object NotFound : FoundDestination()
    object Success : FoundDestination()
    class Profile(url: String) : FoundDestination()
}



@Composable
fun DestinationInput(
    value: String,
    onValueChanged: (String) -> Unit,
    foundState: FoundDestination,
    modifier: Modifier = Modifier
) {
    val focusRequester = remember { FocusRequester() }

    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }

    Surface(
        shape = MaterialTheme.shapes.medium,
        color = MaterialTheme.colors.onSurface.copy(alpha = 0.1f),
        modifier = modifier
    ) {
        Text(
            stringResource(id = R.string.destination),
            style = MaterialTheme.typography.caption,
            modifier = Modifier.padding(start = 16.dp, top = 8.dp)
        )
        TextField(
            value = value,
            onValueChange = onValueChanged,
            modifier = Modifier
//                .semantics { contentDescription = label }
                .fillMaxWidth()
                .padding(top = Dp(MaterialTheme.typography.caption.fontSize.value).plus(8.dp))
                .focusRequester(focusRequester),
            singleLine = true,
            keyboardOptions = KeyboardOptions.Default.copy(
                autoCorrect = false,
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Done
            ),
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Color.Transparent,
                unfocusedBorderColor = Color.Transparent,
            ),
            placeholder = {
                Text(
                    stringResource(id = R.string.paymail),
                    style = MaterialTheme.typography.subtitle1
                )
            },
            trailingIcon = {
                val icon = when (foundState) {
                    is FoundDestination.Profile -> painterResource(id = R.drawable.ic_npc3000)
                    is FoundDestination.Checking -> painterResource(id = R.drawable.baseline_cancel_24)
                    is FoundDestination.Success -> painterResource(id = R.drawable.baseline_check_circle_24)
                    is FoundDestination.NotFound -> painterResource(id = R.drawable.baseline_cancel_24)
                }

                val iconTint = when (foundState) {
                    is FoundDestination.Profile -> Color.Unspecified
                    is FoundDestination.Checking -> MaterialTheme.colors.secondary
                    is FoundDestination.Success -> Color(52,145,2)
                    is FoundDestination.NotFound -> Color(
                        LocalContext.current.getColorFromAttr(R.attr.colorError)
                    )
                }

                val description = when (foundState) {
                    is FoundDestination.Profile -> stringResource(id = R.string.profile)
                    is FoundDestination.Checking -> stringResource(id = R.string.checking)
                    is FoundDestination.Success -> stringResource(id = R.string.success)
                    is FoundDestination.NotFound -> stringResource(id = R.string.not_found)
                }

                Icon(
                    icon,
                    contentDescription = description,
                    tint = iconTint,
                    modifier = Modifier
                        .padding(end = 8.dp)
                        .clip(CircleShape)
                )
            }
        )
    }

}


@Composable
fun NoteInput(
    isEnabled: Boolean,
    value: String,
    onValueChanged: (String) -> Unit,
    modifier: Modifier = Modifier
) {

    Surface(
        shape = MaterialTheme.shapes.medium,
        color = MaterialTheme.colors.onSurface.copy(alpha = 0.1f),
        modifier = modifier.alpha(if (isEnabled) 1f else 0.2f)
    ) {
        Text(
            stringResource(id = R.string.note),
            style = MaterialTheme.typography.caption,
            modifier = Modifier.padding(start = 16.dp, top = 8.dp)
        )
        TextField(
            value = value,
            onValueChange = onValueChanged,
            enabled = isEnabled,
            modifier = Modifier
//                .semantics { contentDescription = label }
                .fillMaxWidth()
                .padding(top = Dp(MaterialTheme.typography.caption.fontSize.value).plus(8.dp)),
            singleLine = true,
            keyboardOptions = KeyboardOptions.Default.copy(
                autoCorrect = false,
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Done
            ),
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Color.Transparent,
                unfocusedBorderColor = Color.Transparent,
                disabledBorderColor = Color.Transparent
            ),
            placeholder = {
                Text(
                    stringResource(id = R.string.input),
                    style = MaterialTheme.typography.subtitle1
                )
            }
        )
    }

}