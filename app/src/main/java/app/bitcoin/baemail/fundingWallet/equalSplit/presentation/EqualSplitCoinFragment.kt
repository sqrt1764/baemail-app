package app.bitcoin.baemail.fundingWallet.equalSplit.presentation

import android.annotation.SuppressLint
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.util.NoSwipeBehavior
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ErrorSnackBar
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*
import javax.inject.Inject


class EqualSplitCoinFragment : Fragment() {

    companion object {
        const val KEY_HASH_TX = "tx_hash"
        const val KEY_ADDRESS = "address"
        const val KEY_OUTPUT_INDEX = "output_index"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: EqualSplitCoinViewModel


    private lateinit var container: FrameLayout
    private lateinit var layoutContainer: ConstraintLayout
    private lateinit var processingContainer: ConstraintLayout
    private lateinit var snackbarContainer: CoordinatorLayout

    private lateinit var parts: TextView
    private lateinit var partValue: TextView
    private lateinit var errorLabel: TextView
    private lateinit var cancel: TextView
    private lateinit var next: TextView
    private lateinit var value: TextView

    private lateinit var slider: Slider

    private val positionListTouchListener = PositionSliderTouchListener()

    private val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorNextBackground: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }

    private val colorNextBackgroundDisabled: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimaryVariant)
    }

    private val drawableNextBackground: ColorDrawable by lazy {
        ColorDrawable(colorNextBackgroundDisabled)
    }

    private val drawableNextSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }

    private var appliedTotalValue = -1L
    private var appliedPartsValue = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val txHash = requireArguments().getString(KEY_HASH_TX)!!
        val address = requireArguments().getString(KEY_ADDRESS)!!
        val outputIndex = requireArguments().getInt(KEY_OUTPUT_INDEX)


        viewModel = ViewModelProvider(this, viewModelFactory)[EqualSplitCoinViewModel::class.java]

        viewModel.setCoin(
            address,
            txHash,
            outputIndex
        )

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_equal_split_coin, container, false)
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        container = requireView().findViewById(R.id.container)
        layoutContainer = requireView().findViewById(R.id.layout_container)
        processingContainer = requireView().findViewById(R.id.processing_container)
        snackbarContainer = requireView().findViewById(R.id.snackbar_container)
        cancel = requireView().findViewById(R.id.cancel)
        next = requireView().findViewById(R.id.next)
        parts = requireView().findViewById(R.id.parts)
        partValue = requireView().findViewById(R.id.part_value)
        errorLabel = requireView().findViewById(R.id.error_label)
        value = requireView().findViewById(R.id.coin_value)
        slider = requireView().findViewById(R.id.slider)


        container.doOnApplyWindowInsets { v, windowInsets, _ ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                v.updatePadding(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                v.updatePadding(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight,
                )
            }

            windowInsets
        }


        cancel.clipToOutline = true
        cancel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }
        }


        val colorCancelBackground = R.color.paymail_id_input_fragment__cancel_background.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        cancel.background = LayerDrawable(arrayOf(
            ColorDrawable(colorCancelBackground),
            DrawableState.getNew(colorSelectorOnAccent)
        ))


        next.clipToOutline = true
        next.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        next.background = LayerDrawable(arrayOf(
            drawableNextBackground,
            drawableNextSelector
        ))


        slider.isSaveEnabled = false
        slider.setOnTouchListener(positionListTouchListener)
        slider.addOnChangeListener { slider, value, fromUser ->
            if (!fromUser) return@addOnChangeListener

            refreshPartCountDependentLabels(value.toInt())
        }
        slider.labelBehavior = LabelFormatter.LABEL_GONE



        slider.valueTo = 20f
        slider.valueFrom = 2f
        slider.value = 2f
        slider.stepSize = 1f



        cancel.setOnClickListener {
            findNavController().popBackStack()
        }

        next.setOnClickListener {
            viewModel.onRequestedSplit()
        }


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.eventFlow.collectLatest { event ->
                    when(event) {
                        is EqualSplitCoinViewModel.UiEvent.ShowSnackbar -> {
                            processingContainer.visibility = View.GONE
                            ErrorSnackBar.make(
                                requireContext(),
                                snackbarContainer,
                                getString(event.message)
                            ).let {
                                it.behavior = NoSwipeBehavior()
                                it.duration = Snackbar.LENGTH_SHORT
                                it.show()
                            }
                        }
                        is EqualSplitCoinViewModel.UiEvent.Processing -> {
                            processingContainer.visibility = View.VISIBLE
                        }
                        is EqualSplitCoinViewModel.UiEvent.SendSuccess -> {
                            findNavController().popBackStack()
                        }
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {

                viewModel.state.collect { state ->
                    Timber.d("viewModel.state.collect: $state")

                    value.text = state.satsInCoin.toString()

                    slider.apply {
                        if (appliedPartsValue == state.parts && appliedTotalValue == state.satsInCoin)
                            return@apply

                        appliedTotalValue = state.satsInCoin
                        appliedPartsValue = state.parts

                        parts.text = "$appliedPartsValue"

                        slider.value = appliedPartsValue.toFloat()
                        Timber.d("equalSplitContentLD observed: satsOfPart ${state.partSize}")
                        if (state.partSize > 0) {

                            partValue.text = buildPartSizeLabel(state.partSize, state.satsInUsdCent)
                        }


                        value = appliedPartsValue.toFloat()
                        invalidate()
                    }

                    when (state.status) {
                        EqualSplitCoinViewModel.EqualSplitState.VALID -> {
                            errorLabel.visibility = View.INVISIBLE
                        }
                        EqualSplitCoinViewModel.EqualSplitState.ERROR_COIN_INVALID -> {
                            errorLabel.visibility = View.VISIBLE
                            errorLabel.text = getString(R.string.coin_is_missing)
                        }
                        EqualSplitCoinViewModel.EqualSplitState.ERROR_PARTS_ARE_TOO_SMALL -> {
                            errorLabel.visibility = View.VISIBLE
                            errorLabel.text = getString(R.string.split_part_size_too_small)
                        }
                        EqualSplitCoinViewModel.EqualSplitState.ERROR_NO_INTERNET -> {
                            errorLabel.visibility = View.VISIBLE
                            errorLabel.text = getString(R.string.error_no_internet)
                        }
                    }

                    if (EqualSplitCoinViewModel.EqualSplitState.VALID == state.status) {
                        next.isEnabled = true
                        drawableNextBackground.color = colorNextBackground
                    } else {
                        next.isEnabled = false
                        drawableNextBackground.color = colorNextBackgroundDisabled
                    }

                }
            }
        }
    }

    private fun buildPartSizeLabel(satsOfPart: Long, satsInCent: Int): String {
        if (satsInCent == 0) {
            return "$satsOfPart sats"
        } else {
            val usdValue = String.format(Locale.US, "%.2f", (satsOfPart / (satsInCent.toFloat() * 100)))

            return "$satsOfPart sats; $usdValue USD"
        }
    }

    private fun onValueChanged(value: Float) {
        val intValue = value.toInt()
        viewModel.onPartCountChanged(intValue)
    }

    private fun refreshPartCountDependentLabels(parts: Int) {
        val state = viewModel.state.value
        if (state.satsInCoin < 0) return

        partValue.text = buildPartSizeLabel(state.partSize, state.satsInUsdCent)

        this.parts.text = "$parts"
    }


    inner class PositionSliderTouchListener : View.OnTouchListener {
//        private val scope = CoroutineScope(Dispatchers.Main)

        @SuppressLint("ClickableViewAccessibility")
        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            event ?: return false
            v ?: return false

            when (event.actionMasked) {
                MotionEvent.ACTION_UP -> {
                    onActionUp(v)
                }
            }

            return false
        }

        private fun onActionUp(view: View) {
            view as Slider

            viewLifecycleOwner.lifecycleScope.launch {
                delay(50)

                onValueChanged(view.value)

            }
        }

    }

}