package app.bitcoin.baemail.fundingWallet

import android.util.Base64
import androidx.lifecycle.*
import app.bitcoin.baemail.core.data.wallet.UnusedCoinAddressHelper
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.presentation.view.recycler.ATUnusedAddress
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class TopUpViewModel @Inject constructor(
    private val unusedCoinAddressHelper: UnusedCoinAddressHelper,
    val authRepository: AuthRepository
) : ViewModel() {

    val activePaymailLD = authRepository.activePaymail
        .asLiveData(viewModelScope.coroutineContext, 5000)

    val topUpFromMoneyButtonLD = MediatorLiveData<String>()

    private val _contentLD = MutableLiveData<List<AdapterType>>()
    val contentLD: LiveData<List<AdapterType>>
        get() = _contentLD

    init {

        topUpFromMoneyButtonLD.addSource(activePaymailLD) {
            refreshTopUpFromMoneyButton()
        }

    }

    private fun refreshTopUpFromMoneyButton() {
        val activePaymail = activePaymailLD.value?.paymail ?: return

        viewModelScope.launch(Dispatchers.Default) {
            val nextAddresses = unusedCoinAddressHelper
                .getUpcomingUnusedAddresses(activePaymail).first()
            val address = nextAddresses[0]

            val query = """
                {
                  "to": "${address.address}",
                  "editable": true,
                  "currency": "USD"
                }
            """.trimIndent()

            val encodedQuery = Base64.encodeToString(query.toByteArray(), Base64.DEFAULT)
            val buttonUrl = "https://button.bitdb.network/#$encodedQuery"

            Timber.d("#refreshTopUpFromMoneyButton q:$query url:$buttonUrl")

            topUpFromMoneyButtonLD.postValue(buttonUrl)





            val unusedAddresses = unusedCoinAddressHelper
                .getUpcomingUnusedAddresses(activePaymail).first()


            unusedAddresses.mapIndexed { i, address ->
                ATUnusedAddress(address, i + 1 == unusedAddresses.size)
            }.mapIndexedNotNull { i, address ->
                if (i < 5) address else null
            }.let {
                _contentLD.postValue(it)
            }


        }
    }


}