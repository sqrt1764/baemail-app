package app.bitcoin.baemail.fundingWallet.equalSplit.presentation

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.data.wallet.ExchangeRateHelper
import app.bitcoin.baemail.core.domain.entities.CoinRef
import app.bitcoin.baemail.core.domain.usecases.CalculateBasicTxSizeUseCase
import app.bitcoin.baemail.core.domain.usecases.GetExchangeRateUseCase
import app.bitcoin.baemail.core.domain.usecases.CoinSyncStatusUseCase
import app.bitcoin.baemail.fundingWallet.equalSplit.domain.ErrorConstructingTx
import app.bitcoin.baemail.fundingWallet.equalSplit.domain.ErrorNotReady
import app.bitcoin.baemail.core.domain.usecases.GetCoinUseCase
import app.bitcoin.baemail.fundingWallet.equalSplit.domain.SplitCoinUseCase
import app.bitcoin.baemail.fundingWallet.equalSplit.domain.TxNotAcceptedByMiner
import app.bitcoin.baemail.core.data.room.entity.Coin
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

class EqualSplitCoinViewModel @Inject constructor(
    private val getCoinUseCase: GetCoinUseCase,
    coinSyncStatusUseCase: CoinSyncStatusUseCase,
    getExchangeRateUseCase: GetExchangeRateUseCase,
    private val calculateBasicTxSizeUseCase: CalculateBasicTxSizeUseCase,
    private val splitCoinUseCase: SplitCoinUseCase
) : ViewModel() {

    companion object {
        const val MIN_PART_COUNT = 2
    }

    private val coinRef = MutableStateFlow<CoinRef?>(null)
    private val parts = MutableStateFlow(MIN_PART_COUNT)

    private val coin: StateFlow<Coin?> = coinRef.flatMapLatest { ref ->
        ref ?: return@flatMapLatest flow { emit(null) }
        getCoinUseCase(ref)
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = null
    )

    val state: StateFlow<EqualSplitModel> =
        coin.combine(coinSyncStatusUseCase()) { coin, syncStatus ->
            coin to syncStatus

        }.combine(parts) { (coin, syncStatus), parts ->
            Triple(coin, syncStatus, parts)

        }.combine(getExchangeRateUseCase.exchangeRate) { (coin, syncStatus, parts), exchangeRate ->
            val satsInUsdCent = if (ExchangeRateHelper.INVALID_STATE != exchangeRate) {
                exchangeRate.satsInACent
            } else 0

            val partSize = (coin?.sats ?: 0).let { sats ->
                if (sats <= 0) return@let 0

                val txSize = calculateBasicTxSizeUseCase(1, parts)
                (sats - txSize) / parts
            }

            if (coin == null) {
                EqualSplitModel(
                    EqualSplitState.ERROR_COIN_INVALID,
                    0,
                    parts,
                    partSize,
                    satsInUsdCent
                )
            } else if (ChainSyncStatus.DISCONNECTED == syncStatus) {
                EqualSplitModel(
                    EqualSplitState.ERROR_NO_INTERNET,
                    coin.sats,
                    parts,
                    partSize,
                    satsInUsdCent
                )
            } else {
                val state =
                    if (partSize < satsInUsdCent / 4) {
                        EqualSplitState.ERROR_PARTS_ARE_TOO_SMALL
                    } else {
                        EqualSplitState.VALID
                    }

                EqualSplitModel(
                    state,
                    coin.sats,
                    parts,
                    partSize,
                    satsInUsdCent
                )
            }
        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = EqualSplitModel(
                EqualSplitState.ERROR_COIN_INVALID,
                0,
                MIN_PART_COUNT,
                0,
                0
            )
        )

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun setCoin(
        address: String,
        txId: String,
        outputIndex: Int
    ) {
        coinRef.value = CoinRef(address, txId, outputIndex)
    }

    fun onPartCountChanged(count: Int) {
        if (count < MIN_PART_COUNT) throw RuntimeException()
        parts.value = count
    }

    fun onRequestedSplit() {
        viewModelScope.launch {
            _eventFlow.emit(UiEvent.Processing)

            try {
                val coin = coin.value ?: throw RuntimeException()
                splitCoinUseCase(coin, parts.value)
                _eventFlow.emit(UiEvent.SendSuccess)

            } catch (e: ErrorNotReady) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_no_internet))

            } catch (e: ErrorConstructingTx) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_failed_constructing_tx))

            } catch (e: TxNotAcceptedByMiner) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_tx_not_accepted))

            } catch (e: Exception) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error))
            }
        }
    }

    sealed class UiEvent {
        object Processing: UiEvent()
        object SendSuccess: UiEvent()
        data class ShowSnackbar(@StringRes val message: Int): UiEvent()
    }

    enum class EqualSplitState {
        VALID,
        ERROR_COIN_INVALID,
        ERROR_PARTS_ARE_TOO_SMALL,
        ERROR_NO_INTERNET,
    }

    data class EqualSplitModel(
        val status: EqualSplitState,
        val satsInCoin: Long,
        val parts: Int,
        val partSize: Long,
        val satsInUsdCent: Int
    )

}


