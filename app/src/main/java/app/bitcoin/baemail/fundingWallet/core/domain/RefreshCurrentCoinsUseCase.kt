package app.bitcoin.baemail.fundingWallet.core.domain

import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.data.wallet.DynamicChainSync
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.domain.repository.CoinRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class RefreshCurrentCoinsUseCaseImpl(
    private val authRepository: AuthRepository,
    private val coinRepository: CoinRepository,
    private val coroutineScope: CoroutineScope,
    private val dynamicChainSync: DynamicChainSync
) : RefreshCurrentCoinsUseCase {
    override fun invoke(): Boolean {
        if (ChainSyncStatus.LIVE != dynamicChainSync.status.value) {
            //do not do it at this time
            return false
        }
        val paymail = authRepository.activePaymail.value?.paymail
        if (paymail == null || paymail != dynamicChainSync.paymail) {
            //do not do it at this time
            return false
        }

        coroutineScope.launch {
            coinRepository.refreshCoins()
        }
        return true
    }

}

interface RefreshCurrentCoinsUseCase {
    operator fun invoke(): Boolean
}