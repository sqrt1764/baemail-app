package app.bitcoin.baemail.fundingWallet.core.presentation

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.fundingWallet.equalSplit.presentation.EqualSplitCoinFragment
import app.bitcoin.baemail.fundingWallet.sendToAddress.presentation.SendCoinToAddressFragment
import app.bitcoin.baemail.fundingWallet.sendToPaymail.presentation.SendCoinToPaymailFragment
import app.bitcoin.baemail.core.presentation.util.RoundedBottomSheetDialogFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class CoinOptionsFragment : RoundedBottomSheetDialogFragment() {

    companion object {
        const val KEY_HASH_TX = "tx_hash"
        const val KEY_ADDRESS = "address"
        const val KEY_OUTPUT_INDEX = "output_index"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: CoinViewModel

    private lateinit var sendToAddress: TextView
    private lateinit var sendToPaymail: TextView
    private lateinit var equalSplit: TextView
    private lateinit var coinSpentLabel: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_coins)

        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)
            .get(CoinViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_coin_options, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        sendToAddress = requireView().findViewById(R.id.send_to_address)
        sendToPaymail = requireView().findViewById(R.id.send_to_paymail)
        equalSplit = requireView().findViewById(R.id.equal_split)
        coinSpentLabel = requireView().findViewById(R.id.coin_spent_label)


        val txHash = requireArguments().getString(KEY_HASH_TX)
        val address = requireArguments().getString(KEY_ADDRESS)
        val outputIndex = requireArguments().getInt(KEY_OUTPUT_INDEX)


        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        sendToAddress.background = DrawableState.getNew(colorSelector)
        sendToPaymail.background = DrawableState.getNew(colorSelector)
        equalSplit.background = DrawableState.getNew(colorSelector)

        sendToAddress.setOnClickListener {
            val args = Bundle()
            args.putString(SendCoinToAddressFragment.KEY_HASH_TX, txHash)
            args.putString(SendCoinToAddressFragment.KEY_ADDRESS, address)
            args.putInt(SendCoinToAddressFragment.KEY_OUTPUT_INDEX, outputIndex)

            findNavController().navigate(R.id.action_coinOptionsFragment_to_sendCoinToAddressFragment, args)
        }

        sendToPaymail.setOnClickListener {
            val args = Bundle()
            args.putString(SendCoinToPaymailFragment.KEY_HASH_TX, txHash)
            args.putString(SendCoinToPaymailFragment.KEY_ADDRESS, address)
            args.putInt(SendCoinToPaymailFragment.KEY_OUTPUT_INDEX, outputIndex)

            findNavController().navigate(R.id.action_coinOptionsFragment_to_sendCoinToPaymailFragment, args)
        }

        equalSplit.setOnClickListener {
            val args = Bundle()
            args.putString(EqualSplitCoinFragment.KEY_HASH_TX, txHash)
            args.putString(EqualSplitCoinFragment.KEY_ADDRESS, address)
            args.putInt(EqualSplitCoinFragment.KEY_OUTPUT_INDEX, outputIndex)

            findNavController().navigate(R.id.action_coinOptionsFragment_to_equalSplitCoinFragment, args)
        }


        coinSpentLabel.visibility = View.GONE
        equalSplit.visibility = View.VISIBLE
        sendToAddress.visibility = View.VISIBLE
        sendToPaymail.visibility = View.VISIBLE

    }

    override fun getPeekHeight(): Int {
        return CONST_UNSET
    }

    override fun shouldFitToContents(): Boolean {
        return true
    }

    override fun getStartingDialogState(): Int {
        return if (Configuration.ORIENTATION_PORTRAIT == resources.configuration.orientation) {
            super.getStartingDialogState()
        } else BottomSheetBehavior.STATE_EXPANDED
    }
}