package app.bitcoin.baemail.fundingWallet.core.presentation.compose

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import app.bitcoin.baemail.R
import java.lang.StringBuilder


@Composable
fun ComposeWalletInfo(
    paymail: String,
    paymailPath: String,
    paymailMnemonic: List<String>,
    fundingPath: String,
    fundingMnemonic: List<String>,
    copyMnemonic: (Boolean)->Unit,
    onClickBack: ()->Unit
) {
    val scrollState = rememberScrollState()

    val paymailMnemonicShowingState = remember {
        mutableStateOf(false)
    }

    val fundingMnemonicShowingState = remember {
        mutableStateOf(false)
    }

    val sectionTitleSize = MaterialTheme.typography.body1.fontSize.times(1.5f)

    val backgroundColor = MaterialTheme.colors.onBackground.copy(alpha = 0.15f)
    val borderColor = MaterialTheme.colors.onBackground.copy(alpha = 0.3f)

    val buttonColors = ButtonDefaults.outlinedButtonColors(
        backgroundColor = backgroundColor
    )

    val paymailMnemonicString = StringBuilder()
    paymailMnemonic.forEachIndexed { i, item ->
        if (paymailMnemonicShowingState.value) {
            paymailMnemonicString.append(item)
        } else {
            paymailMnemonicString.append(
                String(CharArray(item.length) { "●"[0] })
            )
        }
        if (i + 1 != paymailMnemonic.size)
            paymailMnemonicString.append("  ")
    }

    val fundingMnemonicString = StringBuilder()
    fundingMnemonic.forEachIndexed { i, item ->
        if (fundingMnemonicShowingState.value) {
            fundingMnemonicString.append(item)
        } else {
            fundingMnemonicString.append(
                String(CharArray(item.length) { "●"[0] })
            )
        }
        if (i + 1 != fundingMnemonic.size)
            fundingMnemonicString.append("  ")
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp)
            .verticalScroll(scrollState)
    ) {
        Spacer(Modifier.height(24.dp))
        Row {
            IconButton(
                onClick = onClickBack,
                modifier = Modifier.size(60.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.round_arrow_back_ios_24),
                    contentDescription = stringResource(R.string.back)
                )
            }
            Spacer(Modifier.width(8.dp))
            Text(
                stringResource(R.string.wallet_info),
                style = MaterialTheme.typography.h1
            )
        }
        Spacer(Modifier.height(24.dp))
        Box(modifier = Modifier
            .fillMaxWidth()
            .background(backgroundColor, MaterialTheme.shapes.large)
        ) {
            Column(modifier = Modifier.padding(horizontal = 12.dp, vertical = 8.dp)) {
                Spacer(Modifier.height(4.dp))
                Text(
                    paymail,
                    style = MaterialTheme.typography.body1,
                    fontSize = sectionTitleSize
                )
                Spacer(Modifier.height(6.dp))
                Text(
                    paymailPath,
                    style = MaterialTheme.typography.caption,
                    fontFamily = FontFamily.Monospace
                )
                Spacer(Modifier.height(8.dp))
                Text(
                    paymailMnemonicString.toString(),
                    style = MaterialTheme.typography.body1,
                    fontFamily = FontFamily.Monospace,
                    lineHeight = MaterialTheme.typography.body1.fontSize * 1.3
                )
                Row {
                    OutlinedButton(
                        onClick = {
                            paymailMnemonicShowingState.value = !paymailMnemonicShowingState.value
                        },
                        border = BorderStroke(2.dp, borderColor),
                        shape = RoundedCornerShape(18.dp),
                        colors = buttonColors
                    ) {
                        val painter = if (paymailMnemonicShowingState.value) {
                            painterResource(R.drawable.vector_ic_password_visible)
                        } else {
                            painterResource(R.drawable.vector_ic_password_masked)
                        }
                        Icon(
                            painter,
                            "Show password",
                            tint = MaterialTheme.colors.onBackground
                        )
                    }
                    Spacer(Modifier.width(5.dp))
                    OutlinedButton(
                        onClick = { copyMnemonic(true) },
                        border = BorderStroke(2.dp, borderColor),
                        shape = RoundedCornerShape(18.dp),
                        colors = buttonColors
                    ) {
                        Icon(
                            painterResource(R.drawable.ic_copy_link),
                            "Copy",
                            tint = MaterialTheme.colors.onBackground
                        )
                    }
                }
                Spacer(Modifier.height(5.dp))
            }
        }
        Spacer(Modifier.height(24.dp))
        Box(modifier = Modifier
            .fillMaxWidth()
            .background(backgroundColor, MaterialTheme.shapes.large)
        ) {
            Column(modifier = Modifier.padding(horizontal = 12.dp, vertical = 8.dp)) {
                Spacer(Modifier.height(4.dp))
                Text(
                    stringResource(R.string.funding_wallet),
                    style = MaterialTheme.typography.body1,
                    fontSize = sectionTitleSize
                )
                Spacer(Modifier.height(6.dp))
                Text(
                    fundingPath,
                    style = MaterialTheme.typography.caption,
                    fontFamily = FontFamily.Monospace
                )
                Spacer(Modifier.height(8.dp))
                Text(
                    fundingMnemonicString.toString(),
                    style = MaterialTheme.typography.body1,
                    fontFamily = FontFamily.Monospace,
                    lineHeight = MaterialTheme.typography.body1.fontSize * 1.3
                )
                Row {
                    OutlinedButton(
                        onClick = {
                            fundingMnemonicShowingState.value = !fundingMnemonicShowingState.value
                        },
                        border = BorderStroke(2.dp, borderColor),
                        shape = RoundedCornerShape(18.dp),
                        colors = buttonColors
                    ) {
                        val painter = if (fundingMnemonicShowingState.value) {
                            painterResource(R.drawable.vector_ic_password_visible)
                        } else {
                            painterResource(R.drawable.vector_ic_password_masked)
                        }
                        Icon(
                            painter,
                            "Show password",
                            tint = MaterialTheme.colors.onBackground
                        )
                    }
                    Spacer(Modifier.width(5.dp))
                    OutlinedButton(
                        onClick = { copyMnemonic(false) },
                        border = BorderStroke(2.dp, borderColor),
                        shape = RoundedCornerShape(18.dp),
                        colors = buttonColors
                    ) {
                        Icon(
                            painterResource(R.drawable.ic_copy_link),
                            "Copy",
                            tint = MaterialTheme.colors.onBackground
                        )
                    }
                }
                Spacer(Modifier.height(5.dp))
            }
        }
        Spacer(Modifier.height(90.dp))
    }
}