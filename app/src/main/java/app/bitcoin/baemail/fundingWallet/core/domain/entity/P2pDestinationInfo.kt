package app.bitcoin.baemail.fundingWallet.core.domain.entity

import com.google.gson.annotations.SerializedName

data class P2pDestinationInfo(
    @SerializedName("outputs") val outputs: List<P2pDestinationOutput>,
    @SerializedName("reference") val reference: String
)

data class P2pDestinationOutput(
    @SerializedName("script") val script: String,
    @SerializedName("satoshis") val satoshis: Long
)