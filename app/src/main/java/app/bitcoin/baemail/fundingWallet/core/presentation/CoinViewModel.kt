package app.bitcoin.baemail.fundingWallet.core.presentation

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.core.domain.usecases.GetFundingCoinsUseCase
import app.bitcoin.baemail.fundingWallet.core.domain.ErrorConstructingTx
import app.bitcoin.baemail.fundingWallet.core.domain.ErrorNotReady
import app.bitcoin.baemail.fundingWallet.core.domain.MergeCoinsUseCase
import app.bitcoin.baemail.fundingWallet.core.domain.ResyncCoinsUseCase
import app.bitcoin.baemail.fundingWallet.core.domain.TxNotAcceptedByMiner
import app.bitcoin.baemail.core.presentation.view.recycler.ATCoin
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.domain.entities.InfoModel
import app.bitcoin.baemail.core.domain.usecases.CoinSyncStatusUseCase
import app.bitcoin.baemail.core.domain.usecases.GetHighestUsedAddressIndexUseCase
import app.bitcoin.baemail.core.domain.usecases.GetSecretsUseCase
import app.bitcoin.baemail.core.presentation.util.formatSatsLabel
import app.bitcoin.baemail.fundingWallet.core.domain.RefreshCurrentCoinsUseCase
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class CoinViewModel @Inject constructor(
    private val getActivePaymailUseCase: GetActivePaymailUseCase,
    private val resyncCoinsUseCase: ResyncCoinsUseCase,
    private val refreshCoinsUseCase: RefreshCurrentCoinsUseCase,
    private val mergeCoinsUseCase: MergeCoinsUseCase,
    private val getHighestUsedAddressIndexUseCase: GetHighestUsedAddressIndexUseCase,
    private val getSecretsUseCase: GetSecretsUseCase,
    getFundingCoinsUseCase: GetFundingCoinsUseCase,
    coinSyncStatusUseCase: CoinSyncStatusUseCase
) : ViewModel() {

    private val selectedCoins = MutableStateFlow<List<Coin>>(listOf())

    val state: StateFlow<Model> =
        getHighestUsedAddressIndexUseCase().combine(getFundingCoinsUseCase()) {
                highestUsedAddressIndex, fundingCoins ->
            highestUsedAddressIndex to fundingCoins
        }.combine(coinSyncStatusUseCase()) { (highestUsedAddressIndex, fundingCoins), syncStatus ->
            Triple(highestUsedAddressIndex, fundingCoins, syncStatus)
        }.combine(selectedCoins) { (highestUsedAddressIndex, fundingCoins, syncStatus), selected ->
            var coinsSelected = 0
            val contentList = fundingCoins.map { fundingCoin ->
                val coinSelected = selected.find {
                    it.txId == fundingCoin.txId && it.outputIndex == fundingCoin.outputIndex &&
                            it.address == fundingCoin.address
                }
                if (coinSelected != null) coinsSelected++
                ATCoin(fundingCoin, coinSelected != null, false)
            }

            val finalList = contentList.sortedWith(object : Comparator<AdapterType> {
                override fun compare(lhs: AdapterType?, rhs: AdapterType?): Int {
                    lhs as ATCoin
                    rhs as ATCoin


                    if (lhs.coin.pathIndex > rhs.coin.pathIndex) {
                        return -1
                    } else if (lhs.coin.pathIndex < rhs.coin.pathIndex) {
                        return 1
                    }

                    return (rhs.coin.sats - lhs.coin.sats).toInt()
                }
            })

            val unspentCoinsCount = fundingCoins.size
            val unspentSats = fundingCoins.foldRight(0L) { coin, acc ->
                acc + coin.sats
            }.toString()

            val paymail = getActivePaymailUseCase().first()?.paymail ?: ""


            Model(
                paymail,
                coinsSelected,
                unspentCoinsCount,
                formatSatsLabel(unspentSats),
                highestUsedAddressIndex,
                finalList,
                syncStatus
            )

        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = Model(
                paymail = "",
                coinsSelected = 0,
                unspentCoins = 0,
                unspentSats = "0",
                highestUsedAddressIndex = 0,
                coinsList = emptyList(),
                syncStatus = ChainSyncStatus.INITIALISING
            )
        )


    val infoState = getSecretsUseCase().map { info ->
        Timber.d("....info $info")
         info ?: let {
            return@map InfoModel(
                "",
                "m/0",
                emptyList(),
                "m/0",
                emptyList()
            )
        }
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = InfoModel(
            "",
            "m/0",
            emptyList(),
            "m/0",
            emptyList()
        )
    )

    private val _eventFlow = MutableSharedFlow<UiEvent>(replay = 1)
    val eventFlow = _eventFlow.asSharedFlow()

    fun mergeSelectedCoins() {
        viewModelScope.launch {
            try {
                mergeCoinsUseCase(selectedCoins.value)
                clearCoinSelection()

            } catch (e: ErrorNotReady) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_no_internet))


            } catch (e: ErrorConstructingTx) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_failed_constructing_tx))

            } catch (e: TxNotAcceptedByMiner) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_tx_not_accepted))

            } catch (e: Exception) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error))
            }
        }
    }

    fun startFullSync() {
        resyncCoinsUseCase()
    }

    fun startRefreshCurrentCoins() {
        refreshCoinsUseCase()
    }

    fun onToggleCoinSelection(coin: Coin) {
        selectedCoins.value = let {
            val set = selectedCoins.value.toMutableSet()
            val matchingFoundCoin = set.find {
                it.txId == coin.txId && it.address == coin.address &&
                        it.outputIndex == coin.outputIndex
            }

            if (matchingFoundCoin != null) {
                set.remove(matchingFoundCoin)
            } else {
                set.add(coin)
            }

            set.toList()
        }
    }


    fun clearCoinSelection() {
        selectedCoins.value = listOf()
    }


    sealed class UiEvent {
        object Processing: UiEvent()
        object SendSuccess: UiEvent()
        data class ShowSnackbar(@StringRes val message: Int): UiEvent()
    }

    data class Model(
        val paymail: String,
        val coinsSelected: Int,
        val unspentCoins: Int,
        val unspentSats: String,
        val highestUsedAddressIndex: Int,
        val coinsList: List<AdapterType>,
        val syncStatus: ChainSyncStatus
    )
}