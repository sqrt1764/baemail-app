package app.bitcoin.baemail.fundingWallet.core.presentation.compose

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme

@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode of ComposeSyncButton",
    widthDp = 320
)
@Composable
fun ComposeSyncButtonPreview() {
    val items = listOf(
        "config" to "Config",
        "logs" to "Logs",
    )
    val isDropDownExpanded = remember { mutableStateOf(false) }

    ComposeAppTheme {
        ComposableSyncActionsDropDown(
            isDropDownExpanded,
            items,
            {}
        )
    }
}

@Composable
fun ComposableSyncActionsDropDown(
    expanded: MutableState<Boolean>,
    items: List<Pair<String, String>>,
    onItemSelected: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    Box(modifier) {
        Button(
            onClick = { expanded.value = true },
            contentPadding = PaddingValues(
                start = 16.dp,
                top = 3.dp,
                end = 16.dp,
                bottom = 3.dp
            ),
            shape = RoundedCornerShape(50)

        ) {
            Text(
                stringResource(R.string.sync),
                style = MaterialTheme.typography.button
            )
        }
        Box(Modifier.align(Alignment.TopEnd)) {
            DropdownMenu(
                expanded = expanded.value,
                onDismissRequest = { expanded.value = false }
            ) {
                items.forEach { s ->
                    DropdownMenuItem(onClick = {
                        onItemSelected(s.first)
                        expanded.value = false
                    }) {
                        Text(
                            text = s.second,
                            modifier = Modifier.padding(start = 16.dp, end = 24.dp),
                            style = MaterialTheme.typography.body1
                        )
                    }
                }
            }
        }

    }

}