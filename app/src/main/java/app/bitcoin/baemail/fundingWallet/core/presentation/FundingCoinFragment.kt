package app.bitcoin.baemail.fundingWallet.core.presentation

import android.graphics.Outline
import android.os.Build
import android.os.Bundle
import android.view.ActionMode
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewOutlineProvider
import android.view.WindowInsets
import android.widget.ImageView
import android.widget.TextView
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.doOnPreDraw
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.presentation.compose.ComposeAppTheme
import app.bitcoin.baemail.twetch.ui.TwetchFilesMimeTypeHelper
import app.bitcoin.baemail.core.presentation.util.NoSwipeBehavior
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.view.ErrorSnackBar
import app.bitcoin.baemail.fundingWallet.core.presentation.compose.ComposableSyncActionsDropDown
import app.bitcoin.baemail.fundingWallet.core.presentation.compose.ComposeFundingCoinsOverview
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject


class FundingCoinFragment : Fragment(R.layout.fragment_funding_coins) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: CoinViewModel


    private lateinit var container: ConstraintLayout
    private lateinit var coordinator: CoordinatorLayout
    private lateinit var processingContainer: ConstraintLayout
    private lateinit var recycler: RecyclerView
    private lateinit var appBar: AppBarLayout
    private lateinit var close: ImageView
    private lateinit var screenTitle: TextView
    private lateinit var sync: ComposeView
    private lateinit var topbarBottom: View
    private lateinit var header: ComposeView
    private lateinit var syncingIndicator: TextView

    private lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    private lateinit var adapter: BaseListAdapter

    private var actionMode: ActionMode? = null

    private lateinit var actionModeTitle: TextView

    private val actionModeCallback = object : ActionMode.Callback {
        // Called when the action mode is created; startActionMode() was called
        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            // Inflate a menu resource providing context menu items
            val inflater: MenuInflater = mode.menuInflater
            inflater.inflate(R.menu.menu_funding_coins, menu)

            mode.customView = actionModeTitle

            return true
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
            refreshTopBarVisibility()
            return false // Return false if nothing is done

        }

        // Called when the user selects a contextual menu item
        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            return when (item.itemId) {
                R.id.merge -> {
                    viewModel.mergeSelectedCoins()
                    true
                }
                else -> false
            }
        }

        // Called when the user exits the action mode
        override fun onDestroyActionMode(mode: ActionMode) {
            actionMode = null

            viewModel.clearCoinSelection()

            //the following gets rid of fade-out animation
            (mode.customView.parent as View).visibility = View.GONE

            refreshTopBarVisibility()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_coins)

        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)[CoinViewModel::class.java]

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        container = requireView().findViewById(R.id.root)
        coordinator = requireView().findViewById(R.id.coordinator)
        processingContainer = requireView().findViewById(R.id.processing_container)
        recycler = requireView().findViewById(R.id.recycler)
        appBar = requireView().findViewById(R.id.app_bar)
        close = requireView().findViewById(R.id.close)
        screenTitle = requireView().findViewById(R.id.screen_title)
        sync = requireView().findViewById(R.id.sync)
        topbarBottom = requireView().findViewById(R.id.topbar_bottom)
        header = requireView().findViewById(R.id.header)
        syncingIndicator = requireView().findViewById(R.id.syncing_indicator)

        sync.setContent {
            val items = listOf(
                "refreshCurrent" to stringResource(R.string.refresh_coins),
                "syncCoins" to stringResource(R.string.sync_from_start),
            )
            val onItemSelected: (String) -> Unit = { key ->
                when (key) {
                    "refreshCurrent" -> {
                        viewModel.startRefreshCurrentCoins()
                    }
                    "syncCoins" -> {
                        viewModel.startFullSync()
                    }
                }
            }
            ComposeAppTheme {
                val isDropDownExpanded = remember { mutableStateOf(false) }
                ComposableSyncActionsDropDown(isDropDownExpanded, items, onItemSelected)
            }
        }

        header.setContent {
            val state = viewModel.state.collectAsStateWithLifecycle().value

            ComposeAppTheme {
                ComposeFundingCoinsOverview(
                    state.paymail,
                    state.unspentCoins,
                    state.unspentSats,
                    state.highestUsedAddressIndex,
                    ::onClickSyncConfig,
                    ::onClickMnemonics
                )
            }
        }

        ensureActionModeTitleView()

        val topBarElevation = resources.getDimension(R.dimen.added_paymails_fragment__top_bar_elevation)

        container.doOnApplyWindowInsets { v, windowInsets, initialPadding ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                v.updatePadding(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                v.updatePadding(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight,
                )
            }

            windowInsets
        }

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atCoinListener = object : BaseListAdapter.ATCoinListener {
                    override fun onClick(coin: Coin) {
                        if (actionMode == null) {
                            val args = Bundle()
                            args.putString(CoinOptionsFragment.KEY_HASH_TX, coin.txId)
                            args.putString(CoinOptionsFragment.KEY_ADDRESS, coin.address)
                            args.putInt(CoinOptionsFragment.KEY_OUTPUT_INDEX, coin.outputIndex)

                            findNavController().navigate(
                                R.id.action_fundingCoinFragment_to_coinOptionsFragment,
                                args
                            )
                        } else {
                            viewModel.onToggleCoinSelection(coin)
                        }
                    }

                    override fun onLongClick(coin: Coin) {
                        if (actionMode == null) {
                            enterActionMode(coin)
                        } else {
                            viewModel.onToggleCoinSelection(coin)
                        }
                    }

                }
            )
        )

        layoutManager = LinearLayoutManager(context)
        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER


        //avoid drawing the shadow appBar automatically lays out with at first
        appBar.doOnPreDraw { appBar.elevation = 0f }


        syncingIndicator.clipToOutline = true
        syncingIndicator.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }
        }

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (adapter.itemCount == 0) {
                    appBar.elevation = 0f
                    return
                }
                when (layoutManager.findFirstCompletelyVisibleItemPosition()) {
                    RecyclerView.NO_POSITION -> false
                    0 -> false
                    else -> true
                }.let { showShadow ->
                    if (showShadow) {
                        appBar.elevation = topBarElevation
                    } else {
                        appBar.elevation = 0f
                    }
                }
            }
        })

        val colorAccent = R.color.blue1.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
        val colorSelectorOnAccent = R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        close.foreground = DrawableState.getNew(colorSelector)

        val drawableCloseIcon = R.drawable.ic_clear_24.let { id ->
            val d = ContextCompat.getDrawable(requireContext(), id)!!
            d.setTint(colorAccent)
            d
        }
        close.setImageDrawable(drawableCloseIcon)

        close.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        close.clipToOutline = true
        close.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                outline.setOval(0, 0, view.width, view.height)
            }

        }

        refreshTopBarVisibility()

        adapter.setItems(listOf())


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.eventFlow.collectLatest { event ->
                    when(event) {
                        is CoinViewModel.UiEvent.ShowSnackbar -> {
                            processingContainer.visibility = View.GONE
                            ErrorSnackBar.make(
                                requireContext(),
                                coordinator,
                                getString(event.message)
                            ).let {
                                it.behavior = NoSwipeBehavior()
                                it.duration = Snackbar.LENGTH_SHORT
                                it.show()
                            }
                        }
                        is CoinViewModel.UiEvent.Processing -> {
                            processingContainer.visibility = View.VISIBLE
                        }
                        is CoinViewModel.UiEvent.SendSuccess -> {
                            findNavController().popBackStack()
                        }
                    }
                }
            }
        }


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.state.collect { state ->
                    adapter.setItems(state.coinsList)
                    handleActionMode(state.coinsSelected)

                    if (state.paymail.isEmpty()) {
                        syncingIndicator.visibility = View.INVISIBLE
                        return@collect
                    }
                    when (state.syncStatus) {
                        ChainSyncStatus.LIVE -> {
                            syncingIndicator.visibility = View.INVISIBLE
                        }
                        ChainSyncStatus.DISCONNECTED -> {
                            syncingIndicator.visibility = View.VISIBLE
                            syncingIndicator.text = getString(R.string.disconnected)
                        }
                        ChainSyncStatus.INITIALISING -> {
                            syncingIndicator.visibility = View.VISIBLE
                            syncingIndicator.text = getString(R.string.synchronizing_coins)
                        }
                        ChainSyncStatus.ERROR -> {
                            syncingIndicator.visibility = View.VISIBLE
                            syncingIndicator.text = getString(R.string.error)
                        }
                    }
                }
            }
        }

    }

    private fun ensureActionModeTitleView() {
        actionModeTitle = TextView(requireContext())
        actionModeTitle.setTextAppearance(R.style.TextAppearanceH2)
    }

    private fun enterActionMode(coin: Coin) {
        actionMode = requireActivity().startActionMode(actionModeCallback)
        refreshTopBarVisibility()
        viewModel.onToggleCoinSelection(coin)
    }


    private fun handleActionMode(count: Int) {
        actionModeTitle.text = getString(R.string.x_selected, count)

        actionMode?.let {
            if (count == 0) it.finish()
        }
    }

    private fun refreshTopBarVisibility() {
        if (actionMode == null) {
            close.visibility = View.VISIBLE
            screenTitle.visibility = View.VISIBLE
            sync.visibility = View.VISIBLE
            topbarBottom.visibility = View.VISIBLE
        } else {
            close.visibility = View.GONE
            screenTitle.visibility = View.GONE
            sync.visibility = View.GONE
            topbarBottom.visibility = View.GONE
        }

    }

    private fun onClickSyncConfig() {
        //todo
    }

    private fun onClickMnemonics() {
        findNavController().navigate(R.id.action_fundingCoinFragment_to_walletInfoFragment)
    }

}