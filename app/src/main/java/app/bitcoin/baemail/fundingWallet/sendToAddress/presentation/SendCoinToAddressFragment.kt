package app.bitcoin.baemail.fundingWallet.sendToAddress.presentation

import android.content.Context
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.view.WindowInsets
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.core.presentation.util.NoSwipeBehavior
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.formatSatsLabel
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ErrorSnackBar
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.max


class SendCoinToAddressFragment : Fragment() {

    companion object {
        const val KEY_HASH_TX = "tx_hash"
        const val KEY_ADDRESS = "address"
        const val KEY_OUTPUT_INDEX = "output_index"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: SendToAddressViewModel


    private lateinit var container: FrameLayout
    private lateinit var layoutContainer: ConstraintLayout
    private lateinit var processingContainer: ConstraintLayout
    private lateinit var buttonContainer: ConstraintLayout
    private lateinit var snackbarContainer: CoordinatorLayout

    private lateinit var bottomGap: View
    private lateinit var input: EditText
    private lateinit var errorLabel: TextView
    private lateinit var cancel: TextView
    private lateinit var next: TextView
    private lateinit var value: TextView


    private val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorNextBackground: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }

    private val colorNextBackgroundDisabled: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimaryVariant)
    }

    private val drawableNextBackground: ColorDrawable by lazy {
        ColorDrawable(colorNextBackgroundDisabled)
    }

    private val drawableNextSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private val availableHeightHelper = AvailableScreenHeightHelper()
    private val keyboardHeightState = MutableStateFlow(0)
    private val appliedInsetsState = MutableStateFlow(AppliedInsets())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val txHash = requireArguments().getString(KEY_HASH_TX)!!
        val address = requireArguments().getString(KEY_ADDRESS)!!
        val outputIndex = requireArguments().getInt(KEY_OUTPUT_INDEX)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(SendToAddressViewModel::class.java)

        viewModel.setCoin(
            txHash,
            address,
            outputIndex,
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_send_coin_to_address, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        container = requireView().findViewById(R.id.container)
        layoutContainer = requireView().findViewById(R.id.layout_container)
        buttonContainer = requireView().findViewById(R.id.button_container)
        processingContainer = requireView().findViewById(R.id.processing_container)
        snackbarContainer = requireView().findViewById(R.id.snackbar_container)
        bottomGap = requireView().findViewById(R.id.bottom_gap)
        input = requireView().findViewById(R.id.input)
        cancel = requireView().findViewById(R.id.cancel)
        next = requireView().findViewById(R.id.next)
        errorLabel = requireView().findViewById(R.id.error_label)
        value = requireView().findViewById(R.id.coin_value)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            availableHeightHelper.setup(
                requireContext(),
                requireActivity().window,
                viewLifecycleOwner,
                { requireView() }
            )

            availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
                height ?: return@Observer

                val maxHeight = container.height - appliedInsetsState.value.top - appliedInsetsState.value.bottom
                val keyboardHeight = max(maxHeight - height, 0)

                keyboardHeightState.value = keyboardHeight
            })

            viewLifecycleOwner.lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    keyboardHeightState.collectLatest { keyboardHeight ->
                        onKeyboardHeightChange(keyboardHeight)
                    }
                }
            }
        }

        container.doOnApplyWindowInsets { v, windowInsets, _ ->
            val appliedInsets = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                AppliedInsets(
                    top = windowInsets.getInsets(WindowInsets.Type.systemBars()).top,
                    bottom = windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    left = windowInsets.getInsets(WindowInsets.Type.systemBars()).left,
                    right = windowInsets.getInsets(WindowInsets.Type.systemBars()).right
                )
            } else {
                AppliedInsets(
                    top = windowInsets.systemWindowInsetTop,
                    bottom = windowInsets.systemWindowInsetBottom,
                    left = windowInsets.systemWindowInsetLeft,
                    right = windowInsets.systemWindowInsetRight
                )
            }

            v.updatePadding(
                top = appliedInsets.top,
                bottom = appliedInsets.bottom,
                left = appliedInsets.left,
                right = appliedInsets.right
            )

            appliedInsetsState.value = appliedInsets

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val keyboardHeight = max(
                    windowInsets.getInsets(WindowInsets.Type.ime()).bottom -
                            windowInsets.getInsets(WindowInsets.Type.systemBars()).bottom,
                    0
                )

                keyboardHeightState.value = keyboardHeight

                onKeyboardHeightChange(keyboardHeight)
            }

            windowInsets
        }


        cancel.clipToOutline = true
        cancel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }


        cancel.background = let {
            val colorCancelBackground = requireContext().getColorFromAttr(R.attr.colorPrimary)

            LayerDrawable(arrayOf(
                ColorDrawable(colorCancelBackground),
                DrawableState.getNew(colorSelectorOnAccent)
            ))
        }


        next.clipToOutline = true
        next.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        next.background = LayerDrawable(arrayOf(
            drawableNextBackground,
            drawableNextSelector
        ))


        input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                viewModel.onDestinationAddressChanged(input.text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })


        cancel.setOnClickListener {
            imm.hideSoftInputFromWindow(requireView().windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            findNavController().popBackStack()
        }

        next.setOnClickListener {
            imm.hideSoftInputFromWindow(requireView().windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            viewModel.onRequestedSend()
        }


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.eventFlow.collectLatest { event ->
                    when(event) {
                        is SendToAddressViewModel.UiEvent.ShowSnackbar -> {
                            processingContainer.visibility = View.GONE

                            ErrorSnackBar.make(
                                requireContext(),
                                snackbarContainer,
                                getString(event.message)
                            ).let {
                                it.behavior = NoSwipeBehavior()
                                it.duration = Snackbar.LENGTH_SHORT
                                it.show()
                            }
                        }
                        is SendToAddressViewModel.UiEvent.Processing -> {
                            processingContainer.visibility = View.VISIBLE
                        }
                        is SendToAddressViewModel.UiEvent.SendSuccess -> {
                            findNavController().popBackStack()
                        }
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {

                viewModel.state.collect { state ->
                    value.text = getString(R.string.x_sats, formatSatsLabel(state.value.toString()))

                    if (input.text.toString() != state.input) {
                        input.setText(state.input)
                    }

                    when (state.state) {
                        SendToAddressViewModel.SendToAddressState.VALID -> {
                            errorLabel.visibility = View.INVISIBLE
                        }
                        SendToAddressViewModel.SendToAddressState.ERROR_COIN_INVALID -> {
                            errorLabel.visibility = View.VISIBLE
                            errorLabel.text = getString(R.string.coin_is_missing)
                        }
                        SendToAddressViewModel.SendToAddressState.NOT_AN_ADDRESS -> {
                            errorLabel.visibility = View.VISIBLE
                            errorLabel.text = getString(R.string.not_valid_address)
                        }
                        SendToAddressViewModel.SendToAddressState.ERROR_NO_INTERNET -> {
                            errorLabel.visibility = View.VISIBLE
                            errorLabel.text = getString(R.string.error_no_internet)
                        }
                    }

                    if (SendToAddressViewModel.SendToAddressState.VALID == state.state) {
                        next.isEnabled = true
                        drawableNextBackground.color = colorNextBackground
                    } else {
                        next.isEnabled = false
                        drawableNextBackground.color = colorNextBackgroundDisabled
                    }

                }
            }
        }

    }

    private fun onKeyboardHeightChange(keyboardHeight: Int) {
        buttonContainer.translationY = -1f * keyboardHeight
        bottomGap.layoutParams.let { lp ->
            lp.height = keyboardHeight
            bottomGap.layoutParams = lp
        }
    }

    private fun focusOnInput() {
        if (input.requestFocus()) {
            if (!imm.isActive(input)) imm.restartInput(input)
        }
    }

    override fun onStart() {
        super.onStart()
        focusOnInput()
    }
}