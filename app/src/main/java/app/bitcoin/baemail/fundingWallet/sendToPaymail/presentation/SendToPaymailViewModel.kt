package app.bitcoin.baemail.fundingWallet.sendToPaymail.presentation

import androidx.annotation.StringRes
import androidx.lifecycle.*
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.domain.entities.CoinRef
import app.bitcoin.baemail.core.domain.usecases.CheckPaymailExistsUseCase
import app.bitcoin.baemail.core.domain.usecases.CoinSyncStatusUseCase
import app.bitcoin.baemail.core.domain.usecases.GetCoinUseCase
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.ErrorConstructingTx
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.ErrorGettingDestinationOutput
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.ErrorNotReady
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.SendToPaymailUseCase
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.core.domain.entities.PaymailExistence
import app.bitcoin.baemail.core.domain.usecases.GetExchangeRateUseCase
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.ErrorPaymailDoesNotExist
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.ErrorSubmittingTx
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.TxNotAcceptedByServer
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

class SendToPaymailViewModel @Inject constructor(
    private val getCoinUseCase: GetCoinUseCase,
    coinSyncStatusUseCase: CoinSyncStatusUseCase,
    checkPaymailExistsUseCase: CheckPaymailExistsUseCase,
    private val sendToPaymailUseCase: SendToPaymailUseCase,
    exchangeRateUseCase: GetExchangeRateUseCase
) : ViewModel() {

    private val coinRef = MutableStateFlow<CoinRef?>(null)
    private val _destinationPaymail = MutableStateFlow("")
    val destinationPaymail = _destinationPaymail.asStateFlow()

    private val _note = MutableStateFlow("")
    val note = _note.asStateFlow()

    private val coin: StateFlow<Coin?> = coinRef.flatMapLatest { ref ->
        ref ?: return@flatMapLatest flow { emit(null) }
        getCoinUseCase(ref)
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = null
    )

    val paymailExistence: Flow<PaymailExistence> = _destinationPaymail
        .flatMapLatest { value ->
            checkPaymailExistsUseCase(value)
        }


    val coinInfo: Flow<CoinInfo> = coin
        .combine(exchangeRateUseCase.exchangeRate) { coin, exchangeRateInfo ->
            coin ?: return@combine CoinInfo(0, 0f)
            CoinInfo(coin.sats, coin.sats.toFloat() / (exchangeRateInfo.satsInACent * 100))
        }


    val state = coin.combine(coinSyncStatusUseCase()) { coin, syncStatus ->
        coin to syncStatus

    }.combine(paymailExistence) { (coin, syncStatus), paymailExistenceState ->
        return@combine when {
            coin == null -> State.ERROR_COIN_INVALID
            ChainSyncStatus.LIVE != syncStatus -> State.ERROR_NO_INTERNET
            paymailExistenceState is PaymailExistence.Valid -> State.VALID
            else -> null
        }

    }

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun setCoin(
        txHash: String,
        address: String,
        outputIndex: Int,
    ) {
        coinRef.value = CoinRef(address, txHash, outputIndex)
    }

    fun onDestinationPaymailChanged(paymail: String) {
        _destinationPaymail.value = paymail.trim()
    }

    fun onNoteChanged(note: String) {
        _note.value = note
    }

    fun onRequestedSend() {
        viewModelScope.launch {
            _eventFlow.emit(UiEvent.Processing)

            try {
                val coin = coin.value ?: throw RuntimeException()
                sendToPaymailUseCase(coin, _destinationPaymail.value, _note.value)
                _eventFlow.emit(UiEvent.SendSuccess)

            } catch (e: ErrorNotReady) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_no_internet))

            } catch (e: ErrorPaymailDoesNotExist) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.paymail_not_found))

            } catch (e: ErrorConstructingTx) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_failed_constructing_tx))

            } catch (e: ErrorGettingDestinationOutput) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_finding_paymail_destination_output))

            } catch (e: ErrorSubmittingTx) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_tx_not_accepted_by_paymail))

            } catch (e: TxNotAcceptedByServer) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error_tx_not_accepted))

            } catch (e: Exception) {
                _eventFlow.emit(UiEvent.ShowSnackbar(R.string.error))
            }
        }

    }

    sealed class UiEvent {
        object Processing: UiEvent()
        object SendSuccess: UiEvent()
        data class ShowSnackbar(@StringRes val message: Int): UiEvent()
    }

    enum class State {
        VALID,
        ERROR_COIN_INVALID,
        ERROR_NO_INTERNET
    }
}

data class CoinInfo(
    val sats: Long,
    val usd: Float
)