package app.bitcoin.baemail.intercept

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.CentralActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import java.io.Serializable
import javax.inject.Inject

class InterceptActivity : AppCompatActivity(), HasAndroidInjector {

    companion object {
        const val KEY_DESTINATION = "destination"
        const val DESTINATION_P2P_STALL_MANAGEMENT = "p2p_stall_management"
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: InterceptViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.d("created")

        viewModel = ViewModelProvider(this, viewModelFactory).get(InterceptViewModel::class.java)

        lifecycleScope.launchWhenResumed {
            handleInterceptedLink()
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        Timber.d("onNewIntent")

        lifecycleScope.launchWhenResumed {
            handleInterceptedLink()
        }
    }

    private suspend fun handleInterceptedLink() {

        intent.getStringExtra(KEY_DESTINATION)?.let { destination ->
            Timber.d("destination: $destination")

            when (destination) {
                DESTINATION_P2P_STALL_MANAGEMENT -> {
                    navigateToCentralActivity(AppDeepLink(
                        AppDeepLinkType.P2P_STALL_MANAGE,
                        ""
                    ))
                }
                else -> {
                    Timber.d("destination is missing")
                }
            }

            return
        }

        val uri = intent.data ?: let {
            Timber.d("`url` is missing")
            finish()
            return
        }


        val intent = Intent(Intent.ACTION_VIEW, uri)

        //check if the active-user is connected with twetch
        if (!viewModel.hasTwetchBeenAuthenticated()) {
            Timber.d("active paymail has not been authenticated with twetch")
            fallbackOpenInDefaultBrowser(intent)
            return
        }



        val path = uri.encodedPath!!

        Timber.d("uri: $uri; path: $path")

        if (path.startsWith("/t/")) {
            val postTxId = path.replace("/t/", "")

            navigateToCentralActivity(AppDeepLink(
                AppDeepLinkType.TWETCH_POST,
                postTxId
            ))

        } else if (path.startsWith("/u/")) {
            val userId = path.replace("/u/", "")

            navigateToCentralActivity(AppDeepLink(
                AppDeepLinkType.TWETCH_USER,
                userId
            ))

        } else if (path.startsWith("/chat/")) {
            val chatId = path.replace("/chat/", "")

            navigateToCentralActivity(AppDeepLink(
                AppDeepLinkType.TWETCH_CHAT,
                chatId
            ))

        } else {
            fallbackOpenInDefaultBrowser(intent)

        }


    }

    private fun navigateToCentralActivity(link: AppDeepLink) {
        viewModel.deepLinkHelper.requestNavigateToLink(link)

        val intent = Intent(this, CentralActivity::class.java)
        startActivity(intent)
        finish()
    }



    private fun fallbackOpenInDefaultBrowser(intent: Intent) {
        //find all activities that support opening of web-urls
        val webIntentActivities: List<ResolveInfo> = packageManager.queryIntentActivities(
            intent,
            PackageManager.MATCH_ALL
        )

        val componentName = webIntentActivities.let { list ->
            val info = list.find {
                //find first activity that does not belong to this app
                return@find it.activityInfo.packageName != packageName
            } ?: let {
                Toast.makeText(this, R.string.valid_activity_not_found, Toast.LENGTH_SHORT).show()
                return
            }

            ComponentName(
                info.activityInfo.packageName,
                info.activityInfo.name
            )
        }

        intent.component = componentName

        startActivity(intent)
    }


    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }
}

enum class AppDeepLinkType {
    TWETCH_POST,
    TWETCH_USER,
    TWETCH_CHAT,
    P2P_STALL_MANAGE
}

data class AppDeepLink(
    val type: AppDeepLinkType,
    val value: String
) : Serializable