package app.bitcoin.baemail.intercept

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.bitcoin.baemail.twetch.ui.TwetchAuthStore
import app.bitcoin.baemail.core.data.util.DeepLinkHelper
import app.bitcoin.baemail.core.domain.entities.ActivePaymail
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.inject.Inject
import kotlin.coroutines.resume

class InterceptViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    val deepLinkHelper: DeepLinkHelper,
    private val twetchAuthStore: TwetchAuthStore
) : ViewModel() {

    private var isInitialized = false
    private var activePaymail: ActivePaymail? = null
    private val awaitingContinuations = mutableListOf<CancellableContinuation<String?>>()

    init {
        viewModelScope.launch {
            authRepository.activePaymail.collectLatest { paymail ->
                isInitialized = true
                activePaymail = paymail

                val listCopy = ArrayList(awaitingContinuations)
                awaitingContinuations.clear()

                listCopy.forEach { continuation ->
                    continuation.resume(paymail?.paymail)
                }
            }
        }
    }

    suspend fun hasTwetchBeenAuthenticated(): Boolean {

        val activePaymail = suspendCancellableCoroutine<String?> { continuation ->
            if (isInitialized) {
                continuation.resume(activePaymail?.paymail)
            } else {
                awaitingContinuations.add(continuation)
            }
        } ?: return false

        val info = twetchAuthStore.getFromSourceOfTruth(
            TwetchAuthStore.AuthKey(activePaymail, true)
        ) ?: return false

        info.profile?.id ?: return false

        return true
    }



}