package app.bitcoin.baemail.di

import android.content.Context
import app.bitcoin.baemail.core.data.wallet.NotifyCoinsUsedByUserLiveData
import app.bitcoin.baemail.core.data.wallet.sending.SignMessageHelper
import app.bitcoin.baemail.core.domain.repository.AppConfigRepository
import app.bitcoin.baemail.core.domain.repository.BsvStatusRepository
import app.bitcoin.baemail.core.domain.repository.CoinRepository
import app.bitcoin.baemail.core.domain.usecases.AppConfigUseCases
import app.bitcoin.baemail.core.domain.usecases.AppConfigUseCasesImpl
import app.bitcoin.baemail.core.domain.usecases.CalculateBasicTxSizeUseCase
import app.bitcoin.baemail.core.domain.usecases.CalculateBasicTxSizeUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.CheckAddressIsValidUseCase
import app.bitcoin.baemail.core.domain.usecases.CheckAddressIsValidUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.CheckPaymailExistsUseCase
import app.bitcoin.baemail.core.domain.usecases.CheckPaymailExistsUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.CoinSynStatusUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.CoinSyncStatusUseCase
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCase
import app.bitcoin.baemail.core.domain.usecases.GetActivePaymailUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.GetExchangeRateUseCase
import app.bitcoin.baemail.core.domain.usecases.GetExchangeRateUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.GetCoinUseCase
import app.bitcoin.baemail.core.domain.usecases.GetCoinUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.GetFundingCoinsUseCase
import app.bitcoin.baemail.core.domain.usecases.GetFundingCoinsUseCaseImpl
import app.bitcoin.baemail.fundingWallet.core.domain.MergeCoinsUseCase
import app.bitcoin.baemail.fundingWallet.core.domain.MergeCoinsUseCaseImpl
import app.bitcoin.baemail.fundingWallet.core.domain.ResyncCoinsUseCase
import app.bitcoin.baemail.fundingWallet.core.domain.ResyncCoinsUseCaseImpl
import app.bitcoin.baemail.fundingWallet.equalSplit.domain.SplitCoinUseCase
import app.bitcoin.baemail.fundingWallet.equalSplit.domain.SplitCoinUseCaseImpl
import app.bitcoin.baemail.fundingWallet.sendToAddress.domain.SendCoinToAddressUseCase
import app.bitcoin.baemail.fundingWallet.sendToAddress.domain.SendCoinToAddressUseCaseImpl
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.SendToPaymailUseCase
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.SendToPaymailUseCaseImpl
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.SignMessageUseCase
import app.bitcoin.baemail.fundingWallet.sendToPaymail.domain.SignMessageUseCaseImpl
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.data.wallet.DynamicChainSync
import app.bitcoin.baemail.core.data.util.SeedWordList
import app.bitcoin.baemail.core.domain.usecases.CheckIsValidEmailUseCase
import app.bitcoin.baemail.core.domain.usecases.CheckIsValidEmailUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.CheckIsValidPkiPathUseCase
import app.bitcoin.baemail.core.domain.usecases.CheckIsValidPkiPathUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.DeleteAllLogsUseCase
import app.bitcoin.baemail.core.domain.usecases.DeleteAllLogsUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.GetSeedWordListUseCase
import app.bitcoin.baemail.core.domain.usecases.GetSeedWordListUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.RestartCoinSyncUseCase
import app.bitcoin.baemail.core.domain.usecases.RestartCoinSyncUseCaseImpl
import app.bitcoin.baemail.logs.domain.LogsRepository
import app.bitcoin.baemail.paymail.domain.usecases.AddAuthUseCase
import app.bitcoin.baemail.paymail.domain.usecases.AddAuthUseCaseImpl
import app.bitcoin.baemail.paymail.domain.usecases.GetAuthenticatedPaymailsUseCase
import app.bitcoin.baemail.paymail.domain.usecases.GetAuthenticatedPaymailsUseCaseImpl
import app.bitcoin.baemail.paymail.domain.usecases.GetPkiPathUseCase
import app.bitcoin.baemail.paymail.domain.usecases.GetPkiPathUseCaseImpl
import app.bitcoin.baemail.paymail.domain.usecases.SwitchActivePaymailUseCase
import app.bitcoin.baemail.paymail.domain.usecases.SwitchActivePaymailUseCaseImpl
import app.bitcoin.baemail.repl.domain.repository.SavedReplInputRepository
import app.bitcoin.baemail.repl.domain.usecase.AddSavedReplInputUseCase
import app.bitcoin.baemail.repl.domain.usecase.AddSavedReplInputUseCaseImpl
import app.bitcoin.baemail.repl.domain.usecase.GetSavedReplInputsUseCase
import app.bitcoin.baemail.repl.domain.usecase.GetSavedReplInputsUseCaseImpl
import app.bitcoin.baemail.repl.domain.usecase.RemoveSavedReplInputUseCase
import app.bitcoin.baemail.repl.domain.usecase.RemoveSavedReplInputUseCaseImpl
import app.bitcoin.baemail.repl.domain.usecase.ReplRunCommandUseCase
import app.bitcoin.baemail.repl.domain.usecase.ReplRunCommandUseCaseImpl
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.domain.repository.PaymailInfoRepository
import app.bitcoin.baemail.core.domain.repository.SecureDataRepository
import app.bitcoin.baemail.core.domain.usecases.CoinInfoUseCase
import app.bitcoin.baemail.core.domain.usecases.CoinInfoUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.GetBlockStatsFeeUseCase
import app.bitcoin.baemail.core.domain.usecases.GetBlockStatsFeeUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.GetHighestUsedAddressIndexUseCase
import app.bitcoin.baemail.core.domain.usecases.GetHighestUsedAddressIndexUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.GetSecretsUseCase
import app.bitcoin.baemail.core.domain.usecases.GetSecretsUseCaseImpl
import app.bitcoin.baemail.core.domain.usecases.NormalizePkiPathUseCase
import app.bitcoin.baemail.core.domain.usecases.NormalizePkiPathUseCaseImpl
import app.bitcoin.baemail.fundingWallet.core.domain.RefreshCurrentCoinsUseCase
import app.bitcoin.baemail.fundingWallet.core.domain.RefreshCurrentCoinsUseCaseImpl
import app.bitcoin.baemail.logs.domain.GetLogFilesListUseCase
import app.bitcoin.baemail.logs.domain.GetLogFilesListUseCaseImpl
import app.bitcoin.baemail.message.domain.ApplyNewReplyDraftUseCase
import app.bitcoin.baemail.message.domain.ApplyNewReplyDraftUseCaseImpl
import app.bitcoin.baemail.message.domain.ArchiveBaemailMessageUseCase
import app.bitcoin.baemail.message.domain.ArchiveBaemailMessageUseCaseImpl
import app.bitcoin.baemail.message.domain.BaemailRepository
import app.bitcoin.baemail.message.domain.DeleteBaemailMessageUseCase
import app.bitcoin.baemail.message.domain.DeleteBaemailMessageUseCaseImpl
import app.bitcoin.baemail.message.domain.DiscardBaemailDraftUseCase
import app.bitcoin.baemail.message.domain.DiscardBaemailDraftUseCaseImpl
import app.bitcoin.baemail.message.domain.EstimateBaemailMessageTxSize
import app.bitcoin.baemail.message.domain.EstimateBaemailMessageTxSizeImpl
import app.bitcoin.baemail.message.domain.FormatMessageDateUseCase
import app.bitcoin.baemail.message.domain.FormatMessageDateUseCaseImpl
import app.bitcoin.baemail.message.domain.FormatMessageTimeUseCase
import app.bitcoin.baemail.message.domain.FormatMessageTimeUseCaseImpl
import app.bitcoin.baemail.message.domain.GetMessageUseCase
import app.bitcoin.baemail.message.domain.GetMessageUseCaseImpl
import app.bitcoin.baemail.message.domain.GetSavedMessageDraftUseCase
import app.bitcoin.baemail.message.domain.GetSavedMessageDraftUseCaseImpl
import app.bitcoin.baemail.message.domain.HtmlToSpannedStringUseCase
import app.bitcoin.baemail.message.domain.HtmlToSpannedStringUseCaseImpl
import app.bitcoin.baemail.message.domain.MapMessagesToAdapterTypesUseCase
import app.bitcoin.baemail.message.domain.MapMessagesToAdapterTypesUseCaseImpl
import app.bitcoin.baemail.message.domain.ParseDecryptedMessageContent
import app.bitcoin.baemail.message.domain.ParseDecryptedMessageContentImpl
import app.bitcoin.baemail.message.domain.PostBaemailReadReceiptUseCase
import app.bitcoin.baemail.message.domain.PostBaemailReadReceiptUseCaseImpl
import app.bitcoin.baemail.message.domain.SaveBaemailDraftUseCase
import app.bitcoin.baemail.message.domain.SaveBaemailDraftUseCaseImpl
import app.bitcoin.baemail.message.domain.SearchMessagesUseCase
import app.bitcoin.baemail.message.domain.SearchMessagesUseCaseImpl
import app.bitcoin.baemail.message.domain.SendBaemailMessageUseCase
import app.bitcoin.baemail.message.domain.SendBaemailMessageUseCaseImpl
import app.bitcoin.baemail.message.domain.UnarchiveBaemailMessageUseCase
import app.bitcoin.baemail.message.domain.UnarchiveBaemailMessageUseCaseImpl
import app.bitcoin.baemail.paymail.domain.usecases.CheckIsValidMnemonicUseCase
import app.bitcoin.baemail.paymail.domain.usecases.CheckIsValidMnemonicUseCaseImpl
import app.bitcoin.baemail.paymail.domain.usecases.FindFundingWalletSeedUseCase
import app.bitcoin.baemail.paymail.domain.usecases.FindFundingWalletSeedUseCaseImpl
import app.bitcoin.baemail.paymail.domain.usecases.ValidatePaymailUseCase
import app.bitcoin.baemail.paymail.domain.usecases.ValidatePaymailUseCaseImpl
import app.bitcoin.baemail.paymail.domain.usecases.ValidateWalletInfoUseCase
import app.bitcoin.baemail.paymail.domain.usecases.ValidateWalletInfoUseCaseImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

@Module
class UseCaseModule {

    @Provides
    fun provideAppConfigUseCases(
        appConfigRepository: AppConfigRepository,
        coroutineUtil: CoroutineUtil
    ): AppConfigUseCases {

        val job = SupervisorJob()
        val scope = CoroutineScope(coroutineUtil.ioDispatcher + job)

        return AppConfigUseCasesImpl(appConfigRepository, scope)
    }

    @Provides
    fun provideGetCoinUseCase(
        coinRepository: CoinRepository
    ): GetCoinUseCase = GetCoinUseCaseImpl(
        coinRepository
    )

    @Provides
    fun provideCoinSyncStatusUseCase(
        coinRepository: CoinRepository
    ): CoinSyncStatusUseCase =
        CoinSynStatusUseCaseImpl(
            coinRepository
        )

    @Provides
    fun provideGetExchangeRateUseCase(
        bsvStatusRepository: BsvStatusRepository
    ): GetExchangeRateUseCase =
        GetExchangeRateUseCaseImpl(
            bsvStatusRepository
        )

    @Provides
    fun provideCalculateBasicTxSizeUseCase(): CalculateBasicTxSizeUseCase =
        CalculateBasicTxSizeUseCaseImpl()

    @Provides
    fun provideSplitCoinUseCase(
        coinRepository: CoinRepository,
        secureDataRepository: SecureDataRepository,
        bsvStatusRepositoryImpl: BsvStatusRepository,
        nodeRuntimeRepository: NodeRuntimeRepository,
        calculateBasicTxSizeUseCase: CalculateBasicTxSizeUseCase
    ): SplitCoinUseCase =
        SplitCoinUseCaseImpl(
            coinRepository,
            secureDataRepository,
            bsvStatusRepositoryImpl,
            nodeRuntimeRepository,
            calculateBasicTxSizeUseCase
        )

    @Provides
    fun provideCheckAddressIsValidUseCase(): CheckAddressIsValidUseCase =
        CheckAddressIsValidUseCaseImpl()

    @Provides
    fun provideSendCoinToAddressUseCase(
        coinRepository: CoinRepository,
        secureDataRepository: SecureDataRepository,
        nodeRuntimeRepository: NodeRuntimeRepository,
        bsvStatusRepository: BsvStatusRepository,
        calculateBasicTxSizeUseCase: CalculateBasicTxSizeUseCase
    ): SendCoinToAddressUseCase {
        return SendCoinToAddressUseCaseImpl(
            coinRepository,
            secureDataRepository,
            nodeRuntimeRepository,
            bsvStatusRepository,
            calculateBasicTxSizeUseCase
        )
    }

    @Provides
    fun provideCheckPaymailExistsUseCase(
        paymailInfoRepository: PaymailInfoRepository
    ): CheckPaymailExistsUseCase = CheckPaymailExistsUseCaseImpl(paymailInfoRepository)

    @Provides
    fun provideSignMessageUseCase(
        secureDataSource: SecureDataSource,
        nodeRuntimeRepository: NodeRuntimeRepository,
        signMessageHelper: SignMessageHelper
    ): SignMessageUseCase = SignMessageUseCaseImpl(
        signMessageHelper,
        nodeRuntimeRepository,
        secureDataSource,
    )

    @Provides
    fun provideSendToPaymailUseCase(
        signMessageUseCase: SignMessageUseCase,
        coinRepository: CoinRepository,
        secureDataRepository: SecureDataRepository,
        nodeRuntimeRepository: NodeRuntimeRepository,
        bsvStatusRepository: BsvStatusRepository,
        checkPaymailExistsUseCase: CheckPaymailExistsUseCase,
        calculateBasicTxSizeUseCase: CalculateBasicTxSizeUseCase,
        paymailInfoRepository: PaymailInfoRepository
    ): SendToPaymailUseCase = SendToPaymailUseCaseImpl(
        coinRepository,
        secureDataRepository,
        nodeRuntimeRepository,
        bsvStatusRepository,
        checkPaymailExistsUseCase,
        calculateBasicTxSizeUseCase,
        signMessageUseCase,
        paymailInfoRepository
    )

    @Provides
    fun provideResyncCoinsUseCaseImpl(
        authRepository: AuthRepository,
        coinRepository: CoinRepository,
        coroutineUtil: CoroutineUtil,
        dynamicChainSync: DynamicChainSync
    ): ResyncCoinsUseCase = ResyncCoinsUseCaseImpl(
        authRepository,
        coinRepository,
        coroutineUtil.appScope,
        dynamicChainSync
    )

    @Provides
    fun provideRefreshCurrentCoinsUseCase(
        authRepository: AuthRepository,
        coinRepository: CoinRepository,
        coroutineUtil: CoroutineUtil,
        dynamicChainSync: DynamicChainSync
    ): RefreshCurrentCoinsUseCase = RefreshCurrentCoinsUseCaseImpl(
        authRepository,
        coinRepository,
        coroutineUtil.appScope,
        dynamicChainSync
    )

    @Provides
    fun provideGetFundingCoins(
        coinRepository: CoinRepository
    ): GetFundingCoinsUseCase = GetFundingCoinsUseCaseImpl(coinRepository)

    @Provides
    fun GetActivePaymailUseCase(
        authRepository: AuthRepository
    ): GetActivePaymailUseCase = GetActivePaymailUseCaseImpl(authRepository)

    @Provides
    fun provideMergeCoinsUseCase(
        coinRepository: CoinRepository,
        secureDataRepository: SecureDataRepository,
        bsvStatusRepositoryImpl: BsvStatusRepository,
        nodeRuntimeRepository: NodeRuntimeRepository,
        calculateBasicTxSizeUseCase: CalculateBasicTxSizeUseCase
    ): MergeCoinsUseCase = MergeCoinsUseCaseImpl(
        coinRepository,
        secureDataRepository,
        bsvStatusRepositoryImpl,
        nodeRuntimeRepository,
        calculateBasicTxSizeUseCase
    )

    @Provides
    fun provideGetPkiPathUseCase(): GetPkiPathUseCase = GetPkiPathUseCaseImpl()

    @Provides
    fun provideGetSeedWordListUseCaseImpl(
        seedWordList: SeedWordList
    ): GetSeedWordListUseCase = GetSeedWordListUseCaseImpl(
        seedWordList
    )

    @Provides
    fun provideCheckIsValidEmailUseCase(): CheckIsValidEmailUseCase =
        CheckIsValidEmailUseCaseImpl()

    @Provides
    fun provideCheckIsValidPkiPathUseCase(): CheckIsValidPkiPathUseCase =
        CheckIsValidPkiPathUseCaseImpl()

    @Provides
    fun provideAddAuthUseCase(
        authRepository: AuthRepository
    ): AddAuthUseCase = AddAuthUseCaseImpl(
        authRepository
    )

    @Provides
    fun provideGetAuthenticatedPaymailsUseCase(
        authRepository: AuthRepository
    ): GetAuthenticatedPaymailsUseCase = GetAuthenticatedPaymailsUseCaseImpl(
        authRepository
    )

    @Provides
    fun provideSwitchActivePaymailUseCase(
        authRepository: AuthRepository
    ): SwitchActivePaymailUseCase = SwitchActivePaymailUseCaseImpl(
        authRepository
    )

    @Provides
    fun provideDeleteAllLogsUseCase(
        logsRepository: LogsRepository
    ): DeleteAllLogsUseCase = DeleteAllLogsUseCaseImpl(
        logsRepository
    )

    @Provides
    fun provideRestartCoinSyncUseCase(
        dynamicChainSync: DynamicChainSync
    ): RestartCoinSyncUseCase = RestartCoinSyncUseCaseImpl(
        dynamicChainSync
    )

    @Provides
    fun provideGetSavedReplInputsUseCase(
        authRepository: AuthRepository,
        savedReplRepository: SavedReplInputRepository,
        coroutineUtil: CoroutineUtil
    ): GetSavedReplInputsUseCase = GetSavedReplInputsUseCaseImpl(
        authRepository,
        savedReplRepository,
        coroutineUtil.defaultDispatcher
    )

    @Provides
    fun provideAddSavedReplInputUseCase(
        authRepository: AuthRepository,
        savedReplRepository: SavedReplInputRepository,
        coroutineUtil: CoroutineUtil
    ): AddSavedReplInputUseCase = AddSavedReplInputUseCaseImpl(
        authRepository,
        savedReplRepository,
        coroutineUtil.defaultDispatcher
    )

    @Provides
    fun provideRemoveSavedReplInputUseCase(
        authRepository: AuthRepository,
        savedReplRepository: SavedReplInputRepository,
        coroutineUtil: CoroutineUtil
    ): RemoveSavedReplInputUseCase = RemoveSavedReplInputUseCaseImpl(
        authRepository,
        savedReplRepository,
        coroutineUtil.defaultDispatcher
    )

    @Provides
    fun provideReplRunCommandUseCase(
        nodeRuntimeRepository: NodeRuntimeRepository,
        coroutineUtil: CoroutineUtil
    ): ReplRunCommandUseCase = ReplRunCommandUseCaseImpl(
        nodeRuntimeRepository,
        coroutineUtil.ioDispatcher
    )

    @Provides
    fun provideGetLogFilesListUseCase(
        logsRepository: LogsRepository
    ): GetLogFilesListUseCase = GetLogFilesListUseCaseImpl(
        logsRepository
    )

    @Provides
    fun provideFormatMessageDateUseCase(
        appContext: Context
    ): FormatMessageDateUseCase = FormatMessageDateUseCaseImpl(
        appContext
    )

    @Provides
    fun provideFormatMessageTimeUseCase(
        appContext: Context
    ): FormatMessageTimeUseCase = FormatMessageTimeUseCaseImpl(
        appContext
    )

    @Provides
    fun provideSearchMessagesUseCase(
        baemailRepository: BaemailRepository,
        coroutineUtil: CoroutineUtil
    ): SearchMessagesUseCase = SearchMessagesUseCaseImpl(
        baemailRepository,
        coroutineUtil.ioDispatcher
    )

    @Provides
    fun provideGetMessageUseCase(
        getActivePaymailUseCase: GetActivePaymailUseCase,
        baemailRepository: BaemailRepository,
    ): GetMessageUseCase = GetMessageUseCaseImpl(
        getActivePaymailUseCase,
        baemailRepository
    )

    @Provides
    fun provideParseDecryptedMessageContent(
        gson: Gson
    ) : ParseDecryptedMessageContent = ParseDecryptedMessageContentImpl(
        gson
    )

    @Provides
    fun provideHtmlToSpannedStringUseCase(): HtmlToSpannedStringUseCase =
        HtmlToSpannedStringUseCaseImpl()

    @Provides
    fun provideMapMessagesToAdapterTypesUseCaseImpl(
        formatMessageDateUseCase: FormatMessageDateUseCase,
        htmlToSpannedStringUseCase: HtmlToSpannedStringUseCase,
        parseDecryptedMessageContent: ParseDecryptedMessageContent,
        gson: Gson
    ): MapMessagesToAdapterTypesUseCase = MapMessagesToAdapterTypesUseCaseImpl(
        gson,
        formatMessageDateUseCase,
        htmlToSpannedStringUseCase,
        parseDecryptedMessageContent
    )

    @Provides
    fun provideArchiveBaemailMessage(
        baemailRepository: BaemailRepository,
        getActivePaymailUseCase: GetActivePaymailUseCase
    ): ArchiveBaemailMessageUseCase = ArchiveBaemailMessageUseCaseImpl(
        baemailRepository,
        getActivePaymailUseCase
    )

    @Provides
    fun provideUnarchiveBaemailMessage(
        baemailRepository: BaemailRepository,
        getActivePaymailUseCase: GetActivePaymailUseCase
    ): UnarchiveBaemailMessageUseCase = UnarchiveBaemailMessageUseCaseImpl(
        baemailRepository,
        getActivePaymailUseCase
    )

    @Provides
    fun provideDeleteBaemailMessageUseCase(
        baemailRepository: BaemailRepository,
        getActivePaymailUseCase: GetActivePaymailUseCase
    ): DeleteBaemailMessageUseCase = DeleteBaemailMessageUseCaseImpl(
        baemailRepository,
        getActivePaymailUseCase
    )

    @Provides
    fun providePostBaemailReadReceiptUseCase(
        baemailRepository: BaemailRepository,
        getActivePaymailUseCase: GetActivePaymailUseCase
    ): PostBaemailReadReceiptUseCase = PostBaemailReadReceiptUseCaseImpl(
        baemailRepository,
        getActivePaymailUseCase
    )

    @Provides
    fun provideGetBlockStatsFeeUseCase(
        bsvStatusRepository: BsvStatusRepository
    ): GetBlockStatsFeeUseCase = GetBlockStatsFeeUseCaseImpl(
        bsvStatusRepository
    )

    @Provides
    fun provideEstimateBaemailMessageTxSize(): EstimateBaemailMessageTxSize =
        EstimateBaemailMessageTxSizeImpl()

    @Provides
    fun provideGetSavedMessageDraftUseCase(
        baemailRepository: BaemailRepository,
        getActivePaymailUseCase: GetActivePaymailUseCase
    ): GetSavedMessageDraftUseCase = GetSavedMessageDraftUseCaseImpl(
        getActivePaymailUseCase,
        baemailRepository
    )

    @Provides
    fun provideSaveBaemailDraftUseCase(
        baemailRepository: BaemailRepository,
        getActivePaymailUseCase: GetActivePaymailUseCase
    ): SaveBaemailDraftUseCase = SaveBaemailDraftUseCaseImpl(
        getActivePaymailUseCase,
        baemailRepository
    )

    @Provides
    fun provideDiscardBaemailDraftUseCase(
        baemailRepository: BaemailRepository,
        getActivePaymailUseCase: GetActivePaymailUseCase
    ): DiscardBaemailDraftUseCase = DiscardBaemailDraftUseCaseImpl(
        getActivePaymailUseCase,
        baemailRepository
    )

    @Provides
    fun provideApplyNewReplyDraftUseCase(
        baemailRepository: BaemailRepository,
        getActivePaymailUseCase: GetActivePaymailUseCase
    ): ApplyNewReplyDraftUseCase = ApplyNewReplyDraftUseCaseImpl(
        getActivePaymailUseCase,
        baemailRepository
    )

    @Provides
    fun provideSendBaemailMessageUseCase(
        coinRepository: CoinRepository,
        secureDataRepository: SecureDataRepository,
        nodeRuntimeRepository: NodeRuntimeRepository,
        bsvStatusRepository: BsvStatusRepository,
        paymailInfoRepository: PaymailInfoRepository,
        signMessageUseCase: SignMessageUseCase,
        getActivePaymailUseCase: GetActivePaymailUseCase,
        getFundingCoinsUseCase: GetFundingCoinsUseCase,
        checkPaymailExistsUseCase: CheckPaymailExistsUseCase,
        getBlockStatsFeeUseCase: GetBlockStatsFeeUseCase,
        getExchangeRateUseCase: GetExchangeRateUseCase,
    ): SendBaemailMessageUseCase = SendBaemailMessageUseCaseImpl(
        coinRepository,
        secureDataRepository,
        nodeRuntimeRepository,
        bsvStatusRepository,
        paymailInfoRepository,
        signMessageUseCase,
        getActivePaymailUseCase,
        getFundingCoinsUseCase,
        checkPaymailExistsUseCase,
        getBlockStatsFeeUseCase,
        getExchangeRateUseCase
    )

    @Provides
    fun provideValidatePaymailUseCase(
        paymailInfoRepository: PaymailInfoRepository,
        nodeRuntimeRepository: NodeRuntimeRepository
    ): ValidatePaymailUseCase = ValidatePaymailUseCaseImpl(
        paymailInfoRepository,
        nodeRuntimeRepository
    )

    @Provides
    fun provideFindFundingWalletSeedUseCase(
        seedWordListUseCase: GetSeedWordListUseCase,
        nodeRuntimeRepository: NodeRuntimeRepository
    ): FindFundingWalletSeedUseCase = FindFundingWalletSeedUseCaseImpl(
        seedWordListUseCase,
        nodeRuntimeRepository
    )

    @Provides
    fun provideCoinInfoUseCaseImpl(
        coinSyncStatusUseCase: CoinSyncStatusUseCase,
        secureDataRepository: SecureDataRepository,
        getFundingCoinsUseCase: GetFundingCoinsUseCase,
        coinRepository: CoinRepository,
        notifyCoinsUsedByUserLiveData: NotifyCoinsUsedByUserLiveData,
        bsvStatusRepository: BsvStatusRepository,
        authRepository: AuthRepository
    ): CoinInfoUseCase = CoinInfoUseCaseImpl(
        coinSyncStatusUseCase,
        secureDataRepository,
        getFundingCoinsUseCase,
        coinRepository,
        notifyCoinsUsedByUserLiveData,
        bsvStatusRepository,
        authRepository
    )

    @Provides
    fun provideNormalizePkiPathUseCase(): NormalizePkiPathUseCase = NormalizePkiPathUseCaseImpl()

    @Provides
    fun provideCheckIsValidMnemonicUseCase(
        nodeRuntimeRepository: NodeRuntimeRepository
    ): CheckIsValidMnemonicUseCase = CheckIsValidMnemonicUseCaseImpl(nodeRuntimeRepository)

    @Provides
    fun provideValidateWalletInfoUseCase(
        nodeRuntimeRepository: NodeRuntimeRepository
    ): ValidateWalletInfoUseCase = ValidateWalletInfoUseCaseImpl(nodeRuntimeRepository)

    @Provides
    fun provideGetHighestUsedCoinIndexUseCase(
        coinRepository: CoinRepository
    ): GetHighestUsedAddressIndexUseCase = GetHighestUsedAddressIndexUseCaseImpl(coinRepository)

    @Provides
    fun provideGetSecretsUseCase(
        secureDataRepository: SecureDataRepository
    ): GetSecretsUseCase = GetSecretsUseCaseImpl(secureDataRepository)
}