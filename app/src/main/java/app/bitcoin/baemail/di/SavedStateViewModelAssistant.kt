package app.bitcoin.baemail.di

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel

interface SavedStateViewModelAssistant<T : ViewModel> {
    fun create(savedStateHandle: SavedStateHandle): T
}