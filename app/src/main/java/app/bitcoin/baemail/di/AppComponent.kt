package app.bitcoin.baemail.di

import app.bitcoin.baemail.core.presentation.App
import app.bitcoin.baemail.di.helper.CoilHttpClient
import app.bitcoin.baemail.core.data.work.WorkerFactoryFactory
import app.bitcoin.baemail.core.presentation.NodeModel
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    UseCaseModule::class,
    WorkerModule::class,
    ActivityBindingModule::class,
    ViewModelModule::class,
    AndroidInjectionModule::class
])
abstract class AppComponent {

    abstract fun inject(app: App)

    //abstract fun provideViewModelFactory(): ViewModelProvider.Factory

    abstract fun providerWorkerFactoryFactory(): WorkerFactoryFactory

    abstract fun provideCoilHttpClient(): CoilHttpClient

    abstract fun provideNodeModel(): NodeModel

}