package app.bitcoin.baemail.di.helper

import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import app.bitcoin.baemail.di.SavedStateViewModelAssistant
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import javax.inject.Provider
import javax.inject.Singleton


class UniversalSavedStateViewModelFactory @AssistedInject constructor(
    @Assisted savedStateRegistryOwner: SavedStateRegistryOwner,
    private val creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<SavedStateViewModelAssistant<*>>>
) : AbstractSavedStateViewModelFactory(savedStateRegistryOwner, null) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        val creator = creators[modelClass] ?: creators.entries.firstOrNull {
            modelClass.isAssignableFrom(it.key)
        }?.value ?: throw IllegalArgumentException("unknown model class $modelClass")
        try {
            @Suppress("UNCHECKED_CAST")
            return creator.get().create(handle) as T
        } catch (e: Exception) {
            throw RuntimeException("modelClass: $modelClass ...tried to to cast: $creator", e)
        }
    }

    @Singleton
    @AssistedFactory
    interface Assistant : ViewModelProviderAssistant {
        override fun create(savedStateRegistryOwner: SavedStateRegistryOwner): UniversalSavedStateViewModelFactory
    }
}



