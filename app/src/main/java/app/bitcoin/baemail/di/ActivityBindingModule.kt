package app.bitcoin.baemail.di

import app.bitcoin.baemail.core.presentation.SplashActivity
import app.bitcoin.baemail.fundingWallet.core.presentation.CoinActivity
import app.bitcoin.baemail.fundingWallet.SweepActivity
import app.bitcoin.baemail.fundingWallet.TopUpActivity
import app.bitcoin.baemail.intercept.InterceptActivity
import app.bitcoin.baemail.logs.presentation.LogsActivity
import app.bitcoin.baemail.paymail.presentation.PaymailActivity
import app.bitcoin.baemail.core.presentation.CentralActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun splashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun centralActivity(): CentralActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun paymailActivity(): PaymailActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun topUpActivity(): TopUpActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun sweepActivity(): SweepActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun coinActivity(): CoinActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun logsActivity(): LogsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun interceptActivity(): InterceptActivity

}