package app.bitcoin.baemail.di

import android.content.Context
import android.os.Build.VERSION.SDK_INT
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.asFlow
import androidx.room.Room
import app.bitcoin.baemail.BuildConfig
import app.bitcoin.baemail.core.data.AppConfigRepositoryImpl
import app.bitcoin.baemail.core.data.BsvStatusRepositoryImpl
import app.bitcoin.baemail.core.data.wallet.CoinAddressDerivationHelper
import app.bitcoin.baemail.core.data.CoinRepositoryImpl
import app.bitcoin.baemail.core.data.SecureDataRepositoryImpl
import app.bitcoin.baemail.core.data.wallet.AppUtilDiskDAO
import app.bitcoin.baemail.core.data.wallet.sending.SignMessageHelper
import app.bitcoin.baemail.core.data.wallet.UnusedCoinAddressHelper
import app.bitcoin.baemail.core.domain.repository.AppConfigRepository
import app.bitcoin.baemail.core.domain.repository.BsvStatusRepository
import app.bitcoin.baemail.core.domain.repository.CoinRepository
import app.bitcoin.baemail.di.helper.CoilHttpClient
import app.bitcoin.baemail.core.data.wallet.sending.PaymailExistsHelper
import app.bitcoin.baemail.logs.domain.LogsRepository
import app.bitcoin.baemail.message.domain.BaemailRepository
import app.bitcoin.baemail.message.data.BaemailStateStore
import app.bitcoin.baemail.core.data.net.AppApiService
import app.bitcoin.baemail.p2p.InformP2pChangeHelper
import app.bitcoin.baemail.p2p.P2pStallRepository
import app.bitcoin.baemail.core.data.wallet.sending.BsvAliasCapabilitiesHelper
import app.bitcoin.baemail.core.data.PaymailInfoRepositoryImpl
import app.bitcoin.baemail.core.data.AuthRepositoryImpl
import app.bitcoin.baemail.core.data.NodeRuntimeRepositoryImpl
import app.bitcoin.baemail.core.data.util.SecureDataSource
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.data.room.APP_DB_NAME
import app.bitcoin.baemail.core.data.room.AppDatabase
import app.bitcoin.baemail.core.data.room.TWETCH_DB_NAME
import app.bitcoin.baemail.core.data.room.converters.DbConverters
import app.bitcoin.baemail.twetch.db.SafeEmptyingMigration
import app.bitcoin.baemail.twetch.db.TwetchDatabase
import app.bitcoin.baemail.twetch.ui.*
import app.bitcoin.baemail.core.data.util.DeepLinkHelper
import app.bitcoin.baemail.core.data.nodejs.AndroidWorkHandlerProvider
import app.bitcoin.baemail.core.data.nodejs.AndroidWorkHandlerProviderImpl
import app.bitcoin.baemail.core.data.wallet.AppStateLiveData
import app.bitcoin.baemail.core.data.wallet.BlockStatsHelper
import app.bitcoin.baemail.core.data.wallet.BlockchainDataSource
import app.bitcoin.baemail.core.data.wallet.ChainSync
import app.bitcoin.baemail.core.data.nodejs.CoinRefreshRequestHelper
import app.bitcoin.baemail.core.data.wallet.DynamicChainSync
import app.bitcoin.baemail.core.data.wallet.ExchangeRateHelper
import app.bitcoin.baemail.core.data.wallet.NewBlockDetector
import app.bitcoin.baemail.core.data.nodejs.NodeConnectedLiveData
import app.bitcoin.baemail.core.data.nodejs.NodeRuntimeInterface
import app.bitcoin.baemail.core.data.wallet.NotifyCoinsUsedByUserLiveData
import app.bitcoin.baemail.core.data.nodejs.NotifyNodeForegroundLiveData
import app.bitcoin.baemail.core.data.util.AppForegroundLiveData
import app.bitcoin.baemail.core.data.util.ConnectivityLiveData
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.data.util.SeedWordList
import app.bitcoin.baemail.core.data.util.ThrottledExecQueue
import app.bitcoin.baemail.core.data.util.appUtilPrefs
import app.bitcoin.baemail.core.data.util.baemailCachingPrefs
import app.bitcoin.baemail.core.data.util.baemailMessageDraftPrefs
import app.bitcoin.baemail.core.data.util.p2pPrefs
import app.bitcoin.baemail.core.data.util.replPrefs
import app.bitcoin.baemail.core.data.util.twetchAuthPrefs
import app.bitcoin.baemail.core.data.wallet.WocFullSyncHelper
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import app.bitcoin.baemail.core.domain.repository.PaymailInfoRepository
import app.bitcoin.baemail.core.domain.repository.SecureDataRepository
import app.bitcoin.baemail.core.domain.usecases.CoinInfoUseCase
import app.bitcoin.baemail.core.presentation.NodeModel
import app.bitcoin.baemail.core.presentation.util.PermissionStateHelper
import app.bitcoin.baemail.core.presentation.view.ExoFactory
import app.bitcoin.baemail.core.presentation.view.VideoClipManager
import app.bitcoin.baemail.logs.data.LogsRepositoryImpl
import app.bitcoin.baemail.message.data.BaemailDataSource
import app.bitcoin.baemail.message.data.BaemailRepositoryImpl
import app.bitcoin.baemail.message.data.FetchMessagesHelper
import app.bitcoin.baemail.message.data.MessageDraftStore
import app.bitcoin.baemail.message.data.MessageIndex
import app.bitcoin.baemail.repl.data.SavedReplInputRepositoryImpl
import app.bitcoin.baemail.repl.domain.repository.SavedReplInputRepository
import app.bitcoin.node.NodeConnectionHelper
import app.local.p2p.serviceDiscovery.NearbyConnectionsHelper
import coil.ImageLoader
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.decode.VideoFrameDecoder
import coil.disk.DiskCache
import coil.request.CachePolicy
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.io.File
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.inject.Provider
import javax.inject.Singleton

@Module
class AppModule(
    private val context: Context
) {

    @Provides
    @Singleton
    fun getContext(): Context = context

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val builder = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setPrettyPrinting()
            .setLenient()
        return builder.create()
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(context: Context): OkHttpClient {

        val httpClient = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .cache(
                Cache(
                    directory = File(context.cacheDir, "http_cache"),
                    maxSize = 50L * 1024L * 1024L // 50 MiB
                )
            )

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            // Careful when changing the log level to Level.BODY - this causes an OOM error when
            // processing large requests.
            loggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
            //loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(loggingInterceptor)
        }

        return httpClient.build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    @Provides
    @Singleton
    internal fun provideApiService(retrofit: Retrofit): AppApiService {
        return retrofit.create(AppApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideCoilHttpClient(okHttpClient: OkHttpClient): CoilHttpClient {
        return CoilHttpClient(okHttpClient)
    }

    @Provides
    @Singleton
    fun provideCoilLoader(
        context: Context,
        coilHttpClient: CoilHttpClient
    ): ImageLoader {
        return ImageLoader.Builder(context)
            .crossfade(false)
            .okHttpClient(coilHttpClient.httpClient)
            .memoryCachePolicy(CachePolicy.ENABLED)
            .diskCachePolicy(CachePolicy.ENABLED)
            .diskCache {
                DiskCache.Builder()
                    .directory(context.cacheDir.resolve("image_cache"))
                    .build()
            }
            .components {
                if (SDK_INT >= 28) {
                    add(ImageDecoderDecoder.Factory())
                } else {
                    add(GifDecoder.Factory())
                }
                add(VideoFrameDecoder.Factory())
            }
            .build()
    }

    @Provides
    @Singleton
    fun provideConnectivityLiveData(context: Context): ConnectivityLiveData {
        return ConnectivityLiveData().apply {
            initialize(context)
        }
    }

    @Provides
    @Singleton
    fun provideAppForegroundLiveData(): AppForegroundLiveData {
        return AppForegroundLiveData.get()
    }

    @Provides
    @Singleton
    fun provideCoroutineUtil(): CoroutineUtil {
        return CoroutineUtil()
    }

    @Provides
    fun provideSeedWordList(
        context: Context,
        coroutineUtil: CoroutineUtil
    ): SeedWordList {
        return SeedWordList(
            context.resources,
            coroutineUtil.ioDispatcher
        )
    }

    @Provides
    @Singleton
    fun provideAppStateLiveData(
        context: Context,
        coroutineUtil: CoroutineUtil
    ): AppStateLiveData {
        return AppStateLiveData(
            context,
            coroutineUtil
        )
    }

    @Provides
    @Singleton
    fun provideAuthRepository(
        coroutineUtil: CoroutineUtil,
        appStateLiveData: AppStateLiveData,
        secureDataSource: SecureDataSource
    ): AuthRepository {
        return AuthRepositoryImpl(
            coroutineUtil,
            appStateLiveData,
            secureDataSource
        )
    }

    @Provides
    @Singleton
    fun provideBaemailStateHelper(
        appContext: Context,
        gson: Gson
    ): BaemailStateStore {
        return BaemailStateStore(
            appContext.baemailCachingPrefs,
            gson
        )
    }

    @Provides
    @Singleton
    fun provideMessageDraftStore(
        appContext: Context
    ): MessageDraftStore {
        return MessageDraftStore(appContext.baemailMessageDraftPrefs)
    }

    @Provides
    @Singleton
    fun provideMessageIndex(): MessageIndex = MessageIndex(20)

    @Provides
    @Singleton
    fun provideBaemailDataSource(
        gson: Gson,
        apiService: AppApiService,
        appDatabase: AppDatabase,
        secureDataSource: SecureDataSource,
        nodeRuntimeRepository: NodeRuntimeRepository,
        baemailStateStore: BaemailStateStore,
    ): BaemailDataSource = BaemailDataSource(
        gson,
        apiService,
        20,
        appDatabase.decryptedBaemailMessageDao(),
        secureDataSource,
        nodeRuntimeRepository,
        baemailStateStore
    )

    @Provides
    @Singleton
    fun provideFetchMessagesHelper(
        appDatabase: AppDatabase,
        index: MessageIndex,
        baemailDataSource: BaemailDataSource
    ): FetchMessagesHelper = FetchMessagesHelper(
        appDatabase.decryptedBaemailMessageDao(),
        index,
        baemailDataSource
    )

    @Provides
    @Singleton
    fun provideBaemailRepository(
        coroutineUtil: CoroutineUtil,
        appDatabase: AppDatabase,
        messageDraftStore:MessageDraftStore,
        baemailDataSource: BaemailDataSource,
        fetchMessagesHelper: FetchMessagesHelper,
        index: MessageIndex
    ): BaemailRepository {
        return BaemailRepositoryImpl(
            coroutineUtil,
            appDatabase.decryptedBaemailMessageDao(),
            messageDraftStore,
            baemailDataSource,
            fetchMessagesHelper,
            index
        )
    }

    @Provides
    @Singleton
    fun provideTwetchAuthStore(
        context: Context,
        nodeRuntimeRepository: NodeRuntimeRepository,
        gson: Gson
    ): TwetchAuthStore {
        val twetchAuthPrefs: DataStore<Preferences> = context.twetchAuthPrefs

        return TwetchAuthStore(
            twetchAuthPrefs,
            nodeRuntimeRepository,
            gson
        )
    }

    @Provides
    @Singleton
    fun provideTwetchChatInfoStore(
        context: Context,
        coroutineUtil: CoroutineUtil,
        nodeRuntimeRepository: NodeRuntimeRepository,
        gson: Gson
    ): TwetchChatInfoStore {
        return TwetchChatInfoStore(
            context,
            coroutineUtil,
            nodeRuntimeRepository,
            gson
        )
    }

    @Provides
    @Singleton
    fun provideTwetchUserProfileBaseStore(
        context: Context,
        coroutineUtil: CoroutineUtil,
        nodeRuntimeRepository: NodeRuntimeRepository,
        twetchDatabase: TwetchDatabase,
        gson: Gson
    ): TwetchUserProfileBaseStore {
        return TwetchUserProfileBaseStore(
            context,
            coroutineUtil,
            nodeRuntimeRepository,
            twetchDatabase,
            gson
        )
    }


    @Provides
    @Singleton
    fun provideSavedReplInputStore(
        context: Context,
        gson: Gson
    ): SavedReplInputRepository {
        return SavedReplInputRepositoryImpl(
            context.replPrefs,
            gson
        )
    }

    @Provides
    @Singleton
    fun provideTwetchChatMessageStore(
        context: Context,
        nodeRuntimeRepository: NodeRuntimeRepository,
        coroutineUtil: CoroutineUtil,
        gson: Gson,
        twetchDatabase: TwetchDatabase
    ): TwetchChatMessageStore {
        return TwetchChatMessageStore(
            context,
            coroutineUtil,
            nodeRuntimeRepository,
            gson,
            twetchDatabase.twetchChatMessagesDao()
        )
    }

    @Provides
    @Singleton
    fun provideTwetchPostCache(): TwetchPostCache {
        return TwetchPostCache()
    }

    @Provides
    @Singleton
    fun provideTwetchPostUserCache(): TwetchPostUserCache {
        return TwetchPostUserCache()
    }

    @Provides
    @Singleton
    fun provideTwetchNotificationsCache(): TwetchNotificationsCache {
        return TwetchNotificationsCache()
    }

    @Provides
    @Singleton
    fun provideTwetchFilesMimeTypeHelper(
        appContext: Context,
        coroutineUtil: CoroutineUtil,
        blockchainDataSource: BlockchainDataSource
    ): TwetchFilesMimeTypeHelper {
        return TwetchFilesMimeTypeHelper(
            appContext,
            coroutineUtil,
            blockchainDataSource
        )
    }

    @Provides
    @Singleton
    fun provideTwetchPostListStore(
        appContext: Context,
        coroutineUtil: CoroutineUtil,
        nodeRuntimeRepository: NodeRuntimeRepository,
        twetchDatabase: TwetchDatabase,
        postCache: TwetchPostCache,
        userCache: TwetchPostUserCache,
        twetchFilesMimeTypeHelper: TwetchFilesMimeTypeHelper,
        gson: Gson
    ): TwetchPostListStore {
        return TwetchPostListStore(
            appContext,
            coroutineUtil,
            nodeRuntimeRepository,
            twetchDatabase,
            postCache,
            userCache,
            twetchFilesMimeTypeHelper,
            gson
        )
    }

    @Provides
    @Singleton
    fun provideTwetchPostDetailsStore(
        appContext: Context,
        coroutineUtil: CoroutineUtil,
        nodeRuntimeRepository: NodeRuntimeRepository,
        twetchDatabase: TwetchDatabase,
        postCache: TwetchPostCache,
        gson: Gson,
        userCache: TwetchPostUserCache,
        twetchFilesMimeTypeHelper: TwetchFilesMimeTypeHelper
    ): TwetchPostDetailsStore {
        return TwetchPostDetailsStore(
            appContext,
            coroutineUtil,
            nodeRuntimeRepository,
            twetchDatabase,
            postCache,
            gson,
            userCache,
            twetchFilesMimeTypeHelper
        )
    }

    @Provides
    @Singleton
    fun provideTwetchNotificationsStore(
        appContext: Context,
        coroutineUtil: CoroutineUtil,
        nodeRuntimeRepository: NodeRuntimeRepository,
        twetchDatabase: TwetchDatabase,
        postCache: TwetchPostCache,
        gson: Gson,
        userCache: TwetchPostUserCache,
        notificationsCache: TwetchNotificationsCache,
        twetchFilesMimeTypeHelper: TwetchFilesMimeTypeHelper
    ): TwetchNotificationsStore {
        return TwetchNotificationsStore(
            appContext,
            coroutineUtil,
            nodeRuntimeRepository,
            twetchDatabase,
            postCache,
            gson,
            userCache,
            notificationsCache,
            twetchFilesMimeTypeHelper
        )
    }



    @Provides
    @Singleton
    fun provideVideoClipManager(
        context: Context,
        coroutineUtil: CoroutineUtil
    ): VideoClipManager {
        return VideoClipManager(ExoFactory(context), coroutineUtil)
    }


    @Provides
    @Singleton
    fun provideTwetchRepository(
        context: Context,
        coroutineUtil: CoroutineUtil,
        appApiService: AppApiService,
        appStateLD: AppStateLiveData,
        nodeConnectedLiveData: NodeConnectedLiveData,
        appDatabase: AppDatabase,
        twetchDatabase: TwetchDatabase,
        nodeRuntimeRepository: NodeRuntimeRepository,
        twetchAuthStore: TwetchAuthStore,
        twetchChatInfoStore: TwetchChatInfoStore,
        twetchChatMessageStore: TwetchChatMessageStore,
        userProfileBaseStore: TwetchUserProfileBaseStore,
        postListStore: TwetchPostListStore,
        postDetailsStore: TwetchPostDetailsStore,
        notificationsStore: TwetchNotificationsStore,
        postCache: TwetchPostCache,
        postUserCache: TwetchPostUserCache,
        coinManagementUseCase: CoinManagementUseCase,
        connectivityLiveData: ConnectivityLiveData,
        gson: Gson
    ): TwetchRepository {
        return TwetchRepository(
            context,
            coroutineUtil,
            appApiService,
            appStateLD,
            nodeConnectedLiveData,
            appDatabase,
            twetchDatabase,
            nodeRuntimeRepository,
            twetchAuthStore,
            twetchChatInfoStore,
            twetchChatMessageStore,
            userProfileBaseStore,
            postListStore,
            postDetailsStore,
            notificationsStore,
            postCache,
            postUserCache,
            gson,
            coinManagementUseCase,
            connectivityLiveData
        )
    }




    @Provides
    @Singleton
    fun provideAppDatabase(
        context: Context,
        notifyCoinsUsedByUserLiveData: NotifyCoinsUsedByUserLiveData,
        gson: Gson
    ): AppDatabase {
        val db = Room.databaseBuilder(context, AppDatabase::class.java, APP_DB_NAME)
            .addTypeConverter(DbConverters(gson))
            .fallbackToDestructiveMigration()
            .build()

        db.coinDao().injectNotifyCoinsChanged { paymail ->
            notifyCoinsUsedByUserLiveData.ping(paymail)
        }

        return db
    }


    @Provides
    @Singleton
    fun provideTwetchDatabase(
        context: Context,
        coroutineUtil: CoroutineUtil,
        gson: Gson
    ): TwetchDatabase {
        return Room.databaseBuilder(context, TwetchDatabase::class.java, TWETCH_DB_NAME)
            .addTypeConverter(DbConverters(gson))
            .addMigrations(
                SafeEmptyingMigration(
                    context,
                    coroutineUtil,
                    1,
                    8
                ),
                SafeEmptyingMigration(
                    context,
                    coroutineUtil,
                    2,
                    8
                ),
                SafeEmptyingMigration(
                    context,
                    coroutineUtil,
                    3,
                    8
                ),
                SafeEmptyingMigration(
                    context,
                    coroutineUtil,
                    4,
                    8
                ),
                SafeEmptyingMigration(
                    context,
                    coroutineUtil,
                    5,
                    8
                ),
                SafeEmptyingMigration(
                    context,
                    coroutineUtil,
                    6,
                    8
                ),
                SafeEmptyingMigration(
                    context,
                    coroutineUtil,
                    7,
                    8
                )
            )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideBaseListAdapterFactory(
        context: Context,
        twetchFilesMimeHelper: TwetchFilesMimeTypeHelper
    ): BaseListAdapterFactory {
        return BaseListAdapterFactory(
            context,
            twetchFilesMimeHelper
        )
    }


    @Provides
    @Singleton
    fun provideCoinRefreshRequestHelper(): CoinRefreshRequestHelper {
        return CoinRefreshRequestHelper()
    }

    @Provides
    @Singleton
    fun provideAndroidWorkHandlerProvider(
        secureDataSource: SecureDataSource,
        informP2pChangeHelper: InformP2pChangeHelper,
        appDatabase: AppDatabase,
        coinRefreshRequestHelper: CoinRefreshRequestHelper,
        appStateLD: AppStateLiveData,
        paymailInfoRepository: PaymailInfoRepository,
        signMessageHelper: SignMessageHelper
    ): AndroidWorkHandlerProvider {
        return AndroidWorkHandlerProviderImpl(
            secureDataSource,
            informP2pChangeHelper,
            appDatabase,
            coinRefreshRequestHelper,
            appStateLD,
            paymailInfoRepository,
            signMessageHelper
        )
    }

    @Provides
    @Singleton
    fun provideNodeRuntimeRepository(
        nodeRuntimeInterface: NodeRuntimeInterface,
        gson: Gson,
    ): NodeRuntimeRepository {
        return NodeRuntimeRepositoryImpl(
            nodeRuntimeInterface,
            gson
        )
    }

    @Provides
    @Singleton
    fun provideNodeConnectionHelper(
        context: Context,
        foregroundLiveData: AppForegroundLiveData,
        notifyNodeForegroundLiveData: NotifyNodeForegroundLiveData,
        nodeConnectedLiveData: NodeConnectedLiveData
    ): NodeConnectionHelper {

        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            Timber.e(throwable)
        }
        val backgroundDispatcher: CoroutineDispatcher by lazy {
            Executors.newSingleThreadExecutor().asCoroutineDispatcher()
        }

        val scope = CoroutineScope(SupervisorJob() + backgroundDispatcher + exceptionHandler)


        return NodeConnectionHelper(
            context,
            scope,
            foregroundLiveData,
            notifyNodeForegroundLiveData,
            nodeConnectedLiveData
        )
    }

    @Provides
    @Singleton
    fun provideNodeRuntimeInterface(
        nodeConnectionHelper: NodeConnectionHelper,
        nodeConnectedLiveData: NodeConnectedLiveData
    ): NodeRuntimeInterface {
        return NodeRuntimeInterface(
            nodeConnectionHelper,
            nodeConnectedLiveData
        )
    }

    @Provides
    @Singleton
    fun provideSecureDataSource(
        context: Context,
        coroutineUtil: CoroutineUtil
    ): SecureDataSource {
        val ds = SecureDataSource(context, coroutineUtil)
        ds.setup()
        return ds
    }

    @Provides
    @Singleton
    fun provideBlockchainDataSource(
        context: Context,
        apiService: AppApiService,
        execQueue: ThrottledExecQueue,
        gson: Gson,
        coroutineUtil: CoroutineUtil
    ): BlockchainDataSource {
        return BlockchainDataSource(context, apiService, execQueue, gson, coroutineUtil)
    }


    @Singleton
    @Provides
    fun provideCoinAddressDerivationHelper(
        appDatabase: AppDatabase,
        nodeRuntimeRepository: NodeRuntimeRepository
    ): CoinAddressDerivationHelper =
        CoinAddressDerivationHelper(
            appDatabase.cachedDerivedAddressDao(),
            nodeRuntimeRepository
        )


    @Provides
    @Singleton
    fun provideThrottledExecQueue(
        coroutineUtil: CoroutineUtil
    ): ThrottledExecQueue {
        return ThrottledExecQueue(3, coroutineUtil.appScope).apply {
            start()
        }
    }

    @Provides
    fun provideFullSyncHelper(
        blockchainDataSource: BlockchainDataSource,
        appDatabase: AppDatabase,
        coinAddressDerivationHelper: CoinAddressDerivationHelper,
        appUtilDiskDAO: AppUtilDiskDAO
    ): WocFullSyncHelper {
        return WocFullSyncHelper(
            blockchainDataSource,
            appDatabase.coinDao(),
            coinAddressDerivationHelper,
            appUtilDiskDAO
        )
    }

    @Provides
    fun provideNewBlockDetector(
        coroutineUtil: CoroutineUtil,
        gson: Gson
    ): NewBlockDetector {
        return NewBlockDetector(
            coroutineUtil,
            gson
        )
    }

    @Provides
    fun provideChainSync(
        context: Context,
        appDatabase: AppDatabase,
        coroutineUtil: CoroutineUtil,
        blockchainDataSource: BlockchainDataSource,
        fullSyncHelper: WocFullSyncHelper,
        newBlockDetector: NewBlockDetector
    ): ChainSync {
        return ChainSync(
            context,
            appDatabase,
            coroutineUtil,
            blockchainDataSource,
            fullSyncHelper,
            newBlockDetector
        )
    }

    @Provides
    @Singleton
    fun provideDynamicChainSync(
        appStateLD: AppStateLiveData,
        appForegroundLD: AppForegroundLiveData,
        connectivityLiveData: ConnectivityLiveData,
        notifyNodeForegroundLiveData: NotifyNodeForegroundLiveData,
        coinRefreshRequestHelper: CoinRefreshRequestHelper,
        coroutineUtil: CoroutineUtil,
        secureDataSource: SecureDataSource,
        chainSyncProvider: Provider<ChainSync>
    ): DynamicChainSync {


        return DynamicChainSync(
            appStateLD,
            appForegroundLD,
            connectivityLiveData,
            notifyNodeForegroundLiveData,
            coinRefreshRequestHelper,
            coroutineUtil,
            secureDataSource,
            chainSyncProvider
        ).apply {
            setup()
        }
    }

    @Singleton
    @Provides
    fun provideNotifyCoinsUsedByUserLiveData(): NotifyCoinsUsedByUserLiveData {
        return NotifyCoinsUsedByUserLiveData()
    }

    @Provides
    @Singleton
    fun provideNotifyNodeForegroundLiveData(): NotifyNodeForegroundLiveData {
        return NotifyNodeForegroundLiveData()
    }

    @Singleton
    @Provides
    fun provideNodeConnectedLiveData(): NodeConnectedLiveData {
        return NodeConnectedLiveData()
    }

    @Singleton
    @Provides
    fun provideExchangeRateHelper(
        appUtilDiskDAO: AppUtilDiskDAO,
        coroutineUtil: CoroutineUtil,
        blockchainDataSource: BlockchainDataSource,
        connectivityLD: ConnectivityLiveData,
        nodeConnectedLiveData: NodeConnectedLiveData
    ): ExchangeRateHelper {
        return ExchangeRateHelper(
            appUtilDiskDAO,
            blockchainDataSource,
            connectivityLD,
            nodeConnectedLiveData,
            coroutineUtil
        ).apply {
            setup()
        }
    }

    @Singleton
    @Provides
    fun provideBlockStatsHelper(
        appUtilDiskDAO: AppUtilDiskDAO,
        blockchainDataSource: BlockchainDataSource,
        coroutineUtil: CoroutineUtil,
        chainSync: DynamicChainSync,
        gson: Gson
    ): BlockStatsHelper = BlockStatsHelper(
        appUtilDiskDAO,
        blockchainDataSource,
        coroutineUtil,
        chainSync,
        gson
    ).apply { setup() }

    @Singleton
    @Provides
    fun provideCoinManagementUseCase(
        authRepository: AuthRepository,
        appDatabase: AppDatabase,
        exchangeRateHelper: ExchangeRateHelper,
        chainSync: DynamicChainSync,
        connectivityLiveData: ConnectivityLiveData,
        coroutineUtil: CoroutineUtil
    ): CoinManagementUseCase {
        return CoinManagementUseCase(
            authRepository,
            appDatabase.coinDao(),
            exchangeRateHelper,
            chainSync,
            connectivityLiveData,
            coroutineUtil
        )
    }

    @Provides
    @Singleton
    fun provideBsvAliasCapabilitiesHelper(
        apiService: AppApiService,
        gson: Gson
    ): BsvAliasCapabilitiesHelper {
        return BsvAliasCapabilitiesHelper(apiService, gson)
    }

    @Provides
    fun provideSignMessageHelper(): SignMessageHelper = SignMessageHelper()

    @Provides
    fun providePaymailExistsHelper(
        apiService: AppApiService,
        bsvAliasCapabilitiesHelper: BsvAliasCapabilitiesHelper,
        coroutineUtil: CoroutineUtil,
        gson: Gson
    ): PaymailExistsHelper = PaymailExistsHelper(
        apiService,
        bsvAliasCapabilitiesHelper,
        gson,
        coroutineUtil
    )

    @Provides
    @Singleton
    fun providePaymailInfoHelper(
        apiService: AppApiService,
        bsvAliasCapabilitiesHelper: BsvAliasCapabilitiesHelper,
        paymailExistsHelper: PaymailExistsHelper,
        gson: Gson,
        coroutineUtil: CoroutineUtil,
    ): PaymailInfoRepository {
        return PaymailInfoRepositoryImpl(
            apiService,
            bsvAliasCapabilitiesHelper,
            paymailExistsHelper,
            gson,
            coroutineUtil
        )
    }

    @Provides
    @Singleton
    fun provideDeepLinkHelper(): DeepLinkHelper {
        return DeepLinkHelper()
    }

    @Provides
    @Singleton
    fun provideNearbyConnectionsHelper(
        context: Context
    ): NearbyConnectionsHelper {
        return NearbyConnectionsHelper(context)
    }

    @Provides
    @Singleton
    fun provideP2pStallRepository(
        coroutineUtil: CoroutineUtil,
        gson: Gson,
        authRepository: AuthRepository,
        nodeRuntimeRepository: NodeRuntimeRepository,
        appForegroundLiveData: AppForegroundLiveData,
        nodeConnectedLiveData: NodeConnectedLiveData,
        secureDataSource: SecureDataSource,
        informP2pChangeHelper: InformP2pChangeHelper,
        nearbyConnectionsHelper: NearbyConnectionsHelper,
        permissionStateHelper: PermissionStateHelper
    ): P2pStallRepository {

        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            Timber.e(throwable)
        }
        val scope = CoroutineScope(SupervisorJob() +
                coroutineUtil.ioDispatcher +
                exceptionHandler)


        val appForegroundFlow = MutableStateFlow(appForegroundLiveData.value ?: false)
        scope.launch {
            appForegroundLiveData.asFlow().collect { isForeground: Boolean ->
                appForegroundFlow.value = isForeground
            }
        }


        return P2pStallRepository(
            context,
            coroutineUtil,
            gson,
            authRepository,
            nodeRuntimeRepository,
            nearbyConnectionsHelper,
            permissionStateHelper,
            nodeConnectedLiveData,
            secureDataSource,
            informP2pChangeHelper,
            context.p2pPrefs,
            scope,
            appForegroundFlow
        )
    }

    @Singleton
    @Provides
    fun provideInformP2pChangeHelper(): InformP2pChangeHelper {
        return InformP2pChangeHelper()
    }


    @Singleton
    @Provides
    fun providePermissionStateHelper(): PermissionStateHelper {
        return PermissionStateHelper()
    }


    @Singleton
    @Provides
    fun provideAppConfigRepository(
        secureDataSource: SecureDataSource
    ): AppConfigRepository = AppConfigRepositoryImpl(secureDataSource)


    @Singleton
    @Provides
    fun provideLogsRepository(
        coroutineUtil: CoroutineUtil
    ): LogsRepository {
        return LogsRepositoryImpl(
            context,
            coroutineUtil
        )
    }

    @Singleton
    @Provides
    fun provideCoinRepository(
        authRepository: AuthRepository,
        appDatabase: AppDatabase,
        chainSync: DynamicChainSync,
        unusedCoinAddressHelper: UnusedCoinAddressHelper
    ): CoinRepository =
        CoinRepositoryImpl(
            authRepository,
            appDatabase.coinDao(),
            chainSync,
            unusedCoinAddressHelper
        )

    @Singleton
    @Provides
    fun provideBsvStatusRepository(
        blockStatsHelper: BlockStatsHelper,
        exchangeRateHelper: ExchangeRateHelper,
        dynamicChainSync: DynamicChainSync,
        blockchainDataSource: BlockchainDataSource,
        coroutineUtil: CoroutineUtil
    ): BsvStatusRepository = BsvStatusRepositoryImpl(
        blockStatsHelper,
        exchangeRateHelper,
        dynamicChainSync,
        blockchainDataSource,
        coroutineUtil
    )

    @Singleton
    @Provides
    fun provideSecureDataRepository(
        authRepository: AuthRepository,
        secureDataSource: SecureDataSource
    ): SecureDataRepository =
        SecureDataRepositoryImpl(
            authRepository,
            secureDataSource
        )

    @Singleton
    @Provides
    fun provideAppUtilDiskDAO(
        gson: Gson,
        appContext: Context
    ): AppUtilDiskDAO = AppUtilDiskDAO(
        appContext.appUtilPrefs,
        gson
    )

    @Singleton
    @Provides
    fun provideUnusedCoinAddressHelper(
        appUtilDiskDAO: AppUtilDiskDAO,
        chainSync: DynamicChainSync,
        appDatabase: AppDatabase,
        secureDataSource: SecureDataSource,
        addressDerivationHelper: CoinAddressDerivationHelper,
        coroutineUtil: CoroutineUtil
    ): UnusedCoinAddressHelper {
        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            Timber.e(throwable)
        }
        val scope = CoroutineScope(SupervisorJob() +
                coroutineUtil.defaultDispatcher +
                exceptionHandler)

        return UnusedCoinAddressHelper(
            appUtilDiskDAO,
            chainSync,
            appDatabase.coinDao(),
            secureDataSource,
            addressDerivationHelper,
            scope
        ).apply {
            setup()
        }
    }

    @Singleton
    @Provides
    fun provideNodeModel(
        coroutineUtil: CoroutineUtil,
        authRepository: AuthRepository,
        nodeRuntimeRepository: NodeRuntimeRepository,
        secureDataSource: SecureDataSource,
        coinInfoUseCase: CoinInfoUseCase,
        nodeConnectedLiveData: NodeConnectedLiveData,
        nodeRuntimeInterface: NodeRuntimeInterface,
        androidWorkHandlerProvider: AndroidWorkHandlerProvider,
    ): NodeModel = NodeModel(
        coroutineUtil,
        authRepository,
        nodeRuntimeRepository,
        secureDataSource,
        coinInfoUseCase,
        nodeConnectedLiveData,
        nodeRuntimeInterface,
        androidWorkHandlerProvider
    )
}