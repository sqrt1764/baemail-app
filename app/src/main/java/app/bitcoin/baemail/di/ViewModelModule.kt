package app.bitcoin.baemail.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.bitcoin.baemail.core.presentation.AuthViewModel
import app.bitcoin.baemail.di.helper.AppViewModelFactory
import app.bitcoin.baemail.fundingWallet.*
import app.bitcoin.baemail.fundingWallet.core.presentation.CoinViewModel
import app.bitcoin.baemail.fundingWallet.equalSplit.presentation.EqualSplitCoinViewModel
import app.bitcoin.baemail.fundingWallet.sendToAddress.presentation.SendToAddressViewModel
import app.bitcoin.baemail.fundingWallet.sendToPaymail.presentation.SendToPaymailViewModel
import app.bitcoin.baemail.intercept.InterceptViewModel
import app.bitcoin.baemail.logs.presentation.LogsViewModel
import app.bitcoin.baemail.p2p.P2pManageStallViewModel
import app.bitcoin.baemail.p2p.P2pViewModel
import app.bitcoin.baemail.paymail.presentation.AppConfigViewModel
import app.bitcoin.baemail.paymail.presentation.PaymailManagementViewModel
import app.bitcoin.baemail.payments.PaymentsViewModel
import app.bitcoin.baemail.repl.presentation.ReplViewModel
import app.bitcoin.baemail.twetch.ui.TwetchUtilViewModel
import app.bitcoin.baemail.core.presentation.CentralViewModel
import app.bitcoin.baemail.message.presentation.BaemailMessageViewModel
import app.bitcoin.baemail.message.presentation.NewBaemailViewModel
import app.bitcoin.baemail.accountMeta.AccountMetaViewModel
import app.bitcoin.baemail.di.helper.UniversalSavedStateViewModelFactory
import app.bitcoin.baemail.message.presentation.MessagesViewModel
import app.bitcoin.baemail.twetch.ui.TwetchViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(AuthViewModel::class)
    abstract fun bindAuthViewModel(viewModel: AuthViewModel.Assistant):
            SavedStateViewModelAssistant<*>

    @Binds
    @IntoMap
    @ViewModelKey(CentralViewModel::class)
    abstract fun bindCentralViewModel(viewModel: CentralViewModel.Assistant):
            SavedStateViewModelAssistant<*>

    @Binds
    @IntoMap
    @ViewModelKey(MessagesViewModel::class)
    abstract fun bindMessagesViewModel(viewModel: MessagesViewModel.Assistant):
            SavedStateViewModelAssistant<*>

    @Binds
    @IntoMap
    @ViewModelKey(TwetchViewModel::class)
    abstract fun bindTwetchViewModel(viewModel: TwetchViewModel.Assistant):
            SavedStateViewModelAssistant<*>

    @Binds
    @IntoMap
    @ViewModelKey(PaymailManagementViewModel::class)
    abstract fun bindPaymailManagementViewModel(viewModel: PaymailManagementViewModel): ViewModel
    @Binds
    @IntoMap
    @ViewModelKey(AppConfigViewModel::class)
    abstract fun bindAppConfigViewModel(viewModel: AppConfigViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountMetaViewModel::class)
    abstract fun bindSetupFragmentViewModel(viewModel: AccountMetaViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CoinViewModel::class)
    abstract fun bindCoinViewModel(viewModel: CoinViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SendToAddressViewModel::class)
    abstract fun bindSendToAddressViewModel(viewModel: SendToAddressViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SendToPaymailViewModel::class)
    abstract fun bindSendToPaymailViewModel(viewModel: SendToPaymailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TopUpViewModel::class)
    abstract fun bindTopUpViewModel(viewModel: TopUpViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewBaemailViewModel::class)
    abstract fun bindNewMessageViewModel(viewModel: NewBaemailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BaemailMessageViewModel::class)
    abstract fun bindBaemailMessageViewModel(viewModel: BaemailMessageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PaymentsViewModel::class)
    abstract fun bindPaymentsViewModel(viewModel: PaymentsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EqualSplitCoinViewModel::class)
    abstract fun bindEqualSplitCoinViewModel(viewModel: EqualSplitCoinViewModel): ViewModel

//    @Binds
//    @IntoMap
//    @ViewModelKey(TwetchViewModel::class)
//    abstract fun bindTwetchViewModel(viewModel: TwetchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TwetchUtilViewModel::class)
    abstract fun bindTwetchUtilViewModel(viewModel: TwetchUtilViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LogsViewModel::class)
    abstract fun bindLogsViewModel(viewModel: LogsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReplViewModel::class)
    abstract fun bindTerminalViewModel(viewModel: ReplViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InterceptViewModel::class)
    abstract fun bindInterceptViewModel(viewModel: InterceptViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(P2pViewModel::class)
    abstract fun bindP2pViewModel(viewModel: P2pViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(P2pManageStallViewModel::class)
    abstract fun bindP2pManageStallViewModel(viewModel: P2pManageStallViewModel): ViewModel





    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    abstract fun bindViewModelProviderAssistant(factory: UniversalSavedStateViewModelFactory.Assistant): ViewModelProviderAssistant
}