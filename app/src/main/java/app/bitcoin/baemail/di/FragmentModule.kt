package app.bitcoin.baemail.di

import app.bitcoin.baemail.fundingWallet.*
import app.bitcoin.baemail.fundingWallet.core.presentation.CoinOptionsFragment
import app.bitcoin.baemail.fundingWallet.core.presentation.FundingCoinFragment
import app.bitcoin.baemail.fundingWallet.equalSplit.presentation.EqualSplitCoinFragment
import app.bitcoin.baemail.fundingWallet.sendToAddress.presentation.SendCoinToAddressFragment
import app.bitcoin.baemail.fundingWallet.sendToPaymail.presentation.SendCoinToPaymailFragment
import app.bitcoin.baemail.logs.presentation.LogsFragment
import app.bitcoin.baemail.p2p.P2pEditStallFragment
import app.bitcoin.baemail.p2p.P2pFragment
import app.bitcoin.baemail.p2p.P2pManageStallFragment
import app.bitcoin.baemail.paymail.presentation.AddedPaymailsFragment
import app.bitcoin.baemail.paymail.presentation.AppConfigFragment
import app.bitcoin.baemail.paymail.presentation.AppDetailedInfoDialog
import app.bitcoin.baemail.paymail.presentation.PaymailDerivPathFragment
import app.bitcoin.baemail.paymail.presentation.PaymailIdInputFragment
import app.bitcoin.baemail.paymail.presentation.PaymailVerifyingFragment
import app.bitcoin.baemail.paymail.presentation.SeedPhraseInputFragment
import app.bitcoin.baemail.payments.PaymentDetailsFragment
import app.bitcoin.baemail.payments.PaymentManageFragment
import app.bitcoin.baemail.payments.PaymentsFragment
import app.bitcoin.baemail.repl.presentation.ReplFragment
import app.bitcoin.baemail.repl.presentation.SavedReplInputFragment
import app.bitcoin.baemail.twetch.ui.*
import app.bitcoin.baemail.message.presentation.BaemailMessageFragment
import app.bitcoin.baemail.message.presentation.MessagesFragment
import app.bitcoin.baemail.message.presentation.NewBaemailFragment
import app.bitcoin.baemail.accountMeta.AccountMetaFragment
import app.bitcoin.baemail.fundingWallet.core.presentation.WalletInfoFragment
import app.bitcoin.baemail.paymail.presentation.FundingDerivPathFragment
import app.bitcoin.baemail.paymail.presentation.FundingWalletConfigFragment
import app.bitcoin.baemail.paymail.presentation.PaymailAuthorizationFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeMessagesFragment(): MessagesFragment


    @ContributesAndroidInjector
    abstract fun contributeAddedPaymailsFragment(): AddedPaymailsFragment

    @ContributesAndroidInjector
    abstract fun contributePaymailIdInputFragment(): PaymailIdInputFragment

    @ContributesAndroidInjector
    abstract fun contributePaymailDerivPathFragment(): PaymailDerivPathFragment

    @ContributesAndroidInjector
    abstract fun contributeSeedPhraseInputFragment(): SeedPhraseInputFragment

    @ContributesAndroidInjector
    abstract fun contributePaymailVerifyingFragment(): PaymailVerifyingFragment

    @ContributesAndroidInjector
    abstract fun contributePaymailAuthorizationFragment(): PaymailAuthorizationFragment

    @ContributesAndroidInjector
    abstract fun contributeAccountMetaFragment(): AccountMetaFragment

    @ContributesAndroidInjector
    abstract fun contributeFundingCoinFragment(): FundingCoinFragment

    @ContributesAndroidInjector
    abstract fun contributeCoinOptionsFragment(): CoinOptionsFragment

    @ContributesAndroidInjector
    abstract fun contributeSendCoinToAddressFragment(): SendCoinToAddressFragment

    @ContributesAndroidInjector
    abstract fun contributeSendCoinToPaymailFragment(): SendCoinToPaymailFragment

    @ContributesAndroidInjector
    abstract fun contributeTopUpFragment(): TopUpFragment

    @ContributesAndroidInjector
    abstract fun contributeNewMessageFragment(): NewBaemailFragment

    @ContributesAndroidInjector
    abstract fun contributeBaemailMessageFragment(): BaemailMessageFragment

    @ContributesAndroidInjector
    abstract fun contributeAppDetailedInfoDialog(): AppDetailedInfoDialog

    @ContributesAndroidInjector
    abstract fun contributePaymentsFragment(): PaymentsFragment

    @ContributesAndroidInjector
    abstract fun contributeEqualSplitCoinFragment(): EqualSplitCoinFragment

    @ContributesAndroidInjector
    abstract fun contributePaymentDetailsFragment(): PaymentDetailsFragment

    @ContributesAndroidInjector
    abstract fun contributePaymentManageFragment(): PaymentManageFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchAuthFragment(): TwetchAuthFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchContainerFragment(): TwetchContainerFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchAuthSigningAddressFragment(): TwetchAuthSigningAddressFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchNewMessageFragment(): TwetchNewMessageFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchProfileFragment(): TwetchProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchChatListFragment(): TwetchChatListFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchFollowingFragment(): TwetchFollowingFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchLatestFragment(): TwetchLatestFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchNotificationsFragment(): TwetchNotificationsFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchSearchFragment(): TwetchSearchFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchChatContentFragment(): TwetchChatContentFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchChatParticipantsFragment(): TwetchChatParticipantsFragment

    @ContributesAndroidInjector
    abstract fun contributeLogsFragment(): LogsFragment

    @ContributesAndroidInjector
    abstract fun contributeReplFragment(): ReplFragment

    @ContributesAndroidInjector
    abstract fun contributeSavedReplInputFragment(): SavedReplInputFragment

    @ContributesAndroidInjector
    abstract fun contributeP2pFragment1(): P2pFragment

    @ContributesAndroidInjector
    abstract fun contributeAppConfigFragment(): AppConfigFragment

    @ContributesAndroidInjector
    abstract fun contributeFundingWalletConfigFragment(): FundingWalletConfigFragment

    @ContributesAndroidInjector
    abstract fun contributeFundingDerivPathFragment(): FundingDerivPathFragment

    @ContributesAndroidInjector
    abstract fun contributeP2pManageStallFragment(): P2pManageStallFragment

    @ContributesAndroidInjector
    abstract fun contributeP2pEditStallFragment(): P2pEditStallFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchPostDetailsFragment(): TwetchPostDetailsFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchUserProfileFragment(): TwetchUserProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchChatFullImageFragment(): TwetchChatFullImageFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchBranchOptionsFragment(): TwetchBranchOptionsFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchTopFragment(): TwetchTopFragment

    @ContributesAndroidInjector
    abstract fun contributeTwetchVideoClipFragment(): TwetchVideoClipFragment

    @ContributesAndroidInjector
    abstract fun contributeWalletInfoFragment(): WalletInfoFragment
}