package app.bitcoin.baemail.di

import androidx.lifecycle.ViewModelProvider
import androidx.savedstate.SavedStateRegistryOwner

interface ViewModelProviderAssistant {
    fun create(savedStateRegistryOwner: SavedStateRegistryOwner): ViewModelProvider.Factory
}