package app.bitcoin.baemail.di

import app.bitcoin.baemail.core.data.work.AppWorkerFactory
import app.bitcoin.baemail.core.data.work.DummyWorker
import app.bitcoin.baemail.core.data.work.FundingWalletSyncWorker
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface WorkerModule {

    @Binds
    @IntoMap
    @WorkerKey(DummyWorker::class)
    fun bindDummyWorker(factory: DummyWorker.Factory): AppWorkerFactory

    @Binds
    @IntoMap
    @WorkerKey(FundingWalletSyncWorker::class)
    fun bindFundingWalletSyncWorker(factory: FundingWalletSyncWorker.Factory): AppWorkerFactory

}