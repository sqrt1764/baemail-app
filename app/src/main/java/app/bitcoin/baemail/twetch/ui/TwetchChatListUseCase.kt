package app.bitcoin.baemail.twetch.ui

import app.bitcoin.baemail.core.data.util.CoroutineUtil
import com.dropbox.android.external.store4.ResponseOrigin
import com.dropbox.android.external.store4.StoreResponse
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber

@ExperimentalCoroutinesApi
@FlowPreview
class TwetchChatListUseCase(
    private val coroutineUtil: CoroutineUtil,
    private val twetchChatInfoStore: TwetchChatInfoStore
) {

    private var isSetup = false
    private var collectStateJob: Job? = null
    private var chatInfoCollectJob: Job? = null



    private val requestRefresh = MutableStateFlow(0)

    private val _chatInfo = MutableStateFlow<StoreResponse<List<TwetchChatInfoStore.ChatInfo>>>(
        StoreResponse.Loading(ResponseOrigin.Cache)
    )
    val chatInfo: StateFlow<StoreResponse<List<TwetchChatInfoStore.ChatInfo>>>
        get() {
            //ensureSetup()
            return _chatInfo
        }

    fun cleanup() {
        chatInfoCollectJob?.cancel()
        chatInfoCollectJob = null

        collectStateJob?.cancel()
        collectStateJob = null

        isSetup = false

        _chatInfo.value = StoreResponse.Loading(ResponseOrigin.Cache)
    }

    fun refreshNow() {
        requestRefresh.value = 1 + requestRefresh.value
    }

    fun ensureSetup(key: TwetchChatInfoStore.ChatInfoKey) {
        if (isSetup) {
            Timber.d("already setup")
            return
        }
        isSetup = true


        collectStateJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            requestRefresh.collect {
                Timber.d(">>>>>>> starting observing new stream of chat-info store")

                chatInfoCollectJob?.cancel()
                chatInfoCollectJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

                    //eagerly return the cached content
                    twetchChatInfoStore.getAllForKey(key).let {
                        if (it.isEmpty()) return@let
                        //return what is already cached
                        _chatInfo.value = StoreResponse.Data(it, ResponseOrigin.SourceOfTruth)
                    }

                    twetchChatInfoStore.getFlowForKey(key).collect { response ->
                        _chatInfo.value = response

                    }
                }
            }
        }
    }

}