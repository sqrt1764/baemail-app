package app.bitcoin.baemail.twetch.ui

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Outline
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.doOnLayout
import androidx.core.widget.TextViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.view.recycler.ATContentLoading0
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchNoPostsFoundItem
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchSearchPostsHintItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.core.presentation.util.RelativeTimeHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.math.max
import kotlin.math.min

class TwetchSearchFragment : Fragment(R.layout.fragment_twetch_search) {

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel

    private lateinit var helper: TwetchSearchUseCase

    private val postItemHelper: TwetchPostItemHelper by lazy {
        TwetchPostItemHelper(
            requireContext(),
            viewModel.postUserCache,
            TextViewCompat.getTextMetricsParams(dummyBodyText)
        )
    }


    private lateinit var container: CoordinatorLayout
    private lateinit var recycler: RecyclerView
    private lateinit var searchBar: FrameLayout
    private lateinit var searchLayout: FrameLayout
    private lateinit var searchView: SearchView
    private lateinit var cancelSearch: TextView
    private lateinit var clearSearch: ImageView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var scrollToTop: TextView

    private lateinit var dummyBodyText: TextView




    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter

    private var wasCreatedOnce = false


    private var listRestoredOnce = false



    private val topHeightLD = MutableLiveData<Int>()
    private val topHeightItem = ATHeightDecorDynamic("0", topHeightLD)


    private val bottomHeightLD = MutableLiveData<Int>()
    private val bottomHeightItem = ATHeightDecorDynamic("1", bottomHeightLD)

    private val altLoadingItemLD = MutableLiveData<Int>()
    private val altLoadingItem = ATHeightDecorDynamic("2", altLoadingItemLD)

    private val intItem = ATTwetchSearchPostsHintItem("0")
    private val noPostsFoundItem = ATTwetchNoPostsFoundItem("0")

    private val appliedInsets = AppliedInsets()
    private val availableHeightHelper = AvailableScreenHeightHelper()

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)


        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
                .get(TwetchUtilViewModel::class.java)


        helper = viewModel.getSearchContent()



        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true


        dummyBodyText = requireView().findViewById(R.id.dummy_body_text)

        container = requireView().findViewById(R.id.search_container)
        swipeRefresh = requireView().findViewById(R.id.swipe_refresh)
        recycler = requireView().findViewById(R.id.list)
        searchView = requireView().findViewById(R.id.search_view)
        searchBar = requireView().findViewById(R.id.search_bar)
        searchLayout = requireView().findViewById(R.id.search_layout)
        cancelSearch = requireView().findViewById(R.id.cancel_search)
        clearSearch = requireView().findViewById(R.id.clear_search)
        scrollToTop = requireView().findViewById(R.id.scroll_to_top)





        availableHeightHelper.setup(
            requireContext(),
            requireActivity().window,
            viewLifecycleOwner,
            { requireView() }
        )

        availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
            height ?: return@Observer
            refreshDynamicBottomHeight()
        })






        val dp = resources.displayMetrics.density

        val colorSelector = ContextCompat.getColor(requireContext(), R.color.selector_on_surface)
        val colorSurfaceVariant = requireContext().getColorFromAttr(R.attr.colorSurfaceVariant)
        val colorPrimary = requireContext().getColorFromAttr(R.attr.colorPrimary)
        val colorTransparentPrimary = Color.argb(
            200,
            Color.red(colorPrimary),
            Color.green(colorPrimary),
            Color.blue(colorPrimary)
        )

        altLoadingItemLD.value = (108 * dp).toInt()


        swipeRefresh.setProgressViewOffset(true, (30 * dp).toInt(), (118 * dp).toInt())
        swipeRefresh.setColorSchemeColors(requireContext().getColorFromAttr(R.attr.colorOnPrimary))
        swipeRefresh.setProgressBackgroundColorSchemeColor(colorTransparentPrimary)









        scrollToTop.clipToOutline = true
        scrollToTop.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, p0.height / 2f)
            }
        }
        scrollToTop.background = ColorDrawable(colorTransparentPrimary)

        scrollToTop.setOnClickListener {
            recycler.smoothScrollToPosition(0)
            scrollToTop.visibility = View.INVISIBLE
        }







        val inputbarElevatedHeight = 6.0f
        val inputbarBackground = MaterialShapeDrawable.createWithElevationOverlay(requireContext(), inputbarElevatedHeight)
        inputbarBackground.fillColor = ColorStateList.valueOf(colorSurfaceVariant)
        inputbarBackground.strokeColor = ColorStateList.valueOf(requireContext().getColorFromAttr(R.attr.colorPrimary))
        inputbarBackground.strokeWidth = dp * 2.1f
        inputbarBackground.setCornerSize(dp * 30)
        searchLayout.background = inputbarBackground

        searchView.isSaveEnabled = false
        searchView.setIconifiedByDefault(false)

        searchBar.doOnLayout {
            topHeightLD.value = (it.height + 0 * dp).toInt()
        }


        clearSearch.background = DrawableState.getNew(colorSelector, ovalShape = true)
        clearSearch.drawable.mutate().setTint(colorPrimary)

        clearSearch.clipToOutline = true
        clearSearch.outlineProvider = object : ViewOutlineProvider() {
            val r = Rect()
            override fun getOutline(v: View?, o: Outline?) {
                if (v == null || o == null) return

                r.set(0, 0, v.width, v.height)
                o.setOval(r)
            }
        }


        cancelSearch.background = DrawableState.getNew(colorSelector, chipShape = true)

        cancelSearch.clipToOutline = true
        cancelSearch.outlineProvider = object : ViewOutlineProvider() {
            val r = Rect()
            override fun getOutline(v: View?, o: Outline?) {
                if (v == null || o == null) return

                r.set(0, 0, v.width, v.height)
                o.setRoundRect(r, (min(r.height(), r.width()) / 2).toFloat())
            }
        }


        clearSearch.setOnClickListener {

            recycler.scrollToPosition(0)

            if (searchView.hasFocus()) {
                searchView.setQuery("", false)
            } else {
                helper.onRequestClearSearchResults()
            }
        }


        cancelSearch.setOnClickListener {
            helper.onRequestClearSearchResults()
            searchView.clearFocus()
        }


        searchView.setOnQueryTextListener(searchInputQueryTextListener)
        searchView.setOnQueryTextFocusChangeListener(focusChangeListener)

        swipeRefresh.setOnRefreshListener {
            if (helper.content.value.query.isNullOrBlank()) {
                swipeRefresh.isRefreshing = false
                return@setOnRefreshListener
            }

            helper.onRefreshRequested()
        }












        val contentLoadingListener = object : BaseListAdapter.ATContentLoadingListener {
            override fun onBound() {

                if (helper.metaState.value.refreshing) {
                    return
                }

                Timber.d("ATContentLoadingListener#onBound ... requestNextPage")
                swapOutRefreshingItem()
            }
        }

        val postListener = TwetchPostListener(
            requireContext(),
            findNavController(),
            viewModel.actionHelper,
            viewModel.videoClipManager,
            R.id.action_global_twetchUserProfile,
            R.id.action_twetchSearchFragment_to_twetchChatFullImageFragment6,
            R.id.action_global_twetchPostDetails,
            R.id.action_twetchSearchFragment_to_twetchBranchOptionsFragment6,
            R.id.action_twetchSearchFragment_to_twetchVideoClipFragment6,
            viewModel.coinFlipSignalHelper.coinFlipChannel
        )

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atContentLoadingListener = contentLoadingListener,
                atTwetchPostListener = postListener
            )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var lastShowShadow: Boolean? = null

            var hasScrollUpBeenInitialized = false

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                recyclerView.canScrollVertically(-1).let { showShadow ->
                    if (lastShowShadow == showShadow) return@let
                    lastShowShadow = showShadow

                    if (showShadow) {
                        utilViewModel.onMainContentScrollChanged(false)
                    } else {
                        utilViewModel.onMainContentScrollChanged(true)
                    }
                }

                if (!hasScrollUpBeenInitialized) {
                    refreshShowingOfScrollToTop()
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (RecyclerView.SCROLL_STATE_IDLE == newState) {
                    cacheCurrentScrollState()

                    refreshShowingOfScrollToTop()
                }
            }

            fun refreshShowingOfScrollToTop() {
                hasScrollUpBeenInitialized = true

                requireView().post {
                    if (layoutManager.findFirstVisibleItemPosition() > 10) {
                        if (scrollToTop.visibility != View.VISIBLE) {
                            scrollToTop.visibility = View.VISIBLE
                        }
                    } else {
                        if (scrollToTop.visibility != View.INVISIBLE) {
                            scrollToTop.visibility = View.INVISIBLE
                        }
                    }
                }
            }
        })











        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            utilViewModel.mainContentTopDistance.collect {
                refreshDynamicBottomHeight()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            helper.metaState.collectLatest {
                refreshLoadingIndicatorState()
            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {

            helper.content.collectLatest { model ->


                let {
                    val currentInputValue = searchView.query.toString()

                    if (model.query == null) {
                        cancelSearch.visibility = View.INVISIBLE
                        clearSearch.visibility = View.INVISIBLE

                        searchView.setOnQueryTextListener(null)
                        searchView.setQuery("", false)
                        searchView.setOnQueryTextListener(searchInputQueryTextListener)

                        return@let
                    }

                    if (model.query.isEmpty()) {
                        if (searchView.hasFocus()) {
                            cancelSearch.visibility = View.VISIBLE
                            clearSearch.visibility = View.INVISIBLE
                        } else {
                            cancelSearch.visibility = View.INVISIBLE
                            clearSearch.visibility = View.INVISIBLE
                        }
                    } else {
                        cancelSearch.visibility = View.INVISIBLE
                        clearSearch.visibility = View.VISIBLE
                    }

                    if (currentInputValue != model.query) {
                        Timber.d("content.collectLatest setQuery from observer")
                        searchView.setQuery(model.query, false)
                    }
                }


                if (model.query.isNullOrBlank()) {
                    showHintList()
                    return@collectLatest
                }


                initList(model.list)



            }
        }

    }

    private suspend fun showHintList() {
        awaitRecyclerAnimationsFinish()
        //refreshLoadingIndicatorState()

        adapter.setItems(listOf(
            topHeightItem,
            intItem,
            bottomHeightItem
        ))
    }

    var lastAppliedList: TwetchPostModel? = null

    private suspend fun initList(model: TwetchPostModel?) {
        if (lastAppliedList == model) {
            Timber.d("post-list is unchanged; returning")
            return
        }
        lastAppliedList = model



        //refreshLoadingIndicatorState()
        awaitRecyclerAnimationsFinish()



        if (model == null) {
            Timber.d("model.list == null")
            adapter.setItems(emptyList())
        } else {

            val isRefreshing = helper.metaState.value.refreshing

            val loadingItem = if (!isRefreshing && model.hasMorePages) {
                ATContentLoading0(model.page)
            } else {
                altLoadingItem
            }


            if (model.list.isEmpty()) {
                if (model.hasMorePages) {
                    adapter.setItems(
                        listOf(
                            topHeightItem,
                            loadingItem,
                            bottomHeightItem
                        )
                    )
                } else {
                    adapter.setItems(
                        listOf(
                            topHeightItem,
                            noPostsFoundItem,
                            bottomHeightItem
                        )
                    )
                }

            } else {
                val itemList = mutableListOf<AdapterType>()

                withContext(Dispatchers.Default) {
                    val relativeTimeHelper = RelativeTimeHelper(
                        requireContext(),
                        System.currentTimeMillis()
                    )

                    model.list.map {

//                        val refPostTxId = it.post.referencedPost
//                        val refPost = refPostTxId?.let { txId ->
//                            it.referenced[txId]
//                        }
//
//                        val postLabel = relativeTimeHelper.getLabel(it.post.millisCreatedAt)
//                        val refPostLabel = refPost?.millisCreatedAt?.let { millis ->
//                            relativeTimeHelper.getLabel(millis)
//                        }
//                        val doubleRefPostLabel = refPost?.referencedPost?.let { txId ->
//                            it.referenced[txId]?.let { p ->
//                                relativeTimeHelper.getLabel(p.millisCreatedAt)
//                            }
//                        }
//
//                        ATTwetchPostItem(
//                            it.post.id,
//                            it,
//                            postLabel,
//                            refPostLabel,
//                            doubleRefPostLabel
//                        )
                        postItemHelper.createItem(it, relativeTimeHelper)
                    }.toCollection(itemList)
                }

                Timber.d("new post-list; size: ${itemList.size}; page:${model.page} hasMorePages:${model.hasMorePages} isRefreshing:$isRefreshing")


                itemList.add(0, topHeightItem)
                if (model.hasMorePages) {
                    itemList.add(loadingItem)
                }
                itemList.add(bottomHeightItem)

                adapter.setItems(itemList)

                if (!listRestoredOnce) {
                    listRestoredOnce = true
                    layoutManager.onRestoreInstanceState(helper.cachedListState)
                }
            }

        }
    }

    private fun refreshLoadingIndicatorState() {
        if (swipeRefresh.isRefreshing == helper.metaState.value.refreshing) return
        swipeRefresh.isRefreshing = helper.metaState.value.refreshing
    }


    private fun cacheCurrentScrollState() {
        helper.cachedListState = layoutManager.onSaveInstanceState()
    }


    private fun refreshDynamicBottomHeight() {
        val appbarOffset = utilViewModel.mainContentTopDistance.value
        val dp = resources.displayMetrics.density
        val keyboardCoveredHeight = availableHeightHelper.availableHeightLD.value ?: return

        val maxFragmentHeight = container.height
        val keyboardHeight = max(maxFragmentHeight - keyboardCoveredHeight, 0)


        ///////

        bottomHeightLD.value = appbarOffset + keyboardHeight + (48 * dp).toInt()
    }




    private val focusChangeListener = object : View.OnFocusChangeListener {
        override fun onFocusChange(v: View?, hasFocus: Boolean) {
            val currentInputValue = searchView.query.toString()

            if (!hasFocus) {
                if (currentInputValue.trim().isEmpty()) {
                    cancelSearch.visibility = View.INVISIBLE
                    clearSearch.visibility = View.INVISIBLE
                } else {
                    cancelSearch.visibility = View.INVISIBLE
                    clearSearch.visibility = View.VISIBLE
                }
                return
            }

            if (currentInputValue.trim().isNotEmpty()) {
                cancelSearch.visibility = View.INVISIBLE
                clearSearch.visibility = View.VISIBLE
            } else {
                cancelSearch.visibility = View.VISIBLE
                clearSearch.visibility = View.INVISIBLE
            }
        }

    }


    private val searchInputQueryTextListener = object : SearchView.OnQueryTextListener {

        override fun onQueryTextChange(newText: String): Boolean {
            val wasDropped = !helper.onQueryTextChange(newText)

            if (newText.isBlank()) {
                if (searchView.hasFocus()) {
                    cancelSearch.visibility = View.VISIBLE
                    clearSearch.visibility = View.INVISIBLE
                } else {
                    cancelSearch.visibility = View.INVISIBLE
                    clearSearch.visibility = View.INVISIBLE
                }
            } else {
                cancelSearch.visibility = View.INVISIBLE
                clearSearch.visibility = View.VISIBLE
            }

            if (!wasDropped) {
                recycler.scrollToPosition(0)
                scrollToTop.visibility = View.INVISIBLE
            }

            return false
        }

        override fun onQueryTextSubmit(query: String): Boolean {
            if (requireView().isInTouchMode) {
                searchView.clearFocus()
            } else {
                clearSearch.requestFocus()
            }


            val wasDropped = !helper.onQueryTextSubmit(query)

            if (!wasDropped) {
                Timber.d(">>>> onQueryTextSubmit was valid")
                recycler.scrollToPosition(0)
                scrollToTop.visibility = View.INVISIBLE
            }

            return false
        }

    }









    override fun onStop() {
        lastAppliedList = null

        imm.hideSoftInputFromWindow(requireView().windowToken, 0)

        super.onStop()
    }









    private suspend fun awaitRecyclerAnimationsFinish(fromListChange: Boolean = true) {
        if (fromListChange) {
            swapOutRefreshingItemJob?.cancel()
            swapOutRefreshingItemJob = null
        }

        recycler.itemAnimator?.let { animator ->
            //making sure that the any previous animation get completed
            if (!animator.isRunning) {
                return@let
            }

            suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                animator.isRunning { cancellableContinuation.resume(Unit) }
            }
        }

        suspendCancellableCoroutine<Unit> { continuation ->
            if (recycler.isComputingLayout) {
                recycler.post {
                    continuation.resume(Unit)
                }
            } else {
                continuation.resume(Unit)
            }

        }
    }



    var swapOutRefreshingItemJob: Job? = null

    private fun swapOutRefreshingItem() {
        if (swapOutRefreshingItemJob?.isActive == true) return

        swapOutRefreshingItemJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            awaitRecyclerAnimationsFinish(false)

            val items = adapter.getCopyOfItems()

            var loadingItemPos = -1

            val countToCheck = min(5, items.size)
            for (i in 0 until countToCheck) {
                val item = items[items.lastIndex - i]
                if (item is ATContentLoading0) {
                    loadingItemPos = items.lastIndex - i
                    break
                }
            }

            ensureActive()

            if (loadingItemPos == -1) {
                Timber.d(">>> loadingItemPost not found")
                helper.requestNextPage()
                return@launchWhenStarted
            }

            adapter.swapOutItem(loadingItemPos, altLoadingItem)

            awaitRecyclerAnimationsFinish(false)
            helper.requestNextPage()
        }
    }


}