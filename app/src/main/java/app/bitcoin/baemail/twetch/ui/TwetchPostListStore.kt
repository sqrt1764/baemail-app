package app.bitcoin.baemail.twetch.ui

import android.content.Context
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.twetch.db.TwetchDatabase
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.dropbox.android.external.store4.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.*
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import timber.log.Timber
import kotlin.math.max


class TwetchPostListStore(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val twetchDatabase: TwetchDatabase,
    private val postCache: TwetchPostCache,
    private val userCache: TwetchPostUserCache,
    private val twetchFilesMimeTypeHelper: TwetchFilesMimeTypeHelper,
    private val gson: Gson
) {

    companion object {
        private const val PAGE_SIZE = 15
    }


    private val dummyListOrderCache = HashMap<Key, MutableList<String>>()

    private val dummyPagingMetaCache = HashMap<Key, PagingMeta>()


    private val postListStore: Store<Key, List<PostHolder>> = StoreBuilder.from(
        fetcher = Fetcher.ofFlow { filterKey ->
            Timber.d("postListStore#fetcherOfFlow $filterKey")
            return@ofFlow queryServer(filterKey)
        },
        sourceOfTruth = SourceOfTruth.of(
            reader = ::queryByFilter,
            writer = ::insert,
            delete = ::clearByKey,
            deleteAll = ::deleteAll
        )

    ).build()

    private fun queryServer(key: Key): Flow<List<PostHolder>> {
        return flow {

            val activeUserId = key.activeUserId
            val userId = key.userId
            val page = key.page

            val cursor = if (page != 0) {
                dummyPagingMetaCache[key]?.afterCursor
            } else null


            val requestingNodeScript = when (key.type) {
                ListType.PROFILE_REPLIES,
                ListType.PROFILE_LATEST -> {
                    val includeReplies = ListType.PROFILE_REPLIES == key.type

                    assembleUserProfilePostRequestingNodeScript(
                        userId!!,
                        includeReplies,
                        cursor,
                        PAGE_SIZE
                    )
                }
                ListType.PROFILE_LIKES -> {
                    assembleUserProfileLikesRequestingNodeScript(
                        userId!!,
                        cursor,
                        PAGE_SIZE
                    )
                }
                ListType.FOLLOWING -> {
                    val afterTime = if (page != 0) {
                        dummyPagingMetaCache[key]?.afterTime!!
                    } else {
                        ZonedDateTime.now().plusYears(1)
                            .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                    }

                    assembleMyFollowingFeedRequestingNodeScript(
                        afterTime,
                        PAGE_SIZE
                    )
                }
                ListType.LATEST -> {
                    assembleLatestFeedRequestingNodeScript(
                        cursor,
                        PAGE_SIZE
                    )
                }
                ListType.TOP_HOUR -> {
                    val afterTime = if (page != 0) {
                        dummyPagingMetaCache[key]?.afterTime!!
                    } else {
                        val time = ZonedDateTime.now().minusHours(1)
                            .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                        dummyPagingMetaCache[key] = PagingMeta(
                            page,
                            null,
                            false,
                            time
                        )
                        time
                    }

                    assembleTopFeedRequestingNodeScript(
                        cursor,
                        afterTime,
                        PAGE_SIZE
                    )
                }
                ListType.TOP_DAY -> {
                    val afterTime = if (page != 0) {
                        dummyPagingMetaCache[key]?.afterTime!!
                    } else {
                        val time = ZonedDateTime.now().minusDays(1)
                            .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                        dummyPagingMetaCache[key] = PagingMeta(
                            page,
                            null,
                            false,
                            time
                        )
                        time
                    }

                    assembleTopFeedRequestingNodeScript(
                        cursor,
                        afterTime,
                        PAGE_SIZE
                    )
                }
                ListType.TOP_WEEK -> {
                    val afterTime = if (page != 0) {
                        dummyPagingMetaCache[key]?.afterTime!!
                    } else {
                        val time = ZonedDateTime.now().minusWeeks(1)
                            .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                        dummyPagingMetaCache[key] = PagingMeta(
                            page,
                            null,
                            false,
                            time
                        )
                        time
                    }

                    assembleTopFeedRequestingNodeScript(
                        cursor,
                        afterTime,
                        PAGE_SIZE
                    )
                }
                ListType.TOP_MONTH -> {
                    val afterTime = if (page != 0) {
                        dummyPagingMetaCache[key]?.afterTime!!
                    } else {
                        val time = ZonedDateTime.now().minusMonths(1)
                            .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                        dummyPagingMetaCache[key] = PagingMeta(
                            page,
                            null,
                            false,
                            time
                        )
                        time
                    }

                    assembleTopFeedRequestingNodeScript(
                        cursor,
                        afterTime,
                        PAGE_SIZE
                    )
                }
                ListType.TOP_ALL -> {
                    val afterTime = if (page != 0) {
                        dummyPagingMetaCache[key]?.afterTime!!
                    } else {
                        val time = ZonedDateTime.now().minusYears(20)
                            .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                        dummyPagingMetaCache[key] = PagingMeta(
                            page,
                            null,
                            false,
                            time
                        )
                        time
                    }

                    assembleTopFeedRequestingNodeScript(
                        cursor,
                        afterTime,
                        PAGE_SIZE
                    )
                }
                ListType.SEARCH -> {
                    val query = key.query!!
                    val lastPage = max(0, dummyPagingMetaCache[key]?.lastPage ?: 0)
                    val offset = PAGE_SIZE * lastPage

                    assembleSearchFeedRequestingNodeScript(
                        query,
                        offset,
                        PAGE_SIZE
                    )
                }
            }


            val result = nodeRuntimeRepository.runAsyncScript(requestingNodeScript)


            if (result is RunScript.Response.Success) {
//                Timber.d("success: ${result.content}") //...was too much spam in logs


                val (postList, nextMeta) = when (key.type) {
                    ListType.PROFILE_REPLIES,
                    ListType.PROFILE_LATEST,
                    ListType.LATEST,
                    ListType.TOP_HOUR,
                    ListType.TOP_DAY,
                    ListType.TOP_WEEK,
                    ListType.TOP_MONTH,
                    ListType.TOP_ALL -> {
                        parsePostListResponse(result.content, activeUserId)
                    }
                    ListType.PROFILE_LIKES -> {
                        parseUserProfileLikesListResponse(result.content, activeUserId)
                    }
                    ListType.FOLLOWING -> {
                        parseMyFollowingFeedResponse(result.content, activeUserId)
                    }
                    ListType.SEARCH -> {
                        parseSearchPostListResponse(result.content, activeUserId)
                    }
                }

                Timber.d("success... key:$key ...results:${postList.size} ...nextMeta:$nextMeta")

                val orderingList = ensureOrderingList(key)
                if (page == 0) {
                    orderingList.clear()
                    Timber.d("orderingList cleared... key:$key")
                }
                postList.map { it.post.txId }.toCollection(orderingList)


                dummyPagingMetaCache[key] = when (key.type) {
                    ListType.PROFILE_REPLIES,
                    ListType.PROFILE_LATEST,
                    ListType.PROFILE_LIKES -> {
                        PagingMeta(page, nextMeta, false, null)
                    }
                    ListType.FOLLOWING -> {
                        PagingMeta(page, null, false, nextMeta)
                    }
                    ListType.LATEST -> {
                        PagingMeta(page, nextMeta, false, null)
                    }
                    ListType.TOP_HOUR,
                    ListType.TOP_DAY,
                    ListType.TOP_WEEK,
                    ListType.TOP_MONTH,
                    ListType.TOP_ALL -> {
                        val currentMeta = dummyPagingMetaCache[key]!!

                        currentMeta.copy(
                            lastPage = page,
                            afterCursor = nextMeta
                        )
                    }
                    ListType.SEARCH -> {
                        PagingMeta(page, nextMeta, false, null)
                    }
                }



                //todo
                //todo
                //todo
                //todo
                handleUserCachingAndRetrieval(postList)



                emit(postList)

            } else {
                result as RunScript.Response.Fail

                dummyPagingMetaCache[key] = PagingMeta(page, null, true)

                throw result.throwable
            }





        }
    }


    private suspend fun handleUserCachingAndRetrieval(posts: List<PostHolder>) {
        try {
            val mentionedUsers = HashSet<String>()
            posts.forEach { holder ->
                userCache.setUsersOfPost(holder.post)
                mentionedUsers.addAll(TwetchPostUserCache.findUserMentions(holder.post.content))

                holder.referenced.values.forEach { post ->
                    userCache.setUsersOfPost(post)
                    mentionedUsers.addAll(TwetchPostUserCache.findUserMentions(post.content))
                }
            }

            val missingPostUsers = userCache.findUsersMissingForIds(mentionedUsers)
            if (missingPostUsers.isEmpty()) return

            val foundUsers = TwetchPostUserCache.findUserForIds(
                nodeRuntimeRepository,
                gson,
                missingPostUsers
            )

            Timber.d("found: $foundUsers")

            foundUsers.forEach { user ->
                userCache.setUser(user)
            }

        } catch (e: Exception) {
            Timber.e(e)
        }
    }


    private fun ensureOrderingList(key: Key): MutableList<String> {
        return if (dummyListOrderCache.containsKey(key)) {
            dummyListOrderCache[key]!!
        } else {
            val list = mutableListOf<String>()
            dummyListOrderCache[key] = list
            list
        }
    }

    private fun queryByFilter(key: Key): Flow<List<PostHolder>> {
        return postCache.updatedState.mapLatest {
            val gotList = ensureOrderingList(key).let { list ->
                val maxSize = (key.page + 1) * PAGE_SIZE
                if (list.size > maxSize) {
                    list.subList(0, maxSize)
                } else {
                    list
                }
            }
            return@mapLatest gotList.mapNotNull { txId ->
                postCache.getPostHolder(key.activeUserId, txId)
            }
        }
    }

    private suspend fun insert(key: Key, results: List<PostHolder>) {
        //cache the actual posts
        val foundFilesRefs = ArrayList<String>()
        results.forEach { holder ->
            postCache.setPost(key.activeUserId, holder.post)

            holder.referenced.values.forEach { post ->
                postCache.setPost(key.activeUserId, post)

                if (post.files.isNotEmpty()) {
                    foundFilesRefs.addAll(post.files)
                }
            }

            if (holder.post.files.isNotEmpty()) {
                foundFilesRefs.addAll(holder.post.files)
            }
        }

        postCache.notifyUpdated()

        if (foundFilesRefs.isNotEmpty()) {
            twetchFilesMimeTypeHelper.checkFilesContent(foundFilesRefs)
        }
    }

    private suspend fun clearByKey(key: Key) {
        throw RuntimeException()
    }

    private suspend fun deleteAll() {
        postCache.clear()
    }



    /////////////////////////////////////////////////////////////////


    suspend fun getFromSourceOfTruth(key: Key): List<PostHolder> {
        return queryByFilter(key).first()
    }


    fun getFlow(key: Key, fresh: Boolean = false): Flow<StoreResponse<List<PostHolder>>> {
        if (fresh) {
            return postListStore.stream(StoreRequest.fresh(key))
        }
        return postListStore.stream(StoreRequest.cached(key, true))
    }


    fun checkChatHasMorePages(
        type: ListType,
        activeUserId: String,
        userId: String?,
        query: String?
    ): Boolean {
        val pagingMeta = dummyPagingMetaCache[Key(
            type,
            activeUserId,
            userId,
            query
        )]

        pagingMeta ?: let {
            //nothing cached yet
            return true
        }

        return when (type) {
            ListType.FOLLOWING -> {
                pagingMeta.afterTime != null && !pagingMeta.didError
            }
            else -> {
                pagingMeta.afterCursor != null && !pagingMeta.didError
            }
        }
    }

    /////////////////////////////////////////////////////////////////

    private fun assembleSearchFeedRequestingNodeScript(
        query: String,
        offset: Int,
        pageSize: Int
    ): String {
        return """
            let millisStart = new Date().getTime();
            
            await context.twetch.ensureAuthHasCompleted();
            
            let millisPostAuth = new Date().getTime();
            
            let queryResult = await context.twetch.instance.query(`
                {
                    postSearch(searchTerm: "$query", first: $pageSize, offset: $offset) {
                        bContent
                        bContentType
                        createdAt
                        id
                        mapComment
                        nodeId
                        numBranches
                        numLikes
                        replyPostId
                        transaction
                        userId
                        youBranched
                        youLiked
                        mapTwdata
                        files
                        mediaByPostId {
                            nodes {
                                filename
                            }
                        }
                        postsByReplyPostId {
                            totalCount
                        }
                        parents {
                            nodes {
                                nodeId
                                id
                                transaction
                                userByUserId {
                                    icon
                                    id
                                    name
                                    nodeId
                                }
                            }
                        }
                        userByUserId {
                            createdAt
                            icon
                            id
                            name
                            nodeId
                        }
                    }
                }
            `);
            
            let millisPostRequest = new Date().getTime();
            
            
            
            
            /////////////////////////////////////////////////////////////
            
            let extractBranchTxIds = ${TwetchQueryUtil.printBranchTxExtractionFunction()};
            let postFetchingFunction = ${TwetchQueryUtil.printPostFetchingFunction()};
            
            
            var referencedPostsMap = {};
            var postArray = [];
            
            
            postArray = queryResult.postSearch;
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts0 = await postFetchingFunction(referencedPostsMap);
            
            
            let millisPostPostFetchingFunction = new Date().getTime();
            
            
            //
            //
            //must also fetch branches-of-branches
            postArray = Object.values(foundPosts0);
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts1 = await postFetchingFunction(referencedPostsMap);
            
            queryResult.refPostMap = Object.assign({}, foundPosts0, foundPosts1);
            
            
            let millisPostPostPostFetchingFunction = new Date().getTime();
            
            
            
            console.log("time spent awaiting twetch-auth:" 
                + (millisPostAuth - millisStart) 
                + "ms; time spent on post-list:" 
                + (millisPostRequest - millisPostAuth) 
                + "ms; time spent on referenced posts:" 
                + (millisPostPostFetchingFunction - millisPostRequest) 
                + "ms; time spent on references posts of referenced posts:"
                + (millisPostPostPostFetchingFunction - millisPostPostFetchingFunction)
                + "ms");
            
            
            return JSON.stringify(queryResult);
            
        """.trimIndent()
    }



    private fun assembleTopFeedRequestingNodeScript(
        cursor: String?,
        createdAfter: String,
        pageSize: Int
    ): String {
        val afterCursor = if (cursor == null) {
            "null"
        } else {
            "\"$cursor\""
        }

        return """
            let millisStart = new Date().getTime();
            
            await context.twetch.ensureAuthHasCompleted();
            
            let millisPostAuth = new Date().getTime();
            
            let queryResult = await context.twetch.instance.query(`
                {
                    allPosts(first: $pageSize, after: $afterCursor, orderBy: RANKING_DESC, filter: { bContentType: { isNull: false }, createdAt: { greaterThan: "$createdAfter" } }) {
                        totalCount
                        pageInfo {
                            hasNextPage
                            endCursor
                        }
                        nodes {
                            bContent
                            bContentType
                            createdAt
                            id
                            mapComment
                            nodeId
                            numBranches: branchCount
                            numLikes: likeCount
                            replyPostId
                            transaction
                            userId
                            youBranched
                            youLiked
                            mapTwdata
                            files
                            mediaByPostId {
                                nodes {
                                    filename
                                }
                            }
                            postsByReplyPostId {
                                totalCount
                            }
                            parents {
                                nodes {
                                    nodeId
                                    id
                                    transaction
                                    userByUserId {
                                        icon
                                        id
                                        name
                                        nodeId
                                    }
                                }
                            }
                            userByUserId {
                                createdAt
                                icon
                                id
                                name
                                nodeId
                            }
                        }
                    }
                }
            `);
            
            let millisPostRequest = new Date().getTime();
            
            
            
                        
            /////////////////////////////////////////////////////////////
                        
            let extractBranchTxIds = ${TwetchQueryUtil.printBranchTxExtractionFunction()};
            let postFetchingFunction = ${TwetchQueryUtil.printPostFetchingFunction()};
            
            
            let referencedPostsMap = {};
            var postArray = [];
            
            
            postArray = queryResult.allPosts.nodes;
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts0 = await postFetchingFunction(referencedPostsMap);
           
            
            let millisPostPostFetchingFunction = new Date().getTime();
            
            
            //
            //
            //must also fetch branches-of-branches
            postArray = Object.values(foundPosts0);
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts1 = await postFetchingFunction(referencedPostsMap);
            
            queryResult.refPostMap = Object.assign({}, foundPosts0, foundPosts1);
            
            
            let millisPostPostPostFetchingFunction = new Date().getTime();
            
            
            
            console.log("time spent awaiting twetch-auth:" 
                + (millisPostAuth - millisStart) 
                + "ms; time spent on post-list:" 
                + (millisPostRequest - millisPostAuth) 
                + "ms; time spent on referenced posts:" 
                + (millisPostPostFetchingFunction - millisPostRequest) 
                + "ms; time spent on references posts of referenced posts:"
                + (millisPostPostPostFetchingFunction - millisPostPostFetchingFunction)
                + "ms");
            
            
            
            return JSON.stringify(queryResult);
            
        """.trimIndent()
    }

    private fun assembleLatestFeedRequestingNodeScript(
        cursor: String?,
        pageSize: Int
    ): String {
        val afterCursor = if (cursor == null) {
            "null"
        } else {
            "\"$cursor\""
        }

        return """
            let millisStart = new Date().getTime();
            
            await context.twetch.ensureAuthHasCompleted();
            
            let millisPostAuth = new Date().getTime();
            
            let queryResult = await context.twetch.instance.query(`
                {
                    allPosts(first: $pageSize, after: $afterCursor, orderBy: CREATED_AT_DESC, filter: {bContentType:{isNull:false}}) {
                        totalCount
                        pageInfo {
                            hasNextPage
                            endCursor
                        }
                        nodes {
                            bContent
                            bContentType
                            createdAt
                            id
                            mapComment
                            nodeId
                            numBranches: branchCount
                            numLikes: likeCount
                            replyPostId
                            transaction
                            userId
                            youBranched
                            youLiked
                            mapTwdata
                            files
                            mediaByPostId {
                                nodes {
                                    filename
                                }
                            }
                            postsByReplyPostId {
                                totalCount
                            }
                            parents {
                                nodes {
                                    nodeId
                                    id
                                    transaction
                                    userByUserId {
                                        icon
                                        id
                                        name
                                        nodeId
                                    }
                                }
                            }
                            userByUserId {
                                createdAt
                                icon
                                id
                                name
                                nodeId
                            }
                        }
                    }
                }
            `);
            
            let millisPostRequest = new Date().getTime();
            
            
            /////////////////////////////////////////////////////////////
            
            let extractBranchTxIds = ${TwetchQueryUtil.printBranchTxExtractionFunction()};
            let postFetchingFunction = ${TwetchQueryUtil.printPostFetchingFunction()};
            
            
            var referencedPostsMap = {};
            var postArray = [];
            
            

            postArray = queryResult.allPosts.nodes;
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts0 = await postFetchingFunction(referencedPostsMap);
            
            
            let millisPostPostFetchingFunction = new Date().getTime();
            
            
            //
            //
            //must also fetch branches-of-branches
            postArray = Object.values(foundPosts0);
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts1 = await postFetchingFunction(referencedPostsMap);
            
            queryResult.refPostMap = Object.assign({}, foundPosts0, foundPosts1);
            
            
            let millisPostPostPostFetchingFunction = new Date().getTime();
            
            
            
            console.log("time spent awaiting twetch-auth:" 
                + (millisPostAuth - millisStart) 
                + "ms; time spent on post-list:" 
                + (millisPostRequest - millisPostAuth) 
                + "ms; time spent on referenced posts:" 
                + (millisPostPostFetchingFunction - millisPostRequest) 
                + "ms; time spent on references posts of referenced posts:"
                + (millisPostPostPostFetchingFunction - millisPostPostFetchingFunction)
                + "ms");
            
            

            return JSON.stringify(queryResult);
            
        """.trimIndent()
    }

    private fun assembleMyFollowingFeedRequestingNodeScript(
        afterTime: String,
        pageSize: Int
    ): String {
        return """
            let millisStart = new Date().getTime();
            
            await context.twetch.ensureAuthHasCompleted();
            
            let millisPostAuth = new Date().getTime();
            
            let queryResult = await context.twetch.instance.query(`
                {
                    myFollowingFeed(afterTime: "$afterTime", first: $pageSize) {
                        totalCount
                        nodes {
                            bContent
                            bContentType
                            createdAt
                            id
                            mapComment
                            nodeId
                            numBranches: branchCount
                            numLikes: likeCount
                            replyPostId
                            transaction
                            userId
                            youBranched
                            youLiked
                            mapTwdata
                            files
                            mediaByPostId {
                                nodes {
                                    filename
                                }
                            }
                            postsByReplyPostId {
                                totalCount
                            }
                            parents {
                                nodes {
                                    nodeId
                                    id
                                    transaction
                                    userByUserId {
                                        icon
                                        id
                                        name
                                        nodeId
                                    }
                                }
                            }
                            userByUserId {
                                createdAt
                                icon
                                id
                                name
                                nodeId
                            }
                        }
                    }
                }
            `);
            
            let millisPostRequest = new Date().getTime();
            
            
            
                        
            /////////////////////////////////////////////////////////////
            
            let extractBranchTxIds = ${TwetchQueryUtil.printBranchTxExtractionFunction()};
            let postFetchingFunction = ${TwetchQueryUtil.printPostFetchingFunction()};
            
            
            var referencedPostsMap = {};
            var postArray = [];
            
            
            
            
            postArray = queryResult.myFollowingFeed.nodes;
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts0 = await postFetchingFunction(referencedPostsMap);
            
            
            let millisPostPostFetchingFunction = new Date().getTime();
            
            
            //
            //
            //must also fetch branches-of-branches
            postArray = Object.values(foundPosts0);
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts1 = await postFetchingFunction(referencedPostsMap);
            
            queryResult.refPostMap = Object.assign({}, foundPosts0, foundPosts1);
            
            
            let millisPostPostPostFetchingFunction = new Date().getTime();
            
            
            
            console.log("time spent awaiting twetch-auth:" 
                + (millisPostAuth - millisStart) 
                + "ms; time spent on post-list:" 
                + (millisPostRequest - millisPostAuth) 
                + "ms; time spent on referenced posts:" 
                + (millisPostPostFetchingFunction - millisPostRequest) 
                + "ms; time spent on references posts of referenced posts:"
                + (millisPostPostPostFetchingFunction - millisPostPostFetchingFunction)
                + "ms");
            
            
            return JSON.stringify(queryResult);
            
        """.trimIndent()
    }

    private fun assembleUserProfilePostRequestingNodeScript(
        userId: String,
        includeReplies: Boolean,
        cursor: String?,
        pageSize: Int
    ): String {
        val afterCursor = if (cursor == null) {
            "null"
        } else {
            "\"$cursor\""
        }

        val includeRepliesQueryValue = if (includeReplies) {
            "false"
        } else {
            "true"
        }

        return """
            let millisStart = new Date().getTime();
            
            await context.twetch.ensureAuthHasCompleted();
            
            let millisPostAuth = new Date().getTime();
            
            
            let queryResult = await context.twetch.instance.query(`
                {
                    allPosts(first: $pageSize, after: $afterCursor, condition: {userId: "$userId"}, orderBy: CREATED_AT_DESC, filter: {bContentType: {isNull: false}, replyPostId: {isNull: $includeRepliesQueryValue}, userId: {equalTo: "$userId"}}) {
                        totalCount
                        pageInfo {
                            hasNextPage
                            endCursor
                        }
                        nodes {
                            bContent
                            bContentType
                            createdAt
                            id
                            mapComment
                            nodeId
                            numBranches: branchCount
                            numLikes: likeCount
                            replyPostId
                            transaction
                            userId
                            youBranched
                            youLiked
                            mapTwdata
                            files
                            mediaByPostId {
                                nodes {
                                    filename
                                }
                            }
                            postsByReplyPostId {
                                totalCount
                            }
                            parents {
                                nodes {
                                    nodeId
                                    id
                                    transaction
                                    userByUserId {
                                        icon
                                        id
                                        name
                                        nodeId
                                    }
                                }
                            }
                            userByUserId {
                                createdAt
                                icon
                                id
                                name
                                nodeId
                            }
                        }
                    }
                }
            `);
            
            
            let millisPostRequest = new Date().getTime();
            
            
            /////////////////////////////////////////////////////////////
            
            let extractBranchTxIds = ${TwetchQueryUtil.printBranchTxExtractionFunction()};
            let postFetchingFunction = ${TwetchQueryUtil.printPostFetchingFunction()};
            
            
            var referencedPostsMap = {};
            var postArray = [];
            
            
            
            postArray = queryResult.allPosts.nodes;
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts0 = await postFetchingFunction(referencedPostsMap);
            
            
            let millisPostPostFetchingFunction = new Date().getTime();
            
            
            //
            //
            //must also fetch branches-of-branches
            postArray = Object.values(foundPosts0);
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts1 = await postFetchingFunction(referencedPostsMap);
            
            queryResult.refPostMap = Object.assign({}, foundPosts0, foundPosts1);
            
            
            let millisPostPostPostFetchingFunction = new Date().getTime();
            
            
            
            console.log("time spent awaiting twetch-auth:" 
                + (millisPostAuth - millisStart) 
                + "ms; time spent on post-list:" 
                + (millisPostRequest - millisPostAuth) 
                + "ms; time spent on referenced posts:" 
                + (millisPostPostFetchingFunction - millisPostRequest) 
                + "ms; time spent on references posts of referenced posts:"
                + (millisPostPostPostFetchingFunction - millisPostPostFetchingFunction)
                + "ms");
            
            
            return JSON.stringify(queryResult);
        """.trimIndent()
    }


    private fun assembleUserProfileLikesRequestingNodeScript(
        userId: String,
        cursor: String?,
        pageSize: Int
    ): String {
        val afterCursor = if (cursor == null) {
            "null"
        } else {
            "\"$cursor\""
        }

        return """
            let millisStart = new Date().getTime();
            
            await context.twetch.ensureAuthHasCompleted();
            
            let millisPostAuth = new Date().getTime();
            
            
            let queryResult = await context.twetch.instance.query(`
                {
                    allLikes(orderBy: CREATED_AT_DESC, first: $pageSize, after: $afterCursor, condition: {userId: "$userId"}) {
                        totalCount
                        pageInfo {
                            hasNextPage
                            endCursor
                        }
                        nodes {
                            nodeId
                            userId
                            postId
                            id
                            postByPostId {
                                bContent
                                bContentType
                                createdAt
                                id
                                mapComment
                                nodeId
                                numBranches: branchCount
                                numLikes: likeCount
                                replyPostId
                                transaction
                                userId
                                youBranched
                                youLiked
                                mapTwdata
                                files
                                mediaByPostId {
                                    nodes {
                                        filename
                                    }
                                }
                                postsByReplyPostId {
                                    totalCount
                                }
                                parents {
                                    nodes {
                                        nodeId
                                        id
                                        transaction
                                        userByUserId {
                                            icon
                                            id
                                            name
                                            nodeId
                                        }
                                    }
                                }
                                userByUserId {
                                    createdAt
                                    icon
                                    id
                                    name
                                    nodeId
                                }
                            }
                        }
                    }
                }
            `);
            
            
            let millisPostRequest = new Date().getTime();
            
            
            
            
            /////////////////////////////////////////////////////////////
            
            let extractBranchTxIds = ${TwetchQueryUtil.printBranchTxExtractionFunction()};
            let postFetchingFunction = ${TwetchQueryUtil.printPostFetchingFunction()};
            
            
            var referencedPostsMap = {};
            var postArray = [];
            
            
 
            postArray = queryResult.allLikes.nodes;
            postArray.forEach((likeItem) => {
                let item = likeItem.postByPostId;
                if (item == null) return;
                
                postArray.push(item);
            });
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts0 = await postFetchingFunction(referencedPostsMap);
            
            
            let millisPostPostFetchingFunction = new Date().getTime();
            
            
            //
            //
            //must also fetch branches-of-branches
            postArray = Object.values(foundPosts0);
            referencedPostsMap = extractBranchTxIds(postArray);
            let foundPosts1 = await postFetchingFunction(referencedPostsMap);
            
            queryResult.refPostMap = Object.assign({}, foundPosts0, foundPosts1);
            
            
            let millisPostPostPostFetchingFunction = new Date().getTime();
            
            
            
            console.log("time spent awaiting twetch-auth:" 
                + (millisPostAuth - millisStart) 
                + "ms; time spent on post-list:" 
                + (millisPostRequest - millisPostAuth) 
                + "ms; time spent on referenced posts:" 
                + (millisPostPostFetchingFunction - millisPostRequest) 
                + "ms; time spent on references posts of referenced posts:"
                + (millisPostPostPostFetchingFunction - millisPostPostFetchingFunction)
                + "ms");
            
            
            return JSON.stringify(queryResult);
        """.trimIndent()
    }


    /////////////////////////////////////////////////////////////////



    private fun parseRefPostMap(
        jsonMap: JsonObject,
        activeUserId: String,
        millisNow: Long
    ): Map<String, PostModel> {
        val parsedRefPostMap = HashMap<String, PostModel>()

        jsonMap.keySet().forEach { key ->
            val post = PostModel.parsePost(
                gson,
                jsonMap[key].asJsonObject,
                activeUserId,
                millisNow
            )
            parsedRefPostMap[key] = post
        }

        return parsedRefPostMap
    }

    /////////////////////////////////////////////////////////////////

    private fun parseSearchPostListResponse(
        content: String,
        activeUserId: String
    ): Pair<List<PostHolder>, String?> {
        val responseJson = gson.fromJson(content, JsonObject::class.java)
        val postSearchJson = responseJson.getAsJsonArray("postSearch")

        val millisNow = System.currentTimeMillis()

        val refPostMap = if (responseJson.has("refPostMap")
            && !responseJson.get("refPostMap").isJsonNull) {
            parseRefPostMap(
                responseJson.getAsJsonObject("refPostMap"),
                activeUserId,
                millisNow
            )
        } else null



        val parsedPostsList = mutableListOf<PostHolder>()
        postSearchJson.forEach { jsonElement ->
            if (jsonElement.isJsonNull) return@forEach

            val post = PostModel.parsePost(
                gson,
                jsonElement.asJsonObject,
                activeUserId,
                millisNow
            )
            val referencedPost = refPostMap?.let {
                if (it.isNotEmpty()) it[post.txId] else null
            }

            val refMap = mutableMapOf<String, PostModel>()

            referencedPost?.let { p ->
                refMap[p.txId] = p
            }

            referencedPost?.referencedPost?.let {
                refPostMap[referencedPost.txId]?.let { p ->
                    refMap[p.txId] = p
                }
            }

            parsedPostsList.add(PostHolder(
                post,
                refMap
            ))
        }


        val nextMeta = if (parsedPostsList.isEmpty() || postSearchJson.size() < PAGE_SIZE) {
            null
        } else {
            "true"
        }

        return parsedPostsList to nextMeta
    }

    private fun parseMyFollowingFeedResponse(
        content: String,
        activeUserId: String
    ): Pair<List<PostHolder>, String?> {
        val responseJson = gson.fromJson(content, JsonObject::class.java)
        val myFollowingFeedJson = responseJson.getAsJsonObject("myFollowingFeed")

        val millisNow = System.currentTimeMillis()


        val refPostMap = if (responseJson.has("refPostMap")
            && !responseJson.get("refPostMap").isJsonNull) {
            parseRefPostMap(
                responseJson.getAsJsonObject("refPostMap"),
                activeUserId,
                millisNow
            )
        } else null


        //
        val nodesJson = myFollowingFeedJson.getAsJsonArray("nodes")

        val parsedPostsList = mutableListOf<PostHolder>()
        var lastCreatedAt: String? = null

        nodesJson.forEach { jsonElement ->
            val jsonPost = jsonElement.asJsonObject
            val post = PostModel.parsePost(
                gson,
                jsonPost,
                activeUserId,
                millisNow
            )

            val referencedPost = refPostMap?.let {
                if (it.isNotEmpty()) it[post.txId] else null
            }

            val refMap = mutableMapOf<String, PostModel>()

            referencedPost?.let { p ->
                refMap[p.txId] = p
            }

            referencedPost?.referencedPost?.let {
                refPostMap[referencedPost.txId]?.let { p ->
                    refMap[p.txId] = p
                }
            }

            parsedPostsList.add(PostHolder(
                post,
                refMap
            ))

            //necessary for fetching of the next page
            if (jsonPost.has("createdAt")) {
                val createdAt = jsonPost.get("createdAt")
                if (!createdAt.isJsonNull) {
                    lastCreatedAt = createdAt.asString
                }
            }
        }

        if (nodesJson.size() < PAGE_SIZE) {
            lastCreatedAt = null
        }



        return parsedPostsList to lastCreatedAt
    }

    private fun parsePostListResponse(
        content: String,
        activeUserId: String
    ): Pair<List<PostHolder>, String?> {
        val responseJson = gson.fromJson(content, JsonObject::class.java)
        val allPostsJson = responseJson.getAsJsonObject("allPosts")

        val millisNow = System.currentTimeMillis()


        val refPostMap = if (responseJson.has("refPostMap")
            && !responseJson.get("refPostMap").isJsonNull) {
                parseRefPostMap(
                    responseJson.getAsJsonObject("refPostMap"),
                    activeUserId,
                    millisNow
                )
        } else null



        val pageInfoJson = allPostsJson.getAsJsonObject("pageInfo")
//        val hasNextPage = pageInfoJson.get("hasNextPage").asBoolean
        var endCursor = if (pageInfoJson.has("endCursor")) {
            val endCursorJson = pageInfoJson.get("endCursor")
            if (endCursorJson.isJsonNull) {
                null
            } else {
                endCursorJson.asString
            }
        } else null

        //
        val nodesJson = allPostsJson.getAsJsonArray("nodes")
        val parsedPostsList = mutableListOf<PostHolder>()
        nodesJson.forEach { jsonElement ->
            val post = PostModel.parsePost(
                gson,
                jsonElement.asJsonObject,
                activeUserId,
                millisNow
            )
            val referencedPost = refPostMap?.let {
                if (it.isNotEmpty()) it[post.txId] else null
            }

            val refMap = mutableMapOf<String, PostModel>()

            referencedPost?.let { p ->
                refMap[p.txId] = p
            }

            referencedPost?.referencedPost?.let {
                refPostMap[referencedPost.txId]?.let { p ->
                    refMap[p.txId] = p
                }
            }

            parsedPostsList.add(PostHolder(
                post,
                refMap
            ))
        }

        if (nodesJson.size() < PAGE_SIZE) {
            endCursor = null
        }

        return parsedPostsList to endCursor
    }

    private fun parseUserProfileLikesListResponse(
        content: String,
        activeUserId: String
    ): Pair<List<PostHolder>, String?> {
        val responseJson = gson.fromJson(content, JsonObject::class.java)
        val allLikesJson = responseJson.getAsJsonObject("allLikes")

        val millisNow = System.currentTimeMillis()

        val refPostMap = if (responseJson.has("refPostMap")
            && !responseJson.get("refPostMap").isJsonNull) {
            parseRefPostMap(
                responseJson.getAsJsonObject("refPostMap"),
                activeUserId,
                millisNow
            )
        } else null


        val pageInfoJson = allLikesJson.getAsJsonObject("pageInfo")
        var endCursor = if (pageInfoJson.has("endCursor")) {
            val endCursorJson = pageInfoJson.get("endCursor")
            if (endCursorJson.isJsonNull) {
                null
            } else {
                endCursorJson.asString
            }
        } else null

        if (pageInfoJson.has("hasNextPage")) {
            val endCursorJson = pageInfoJson.get("hasNextPage")
            if (endCursorJson.isJsonNull) {
                endCursor = null
            }
        } else {
            endCursor = null
        }


        val nodesJson = allLikesJson.getAsJsonArray("nodes")

        val parsedPostsList = mutableListOf<PostHolder>()

        nodesJson.forEach { jsonElement ->
            val jsonNodeObj = jsonElement.asJsonObject
            //val likeId = jsonNodeObj.get("id").asString

            val jsonPost = if (jsonNodeObj.has("postByPostId")) {
                val postJson = jsonNodeObj.get("postByPostId")
                if (postJson.isJsonNull) {
                    return@forEach
                } else {
                    postJson.asJsonObject
                }
            } else {
                return@forEach
            }

            val post = PostModel.parsePost(
                gson,
                jsonPost,
                activeUserId,
                millisNow
            )

            val referencedPost = refPostMap?.let {
                if (it.isNotEmpty()) it[post.txId] else null
            }

            val refMap = mutableMapOf<String, PostModel>()

            referencedPost?.let { p ->
                refMap[p.txId] = p
            }

            referencedPost?.referencedPost?.let {
                refPostMap[referencedPost.txId]?.let { p ->
                    refMap[p.txId] = p
                }
            }

            parsedPostsList.add(PostHolder(
                post,
                refMap
            ))


        }


        if (nodesJson.size() < PAGE_SIZE) {
            endCursor = null
        }


        return parsedPostsList to endCursor
    }





    /////////////////////////////////////////////////////////////////


    data class Key(
        val type: ListType,
        val activeUserId: String,
        val userId: String?,
        val query: String? = null
    ) {
        var page = 0

        override fun toString(): String {
            return "Key(type=$type, activeUserId='$activeUserId', userId=$userId, query=$query, page=$page)"
        }

    }

    data class PagingMeta(
        val lastPage: Int,
        val afterCursor: String?,
        val didError: Boolean,
        var afterTime: String? = null
    )

}




enum class ListType {
    SEARCH,
    PROFILE_LATEST,
    PROFILE_REPLIES,
    PROFILE_LIKES,
    FOLLOWING,
    LATEST,
    TOP_HOUR,
    TOP_DAY,
    TOP_WEEK,
    TOP_MONTH,
    TOP_ALL
}