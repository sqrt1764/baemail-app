package app.bitcoin.baemail.twetch.ui

import android.content.Context
import androidx.annotation.Keep
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.data.util.twetchChatInfoStore
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.dropbox.android.external.store4.StoreBuilder
import com.dropbox.android.external.store4.Store
import com.dropbox.android.external.store4.Fetcher
import com.dropbox.android.external.store4.SourceOfTruth
import com.dropbox.android.external.store4.StoreRequest
import com.dropbox.android.external.store4.StoreResponse
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.threeten.bp.ZonedDateTime
import timber.log.Timber
import kotlin.RuntimeException

class TwetchChatInfoStore(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val gson: Gson
) {

    companion object {

        fun findAddedChats(
            latestList: List<ChatActivityInfo>,
            currentList: List<ChatActivityInfo>
        ): List<String> {
            return latestList.filter { latestInfo ->
                val matchingCurrent = currentList.find { currentInfo ->
                    latestInfo.chatId == currentInfo.chatId
                }

                return@filter matchingCurrent == null
            }.map { it.chatId }
        }

        fun findRemovedChats(
            latestList: List<ChatActivityInfo>,
            currentList: List<ChatActivityInfo>
        ): List<String> {
            return currentList.filter { currentInfo ->
                val matchingLatest = latestList.find { latestInfo ->
                    latestInfo.chatId == currentInfo.chatId
                }

                return@filter matchingLatest == null
            }.map { it.chatId }
        }

        fun findFindNewMessages(
            latestList: List<ChatActivityInfo>,
            currentList: List<ChatActivityInfo>
        ): List<ChatNewMessageCount> {
            return latestList.mapNotNull { latestInfo ->
                val matchingCurrent = currentList.find { currentInfo ->
                    latestInfo.chatId == currentInfo.chatId
                } ?: return@mapNotNull null

                val unreadCountDiff = latestInfo.unreadCount - matchingCurrent.unreadCount
                if (latestInfo.millisLastMessageAt <= matchingCurrent.millisLastMessageAt) {
                    return@mapNotNull null
                }

                return@mapNotNull ChatNewMessageCount(
                    latestInfo.chatId,
                    unreadCountDiff,
                    latestInfo.millisLastMessageAt,
                    latestInfo.unreadCount
                )
            }
        }

    }

    val ds: DataStore<Preferences> = appContext.twetchChatInfoStore

    private val addedListeners = mutableListOf<ListenerHolder>()

    fun clearAllListeners() {
        addedListeners.clear()
    }

    fun addListeners(
        paymail: String,
        onNewMessagesDetected: suspend (List<ChatNewMessageCount>) -> Unit,
        onRemovedChatsDetected: suspend (List<String>) -> Unit
    ) {
        val matchingExisting = addedListeners.find { it.paymail == paymail }
        if (matchingExisting != null) throw RuntimeException()

        addedListeners.add(
            ListenerHolder(
                paymail,
                onNewMessagesDetected,
                onRemovedChatsDetected
            )
        )
    }


    private val chatInfoStore: Store<ChatInfoKey, List<ChatInfo>> by lazy {
        StoreBuilder.from(
                fetcher = Fetcher.ofFlow { filterKey ->
                    Timber.d("chatInfoStore#fetcherOfFlow $filterKey")
                    return@ofFlow queryServer(filterKey)
                },
                sourceOfTruth = SourceOfTruth.of(
                        reader = ::queryByFilter,
                        writer = ::insertAll,
                        delete = ::clearByKey,
                        deleteAll = ::deleteAll
                )

        ).build()
    }

    private fun queryServer(key: ChatInfoKey): Flow<List<ChatInfo>> {
        return flow {
            //fetch current state from db
            val cachedInfo = mutableListOf<ChatActivityInfo>()
            val chatList = queryByFilter(key).first()
            chatList.map { chat ->
                ChatActivityInfo(
                    chat.id,
                    chat.millisLastMessageAt,
                    chat.unreadCount
                )
            }.let {
                cachedInfo.clear()
                cachedInfo.addAll(it)
            }




            val queriedList = mutableListOf<ChatInfo>()

            val limit = 30
            var offset = 0

            for (i in 0 until 10) { //todo currently stops fetching after 10 pages of chats
                val result = nodeRuntimeRepository.runAsyncScript("""
                    var millisStart = new Date().getTime();
                    
                    await context.twetch.ensureAuthHasCompleted();
                    
                    
                    var millisPostAuth = new Date().getTime();
                    
                    let queryResult = await context.twetch.instance.query(`
                        {
                            allConversations(first: $limit, offset: $offset) {
                                nodes {
                                    id
                                    name
                                    lastMessageAt
                                    isGroup
                                    icon
                                    me {
                                        encryptedKey
                                    }
                                    unreadCount
                                    conversationsUsersByConversationId(
                                        orderBy: USER_ID_ASC
                                        first: 2
                                        filter: {userId: {notEqualTo: "${key.userId}"}}
                                    ) {
                                        nodes {
                                            userByUserId {
                                                id
                                                name
                                                publicKey
                                                icon
                                                dmConversationId
                                            }
                                        }
                                    }
                                }
                                pageInfo {
                                    hasNextPage
                                    startCursor
                                }
                                totalCount
                            }
                        }
                    `);
                    
                    var millisPostRequest = new Date().getTime();
                    console.log("time spent awaiting twetch-auth:" + (millisPostAuth - millisStart) + "ms; time spent on chat-list-request:" + (millisPostRequest - millisPostAuth) + "ms");
                    
                    return JSON.stringify(queryResult);
                """.trimIndent())

                if (result is RunScript.Response.Success) {

                    val (hasNextPage, infoList) = try {
                        parseSuccessResponse(result.content, key.userId)
                    } catch (e: Exception) {
                        Timber.d(e)
                        throw e
                    }
                    queriedList.addAll(infoList)

                    if (hasNextPage) {
                        offset += limit
                    } else {
                        break
                    }


                } else {
                    result as RunScript.Response.Fail
                    throw result.throwable
                    //break
                }

            }



            //find diff in the meta-state of all chats

            val queriedActivityList = queriedList.map { chat ->
                ChatActivityInfo(
                    chat.id,
                    chat.millisLastMessageAt,
                    chat.unreadCount
                )
            }.toCollection(mutableListOf())

            val removedChats = findRemovedChats(
                queriedActivityList,
                cachedInfo
            )

            val changeList = findFindNewMessages(
                queriedActivityList,
                cachedInfo
            )





            //satisfy the flow
            emit(queriedList)







            //inform the registered listeners
            if (removedChats.isNotEmpty()) {
                removeChats(key.paymail, removedChats)

                addedListeners.forEach {
                    it.onRemovedChatsDetected.invoke(removedChats)
                }
            }

            if (changeList.isNotEmpty()) {
                addedListeners.forEach {
                    it.onNewMessagesDetected.invoke(changeList)
                }
            }

        }
    }


    fun queryByFilter(key: ChatInfoKey): Flow<List<ChatInfo>> {
        return ds.data.mapLatest { prefs ->
            val chatsOfPaymail = prefs.asMap().mapNotNull { (mapKey, value) ->
                if (mapKey.name.startsWith("chat_${key.paymail}_")) {
                    value as String
                } else {
                    null
                }
            }

            val list = chatsOfPaymail.mapNotNull { serialized ->
                try {
                    val chat = gson.fromJson(serialized, ChatInfo::class.java)

                    if (chat.name == null) return@mapNotNull null

                    chat
                } catch (e: Exception) {
                    Timber.e(e)
                    return@mapNotNull null
                }


            }.sortedByDescending { it.millisLastMessageAt } //TODO maybe the unread count should also be relevant in sorting?
            return@mapLatest list
        }
    }

    private suspend fun deleteAll() {
        ds.edit { prefs ->
            prefs.asMap().keys.forEach {
                Timber.d("removing key: $it")
                prefs.remove(it)
            }

        }
    }

    private suspend fun insertAll(key: ChatInfoKey, info: List<ChatInfo>) {
        ds.edit { prefs ->
            info.forEach { chat ->
                prefs[stringPreferencesKey("chat_${key.paymail}_${chat.id}")] = gson.toJson(chat)
            }
        }
    }


    private suspend fun clearByKey(key: ChatInfoKey) {
        ds.edit { prefs ->
            prefs.asMap().keys.forEach { prefKey ->
                if (prefKey.name.startsWith("chat_${key.paymail}_")) {
                    prefs.remove(prefKey)
                }
            }
        }
    }

    suspend fun updateInfoOnMessagesSeen(
        paymail: String,
        chatId: String
    ) {
        ds.edit { prefs ->

            val key = prefs[stringPreferencesKey("chat_${paymail}_${chatId}")]
            val info = gson.fromJson(key, ChatInfo::class.java)

            prefs[stringPreferencesKey("chat_${paymail}_${chatId}")] = gson.toJson(info.copy(
                unreadCount = 0
            ))
        }
    }


    /////////////////////////////////////////////////////////////////


    private fun parseSuccessResponse(content: String, activeuserId: String): Pair<Boolean, List<ChatInfo>> {
        val responseJson = gson.fromJson(content, JsonObject::class.java)
        val allConversationsJson = responseJson.getAsJsonObject("allConversations")
        val pageInfo = allConversationsJson.getAsJsonObject("pageInfo")
        val hasNextPage = pageInfo.get("hasNextPage").asBoolean

        val totalCount = allConversationsJson.get("totalCount").asInt

        val nodes = allConversationsJson.getAsJsonArray("nodes")

        val parsedChatList = mutableListOf<ChatInfo>()

        nodes.forEach { it ->
            val node = it.asJsonObject
            val id = node.get("id").asString
            val isGroup = node.get("isGroup").asBoolean

            val conversationsUsersArray = node
                .getAsJsonObject("conversationsUsersByConversationId")
                .getAsJsonArray("nodes")
                .map { item ->
                    val userJson = item.asJsonObject.getAsJsonObject("userByUserId")
                    val userId = userJson.get("id").asString
                    val userName = if (userJson.has("name")) {
                        userJson.get("name").let {
                            if (it.isJsonNull) "@$userId" else it.asString
                        }
                    } else {
                        "@$userId"
                    }
                    val userIcon = if (userJson.has("icon")) {
                        userJson.get("icon").let {
                            if (it.isJsonNull) null else it.asString
                        }
                    } else null

                    userName to userIcon
                }

            var name = if (node.has("name")) {
                node.get("name").let {
                    if (it.isJsonNull) null else it.asString
                }
            } else null

            if (name == null) {
                name = if (isGroup) {
                    val sb = StringBuilder()
                    conversationsUsersArray.forEachIndexed { index, userPair ->
                        if (index != 0) {
                            sb.append(", ")
                        }
                        sb.append(userPair.first)
                    }

                    if (totalCount > conversationsUsersArray.size + 1) {
                        val remaining = totalCount - conversationsUsersArray.size
                        sb.append(" +").append(remaining)
                    }
                    sb.toString()

                } else {
                    if (conversationsUsersArray.isEmpty()) {
                        //me-chat
                        "@$activeuserId"
                    } else conversationsUsersArray[0].first
                }
                name !!
            }


            val lastMessageAt = node.get("lastMessageAt").asString
            val millisLastMessageAt = lastMessageAt.let {
                try {
                    ZonedDateTime.parse(it).toInstant().toEpochMilli()
                } catch (e: Exception) {
                    Timber.e(e)
                    -1
                }
            }
            var icon = if (node.has("icon")) {
                node.get("icon").let {
                    if (it.isJsonNull) null else it.asString
                }
            } else null
            if (icon == null && !isGroup && conversationsUsersArray.isNotEmpty()) {
                icon = conversationsUsersArray[0].second
            }

            val unreadCount = node.get("unreadCount").asInt

            val meEncryptedKey = node.get("me").asJsonObject.get("encryptedKey").asString

            val isSelfChat = conversationsUsersArray.isEmpty()

            parsedChatList.add(ChatInfo(
                id,
                name,
                millisLastMessageAt,
                isGroup,
                icon,
                isSelfChat,
                meEncryptedKey,
                unreadCount
            ))

        }

        return hasNextPage to parsedChatList
    }


    /////////////////////////////////////////////////////////////////


    fun getChatFlow(
            paymail: String,
            chatId: String
    ): Flow<ChatInfo?> {
        return ds.data.mapLatest { prefs ->
            prefs[stringPreferencesKey("chat_${paymail}_${chatId}")]?.let {
                gson.fromJson(it, ChatInfo::class.java)
            }
        }
    }

    suspend fun getAllForKey(
            key: ChatInfoKey
    ): List<ChatInfo> {
        return queryByFilter(key).first()
    }

    suspend fun removeChats(
            paymail: String,
            toRemove: List<String>
    ) {
        //delete the chats
        ds.edit { prefs ->

            toRemove.forEach { chatId ->
                val key = stringPreferencesKey("chat_${paymail}_${chatId}")
                prefs.remove(key)
            }
        }
        Timber.d("chats of user removed: ${toRemove.size}")
    }

    suspend fun updateChats(
        key: ChatInfoKey,
        changeList: List<ChatNewMessageCount>
    ) {
        val prefs = ds.data.first()
        val updatedMatchingChats = mutableListOf<ChatInfo>()

        changeList.forEach { info ->
            //if (info.unreadCount == 0) return@forEach

            val prefsKey = stringPreferencesKey("chat_${key.paymail}_${info.chatId}")
            val currentChatInfo = prefs[prefsKey]!!.let {
                gson.fromJson(it, ChatInfo::class.java)
            }
            updatedMatchingChats.add(
                currentChatInfo.copy(
                    millisLastMessageAt = info.millisLastMessageAt,
                    unreadCount = info.unreadCount
                )
            )

        }

        insertAll(
            key,
            updatedMatchingChats
        )

        //todo consider querying the message-db to find out the millisCreatedAt of it & do further filtering of `updatedMatchingChats`


        Timber.d("updated `latestActivityInfo` of chats: ${updatedMatchingChats.size}")
    }



    fun getFlowForKey(
            key: ChatInfoKey,
            refresh: Boolean = true
    ): Flow<StoreResponse<List<ChatInfo>>> {
        return chatInfoStore.stream(StoreRequest.cached(key, refresh))
    }


    suspend fun refreshChatList(key: ChatInfoKey): Boolean {
        return try {
            val list = queryServer(key).first()
            insertAll(key, list)
            Timber.d("refreshChatList paymail:${key.paymail} completed successfully")
            true
        } catch (e: Exception) {
            Timber.e(e, "refreshChatList paymail:${key.paymail} crashed")
            false
        }
    }


    data class ChatInfoKey(
            val paymail: String,
            val userId: String
    )

    @Keep
    data class ChatInfo(
        @SerializedName("id") val id: String,
        @SerializedName("name")val name: String,
        @SerializedName("millisLastMessageAt") val millisLastMessageAt: Long,
        @SerializedName("isGroup") val isGroup: Boolean,
        @SerializedName("icon") val icon: String?,
        @SerializedName("selfChat") val selfChat: Boolean,
        @SerializedName("encryptedKey") val meEncryptedKey: String,
        @SerializedName("unreadCount") val unreadCount: Int
    )

    @Keep
    data class Participant(
        @SerializedName("id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("publicKey") val publicKey: String,
        @SerializedName("icon") val icon: String?,
        @SerializedName("dmConversationId") val dmConversationId: String?
    )


    data class ChatActivityInfo(
            val chatId: String,
            val millisLastMessageAt: Long,
            val unreadCount: Int
    )

    data class ChatNewMessageCount(
            val chatId: String,
            val count: Int,
            val millisLastMessageAt: Long,
            val unreadCount: Int
    )

    data class ListenerHolder(
        val paymail: String,
        val onNewMessagesDetected: suspend (List<ChatNewMessageCount>) -> Unit,
        val onRemovedChatsDetected: suspend (List<String>) -> Unit
    )
}