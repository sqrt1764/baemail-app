package app.bitcoin.baemail.twetch.ui

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import app.bitcoin.baemail.core.data.wallet.BlockchainDataSource
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.data.util.twetchFilesMimeCachingPrefs
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import timber.log.Timber

class TwetchFilesMimeTypeHelper(
    val appContext: Context,
    val coroutineUtil: CoroutineUtil,
    val blockchainDataSource: BlockchainDataSource
) {

    private val ds = appContext.twetchFilesMimeCachingPrefs

    private val memoryCache = HashMap<String, Holder>()

    fun ensureMemoryCacheItem(txId: String): Holder {
        memoryCache[txId]?.let {
            return it
        }


        val holder = Holder(
                txId,
                MutableStateFlow(State.UNKNOWN_CHECKING)
        )
        memoryCache[txId] = holder

        coroutineUtil.appScope.launch {
            //read from db; if not found start a query
            val matchingMime = ds.data.first()[stringPreferencesKey(txId)]
            if (matchingMime != null) {
                Timber.d("local ...gotMime: $matchingMime $txId")

                if (matchingMime.startsWith("video")) {
                    holder.state.value = State.VIDEO
                } else {
                    holder.state.value = State.IMAGE
                }
                return@launch
            }

            //query to find out
            try {
                val gotMime = blockchainDataSource.getTwetchFilesMimeType(txId)
                Timber.d("...gotMime: $gotMime $txId")
                if (gotMime == null) throw RuntimeException()

                //persist locally
                ds.edit { prefs ->
                    prefs[stringPreferencesKey(txId)] = gotMime
                }

                //update ram-state
                if (gotMime.startsWith("video")) {
                    holder.state.value = State.VIDEO
                } else {
                    holder.state.value = State.IMAGE
                }


            } catch (e: Exception) {
                Timber.e(e)
            }

        }

        return holder
    }

    fun checkFilesContent(listTxId: List<String>) {
        listTxId.forEach { ensureMemoryCacheItem(it) }
    }

    data class Holder(
        val key: String,
        val state: MutableStateFlow<State>
    )

    enum class State {//TODO AUDIO
        IMAGE,
        VIDEO,
        UNKNOWN_CHECKING
    }
}