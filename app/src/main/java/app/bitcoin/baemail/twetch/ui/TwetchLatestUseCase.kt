package app.bitcoin.baemail.twetch.ui

import android.os.Parcelable
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest

@FlowPreview
@ExperimentalCoroutinesApi
class TwetchLatestUseCase(
    private val activeUserId: String,
    private val coroutineUtil: CoroutineUtil,
    private val postListStore: TwetchPostListStore
) {

    companion object {
        const val PAGE_NOT_LOADED = -1
    }

    private var isSetup = false

    private val _content = MutableStateFlow(Model(
        null
    ))
    val content: StateFlow<Model>
        get() {
            ensureSetup()
            return _content
        }



    private val _metaState = MutableStateFlow(MetaState(
        true,
        false
    ))
    val metaState: StateFlow<MetaState>
        get() = _metaState


    var postListDataObservationJob: Job? = null
    var postListRefreshingObservationJob: Job? = null

    var cachedListState: Parcelable? = null

    private val followingListHelper = TwetchPostListHelper(
        activeUserId,
        null,
        ListType.LATEST,
        coroutineUtil,
        postListStore
    )

    fun onRefreshRequested() {
        followingListHelper.refreshFromStart()
    }


    private fun ensureSetup() {
        if (isSetup) {
            return
        }
        isSetup = true

        followingListHelper.ensureSetup()

        postListDataObservationJob?.cancel()
        postListDataObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            //todo maybe consider using kotlin-channels??
            followingListHelper.data.collectLatest { model ->
                delay(15)
                _content.value = _content.value.copy(
                    list = model
                )
            }
        }

        postListRefreshingObservationJob?.cancel()
        postListRefreshingObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            //todo maybe consider using kotlin-channels??
            followingListHelper.refreshing.collect { meta ->
                _metaState.value = _metaState.value.copy(
                    listRefreshing = meta
                )
            }

        }

    }

    fun requestNextPage() {
        followingListHelper.userRequestedNextPage()
    }

    fun cleanup() {
        isSetup = false

        postListDataObservationJob?.cancel()
        postListRefreshingObservationJob?.cancel()

        followingListHelper.cleanup()

        _content.value = Model(
            null
        )
        _metaState.value = MetaState(
            true,
            false
        )
    }





    data class Model(
        val list: TwetchPostModel?,
    )

    data class MetaState(
        val listRefreshing: Boolean,
        val listError: Boolean
    )


}