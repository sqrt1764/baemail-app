package app.bitcoin.baemail.twetch.ui

import android.os.Parcelable
import android.util.Log
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.twetch.db.entity.TwetchChatMessage
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.dropbox.android.external.store4.ResponseOrigin
import com.dropbox.android.external.store4.StoreResponse
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.threeten.bp.ZonedDateTime
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class TwetchChatContentUseCase(
    private val paymail: String,
    val chatId: String,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val detectorManager: TwetchNotificationDetectorManager,
    private val twetchChatMessageStore: TwetchChatMessageStore,
    private val twetchChatInfoStore: TwetchChatInfoStore,
    private val coroutineUtil: CoroutineUtil,
    private val gson: Gson,
    private val getChatInfo: (chatId: String) -> Flow<TwetchChatInfoStore.ChatInfo?>,
    private val getInitializedForPaymail: () -> String,
    private val getActiveProfile: () -> TwetchAuthStore.TwetchProfile?
) {

    private var isSetup = false
    private var page = -1
    private var completedPageFetch = MutableStateFlow(page)
    private var collectMessagesJob: Job? = null
    private var collectContentJob: Job? = null

    private var delayedPostMessageSendRefreshJob: Job? = null

    var cachedOutputState: Parcelable? = null

    private val informChatSeenMap = HashMap<String, ChatSeenInfoHolder>()

    private val _chatMessagesRefreshing = MutableStateFlow(true)
    val chatMessagesRefreshing: StateFlow<Boolean>
        get() = _chatMessagesRefreshing

    private val _chatMessagesError = MutableStateFlow(false)
    val chatMessagesError: StateFlow<Boolean>
        get() = _chatMessagesError



    private val _chatContent = MutableStateFlow<ChatContentModel?>(null)
    val chatContent: StateFlow<ChatContentModel?>
        get() {
            ensureSetup()
            return _chatContent
        }



    private val _messageInput = MutableStateFlow("")
    val messageInput: StateFlow<String>
        get() = _messageInput




    private val _messageSendingState = MutableStateFlow<List<SendingTwetchMessage>>(listOf())
    val messageSendingState: StateFlow<List<SendingTwetchMessage>>
        get() = _messageSendingState




    private val _chatDisabledState = MutableStateFlow(false)
    val chatDisabledState: StateFlow<Boolean>
        get() = _chatDisabledState





    private val _chatMessages = MutableStateFlow<StoreResponse<List<TwetchChatMessage>>>(
        StoreResponse.Loading(ResponseOrigin.Cache)
    )

    private fun getPage(): Int = page

    private fun checkHasMorePages(): Boolean {
        return twetchChatMessageStore.checkChatHasMorePages(paymail, chatId)
    }

    private fun checkHasFetchError(): Boolean {
        return twetchChatMessageStore.checkChatHasError(paymail, chatId)
    }


    var enqueuedGetNextPageJob: Job? = null

    fun reset() {
        Timber.d(">>>> reset()")
        //cancel the enqueued if exists
        val enqueuedJob = enqueuedGetNextPageJob
        enqueuedGetNextPageJob = null
        enqueuedJob?.cancel()

        //cancel current request
        collectMessagesJob?.cancel()

        //retry from the first page
        page = -1
        completedPageFetch.value = page
        requestNextPage()
    }

    /**
     * Cancel the in-progress loading of the very last page & force-reload it
     */
    fun retryRequest() {
        Timber.d(">>>> retryRequest()")
        //cancel the enqueued if exists
        val enqueuedJob = enqueuedGetNextPageJob
        enqueuedGetNextPageJob = null
        enqueuedJob?.cancel()

        //cancel current request
        collectMessagesJob?.cancel()

        //retry the very last page
        page -= 1
        completedPageFetch.value = page
        requestNextPage()
    }

    fun userRequestedNextPage() {
        Timber.d(">>>> userRequestedNextPage()")

        if (enqueuedGetNextPageJob != null) {
            Timber.d(">>>> userRequestedNextPage() dropped because a request is already enqueued")
            return
        }

        if (!checkHasMorePages()) {
            Timber.d(">>>> userRequestedNextPage() dropped because there are no more pages to fetch")
            return
        }

        enqueuedGetNextPageJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            Timber.d(">>>> userRequestedNextPage() the request is now enqueued")

            completedPageFetch.collect { completedPage ->
                if (completedPage != page) {
                    Timber.d(">>>> message-page-load is in progress; page:$page completedPage:$completedPage")
                    return@collect
                }

                val didError = chatMessagesError.value
                if (didError) {
                    Timber.d(">>>> not carrying out an enqueued request because there was an error")
                    return@collect
                }

                if (!checkHasMorePages()) {
                    //cleanup
                    val thisJob = enqueuedGetNextPageJob
                    enqueuedGetNextPageJob = null
                    thisJob?.cancel()

                    return@collect
                }

                Timber.d(">>>>carrying out an enqueued request because the messages NOT refreshing")

                requestNextPage()

                //cleanup
                val thisJob = enqueuedGetNextPageJob
                enqueuedGetNextPageJob = null
                thisJob?.cancel()
            }
        }


    }

    private fun requestNextPage() {

        if (!isSetup) throw RuntimeException()

        Timber.d("requestNextPage page:${page + 1}")

        collectMessagesJob?.cancel()
        collectMessagesJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            page += 1
            val currentPage = page

            //eagerly return the cached content
            twetchChatMessageStore.getContentForKey(
                TwetchChatMessageStore.Key(
                    paymail,
                    chatId,
                    page
                )
            ).let {
                if (it.isEmpty()) return@let
                //return what is already cached
                _chatMessages.value = StoreResponse.Data(it, ResponseOrigin.SourceOfTruth)
            }

            twetchChatMessageStore.getMessageFlow(
                paymail,
                chatId,
                page
            ).collect { response ->

                _chatMessages.value = response

                //update the tracking of fetch progress
                when (response) {
                    is StoreResponse.Loading -> { }
                    is StoreResponse.Data<*> -> {
                        if (response.origin == ResponseOrigin.Fetcher) {
                            completedPageFetch.value = currentPage
                        }

                    }
                    is StoreResponse.Error -> {
                        if (response.origin == ResponseOrigin.Fetcher) {
                            completedPageFetch.value = currentPage
                        }

                    }
                    is StoreResponse.NoNewData -> {
                        completedPageFetch.value = currentPage
                    }
                }


            }
        }
    }


    fun onMessageInputChanged(value: String) {
        _messageInput.value = value
    }





    fun sendMessage() {
        val chatInfo = _chatContent.value?.info ?: return
        val content = messageInput.value.trim()





        // Current chat-sync-mechanism relies on last-message-time & unread-count fields
        // & polling to figure out the sync-state of each chat. This mechanism has a quirky
        // interaction with message-sending-feature; someone else could send a chat-message
        // moments before the logged in user; user's message would have a message-time
        // that is the latest and this would fool the polling of chat-sync-mechanism to
        // not look for more messages. In order to resolve this scenario, a latest-message fetch
        // is performed for a chat after sending of a message.

        delayedPostMessageSendRefreshJob?.let {
            if (it.isActive) {
                Timber.d("cancelling `delayedPostMessageSendRefreshJob`")
                it.cancel()
            }
        }
        delayedPostMessageSendRefreshJob = null


        val delayedRefreshWork: suspend CoroutineScope.()->Unit = {
            Timber.d("scheduling `delayedPostMessageSendRefreshJob`")
            delay(5000)
            ensureActive()
            //force-refresh the first pages of this chat
            val numOfMessageSinceLastPolling = messageSendingState.value.let { sentMessages ->
                val millisPolledLast = detectorManager.millisPolledLast

                sentMessages.filter { it.millisCreated > millisPolledLast }.size
            }

            twetchChatMessageStore.fetchNewlyReceivedMessages(
                paymail,
                chatInfo.id
            )
            Timber.d("completed `delayedPostMessageSendRefreshJob`")
        }







        val profile = getActiveProfile() ?: throw Exception()
        val me = TwetchChatInfoStore.Participant(
            profile.id.toString(),
            profile.name,
            profile.publicKey,
            profile.icon,
            profile.dmConversationId
        )
        val sendingMessage = SendingTwetchMessage(
            UUID.randomUUID().toString(),
            chatInfo.id,
            content,
            System.currentTimeMillis(),
            me,
            false,
            null
        )




        ArrayList(messageSendingState.value).let {
            it.add(sendingMessage)

            _messageSendingState.value = it
        }
        _messageInput.value = ""







        coroutineUtil.appScope.launch(coroutineUtil.defaultDispatcher) {
            val result = sendMessage(
                content,
                chatInfo
            )

            result ?: let {
                //sending was not attempted
                ArrayList(messageSendingState.value).let {
                    it.remove(sendingMessage)
                    it.add(sendingMessage.copy(
                        didError = true
                    ))

                    _messageSendingState.value = it
                }
                return@launch
            }

            val (success, createdMessage) = result

            if (success) {

                ArrayList(messageSendingState.value).let {
                    it.remove(sendingMessage)
                    it.add(sendingMessage.copy(
                        didError = false,
                        createdMessage = createdMessage
                    ))

                    _messageSendingState.value = it
                }



                // do a delayed fetch of very latest messages of this chat in case
                // someone else sent a message at the same time as the user
                delayedPostMessageSendRefreshJob = coroutineUtil.appScope
                    .launch(coroutineUtil.ioDispatcher, block = delayedRefreshWork)




            } else {
                ArrayList(messageSendingState.value).let {
                    it.remove(sendingMessage)
                    it.add(sendingMessage.copy(
                        didError = true
                    ))

                    _messageSendingState.value = it
                }
            }
        }



    }


    suspend fun onShouldInformSeen(info: TwetchChatInfoStore.ChatInfo) {
        informServerMessageSeen(info)
    }

    suspend fun onNewMessagesDetected(
        paymail: String,
        update: TwetchChatInfoStore.ChatNewMessageCount
    ) {
        twetchChatMessageStore.fetchNewlyReceivedMessages(
            paymail,
            update.chatId
        )
    }

    suspend fun onChatRemovalDetected() {
        //must kick the user out of the chat window if he is looking at it
        _chatDisabledState.value = true

        cleanup()
        isSetup = true
    }



    private fun ensureSetup() {
        if (isSetup) {
            Timber.d("already setup")
            return
        }
        isSetup = true


        var lastMessageData = listOf<TwetchChatMessage>()

        collectContentJob?.cancel()
        collectContentJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            _chatMessages.combine(
                getChatInfo(chatId)
            ) { p0, p1 ->
                p0 to p1
            }.collect { (response, chatInfo) ->
                chatInfo ?: return@collect

                when (response) {
                    is StoreResponse.Loading -> {
                        Timber.d("StoreResponse.Loading origin:${response.origin}")
                        _chatMessagesRefreshing.value = true

                    }
                    is StoreResponse.Data<*> -> {
                        Timber.d("StoreResponse.Data origin:${response.origin}")
                        if (response.origin == ResponseOrigin.Fetcher) {
                            _chatMessagesRefreshing.value = false
                            _chatMessagesError.value = false

                        } else if (response.origin == ResponseOrigin.SourceOfTruth
                            || response.origin == ResponseOrigin.Cache) {
                            _chatMessagesRefreshing.value = false

                        }

                        lastMessageData = response.requireData()


                    }
                    is StoreResponse.Error -> {
                        response as StoreResponse.Error.Exception
                        Timber.d("StoreResponse.Error $response; ${
                            Log.getStackTraceString(response.error)
                        }")

                        //show error
                        _chatMessagesError.value = true

                        if (response.origin == ResponseOrigin.Fetcher) {
                            //no longer loading
                            _chatMessagesRefreshing.value = false
                        }

                    }
                    is StoreResponse.NoNewData -> {
                        Timber.d("StoreResponse.NoNewData")
                        _chatMessagesRefreshing.value = false
                    }
                }


                val profile = getActiveProfile() ?: throw Exception()
                val me = TwetchChatInfoStore.Participant(
                    profile.id.toString(),
                    profile.name,
                    profile.publicKey,
                    profile.icon,
                    profile.dmConversationId
                )


                _chatContent.value = ChatContentModel(
                    lastMessageData,
                    chatInfo,
                    getPage(),
                    me,
                    checkHasMorePages(),
                    checkHasFetchError()
                )


            }

        }

        //ensure first page
        requestNextPage()
    }



    private suspend fun informServerMessageSeen(chatInfo: TwetchChatInfoStore.ChatInfo) {

        val existingSeenHolder = informChatSeenMap[chatInfo.id]
        if (existingSeenHolder != null) {
            if (existingSeenHolder.millisLatestMessage >= chatInfo.millisLastMessageAt) {
                Timber.d("#informServerMessageSeen dropped because this has already been triggered")
                return
            }
            existingSeenHolder.job.cancel()
            informChatSeenMap.remove(chatInfo.id)
        }


        val informChatSeenJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            val result = nodeRuntimeRepository.runAsyncScript("""
                await context.twetch.ensureAuthHasCompleted();
                
                let dataPayload = {
                    "conversationId": "${chatInfo.id}",
                    "userId": "${getActiveProfile()!!.id}",
                    "payload": {
                        "readAt": new Date().toJSON()
                    }
                }
                
                let result = await context.twetch.instance.query(`
                    mutation updateConversationUser(${'$'}userId: BigInt!, ${'$'}conversationId: UUID!, ${'$'}payload: ConversationsUserPatch!) {
                        updateConversationsUserByUserIdAndConversationId(input: {
                            conversationsUserPatch: ${'$'}payload,
                            userId: ${'$'}userId,
                            conversationId: ${'$'}conversationId
                        }) {
                            clientMutationId
                        }
                    }
                `, dataPayload);
                
                
                return JSON.stringify(result);
            """.trimIndent())

            ensureActive()

            if (result is RunScript.Response.Success) {
                Timber.d("chat message-seen successfully updated: $result")

                twetchChatInfoStore.updateInfoOnMessagesSeen(
                    getInitializedForPaymail(),
                    chatInfo.id
                )


            } else {
                result as RunScript.Response.Fail

                Timber.e(RuntimeException(), "chat message-seen not updated: $result")
                //todo
                //todo
                //todo
                //todo
                //todo
                //todo
                //todo
            }

            informChatSeenMap.remove(chatInfo.id)


        }



        //cache it to avoid repeats
        informChatSeenMap[chatInfo.id] = ChatSeenInfoHolder(
            chatInfo.id,
            chatInfo.millisLastMessageAt,
            informChatSeenJob
        )



    }



    private suspend fun sendMessage(
        content: String,
        chatInfo: TwetchChatInfoStore.ChatInfo
    ): Pair<Boolean, TwetchChatMessage?>? {
        val profile = getActiveProfile() ?: return null
        val paymail = getInitializedForPaymail().let { paymail ->
            if (TwetchRepository.INVALID_PAYMAIL == paymail) return null
            paymail
        }

        val result = nodeRuntimeRepository.runAsyncScript("""
            await context.twetch.ensureAuthHasCompleted();
            
            let chatEncryptedKey = '${chatInfo.meEncryptedKey}';

            let twetchPrivWIF = context.twetch.twetchPrivWIF;
            
            let twetchCryptoHelper = context.twetch.instance.crypto;
            let chatAesKey = twetchCryptoHelper.eciesDecrypt(chatEncryptedKey, twetchPrivWIF);
            
            
            let content = `$content`;
            let encryptedContent = twetchCryptoHelper.aesEncrypt(content, chatAesKey);
            
            
            let payload = {
                conversationId: '${chatInfo.id}',
                userId: '${profile.id}',
                description: encryptedContent
            };
            
            
            let result = await context.twetch.instance.query(`
                mutation createMessage(${'$'}payload: MessageInput!) {
                    createMessage(input: { message: ${'$'}payload }) {
                        message {
                            description
                            id
                            createdAt
                        }
                    }
                }
            `, { payload });
            
            return JSON.stringify(result.createMessage.message);
        """.trimIndent())

        if (result is RunScript.Response.Success) {
            Timber.d("chat message successfully sent: $result")

            val messageJson = gson.fromJson(result.content, JsonObject::class.java)
            val messageId = messageJson.get("id").asString
            val createdAt = messageJson.get("createdAt").asString
            val millisCreatedAt = createdAt.let {
                try {
                    ZonedDateTime.parse(it).toInstant().toEpochMilli()
                } catch (e: Exception) {
                    Timber.e(e)
                    -1
                }
            }

            val message = TwetchChatMessage(
                paymail,
                messageId,
                chatInfo.id,
                millisCreatedAt,
                profile.id.toString(),
                content,
                emptyList(),
                TwetchChatInfoStore.Participant(
                    profile.id.toString(),
                    profile.name,
                    profile.publicKey,
                    profile.icon,
                    profile.dmConversationId
                )
            )

            coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
                //this source-of-truth updated is executed after a delay to give an opportunity to
                // the caller to do some processing on the returned `message` before the world changes
                delay(250)
                twetchChatMessageStore.insertAll(
                    TwetchChatMessageStore.Key(
                        paymail,
                        chatInfo.id,
                        0
                    ),
                    listOf(message)
                )
            }


            return true to message

        } else {
            result as RunScript.Response.Fail

            Timber.d("chat message not sent: $result")

            return false to null

        }

    }



    internal fun cleanup() {
        collectMessagesJob?.cancel()
        collectMessagesJob = null

        collectContentJob?.cancel()
        collectContentJob = null

        enqueuedGetNextPageJob?.cancel()
        enqueuedGetNextPageJob = null

        delayedPostMessageSendRefreshJob?.cancel()
        delayedPostMessageSendRefreshJob = null

        page = -1
        completedPageFetch.value = page
        isSetup = false

        _chatMessages.value = StoreResponse.Loading(ResponseOrigin.Cache)
    }
}

data class ChatSeenInfoHolder(
    val chatId: String,
    val millisLatestMessage: Long,
    val job: Job
)

@ExperimentalCoroutinesApi
@FlowPreview
data class SendingTwetchMessage constructor(
    val id: String,
    val chatId: String,
    val content: String,
    val millisCreated: Long,
    val participant: TwetchChatInfoStore.Participant,
    val didError: Boolean,
    val createdMessage: TwetchChatMessage?
)

@ExperimentalCoroutinesApi
@FlowPreview
data class ChatContentModel(
    val messages: List<TwetchChatMessage>,
    val info: TwetchChatInfoStore.ChatInfo,
    val page: Int,
    val me: TwetchChatInfoStore.Participant,
    val hasMorePages: Boolean,
    val hasFetchError: Boolean
)