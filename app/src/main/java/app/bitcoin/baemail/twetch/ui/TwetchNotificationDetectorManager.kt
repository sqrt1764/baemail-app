package app.bitcoin.baemail.twetch.ui

import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.google.gson.Gson
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.StateFlow

class TwetchNotificationDetectorManager(
    private val coroutineUtil: CoroutineUtil,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val gson: Gson,
    private val twetchChatInfoStore: TwetchChatInfoStore
) {

    val detector = TwetchNotificationDetector(
        coroutineUtil,
        nodeRuntimeRepository,
        gson
    ) { key ->
        twetchChatInfoStore.queryByFilter(key)
    }

    val isNowRefreshingChatListMessages: StateFlow<Boolean>
        get() = detector.isRefreshingNow

    val millisPolledLast: Long
        get() {
            return detector.millisPolledLast
        }

    private var runningDetectorJob: Job? = null

    val isStarted: Boolean
        get() {
            return runningDetectorJob != null
        }

    fun clear() {
        runningDetectorJob?.cancel()
        runningDetectorJob = null
    }

    fun start(
        key: TwetchChatInfoStore.ChatInfoKey,
        onAddedChatsDetected: suspend () -> Unit,
        onRemovedChatsDetected: suspend (List<String>) -> Unit,
        onNewMessagesDetected: suspend (List<TwetchChatInfoStore.ChatNewMessageCount>) -> Unit,
        onNotificationInfoUpdated: suspend (Int, Long) -> Unit,
        onError: suspend () -> Unit
    ) {
        runningDetectorJob?.cancel()
        runningDetectorJob = detector.start(
            key,
            onAddedChatsDetected,
            onRemovedChatsDetected,
            onNewMessagesDetected,
            onNotificationInfoUpdated,
            onError
        )
    }



}