package app.bitcoin.baemail.twetch.ui

import android.os.Parcelable
import app.bitcoin.baemail.twetch.db.entity.UserProfileBaseModel
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import com.dropbox.android.external.store4.ResponseOrigin
import com.dropbox.android.external.store4.StoreResponse
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber

@FlowPreview
@ExperimentalCoroutinesApi
class TwetchUserProfileUseCase(
    private val activeUserId: String,
    private val userId: String,
    private val coroutineUtil: CoroutineUtil,
    private val userProfileBaseStore: TwetchUserProfileBaseStore,
    private val postListStore: TwetchPostListStore,
    initialTab: UserProfileTab
) {

    companion object {
        const val PAGE_NOT_LOADED = -1
    }


    private var isSetup = false
    private var isSetupProfileBase = false

    private val _content = MutableStateFlow(Model(
        userId,
        initialTab,
        null,
        null,
        null,
        null
    ))
    val content: StateFlow<Model>
        get() {
            ensureSetup()
            return _content
        }

    private val _metaState = MutableStateFlow(MetaState(
        true,
        true,
        true,
        true,
        false,
        false,
        false,
        false
    ))
    val metaState: StateFlow<MetaState>
        get() = _metaState




    private var userProfileCollectJob: Job? = null

    var postListDataObservationJob: Job? = null
    var postListRefreshingObservationJob: Job? = null


    var cachedProfileBaseState: Parcelable? = null
    var cachedLatestState: Parcelable? = null
    var cachedRepliesState: Parcelable? = null
    var cachedLikesState: Parcelable? = null

    var cachedTabsScrolledAmount = 0


    private val latestPostListHelper = TwetchPostListHelper(
        activeUserId,
        userId,
        ListType.PROFILE_LATEST,
        coroutineUtil,
        postListStore
    )

    private val repliesPostListHelper = TwetchPostListHelper(
        activeUserId,
        userId,
        ListType.PROFILE_REPLIES,
        coroutineUtil,
        postListStore
    )

    private val likesPostListHelper = TwetchPostListHelper(
        activeUserId,
        userId,
        ListType.PROFILE_LIKES,
        coroutineUtil,
        postListStore
    )


    fun onTabChanged(newTab: UserProfileTab): Boolean {
        if (_content.value.activeTab == newTab) return false

        _content.value = _content.value.copy(activeTab = newTab)
        return true
    }

    fun onRefreshRequested() {
        when (_content.value.activeTab) {
            UserProfileTab.PROFILE -> {
                isSetupProfileBase = false
                ensureSetupProfileBase()
            }
            UserProfileTab.LATEST -> {
                latestPostListHelper.refreshFromStart()
            }
            UserProfileTab.REPLIES -> {
                repliesPostListHelper.refreshFromStart()
            }
            UserProfileTab.LIKES -> {
                likesPostListHelper.refreshFromStart()
            }
        }
    }

    private fun ensureSetup() {
        if (isSetup) {
            return
        }
        isSetup = true


        ensureSetupProfileBase()

        //////////////////////////////////////////////////////

        latestPostListHelper.ensureSetup()
        repliesPostListHelper.ensureSetup()
        likesPostListHelper.ensureSetup()


        postListDataObservationJob?.cancel()
        postListDataObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            //todo maybe consider using kotlin-channels??
            latestPostListHelper.data.combine(repliesPostListHelper.data) { latest, replies ->
                latest to replies
            }.combine(likesPostListHelper.data) { (latest, replies), likes ->
                Triple(latest, replies, likes)
            }.collectLatest { (latest, replies, likes) ->
                delay(15)
                _content.value = _content.value.copy(
                    latest = latest,
                    replies = replies,
                    likes = likes
                )
            }

        }

        postListRefreshingObservationJob?.cancel()
        postListRefreshingObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            //todo maybe consider using kotlin-channels??
            latestPostListHelper.refreshing.combine(repliesPostListHelper.refreshing) { latest, replies ->
                latest to replies
            }.combine(likesPostListHelper.refreshing) { (latest, replies), likes ->
                Triple(latest, replies, likes)
            }.collect { (latest, replies, likes) ->
                _metaState.value = _metaState.value.copy(
                    latestRefreshing = latest,
                    repliesRefreshing = replies,
                    likesRefreshing = likes
                )
            }

        }


    }

    private fun ensureSetupProfileBase() {
        if (isSetupProfileBase) return
        isSetupProfileBase = true

        userProfileCollectJob?.cancel()
        userProfileCollectJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            Timber.d(">>>>>>> starting observing new stream of user-profile store; userId:$userId")

            val key = TwetchUserProfileBaseStore.Key(activeUserId, userId)

            var lastUserProfileData: UserProfileBaseModel? = null
            var lastRefreshingState = true
            var lastErrorState = false

            _metaState.value = _metaState.value.copy(
                profileBaseRefreshing = lastRefreshingState
            )

            //eagerly return the cached content
            userProfileBaseStore.getFromSourceOfTruth(key)?.let {
                lastUserProfileData = it
                _content.value = _content.value.copy(
                    profileBase = lastUserProfileData
                )
            }




            userProfileBaseStore.getFlow(key).collect { response ->

                when (response) {
                    is StoreResponse.Loading -> {
                        Timber.d("StoreResponse.Loading origin:${response.origin}")
                        lastRefreshingState = true

                    }
                    is StoreResponse.Data<*> -> {
                        Timber.d("StoreResponse.Data origin:${response.origin}")
                        if (response.origin == ResponseOrigin.Fetcher) {
                            lastRefreshingState = false
                            lastErrorState = false
                        } else if (response.origin == ResponseOrigin.SourceOfTruth
                            || response.origin == ResponseOrigin.Cache) {
//                            lastRefreshingState = false
                        }

                        lastUserProfileData = response.requireData()


                    }
                    is StoreResponse.Error -> {
                        Timber.d("StoreResponse.Error $response; ")
                        if (response.origin == ResponseOrigin.Fetcher) {
                            //no longer loading
                            lastRefreshingState = false
                        }

                        if (response is StoreResponse.Error.Exception) {
                            Timber.e(response.error, "StoreResponse.Error.Exception")
                        }

                        //show error
                        lastErrorState = true

                    }
                    is StoreResponse.NoNewData -> {
                        Timber.d("StoreResponse.NoNewData")
                        lastRefreshingState = false
                    }
                }


                _content.value = _content.value.copy(
                    profileBase = lastUserProfileData
                )

                _metaState.value = _metaState.value.copy(
                    profileBaseRefreshing = lastRefreshingState
                )

                //todo
                //todo lastErrorState
                //todo

            }


        }
    }



    fun requestNextPage() {
        when (_content.value.activeTab) {
            UserProfileTab.PROFILE -> {
                throw RuntimeException()
            }
            UserProfileTab.LATEST -> {
                latestPostListHelper.userRequestedNextPage()
            }
            UserProfileTab.REPLIES -> {
                repliesPostListHelper.userRequestedNextPage()
            }
            UserProfileTab.LIKES -> {
                likesPostListHelper.userRequestedNextPage()
            }
        }
    }




    fun cleanup() {
        userProfileCollectJob?.cancel()
        userProfileCollectJob = null

        isSetup = false
        isSetupProfileBase = false


        postListDataObservationJob?.cancel()
        postListRefreshingObservationJob?.cancel()

        latestPostListHelper.cleanup()
        repliesPostListHelper.cleanup()
        likesPostListHelper.cleanup()




        _content.value = Model(
            userId,
            UserProfileTab.PROFILE,
            null,
            null,
            null,
            null
        )

        _metaState.value = MetaState(
            true,
            true,
            true,
            true,
            false,
            false,
            false,
            false
        )
    }




    enum class UserProfileTab {
        PROFILE,
        LATEST,
        REPLIES,
        LIKES
    }


    //todo must also figure out if there is a common private chat
    data class Model(
        val userId: String,
        val activeTab: UserProfileTab,
        val profileBase: UserProfileBaseModel?,
        val latest: TwetchPostModel?,
        val replies: TwetchPostModel?,
        val likes: TwetchPostModel?,
    )


    data class MetaState(
        val profileBaseRefreshing: Boolean,
        val latestRefreshing: Boolean,
        val repliesRefreshing: Boolean,
        val likesRefreshing: Boolean,
        val profileBaseError: Boolean,
        val latestError: Boolean,
        val repliesError: Boolean,
        val likesError: Boolean
    )

}




data class TwetchPostModel(
    val list: List<PostHolder>,
    val page: Int,
    val hasMorePages: Boolean
)