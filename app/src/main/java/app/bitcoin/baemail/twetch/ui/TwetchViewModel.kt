package app.bitcoin.baemail.twetch.ui

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.format.DateFormat
import androidx.lifecycle.*
import app.bitcoin.baemail.intercept.AppDeepLinkType
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchChatListItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.data.util.DeepLinkHelper
import app.bitcoin.baemail.core.data.wallet.DynamicChainSync
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.core.presentation.view.VideoClipManager
import app.bitcoin.baemail.di.SavedStateViewModelAssistant
import com.dropbox.android.external.store4.ResponseOrigin
import com.dropbox.android.external.store4.StoreResponse
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber
import java.io.*


class TwetchViewModel @AssistedInject constructor(
    @Assisted private val state : SavedStateHandle,
    private val appContext: Context,
    private val authRepository: AuthRepository,
    private val coroutineUtil: CoroutineUtil,
    val chainSync: DynamicChainSync,
    private val twetchRepository: TwetchRepository,
    val deepLinkHelper: DeepLinkHelper,
    val videoClipManager: VideoClipManager,
    val twetchFilesMimeTypeHelper: TwetchFilesMimeTypeHelper
) : ViewModel() {

    @AssistedFactory
    interface Assistant : SavedStateViewModelAssistant<TwetchViewModel> {
        override fun create(savedStateHandle: SavedStateHandle): TwetchViewModel
    }

    private val stateHandler = StateHandler(state, coroutineUtil)

    val coinManagementUseCase: CoinManagementUseCase
        get() = twetchRepository.coinManagementUseCase

    private val _activeSection = MutableStateFlow(Section.CHAT)
    val activeSection: StateFlow<Section>
        get() = _activeSection


    private val _authState = MutableStateFlow(twetchRepository.authUseCase.authState.value)
    val authState: StateFlow<Auth>
        get() = _authState

    private val _authInfo = MutableStateFlow(twetchRepository.authUseCase.authInfo.value)
    val authInfo: StateFlow<TwetchAuthStore.AuthInfo?>
        get() = _authInfo


    val actionHelper: TwetchActionHelper
        get() = twetchRepository.postActionHelper


    val postUserCache: TwetchPostUserCache
        get() = twetchRepository.postUserCache

    val loadingIndicator: StateFlow<Boolean>
        get() = twetchRepository.loadingIndicator


    private val _chatListInfoRefreshing = MutableStateFlow(false)
    val chatListInfoRefreshing: StateFlow<Boolean>
        get() = _chatListInfoRefreshing

    val isNowRefreshingChatListMessages: StateFlow<Boolean>
        get() = twetchRepository.notificationDetectorManager.isNowRefreshingChatListMessages

    private val _chatListInfo = MutableStateFlow(listOf<AdapterType>())
    val chatListInfo: StateFlow<List<AdapterType>>
        get() = _chatListInfo

    private val _chatBadgeCount = MutableStateFlow(0)
    val chatBadgeCount: StateFlow<Int>
        get() = _chatBadgeCount

    val notificationInfo: StateFlow<TwetchRepository.NotificationInfo>
        get() = twetchRepository.notificationInfo

    val composePostUseCase: TwetchNewPostUseCase
        get() = twetchRepository.composePostUseCase

    private val chatDateFormat = DateFormat.getMediumDateFormat(appContext)

    val coinFlipSignalHelper: CoinFlipSignalHelper
        get() = twetchRepository.coinFlipSignalHelper

    init {

        if (stateHandler.isInitialized()) {
            _activeSection.value = stateHandler.getActiveSection()
        }




        viewModelScope.launch(coroutineUtil.defaultDispatcher) {
            val supported = listOf(
                AppDeepLinkType.TWETCH_CHAT,
                AppDeepLinkType.TWETCH_POST,
                AppDeepLinkType.TWETCH_USER
            )

            deepLinkHelper.unsatisfiedLink.collectLatest { link ->
                link ?: return@collectLatest

                if (!supported.contains(link.type))return@collectLatest

                when (link.type) {
                    AppDeepLinkType.TWETCH_CHAT -> {
                        changeSection(Section.CHAT)
                    }
                    AppDeepLinkType.TWETCH_POST,
                    AppDeepLinkType.TWETCH_USER -> {
                        changeSection(Section.NOTIFICATIONS)
                    }
                    else -> {
                        //do nothing
                    }
                }

            }
        }



        viewModelScope.launch(coroutineUtil.defaultDispatcher) {
            twetchRepository.authUseCase.authState.collect { status ->
                _authState.value = status
            }
        }
        viewModelScope.launch(coroutineUtil.defaultDispatcher) {
            twetchRepository.authUseCase.authInfo.collect { info ->
                info?.profile?.let { profile ->
                    stateHandler.setActiveUserId(profile.id.toString())
                    stateHandler.setInitialized()
                }

                _authInfo.value = info
            }
        }


        val heightLD = MutableLiveData<Int>()
        heightLD.value = (appContext.resources.displayMetrics.density * 80).toInt()
        val chatListBottomDecorItem = ATHeightDecorDynamic("0", heightLD)


        viewModelScope.launch(coroutineUtil.defaultDispatcher) {
            Timber.d("starting -- twetchRepository.chatInfoHelper.chatInfo.collect")
            var lastProcessedData: List<TwetchChatInfoStore.ChatInfo>? = null

            twetchRepository.chatListUseCase.chatInfo.collect { response ->
                when (response) {
                    is StoreResponse.Loading -> {
                        Timber.d("StoreResponse.Loading origin:${response.origin}")
                        _chatListInfoRefreshing.value = true

                    }
                    is StoreResponse.Data<*> -> {
                        Timber.d("StoreResponse.Data origin:${response.origin}")
                        if (response.origin == ResponseOrigin.Fetcher) {
                            _chatListInfoRefreshing.value = false
                            //errorHolder = false
                        } else if (response.origin == ResponseOrigin.SourceOfTruth
                                || response.origin == ResponseOrigin.Cache) {
                            _chatListInfoRefreshing.value = false
                        }

                        var totalBadgeCount = -1

                        val chatItemList = try {
                            val data = response.requireData()
                            if (data == lastProcessedData) {
                                Timber.d("collected data is dropped as it is unchanged")
                                return@collect
                            }
                            lastProcessedData = data

                            val me = authInfo.value!!.profile!!

                            val list = mutableListOf<AdapterType>()
                            data.map { chat ->
                                val createdOnLabel = chatDateFormat.format(chat.millisLastMessageAt)

                                val name = if (chat.selfChat) {
                                    me.name
                                } else {
                                    chat.name
                                }
                                val icon = if (chat.selfChat) {
                                    me.icon
                                } else {
                                    chat.icon
                                }

                                ATTwetchChatListItem(
                                        chat.id,
                                        name,
                                        icon,
                                        createdOnLabel,
                                        chat
                                )
                            }.toCollection(list)

                            totalBadgeCount = 0
                            data.forEach { chat ->
                                totalBadgeCount += chat.unreadCount
                            }


                            //add extra free space at the bottom
                            list.add(chatListBottomDecorItem)

                            list
                        } catch (e: Exception) {
                            Timber.e(e, "${response.requireData()}")
                            listOf()
                        }

                        //todo add a no-chats item if the list is empty
                        _chatListInfo.value = chatItemList

                        if (totalBadgeCount != -1) {
                            _chatBadgeCount.value = totalBadgeCount
                        }

                    }
                    is StoreResponse.Error -> {
                        Timber.d("StoreResponse.Error $response")
                        if (response.origin == ResponseOrigin.Fetcher) {
                            //no longer loading
                            _chatListInfoRefreshing.value = false
                        }

                        //show error
                        //errorHolder = true

                    }
                    is StoreResponse.NoNewData -> {
                        Timber.d("StoreResponse.NoNewData")
                        _chatListInfoRefreshing.value = false
                    }
                }
            }
        }


    }



    fun changeSection(section: Section): Boolean {
        if (activeSection.value == section) return false

        viewModelScope.launch(coroutineUtil.defaultDispatcher) {
//            delay(50)
            _activeSection.value = section
            stateHandler.setActiveSection(section)
        }

        return true
    }

    fun setNavState(state: Bundle) {
        stateHandler.setNavState(state)
    }

    fun getNavState(): Bundle? = stateHandler.getNavState()


    fun retryAuth() {
//        if (twetchRepository.authState.value != TwetchRepository.Auth.NOT_LINKED) {
//            return
//        }

        twetchRepository.authUseCase.refreshAuth(true)
    }

    fun refreshChatList() {
        twetchRepository.chatListUseCase.refreshNow()
    }





    fun getChatInfo(chatId: String): Flow<TwetchChatInfoStore.ChatInfo?> {
        return twetchRepository.getChatInfo(chatId)
    }


    fun getChatMessages(chatId: String): TwetchChatContentUseCase {
        return twetchRepository.ensureChatContentUseCase(chatId)
    }

    fun getUserProfileContent(userId: String): TwetchUserProfileUseCase {
        return twetchRepository.ensureUserProfileContent(userId, stateHandler.activeUserId)
    }

    fun checkMyProfileContentUseCaseInitialized(): Boolean {
        return twetchRepository.checkMyProfileContentUseCaseInitialized()
    }

    fun getMyProfileContent(): TwetchUserProfileUseCase {
        return twetchRepository.ensureMyProfileContent(stateHandler.activeUserId)
    }

    fun getFollowingContent(): TwetchFollowingUseCase {
        return twetchRepository.ensureFollowingContent(stateHandler.activeUserId)
    }

    fun getLatestContent(): TwetchLatestUseCase {
        return twetchRepository.ensureLatestContent(stateHandler.activeUserId)
    }

    fun getTopContent(): TwetchTopUseCase {
        return twetchRepository.ensureTopContent(stateHandler.activeUserId)
    }

    fun getSearchContent(): TwetchSearchUseCase {
        return twetchRepository.ensureSearchContent(stateHandler.activeUserId)
    }

    fun getNotificationContent(): TwetchNotificationUseCase {
        return twetchRepository.ensureNotificationUseCase(stateHandler.activeUserId)
    }

    fun getPostDetailsContent(postTxId: String): TwetchPostDetailsUseCase {
        return twetchRepository.ensurePostDetailContent(postTxId, stateHandler.activeUserId)
    }


    @Suppress("BlockingMethodInNonBlockingContext")
    fun copyFileData(imageFile: File, destinationUri: Uri) {
        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            try {
                val contentResolver = appContext.contentResolver
                contentResolver.openFileDescriptor(destinationUri, "w")?.use {

                    FileInputStream(imageFile).use { inStream ->
                        FileOutputStream(it.fileDescriptor).use { outStream ->
                            val buf = ByteArray(2048)
                            var len: Int
                            while (inStream.read(buf).also { len = it } > 0) {
                                outStream.write(buf, 0, len)
                            }
                            outStream.flush()
                        }
                    }

                }
            } catch (e: FileNotFoundException) {
                Timber.e(e)
            } catch (e: IOException) {
                Timber.e(e)
            }
        }

    }

    @Suppress("BlockingMethodInNonBlockingContext")
    fun copyBitmapData(bitmap: Bitmap, destinationUri: Uri) {
        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            try {
                val contentResolver = appContext.contentResolver
                contentResolver.openFileDescriptor(destinationUri, "w")?.use {
                    FileOutputStream(it.fileDescriptor).use { outStream ->
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outStream)
                        outStream.flush()
                    }
                }
            } catch (e: FileNotFoundException) {
                Timber.e(e)
            } catch (e: IOException) {
                Timber.e(e)
            }
        }
    }



    enum class Section(
            val const: String
    ) {
        PROFILE("profile"),
        CHAT("chat"),
        FOLLOWING("following"),
        LATEST("latest"),
        TOP("top"),
        NOTIFICATIONS("notifications"),
        SEARCH("search")
    }


}





class StateHandler(
    private val savedState: SavedStateHandle,
    private val coroutineUtil: CoroutineUtil
) {
    companion object {
        const val INVALID_USER_ID = "invalid"

        const val KEY_INITIALIZED = "initialized"
        const val KEY_USER_ID = "user_id"
        const val KEY_ACTIVE_SECTION = "active_section"
        const val KEY_NAV_STATE = "nav_state"
    }

    private var isInitialized = false

    var activeUserId: String = INVALID_USER_ID
        private set
        get() {
            if (field == INVALID_USER_ID) throw RuntimeException()
            return field
        }

    private var lastNavState: Bundle? = null


    init {
        restore()
    }

    fun isInitialized(): Boolean = isInitialized

    fun setInitialized() {
        isInitialized = true
        savedState.set(KEY_INITIALIZED, true)
    }

    fun setActiveUserId(id: String) {
        savedState.set(KEY_USER_ID, id)
        activeUserId = id
    }

    fun setActiveSection(section: TwetchViewModel.Section) {
        savedState.set(KEY_ACTIVE_SECTION, section.const)
    }

    fun getActiveSection(): TwetchViewModel.Section {
        val serialized = savedState.get<String>(KEY_ACTIVE_SECTION) ?: TwetchViewModel.Section.CHAT.const

        return when (serialized) {
            TwetchViewModel.Section.PROFILE.const -> TwetchViewModel.Section.PROFILE
            TwetchViewModel.Section.CHAT.const -> TwetchViewModel.Section.CHAT
            TwetchViewModel.Section.FOLLOWING.const -> TwetchViewModel.Section.FOLLOWING
            TwetchViewModel.Section.LATEST.const -> TwetchViewModel.Section.LATEST
            TwetchViewModel.Section.TOP.const -> TwetchViewModel.Section.TOP
            TwetchViewModel.Section.SEARCH.const -> TwetchViewModel.Section.SEARCH
            TwetchViewModel.Section.NOTIFICATIONS.const -> TwetchViewModel.Section.NOTIFICATIONS
            else -> TwetchViewModel.Section.CHAT
        }
    }


    fun setNavState(state: Bundle) {
        lastNavState = state
        //coroutineUtil.appScope.launch {
        savedState[KEY_NAV_STATE] = state
        //}
    }

    fun getNavState(): Bundle? {
        lastNavState?.let {
            return it
        }
        return savedState[KEY_NAV_STATE]
    }

    private fun restore() {
        Timber.d("#restore")

        isInitialized = savedState.get<Boolean>(KEY_INITIALIZED)?: false

        activeUserId = savedState.get<String>(KEY_USER_ID) ?: INVALID_USER_ID
    }
}
