package app.bitcoin.baemail.twetch.ui

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.HorizontalScrollView
import android.widget.TextView
import androidx.core.view.doOnPreDraw
import androidx.core.widget.TextViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.bitcoin.baemail.R
import app.bitcoin.baemail.fundingWallet.core.presentation.CoinActivity
import app.bitcoin.baemail.fundingWallet.TopUpActivity
import app.bitcoin.baemail.core.presentation.util.RelativeTimeHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ViewCoinManagementHelper
import app.bitcoin.baemail.core.presentation.view.recycler.ATContentLoading0
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchNoPostsFoundItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import com.google.android.material.button.MaterialButton
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.mapLatest
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.math.min

class TwetchTopFragment : Fragment(R.layout.fragment_twetch_top) {

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel

    private lateinit var helper: TwetchTopUseCase

    private val postItemHelper: TwetchPostItemHelper by lazy {
        TwetchPostItemHelper(
            requireContext(),
            viewModel.postUserCache,
            TextViewCompat.getTextMetricsParams(dummyBodyText)
        )
    }



    private lateinit var tabScrollView: HorizontalScrollView
    private lateinit var tabHour: MaterialButton
    private lateinit var tabDay: MaterialButton
    private lateinit var tabWeek: MaterialButton
    private lateinit var tabMonth: MaterialButton
    private lateinit var tabAll: MaterialButton
    private lateinit var topbar: View
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var recycler: RecyclerView
    private lateinit var scrollToTop: TextView
    private lateinit var coinManagementHelper: ViewCoinManagementHelper


    private lateinit var dummyBodyText: TextView



    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter

    private var wasCreatedOnce = false

    private var lastRestoredScrollStateTab: TwetchTopUseCase.TopPostTab? = null

    var hasScrollUpBeenInitialized = false


    private val noPostsFoundItem = ATTwetchNoPostsFoundItem("0")

    private val topHeightLD = MutableLiveData<Int>()
    private val topHeightItem = ATHeightDecorDynamic("0", topHeightLD)

    private val altLoadingItemLD = MutableLiveData<Int>()
    private val altLoadingItem = ATHeightDecorDynamic("1", altLoadingItemLD)


    val colorPrimary: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }


    val colorDanger: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorDanger)
    }

    val dp: Float by lazy {
        resources.displayMetrics.density
    }





    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(TwetchUtilViewModel::class.java)



        helper = viewModel.getTopContent()





        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true

        dummyBodyText = requireView().findViewById(R.id.dummy_body_text)

        swipeRefresh = requireView().findViewById(R.id.swipe_refresh)
        tabScrollView = requireView().findViewById(R.id.tab_button_scroller)
        tabHour = requireView().findViewById(R.id.tab_hour)
        tabDay = requireView().findViewById(R.id.tab_day)
        tabWeek = requireView().findViewById(R.id.tab_week)
        tabMonth = requireView().findViewById(R.id.tab_month)
        tabAll = requireView().findViewById(R.id.tab_all)

        recycler = requireView().findViewById(R.id.list)
        topbar = requireView().findViewById(R.id.topbar)

        scrollToTop = requireView().findViewById(R.id.scroll_to_top)

        coinManagementHelper = requireView().findViewById(R.id.coin_management_helper)



        topHeightLD.value = (4 * dp).toInt()
        altLoadingItemLD.value = (108 * dp).toInt()



        val topbarElevatedHeight = resources.getDimension(R.dimen.inner_topbar_floating_elevation) / dp
        val topbarBackground = MaterialShapeDrawable.createWithElevationOverlay(requireContext(), 0f)

        topbar.background = topbarBackground



        val colorPrimary = requireContext().getColorFromAttr(R.attr.colorPrimary)
        val colorTransparentPrimary = Color.argb(
            200,
            Color.red(colorPrimary),
            Color.green(colorPrimary),
            Color.blue(colorPrimary)
        )


        swipeRefresh.setProgressViewOffset(true, (30 * dp).toInt(), (120 * dp).toInt())
        swipeRefresh.setColorSchemeColors(requireContext().getColorFromAttr(R.attr.colorOnPrimary))
        swipeRefresh.setProgressBackgroundColorSchemeColor(colorTransparentPrimary)
        swipeRefresh.setOnRefreshListener {
            helper.onRefreshRequested()

        }









        scrollToTop.clipToOutline = true
        scrollToTop.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, p0.height / 2f)
            }
        }
        scrollToTop.background = ColorDrawable(colorTransparentPrimary)

        scrollToTop.setOnClickListener {
            recycler.smoothScrollToPosition(0)
            scrollToTop.visibility = View.INVISIBLE
        }


















        tabScrollView.overScrollMode = View.OVER_SCROLL_NEVER






        tabHour.setOnClickListener {
            recycler.stopScroll()
            cacheCurrentScrollState()

            if (!helper.onTabChanged(TwetchTopUseCase.TopPostTab.HOUR)) {
                recycler.smoothScrollToPosition(0)
            }

            dismissScrollToTop()

            scrollToViewOfBar(tabHour, tabScrollView)
        }

        tabDay.setOnClickListener {
            recycler.stopScroll()
            cacheCurrentScrollState()

            if (!helper.onTabChanged(TwetchTopUseCase.TopPostTab.DAY)) {
                recycler.smoothScrollToPosition(0)
            }

            dismissScrollToTop()

            scrollToViewOfBar(tabDay, tabScrollView)
        }

        tabWeek.setOnClickListener {
            recycler.stopScroll()
            cacheCurrentScrollState()

            if (!helper.onTabChanged(TwetchTopUseCase.TopPostTab.WEEK)) {
                recycler.smoothScrollToPosition(0)
            }

            dismissScrollToTop()

            scrollToViewOfBar(tabWeek, tabScrollView)
        }

        tabMonth.setOnClickListener {
            recycler.stopScroll()
            cacheCurrentScrollState()

            if (!helper.onTabChanged(TwetchTopUseCase.TopPostTab.MONTH)) {
                recycler.smoothScrollToPosition(0)
            }

            dismissScrollToTop()

            scrollToViewOfBar(tabMonth, tabScrollView)
        }

        tabAll.setOnClickListener {
            recycler.stopScroll()
            cacheCurrentScrollState()

            if (!helper.onTabChanged(TwetchTopUseCase.TopPostTab.ALL)) {
                recycler.smoothScrollToPosition(0)
            }

            dismissScrollToTop()

            scrollToViewOfBar(tabAll, tabScrollView)
        }




        val contentLoadingListener = object : BaseListAdapter.ATContentLoadingListener {
            override fun onBound() {

                when (helper.content.value.activeTab) {
                    TwetchTopUseCase.TopPostTab.HOUR -> {
                        if (helper.metaState.value.hourRefreshing) {
                            return
                        }
                    }
                    TwetchTopUseCase.TopPostTab.DAY -> {
                        if (helper.metaState.value.dayRefreshing) {
                            return
                        }

                    }
                    TwetchTopUseCase.TopPostTab.WEEK -> {
                        if (helper.metaState.value.weekRefreshing) {
                            return
                        }

                    }
                    TwetchTopUseCase.TopPostTab.MONTH -> {
                        if (helper.metaState.value.monthRefreshing) {
                            return
                        }

                    }
                    TwetchTopUseCase.TopPostTab.ALL -> {
                        if (helper.metaState.value.allRefreshing) {
                            return
                        }

                    }
                    else -> {
                        //do nothing
                    }
                }

                swapOutRefreshingItem()
            }
        }


        val postListener = TwetchPostListener(
            requireContext(),
            findNavController(),
            viewModel.actionHelper,
            viewModel.videoClipManager,
            R.id.action_global_twetchUserProfile,
            R.id.action_twetchTopFragment_to_twetchChatFullImageFragment7,
            R.id.action_global_twetchPostDetails,
            R.id.action_twetchTopFragment_to_twetchBranchOptionsFragment5,
            R.id.action_twetchTopFragment_to_twetchVideoClipFragment5,
            viewModel.coinFlipSignalHelper.coinFlipChannel
        )

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atContentLoadingListener = contentLoadingListener,
                atTwetchPostListener = postListener
            )
        )




        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var lastShowShadow: Boolean? = null

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                recyclerView.canScrollVertically(-1).let { showShadow ->
                    if (lastShowShadow == showShadow) return@let
                    lastShowShadow = showShadow

                    if (showShadow) {
                        utilViewModel.onMainContentScrollChanged(false)
                        topbarBackground.elevation = topbarElevatedHeight
                        tabScrollView.elevation = topbarElevatedHeight * dp
                        topbar.elevation = topbarElevatedHeight * dp
                    } else {
                        utilViewModel.onMainContentScrollChanged(true)
                        topbarBackground.elevation = 0f
                        tabScrollView.elevation = 0f
                        topbar.elevation = 0f
                    }
                }

                if (!hasScrollUpBeenInitialized) {
                    refreshShowingOfScrollToTop()
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (RecyclerView.SCROLL_STATE_IDLE == newState) {
                    cacheCurrentScrollState()

                    refreshShowingOfScrollToTop()

                }
            }
        })






        coinManagementHelper.clickTopUpListener = {
            startActivity(Intent(requireActivity(), TopUpActivity::class.java))
        }

        coinManagementHelper.clickSplitCoinsListener = {
            startActivity(Intent(requireActivity(), CoinActivity::class.java))
        }

        coinManagementHelper.clickRetryReconnectListener = {
            viewModel.chainSync.considerRetry()
        }






        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            utilViewModel.mainContentTopDistance.collect {
                refreshDynamicBottomHeight()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.coinManagementUseCase.state.mapLatest { it }
                .combine(utilViewModel.snackBarShowingState) { coinModel, snackBarReservation ->
                    coinModel to snackBarReservation
                }.collectLatest { (coinModel, snackBarReservation) ->
                    if (!snackBarReservation) {
                        coinManagementHelper.applyModel(coinModel)
                    } else {
                        //apply a model that will result in this bar being hidden
                        coinManagementHelper.applyModel(ViewCoinManagementHelper.HELPER_HIDDEN)
                    }
                }
        }





        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            helper.metaState.collectLatest {
                refreshLoadingIndicatorState()
            }
        }





        viewLifecycleOwner.lifecycleScope.launchWhenStarted {

            helper.content.collectLatest { model ->

                if (TwetchTopUseCase.TopPostTab.HOUR == model.activeTab) {
                    tabHour.setTextColor(colorDanger)
                    tabHour.strokeColor = ColorStateList.valueOf(colorDanger)
                } else {
                    tabHour.setTextColor(colorPrimary)
                    tabHour.strokeColor = ColorStateList.valueOf(colorPrimary)
                }

                if (TwetchTopUseCase.TopPostTab.DAY == model.activeTab) {
                    tabDay.setTextColor(colorDanger)
                    tabDay.strokeColor = ColorStateList.valueOf(colorDanger)
                } else {
                    tabDay.setTextColor(colorPrimary)
                    tabDay.strokeColor = ColorStateList.valueOf(colorPrimary)
                }

                if (TwetchTopUseCase.TopPostTab.WEEK == model.activeTab) {
                    tabWeek.setTextColor(colorDanger)
                    tabWeek.strokeColor = ColorStateList.valueOf(colorDanger)
                } else {
                    tabWeek.setTextColor(colorPrimary)
                    tabWeek.strokeColor = ColorStateList.valueOf(colorPrimary)
                }

                if (TwetchTopUseCase.TopPostTab.MONTH == model.activeTab) {
                    tabMonth.setTextColor(colorDanger)
                    tabMonth.strokeColor = ColorStateList.valueOf(colorDanger)
                } else {
                    tabMonth.setTextColor(colorPrimary)
                    tabMonth.strokeColor = ColorStateList.valueOf(colorPrimary)
                }

                if (TwetchTopUseCase.TopPostTab.ALL == model.activeTab) {
                    tabAll.setTextColor(colorDanger)
                    tabAll.strokeColor = ColorStateList.valueOf(colorDanger)
                } else {
                    tabAll.setTextColor(colorPrimary)
                    tabAll.strokeColor = ColorStateList.valueOf(colorPrimary)
                }




                when (model.activeTab) {
                    TwetchTopUseCase.TopPostTab.HOUR -> {
                        initListForTab(model.hour, model.activeTab)
                    }
                    TwetchTopUseCase.TopPostTab.DAY -> {
                        initListForTab(model.day, model.activeTab)
                    }
                    TwetchTopUseCase.TopPostTab.WEEK -> {
                        initListForTab(model.week, model.activeTab)
                    }
                    TwetchTopUseCase.TopPostTab.MONTH -> {
                        initListForTab(model.month, model.activeTab)
                    }
                    TwetchTopUseCase.TopPostTab.ALL -> {
                        initListForTab(model.all, model.activeTab)
                    }
                }

            }

        }
    }



    private var tabBarScrollingJob: Job? = null

    fun scrollToViewOfBar(child: View, scrollView: HorizontalScrollView) {
        tabBarScrollingJob?.cancel()

        tabBarScrollingJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            delay(500)
            val distanceFromLeft = child.left - scrollView.scrollX
            val mustScrollAmount = distanceFromLeft - dp * 150
            scrollView.smoothScrollBy(mustScrollAmount.toInt(), 0)
        }
    }


    fun refreshShowingOfScrollToTop() {
        hasScrollUpBeenInitialized = true

        requireView().post {
            if (layoutManager.findFirstVisibleItemPosition() > 10) {
                if (scrollToTop.visibility != View.VISIBLE) {
                    scrollToTop.visibility = View.VISIBLE
                }
            } else {
                if (scrollToTop.visibility != View.INVISIBLE) {
                    scrollToTop.visibility = View.INVISIBLE
                }
            }
        }
    }

    fun dismissScrollToTop() {
        hasScrollUpBeenInitialized = false

        if (scrollToTop.visibility != View.INVISIBLE) {
            scrollToTop.visibility = View.INVISIBLE
        }
    }




    private var lastAppliedList: TwetchPostModel? = null
    private var lastAppliedTab: TwetchTopUseCase.TopPostTab? = null


    private suspend fun initListForTab(postModel: TwetchPostModel?, tab: TwetchTopUseCase.TopPostTab) {
        if (lastAppliedList == postModel && lastAppliedTab == tab) {
            Timber.d("post-list is unchanged; returning")
            return
        }
        val currentLastAppliedTabValue = lastAppliedTab
        lastAppliedList = postModel
        lastAppliedTab = tab



        //refreshLoadingIndicatorState()
        awaitRecyclerAnimationsFinish()



        if (postModel == null) {
            Timber.d("postModel == null")
            adapter.setItems(emptyList())

            lastRestoredScrollStateTab = tab

        } else {

            val isRefreshing = isRefreshing()
            val loadingItem = if (!isRefreshing && postModel.hasMorePages) {
                ATContentLoading0(postModel.page)
            } else {
                altLoadingItem
            }

            if (TwetchPostListHelper.PAGE_NOT_LOADED == postModel.page) {
                Timber.d("postModel == PAGE_NOT_LOADED")
                helper.requestNextPage()
            }

            if (postModel.list.isEmpty()) {
                if (postModel.hasMorePages) {
                    adapter.setItems(
                        listOf(
                            topHeightItem,
                            loadingItem
                        )
                    )
                } else {
                    adapter.setItems(
                        listOf(
                            topHeightItem,
                            noPostsFoundItem
                        )
                    )
                }

                lastRestoredScrollStateTab = tab

            } else {
                val itemList = mutableListOf<AdapterType>()

                withContext(Dispatchers.Default) {
                    val relativeTimeHelper = RelativeTimeHelper(
                        requireContext(),
                        System.currentTimeMillis()
                    )

                    postModel.list.map {

//                        val refPostTxId = it.post.referencedPost
//                        val refPost = refPostTxId?.let { txId ->
//                            it.referenced[txId]
//                        }
//
//                        val postLabel = relativeTimeHelper.getLabel(it.post.millisCreatedAt)
//                        val refPostLabel = refPost?.millisCreatedAt?.let { millis ->
//                            relativeTimeHelper.getLabel(millis)
//                        }
//                        val doubleRefPostLabel = refPost?.referencedPost?.let { txId ->
//                            it.referenced[txId]?.let { p ->
//                                relativeTimeHelper.getLabel(p.millisCreatedAt)
//                            }
//                        }
//
//                        ATTwetchPostItem(
//                            it.post.id,
//                            it,
//                            postLabel,
//                            refPostLabel,
//                            doubleRefPostLabel
//                        )
                        postItemHelper.createItem(it, relativeTimeHelper)
                    }.toCollection(itemList)
                }

                Timber.d("new post-list; size: ${itemList.size}; page:${postModel.page} hasMorePages:${postModel.hasMorePages} isRefreshing:$isRefreshing")

                if (postModel.hasMorePages) {
                    itemList.add(loadingItem)
                }

                itemList.add(0, topHeightItem)

                adapter.setItems(itemList)

                if (tab != lastRestoredScrollStateTab) {
                    val cachedState = when (tab) {
                        TwetchTopUseCase.TopPostTab.HOUR -> helper.cachedHourListState
                        TwetchTopUseCase.TopPostTab.DAY -> helper.cachedDayListState
                        TwetchTopUseCase.TopPostTab.WEEK -> helper.cachedWeekListState
                        TwetchTopUseCase.TopPostTab.MONTH -> helper.cachedMonthListState
                        TwetchTopUseCase.TopPostTab.ALL -> helper.cachedAllListState
                    }

                    layoutManager.onRestoreInstanceState(cachedState)
                    lastRestoredScrollStateTab = tab
                }
            }
        }

        if (currentLastAppliedTabValue == null && lastRestoredScrollStateTab != null) {
            //detected returning from deeper within nav-graph
            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                delay(200)
                //todo this is hacky
                refreshShowingOfScrollToTop()
            }
        }
    }

    private fun isRefreshing(): Boolean {
        val metaState = helper.metaState.value

        return when (helper.content.value.activeTab) {
            TwetchTopUseCase.TopPostTab.HOUR -> {
                metaState.hourRefreshing
            }
            TwetchTopUseCase.TopPostTab.DAY -> {
                metaState.dayRefreshing
            }
            TwetchTopUseCase.TopPostTab.WEEK -> {
                metaState.weekRefreshing
            }
            TwetchTopUseCase.TopPostTab.MONTH -> {
                metaState.monthRefreshing
            }
            TwetchTopUseCase.TopPostTab.ALL -> {
                metaState.allRefreshing
            }
        }
    }

    private fun refreshLoadingIndicatorState() {
        val activeTab = helper.content.value.activeTab
        val metaState = helper.metaState.value

        when (activeTab) {
            TwetchTopUseCase.TopPostTab.HOUR -> {
                if (swipeRefresh.isRefreshing == metaState.hourRefreshing) return
                swipeRefresh.isRefreshing = metaState.hourRefreshing
            }
            TwetchTopUseCase.TopPostTab.DAY -> {
                if (swipeRefresh.isRefreshing == metaState.dayRefreshing) return
                swipeRefresh.isRefreshing = metaState.dayRefreshing
            }
            TwetchTopUseCase.TopPostTab.WEEK -> {
                if (swipeRefresh.isRefreshing == metaState.weekRefreshing) return
                swipeRefresh.isRefreshing = metaState.weekRefreshing
            }
            TwetchTopUseCase.TopPostTab.MONTH -> {
                if (swipeRefresh.isRefreshing == metaState.monthRefreshing) return
                swipeRefresh.isRefreshing = metaState.monthRefreshing
            }
            TwetchTopUseCase.TopPostTab.ALL -> {
                if (swipeRefresh.isRefreshing == metaState.allRefreshing) return
                swipeRefresh.isRefreshing = metaState.allRefreshing
            }
        }
    }



    private fun cacheCurrentScrollState() {
        when (helper.content.value.activeTab) {
            TwetchTopUseCase.TopPostTab.HOUR -> {
                Timber.d("profileContent scroll-state saved")
                helper.cachedHourListState = layoutManager.onSaveInstanceState()
            }
            TwetchTopUseCase.TopPostTab.DAY -> {
                Timber.d("latestContent scroll-state saved")
                helper.cachedDayListState = layoutManager.onSaveInstanceState()
            }
            TwetchTopUseCase.TopPostTab.WEEK -> {
                Timber.d("repliesContent scroll-state saved")
                helper.cachedWeekListState = layoutManager.onSaveInstanceState()
            }
            TwetchTopUseCase.TopPostTab.MONTH -> {
                Timber.d("likesContent scroll-state saved")
                helper.cachedMonthListState = layoutManager.onSaveInstanceState()
            }
            TwetchTopUseCase.TopPostTab.ALL -> {
                Timber.d("likesContent scroll-state saved")
                helper.cachedAllListState = layoutManager.onSaveInstanceState()
            }
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!wasCreatedOnce) return

        helper.cachedTabsScrolledAmount = tabScrollView.scrollX

        recycler.stopScroll()
        cacheCurrentScrollState()
    }


    override fun onStart() {
        super.onStart()

        requireView().post {
            requireView().doOnPreDraw {
                tabScrollView.scrollX = helper.cachedTabsScrolledAmount
            }
        }


    }


    override fun onStop() {
        lastAppliedList = null
        lastAppliedTab = null
        super.onStop()
    }





    private suspend fun awaitRecyclerAnimationsFinish(fromListChange: Boolean = true) {
        if (fromListChange) {
            swapOutRefreshingItemJob?.cancel()
            swapOutRefreshingItemJob = null
        }

        recycler.itemAnimator?.let { animator ->
            //making sure that the any previous animation get completed
            if (!animator.isRunning) {
                return@let
            }

            suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                animator.isRunning { cancellableContinuation.resume(Unit) }
            }
        }

        suspendCancellableCoroutine<Unit> { continuation ->
            if (recycler.isComputingLayout) {
                recycler.post {
                    continuation.resume(Unit)
                }
            } else {
                continuation.resume(Unit)
            }

        }
    }




    var swapOutRefreshingItemJob: Job? = null

    private fun swapOutRefreshingItem() {
        if (swapOutRefreshingItemJob?.isActive == true) return

        swapOutRefreshingItemJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            awaitRecyclerAnimationsFinish(false)

            val items = adapter.getCopyOfItems()

            var loadingItemPos = -1

            val countToCheck = min(5, items.size)
            for (i in 0 until countToCheck) {
                val item = items[items.lastIndex - i]
                if (item is ATContentLoading0) {
                    loadingItemPos = items.lastIndex - i
                    break
                }
            }

            ensureActive()

            if (loadingItemPos == -1) {
                Timber.d(">>> loadingItemPost not found")
                helper.requestNextPage()
                return@launchWhenStarted
            }

            adapter.swapOutItem(loadingItemPos, altLoadingItem)

            awaitRecyclerAnimationsFinish(false)
            helper.requestNextPage()
        }
    }







    private fun refreshDynamicBottomHeight() {
        val appbarOffset = utilViewModel.mainContentTopDistance.value

        coinManagementHelper.translationY = -1f * appbarOffset
    }

}