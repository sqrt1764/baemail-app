package app.bitcoin.baemail.twetch.ui

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.constraintlayout.widget.ConstraintLayout

class TouchingConstraintLayout : ConstraintLayout {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr)


    var onTouchReleased: ()->Unit = {}

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {

        if (MotionEvent.ACTION_UP == event.actionMasked
            || MotionEvent.ACTION_CANCEL == event.actionMasked) {
            onTouchReleased()
        }

        return super.onInterceptTouchEvent(event)
    }
}