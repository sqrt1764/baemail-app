package app.bitcoin.baemail.twetch.ui

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewOutlineProvider
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableRoundedBitmap
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableTwetchReplyColumn
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.FocusingEditText
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import coil.load
import coil.size.Precision
import coil.target.ImageViewTarget
import com.google.android.material.button.MaterialButton
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.min

class TwetchNewMessageFragment : Fragment(R.layout.fragment_twetch_new_message) {

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TwetchViewModel
//    private lateinit var utilViewModel: TwetchUtilViewModel

    private lateinit var container: ConstraintLayout
    private lateinit var floatingCoordinator: CoordinatorLayout
    private lateinit var scroller: NestedScrollView
    private lateinit var close: MaterialButton
    private lateinit var discard: MaterialButton
    private lateinit var title: TextView
    private lateinit var send: MaterialButton
    private lateinit var contentInput: FocusingEditText
    private lateinit var bottomGap: View

    private lateinit var quotedPost: ViewTwetchQuotedPost
    private lateinit var replyPost: ViewTwetchReplyPost

    private lateinit var contentContainer: LinearLayout

    private lateinit var charsRemaining: TextView

    private lateinit var icon: ImageView
    private lateinit var name: TextView
    private lateinit var userId: TextView







    private val imagePlaceholderDrawable: ColorDrawable by lazy {
        ColorDrawable(
                requireContext().getColorFromAttr(R.attr.colorSurfaceVariant)
        )
    }



    private val colorSelector: Int by lazy {
        R.color.selector_on_primary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorReplyColumn: Int by lazy {
        val color = requireContext().getColorFromAttr(R.attr.colorOnSurfaceSubtle)
        ColorUtils.setAlphaComponent(color, 88)
    }


    private val colorPrimary: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }
    private val colorOnPrimary: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorOnPrimary)
    }


    private val dp: Float by lazy {
        resources.displayMetrics.density
    }



    private val replyColumnDrawable = DrawableTwetchReplyColumn()




    private val appliedInsets = AppliedInsets()

    private val availableHeightHelper = AvailableScreenHeightHelper()


    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }


    private val valueLabelNewPost: String by lazy {
        requireContext().resources.getString(R.string.new_post)
    }

    private val valueLabelReplyPost: String by lazy {
        requireContext().resources.getString(R.string.reply_post)
    }

    private val valueLabelQuotePost: String by lazy {
        requireContext().resources.getString(R.string.quote_post)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)


        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

//        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
//                .get(TwetchUtilViewModel::class.java)




        container = requireView().findViewById(R.id.container)
        floatingCoordinator = requireView().findViewById(R.id.floating_coordinator)
        scroller = requireView().findViewById(R.id.scroller)
        close = requireView().findViewById(R.id.close)
        discard = requireView().findViewById(R.id.discard)
        title = requireView().findViewById(R.id.title)
        send = requireView().findViewById(R.id.send)
        contentInput = requireView().findViewById(R.id.content_input)
        bottomGap = requireView().findViewById(R.id.bottom_gap)

        quotedPost = requireView().findViewById(R.id.quoted_post)
        replyPost = requireView().findViewById(R.id.reply_post)

        contentContainer = requireView().findViewById(R.id.content_container)

        charsRemaining = requireView().findViewById(R.id.chars_remaining)

        icon = requireView().findViewById(R.id.icon)
        name = requireView().findViewById(R.id.user_name)
        userId = requireView().findViewById(R.id.user_id)




        availableHeightHelper.setup(
                requireContext(),
                requireActivity().window,
                viewLifecycleOwner,
                { requireView() }
        )

        availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
            height ?: return@Observer

            val maxHeight = container.height// - appliedInsets.top// - appliedInsets.bottom

            val keyboardHeight = max(maxHeight - height, 0)
            Timber.d("keyboardHeight: $keyboardHeight")


            floatingCoordinator.translationY = -1f * keyboardHeight

            //this view touches the bottom of the layout

            val canScrollDown = scroller.canScrollVertically(1)

            scroller.updatePadding(
                    bottom = /*(dp * 75).toInt() + */keyboardHeight
            )

//            if (!canScrollDown && inputContent.isFocused) {
//                Timber.d(">>> doing extra scroll")
//                scroller.scrollBy(0, keyboardHeight)
//            }


        })







        val replyColumnHeightExtra = (dp * (32 + 12 + 1)).toInt()

        replyPost.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            replyColumnDrawable.setColumnHeightLimitation(
                    true,
                    bottom - top + replyColumnHeightExtra
            )

        }



        val colorPrimary = requireContext().getColorFromAttr(R.attr.colorPrimary)

        val shape = ShapeAppearanceModel.Builder()
                .setAllCornerSizes(16 * dp)
                .build()
        val backgroundDrawable = MaterialShapeDrawable(shape)
        backgroundDrawable.strokeWidth = 1.5f * dp
        backgroundDrawable.fillColor = ColorStateList.valueOf(Color.TRANSPARENT)
        backgroundDrawable.strokeColor = ColorStateList
                .valueOf(ColorUtils.setAlphaComponent(colorPrimary, 180))
        quotedPost.background = backgroundDrawable

        quotedPost.setOnClickListener {
            //TODO open post ???
        }




        icon.clipToOutline = true
        icon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }
        icon.foreground = DrawableState.getNew(colorSelector)



        charsRemaining.clipToOutline = true
        charsRemaining.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, dp * 6)
            }
        }
        icon.foreground = DrawableState.getNew(colorSelector)




        replyColumnDrawable.setColumnWidth((6 * dp).toInt())
        replyColumnDrawable.setColumnPaddingLeft((36.5 * dp).toInt())
        replyColumnDrawable.setColumnColor(colorReplyColumn)



        contentContainer.background = LayerDrawable(arrayOf(
                replyColumnDrawable
        ))












        contentInput.addTextChangedListener(object : TextWatcher {
            var textBefore = ""

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                textBefore = s.toString()
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                viewModel.composePostUseCase.onContentChanged(s.toString())


                run { //scroll the view when the user presses `enter` so the cursor does not end up
                    // behind the keyboard
                    val countOfEnters = s.toString().filter { c -> c == '\n' }.length
                    val countOfEntersBefore = textBefore.filter { c -> c == '\n' }.length


                    if (countOfEnters > countOfEntersBefore) {
                        Timber.d("new enters detected, scroll the view")
                        val count = countOfEnters - countOfEntersBefore
                        viewLifecycleOwner.lifecycleScope.launch {
                            delay(50L)
                            scroller.smoothScrollBy(0, contentInput.lineHeight * count)
                        }
                    }
                }


                refreshRemainingCharacterCounter(s.toString())

                refreshSendButtonEnabledState(s.toString().trim().length)
            }


            fun refreshSendButtonEnabledState(length: Int) {
                val shouldBeEnabled = length in 1..256

                if (shouldBeEnabled == send.isEnabled) return
                send.isEnabled = shouldBeEnabled
                if (shouldBeEnabled) {
                    send.setBackgroundColor(colorPrimary)
                    send.setTextColor(colorOnPrimary)
                } else {
                    send.setBackgroundColor(Color.TRANSPARENT)
                    send.setTextColor(colorPrimary)
                }
            }
        })










        bottomGap.setOnClickListener {
            if (contentInput.requestFocus()) {
                imm.showSoftInput(contentInput, 0)
            }
        }

        close.setOnClickListener {
            imm.hideSoftInputFromWindow(requireView().windowToken, 0)

            findNavController().popBackStack()
        }

        discard.setOnClickListener {
            imm.hideSoftInputFromWindow(requireView().windowToken, 0)
            findNavController().popBackStack()

            viewModel.composePostUseCase.discardDelayed()

        }

        send.setOnClickListener {
            Timber.d("do send & pop")

            if (!viewModel.composePostUseCase.sendPost()) return@setOnClickListener

            imm.hideSoftInputFromWindow(requireView().windowToken, 0)
            contentInput.isEnabled = false

            findNavController().popBackStack()
        }





        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.authInfo.collect { info ->

                info?.profile?.let { profile ->
                    profile.icon?.let {
                        icon.load(it) {
                            placeholder(imagePlaceholderDrawable)
                            error(imagePlaceholderDrawable)
                            precision(Precision.INEXACT)
                            target(object : ImageViewTarget(icon) {
                                override fun onSuccess(result: Drawable) {
                                    val d = if (result is BitmapDrawable) {
                                        DrawableRoundedBitmap(requireContext(), 0f, result.bitmap)
                                    } else {
                                        result
                                    }
                                    super.onSuccess(d)
                                }
                            })
                        }
                    } ?: let {
                        icon.load(imagePlaceholderDrawable)
                    }

                    name.text = profile.name
                    userId.text = "@${profile.id}"


                }
            }
        }



        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.composePostUseCase.content.collect { model ->
                if (contentInput.text.toString().trim() != model.content.trim()) {
                    contentInput.setText(model.content)

                }

                if (model.replying == null) {
                    replyPost.visibility = View.GONE
                    replyColumnDrawable.alpha = 0
                } else {
                    replyColumnDrawable.alpha = 255
                    replyPost.visibility = View.VISIBLE
                    replyPost.applyModel(model.replying)
                }

                if (model.quoted == null) {
                    quotedPost.visibility = View.GONE
                } else {
                    quotedPost.visibility = View.VISIBLE
                    quotedPost.applyModel(model.quoted)
                }

                if (model.replying == null && model.quoted == null) {
                    if (title.text != valueLabelNewPost) {
                        title.text = valueLabelNewPost
                    }
                } else if (model.replying != null) {
                    if (title.text != valueLabelReplyPost) {
                        title.text = valueLabelReplyPost
                    }
                } else {
                    if (title.text != valueLabelQuotePost) {
                        title.text = valueLabelQuotePost
                    }
                }


            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.actionHelper.snackbarChannel.receiveAsFlow().collect { model ->
                Timber.d("receiving snackbar-event: $model")

                delay(150)
                try {
                    TwetchSnackbarHelper.displaySnackbar(model, floatingCoordinator)
                    delay(150)

                } finally {
//                    utilViewModel.onChangeSnackBarSpaceReservation(false)
                }

            }
        }



        refreshReplyingPost(ViewTwetchReplyPost.dummy)

    }



    private fun refreshReplyingPost(post: PostModel?) {
        if (post == null) {
            replyColumnDrawable.alpha = 0
            replyPost.visibility = View.GONE
        } else {
            replyColumnDrawable.alpha = 255


            val pxDistance = (16 + 12) * dp + //accounts for gone-margin of user-icon
                    5 * dp //a little extra distance behind the icon
            replyColumnDrawable.setColumnPaddingTop(pxDistance.toInt())

            replyPost.applyModel(post)
            replyPost.visibility = View.VISIBLE
        }
    }



    private fun refreshRemainingCharacterCounter(content: String) {
        val maxLength = 256
        val remainingLength =  maxLength - content.length
        charsRemaining.text = remainingLength.toString()
    }




    private fun focusOnInput() {
        if (contentInput.requestFocus()) {
            Timber.d("onStart got focus")

            if (!imm.isActive(contentInput)) imm.restartInput(contentInput)
        }
    }

    override fun onStart() {
        super.onStart()

        Timber.d("onStart $imm")
        focusOnInput()

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            //lazy implementation that scrolls to the content-input

            delay(666)
            if (viewModel.composePostUseCase.content.value.replying == null) return@launchWhenResumed

            //scroll to it
            scroller.smoothScrollBy(0, bottomGap.top)
        }
    }

}