package app.bitcoin.baemail.twetch.ui

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.bitcoin.baemail.R
import app.bitcoin.baemail.fundingWallet.core.presentation.CoinActivity
import app.bitcoin.baemail.fundingWallet.TopUpActivity
import app.bitcoin.baemail.intercept.AppDeepLinkType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ViewCoinManagementHelper
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.mapLatest
import timber.log.Timber
import javax.inject.Inject

class TwetchChatListFragment : Fragment(R.layout.fragment_twetch_chat_list) {

    companion object {
        const val KEY_SCROLL_POS = "scroll_pos"

    }

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel

    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var recycler: RecyclerView
    private lateinit var coinManagementHelper: ViewCoinManagementHelper


    lateinit var layoutManager: LinearLayoutManager
    private var lastSavedRecyclerState: Parcelable? = null

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter

    private var wasCreatedOnce = false


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)


        lastSavedRecyclerState = null

        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true


        val serializedRecyclerState: Parcelable? = savedInstanceState?.getParcelable(KEY_SCROLL_POS)






        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
                .get(TwetchUtilViewModel::class.java)



        recycler = requireView().findViewById(R.id.chat_recycler)
        swipeRefresh = requireView().findViewById(R.id.swipe_refresh)
        coinManagementHelper = requireView().findViewById(R.id.coin_management_helper)












        val dp = resources.displayMetrics.density

        val colorPrimary = requireContext().getColorFromAttr(R.attr.colorPrimary)
        val colorTransparentPrimary = Color.argb(
            200,
            Color.red(colorPrimary),
            Color.green(colorPrimary),
            Color.blue(colorPrimary)
        )


        swipeRefresh.setProgressViewOffset(true, (5 * dp).toInt(), (45 * dp).toInt())
        swipeRefresh.setOnRefreshListener {
            Timber.d("refresh!!!")
            viewModel.refreshChatList()
        }
        swipeRefresh.setColorSchemeColors(requireContext().getColorFromAttr(R.attr.colorOnPrimary))
        swipeRefresh.setProgressBackgroundColorSchemeColor(colorTransparentPrimary)


        /////////////////////////

        val chatItemListener = object : BaseListAdapter.ATTwetchChatInfoListener {
            override fun onClick(chat: TwetchChatInfoStore.ChatInfo) {
                val args = Bundle()
                args.putString(TwetchChatContentFragment.KEY_CHAT_ID, chat.id)


                findNavController().navigate(
                        R.id.action_twetchChatListFragment_to_twetchChatContentFragment,
                        args
                )
            }

        }

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atTwetchChatInfoListener = chatItemListener
            )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)

        serializedRecyclerState?.let {
            lastSavedRecyclerState = it
            layoutManager.onRestoreInstanceState(it)
        }

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                when (layoutManager.findFirstCompletelyVisibleItemPosition()) {
                    RecyclerView.NO_POSITION -> false
                    0 -> false
                    else -> true
                }.let { showShadow ->
                    if (showShadow) {
                        utilViewModel.onMainContentScrollChanged(false)
                    } else {
                        utilViewModel.onMainContentScrollChanged(true)
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (RecyclerView.SCROLL_STATE_IDLE == newState) {
                    Timber.d("new state saved")
                    lastSavedRecyclerState = layoutManager.onSaveInstanceState()
                }
            }
        })








        coinManagementHelper.clickTopUpListener = {
            startActivity(Intent(requireActivity(), TopUpActivity::class.java))
        }

        coinManagementHelper.clickSplitCoinsListener = {
            startActivity(Intent(requireActivity(), CoinActivity::class.java))
        }

        coinManagementHelper.clickRetryReconnectListener = {
            viewModel.chainSync.considerRetry()
        }






        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            utilViewModel.mainContentTopDistance.collect {
                refreshDynamicBottomHeight()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.coinManagementUseCase.state.mapLatest { it }
                .combine(utilViewModel.snackBarShowingState) { coinModel, snackBarReservation ->
                    coinModel to snackBarReservation
                }.collectLatest { (coinModel, snackBarReservation) ->
                    if (!snackBarReservation) {
                        coinManagementHelper.applyModel(coinModel)
                    } else {
                        //apply a model that will result in this bar being hidden
                        coinManagementHelper.applyModel(ViewCoinManagementHelper.HELPER_HIDDEN)
                    }
                }
        }




        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.isNowRefreshingChatListMessages.combine(
                viewModel.chatListInfoRefreshing
            ) { p0, p1 ->
                p0 || p1
            }.collect { state ->
                swipeRefresh.isRefreshing = state

            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.chatListInfo.collect { itemList ->

                //Timber.d("items: $itemList")

                adapter.setItems(itemList)

                lastSavedRecyclerState?.let {
                    layoutManager.onRestoreInstanceState(it)
                }


            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            val supported = listOf(
                AppDeepLinkType.TWETCH_CHAT
            )

            viewModel.deepLinkHelper.unsatisfiedLink.collectLatest { link ->
                link ?: return@collectLatest

                if (!supported.contains(link.type)) return@collectLatest

                val args = Bundle()
                args.putString(TwetchChatContentFragment.KEY_CHAT_ID, link.value)


                findNavController().navigate(
                    R.id.action_twetchChatListFragment_to_twetchChatContentFragment,
                    args
                )

                viewModel.deepLinkHelper.informLinkNavigationSatisfied()

            }
        }



    }




    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!wasCreatedOnce) return

        val state = layoutManager.onSaveInstanceState()
        lastSavedRecyclerState = state
        outState.putParcelable(KEY_SCROLL_POS, state)
    }







    private fun refreshDynamicBottomHeight() {
        val appbarOffset = utilViewModel.mainContentTopDistance.value

        coinManagementHelper.translationY = -1f * appbarOffset
    }


}