package app.bitcoin.baemail.twetch.ui

import android.content.Context
import androidx.annotation.Keep
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.twetch.db.dao.TwetchChatMessagesDao
import com.google.gson.Gson
import app.bitcoin.baemail.twetch.db.entity.TwetchChatMessage
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.data.util.twetchChatMessageSyncMetaPrefs
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.dropbox.android.external.store4.SourceOfTruth
import com.dropbox.android.external.store4.Store
import com.dropbox.android.external.store4.StoreResponse
import com.dropbox.android.external.store4.StoreBuilder
import com.dropbox.android.external.store4.Fetcher
import com.dropbox.android.external.store4.StoreRequest
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.threeten.bp.ZonedDateTime
import timber.log.Timber
import java.util.regex.Pattern
import kotlin.coroutines.Continuation


class TwetchChatMessageStore(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val gson: Gson,
    private val twetchChatMessagesDao: TwetchChatMessagesDao
) {

    companion object {
        const val NAME = "twetch_chat_message_sync_meta"
        const val PAGE_SIZE = 15

        fun mergeUpdatedMetaWithFullHistory(
                updated: ChatSyncMeta,
                metaHistory: List<ChatSyncMeta>
        ): List<ChatSyncMeta> {

            val remainingHistory = ArrayList(metaHistory)
            var currentMeta = updated

            if (remainingHistory.isEmpty()) {
                Timber.d("none of the meta instances need to be popped")
                remainingHistory.add(0, currentMeta)
                return remainingHistory
            }

            var nearestMeta = remainingHistory.first()
            remainingHistory.removeAt(0)


            while (true) {
                if (currentMeta.millisOldestMessage > nearestMeta.millisNewestMessage) {
                    //a gap still exists, keep the nearest history-meta
                    break
                }

                //merge the meta's & get rid of the older one
                currentMeta = currentMeta.copy(
                        millisOldestMessage = nearestMeta.millisOldestMessage
                )
                if (remainingHistory.isEmpty()) {
                    break
                }

                Timber.d("meta popped from history: $nearestMeta")
                nearestMeta = remainingHistory.first()
                remainingHistory.removeAt(0)

            }

            currentMeta = currentMeta.copy(
                    isSyncGapFilled = false
            )
            remainingHistory.add(0, currentMeta)

            return remainingHistory
        }
    }

    val ds = appContext.twetchChatMessageSyncMetaPrefs

    private val mappedChatSyncMeta = HashMap<SyncMetaKey, ChatSyncMeta>()

    fun checkChatHasMorePages(paymail: String, chatId: String): Boolean {
        val meta = mappedChatSyncMeta[SyncMetaKey(paymail, chatId)]

        val hasMorePages = meta?.serverHasMoreMessages ?: let {
            Timber.e(/*RuntimeException(), */"#checkChatHasMorePages was called before the creation of a stream & it's corresponding metadata")
            return true
        }

        Timber.d("#checkChatHasMorePages .. hasMorePages:$hasMorePages meta?.serverHasMoreMessages:${meta.serverHasMoreMessages}")
        return hasMorePages && !meta.fetchError
    }

    fun checkChatHasError(paymail: String, chatId: String): Boolean {
        val meta = mappedChatSyncMeta[SyncMetaKey(paymail, chatId)]

        return meta?.fetchError ?: false
    }




    private suspend fun readSyncMeta(
            paymail: String,
            chatId: String
    ): List<ChatSyncMeta> {
        val prefs = ds.data.first()
        val readValue = prefs[stringPreferencesKey("meta_${paymail}_${chatId}")]
                ?: return emptyList()

        val jsonArray = gson.fromJson(readValue, JsonArray::class.java)
        if (jsonArray.size() == 0) return emptyList()

        return jsonArray.map { item ->
            gson.fromJson(item.toString(), ChatSyncMeta::class.java)
        }
    }

    private suspend fun writeSyncMeta(
            paymail: String,
            chatId: String,
            metaList: List<ChatSyncMeta>
    ) {
        ds.edit { prefs ->
            val jsonArray = gson.toJson(metaList)
            prefs[stringPreferencesKey("meta_${paymail}_${chatId}")] = jsonArray.toString()
        }
    }


    //////////////

    private val IS_ASYNC_DEBUG = false

    data class AsyncSynchronized(
            val key: SyncMetaKey,
            val notifyCompleted: ()->Unit
    )

    data class AsyncSynchronizedHolder(
            val async: AsyncSynchronized,
            val awaitingCoroutines: MutableList<Continuation<AsyncSynchronized>> = mutableListOf()
    )


    private val runningAsyncSynchronizedMap = HashMap<SyncMetaKey, AsyncSynchronizedHolder>()

    private suspend fun awaitSafeToProceed(key: SyncMetaKey): AsyncSynchronized {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val existing = runningAsyncSynchronizedMap[key]
            if (existing == null) {
                //initialize a holder for the item to signify existence of requested synchronization
                val holderHolder = arrayOf<AsyncSynchronizedHolder?>(null)
                val cleanup: ()->Unit = {
                    //cleanup OR launch next pending
                    val holder = holderHolder[0] ?: throw RuntimeException()
                    val nextAwaitingToRun = holder.awaitingCoroutines.firstOrNull()
                    if (holder.awaitingCoroutines.isNotEmpty()) {
                        holder.awaitingCoroutines.removeAt(0)
                    }
                    if (nextAwaitingToRun == null) {
                        //cleanup the holder from the map
                        if (IS_ASYNC_DEBUG) Timber.d("#awaitSafeToProceed is now cleaning-up the holder from the map $key")
                        runningAsyncSynchronizedMap.remove(key)
                    } else {
                        //resume the next awaiting async-synchronized coroutine
                        if (IS_ASYNC_DEBUG) Timber.d("#awaitSafeToProceed is now resuming the next awaiting async-synchronized coroutine $key")
                        nextAwaitingToRun.resumeWith(Result.success(holder.async))
                    }
                }
                val aszd = AsyncSynchronized(key) {
                    //notify synchronized-completed invoked
                    cleanup()
                }
                val holder = AsyncSynchronizedHolder(
                        aszd
                )
                holderHolder[0] = holder
                //this signifies the existence of requested synchronization
                runningAsyncSynchronizedMap[key] = holder

                //proceed, no need to await
                if (IS_ASYNC_DEBUG) Timber.d("#awaitSafeToProceed is now resuming proceed, no need to await $key")
                cancellableContinuation.resumeWith(Result.success(aszd))
                return@suspendCancellableCoroutine
            } else {
                if (IS_ASYNC_DEBUG) Timber.d("#awaitSafeToProceed  AsyncSynchronizedHolder found $key")
            }

            if (IS_ASYNC_DEBUG) Timber.d("#awaitSafeToProceed is now delayed until continuation is resumed $key")
            existing.awaitingCoroutines.add(cancellableContinuation)

            cancellableContinuation.invokeOnCancellation {
                if (IS_ASYNC_DEBUG) Timber.d("#awaitSafeToProceed invokeOnCancellation; continuation removed from list $it")
                existing.awaitingCoroutines.remove(cancellableContinuation)
            }
        }
    }

    //////////////






    fun getMessageFlow(
        paymail: String,
        chatId: String,
        page: Int,
        refresh: Boolean = true
    ): Flow<StoreResponse<List<TwetchChatMessage>>> {
        return messageStore.stream(StoreRequest.cached(Key(paymail, chatId, page), refresh))
    }


    suspend fun getContentForKey(
        key: Key
    ): List<TwetchChatMessage> {
        return queryByFilter(key).first()
    }


    private val messageStore: Store<Key, List<TwetchChatMessage>> = StoreBuilder.from(
        fetcher = Fetcher.ofFlow { key ->
            flow {
                val syncMetaKey = SyncMetaKey(key.paymail, key.chatId)
                val asynchronized = awaitSafeToProceed(syncMetaKey)
                if (IS_ASYNC_DEBUG) Timber.d("after awaitSafeToProceed; fetcher")

                try {

                    var meta = mappedChatSyncMeta[syncMetaKey]
                    if (meta == null) {
                        meta = ChatSyncMeta(
                                key.paymail,
                                key.chatId,
                                -1,
                                -1,
                                false,
                                false,
                                true
                        )
                        mappedChatSyncMeta[syncMetaKey] = meta
                    }

                    val getMeta: suspend ()->ChatSyncMeta = {
                        mappedChatSyncMeta[syncMetaKey]!!
                    }
                    val updateMeta: suspend (ChatSyncMeta)->Unit = lambda@{ updated ->
                        mappedChatSyncMeta[syncMetaKey] = updated

                        if (updated.millisOldestMessage == -1L) throw RuntimeException()

                        val metaHistory = readSyncMeta(key.paymail, key.chatId)
                        val updatedHistory = mergeUpdatedMetaWithFullHistory(
                                updated,
                                metaHistory
                        )

                        mappedChatSyncMeta[syncMetaKey] = updatedHistory[0]
                        writeSyncMeta(
                                key.paymail,
                                key.chatId,
                                updatedHistory
                        )

                    }


                    if (IS_ASYNC_DEBUG) {
                        Timber.d("IS_ASYNC_DEBUG==true PRE delay fetcher")
                        delay(10000)
                        Timber.d("IS_ASYNC_DEBUG==true POST delay fetcher")
                    }


                    queryServer(key, getMeta, updateMeta).collect {
                        Timber.d("PRE emit}")
                        emit(it)
                        Timber.d("POST emit")
                    }

                } finally {
                    if (IS_ASYNC_DEBUG) Timber.d("executed asynchronized.notifyCompleted() fetcher")
                    asynchronized.notifyCompleted()
                }

            }

        },
        sourceOfTruth = SourceOfTruth.of(
            reader = ::queryByFilter,
            writer = ::insertAll,
            delete = ::clearByKey,
            deleteAll = ::deleteAll
        )
    ).build()



    private fun queryServer(
            key: Key,
            getMeta: suspend ()->ChatSyncMeta,
            updateMeta: suspend (ChatSyncMeta)->Unit
    ): Flow<List<TwetchChatMessage>> {

        return flow {

            Timber.d("queryServer key: $key")
            val meta = getMeta()

            if (meta.fetchError) TODO()


            //if (meta.millisOldestMessage != -1L) {
            if (key.lastPageIndex != 0) {
                //not fetching the very first page
                if (meta.millisOldestMessage == -1L) throw ZeroPageRequiredException()

                //query next page from db, if it is big enough then treat is a successful server-query

                val dbMessages = twetchChatMessagesDao.getForChat(
                        key.paymail,
                        key.chatId,
                        key.lastPageIndex * PAGE_SIZE,
                        PAGE_SIZE
                )
                val oldestMessage = dbMessages.lastOrNull()

                if (dbMessages.size == PAGE_SIZE
                        && oldestMessage != null
                        && meta.millisOldestMessage < oldestMessage.millisCreatedAt) {
                    Timber.d("chat history is synced back to this point")
                    return@flow
                }

                //chat history sync is not satisfying; must fetch from server
                Timber.d("chat history sync is not satisfying; must fetch from server")
            }



            //fetch page from server
            val pageOffset = key.lastPageIndex * PAGE_SIZE

            Timber.d("fetch page from server - ${key.lastPageIndex}; pageOffset:$pageOffset chatId:${key.chatId}")


            val result = nodeRuntimeRepository.runAsyncScript("""
                let millisStart = new Date().getTime();
                
                await context.twetch.ensureAuthHasCompleted();
                
                let millisPostAuth = new Date().getTime();
                
                let queryResult = await context.twetch.instance.query(`
                    {
                        allMessages(first: $PAGE_SIZE, orderBy: CREATED_AT_DESC, filter: {conversationId: {equalTo: "${key.chatId}"}}, offset: $pageOffset) {
                            nodes {
                                id
                                createdAt
                                userId
                                description
                                userByUserId {
                                    id
                                    name
                                    publicKey
                                    icon
                                    dmConversationId
                                }
                            }
                            pageInfo {
                                hasNextPage
                            }
                        }
                        allConversations(filter: {id: {equalTo: "${key.chatId}"}}) {
                            nodes {
                                me {
                                    encryptedKey
                                }
                            }
                        }
                    }
                `);
                
                let millisPostRequest = new Date().getTime();
                
                try {
                    let chatEncryptedKey = queryResult.allConversations.nodes[0].me.encryptedKey;
                    
                    let twetchPrivWIF = context.twetch.twetchPrivWIF;
                    
                    let twetchCryptoHelper = context.twetch.instance.crypto;
                    let chatAesKey = twetchCryptoHelper.eciesDecrypt(chatEncryptedKey, twetchPrivWIF);
                    
                    
                    queryResult.allMessages.nodes.forEach((item) => {
                        if (item.userId == null) return;
                        let description = item.description;
                        let decryptedMessage = twetchCryptoHelper.aesDecrypt(description, chatAesKey);
                        item.description = decryptedMessage;
                    });
                    
                    console.log('page of messages decrypted successfully');
                    
                    
                    
                    //chat messages can contain links to chat-encrypted images
                    let imagesToDownload = [];
                    
                    queryResult.allMessages.nodes.forEach((item) => {
                        if (item.userId == null) return;
                        
                        var matches = item.description.matchAll(new RegExp("(https://cimg.twetch.com/chats/[a-zA-Z_0-9/.\-]+)"));
                        
                        item.images = [];
                        for (var m of matches) {
                            let imageUrl = m[1];
                            
                            var imageFileName = imageUrl.replace('https://cimg.twetch.com/chats/', '');
                            imageFileName = imageFileName.split('/').join('__');
                            imageFileName = imageFileName.split('.').join('_');
                            
                            let metaInfo = {
                                url: imageUrl,
                                fileName: imageFileName
                            };
                            item.images.push(metaInfo);
                            imagesToDownload.push(metaInfo);
                            item.description = item.description.replace(imageUrl, '');
                        }
                        
                    });
                    
                    //download the found images
                    for (var i = 0; i < imagesToDownload.length; i++) {
                        const info = imagesToDownload[i];
                        
                        //check if the image is already downloaded
                        var foundName = await context.twetch.checkChatImageExists('${key.chatId}', info.fileName);
                        if (foundName) {
                            console.log('image already downloaded; filename:' + foundName);
                            info.fileName = foundName;
                            continue;
                        }
                        
                        console.log('about to request an image: ' + info.url);
                        try {
                            const response = await context.twetch.instance.client.get(info.url);
                            if (!response.data) {
                                console.log('image request data is NULL');
                                continue;
                            }
                            
                            const decryptedImg = twetchCryptoHelper.aesDecrypt(response.data, chatAesKey);
                            let finalFileName = context.twetch.writeChatImage('${key.chatId}', info.fileName, decryptedImg);
                            info.fileName = finalFileName;
                        } catch (e) {
                            console.log('crash when requesting image: ' + info.url);
                        }
                    }
                    
                    
                } catch (e) {
                    console.log(e, JSON.stringify(queryResult));
                    throw e;
                }
                
                let millisPostDecryptAndImageDownloadFunction = new Date().getTime();
                
                console.log("time spent awaiting twetch-auth:" + (millisPostAuth - millisStart) + "ms; time spent on page of chat-messages:" + (millisPostRequest - millisPostAuth) + "ms; time spent on decrypt and image download:" + (millisPostDecryptAndImageDownloadFunction - millisPostRequest) + "ms");
            
                
                
                return JSON.stringify(queryResult);
                """.trimIndent()
            )

            if (result is RunScript.Response.Success) {

                val (hasNextPage, messageList) = try {
                    parseSuccessResponse(result.content, key.chatId, key.paymail)
                } catch (e: Exception) {
                    Timber.d(e)
                    throw e
                }

                Timber.d("chat-messages successfully queried... size: ${messageList.size} ... hasNextPage:$hasNextPage")


                val millisPageNewestMessage = messageList.firstOrNull()?.millisCreatedAt ?: -1L
                val millisPageOldestMessage = messageList.lastOrNull()?.millisCreatedAt ?: -1L

                var millisNewestMessage = meta.millisNewestMessage
                var millisOldestMessage = meta.millisOldestMessage
                if (key.lastPageIndex == 0) {
                    //just fetched the first page
                    millisNewestMessage = millisPageNewestMessage
                    millisOldestMessage = millisPageOldestMessage
                }

                if (millisPageOldestMessage < millisOldestMessage) {
                    millisOldestMessage = millisPageOldestMessage
                }


                //todo must retrieve images refrenced in messages, their content must be decrypted using `chatAesKey`
                //todo
                //todo
                //todo

                //todo consider noting down the message-time of the very first message (if hasNextPage == FALSE) to avoid an unnecessary request

                updateMeta(meta.copy(
                        serverHasMoreMessages = hasNextPage,
                        millisNewestMessage = millisNewestMessage,
                        millisOldestMessage = millisOldestMessage
                ))

                Timber.d("chat-messages successfully queried... chatId:${key.chatId}")

                emit(messageList)


            } else {
                result as RunScript.Response.Fail

                Timber.d("chat-messages failed to be queried; chatId:${key.chatId}")

                updateMeta(meta.copy(
                        fetchError = true
                ))

                throw RuntimeException()
            }




        }
    }




    private fun queryByFilter(key: Key): Flow<List<TwetchChatMessage>> {
        val offset = 0
        val limit = (key.lastPageIndex + 1) * PAGE_SIZE

        return twetchChatMessagesDao.flowForChat(key.paymail, key.chatId, offset, limit)
    }

    private suspend fun deleteAll() {
        twetchChatMessagesDao.deleteAll()
    }

    suspend fun insertAll(key: Key, messages: List<TwetchChatMessage>) {
        Timber.d("PRE insertAll key:$key")
        twetchChatMessagesDao.insertAll(messages)
    }

    suspend fun clearByKey(key: Key) {
        twetchChatMessagesDao.deleteForChat(key.paymail, key.chatId)
    }










    /////////////////////////////////////////////////////////

    suspend fun fetchNewlyReceivedMessages(paymail: String, chatId: String) {
        //todo
        //todo
        //todo must make sure that this is never done concurrently with the store-fetcher


        Timber.d("fetchNewlyReceivedMessages paymail:$paymail chatId:$chatId")

        val syncMetaKey = SyncMetaKey(paymail, chatId)
        val asynchronized = awaitSafeToProceed(syncMetaKey)
        if (IS_ASYNC_DEBUG) Timber.d("after awaitSafeToProceed; fetchNewlyReceivedMessages")

        try {

            val syncMeta = mappedChatSyncMeta[syncMetaKey]
            if (syncMeta == null || syncMeta.millisOldestMessage == -1L || syncMeta.fetchError) {
                Timber.d("this chat has not yet been synced; exiting")
                return
            }


            var pagingMeta = ChatSyncMeta(
                    paymail,
                    chatId,
                    -1L,
                    -1L,
                    false,
                    false,
                    true
            )

            var pagingKey = Key(
                    paymail,
                    chatId,
                    0
            )

            try {
                val getMeta: suspend ()->ChatSyncMeta = {
                    pagingMeta
                }
                val updateMeta: suspend (ChatSyncMeta)->Unit = {
                    pagingMeta = it
                }

                if (IS_ASYNC_DEBUG) {
                    Timber.d("IS_ASYNC_DEBUG==true PRE delay fetcher")
                    delay(10000)
                    Timber.d("IS_ASYNC_DEBUG==true POST delay fetcher")
                }

                for (i in 0 until 20) {
                    if (i == 20) throw RuntimeException()

                    Timber.d("fetchNewlyReceivedMessages ... loop:$i ... pagingKey:$pagingKey")

                    queryServer(pagingKey, getMeta, updateMeta).collect { list ->
                        Timber.d("fetchNewlyReceivedMessages ... inserting fetched messages: ${list.size}")
                        insertAll(pagingKey, list)
                    }

                    if (pagingMeta.millisOldestMessage == -1L) throw RuntimeException()
                    if (pagingMeta.fetchError) throw RuntimeException()
                    if (pagingMeta.millisOldestMessage <= syncMeta.millisNewestMessage) {
                        //all of the missing latest messages have been retrieved
                        break
                    }

                    pagingKey = pagingKey.copy(
                            lastPageIndex = pagingKey.lastPageIndex + 1
                    )
                }


                val updated = syncMeta.copy(
                        millisNewestMessage = pagingMeta.millisNewestMessage
                )
                mappedChatSyncMeta[syncMetaKey] = updated
                val metaHistory = readSyncMeta(paymail, chatId)
                val updatedHistory = mergeUpdatedMetaWithFullHistory(
                        updated,
                        metaHistory
                )
                writeSyncMeta(
                        paymail,
                        chatId,
                        updatedHistory
                )
                Timber.d("fetchNewlyReceivedMessages completed for paymail:$paymail chatId:$chatId; updated millisNewestMessage:${pagingMeta.millisNewestMessage}")

            } catch (e: Exception) {
                if (e is CancellationException) return

                Timber.e(e)
                TODO()
            }


        } finally {
            if (IS_ASYNC_DEBUG) Timber.d("executed asynchronized.notifyCompleted() fetchNewlyReceivedMessages")
            asynchronized.notifyCompleted()
        }

    }




    /////////////////////////////////////////////////////////


    private fun parseSuccessResponse(
        content: String,
        chatId: String,
        paymail: String
    ): Pair<Boolean, List<TwetchChatMessage>> {
        val responseJson = gson.fromJson(content, JsonObject::class.java)
        val allMessagesJson = responseJson.getAsJsonObject("allMessages")
        val pageInfo = allMessagesJson.getAsJsonObject("pageInfo")
        val hasNextPage = pageInfo.get("hasNextPage").asBoolean

        val parsedList = mutableListOf<TwetchChatMessage>()

        val nodesJsonArr = allMessagesJson.getAsJsonArray("nodes")

        nodesJsonArr.forEach {
            val messageJson = it.asJsonObject
            val id = messageJson.get("id").asString
            val createdAt = messageJson.get("createdAt").asString
            val millisCreatedAt = createdAt.let {
                try {
                    ZonedDateTime.parse(it).toInstant().toEpochMilli()
                } catch (e: Exception) {
                    Timber.e(e)
                    -1
                }
            }

            val userId = if (messageJson.has("userId")) {
                messageJson.get("userId").let {
                    if (it.isJsonNull) "-1" else it.asString
                }
            } else "-1"


            val description = if (messageJson.has("description")) {
                messageJson.get("description").let {
                    if (it.isJsonNull) "-1" else it.asString
                }
            } else "-1"


            val imageFilesList = if (messageJson.has("images")) {
                messageJson.getAsJsonArray("images").let { imagesMetaArray ->
                    if (imagesMetaArray.size() == 0) {
                        return@let emptyList<String>()
                    }
                    imagesMetaArray.map { item ->
                        val metaJson = item.asJsonObject
                        metaJson.get("fileName").asString
                    }
                }
            } else {
                emptyList()
            }

            val participant = if (messageJson.has("userByUserId")) {
                val userJson = messageJson.get("userByUserId")
                if (!userJson.isJsonNull) {
                    val jsonObj = userJson.asJsonObject
                    val userIdParticipant = jsonObj.get("id").asString
                    val name = if (jsonObj.get("name").isJsonNull) {
                        "@$userIdParticipant"
                    } else {
                        jsonObj.get("name").asString
                    }
                    val publicKey = jsonObj.get("publicKey").asString
                    val icon = if (jsonObj.has("icon") && !jsonObj.get("icon").isJsonNull) {
                        jsonObj.get("icon").asString
                    } else null

                    val dmConversationId = if (jsonObj.has("dmConversationId")
                        && !jsonObj.get("dmConversationId").isJsonNull) {
                        jsonObj.get("dmConversationId").asString
                    } else null

                    TwetchChatInfoStore.Participant(
                        userIdParticipant,
                        name,
                        publicKey,
                        icon,
                        dmConversationId
                    )
                } else null
            } else null

            parsedList.add(TwetchChatMessage(
                paymail,
                id,
                chatId,
                millisCreatedAt,
                userId,
                description,
                imageFilesList,
                participant
            ))
        }

        return hasNextPage to parsedList
    }




    data class Key(
        val paymail: String,
        val chatId: String,
        val lastPageIndex: Int
    )



    data class SyncMetaKey(
        val paymail: String,
        val chatId: String
    )

    @Keep
    data class ChatSyncMeta(
            @SerializedName("paymail") val paymail: String,
            @SerializedName("chatId") val chatId: String,
            @SerializedName("millisNewestMessage") val millisNewestMessage: Long,
            @SerializedName("millisOldestMessage") val millisOldestMessage: Long,
            @SerializedName("isSyncGapFilled") val isSyncGapFilled: Boolean,
            @SerializedName("fetchError") val fetchError: Boolean,
            @SerializedName("serverHasMoreMessages") val serverHasMoreMessages: Boolean
    )

    class ZeroPageRequiredException : RuntimeException()

    data class TwetchImageExtractionResult(
        val content: String,
        val links: List<String>
    ) {

        companion object {
            private const val TWETCH_IMG_PATTERN = "(https://cimg.twetch.com/chats/[a-zA-Z_0-9/.\\-]+)"

            fun extractTwetchImages(content: String): TwetchImageExtractionResult {
                val urlMatcher = Pattern.compile(TWETCH_IMG_PATTERN).matcher(content)

                val foundLinks = mutableListOf<String>()
                while (urlMatcher.find()) {
                    foundLinks.add(urlMatcher.group())
                }

                if (foundLinks.isEmpty()) {
                    return TwetchImageExtractionResult(
                        content,
                        emptyList()
                    )
                }

                urlMatcher.reset()
                val modifiedContent = urlMatcher.replaceAll("")

                return TwetchImageExtractionResult(
                    modifiedContent,
                    foundLinks
                )
            }
        }
    }
}