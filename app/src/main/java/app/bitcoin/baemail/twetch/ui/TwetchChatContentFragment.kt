package app.bitcoin.baemail.twetch.ui

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateFormat
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableOval
import app.bitcoin.baemail.core.presentation.drawable.DrawableRoundedBitmap
import app.bitcoin.baemail.core.presentation.view.recycler.ATContentLoading0
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchChatMessageMeItem
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchChatMessageOtherItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterTypeEnum
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import coil.load
import coil.target.ImageViewTarget
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.gson.Gson
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber
import java.io.File
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.math.max
import kotlin.math.min

class TwetchChatContentFragment : Fragment(R.layout.fragment_twetch_chat_content) {

    companion object {
        const val KEY_SCROLL_POS = "scroll_pos"
        const val KEY_CHAT_ID = "id"
    }

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var gson: Gson

    private lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel


    private lateinit var topbar: View
    private lateinit var input: EditText
    private lateinit var sendMessage: Button
    private lateinit var close: Button
    private lateinit var participants: Button
    private lateinit var chatIcon: ImageView
    private lateinit var chatTitle: TextView
    private lateinit var recycler: RecyclerView
    private lateinit var floatingContainer: ConstraintLayout
    private lateinit var contentContainer: ConstraintLayout
    private lateinit var floatingIndicator: ConstraintLayout
    private lateinit var floatingLoading: TextView
    private lateinit var floatingScrollToBottom: TextView
    private lateinit var floatingAction: TextView


    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter

    private var wasCreatedOnce = false


    private lateinit var helper: TwetchChatContentUseCase


    private val imagePlaceholderDrawable: Drawable by lazy {
        ColorDrawable(requireContext().getColorFromAttr(R.attr.colorSurfaceVariant))
    }


    private val heightLD = MutableLiveData<Int>()
    private val dynamicHeightItem = ATHeightDecorDynamic("0", heightLD)


    private val chatDateFormat: java.text.DateFormat by lazy {
        DateFormat.getMediumDateFormat(requireContext())
    }
    private val chatTimeFormat: java.text.DateFormat by lazy {
        DateFormat.getTimeFormat(requireContext())
    }


    private val appliedInsets = AppliedInsets()
    private val availableHeightHelper = AvailableScreenHeightHelper()




    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }




    private val inputWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            s ?: return

            helper.onMessageInputChanged(s.toString())
        }
    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)



        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
                .get(TwetchUtilViewModel::class.java)


        val chatId = requireArguments().getString(KEY_CHAT_ID)!!

        helper = viewModel.getChatMessages(chatId)


        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true

        val serializedRecyclerState: Parcelable? = helper.cachedOutputState






        input = requireView().findViewById(R.id.input)
        sendMessage = requireView().findViewById(R.id.send_message)
        contentContainer = requireView().findViewById(R.id.content_container)
        floatingContainer = requireView().findViewById(R.id.floating_container)
        floatingIndicator = requireView().findViewById(R.id.floating_indicator)
        floatingLoading = requireView().findViewById(R.id.loading)
        floatingScrollToBottom = requireView().findViewById(R.id.scroll_to_bottom)
        floatingAction = requireView().findViewById(R.id.action_button)
        close = requireView().findViewById(R.id.close)
        chatIcon = requireView().findViewById(R.id.chat_icon)
        participants = requireView().findViewById(R.id.participants)
        chatTitle = requireView().findViewById(R.id.chat_title)
        recycler = requireView().findViewById(R.id.message_list)
        topbar = requireView().findViewById(R.id.topbar)








        contentContainer.doOnApplyWindowInsets { view, windowInsets, initialPadding ->

            appliedInsets.top = windowInsets.systemWindowInsetTop
            appliedInsets.bottom = windowInsets.systemWindowInsetBottom

            windowInsets
        }



        availableHeightHelper.setup(
                requireContext(),
                requireActivity().window,
                viewLifecycleOwner,
                { requireView() }
        )

        availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
            height ?: return@Observer
            refreshDynamicBottomHeight()
        })




        val dp = resources.displayMetrics.density



        val colorPrimary = requireContext().getColorFromAttr(R.attr.colorPrimary)
        val colorSurfaceVariant = requireContext().getColorFromAttr(R.attr.colorSurfaceVariant)





        chatIcon.clipToOutline = true
        chatIcon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }

        }







        val drawableLoadingBackground = DrawableOval(requireContext()).also {
            val ovalStrokeWidth = 1.5f * dp
            it.setBgColor(colorSurfaceVariant)
            it.setStrokeWidth(ovalStrokeWidth)
            it.setStrokeColor(colorPrimary)
            it.setPaddings(0f, 0f, 0f, 0f)
            it.initPaints()
        }
        floatingLoading.background = drawableLoadingBackground

        floatingLoading.clipToOutline = true
        floatingLoading.outlineProvider = drawableLoadingBackground.getOutlineProvider()





        floatingAction.clipToOutline = true
        floatingAction.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, p0.height / 2f)
            }
        }
        val dangerColor = requireContext().getColorFromAttr(R.attr.colorDanger)
        val transparentDangerColor = Color.argb(
                200,
                Color.red(dangerColor),
                Color.green(dangerColor),
                Color.blue(dangerColor)
        )
        floatingAction.background = ColorDrawable(transparentDangerColor)

        floatingAction.setOnClickListener {
            if (adapter.itemCount > 0) {
                recycler.smoothScrollToPosition(adapter.itemCount - 1)
            }
            helper.reset()
            floatingAction.visibility = View.GONE
        }








        floatingScrollToBottom.clipToOutline = true
        floatingScrollToBottom.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, p0.height / 2f)
            }
        }
        val colorTransparentPrimary = Color.argb(
                200,
                Color.red(colorPrimary),
                Color.green(colorPrimary),
                Color.blue(colorPrimary)
        )
        floatingScrollToBottom.background = ColorDrawable(colorTransparentPrimary)

        floatingScrollToBottom.setOnClickListener {
            recycler.smoothScrollToPosition(adapter.itemCount - 1)
        }







        close.setOnClickListener {
            findNavController().popBackStack()
        }


        participants.setOnClickListener {
            val args = Bundle()
            args.putString(TwetchChatParticipantsFragment.KEY_CHAT_ID, chatId)

            findNavController().navigate(
                    R.id.action_twetchChatContentFragment_to_twetchChatParticipantsFragment,
                    args
            )
        }


        val topbarElevatedHeight = resources.getDimension(R.dimen.inner_topbar_floating_elevation) / dp
        val topbarBackground = MaterialShapeDrawable.createWithElevationOverlay(requireContext(), 0f)
        topbar.background = topbarBackground



        val inputbarElevatedHeight = 6.0f
        val inputbarBackground = MaterialShapeDrawable.createWithElevationOverlay(requireContext(), inputbarElevatedHeight)
        inputbarBackground.fillColor = ColorStateList.valueOf(colorSurfaceVariant)
        inputbarBackground.strokeColor = ColorStateList.valueOf(requireContext().getColorFromAttr(R.attr.colorPrimary))
        inputbarBackground.strokeWidth = dp * 2.1f
        inputbarBackground.setCornerSize(dp * 12)

        input.background = inputbarBackground




        floatingContainer.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            if (bottom - top == oldBottom - oldTop) return@addOnLayoutChangeListener
            refreshDynamicBottomHeight()
        }


        val contentLoadingListener = object : BaseListAdapter.ATContentLoadingListener {
            override fun onBound() {
                scheduleLongLoadingFallback()
                helper.userRequestedNextPage()
            }
        }

        val messageMeListener = object : BaseListAdapter.ATTwetchChatMessageMeListener {
            override fun onProfileClicked(participant: TwetchChatInfoStore.Participant) {
                Toast.makeText(requireContext(), "onProfileClicked", Toast.LENGTH_SHORT).show()
            }

            override fun onContentClicked(
                chatId: String,
                messageId: String,
                content: String,
                participant: TwetchChatInfoStore.Participant,
                dateLabel: String,
                millisCreatedAt: Long
            ) {
                Toast.makeText(requireContext(), "onContentClicked", Toast.LENGTH_SHORT).show()
            }

            override fun onImageClicked(image: File) {
                val args = Bundle().also {
                    it.putString(TwetchChatFullImageFragment.KEY_IMAGE_FILE_PATH, image.absolutePath)
                }
                findNavController().navigate(R.id.action_twetchChatContentFragment_to_twetchChatFullImageFragment, args)
            }
        }

        val messageOtherListener = object : BaseListAdapter.ATTwetchChatMessageOtherListener {
            override fun onProfileClicked(participant: TwetchChatInfoStore.Participant) {
                val args = Bundle().also {
                    it.putString(TwetchUserProfileFragment.KEY_USER_ID, participant.id)
                }


                findNavController().navigate(R.id.action_global_twetchUserProfile, args)
            }

            override fun onContentClicked(
                chatId: String,
                messageId: String,
                content: String,
                participant: TwetchChatInfoStore.Participant,
                dateLabel: String,
                millisCreatedAt: Long
            ) {
                Toast.makeText(requireContext(), "onContentClicked", Toast.LENGTH_SHORT).show()
            }

            override fun onImageClicked(image: File) {
                val args = Bundle().also {
                    it.putString(TwetchChatFullImageFragment.KEY_IMAGE_FILE_PATH, image.absolutePath)
                }
                findNavController().navigate(R.id.action_twetchChatContentFragment_to_twetchChatFullImageFragment, args)

            }
        }



        adapter = BaseListAdapter(
                viewLifecycleOwner.lifecycle,
                BaseAdapterHelper(
                    adapterFactory,
                    atContentLoadingListener = contentLoadingListener,
                    atTwetchChatMessageMeListener = messageMeListener,
                    atTwetchChatMessageOtherListener = messageOtherListener
                )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)
        layoutManager.stackFromEnd = true

        serializedRecyclerState?.let {
            layoutManager.onRestoreInstanceState(it)
        }

        recycler.adapter = adapter
        recycler.layoutManager = layoutManager
        recycler.overScrollMode = View.OVER_SCROLL_NEVER

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                recyclerView.canScrollVertically(-1).let { showShadow ->
                    if (showShadow) {
                        utilViewModel.onMainContentScrollChanged(false)
                        topbarBackground.elevation = topbarElevatedHeight
                        topbar.elevation = topbarElevatedHeight * dp
                        close.elevation = topbarElevatedHeight * dp
                        chatIcon.elevation = topbarElevatedHeight * dp
                        chatTitle.elevation = topbarElevatedHeight * dp
                        participants.elevation = topbarElevatedHeight * dp

                    } else {
                        utilViewModel.onMainContentScrollChanged(true)
                        topbarBackground.elevation = 0f
                        topbar.elevation = 0f
                        close.elevation = 0f
                        chatIcon.elevation = 0f
                        chatTitle.elevation = 0f
                        participants.elevation = 0f
                    }
                }

                layoutManager.findLastVisibleItemPosition().let { pos ->
                    when (pos) {
                        RecyclerView.NO_POSITION -> null
                        else -> pos
                    }?.let { validPos ->
                        if (adapter.getItemOfPosition(validPos).type == AdapterTypeEnum.HEIGHT_DECOR_DYNAMIC) {
                            floatingScrollToBottom.visibility = View.GONE
                        } else {
                            floatingScrollToBottom.visibility = View.VISIBLE
                        }
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            }
        })







        sendMessage.setOnClickListener {
            val content = input.text.toString()
            if (content.isBlank()) {
                input.setText("")
                return@setOnClickListener
            }

            helper.sendMessage()
        }







        //todo temporarily hide the go-to-participants button
        participants.visibility = View.INVISIBLE



        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            withContext(Dispatchers.Default) {

                var lastInitializedChat: TwetchChatInfoStore.ChatInfo? = null

                helper.chatContent.combine(helper.messageSendingState) { p0, p1 ->
                    p0 to p1
                }.collectLatest { (content, messageSendingState) ->
                    content ?: return@collectLatest


                    if (content.info != lastInitializedChat) {
                        lastInitializedChat = content.info

                        val name = if (content.info.selfChat) {
                            content.me.name
                        } else {
                            content.info.name
                        }
                        val icon = if (content.info.selfChat) {
                            content.me.icon
                        } else {
                            content.info.icon
                        }


                        chatTitle.text = name

                        icon?.let {
                            chatIcon.load(it) {
                                placeholder(imagePlaceholderDrawable)
                                error(imagePlaceholderDrawable)

                                target(object : ImageViewTarget(chatIcon) {
                                    override fun onSuccess(result: Drawable) {
                                        val d = if (result is BitmapDrawable) {
                                            DrawableRoundedBitmap(requireContext(), 0f, result.bitmap)
                                        } else {
                                            result
                                        }
                                        super.onSuccess(d)
                                    }
                                })
                            }
                        } ?: chatIcon.load(imagePlaceholderDrawable)

                    }



                    applyMessages(content, messageSendingState)


                }
            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            utilViewModel.mainContentTopDistance.collect {
                refreshDynamicBottomHeight()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            helper.chatMessagesRefreshing.collect {
                if (it) {
                    floatingLoading.visibility = View.VISIBLE
                } else {
                    floatingLoading.visibility = View.INVISIBLE

                    clearLongLoadingFallback()
                }
            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
//            helper.chatMessagesError.combine(helper.messageSendingState) { p0, p1 ->
//                p0 to p1
//            }.collect { (isMessageError, sendingState) ->
            helper.chatMessagesError.collect { isMessageError ->

                if (isMessageError/* || SendingState.ERROR == sendingState*/) {
                    floatingAction.visibility = View.VISIBLE
                } else {
                    floatingAction.visibility = View.GONE
                }

                clearLongLoadingFallback()


//                when (sendingState) {
//                    SendingState.READY -> {
//                        input.isEnabled = true
//                    }
//                    SendingState.ERROR -> {
//
//                        input.isEnabled = false
//                    }
//                }



            }

        }



        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            helper.messageInput.collect { value ->
                input.removeTextChangedListener(inputWatcher)

                if (input.text.toString() != value/* && !input.isFocused*/) {
                    input.setText(value)
                }

                input.addTextChangedListener(inputWatcher)

            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            helper.chatDisabledState.collect {
                if (it) {
                    Toast.makeText(requireContext(), R.string.chat_removed, Toast.LENGTH_SHORT).show()
                    findNavController().popBackStack()
                }

            }
        }

    }

    private var longLoadingFallbackJob: Job? = null

    private fun scheduleLongLoadingFallback() { //todo move this to the fetching-store
        longLoadingFallbackJob?.cancel()
        longLoadingFallbackJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            delay(11000)

            //show the reset button
            floatingAction.visibility = View.VISIBLE
        }
    }

    private fun clearLongLoadingFallback() {
        longLoadingFallbackJob?.cancel()
        longLoadingFallbackJob = null
    }


    private fun refreshDynamicBottomHeight() {
        val appbarOffset = utilViewModel.mainContentTopDistance.value
        val floatingContainerHeight = floatingContainer.height
        val floatingContainerBotMargin = floatingContainer.layoutParams.let {
            it as ViewGroup.MarginLayoutParams
            it.bottomMargin
        }
        val keyboardCoveredHeight = availableHeightHelper.availableHeightLD.value ?: return


        val maxFragmentHeight = contentContainer.height

        val keyboardHeight = max(maxFragmentHeight - keyboardCoveredHeight, 0)
        //Timber.d("maxFragmentHeight:$maxFragmentHeight ...available-height:$keyboardCoveredHeight ...keyboardHeight: $keyboardHeight")


        ///////

        heightLD.value = appbarOffset + floatingContainerHeight + (2 * floatingContainerBotMargin) + keyboardHeight
        floatingContainer.translationY = -1f * (keyboardHeight + appbarOffset)
        floatingIndicator.translationY = -1f * (keyboardHeight + appbarOffset)
    }


    private suspend fun applyMessages(
            content: ChatContentModel,
            messageSendingState: List<SendingTwetchMessage>
    ) {
        val messageList = content.messages.reversed()
        val chatInfo = content.info

        val successfulSentList = messageSendingState.filter { it.createdMessage != null }

        val addedSendingListMessages = mutableListOf<String>()

        val chatItemList = try {
            Timber.d("got chat-messages size: ${messageList.size}")

            messageList.map { chatMessage ->

                val messageTimeLabel =
                        "${
                            chatDateFormat.format(chatMessage.millisCreatedAt)
                        } - ${
                            chatTimeFormat.format(chatMessage.millisCreatedAt)
                        }"

                if (chatMessage.user?.id == content.me.id) {
                    val foundMatchingSuccessfulSentMessage = successfulSentList.find {
                        it.createdMessage == chatMessage
                    }

                    val itemId = foundMatchingSuccessfulSentMessage?.id ?: chatMessage.id
                    foundMatchingSuccessfulSentMessage?.let {
                        addedSendingListMessages.add(it.id)
                    }

                    val imageFile: File? = if (chatMessage.images.isEmpty()) {
                        null
                    } else {
                        File(requireContext().filesDir.absolutePath
                                + "/Twetch/chatImage/${chatInfo.id}/${chatMessage.images[0]}")
                    }
                    val imageFileFound = imageFile?.exists() ?: false

                    ATTwetchChatMessageMeItem(
                        itemId,
                        chatInfo.id,
                        content.me,
                        chatMessage.description,
                        imageFile,
                        imageFileFound,
                        messageTimeLabel,
                        chatMessage.millisCreatedAt,
                        true,
                        true
                    )
                } else {

                    val imageFile: File? = if (chatMessage.images.isEmpty()) {
                        null
                    } else {
                        File(requireContext().filesDir.absolutePath
                                + "/Twetch/chatImage/${chatInfo.id}/${chatMessage.images[0]}")
                    }
                    val imageFileFound = imageFile?.exists() ?: false

                    ATTwetchChatMessageOtherItem(
                        chatMessage.id,
                        chatInfo.id,
                        chatMessage.user,
                        chatMessage.description,
                        imageFile,
                        imageFileFound,
                        messageTimeLabel,
                        chatMessage.millisCreatedAt,
                        true,
                        true
                    )
                }
            }.let {
                ArrayList(it)
            }

        } catch (e: Exception) {
            Timber.e(e, "$messageList")
            mutableListOf<AdapterType>()
        }


        //add items for sending-messages
        if (messageSendingState.isNotEmpty()) {
            ArrayList(messageSendingState).sortedBy { it.millisCreated }.mapNotNull { item ->

                if (addedSendingListMessages.contains(item.id))
                    return@mapNotNull null //a message is already added for this item

                val messageTimeLabel =
                    "${
                        chatDateFormat.format(item.millisCreated)
                    } - ${
                        chatTimeFormat.format(item.millisCreated)
                    }"

                ATTwetchChatMessageMeItem(
                    item.id,
                    chatInfo.id,
                    item.participant,
                    item.content,
                    null,
                    false,
                    messageTimeLabel,
                    item.millisCreated,
                    true,
                    true,
                    !item.didError,
                    item.didError
                )

            }.toCollection(chatItemList)
        }


        //modify the list-items to hide unnecessary elements, like
        // repeating icons in repeating message from the same person
        val messageTimeGap = 1000 * 60L
        chatItemList.mapIndexed { index, item ->
            if (index + 1 == chatItemList.size) return@mapIndexed item
            val next = chatItemList[index + 1]

            if (next.type != item.type) return@mapIndexed item

            when (item) {
                is ATTwetchChatMessageMeItem -> {
                    next as ATTwetchChatMessageMeItem
                    if (next.millisCreatedAt - item.millisCreatedAt < messageTimeGap) {
                        item.showIcon = false
                        item.showDate = false
                    }
                }
                is ATTwetchChatMessageOtherItem -> {
                    next as ATTwetchChatMessageOtherItem
                    if (next.participant?.id == item.participant?.id
                            && next.millisCreatedAt - item.millisCreatedAt < messageTimeGap) {
                        item.showIcon = false
                        item.showDate = false
                    }

                }
                else -> {
                }
            }

            item
        }




        //todo add a no-chat-messages item if the list is empty

        //add load-more-indicating item to the top of the list
        Timber.d("applyingMessages  hasError:${content.hasFetchError} hasMorePages:${content.hasMorePages} page:${content.page}")
        if (content.hasMorePages) {
            chatItemList.add(0, ATContentLoading0(content.page))
        }

        //add a gap to the bottom of the list
        chatItemList.add(dynamicHeightItem)

        withContext(Dispatchers.Main) {

            recycler.itemAnimator?.let {
                //making sure that the any previous animation get completed
                if (!it.isRunning) {
                    Timber.d("recycler itemAnimator is NOT running")
                    return@let
                }

                suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                    Timber.d("recycler itemAnimator is running ... adding a finish-listener")
                    it.isRunning(object : RecyclerView.ItemAnimator.ItemAnimatorFinishedListener {
                        override fun onAnimationsFinished() {
                            Timber.d("recycler itemAnimator ... onAnimationsFinished")
                            cancellableContinuation.resume(Unit)
                        }
                    })

                }
            }




            var shouldForceScrollToBottom = false
            val pos = layoutManager.findLastVisibleItemPosition()

            if (pos == -1 || pos + 1 > chatItemList.size) {
                //shouldForceScrollToBottom = true
            } else {
                val itemAdapterType = adapter.getItemOfPosition(pos)
                if (itemAdapterType is ATHeightDecorDynamic) {
                    shouldForceScrollToBottom = true
                }
            }



            adapter.setItems(chatItemList)
            Timber.d("adapter#setItems  shouldForceScrollToBottom:$shouldForceScrollToBottom")

            //if the content is scrolled to bottom of messages, then
            // await until the adapter has finished animating in the list-items
            // and make sure that the recycler is scrolled to bottom to insure
            // that the newly added messages are in view.
            if (shouldForceScrollToBottom) {
                viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                    delay(123)

                    recycler.itemAnimator?.let {
                        //making sure that the any previous animation get completed
                        if (!it.isRunning) {
                            Timber.d("recycler itemAnimator is NOT running")
                            return@let
                        }

                        suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                            Timber.d("recycler itemAnimator is running ... adding a finish-listener")
                            it.isRunning(object : RecyclerView.ItemAnimator.ItemAnimatorFinishedListener {
                                override fun onAnimationsFinished() {
                                    Timber.d("recycler itemAnimator ... onAnimationsFinished")
                                    cancellableContinuation.resume(Unit)
                                }
                            })

                        }
                    }

                    val lastCompletelyVisiblePos = layoutManager.findLastCompletelyVisibleItemPosition()
                    if (RecyclerView.NO_POSITION == lastCompletelyVisiblePos) {
                        Timber.d("findLastCompletelyVisibleItemPosition: NO_POSITION")
                        return@launchWhenStarted
                    }

                    if (chatItemList.size != lastCompletelyVisiblePos + 1) {
                        recycler.smoothScrollToPosition(chatItemList.size -1)
                    }

                }
            }




        }

        //if last-message-time of the chat is different from the actual last message then inform the serverside that the very last message has been seen
        //

        messageList.lastOrNull()?.let {
            if (it.millisCreatedAt == chatInfo.millisLastMessageAt && chatInfo.unreadCount > 0) {
                helper.onShouldInformSeen(chatInfo)
            }
        }




    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!wasCreatedOnce) return

        helper.cachedOutputState = layoutManager.onSaveInstanceState()
    }


    override fun onStop() {
        imm.hideSoftInputFromWindow(
                requireView().windowToken,
                0
        )

        super.onStop()
    }
}