package app.bitcoin.baemail.twetch.ui

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class TwetchAuthSigningAddressFragment : Fragment(R.layout.fragment_twetch_auth_signing_address) {

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TwetchViewModel

    private lateinit var container: ConstraintLayout
    private lateinit var coordinator: CoordinatorLayout

    private lateinit var addressValue: TextView
    private lateinit var messageValue: TextView
    private lateinit var signatureValue: TextView
    private lateinit var buttonRetry: Button


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)



        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]



        container = requireView().findViewById(R.id.container)
        coordinator = requireView().findViewById(R.id.coordinator)
        addressValue = requireView().findViewById(R.id.address_value)
        messageValue = requireView().findViewById(R.id.message_value)
        signatureValue = requireView().findViewById(R.id.signature_value)
        buttonRetry = requireView().findViewById(R.id.button_retry)







        addressValue.setOnClickListener {
            val address = viewModel.authInfo.value?.signingAddress?.address
                ?: return@setOnClickListener

            copyFieldValueToClipBoard("Address", address)
        }

        messageValue.setOnClickListener {
            val message = viewModel.authInfo.value?.signingAddress?.message
                ?: return@setOnClickListener

            copyFieldValueToClipBoard("Message", message)
        }

        signatureValue.setOnClickListener {
            val signature = viewModel.authInfo.value?.signingAddress?.signature
                ?: return@setOnClickListener

            copyFieldValueToClipBoard("Signature", signature)
        }




        buttonRetry.setOnClickListener {
            viewModel.retryAuth()
            findNavController().popBackStack()
        }









        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    isEnabled = false


                    requireActivity().finish()
                }

            })





        viewLifecycleOwner.lifecycleScope.launchWhenStarted {

            viewModel.authInfo.collect {
                val signingAddress = it?.signingAddress ?: return@collect

                addressValue.text = signingAddress.address
                messageValue.text = signingAddress.message
                signatureValue.text = signingAddress.signature
            }
        }


    }






    val cbm: ClipboardManager by lazy {
        requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }

    private fun copyFieldValueToClipBoard(field: String, value: String) {
        cbm.let { manager ->
            val clip = ClipData.newPlainText("$field value", value)

            manager.setPrimaryClip(clip)
            Snackbar.make(coordinator, "$field copied", Snackbar.LENGTH_SHORT).show()
            //Toast.makeText(requireContext(), "$field copied", Toast.LENGTH_SHORT).show()
        }
    }



}