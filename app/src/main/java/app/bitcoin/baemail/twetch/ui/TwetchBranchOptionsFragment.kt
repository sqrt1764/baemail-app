package app.bitcoin.baemail.twetch.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.util.RoundedBottomSheetDialogFragment
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class TwetchBranchOptionsFragment : RoundedBottomSheetDialogFragment() {

    companion object {
        const val KEY_POST_TX_ID = "key_post_tx_id"
        const val KEY_POST_PARENT_ID = "key_post_parent_id"
    }

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel



    private lateinit var branchAndLike: TextView
    private lateinit var branch: TextView
    private lateinit var quote: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_twetch_branch_options, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)


        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(TwetchUtilViewModel::class.java)


        branchAndLike = requireView().findViewById(R.id.branch_and_like)
        branch = requireView().findViewById(R.id.branch)
        quote = requireView().findViewById(R.id.quote)


        val postTx = requireArguments().getString(KEY_POST_TX_ID)!!
        val parentId = requireArguments().getString(KEY_POST_PARENT_ID)


        val colorSelector = R.color.selector_on_surface.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        branchAndLike.background = DrawableState.getNew(colorSelector)
        branch.background = DrawableState.getNew(colorSelector)
        quote.background = DrawableState.getNew(colorSelector)

        branchAndLike.setOnClickListener {
            viewModel.actionHelper.executeBranchAndLike(postTx, parentId)
            findNavController().popBackStack()
        }

        branch.setOnClickListener {
            viewModel.actionHelper.executeBranch(postTx, parentId)
            findNavController().popBackStack()
        }

        quote.setOnClickListener {
            findNavController().popBackStack()
            viewModel.viewModelScope.launch {
                delay(100)
                viewModel.composePostUseCase.prepareQuoteBranch(postTx)
            }

        }


    }

    override fun getPeekHeight(): Int {
        return CONST_UNSET
    }

    override fun shouldFitToContents(): Boolean {
        return true
    }
}