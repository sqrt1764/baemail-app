package app.bitcoin.baemail.twetch.ui

import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.threeten.bp.ZonedDateTime
import timber.log.Timber

class TwetchNotificationDetector(
    val coroutineUtil: CoroutineUtil,
    val nodeRuntimeRepository: NodeRuntimeRepository,
    val gson: Gson,
    val flowOfChatList: (key: TwetchChatInfoStore.ChatInfoKey)-> Flow<List<TwetchChatInfoStore.ChatInfo>>
) {

    private var _millisPolledLast = 0L
    val millisPolledLast: Long
        get() { return _millisPolledLast }

    private val _isRefreshingNow = MutableStateFlow(false)
    val isRefreshingNow: StateFlow<Boolean>
        get() = _isRefreshingNow

    fun start(
        key: TwetchChatInfoStore.ChatInfoKey,
        onAddedChatsDetected: suspend () -> Unit,
        onRemovedChatsDetected: suspend (List<String>) -> Unit,
        onNewMessagesDetected: suspend (List<TwetchChatInfoStore.ChatNewMessageCount>) -> Unit,
        onNotificationInfoUpdated: suspend (Int, Long) -> Unit,
        onError: suspend () -> Unit
    ): Job {
        return coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            try {
                val cachedInfo = mutableListOf<TwetchChatInfoStore.ChatActivityInfo>()

                launch(coroutineUtil.ioDispatcher) {
                    try {
                        //start observing the current activity-info
                        flowOfChatList(key).collect { chatList ->
                            chatList.map { chat ->
                                TwetchChatInfoStore.ChatActivityInfo(
                                    chat.id,
                                    chat.millisLastMessageAt,
                                    chat.unreadCount
                                )
                            }.let {
                                cachedInfo.clear()
                                cachedInfo.addAll(it)
                            }
                            Timber.d("`cachedInfo` updated")

                        }
                    } catch (e: Exception) {
                        Timber.d("cachedInfoRefreshJob got cancelled or crashed - $e")
                    }
                }

                //start polling
                while (true) {
                    delay(35 * 1000)

                    _millisPolledLast = System.currentTimeMillis()
                    _isRefreshingNow.value = true

                    Timber.d("polling for new twetch-chat-messages")
                    val latestInfo = fetchLatestChatActivityInfo()

                    if (latestInfo == null) {
                        //error fetching info from the server
                        onError()
                        continue
                    }

                    onNotificationInfoUpdated(
                        latestInfo.notificationCount,
                        latestInfo.millisLastReadNotifications
                    )

                    updatePersistedChatWithLatestActivityInfo(
                        key,
                        cachedInfo,
                        latestInfo.chatState,
                        onAddedChatsDetected,
                        onRemovedChatsDetected,
                        onNewMessagesDetected,
                        onError
                    )

                    _isRefreshingNow.value = false
                }

            } catch (e: Exception) {
                Timber.d("TwetchMessageDetector-job got cancelled or crashed - $e")
            }

        }
    }

    private suspend fun fetchLatestChatActivityInfo(): NotificationInfo? {
        val finalList = mutableListOf<TwetchChatInfoStore.ChatActivityInfo>()

        val limit = 30
        var offset = 0

        var notificationCount: Int = -1
        var millisLastReadNotifications: Long = -1

        for (i in 0 until 10) { //todo currently stops fetching after 10 pages of chats
            val result = nodeRuntimeRepository.runAsyncScript("""
                    
                    let millisStart = new Date().getTime();
                
                    await context.twetch.ensureAuthHasCompleted();
                    
                    let millisPostAuth = new Date().getTime();
                
                    let queryResult = await context.twetch.instance.query(`
                        {
                            allConversations(first: $limit, offset: $offset) {
                                edges {
                                    node {
                                        id
                                        lastMessageAt
                                        unreadCount
                                    }
                                }
                                pageInfo {
                                    hasNextPage
                                }
                                totalCount
                            }
                            ${
                                if (i == 0) {
                                    """
                                        me {
                                            notificationsCount
                                            lastReadNotifications
                                        }
                                    """.trimIndent()
                                } else ""
                            }
                        }
                    `);
                    
                    let millisPostRequest = new Date().getTime();
                    
                    
                    console.log("time spent awaiting twetch-auth:" + (millisPostAuth - millisStart) + "ms; time spent fetching 'lastMessageAt' of page of chats plus notification-count:" + (millisPostRequest - millisPostAuth) + "ms");
            
                    
                    return JSON.stringify(queryResult);
                """.trimIndent())

            if (result is RunScript.Response.Success) {

                if (i == 0) {
                    try {
                        val (count, millisLastRead) = parseNotificationCount(result.content)

                        notificationCount = count
                        millisLastReadNotifications = millisLastRead

                    } catch (e: Exception) {
                        Timber.e(e)
                    }
                }

                val (hasNextPage, activityInfoList) = try {
                    parseChatActivityInfoSuccessResponse(result.content)
                } catch (e: Exception) {
                    Timber.d(e)
                    throw e
                }

                finalList.addAll(activityInfoList)
                Timber.d("fetchLatestChatActivityInfo iteration completed; got info for ${activityInfoList.size} chats")

                if (hasNextPage) {
                    offset += limit
                } else {
                    break
                }


            } else {
                result as RunScript.Response.Fail
                return null //break
            }


        }


        return NotificationInfo(
            finalList,
            notificationCount,
            millisLastReadNotifications
        )
    }

    private fun parseNotificationCount(content: String): Pair<Int, Long> {
        val responseJson = gson.fromJson(content, JsonObject::class.java)
        val meJson = responseJson.getAsJsonObject("me")

        val count = meJson.get("notificationsCount").asString.toInt()
        val millisLastRead = if (meJson.has("lastReadNotifications")
            && !meJson.get("lastReadNotifications").isJsonNull
        ) {
            val lastRead = meJson.get("lastReadNotifications").asString
            try {
                ZonedDateTime.parse(lastRead).toInstant().toEpochMilli()
            } catch (e: Exception) {
                Timber.e(e)
                0L
            }
        } else 0L


        return count to millisLastRead
    }

    private fun parseChatActivityInfoSuccessResponse(
        content: String
    ): Pair<Boolean, List<TwetchChatInfoStore.ChatActivityInfo>> {
        val responseJson = gson.fromJson(content, JsonObject::class.java)
        val allConversationsJson = responseJson.getAsJsonObject("allConversations")
        val pageInfo = allConversationsJson.getAsJsonObject("pageInfo")
        val hasNextPage = pageInfo.get("hasNextPage").asBoolean

        val edges = allConversationsJson.getAsJsonArray("edges")

        val parsedList = mutableListOf<TwetchChatInfoStore.ChatActivityInfo>()

        edges.forEach { item ->
            val node = item.asJsonObject.getAsJsonObject("node")

            val id = node.get("id").asString
            val lastMessageAt = node.get("lastMessageAt").asString
            val millisLastMessageAt = lastMessageAt.let {
                try {
                    ZonedDateTime.parse(it).toInstant().toEpochMilli()
                } catch (e: Exception) {
                    Timber.e(e)
                    -1
                }
            }

            val unreadCount = node.get("unreadCount").asInt

            parsedList.add(
                TwetchChatInfoStore.ChatActivityInfo(
                    id,
                    millisLastMessageAt,
                    unreadCount
                )
            )

        }

        return hasNextPage to parsedList
    }

    private suspend fun updatePersistedChatWithLatestActivityInfo(
        key: TwetchChatInfoStore.ChatInfoKey,
        currentActivityInfo: List<TwetchChatInfoStore.ChatActivityInfo>,
        latestActivityInfo: List<TwetchChatInfoStore.ChatActivityInfo>,
        onAddedChatsDetected: suspend () -> Unit,
        onRemovedChatsDetected: suspend (List<String>) -> Unit,
        onNewMessagesDetected: suspend (List<TwetchChatInfoStore.ChatNewMessageCount>) -> Unit,
        onError: suspend () -> Unit
    ) {

        val addedChats = TwetchChatInfoStore.findAddedChats(
            latestActivityInfo,
            currentActivityInfo
        )

        val removedChats = TwetchChatInfoStore.findRemovedChats(
            latestActivityInfo,
            currentActivityInfo
        )

        val changeList = TwetchChatInfoStore.findFindNewMessages(
            latestActivityInfo,
            currentActivityInfo
        )

        if (removedChats.isNotEmpty()) {
            onRemovedChatsDetected(removedChats)
        }

        if (changeList.isNotEmpty()) {
            onNewMessagesDetected(changeList)
        }

        if (addedChats.isNotEmpty()) {
            onAddedChatsDetected()
        }

    }


    data class NotificationInfo(
        val chatState: List<TwetchChatInfoStore.ChatActivityInfo>,
        val notificationCount: Int,
        val millisLastReadNotifications: Long
    )

}