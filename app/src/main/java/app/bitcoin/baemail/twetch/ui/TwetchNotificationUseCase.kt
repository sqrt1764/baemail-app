package app.bitcoin.baemail.twetch.ui

import android.os.Parcelable
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.dropbox.android.external.store4.ResponseOrigin
import com.dropbox.android.external.store4.StoreResponse
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.threeten.bp.ZonedDateTime
import timber.log.Timber

class TwetchNotificationUseCase(
    private val activeUserId: String,
    private val coroutineUtil: CoroutineUtil,
    private val notificationInfoFlow: MutableStateFlow<TwetchRepository.NotificationInfo>,
    private val notificationsStore: TwetchNotificationsStore,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val gson: Gson
) {

    companion object {
        const val PAGE_NOT_LOADED = -1
    }

    private var isSetup = false

    private var page = PAGE_NOT_LOADED

    private val _content = MutableStateFlow(Model(
        emptyList(),
        PAGE_NOT_LOADED,
        true
    ))
    val content: StateFlow<Model>
        get() {
            ensureSetup()
            return _content
        }

    private val _metaState = MutableStateFlow(MetaState(
        true,
        false
    ))
    val metaState: StateFlow<MetaState>
        get() = _metaState


    private val _notifications = MutableStateFlow<StoreResponse<List<TwetchNotificationsStore.Notification>>>(
        StoreResponse.Loading(ResponseOrigin.Cache)
    )

    private var _completedPageFetch = MutableStateFlow(page)



    private var dataObservationJob: Job? = null
    private var enqueuedGetNextPageJob: Job? = null
    private var pagingJob: Job? = null



    var cachedListState: Parcelable? = null





    private fun ensureSetup() {
        if (isSetup) {
            return
        }
        isSetup = true


        dataObservationJob?.cancel()
        dataObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            var lastData = emptyList<TwetchNotificationsStore.Notification>()
            var lastRefreshing = true
            var lastError = false

            _metaState.value = _metaState.value.copy(
                refreshing = lastRefreshing
            )


            _notifications.collect { response ->
                when (response) {
                    is StoreResponse.Loading -> {
                        Timber.d("StoreResponse.Loading origin:${response.origin}")
                        lastRefreshing = true

                    }
                    is StoreResponse.Data<*> -> {
                        Timber.d("StoreResponse.Data origin:${response.origin}")
                        if (response.origin == ResponseOrigin.Fetcher) {
                            lastRefreshing = false
                            lastError = false
                        } else if (response.origin == ResponseOrigin.SourceOfTruth
                            || response.origin == ResponseOrigin.Cache) {
//                            lastRefreshingState = false
                        }

                        lastData = response.requireData()


                    }
                    is StoreResponse.Error -> {
                        Timber.d("StoreResponse.Error $response; ")
                        if (response.origin == ResponseOrigin.Fetcher) {
                            //no longer loading
                            lastRefreshing = false
                        }

                        if (response is StoreResponse.Error.Exception) {
                            Timber.e(response.error, "StoreResponse.Error.Exception")
                        }

                        //show error
                        lastError = true

                    }
                    is StoreResponse.NoNewData -> {
                        Timber.d("StoreResponse.NoNewData")
                        lastRefreshing = false
                    }
                }


                _content.value = _content.value.copy(
                    notifications = lastData,
                    page = page

                )

                _metaState.value = _metaState.value.copy(
                    refreshing = lastRefreshing,
                    error = lastError
                )

            }
        }
    }





    private fun checkHasMorePages(): Boolean {
        return notificationsStore.checkHasMorePages(
            activeUserId
        )
    }


    fun userRequestedNextPage() {
        Timber.d(">>>> userRequestedNextPage()")

        if (enqueuedGetNextPageJob != null) {
            Timber.d(">>>> userRequestedNextPage() dropped because a request is already enqueued")
            return
        }

        if (!checkHasMorePages()) {
            Timber.d(">>>> userRequestedNextPage() dropped because there are no more pages to fetch")
            return
        }


        enqueuedGetNextPageJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            Timber.d(">>>> userRequestedNextPage() the request is now enqueued")

            _completedPageFetch.collect { completedPage ->
                if (completedPage != page) {
                    Timber.d(">>>> message-page-load is in progress; page:$page completedPage:$completedPage")
                    return@collect
                }

                val didError = _metaState.value.error
                if (didError) {
                    Timber.d(">>>> not carrying out an enqueued request because there was an error")
                    return@collect
                }

                if (!checkHasMorePages()) {
                    //cleanup
                    Timber.d(">>>> not carrying out an enqueued request because there was are no more pages")
                    val thisJob = enqueuedGetNextPageJob
                    enqueuedGetNextPageJob = null
                    thisJob?.cancel()

                    return@collect
                }

                Timber.d(">>>>carrying out an enqueued request because the messages NOT refreshing")

                requestNextPage()

                //cleanup
                val thisJob = enqueuedGetNextPageJob
                enqueuedGetNextPageJob = null
                thisJob?.cancel()
            }
        }
    }


    private fun requestNextPage(forceFresh: Boolean = false) {
        if (!isSetup) throw RuntimeException()

        Timber.d("requestNextPage page:${page + 1}")

        pagingJob?.cancel()
        pagingJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            page += 1
            val currentPage = page

            val key = TwetchNotificationsStore.Key(
                activeUserId
            )
            key.page = currentPage
            key.forceRefresh = forceFresh

            //eagerly return the cached content
            if (currentPage == 0) {
                notificationsStore.getFromSourceOfTruth(key).let {
                    if (it.isEmpty()) return@let
                    _notifications.value = StoreResponse.Data(it, ResponseOrigin.SourceOfTruth)
                }
            }

            notificationsStore.getFlow(key).collect { response ->
                _notifications.value = response

                //update the tracking of fetch progress
                when (response) {
                    is StoreResponse.Loading -> { }
                    is StoreResponse.Data<*> -> {
                        if (response.origin == ResponseOrigin.Fetcher) {
                            _completedPageFetch.value = currentPage
                        }

                    }
                    is StoreResponse.Error -> {
                        if (response.origin == ResponseOrigin.Fetcher) {
                            _completedPageFetch.value = currentPage
                        }

                    }
                    is StoreResponse.NoNewData -> {
                        _completedPageFetch.value = currentPage
                    }
                }
            }


        }
    }


    fun onRefreshRequested(): Boolean {
        val currentMetaState = _metaState.value
        if (currentMetaState.refreshing) {
            Timber.d("refresh-request dropped; currently loading")
            return false
        }

        isSetup = false

        dataObservationJob?.cancel()

        pagingJob?.cancel()
        pagingJob = null

        enqueuedGetNextPageJob?.cancel()
        enqueuedGetNextPageJob = null

        _metaState.value = currentMetaState.copy(
            refreshing = false,
            error = false
        )

        _notifications.value = StoreResponse.Data(_content.value.notifications, ResponseOrigin.SourceOfTruth)

        page = PAGE_NOT_LOADED
        _completedPageFetch.value = page

        notificationsStore.clearFromMetaCache(activeUserId)

        ensureSetup()

        requestNextPage(true)

        return true
    }



    fun onRetryLastRequested() {
        val currentMetaState = _metaState.value
        if (currentMetaState.refreshing) {
            Timber.d("refresh-request dropped; currently loading")
            return
        }

        isSetup = false

        dataObservationJob?.cancel()

        pagingJob?.cancel()
        pagingJob = null

        enqueuedGetNextPageJob?.cancel()
        enqueuedGetNextPageJob = null

        _metaState.value = currentMetaState.copy(
            refreshing = false,
            error = false
        )

        _notifications.value = StoreResponse.Data(_content.value.notifications, ResponseOrigin.SourceOfTruth)

        if (PAGE_NOT_LOADED != page) {
            page -= 1
            _completedPageFetch.value = page
        }

        notificationsStore.clearErrorFromMetaCache(activeUserId)

        ensureSetup()

        requestNextPage(false)
    }


    //////////////////////////////////////////////////////////////////////////////////////////


    private var informSeenJob: Job? = null

    suspend fun onShouldInformSeen() = withContext(coroutineUtil.ioDispatcher) {
        val latestNotification = _content.value.notifications.firstOrNull() ?: return@withContext

        if (informSeenJob != null) {
            Timber.d("informing of notification-seen is in progress; returning")
            return@withContext
        }

        val info = notificationInfoFlow.value
        val hasLastSeenNotBeenInitialized = info.millisLastRead == -1L

        if (!hasLastSeenNotBeenInitialized) {
            if (info.millisLastRead >= latestNotification.millisCreatedAt) return@withContext
        }

        informSeenJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            Timber.d(">>> informing now")

            val result = nodeRuntimeRepository.runAsyncScript("""
                await context.twetch.ensureAuthHasCompleted();
                
                
                let dataPayload = {
                    "userId": "$activeUserId",
                    "payload": {
                        "lastReadNotifications": new Date().toJSON()
                    }
                }
                
                let result = await context.twetch.instance.query(`
                    mutation updateUser(${"$"}userId: BigInt!, ${"$"}payload: UserPatch!) {
                        updateUserById(input: {id: ${'$'}userId, userPatch: ${'$'}payload}) {
                            user {
                                lastReadNotifications
                            }
                        }
                    }
                `, dataPayload);
                
                return JSON.stringify(result);
            """.trimIndent())

            if (result is RunScript.Response.Success) {
                //Timber.d("notifications-seen successfully updated: $result")
                //parse
                val responseJson = gson.fromJson(result.content, JsonObject::class.java)
                val updateUserByIdJson = responseJson.getAsJsonObject("updateUserById")
                val userJson = updateUserByIdJson.getAsJsonObject("user")
                val timeLastRead = userJson.get("lastReadNotifications").asString

                val millisLastRead = timeLastRead.let {
                    try {
                        ZonedDateTime.parse(it).toInstant().toEpochMilli()
                    } catch (e: Exception) {
                        Timber.e(e)
                        -1
                    }
                }

                notificationInfoFlow.value = TwetchRepository.NotificationInfo(
                    0,
                    millisLastRead
                )
                Timber.d("notifications-seen successfully updated: $result")

            } else {
                result as RunScript.Response.Fail

                Timber.e(RuntimeException(), "notifications-seen not updated: $result")
            }

            informSeenJob = null


        }


    }






    //////////////////////////////////////////////////////////////////////////////////////////


    fun cleanup() {
        isSetup = false

        dataObservationJob?.cancel()
//        refreshingObservationJob?.cancel()



        enqueuedGetNextPageJob?.cancel()
        enqueuedGetNextPageJob = null

        pagingJob?.cancel()
        pagingJob = null

        page = PAGE_NOT_LOADED
        _completedPageFetch.value = page

        _notifications.value = StoreResponse.Loading(ResponseOrigin.Cache)

        _content.value = Model(
            emptyList(),
            PAGE_NOT_LOADED,
            true
        )

        _metaState.value = MetaState(
            true,
            false
        )
    }





    data class Model(
        val notifications: List<TwetchNotificationsStore.Notification>,
        val page: Int,
        val hasMorePages: Boolean
    )

    data class MetaState(
        val refreshing: Boolean,
        val error: Boolean
    )


}