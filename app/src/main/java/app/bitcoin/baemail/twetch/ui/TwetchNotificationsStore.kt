package app.bitcoin.baemail.twetch.ui

import android.content.Context
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.twetch.db.TwetchDatabase
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.dropbox.android.external.store4.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapLatest
import org.threeten.bp.ZonedDateTime
import timber.log.Timber

class TwetchNotificationsStore(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val twetchDatabase: TwetchDatabase,
    private val postCache: TwetchPostCache,
    private val gson: Gson,
    private val userCache: TwetchPostUserCache,
    private val notificationCache: TwetchNotificationsCache,
    private val twetchFilesMimeTypeHelper: TwetchFilesMimeTypeHelper
) {

    companion object {
        private const val PAGE_SIZE = 15
    }

    private val dummyNotificationOrderCache = HashMap<Key, MutableList<String>>()

    private val dummyPagingMetaCache = HashMap<Key, PagingMeta>()

    private val notificationsStore: Store<Key, List<Notification>> = StoreBuilder.from(
        fetcher = Fetcher.ofFlow { filterKey ->
            Timber.d("notificationsStore#fetcherOfFlow $filterKey")
            return@ofFlow queryServer(filterKey)
        },
        sourceOfTruth = SourceOfTruth.of(
            reader = ::queryByFilter,
            writer = ::insert,
            delete = ::clearByKey,
            deleteAll = ::deleteAll
        )

    ).build()










    private fun queryServer(key: Key): Flow<List<Notification>> {
        return flow {

            val page = key.page

            val cursor = if (page != 0) {
                dummyPagingMetaCache[key]?.endCursor
            } else null


            val finalCursor = if (cursor == null) {
                "null"
            } else {
                "\"$cursor\""
            }



            val result = nodeRuntimeRepository.runAsyncScript("""
                const postFetchingFunction = ${TwetchQueryUtil.printPostFetchingFunction()};
                const extractBranchTxIds = ${TwetchQueryUtil.printBranchTxExtractionFunction()};
                
                let millisStart = new Date().getTime();
            
                await context.twetch.ensureAuthHasCompleted();
                
                let millisPostAuth = new Date().getTime();
                
                
                let res = await context.twetch.instance.query(`
                    {
                        allNotifications(
                            after: $finalCursor
                            filter: {actorUserId: {notEqualTo: "${key.activeUserId}"}, userId: {equalTo: "${key.activeUserId}"}}
                            first: $PAGE_SIZE
                            orderBy: CREATED_AT_DESC
                        ) {
                            totalCount
                            pageInfo {
                                hasNextPage
                                endCursor
                            }
                            nodes {
                                id
                                actorUserId
                                price
                                priceSats
                                type
                                url
                                description
                                createdAt
                                userByActorUserId {
                                    nodeId
                                    icon
                                    id
                                    name
                                }
                                postByPostId {
                                    id
                                    bContent
                                    bContentType
                                    createdAt
                                    mapComment
                                    nodeId
                                    numBranches: branchCount
                                    numLikes: likeCount
                                    replyPostId
                                    transaction
                                    userId
                                    youBranched
                                    youLiked
                                    mapTwdata
                                    files
                                    status
                                    mediaByPostId {
                                        nodes {
                                            filename
                                        }
                                    }
                                    postsByReplyPostId {
                                        totalCount
                                    }
                                    parents {
                                        nodes {
                                            nodeId
                                            id
                                            transaction
                                            userByUserId {
                                                icon
                                                id
                                                name
                                                nodeId
                                            }
                                        }
                                    }
                                    userByUserId {
                                        createdAt
                                        icon
                                        id
                                        name
                                        nodeId
                                    }
                                }
                            }
                        }
                    }
                `);
                
                
                let millisPostFetch = new Date().getTime();
                
                
                //fetch posts referenced by branched-posts
                let branchNotificationList = [];
                let branchPostList = [];
                res.allNotifications.nodes.forEach((item) => {
                    if ("branch" == item.type && item.postByPostId.bContent.length == 85) {
                         //a branch & not a quote
                         branchNotificationList.push(item);
                         branchPostList.push(item.postByPostId);
                    }
                });
                let branchTxIdMap = extractBranchTxIds(branchPostList);
                let fetchedPosts = await postFetchingFunction(branchTxIdMap);
                
                
                let millisPostFetchOfRefPosts = new Date().getTime();
                
                
                //update the branch-posts of notification page
                branchNotificationList.forEach((item) => {
                    let branchedPost = fetchedPosts[item.postByPostId.transaction];
                    item.postByPostId = branchedPost;
                });
                
                
                
                console.log("time spent awaiting twetch-auth:" 
                    + (millisPostAuth - millisStart) 
                    + "ms; time spent on page of notifications:" 
                    + (millisPostFetch - millisPostAuth) 
                    + "ms; time spent on posts referenced by branches:"
                    + (millisPostFetchOfRefPosts - millisPostFetch) 
                    + "ms");
                
                
                return JSON.stringify(res);
            """.trimIndent())

            if (result is RunScript.Response.Success) {

                val (notificationPage, afterCursor) = parseSuccess(result.content, key.activeUserId)


                handleUserCachingAndRetrieval(notificationPage)

                dummyPagingMetaCache[key] = PagingMeta(
                    key.page,
                    afterCursor,
                    false
                )


                val orderingList = ensureOrderingList(key)
                if (page == 0) {
                    orderingList.clear()
                    Timber.d("orderingList cleared... key:$key")
                }
                notificationPage.map { it.id }.toCollection(orderingList)

                emit(notificationPage)


            } else {
                result as RunScript.Response.Fail

                dummyPagingMetaCache[key] = PagingMeta(
                    key.page,
                    null,
                    true
                )

                throw result.throwable

            }
        }
    }


    private fun ensureOrderingList(key: Key): MutableList<String> {
        return if (dummyNotificationOrderCache.containsKey(key)) {
            dummyNotificationOrderCache[key]!!
        } else {
            val list = mutableListOf<String>()
            dummyNotificationOrderCache[key] = list
            list
        }
    }


    private fun queryByFilter(key: Key): Flow<List<Notification>> {
        return notificationCache.updatedState.mapLatest {
            val ordered = dummyNotificationOrderCache[key]
                ?: return@mapLatest emptyList<Notification>()


            val notifications = ordered.mapNotNull { notificationId ->
                notificationCache.getNotification(key.activeUserId, notificationId)?.let { notification ->
                    notification.postTxId?.let { refPostTxId ->
                        postCache.getPost(key.activeUserId, refPostTxId)?.let { post ->
                            return@mapNotNull notification.copy(post = post)
                        }
                    }

                    notification
                }
            }

            notifications
        }
    }

    private fun insert(key: Key, results: List<Notification>) {
        val foundFilesRefs = ArrayList<String>()

        results.forEach { notification ->
            notificationCache.setNotification(key.activeUserId, notification)

            notification.post?.let { post ->
                postCache.setPost(key.activeUserId, post)

                if (post.files.isNotEmpty()) {
                    foundFilesRefs.addAll(post.files)
                }
            }

        }


        postCache.notifyUpdated()
        notificationCache.notifyUpdated()


        if (foundFilesRefs.isNotEmpty()) {
            twetchFilesMimeTypeHelper.checkFilesContent(foundFilesRefs)
        }
    }

    private suspend fun clearByKey(key: Key) {
        throw RuntimeException()
    }

    private suspend fun deleteAll() {
        throw RuntimeException()
    }

    ///////////////////////////////////////////////////////////////////////////

    private suspend fun handleUserCachingAndRetrieval(
        notificationPage: List<Notification>
    ) {
        try {
            val mentionedUsers = HashSet<String>()

            notificationPage.forEach {
                userCache.setUser(it.actor)
                it.post?.let { p ->
                    userCache.setUsersOfPost(p)
                    mentionedUsers.addAll(TwetchPostUserCache.findUserMentions(p.content))
                }
            }


            val missingPostUsers = userCache.findUsersMissingForIds(mentionedUsers)
            if (missingPostUsers.isEmpty()) return

            val foundUsers = TwetchPostUserCache.findUserForIds(
                nodeRuntimeRepository,
                gson,
                missingPostUsers
            )

            Timber.d("found: $foundUsers")

            foundUsers.forEach { user ->
                userCache.setUser(user)
            }


        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    ///////////////////////////////////////////////////////////////////////////

    private fun parseSuccess(
        content: String,
        activeUserId: String
    ): Pair<List<Notification>, String?> {
        val millisNow = System.currentTimeMillis()

        val responseJson = gson.fromJson(content, JsonObject::class.java)

        val allNotificationsJson = responseJson.getAsJsonObject("allNotifications")

        val totalCount = allNotificationsJson.get("totalCount").asInt

        val pageInfoJson = allNotificationsJson.getAsJsonObject("pageInfo")
        val hasNextPage = pageInfoJson.get("hasNextPage").asBoolean
        val endCursor = pageInfoJson.get("endCursor").asString



        val notificationsJson = allNotificationsJson.getAsJsonArray("nodes")

        val parsedList = notificationsJson.map { item ->

            val id = item.asJsonObject.get("id").asString
            val priceUsd = item.asJsonObject.get("price").asFloat
            val priceSats = if (item.asJsonObject.has("priceSats")
                && !item.asJsonObject.get("priceSats").isJsonNull) {
                item.asJsonObject.get("priceSats").asString.toInt()
            } else 0
            val type = item.asJsonObject.get("type").asString
            val description = item.asJsonObject.get("description").asString

            val createdAt = item.asJsonObject.get("createdAt").asString
            val millisCreatedAt = createdAt.let {
                try {
                    ZonedDateTime.parse(it).toInstant().toEpochMilli()
                } catch (e: Exception) {
                    Timber.e(e)
                    -1
                }
            }


            val actor = if (item.asJsonObject.has("userByActorUserId") &&
                !item.asJsonObject.get("userByActorUserId").isJsonNull
            ) {
                val userJson = item.asJsonObject.getAsJsonObject("userByActorUserId")
                val actorId = userJson.get("id").asString
                val actorName = userJson.get("name").asString
                val actorIcon = if (userJson.has("icon") && !userJson.get("icon").isJsonNull) {
                    userJson.get("icon").asString
                } else null

                PostUser(
                    actorId,
                    actorName,
                    actorIcon
                )
            } else {
                PostUser(
                    "-1",
                    "NULL",
                    null
                )
            }

            val post = if (item.asJsonObject.has("postByPostId")
                && !item.asJsonObject.get("postByPostId").isJsonNull) {

                PostModel.parsePost(
                    gson,
                    item.asJsonObject.getAsJsonObject("postByPostId"),
                    activeUserId,
                    millisNow
                )
            } else null

            Notification(
                id,
                priceUsd,
                priceSats,
                type,
                description,
                millisCreatedAt,
                actor,
                post?.txId,
                post
            )

        }


        return parsedList to endCursor
    }





    ///////////////////////////////////////////////////////////////////////////

    fun checkHasMorePages(
        activeUserId: String
    ): Boolean {
        val pagingMeta = dummyPagingMetaCache[Key(activeUserId)]

        pagingMeta ?: let {
            //nothing cached yet
            return true
        }

        return pagingMeta.endCursor != null && !pagingMeta.didError
    }

    fun clearErrorFromMetaCache(activeUserId: String) {
        val key = Key(activeUserId)
        val pagingMeta = dummyPagingMetaCache[key]

        pagingMeta ?: let {
            //nothing cached yet
            return
        }

        dummyPagingMetaCache[key] = pagingMeta.copy(
            didError = false,
            lastPage = pagingMeta.lastPage - 1
        )
    }

    fun clearFromMetaCache(activeUserId: String) {
        dummyPagingMetaCache.remove(Key(activeUserId))
    }


    ///////////////////////////////////////////////////////////////////////////


    suspend fun getFromSourceOfTruth(key: Key): List<Notification> {
        return queryByFilter(key).first()
    }

    fun getFlow(key: Key): Flow<StoreResponse<List<Notification>>> {
        return notificationsStore.stream(StoreRequest.cached(key, true))
    }


    ///////////////////////////////////////////////////////////////////////////

















    data class Key(
        val activeUserId: String
    ) {
        var page = 0
        var forceRefresh = false

        override fun toString(): String {
            return "Key(activeUserId='$activeUserId', page=$page)"
        }

    }

    data class PagingMeta(
        val lastPage: Int,
        val endCursor: String?,
        val didError: Boolean
    )





    data class Notification(
        val id: String,
        val priceUsd: Float,
        val priceSats: Int,
        val type: String,
        val description: String,
        val millisCreatedAt: Long,
        val actor: PostUser,
        val postTxId: String?,
        val post: PostModel?
    )

}