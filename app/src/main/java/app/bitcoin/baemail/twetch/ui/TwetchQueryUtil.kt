package app.bitcoin.baemail.twetch.ui

object TwetchQueryUtil {

    fun printPostFetchingFunction(): String {
        return """
            async function (referencedPostMap) {
                let txIdArray = Object.values(referencedPostMap);
                let txIdJson = JSON.stringify(txIdArray);
                
                if (txIdArray.length == 0) {
                    return {};
                }
                
                let res = await context.twetch.instance.query(`
                    {
                        allPosts(filter: {transaction: {in: ${"$"}{txIdJson} }}) {
                            edges {
                                node {
                                    bContent
                                    bContentType
                                    createdAt
                                    id
                                    mapComment
                                    nodeId
                                    numBranches: branchCount
                                    numLikes: likeCount
                                    replyPostId
                                    transaction
                                    userId
                                    youBranched
                                    youLiked
                                    mapTwdata
                                    files
                                    mediaByPostId {
                                        nodes {
                                            filename
                                        }
                                    }
                                    postsByReplyPostId {
                                        totalCount
                                    }
                                    parents {
                                        nodes {
                                            nodeId
                                            id
                                            transaction
                                            userByUserId {
                                                icon
                                                id
                                                name
                                                nodeId
                                            }
                                        }
                                    }
                                    userByUserId {
                                        createdAt
                                        icon
                                        id
                                        name
                                        nodeId
                                    }
                                }
                            }
                        }
                    }
                `);
                
                let postIds = Object.keys(referencedPostMap);
                const finalMap = {};
                
                
                for (var post of res.allPosts.edges) {
                    postIds.find((id) => {
                        let referencedId = referencedPostMap[id];
                        if (referencedId == post.node.transaction) {
                            finalMap[id] = post.node;
                            return true;
                        }
                        return false;
                    });
                }
                
                return finalMap;
            }
        """.trimIndent()
    }

    fun printBranchTxExtractionFunction(): String {
        return """
            function (postArray) {
                let referencedPostsMap = {};
                
                postArray.forEach((item) => {
                    var bContent = null;
                    if (item != null) {
                        bContent = item.bContent;
                    }
                    
                    if (bContent != null) {
                        let m = bContent.matchAll(new RegExp("(https://twetch.(app|com)/t/[a-zA-Z_0-9]+)"));
                        for (var mm of m) {
                            let txId = mm[1].replace("https://twetch.app/t/", "")
                                .replace("https://twetch.com/t/", "");
                            referencedPostsMap[item.transaction] = txId;
                        }
                        
                    }
                });
                
                return referencedPostsMap;
            }
        """.trimIndent()
    }
}