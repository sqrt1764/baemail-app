package app.bitcoin.baemail.twetch.ui

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import com.github.chrisbanes.photoview.PhotoView
import dagger.android.support.AndroidSupportInjection
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import app.bitcoin.baemail.core.presentation.drawable.DrawableExpandingTopStroke
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import coil.imageLoader
import coil.load
import coil.request.ImageRequest
import com.github.chrisbanes.photoview.OnOutsidePhotoTapListener
import com.github.chrisbanes.photoview.OnViewDragListener
import timber.log.Timber
import java.io.*
import javax.inject.Inject
import android.graphics.Bitmap
import android.graphics.Canvas

import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ProgressBar
import androidx.browser.customtabs.CustomTabsIntent
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import kotlinx.coroutines.*


class TwetchChatFullImageFragment : AppCompatDialogFragment() {

    companion object {
        const val KEY_IMAGE_FILE_PATH = "path"
        const val KEY_IMAGE_URL = "url"
        const val CREATE_FILE = 1

        private const val IS_DEBUG = false


        fun drawableToBitmap(drawable: Drawable): Bitmap {
            if (drawable is BitmapDrawable) {
                if (drawable.bitmap != null) {
                    return drawable.bitmap
                }
            }
            val bitmap = if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
                Bitmap.createBitmap(
                    1,
                    1,
                    Bitmap.Config.ARGB_8888
                ) // Single color bitmap will be created of 1x1 pixel
            } else {
                Bitmap.createBitmap(
                    drawable.intrinsicWidth,
                    drawable.intrinsicHeight,
                    Bitmap.Config.ARGB_8888
                )
            }
            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            return bitmap
        }

    }

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var coroutineUtil: CoroutineUtil

    private lateinit var viewModel: TwetchViewModel

    private lateinit var loading: ProgressBar
    private lateinit var container: TouchingConstraintLayout
    private lateinit var zoomImageView: PhotoView
    private lateinit var buttonBack: Button
    private lateinit var buttonSave: Button

    private lateinit var buttonOpenInBrowser: Button
    private lateinit var buttonPrev: Button
    private lateinit var buttonNext: Button


    private var currentImageUrlPos = 0
    private var imageUrl: List<String>? = null
    private var imageUrlDrawable: BitmapDrawable? = null


    private var filePath: String? = null
    private var imageFile: File? = null



    private val drawableSwipeDismissIndicator = DrawableExpandingTopStroke()

    private val minDismissDragDist: Float by lazy {
        resources.displayMetrics.density * 120
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        super.onActivityResult(requestCode, resultCode, resultData)
        if (requestCode == CREATE_FILE && resultCode == Activity.RESULT_OK) {
            // The result data contains a URI for the document or directory that
            // the user selected.
            resultData?.data?.also { uri ->
                imageFile?.let { file ->
                    viewModel.copyFileData(file, uri)
                } ?: let {
                    viewModel.copyBitmapData(imageUrlDrawable!!.bitmap, uri)
                }

            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_twetch_chat_full_image, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return FullScreenDialog(requireContext())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]


        loading = requireView().findViewById(R.id.loading)
        container = requireView().findViewById(R.id.container)
        zoomImageView = requireView().findViewById(R.id.zoom_image_view)
        buttonBack = requireView().findViewById(R.id.back)
        buttonSave = requireView().findViewById(R.id.action_save)

        buttonOpenInBrowser = requireView().findViewById(R.id.action_open_in_browser)
        buttonPrev = requireView().findViewById(R.id.action_prev)
        buttonNext = requireView().findViewById(R.id.action_next)

        val urlList = requireArguments().getStringArrayList(KEY_IMAGE_URL)
        imageUrl = urlList
        filePath = requireArguments().getString(KEY_IMAGE_FILE_PATH)

        if (imageUrl == null && filePath == null) {
            Timber.d("dismissing now")
            dismissFragment()
            return
        }

        if (urlList == null) {
            buttonOpenInBrowser.visibility = View.INVISIBLE
            buttonPrev.visibility = View.INVISIBLE
            buttonNext.visibility = View.INVISIBLE

        } else {
            buttonOpenInBrowser.visibility = View.VISIBLE
        }

        initPrevNextButtonVisibility()











        filePath?.let {
            val file = File(it)
            imageFile = file

            if (!file.isFile) {
                dismissFragment()
                return
            }
        }



        zoomImageView.setOnOutsidePhotoTapListener(object : OnOutsidePhotoTapListener {
            override fun onOutsidePhotoTap(imageView: ImageView?) {
                dismissFragment()
            }
        })


        buttonBack.setOnClickListener {
            dismissFragment()
        }

        buttonSave.setOnClickListener {
            openCreateFileWizard()
        }

        setupDragToDismiss()




        buttonOpenInBrowser.setOnClickListener {
            val url = imageUrl!![currentImageUrlPos]
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(requireActivity(), Uri.parse(url))
        }

        buttonPrev.setOnClickListener {
            currentImageUrlPos -= 1
            initPrevNextButtonVisibility()
            showImageFromUrl(imageUrl!![currentImageUrlPos])
        }

        buttonNext.setOnClickListener {
            currentImageUrlPos += 1
            initPrevNextButtonVisibility()
            showImageFromUrl(imageUrl!![currentImageUrlPos])
        }














        imageFile?.let {
            //set image from file
            zoomImageView.load(it)

        } ?: let {
            //set image from url
            val imageUrl = imageUrl!![currentImageUrlPos]

            showImageFromUrl(imageUrl)


        }


    }

    private fun showImageFromUrl(url: String) {
        viewLifecycleOwner.lifecycleScope.launch {
            Timber.d("imageUrl: $url")

            zoomImageView.visibility = View.INVISIBLE
            loading.visibility = View.VISIBLE

            val request = ImageRequest.Builder(requireContext())
                .data(url)
                .build()

            val result = requireContext().imageLoader.execute(request)

            result.drawable?.let {
                if (it is BitmapDrawable) {
                    imageUrlDrawable = it
                } else {
                    withContext(coroutineUtil.ioDispatcher) {
                        val bitmapDrawable = BitmapDrawable(resources, drawableToBitmap(it))
                        imageUrlDrawable = bitmapDrawable
                    }
                }

                zoomImageView.load(it)

                zoomImageView.visibility = View.VISIBLE
                loading.visibility = View.INVISIBLE

            } ?: dismissFragment()
        }
    }

    private fun initPrevNextButtonVisibility() {
        val urlList = imageUrl

        if (urlList == null) {
            buttonPrev.visibility = View.INVISIBLE
            buttonNext.visibility = View.INVISIBLE

        } else if (urlList.size == 0) {
            throw RuntimeException()

        } else if (urlList.size == 1) {
            buttonPrev.visibility = View.INVISIBLE
            buttonNext.visibility = View.INVISIBLE

        } else {
            if (currentImageUrlPos == 0) {
                buttonPrev.visibility = View.INVISIBLE
            } else {
                buttonPrev.visibility = View.VISIBLE
            }
            if (currentImageUrlPos + 1 == urlList.size) {
                buttonNext.visibility = View.INVISIBLE
            } else {
                buttonNext.visibility = View.VISIBLE
            }

        }
    }

    private fun dismissFragment() {
        drawableSwipeDismissIndicator.setPercentExpanded(0f)
        buttonBack.visibility = View.INVISIBLE
        buttonSave.visibility = View.INVISIBLE
        buttonOpenInBrowser.visibility = View.INVISIBLE
        buttonPrev.visibility = View.INVISIBLE
        buttonNext.visibility = View.INVISIBLE
        requireView().post {
            findNavController().popBackStack()
        }

    }

    private fun setupDragToDismiss() {
        container.foreground = drawableSwipeDismissIndicator
        drawableSwipeDismissIndicator.setStrokeColor(requireContext().getColorFromAttr(R.attr.colorSecondary))
        drawableSwipeDismissIndicator.setStrokeWidths((5 * resources.displayMetrics.density).toInt())
        drawableSwipeDismissIndicator.alpha = 255



        zoomImageView.setAllowParentInterceptOnEdge(true)

        var dragDist = 0f
        var didGestureComplete = false

        val resetDrag = {
            //todo animate reset
            dragDist = 0f
            drawableSwipeDismissIndicator.setPercentExpanded(0f)
        }

        zoomImageView.setOnViewDragListener(object : OnViewDragListener {

            val dp = resources.displayMetrics.density

            override fun onDrag(dx: Float, dy: Float) {
                val r = zoomImageView.displayRect

                if (IS_DEBUG) Timber.d("onDrag dx:$dx dy:$dy; displayRect:$r")

                if (didGestureComplete) return
                if (dy < 0 && dragDist <= 0) return
                if (r.top < -2 * dp) { //the minus two multiplier makes it less sensitive
                    resetDrag()
                    return
                }

                dragDist += dy
                drawableSwipeDismissIndicator.setPercentExpanded(dragDist / minDismissDragDist)

                if (dragDist > minDismissDragDist) {
                    didGestureComplete = true

                    //dismiss
                    dismissFragment()

                }

            }

        })


        container.onTouchReleased = lambda@{
            if (didGestureComplete) {
                if (IS_DEBUG) Timber.d("onReleased dropped didGestureComplete==TRUE")
                return@lambda
            }

            if (IS_DEBUG) Timber.d("onReleased ... scroll back")
            resetDrag()
        }

    }

    private fun openCreateFileWizard() {
        val intent = imageFile?.let { file ->
            Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "image/${file.extension}"

                putExtra(Intent.EXTRA_TITLE, file.name)
            }
        } ?: let {
            val url = imageUrl!![currentImageUrlPos]
            val pos = url.lastIndexOf("/")
            val fileName = url.substring(pos + 1) + ".jpeg"

            Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "image/jpeg"

                putExtra(Intent.EXTRA_TITLE, fileName)
            }
        }

        startActivityForResult(intent, CREATE_FILE)
    }


}

class FullScreenDialog(context: Context) : AppCompatDialog(context, R.style.FullScreenDialog) {

    init {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        setCanceledOnTouchOutside(true)
    }
}