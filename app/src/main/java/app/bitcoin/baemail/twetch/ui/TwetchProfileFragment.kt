package app.bitcoin.baemail.twetch.ui

import android.content.Intent
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.widget.TextViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.bitcoin.baemail.R
import app.bitcoin.baemail.fundingWallet.core.presentation.CoinActivity
import app.bitcoin.baemail.fundingWallet.TopUpActivity
import app.bitcoin.baemail.core.presentation.view.recycler.holder.ATTwetchTabsViewHolder
import app.bitcoin.baemail.core.presentation.view.recycler.holder.TabItem
import app.bitcoin.baemail.twetch.db.entity.UserProfileBaseModel
import app.bitcoin.baemail.core.presentation.util.RelativeTimeHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ViewCoinManagementHelper
import app.bitcoin.baemail.core.presentation.view.recycler.ATContentLoading0
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchNoPostsFoundItem
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchTabsItem
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchUserProfileRootItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.mapLatest
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.math.min

class TwetchProfileFragment : Fragment(R.layout.fragment_twetch_profile) {

    companion object {
        const val TAB_ADAPTER_POS = 1
    }

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel

    private var helper: TwetchUserProfileUseCase? = null

    private val postItemHelper: TwetchPostItemHelper by lazy {
        TwetchPostItemHelper(
            requireContext(),
            viewModel.postUserCache,
            TextViewCompat.getTextMetricsParams(dummyBodyText)
        )
    }


    private lateinit var container: CoordinatorLayout
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var recycler: RecyclerView
    private lateinit var scrollToTop: TextView
    private lateinit var coinManagementHelper: ViewCoinManagementHelper

    private lateinit var dummyBodyText: TextView


    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter

    private var wasCreatedOnce = false

    private var lastRestoredScrollStateTab: TwetchUserProfileUseCase.UserProfileTab? = null

    private var isProfileUseCaseSetup = false
    private var hasPostListBeenApplied = false

    private val noPostsFoundItem = ATTwetchNoPostsFoundItem("0")

    private val altLoadingItemLD = MutableLiveData<Int>()
    private val altLoadingItem = ATHeightDecorDynamic("1", altLoadingItemLD)


    val colorPrimary: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }


    val colorDanger: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorDanger)
    }




    private lateinit var lastProfileRootItem: ATTwetchUserProfileRootItem
    private lateinit var tabsItem: ATTwetchTabsItem

    private var lastPostItemList = listOf<AdapterType>()





    private lateinit var tabItemLatest: TabItem


    private lateinit var tabItemReply: TabItem
    private lateinit var tabItemLikes: TabItem

    private var lastTabsList = emptyList<TabItem>()

    var hasScrollUpBeenInitialized = false


    private lateinit var floatingTabHolder: ATTwetchTabsViewHolder
    private val floatingTabsListener = FloatingTabsListener()


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
                .get(TwetchUtilViewModel::class.java)



        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true


        dummyBodyText = requireView().findViewById(R.id.dummy_body_text)

        container = requireView().findViewById(R.id.wrapping_coordinator)
        swipeRefresh = requireView().findViewById(R.id.swipe_refresh)
        recycler = requireView().findViewById(R.id.list)
        scrollToTop = requireView().findViewById(R.id.scroll_to_top)
        coinManagementHelper = requireView().findViewById(R.id.coin_management_helper)

        val dp = resources.displayMetrics.density

        altLoadingItemLD.value = (108 * dp).toInt()

        floatingTabHolder = ATTwetchTabsViewHolder.create(
            null,
            LayoutInflater.from(requireContext()),
            container
        )
        container.addView(floatingTabHolder.itemView)
        floatingTabHolder.itemView.visibility = View.INVISIBLE

        let {
            val elevatedHeight = resources.getDimension(R.dimen.inner_topbar_floating_elevation) / dp
            val background = MaterialShapeDrawable.createWithElevationOverlay(requireContext(), elevatedHeight)
            floatingTabHolder.itemView.background = background
            floatingTabHolder.itemView.elevation = elevatedHeight * dp
        }


        val colorPrimary = requireContext().getColorFromAttr(R.attr.colorPrimary)
        val colorTransparentPrimary = Color.argb(
            200,
            Color.red(colorPrimary),
            Color.green(colorPrimary),
            Color.blue(colorPrimary)
        )



        swipeRefresh.setProgressViewOffset(true, (5 * dp).toInt(), (120 * dp).toInt())
        swipeRefresh.setOnRefreshListener {
            helper?.onRefreshRequested() ?: let {
                requireView().postDelayed({
                    swipeRefresh.isRefreshing = false
                }, 100)
            }
        }
        swipeRefresh.setColorSchemeColors(requireContext().getColorFromAttr(R.attr.colorOnPrimary))
        swipeRefresh.setProgressBackgroundColorSchemeColor(colorTransparentPrimary)







        scrollToTop.clipToOutline = true
        scrollToTop.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, p0.height / 2f)
            }
        }
        scrollToTop.background = ColorDrawable(colorTransparentPrimary)

        scrollToTop.setOnClickListener {
            recycler.smoothScrollToPosition(0)
            scrollToTop.visibility = View.INVISIBLE
        }









        tabItemLatest = TabItem(
            "latest",
            getString(R.string.latest),
            true,
        )

        tabItemReply = TabItem(
            "reply",
            getString(R.string.reply),
            false,
        )

        tabItemLikes = TabItem(
            "likes",
            getString(R.string.likes),
            false,
        )

        lastTabsList = listOf(
            tabItemLatest,
            tabItemReply,
            tabItemLikes
        )



        val contentLoadingListener = object : BaseListAdapter.ATContentLoadingListener {
            override fun onBound() {
                val helper = helper ?: return

                when (helper.content.value.activeTab) {
                    TwetchUserProfileUseCase.UserProfileTab.LATEST -> {
                        if (helper.metaState.value.latestRefreshing) {
                            return
                        }
                    }
                    TwetchUserProfileUseCase.UserProfileTab.REPLIES -> {
                        if (helper.metaState.value.repliesRefreshing) {
                            return
                        }

                    }
                    TwetchUserProfileUseCase.UserProfileTab.LIKES -> {
                        if (helper.metaState.value.likesRefreshing) {
                            return
                        }

                    }
                    else -> {
                        //do nothing
                    }
                }


                swapOutRefreshingItem()
            }
        }

        val profileBaseListener = object : BaseListAdapter.ATTwetchUserProfileBaseListener {
            override fun onUserIconClicked(url: String) {
                val args = Bundle().also {
                    val list = ArrayList<String>()
                    list.add(url)
                    it.putStringArrayList(TwetchChatFullImageFragment.KEY_IMAGE_URL, list)

                }
                findNavController().navigate(R.id.action_twetchProfileFragment2_to_twetchChatFullImageFragment2, args)
            }

            override fun onFollowUserClicked(model: ATTwetchUserProfileRootItem): Boolean {
                return viewModel.actionHelper.executeFollowOfUser(model.profileBase.userId)
            }

            override fun onUnfollowUserClicked(model: ATTwetchUserProfileRootItem): Boolean {
                return viewModel.actionHelper.executeUnfollowOfUser(model.profileBase.userId)
            }

        }




        val tabsListener = object : BaseListAdapter.ATTwetchTabsListener {
            override fun onBound(id: String): List<TabItem> {
                return lastTabsList
            }

            override fun onTabClicked(id: String, tab: TabItem): List<TabItem> {
                if (tab.active) {
                    recycler.smoothScrollToPosition(0)
                    return lastTabsList
                } else {
                    recycler.stopScroll()
                    cacheCurrentScrollState()
                }

                refreshTabsItemList(tab.id)

                floatingTabsListener.onInlineTabClicked()

                dismissScrollToTop()


                return lastTabsList
            }

            override fun onAttached(holder: RecyclerView.ViewHolder) {
                floatingTabsListener.onInlineTabsAttached(holder)
            }

            override fun onDetached() {
                floatingTabsListener.onInlineTabsDetached()
            }

        }

        val postListener = TwetchPostListener(
            requireContext(),
            findNavController(),
            viewModel.actionHelper,
            viewModel.videoClipManager,
            R.id.action_global_twetchUserProfile,
            R.id.action_twetchProfileFragment2_to_twetchChatFullImageFragment2,
            R.id.action_global_twetchPostDetails,
            R.id.action_twetchProfileFragment2_to_twetchBranchOptionsFragment4,
            R.id.action_twetchProfileFragment2_to_twetchVideoClipFragment,
            viewModel.coinFlipSignalHelper.coinFlipChannel
        )

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atContentLoadingListener = contentLoadingListener,
                atTwetchUserProfileBaseListener = profileBaseListener,
                atTwetchTabsListener = tabsListener,
                atTwetchPostListener = postListener
            )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)


        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER


        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var lastShowShadow: Boolean? = null

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                recyclerView.canScrollVertically(-1).let { showShadow ->
                    if (lastShowShadow == showShadow) return@let
                    lastShowShadow = showShadow

                    if (showShadow) {
                        utilViewModel.onMainContentScrollChanged(false)
                    } else {
                        utilViewModel.onMainContentScrollChanged(true)
                    }
                }

                floatingTabsListener.onRecyclerScrolled(dy)

                if (!hasScrollUpBeenInitialized) {
                    refreshShowingOfScrollToTop()
                }

            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (RecyclerView.SCROLL_STATE_IDLE == newState) {
                    cacheCurrentScrollState()

                    refreshShowingOfScrollToTop()

                }
            }
        })





        viewLifecycleOwner.lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onStart(owner: LifecycleOwner) {
                floatingTabHolder.onAppear()
            }

            override fun onStop(owner: LifecycleOwner) {
                floatingTabHolder.onDisappear()
            }

            override fun onDestroy(owner: LifecycleOwner) {
                floatingTabHolder.onRecycled()
            }
        })




        coinManagementHelper.clickTopUpListener = {
            startActivity(Intent(requireActivity(), TopUpActivity::class.java))
        }

        coinManagementHelper.clickSplitCoinsListener = {
            startActivity(Intent(requireActivity(), CoinActivity::class.java))
        }

        coinManagementHelper.clickRetryReconnectListener = {
            viewModel.chainSync.considerRetry()
        }






        isProfileUseCaseSetup = false



        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            utilViewModel.mainContentTopDistance.collect {
                refreshDynamicBottomHeight()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.coinManagementUseCase.state.mapLatest { it }
                .combine(utilViewModel.snackBarShowingState) { coinModel, snackBarReservation ->
                    coinModel to snackBarReservation
                }.collectLatest { (coinModel, snackBarReservation) ->
                    if (!snackBarReservation) {
                        coinManagementHelper.applyModel(coinModel)
                    } else {
                        //apply a model that will result in this bar being hidden
                        coinManagementHelper.applyModel(ViewCoinManagementHelper.HELPER_HIDDEN)
                    }
                }
        }



        viewLifecycleOwner.lifecycleScope.launchWhenStarted {

            viewModel.authInfo.collect { info ->

                // update the content
                info?.profile?.let { profile ->



                    //awaitRecyclerAnimationsFinish()
                    initProfileItem(profile)



                    if (!viewModel.checkMyProfileContentUseCaseInitialized()) {
                        setItemsOnAdapter()
                    }


                    ensureProfileUseCaseSetup()
                }
            }
        }



    }

    private fun initProfileItem(profile: TwetchAuthStore.TwetchProfile) {
        val meProfileBase = UserProfileBaseModel(
            profile.id.toString(),
            profile.id.toString(),
            profile.followerCount,
            profile.followingCount,
            profile.icon,
            profile.name,
            profile.publicKey,
            profile.description,
            profile.dmConversationId,
            true,
            true,
            -1
        )

        if (::lastProfileRootItem.isInitialized
            && lastProfileRootItem.profileBase == meProfileBase
        ) {
            return
        }
        lastProfileRootItem = ATTwetchUserProfileRootItem(
            "0",
            meProfileBase,
            false
        )
    }



    fun refreshShowingOfScrollToTop() {
        hasScrollUpBeenInitialized = true

        requireView().post {
            if (layoutManager.findFirstVisibleItemPosition() > 10) {
                if (scrollToTop.visibility != View.VISIBLE) {
                    scrollToTop.visibility = View.VISIBLE
                }
            } else {
                if (scrollToTop.visibility != View.INVISIBLE) {
                    scrollToTop.visibility = View.INVISIBLE
                }
            }
        }
    }

    fun dismissScrollToTop() {
        hasScrollUpBeenInitialized = false

        if (scrollToTop.visibility != View.INVISIBLE) {
            scrollToTop.visibility = View.INVISIBLE
        }
    }



    private fun setItemsOnAdapter() {
        val adapterList = ArrayList<AdapterType>(lastPostItemList)
        adapterList.add(0, lastProfileRootItem)

        adapter.setItems(adapterList)
    }




    private fun ensureProfileUseCaseSetup() {
        if (isProfileUseCaseSetup) return
        isProfileUseCaseSetup = true


        val helper = viewModel.getMyProfileContent()
        this.helper = helper






        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            helper.metaState.collectLatest {
                refreshLoadingIndicatorState()
            }
        }



        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            try {
                helper.content.collectLatest { model ->

                    when (model.activeTab) {
                        TwetchUserProfileUseCase.UserProfileTab.PROFILE -> {
                            throw RuntimeException()
                        }
                        TwetchUserProfileUseCase.UserProfileTab.LATEST -> {
                            initListForTab(model.latest, model.activeTab)
                        }
                        TwetchUserProfileUseCase.UserProfileTab.REPLIES -> {
                            initListForTab(model.replies, model.activeTab)
                        }
                        TwetchUserProfileUseCase.UserProfileTab.LIKES -> {
                            initListForTab(model.likes, model.activeTab)
                        }
                    }

                }
            } finally {
                hasPostListBeenApplied = false
            }
        }
    }


    private var lastAppliedList: TwetchPostModel? = null
    private var lastAppliedTab: TwetchUserProfileUseCase.UserProfileTab? = null


    private suspend fun initListForTab(postModel: TwetchPostModel?, tab: TwetchUserProfileUseCase.UserProfileTab) {
        if (lastAppliedList == postModel && lastAppliedTab == tab) {
            Timber.d("post-list is unchanged; returning")
            return
        }
        lastAppliedList = postModel
        lastAppliedTab = tab


        awaitRecyclerAnimationsFinish()


        if (TwetchUserProfileUseCase.UserProfileTab.PROFILE == tab) {
            throw RuntimeException()
        }

        val helper = helper ?: throw RuntimeException()



        if (!hasPostListBeenApplied) {
            hasPostListBeenApplied = true

            Timber.d(">>>>>>>>>>>>>>>>>>>>>>> ATTwetchTabsItem recreated")
            tabsItem = ATTwetchTabsItem(System.currentTimeMillis().toString())

            when (tab) {
                TwetchUserProfileUseCase.UserProfileTab.LATEST -> refreshTabsItemList(tabItemLatest.id)
                TwetchUserProfileUseCase.UserProfileTab.REPLIES -> refreshTabsItemList(tabItemReply.id)
                TwetchUserProfileUseCase.UserProfileTab.LIKES -> refreshTabsItemList(tabItemLikes.id)
                else -> throw RuntimeException()
            }

            //initialize the floating tabs
            floatingTabHolder.onBindViewHolder(tabsItem, floatingTabsListener)


            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                delay(200)
                //todo this is hacky
                refreshShowingOfScrollToTop()
            }


        }


        if (postModel == null) {
            Timber.d("model.list == null")
            lastPostItemList = emptyList()
            setItemsOnAdapter()

            lastRestoredScrollStateTab = tab

        } else {

            val isRefreshing = isRefreshing()
            val loadingItem = if (!isRefreshing && postModel.hasMorePages) {
                ATContentLoading0(postModel.page)
            } else {
                altLoadingItem
            }

            if (TwetchPostListHelper.PAGE_NOT_LOADED == postModel.page) {
                Timber.d("model.list == PAGE_NOT_LOADED")
                helper.requestNextPage()
            }

            if (postModel.list.isEmpty()) {
                val itemList = mutableListOf<AdapterType>()
                itemList.add(tabsItem)

                if (postModel.hasMorePages) {
                    itemList.add(loadingItem)
                } else {
                    itemList.add(noPostsFoundItem)
                }


                lastPostItemList = itemList
                setItemsOnAdapter()

                lastRestoredScrollStateTab = tab

            } else {
                val itemList = mutableListOf<AdapterType>()
                itemList.add(tabsItem)

                withContext(Dispatchers.Default) {
                    val relativeTimeHelper = RelativeTimeHelper(
                        requireContext(),
                        System.currentTimeMillis()
                    )

                    postModel.list.map {

//                        val refPostTxId = it.post.referencedPost
//                        val refPost = refPostTxId?.let { txId ->
//                            it.referenced[txId]
//                        }
//
//                        val postLabel = relativeTimeHelper.getLabel(it.post.millisCreatedAt)
//                        val refPostLabel = refPost?.millisCreatedAt?.let { millis ->
//                            relativeTimeHelper.getLabel(millis)
//                        }
//                        val doubleRefPostLabel = refPost?.referencedPost?.let { txId ->
//                            it.referenced[txId]?.let { p ->
//                                relativeTimeHelper.getLabel(p.millisCreatedAt)
//                            }
//                        }
//
//                        ATTwetchPostItem(
//                            it.post.id,
//                            it,
//                            postLabel,
//                            refPostLabel,
//                            doubleRefPostLabel
//                        )
                        postItemHelper.createItem(it, relativeTimeHelper)
                    }.toCollection(itemList)
                }

                Timber.d("new post-list; size: ${itemList.size}; page:${postModel.page} hasMorePages:${postModel.hasMorePages} isRefreshing:$isRefreshing")

                if (postModel.hasMorePages) {
                    itemList.add(loadingItem)
                }


                lastPostItemList = itemList
                setItemsOnAdapter()

                if (tab != lastRestoredScrollStateTab) {

                    val cachedState = when (tab) {
                        TwetchUserProfileUseCase.UserProfileTab.LATEST -> helper.cachedLatestState
                        TwetchUserProfileUseCase.UserProfileTab.REPLIES -> helper.cachedRepliesState
                        TwetchUserProfileUseCase.UserProfileTab.LIKES -> helper.cachedLikesState
                        else -> throw RuntimeException()
                    }


                    layoutManager.onRestoreInstanceState(cachedState)
                    lastRestoredScrollStateTab = tab
                }
            }
        }
    }


    private fun refreshTabsItemList(activeTabItemId: String) {
        val helper = helper ?: throw RuntimeException()

        when {
            tabItemLatest.id == activeTabItemId -> {
                tabItemLatest = tabItemLatest.copy(
                    active = true
                )
                tabItemReply = tabItemReply.copy(
                    active = false
                )
                tabItemLikes = tabItemLikes.copy(
                    active = false
                )


                helper.onTabChanged(TwetchUserProfileUseCase.UserProfileTab.LATEST)

            }
            tabItemReply.id == activeTabItemId -> {
                tabItemLatest = tabItemLatest.copy(
                    active = false
                )
                tabItemReply = tabItemReply.copy(
                    active = true
                )
                tabItemLikes = tabItemLikes.copy(
                    active = false
                )


                helper.onTabChanged(TwetchUserProfileUseCase.UserProfileTab.REPLIES)

            }
            tabItemLikes.id == activeTabItemId -> {
                tabItemLatest = tabItemLatest.copy(
                    active = false
                )
                tabItemReply = tabItemReply.copy(
                    active = false
                )
                tabItemLikes = tabItemLikes.copy(
                    active = true
                )


                helper.onTabChanged(TwetchUserProfileUseCase.UserProfileTab.LIKES)

            }
            else -> {
                throw RuntimeException()
            }
        }

        refreshLoadingIndicatorState()

        lastTabsList = listOf(
            tabItemLatest,
            tabItemReply,
            tabItemLikes
        )
    }

    private fun isRefreshing(): Boolean {
        val helper = helper ?: let {
            return false
        }
        val refreshingState = helper.metaState.value

        return when (helper.content.value.activeTab) {
            TwetchUserProfileUseCase.UserProfileTab.PROFILE -> {
                refreshingState.profileBaseRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.LATEST -> {
                refreshingState.latestRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.REPLIES -> {
                refreshingState.repliesRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.LIKES -> {
                refreshingState.likesRefreshing
            }
        }
    }

    private fun refreshLoadingIndicatorState() {
        val helper = helper ?: throw RuntimeException()

        val activeTab = helper.content.value.activeTab
        val refreshingState = helper.metaState.value

        when (activeTab) {
            TwetchUserProfileUseCase.UserProfileTab.PROFILE -> {
                if (swipeRefresh.isRefreshing == refreshingState.profileBaseRefreshing) return
                swipeRefresh.isRefreshing = refreshingState.profileBaseRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.LATEST -> {
                if (swipeRefresh.isRefreshing == refreshingState.latestRefreshing) return
                swipeRefresh.isRefreshing = refreshingState.latestRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.REPLIES -> {
                if (swipeRefresh.isRefreshing == refreshingState.repliesRefreshing) return
                swipeRefresh.isRefreshing = refreshingState.repliesRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.LIKES -> {
                if (swipeRefresh.isRefreshing == refreshingState.likesRefreshing) return
                swipeRefresh.isRefreshing = refreshingState.likesRefreshing
            }
        }
    }





    private fun cacheCurrentScrollState() {
        val helper = helper ?: return

        when (helper.content.value.activeTab) {
            TwetchUserProfileUseCase.UserProfileTab.PROFILE -> {
                Timber.d("profileContent scroll-state saved")
                helper.cachedProfileBaseState = layoutManager.onSaveInstanceState()
            }
            TwetchUserProfileUseCase.UserProfileTab.LATEST -> {
                Timber.d("latestContent scroll-state saved")
                helper.cachedLatestState = layoutManager.onSaveInstanceState()
            }
            TwetchUserProfileUseCase.UserProfileTab.REPLIES -> {
                Timber.d("repliesContent scroll-state saved")
                helper.cachedRepliesState = layoutManager.onSaveInstanceState()
            }
            TwetchUserProfileUseCase.UserProfileTab.LIKES -> {
                Timber.d("likesContent scroll-state saved")
                helper.cachedLikesState = layoutManager.onSaveInstanceState()
            }
        }
    }




    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!wasCreatedOnce) return

        recycler.stopScroll()
        cacheCurrentScrollState()
    }


    inner class FloatingTabsListener : BaseListAdapter.ATTwetchTabsListener {
        override fun onBound(id: String): List<TabItem> {
            return lastTabsList
        }

        override fun onTabClicked(id: String, tab: TabItem): List<TabItem> {
            if (tab.active) {
                recycler.smoothScrollToPosition(0)
                return lastTabsList
            } else {
                recycler.stopScroll()
                cacheCurrentScrollState()
            }

            refreshTabsItemList(tab.id)

            invalidateInListTabItem()

            dismissScrollToTop()

            return lastTabsList
        }

        override fun onAttached(holder: RecyclerView.ViewHolder) {
        }

        override fun onDetached() {
        }

        private fun invalidateInListTabItem() {
            //recycler.findViewHolderForAdapterPosition(1)
            if (adapter.itemCount <= 1) return
            adapter.notifyItemChanged(TAB_ADAPTER_POS)
        }


        fun onInlineTabClicked() {
            floatingTabHolder.onBindViewHolder(tabsItem, floatingTabsListener)
        }


        var attachedInlineTabHolder: RecyclerView.ViewHolder? = null

        fun onInlineTabsAttached(holder: RecyclerView.ViewHolder) {
            attachedInlineTabHolder = holder
        }

        fun onInlineTabsDetached() {
            attachedInlineTabHolder = null
        }

        fun onRecyclerScrolled(dy: Int) {
            if (!hasPostListBeenApplied) return

            if (dy == 0) {
                // scroll pos of recycler restored
                val pos = layoutManager.findFirstVisibleItemPosition()
                if (pos >= TAB_ADAPTER_POS) {
                    if (View.VISIBLE == floatingTabHolder.itemView.visibility) return
                    floatingTabHolder.itemView.visibility = View.VISIBLE
                } else {
                    if (View.INVISIBLE == floatingTabHolder.itemView.visibility) return
                    floatingTabHolder.itemView.visibility = View.INVISIBLE
                }

            } else {
                val holder = attachedInlineTabHolder
                if (holder == null) {
                    // the inline tabs are not attached to layout -- way off-screen
                    if (View.VISIBLE == floatingTabHolder.itemView.visibility) return
                    floatingTabHolder.itemView.visibility = View.VISIBLE

                } else {
                    // show/hide floating-tabs depending on the layout position of the in-line tabs
                    if (holder.itemView.top <= 0) {
                        if (View.VISIBLE == floatingTabHolder.itemView.visibility) return
                        floatingTabHolder.itemView.visibility = View.VISIBLE

                    } else {
                        if (View.INVISIBLE == floatingTabHolder.itemView.visibility) return
                        floatingTabHolder.itemView.visibility = View.INVISIBLE

                    }
                }
            }
        }

    }



    override fun onStop() {
        lastAppliedList = null
        lastAppliedTab = null
        super.onStop()
    }







    private suspend fun awaitRecyclerAnimationsFinish(fromListChange: Boolean = true) {
        if (fromListChange) {
            swapOutRefreshingItemJob?.cancel()
            swapOutRefreshingItemJob = null
        }

        recycler.itemAnimator?.let { animator ->
            //making sure that the any previous animation get completed
            if (!animator.isRunning) {
                return@let
            }

            suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                animator.isRunning { cancellableContinuation.resume(Unit) }
            }
        }

        suspendCancellableCoroutine<Unit> { continuation ->
            if (recycler.isComputingLayout) {
                recycler.post {
                    continuation.resume(Unit)
                }
            } else {
                continuation.resume(Unit)
            }

        }
    }


    var swapOutRefreshingItemJob: Job? = null

    private fun swapOutRefreshingItem() {
        val helper = helper ?: return
        if (swapOutRefreshingItemJob?.isActive == true) return

        swapOutRefreshingItemJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            awaitRecyclerAnimationsFinish(false)

            val items = adapter.getCopyOfItems()

            var loadingItemPos = -1

            val countToCheck = min(5, items.size)
            for (i in 0 until countToCheck) {
                val item = items[items.lastIndex - i]
                if (item is ATContentLoading0) {
                    loadingItemPos = items.lastIndex - i
                    break
                }
            }

            ensureActive()

            if (loadingItemPos == -1) {
                Timber.d(">>> loadingItemPost not found")
                helper.requestNextPage()
                return@launchWhenStarted
            }

            adapter.swapOutItem(loadingItemPos, altLoadingItem)

            awaitRecyclerAnimationsFinish(false)
            helper.requestNextPage()
        }
    }




    private fun refreshDynamicBottomHeight() {
        val appbarOffset = utilViewModel.mainContentTopDistance.value

        coinManagementHelper.translationY = -1f * appbarOffset
    }

}