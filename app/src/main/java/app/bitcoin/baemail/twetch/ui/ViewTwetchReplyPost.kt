package app.bitcoin.baemail.twetch.ui

import android.content.Context
import android.graphics.Outline
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.util.Linkify
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.text.util.LinkifyCompat
import androidx.core.view.ViewCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableRoundedBitmap
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.span.LabelSpan
import app.bitcoin.baemail.core.presentation.util.RelativeTimeHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.data.util.match
import app.bitcoin.baemail.core.presentation.view.ViewTwetchTweetCard
import coil.load
import coil.size.Precision
import coil.target.ImageViewTarget
import com.google.android.material.textview.MaterialTextView
import kotlin.math.min

class ViewTwetchReplyPost : ConstraintLayout {

    companion object {
        val dummy = PostModel(
            "d6f1473218e5e1d256c0d83fd30b92e8c941baf41344b26472f4d04f76f12145",
            "d6f1473218e5e1d256c0d83fd30b92e8c941baf41344b26472f4d04f76f12145",
            "hey interesting idea",
            false,
            false,
            0,
            0,
            0,
            0,
            0,
            System.currentTimeMillis(),
            null,
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            "d6f1473218e5e1d256c0d83fd30b92e8c941baf41344b26472f4d04f76f12145",
            PostUser(
                    "1337",
                    "dummy name",
                    "https://cimg.twetch.com/avatars/2021/04/25/7050a3268c8fed54ca42ba3702fc8a27ae06fe712741c15f55b8f4ad8ffcb0b9.51c31e34-0115-4489-8055-458b0113331f"
            ),
            System.currentTimeMillis(),
            "-",
            false,
            ""
        )
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private val dp: Float

    private var value: PostModel = dummy
    private var relativeTimeHelper = RelativeTimeHelper(context, System.currentTimeMillis())




    private val tweet: ViewTwetchTweetCard
    private val replyingLabel: MaterialTextView
    private val icon: ImageView
    private val postImage: ImageView
    private val postImageLabel: MaterialTextView
    private val created: MaterialTextView
    private val name: MaterialTextView
    private val userId: MaterialTextView
    private val content: MaterialTextView



    private val drawableNpc = ContextCompat.getDrawable(context, R.drawable.ic_npc3000)

    private val imagePlaceholderDrawable = ColorDrawable(
            context.getColorFromAttr(R.attr.colorSurfaceVariant)
    )
    private val colorSelector = R.color.selector_on_primary.let { id ->
        ContextCompat.getColor(context, id)
    }

    private val colorPrimary = context.getColorFromAttr(R.attr.colorPrimary)
    private val colorOnPrimary = context.getColorFromAttr(R.attr.colorOnPrimary)



    init {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.view_twetch_reply_post, this, false)
        val set = ConstraintSet()
        view.id = ViewCompat.generateViewId()
        addView(view)

        set.clone(this)
        set.match(view, this)

        dp = context.resources.displayMetrics.density


        tweet = view.findViewById(R.id.tweet)
        replyingLabel = view.findViewById(R.id.replying_label)
        icon = view.findViewById(R.id.reply_icon)
        postImage = view.findViewById(R.id.post_image)
        postImageLabel = view.findViewById(R.id.post_image_label)
        created = view.findViewById(R.id.created)
        name = view.findViewById(R.id.reply_name)
        userId = view.findViewById(R.id.reply_name_user_id)
        content = view.findViewById(R.id.content)



        val colorTransparentSurface = ColorUtils.setAlphaComponent(
                context.getColorFromAttr(R.attr.colorSurface),
                123
        )



        postImageLabel.clipToOutline = true
        postImageLabel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }
        postImageLabel.background = ColorDrawable(colorTransparentSurface)



        postImage.clipToOutline = true
        postImage.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, 12 * dp)
            }
        }
        postImage.foreground = DrawableState.getNew(colorSelector)


        icon.clipToOutline = true
        icon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                val roundness = min(p0.width / 2f, p0.height / 2f)
                p1.setRoundRect(0, 0, p0.width, p0.height, roundness)
            }
        }
        icon.foreground = DrawableState.getNew(colorSelector)




        refresh()
    }

    private fun refresh() {


        initPostBase(value)
        initReplyingLabel(value)
        initTweet(value)
        initPostImage(value)


    }

    fun applyModel(model: PostModel) {
        if (model == value) return
        value = model
        relativeTimeHelper = RelativeTimeHelper(context, System.currentTimeMillis())

        refresh()
    }


    private fun initPostBase(post: PostModel) {

        if (post.isRemoved) {
            name.text = context.getString(R.string.twetch_npc_3000)
            userId.text = ""

        } else {
            name.text = post.user.name
            userId.text = resources.getString(R.string.twetch_user_id, post.user.id)
        }

        created.text = relativeTimeHelper.getLabel(post.millisCreatedAt)


        if (post.isRemoved) {
            val finalContent = SpannableStringBuilder(context.getString(R.string.twetch_post_removed))
            finalContent.setSpan(
                ForegroundColorSpan(ColorUtils.setAlphaComponent(
                    context.getColorFromAttr(R.attr.colorOnSurface),
                    150
                )),
                0,
                finalContent.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            content.setText(finalContent, TextView.BufferType.SPANNABLE)

        } else {
            val finalContent = SpannableStringBuilder(post.content)
            if (post.isBranch) {
                finalContent.append("\n")
                val startPos = finalContent.length
                val postIndicator =
                    context.resources.getString(R.string.referenced_post).toUpperCase()
                finalContent.append(postIndicator)


                finalContent.setSpan(
                    LabelSpan(
                        context.resources.getString(R.string.referenced_post).toUpperCase(),
                        ColorUtils.setAlphaComponent(colorPrimary, 150),
                        colorOnPrimary,
                        dp * 20,
                        dp * 12,
                        dp * 2
                    ),
                    startPos,
                    startPos + postIndicator.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }

            content.setText(finalContent, TextView.BufferType.SPANNABLE)
        }


        if (post.isRemoved) {
            content.visibility = View.VISIBLE

        } else if (post.content.isBlank()) {
            content.visibility = View.GONE
        } else {
            LinkifyCompat.addLinks(content, Linkify.WEB_URLS)
            content.visibility = View.VISIBLE
        }


        if (post.isRemoved) {
            icon.load(drawableNpc)

        } else {
            post.user.icon?.let {
                icon.load(it) {
                    placeholder(imagePlaceholderDrawable)
                    error(imagePlaceholderDrawable)
                    precision(Precision.INEXACT)
                    target(object : ImageViewTarget(icon) {
                        override fun onSuccess(result: Drawable) {
                            val d = if (result is BitmapDrawable) {
                                DrawableRoundedBitmap(context, 0f, result.bitmap)
                            } else {
                                result
                            }
                            super.onSuccess(d)
                        }
                    })
                }
            } ?: let {
                icon.load(imagePlaceholderDrawable)
            }
        }

    }

    private fun initReplyingLabel(post: PostModel) {
        if (post.parents.isNotEmpty()) {
            val list = post.parents
            val namesList = list
                    .map { it.user.name }
                    .asReversed()
                    .fold(mutableListOf<String>()) { acc, item ->
                        if (acc.contains(item)) {
                            return@fold acc
                        } else {
                            acc.add(item)
                            return@fold acc
                        }
                    }
            replyingLabel.visibility = View.VISIBLE
            val sb = StringBuilder(resources.getString(R.string.replying_to))
            val maxIndex = min(namesList.size, 2)
            for (i in 0 until maxIndex) {
                sb.append(" @").append(namesList[i])
            }
            if (namesList.size > maxIndex) {
                sb.append(" ...")
            }
            replyingLabel.text = sb.toString()
        } else {
            replyingLabel.visibility = View.GONE
        }
    }

    private fun initTweet(post: PostModel) {
        post.tweet?.let {
            tweet.applyModel(it)
            tweet.visibility = View.VISIBLE
        } ?: let {
            tweet.visibility = View.GONE
        }
    }

    private fun initPostImage(post: PostModel) {
        post.getFullImageUrlList().let { list ->
            if (list.isEmpty()) {
                postImage.visibility = View.GONE
                postImageLabel.visibility = View.GONE
            } else {
                postImage.visibility = View.VISIBLE

                postImage.load(list[0]) {
                    placeholder(imagePlaceholderDrawable)
                    error(imagePlaceholderDrawable)
                    precision(Precision.INEXACT)
                    target(object : ImageViewTarget(postImage) {
                        override fun onSuccess(result: Drawable) {
                            val d = if (result is BitmapDrawable) {
                                DrawableRoundedBitmap(context, 0f, result.bitmap)
                            } else {
                                result
                            }
                            super.onSuccess(d)
                        }
                    })
                }

                if (list.size > 1) {
                    postImageLabel.visibility = View.VISIBLE
                    postImageLabel.text = context.getString(
                            R.string.one_of_label,
                            list.size
                    )
                } else {
                    postImageLabel.visibility = View.GONE
                }
            }
        }



    }

}