package app.bitcoin.baemail.twetch.ui

import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchChatParticipantItem
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.gson.Gson
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.mapLatest
import timber.log.Timber
import javax.inject.Inject

class TwetchChatParticipantsFragment : Fragment(R.layout.fragment_twetch_chat_participants) {

    companion object {
        const val KEY_CHAT_ID = "id"
        const val KEY_SCROLL_POS = "scroll_pos"
    }

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var gson: Gson

    private lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel

    private lateinit var topbar: View
    private lateinit var close: Button
    private lateinit var recycler: RecyclerView



    lateinit var layoutManager: LinearLayoutManager
    private var lastSavedRecyclerState: Parcelable? = null

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter

    private var wasCreatedOnce = false



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)



        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
                .get(TwetchUtilViewModel::class.java)


        val chatId = requireArguments().getString(KEY_CHAT_ID)!!



        lastSavedRecyclerState = null
        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true
        val serializedRecyclerState: Parcelable? = savedInstanceState?.getParcelable(KEY_SCROLL_POS)






        topbar = requireView().findViewById(R.id.topbar)
        close = requireView().findViewById(R.id.close)
        recycler = requireView().findViewById(R.id.list)



        val dp = resources.displayMetrics.density
        val topbarElevatedHeight = 3.0f
        val topbarBackground = MaterialShapeDrawable.createWithElevationOverlay(requireContext(), 0f)

        topbar.background = topbarBackground








        val participantListener = object : BaseListAdapter.ATTwetchChatParticipantsListener {
            override fun onProfileClicked(participant: TwetchChatInfoStore.Participant) {
                val args = Bundle().also {
                    it.putString(TwetchUserProfileFragment.KEY_USER_ID, participant.id)
                }

                findNavController().navigate(R.id.action_global_twetchUserProfile, args)
            }

        }


        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atTwetchChatParticipantsListener = participantListener
            )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)

        serializedRecyclerState?.let {
            lastSavedRecyclerState = it
            layoutManager.onRestoreInstanceState(it)
        }

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                when (layoutManager.findFirstCompletelyVisibleItemPosition()) {
                    RecyclerView.NO_POSITION -> false
                    0 -> false
                    else -> true
                }.let { showShadow ->
                    if (showShadow) {
                        utilViewModel.onMainContentScrollChanged(false)
                        topbarBackground.elevation = topbarElevatedHeight
                        topbar.elevation = topbarElevatedHeight * dp
                        close.elevation = topbarElevatedHeight * dp
                    } else {
                        utilViewModel.onMainContentScrollChanged(true)
                        topbarBackground.elevation = 0f
                        topbar.elevation = 0f
                        close.elevation = 0f
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (RecyclerView.SCROLL_STATE_IDLE == newState) {
                    Timber.d("new state saved")
                    lastSavedRecyclerState = layoutManager.onSaveInstanceState()
                }
            }
        })














        close.setOnClickListener {
            findNavController().popBackStack()
        }






        viewModel.getChatInfo(chatId).mapLatest {
            it ?: return@mapLatest null

            emptyList<ATTwetchChatParticipantItem>()
            //todo
            //todo
            //todo
            //todo
            //todo
//            it.participant.map { participant ->
//                ATTwetchChatParticipantItem(participant)
//            }
        }.asLiveData().observe(viewLifecycleOwner) { adapterTypeList ->
            adapterTypeList ?: return@observe

            adapter.setItems(adapterTypeList)

            lastSavedRecyclerState?.let {
                layoutManager.onRestoreInstanceState(it)
            }



        }



    }















    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!wasCreatedOnce) return

        val state = layoutManager.onSaveInstanceState()
        lastSavedRecyclerState = state
        outState.putParcelable(KEY_SCROLL_POS, state)
    }



}