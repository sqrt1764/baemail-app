package app.bitcoin.baemail.twetch.ui

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class TwetchNotificationsCache {
    private val dummyCache = HashMap<String, TwetchNotificationsStore.Notification>()

    private val _updatedState = MutableStateFlow(0)
    val updatedState: StateFlow<Int>
        get() = _updatedState


    fun getNotification(activeUserId: String, id: String): TwetchNotificationsStore.Notification? {
        return dummyCache[activeUserId + id]
    }

    fun setNotification(activeUserId: String, item: TwetchNotificationsStore.Notification) {
        dummyCache[activeUserId + item.id] = item.copy(post = null)
    }

    fun notifyUpdated() {
        _updatedState.value = 1 + _updatedState.value
    }


}