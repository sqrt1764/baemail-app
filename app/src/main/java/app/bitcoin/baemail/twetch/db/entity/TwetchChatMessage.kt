package app.bitcoin.baemail.twetch.db.entity

import androidx.room.Entity
import app.bitcoin.baemail.core.data.room.TABLE_TWETCH_CHAT_MESSAGES
import app.bitcoin.baemail.twetch.ui.TwetchChatInfoStore

@Entity(tableName = TABLE_TWETCH_CHAT_MESSAGES, primaryKeys = ["paymail", "chatId", "id"])
data class TwetchChatMessage(
    val paymail: String,
    val id: String,
    val chatId: String,
    val millisCreatedAt: Long,
    val userId: String,
    val description: String,
    val images: List<String>,
    val user: TwetchChatInfoStore.Participant?
)