package app.bitcoin.baemail.twetch.ui

import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.google.gson.Gson
import com.google.gson.JsonArray
import java.util.regex.Pattern


class TwetchPostUserCache {
    val cache = HashMap<String, PostUser>()

    fun setUser(user: PostUser) {
        cache[user.id] = user
    }

    fun getUser(id: String): PostUser? {
        return cache[id]
    }



    fun setUsersOfPost(post: PostModel) {
        setUser(post.user)
        post.parents.forEach { parent ->
            setUser(parent.user)
        }
    }


    fun findUsersMissingForIds(userIdList: Collection<String>): Set<String> {
        if (userIdList.isEmpty()) return emptySet()
        val missing = HashSet<String>()

        userIdList.forEach { id ->
            if (!cache.containsKey(id)) {
                missing.add(id)
            }
        }

        return missing
    }

    companion object {
        private val userIdPattern = Pattern.compile("(@)([0-9]+)")

        fun findUserMentions(content: String): List<String> {
            val matcher = userIdPattern.matcher(content)
            val found = mutableListOf<String>()
            while (matcher.find()) {
                val foundSequence = matcher.group(2) ?: continue
                found.add(foundSequence)
            }
            return found
        }



        suspend fun findUserForIds(
            nodeRuntimeRepository: NodeRuntimeRepository,
            gson: Gson,
            user: Set<String>
        ): List<PostUser> {

            val jsonArray = JsonArray()
            user.forEach { id ->
                jsonArray.add(id)
            }

            val result = nodeRuntimeRepository.runAsyncScript("""
                
                let millisStart = new Date().getTime();
                
                await context.twetch.ensureAuthHasCompleted();
                
                let millisPostAuth = new Date().getTime();
                
                let queryResult = await context.twetch.instance.query(`
                    {
                        allUsers(filter: {id: {in: $jsonArray }}) {
                            nodes {
                                id
                                name
                                icon
                            }
                        }
                    }
                `);
                
                let millisPostRequest = new Date().getTime();
                
                console.log("time spent awaiting twetch-auth:" 
                + (millisPostAuth - millisStart) 
                + "ms; time spent on post-list:" 
                + (millisPostRequest - millisPostAuth) 
                + "ms");
                
                
                return JSON.stringify(queryResult.allUsers.nodes);
                
            """.trimIndent())

            if (result is RunScript.Response.Success) {
//                Timber.d("success: ${result.content}") //...was too much spam in logs

                val responseJson = gson.fromJson(result.content, JsonArray::class.java)
                val parsedList = mutableListOf<PostUser>()

                responseJson.forEach { item ->
                    val id = item.asJsonObject.get("id").asString
                    val name = item.asJsonObject.get("name").asString
                    val icon = if (item.asJsonObject.has("icon")) {
                        val icon = item.asJsonObject.get("icon")
                        if (!icon.isJsonNull) icon.asString else null
                    } else null
                    parsedList.add(
                        PostUser(
                        id,
                        name,
                        icon
                    )
                    )
                }

                return parsedList

            } else {
                result as RunScript.Response.Fail
                throw result.throwable
            }
        }
    }
}