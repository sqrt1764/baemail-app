package app.bitcoin.baemail.twetch.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.bitcoin.baemail.core.data.room.TABLE_TWETCH_USER_PROFILE_BASE
import app.bitcoin.baemail.twetch.db.entity.UserProfileBaseModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine

@Dao
abstract class UserProfileBaseDao {

    private val _updatedState = MutableStateFlow(0)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(userProfile: UserProfileBaseModel)

    @Query("select * from $TABLE_TWETCH_USER_PROFILE_BASE where activeUserId = :activeUserId and userId = :userId")
    abstract fun doGetFlow(activeUserId: String, userId: String): Flow<UserProfileBaseModel?>

    fun getFlow(activeUserId: String, userId: String): Flow<UserProfileBaseModel?> {
        return _updatedState.combine(doGetFlow(activeUserId, userId)) { _, profile ->
            profile?.copy()
        }
    }


    @Query("delete from $TABLE_TWETCH_USER_PROFILE_BASE where activeUserId = :activeUserId and userId = :userId")
    abstract suspend fun delete(activeUserId: String, userId: String)


    @Query("delete from $TABLE_TWETCH_USER_PROFILE_BASE")
    abstract suspend fun deleteAll()


    fun notifyUpdated() {
        _updatedState.value = 1 + _updatedState.value
    }

}