package app.bitcoin.baemail.twetch.ui

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.Button
import android.widget.HorizontalScrollView
import android.widget.TextView
import androidx.core.view.doOnPreDraw
import androidx.core.widget.TextViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATContentLoading0
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchNoPostsFoundItem
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchUserProfileRootItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.twetch.db.entity.UserProfileBaseModel
import app.bitcoin.baemail.core.presentation.util.RelativeTimeHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import com.google.android.material.button.MaterialButton
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.math.min

class TwetchUserProfileFragment : Fragment(R.layout.fragment_twetch_user_profile) {

    companion object {
        const val KEY_USER_ID = "user_id"
    }

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel

    private lateinit var helper: TwetchUserProfileUseCase

    private val postItemHelper: TwetchPostItemHelper by lazy {
        TwetchPostItemHelper(
            requireContext(),
            viewModel.postUserCache,
            TextViewCompat.getTextMetricsParams(dummyBodyText)
        )
    }



    private lateinit var close: Button
    private lateinit var tabScrollView: HorizontalScrollView
    private lateinit var tabProfile: MaterialButton
    private lateinit var tabLatest: MaterialButton
    private lateinit var tabReplies: MaterialButton
    private lateinit var tabLikes: MaterialButton
    private lateinit var topbar: View
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var recycler: RecyclerView
    private lateinit var scrollToTop: TextView

    private lateinit var dummyBodyText: TextView


    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter

    private var wasCreatedOnce = false

    private var lastRestoredScrollStateTab: TwetchUserProfileUseCase.UserProfileTab? = null

    var hasScrollUpBeenInitialized = false

    private val noPostsFoundItem = ATTwetchNoPostsFoundItem("0")

    private val topHeightLD = MutableLiveData<Int>()
    private val topHeightItem = ATHeightDecorDynamic("0", topHeightLD)

    private val altLoadingItemLD = MutableLiveData<Int>()
    private val altLoadingItem = ATHeightDecorDynamic("1", altLoadingItemLD)

    val colorPrimary: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }


    val colorDanger: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorDanger)
    }

    val dp: Float by lazy {
        resources.displayMetrics.density
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)


        val userId = requireArguments().getString(KEY_USER_ID)!!

        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(TwetchUtilViewModel::class.java)

        helper = viewModel.getUserProfileContent(userId)


        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true



        dummyBodyText = requireView().findViewById(R.id.dummy_body_text)

        swipeRefresh = requireView().findViewById(R.id.swipe_refresh)
        close = requireView().findViewById(R.id.close)
        tabScrollView = requireView().findViewById(R.id.tab_button_scroller)
        tabProfile = requireView().findViewById(R.id.tab_profile)
        tabLatest = requireView().findViewById(R.id.tab_latest)
        tabReplies = requireView().findViewById(R.id.tab_replies)
        tabLikes = requireView().findViewById(R.id.tab_likes)
        recycler = requireView().findViewById(R.id.list)
        topbar = requireView().findViewById(R.id.topbar)
        scrollToTop = requireView().findViewById(R.id.scroll_to_top)






        val colorPrimary = requireContext().getColorFromAttr(R.attr.colorPrimary)
        val colorTransparentPrimary = Color.argb(
            200,
            Color.red(colorPrimary),
            Color.green(colorPrimary),
            Color.blue(colorPrimary)
        )

        topHeightLD.value = (4 * dp).toInt()
        altLoadingItemLD.value = (108 * dp).toInt()


        val topbarElevatedHeight = resources.getDimension(R.dimen.inner_topbar_floating_elevation) / dp
        val topbarBackground = MaterialShapeDrawable.createWithElevationOverlay(requireContext(), 0f)

        topbar.background = topbarBackground




        swipeRefresh.setProgressViewOffset(true, (30 * dp).toInt(), (120 * dp).toInt())
        swipeRefresh.setColorSchemeColors(requireContext().getColorFromAttr(R.attr.colorOnPrimary))
        swipeRefresh.setProgressBackgroundColorSchemeColor(colorTransparentPrimary)
        swipeRefresh.setOnRefreshListener {
            helper.onRefreshRequested()

        }





        scrollToTop.clipToOutline = true
        scrollToTop.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, p0.height / 2f)
            }
        }
        scrollToTop.background = ColorDrawable(colorTransparentPrimary)

        scrollToTop.setOnClickListener {
            recycler.smoothScrollToPosition(0)
            scrollToTop.visibility = View.INVISIBLE
        }















        tabScrollView.overScrollMode = View.OVER_SCROLL_NEVER



        tabProfile.setOnClickListener {
            recycler.stopScroll()
            cacheCurrentScrollState()

            if (!helper.onTabChanged(TwetchUserProfileUseCase.UserProfileTab.PROFILE)) {
                recycler.smoothScrollToPosition(0)
            } else {
                refreshLoadingIndicatorState()
            }

            dismissScrollToTop()

            scrollToViewOfBar(tabProfile, tabScrollView)
        }

        tabLatest.setOnClickListener {
            recycler.stopScroll()
            cacheCurrentScrollState()

            if (!helper.onTabChanged(TwetchUserProfileUseCase.UserProfileTab.LATEST)) {
                recycler.smoothScrollToPosition(0)
            } else {
                refreshLoadingIndicatorState()
            }

            dismissScrollToTop()

            scrollToViewOfBar(tabLatest, tabScrollView)
        }

        tabReplies.setOnClickListener {
            recycler.stopScroll()
            cacheCurrentScrollState()

            if (!helper.onTabChanged(TwetchUserProfileUseCase.UserProfileTab.REPLIES)) {
                recycler.smoothScrollToPosition(0)
            } else {
                refreshLoadingIndicatorState()
            }

            dismissScrollToTop()

            scrollToViewOfBar(tabReplies, tabScrollView)
        }

        tabLikes.setOnClickListener {
            recycler.stopScroll()
            cacheCurrentScrollState()

            if (!helper.onTabChanged(TwetchUserProfileUseCase.UserProfileTab.LIKES)) {
                recycler.smoothScrollToPosition(0)
            } else {
                refreshLoadingIndicatorState()
            }

            dismissScrollToTop()

            scrollToViewOfBar(tabLikes, tabScrollView)
        }







        val contentLoadingListener = object : BaseListAdapter.ATContentLoadingListener {
            override fun onBound() {

                when (helper.content.value.activeTab) {
                    TwetchUserProfileUseCase.UserProfileTab.LATEST -> {
                        if (helper.metaState.value.latestRefreshing) {
                            return
                        }
                    }
                    TwetchUserProfileUseCase.UserProfileTab.REPLIES -> {
                        if (helper.metaState.value.repliesRefreshing) {
                            return
                        }

                    }
                    TwetchUserProfileUseCase.UserProfileTab.LIKES -> {
                        if (helper.metaState.value.likesRefreshing) {
                            return
                        }

                    }
                    else -> {
                        //do nothing
                    }
                }


                swapOutRefreshingItem()
            }
        }

        val profileBaseListener = object : BaseListAdapter.ATTwetchUserProfileBaseListener {
            override fun onUserIconClicked(url: String) {
                val args = Bundle().also {
                    val list = ArrayList<String>()
                    list.add(url)
                    it.putStringArrayList(TwetchChatFullImageFragment.KEY_IMAGE_URL, list)

                }
                findNavController().navigate(R.id.action_twetchUserProfileFragment2_to_twetchChatFullImageFragment3, args)
            }

            override fun onFollowUserClicked(model: ATTwetchUserProfileRootItem): Boolean {
                return viewModel.actionHelper.executeFollowOfUser(model.profileBase.userId)
            }

            override fun onUnfollowUserClicked(model: ATTwetchUserProfileRootItem): Boolean {
                return viewModel.actionHelper.executeUnfollowOfUser(model.profileBase.userId)
            }

        }

        val postListener = TwetchPostListener(
            requireContext(),
            findNavController(),
            viewModel.actionHelper,
            viewModel.videoClipManager,
            R.id.action_twetchUserProfileFragment2_self,
            R.id.action_twetchUserProfileFragment2_to_twetchChatFullImageFragment3,
            R.id.action_global_twetchPostDetails,
            R.id.action_twetchUserProfileFragment2_to_twetchBranchOptionsFragment7,
            R.id.action_twetchUserProfileFragment2_to_twetchVideoClipFragment2,
            viewModel.coinFlipSignalHelper.coinFlipChannel
        )

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atContentLoadingListener = contentLoadingListener,
                atTwetchUserProfileBaseListener = profileBaseListener,
                atTwetchPostListener = postListener
            )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER


        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var lastShowShadow: Boolean? = null

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                recyclerView.canScrollVertically(-1).let { showShadow ->
                    if (lastShowShadow == showShadow) return@let
                    lastShowShadow = showShadow

                    if (showShadow) {
                        utilViewModel.onMainContentScrollChanged(false)
                        topbarBackground.elevation = topbarElevatedHeight
                        tabScrollView.elevation = topbarElevatedHeight * dp
                        topbar.elevation = topbarElevatedHeight * dp
                        close.elevation = topbarElevatedHeight * dp
                    } else {
                        utilViewModel.onMainContentScrollChanged(true)
                        topbarBackground.elevation = 0f
                        tabScrollView.elevation = 0f
                        topbar.elevation = 0f
                        close.elevation = 0f
                    }
                }

                if (!hasScrollUpBeenInitialized) {
                    refreshShowingOfScrollToTop()
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (RecyclerView.SCROLL_STATE_IDLE == newState) {
                    cacheCurrentScrollState()

                    refreshShowingOfScrollToTop()

                }
            }
        })








        close.setOnClickListener {
            findNavController().popBackStack()
        }






        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            helper.metaState.collectLatest {
                refreshLoadingIndicatorState()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {

            helper.content.collectLatest { model ->

                if (TwetchUserProfileUseCase.UserProfileTab.PROFILE == helper.content.value.activeTab) {
                    tabProfile.setTextColor(colorDanger)
                    tabProfile.strokeColor = ColorStateList.valueOf(colorDanger)
                } else {
                    tabProfile.setTextColor(colorPrimary)
                    tabProfile.strokeColor = ColorStateList.valueOf(colorPrimary)
                }

                if (TwetchUserProfileUseCase.UserProfileTab.LATEST == helper.content.value.activeTab) {
                    tabLatest.setTextColor(colorDanger)
                    tabLatest.strokeColor = ColorStateList.valueOf(colorDanger)
                } else {
                    tabLatest.setTextColor(colorPrimary)
                    tabLatest.strokeColor = ColorStateList.valueOf(colorPrimary)
                }

                if (TwetchUserProfileUseCase.UserProfileTab.REPLIES == helper.content.value.activeTab) {
                    tabReplies.setTextColor(colorDanger)
                    tabReplies.strokeColor = ColorStateList.valueOf(colorDanger)
                } else {
                    tabReplies.setTextColor(colorPrimary)
                    tabReplies.strokeColor = ColorStateList.valueOf(colorPrimary)
                }

                if (TwetchUserProfileUseCase.UserProfileTab.LIKES == helper.content.value.activeTab) {
                    tabLikes.setTextColor(colorDanger)
                    tabLikes.strokeColor = ColorStateList.valueOf(colorDanger)
                } else {
                    tabLikes.setTextColor(colorPrimary)
                    tabLikes.strokeColor = ColorStateList.valueOf(colorPrimary)
                }



                when (model.activeTab) {
                    TwetchUserProfileUseCase.UserProfileTab.PROFILE -> {
                        initListForProfileTab(model.profileBase)
                    }
                    TwetchUserProfileUseCase.UserProfileTab.LATEST -> {
                        initListForTab(model.latest, model.activeTab)
                    }
                    TwetchUserProfileUseCase.UserProfileTab.REPLIES -> {
                        initListForTab(model.replies, model.activeTab)
                    }
                    TwetchUserProfileUseCase.UserProfileTab.LIKES -> {
                        initListForTab(model.likes, model.activeTab)
                    }
                }


            }
        }
    }


    private var tabBarScrollingJob: Job? = null

    fun scrollToViewOfBar(child: View, scrollView: HorizontalScrollView) {
        tabBarScrollingJob?.cancel()

        tabBarScrollingJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            delay(500)
            val distanceFromLeft = child.left - scrollView.scrollX
            val mustScrollAmount = distanceFromLeft - dp * 150
            scrollView.smoothScrollBy(mustScrollAmount.toInt(), 0)
        }
    }


    fun refreshShowingOfScrollToTop() {
        hasScrollUpBeenInitialized = true

        if (layoutManager.findFirstVisibleItemPosition() > 10) {
            if (scrollToTop.visibility != View.VISIBLE) {
                scrollToTop.visibility = View.VISIBLE
            }
        } else {
            if (scrollToTop.visibility != View.INVISIBLE) {
                scrollToTop.visibility = View.INVISIBLE
            }
        }
    }

    fun dismissScrollToTop() {
        hasScrollUpBeenInitialized = false

        if (scrollToTop.visibility != View.INVISIBLE) {
            scrollToTop.visibility = View.INVISIBLE
        }
    }


    private var lastAppliedProfileBase: UserProfileBaseModel? = null
    private var lastAppliedList: TwetchPostModel? = null
    private var lastAppliedTab: TwetchUserProfileUseCase.UserProfileTab? = null

    private suspend fun initListForProfileTab(profileModel: UserProfileBaseModel?) {
        if (lastAppliedProfileBase == profileModel
            && lastAppliedTab == TwetchUserProfileUseCase.UserProfileTab.PROFILE
        ) {
            Timber.d("post-list is unchanged; returning")
            return
        }
        lastAppliedList = null
        lastAppliedTab = TwetchUserProfileUseCase.UserProfileTab.PROFILE
        lastAppliedProfileBase = profileModel

        awaitRecyclerAnimationsFinish()

        if (profileModel == null) {
            adapter.setItems(emptyList())
        } else {

            adapter.setItems(listOf(
                ATTwetchUserProfileRootItem(
                    "0",
                    profileModel,
                    true //todo false if me-user
                )
            ))

            if (TwetchUserProfileUseCase.UserProfileTab.PROFILE != lastRestoredScrollStateTab) {
                layoutManager.onRestoreInstanceState(helper.cachedProfileBaseState)
                lastRestoredScrollStateTab = TwetchUserProfileUseCase.UserProfileTab.PROFILE
            }
        }


    }


    private suspend fun initListForTab(postModel: TwetchPostModel?, tab: TwetchUserProfileUseCase.UserProfileTab) {
        if (lastAppliedList == postModel && lastAppliedTab == tab) {
            Timber.d("post-list is unchanged; returning")
            return
        }
        lastAppliedList = postModel
        lastAppliedTab = tab
        lastAppliedProfileBase = null


        if (TwetchUserProfileUseCase.UserProfileTab.PROFILE == tab) {
            throw RuntimeException()
        }



        awaitRecyclerAnimationsFinish()


        if (postModel == null) {
            Timber.d("model == null")
            adapter.setItems(emptyList())

            lastRestoredScrollStateTab = tab

        } else {

            val isRefreshing = isRefreshing()
            val loadingItem = if (!isRefreshing && postModel.hasMorePages) {
                ATContentLoading0(postModel.page)
            } else {
                altLoadingItem
            }

            if (TwetchPostListHelper.PAGE_NOT_LOADED == postModel.page) {
                Timber.d("model.page == PAGE_NOT_LOADED")
                helper.requestNextPage()
            }

            if (postModel.list.isEmpty()) {
                if (postModel.hasMorePages) {
                    adapter.setItems(
                        listOf(
                            topHeightItem,
                            loadingItem
                        )
                    )
                } else {
                    adapter.setItems(
                        listOf(
                            topHeightItem,
                            noPostsFoundItem
                        )
                    )
                }

                lastRestoredScrollStateTab = tab

            } else {
                val itemList = mutableListOf<AdapterType>()

                withContext(Dispatchers.Default) {
                    val relativeTimeHelper = RelativeTimeHelper(
                        requireContext(),
                        System.currentTimeMillis()
                    )

                    postModel.list.map {

//                        val refPostTxId = it.post.referencedPost
//                        val refPost = refPostTxId?.let { txId ->
//                            it.referenced[txId]
//                        }
//
//                        val postLabel = relativeTimeHelper.getLabel(it.post.millisCreatedAt)
//                        val refPostLabel = refPost?.millisCreatedAt?.let { millis ->
//                            relativeTimeHelper.getLabel(millis)
//                        }
//                        val doubleRefPostLabel = refPost?.referencedPost?.let { txId ->
//                            it.referenced[txId]?.let { p ->
//                                relativeTimeHelper.getLabel(p.millisCreatedAt)
//                            }
//                        }
//
//                        ATTwetchPostItem(
//                            it.post.id,
//                            it,
//                            postLabel,
//                            refPostLabel,
//                            doubleRefPostLabel
//                        )
                        postItemHelper.createItem(it, relativeTimeHelper)
                    }.toCollection(itemList)
                }

                Timber.d("new post-list; size: ${itemList.size}; page:${postModel.page} hasMorePages:${postModel.hasMorePages} isRefreshing:$isRefreshing")

                if (postModel.hasMorePages) {
                    itemList.add(loadingItem)
                }


                itemList.add(0, topHeightItem)

                adapter.setItems(itemList)

                if (tab != lastRestoredScrollStateTab) {

                    val cachedState = when (tab) {
                        TwetchUserProfileUseCase.UserProfileTab.LATEST -> helper.cachedLatestState
                        TwetchUserProfileUseCase.UserProfileTab.REPLIES -> helper.cachedRepliesState
                        TwetchUserProfileUseCase.UserProfileTab.LIKES -> helper.cachedLikesState
                        else -> throw RuntimeException()
                    }


                    layoutManager.onRestoreInstanceState(cachedState)
                    lastRestoredScrollStateTab = tab
                }
            }
        }
    }

    private fun isRefreshing(): Boolean {
        val refreshingState = helper.metaState.value

        return when (helper.content.value.activeTab) {
            TwetchUserProfileUseCase.UserProfileTab.PROFILE -> {
                refreshingState.profileBaseRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.LATEST -> {
                refreshingState.latestRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.REPLIES -> {
                refreshingState.repliesRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.LIKES -> {
                refreshingState.likesRefreshing
            }
        }
    }

    private fun refreshLoadingIndicatorState() {
        val activeTab = helper.content.value.activeTab
        val refreshingState = helper.metaState.value

        when (activeTab) {
            TwetchUserProfileUseCase.UserProfileTab.PROFILE -> {
                if (swipeRefresh.isRefreshing == refreshingState.profileBaseRefreshing) return
                swipeRefresh.isRefreshing = refreshingState.profileBaseRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.LATEST -> {
                if (swipeRefresh.isRefreshing == refreshingState.latestRefreshing) return
                swipeRefresh.isRefreshing = refreshingState.latestRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.REPLIES -> {
                if (swipeRefresh.isRefreshing == refreshingState.repliesRefreshing) return
                swipeRefresh.isRefreshing = refreshingState.repliesRefreshing
            }
            TwetchUserProfileUseCase.UserProfileTab.LIKES -> {
                if (swipeRefresh.isRefreshing == refreshingState.likesRefreshing) return
                swipeRefresh.isRefreshing = refreshingState.likesRefreshing
            }
        }
    }


    private fun cacheCurrentScrollState() {
        when (helper.content.value.activeTab) {
            TwetchUserProfileUseCase.UserProfileTab.PROFILE -> {
                Timber.d("profileContent scroll-state saved")
                helper.cachedProfileBaseState = layoutManager.onSaveInstanceState()
            }
            TwetchUserProfileUseCase.UserProfileTab.LATEST -> {
                Timber.d("latestContent scroll-state saved")
                helper.cachedLatestState = layoutManager.onSaveInstanceState()
            }
            TwetchUserProfileUseCase.UserProfileTab.REPLIES -> {
                Timber.d("repliesContent scroll-state saved")
                helper.cachedRepliesState = layoutManager.onSaveInstanceState()
            }
            TwetchUserProfileUseCase.UserProfileTab.LIKES -> {
                Timber.d("likesContent scroll-state saved")
                helper.cachedLikesState = layoutManager.onSaveInstanceState()
            }
        }
    }


    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        tabScrollView.scrollX = helper.cachedTabsScrolledAmount
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!wasCreatedOnce) return

        helper.cachedTabsScrolledAmount = tabScrollView.scrollX

        recycler.stopScroll()
        cacheCurrentScrollState()
    }


//    override fun onStart() {
//        super.onStart()
//
//        requireView().post {
//            requireView().doOnPreDraw {
//                tabScrollView.scrollX = helper.cachedTabsScrolledAmount
//            }
//        }
//
//    }

    override fun onStop() {
        lastAppliedProfileBase = null
        lastAppliedList = null
        lastAppliedTab = null
        super.onStop()
    }










    private suspend fun awaitRecyclerAnimationsFinish(fromListChange: Boolean = true) {
        if (fromListChange) {
            swapOutRefreshingItemJob?.cancel()
            swapOutRefreshingItemJob = null
        }

        recycler.itemAnimator?.let { animator ->
            //making sure that the any previous animation get completed
            if (!animator.isRunning) {
                return@let
            }

            suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                animator.isRunning { cancellableContinuation.resume(Unit) }
            }
        }
    }







    var swapOutRefreshingItemJob: Job? = null

    private fun swapOutRefreshingItem() {
        if (swapOutRefreshingItemJob?.isActive == true) return

        swapOutRefreshingItemJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            awaitRecyclerAnimationsFinish(false)

            val items = adapter.getCopyOfItems()

            var loadingItemPos = -1

            val countToCheck = min(5, items.size)
            for (i in 0 until countToCheck) {
                val item = items[items.lastIndex - i]
                if (item is ATContentLoading0) {
                    loadingItemPos = items.lastIndex - i
                    break
                }
            }

            ensureActive()

            if (loadingItemPos == -1) {
                Timber.d(">>> loadingItemPost not found")
                helper.requestNextPage()
                return@launchWhenStarted
            }

            adapter.swapOutItem(loadingItemPos, altLoadingItem)

            awaitRecyclerAnimationsFinish(false)
            helper.requestNextPage()
        }
    }

}