package app.bitcoin.baemail.twetch.ui

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.navigation.NavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.util.Event
import app.bitcoin.baemail.core.presentation.view.VideoClipManager
import kotlinx.coroutines.flow.StateFlow
import java.util.*

open class TwetchPostListener(
    protected val context: Context,
    private val navController: NavController,
    private val actionHelper: TwetchActionHelper,
    private val videoClipManager: VideoClipManager,
    private val userProfileDestinationActionId: Int,
    private val fullImageDestinationActionId: Int,
    private val postDetailsDestinationActionId: Int,
    private val branchOptionsDestinationActionId: Int,
    private val videoClipDestinationActionId: Int,
    private val coinFlipChannel: StateFlow<Event<CoinFlipModel>?>
) : BaseListAdapter.ATTwetchPostListener {

    val cbm: ClipboardManager by lazy {
        context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }


    override fun onContentClicked(holder: PostHolder, ofPost: PostModel) {
        val args = Bundle().also {
            it.putString(TwetchPostDetailsFragment.KEY_POST_TX_ID, ofPost.txId)

        }
        navController.navigate(postDetailsDestinationActionId, args)
    }

    override fun onUnloadedPostClicked(holder: PostHolder, ofPost: PostModel, txId: String) {
        val args = Bundle().also {
            it.putString(TwetchPostDetailsFragment.KEY_POST_TX_ID, txId)

        }
        navController.navigate(postDetailsDestinationActionId, args)
    }

    override fun onImageClicked(holder: PostHolder, allImageUrlList: List<String>) {
        val args = Bundle().also {
            it.putStringArrayList(TwetchChatFullImageFragment.KEY_IMAGE_URL, ArrayList(allImageUrlList))

        }
        navController.navigate(fullImageDestinationActionId, args)
    }

    override fun onProfileClicked(holder: PostHolder, ofPost: PostModel) {
        val args = Bundle().also {
            val userId = ofPost.user.id

            it.putString(TwetchUserProfileFragment.KEY_USER_ID, userId)
        }
        navController.navigate(userProfileDestinationActionId, args)
    }

    override fun onBranchedLabelClicked(holder: PostHolder) {
        val args = Bundle().also {
            it.putString(TwetchUserProfileFragment.KEY_USER_ID, holder.post.user.id)
        }
        navController.navigate(userProfileDestinationActionId, args)
    }

    override fun onLikeClicked(holder: PostHolder, ofPost: PostModel): Boolean {
        val parentId = if (holder.post.txId == ofPost.txId) null else holder.post.txId
        return actionHelper.executeLikeOfPost(ofPost.txId, parentId)
    }

    override fun onCommentClicked(holder: PostHolder, ofPost: PostModel) {
        actionHelper.startReply(ofPost)
    }

    override fun onBranchClicked(holder: PostHolder, ofPost: PostModel) {
        val args = Bundle().also {
            it.putString(TwetchBranchOptionsFragment.KEY_POST_TX_ID, ofPost.txId)

            val parentId = if (holder.post.txId == ofPost.txId) null else holder.post.txId
            it.putString(TwetchBranchOptionsFragment.KEY_POST_PARENT_ID, parentId)
        }
        navController.navigate(branchOptionsDestinationActionId, args)
    }

    override fun onCopyLinkClicked(holder: PostHolder, ofPost: PostModel) {
        cbm.let { manager ->
            val txId = ofPost.txId

            val url = "https://twetch.com/t/$txId"
            val clip = ClipData.newPlainText("Twetch post URL", url)

            manager.setPrimaryClip(clip)
            Toast.makeText(
                context,
                context.getString(R.string.twetch_url_copied_info),
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    override fun onCreatedLabelClicked(holder: PostHolder, ofPost: PostModel) {
        val twtId = ofPost.txId


        val url = "https://bitcoinfiles.org/tx/$twtId"
        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(context, Uri.parse(url))
    }

    override fun onTweetClicked(holder: PostHolder, ofPost: PostModel) {
        val twtId = ofPost.tweet!!.twtId
        val screenName = ofPost.tweet.user.screenName

        val url = "https://twitter.com/$screenName/status/$twtId"
        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(context, Uri.parse(url))
    }

    override fun onUserIdBadgeClicked(holder: PostHolder, userId: String) {
        val args = Bundle().also {
            it.putString(TwetchUserProfileFragment.KEY_USER_ID, userId)
        }
        navController.navigate(userProfileDestinationActionId, args)
    }

    override fun getVideoClipManager(): VideoClipManager {
        return videoClipManager
    }

    override fun onVideoClipClicked(holder: PostHolder, videoTxId: String) {
        val args = Bundle().also {
            it.putString(TwetchVideoClipFragment.KEY_TX_ID, videoTxId)

        }
        navController.navigate(videoClipDestinationActionId, args)
    }

    override fun getCoinFlipSuccessSignal(): StateFlow<Event<CoinFlipModel>?> {
        return coinFlipChannel
    }
}