package app.bitcoin.baemail.twetch.ui

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import org.threeten.bp.ZonedDateTime
import timber.log.Timber
import java.util.regex.Pattern
import kotlin.collections.*

class TwetchPostCache {
    private val dummyPostCache = HashMap<String, PostModel>()

    private val _updatedState = MutableStateFlow(0)
    val updatedState: StateFlow<Int>
        get() = _updatedState

    fun getPost(activeUserId: String, postTxId: String): PostModel? {
        return dummyPostCache[activeUserId + postTxId]?.copy()
    }

    fun getPostHolder(activeUserId: String, postTxId: String): PostHolder? {
        val post = getPost(activeUserId, postTxId) ?: return null

        val refMap = mutableMapOf<String, PostModel>()

        post.referencedPost?.let { refTxId ->
            val refP = getPost(activeUserId, refTxId)

            refP?.let { refPost ->
                refMap[refPost.txId] = refPost
            }

            refP?.referencedPost?.let { doubleRefTxId ->
                getPost(activeUserId, doubleRefTxId)?.let { doubleRefP ->
                    refMap[doubleRefP.txId] = doubleRefP
                }
            }
        }

        return PostHolder(
            post,
            refMap
        )
    }

    fun setPost(activeUserId: String, post: PostModel) {
        dummyPostCache[activeUserId + post.txId] = post
    }

    fun notifyUpdated() {
        _updatedState.value = 1 + _updatedState.value
    }

    fun clear() {
        dummyPostCache.clear()
        _updatedState.value = 1 + _updatedState.value
    }
}





data class PostHolder(
    val post: PostModel,
    val referenced: Map<String, PostModel>
) {
    fun hasMissingRefPosts(): Boolean {
       return post.referencedPost?.let { txId ->
            val refPost = referenced[txId]

            val missingDoubleRefPost = refPost?.referencedPost?.let { txId1 ->
                referenced[txId1] == null
            } ?: false

            missingDoubleRefPost || refPost == null

        } ?: false
    }
}

data class PostModel(
    val id: String,
    val txId: String,
    val content: String,
    val isBranch: Boolean,
    val isBranchWithComment: Boolean,
    var numLikes: Int,
    var numBranches: Int,
    val numComments: Int,
    var youLiked: Int,
    var youBranched: Int,
    val millisCreatedAt: Long,
    val tweet: TweetModel?,
    val parents: List<PostParent>,
    val files: List<String>,
    val mediaFiles: List<String>,
    val bitcoinFiles: List<String>,
    val bProtocolImageList: List<String>,
    val referencedPost: String?,
    val user: PostUser,
    val millisRefreshed: Long,
    val activeUserId: String,
    val isRemoved: Boolean,
    val data: String
) {

    fun getFullImageUrlList(): List<String> {
        val added = HashSet<String>()
        val fullImageUrlList = mutableListOf<String>()
        files.mapNotNull {
            if (added.contains(it)) return@mapNotNull null
            added.add(it)
            "https://dogefiles.twetch.app/${it}"
        }.toCollection(fullImageUrlList)

        mediaFiles.mapNotNull {
            if (added.contains(it)) return@mapNotNull null
            added.add(it)
            "https://cimg.twetch.com/${it}"
        }.toCollection(fullImageUrlList)

        bitcoinFiles.mapNotNull {
            if (added.contains(it)) return@mapNotNull null
            added.add(it)
            "https://doge.bitcoinfiles.org/${it}"
        }.toCollection(fullImageUrlList)

        bProtocolImageList.mapNotNull {
            if (added.contains(it)) return@mapNotNull null
            added.add(it)
            "https://dogefiles.twetch.app/${it}"
        }.toCollection(fullImageUrlList)

        return fullImageUrlList
    }

    fun getVideoFileTxId(): String? {
        if (files.isEmpty()) return null
        return files.first()
    }

    fun getVideoFileUrl(): String? {
        if (files.isEmpty()) return null
        return "https://ink.twetch.com/dogefiles/${files.first()}"
    }

    fun onLikedByMe() {
        numLikes++
        youLiked++
    }

    fun onLikingError() {
        numLikes--
        youLiked--
    }


    fun onBranchedByMe() {
        numBranches++
        youBranched++
    }

    fun onBranchingError() {
        numBranches--
        youBranched--
    }




    companion object {

        val TWETCH_POST_URL_START = "https://twetch.app/t/"
        val TWETCH_POST_URL_START0 = "https://twetch.com/t/"
        val PATTERN_TWETCH_POST_REF = Pattern
            .compile("https://twetch.(app|com)/t/[a-zA-Z_0-9]+")


        private const val BITCOIN_FILES_URL_START = "https://bitcoinfiles.org/t/"
        private const val BITCOIN_FILES_URL_ALT_START = "https://media.bitcoinfiles.org/"
        private val PATTERN_BITCOIN_FILES_REF = Pattern.compile("(https://bitcoinfiles.org/t/|https://media.bitcoinfiles.org/)[a-zA-Z_0-9]+")



        fun findReferencedPosts(content: String): List<String> {
            val matcher = PATTERN_TWETCH_POST_REF.matcher(content)

            val found = ArrayList<String>()
            while (matcher.find()) {
                val foundSequence = matcher.group()
                found.add(foundSequence
                    .replace(TWETCH_POST_URL_START, "")
                    .replace(TWETCH_POST_URL_START0, ""))
            }

            return found
        }

        fun parsePost(gson: Gson, jsonPost: JsonObject, activeUserId: String, millisNow: Long): PostModel {
            //Timber.d(">>>> $jsonPost")
            val id = jsonPost.get("id").asString
            val txId = jsonPost.get("transaction").asString

            var referencedPost: String? = null

            var bContent = if (jsonPost.has("bContent")) {
                val bContent = jsonPost.get("bContent")
                if (bContent.isJsonNull) {
                    ""
                } else {
                    val content = bContent.asString
                    val foundRefs = findReferencedPosts(content)
                    if (foundRefs.isNotEmpty()) {
                        referencedPost = foundRefs[0]
                    }

                    if (referencedPost != null) {
                        content
                            .replace(TWETCH_POST_URL_START + referencedPost, "")
                            .replace(TWETCH_POST_URL_START0 + referencedPost, "")
                            .trim()
                    } else {
                        content.trim()
                    }

                }
            } else {
                ""
            }

            val isBranch = referencedPost != null
            val isBranchWithComment = referencedPost != null && bContent.isNotBlank()




            val numLikes = jsonPost.get("numLikes").asString.toInt()
            val numBranches = jsonPost.get("numBranches").asString.toInt()
            //val branchesJson = jsonPost.getAsJsonObject("branches")
            //val numBranches = branchesJson.get("totalCount").asString.toInt()

            val youLiked = if (jsonPost.get("youLiked").asBoolean) 1 else 0
            val youBranched = if (jsonPost.get("youBranched").asBoolean) 1 else 0

            val numComments = jsonPost.getAsJsonObject("postsByReplyPostId")
                .get("totalCount").asInt








            val createdAt = jsonPost.get("createdAt").asString
            val millisCreatedAt = createdAt.let {
                try {
                    ZonedDateTime.parse(it).toInstant().toEpochMilli()
                } catch (e: Exception) {
                    Timber.e(e)
                    -1
                }
            }



            val user = parseUser(jsonPost.getAsJsonObject("userByUserId"))
            val tweet = if (jsonPost.has("mapTwdata")
                && !jsonPost.get("mapTwdata").isJsonNull) {
                val serializedTweetModel = jsonPost.get("mapTwdata").asString
                val tweetJson = gson.fromJson(serializedTweetModel, JsonObject::class.java)
                parseTweet(tweetJson)
            } else null

            tweet?.also {
                //remove the url for the unfurled tweet
                val url = "https://twitter.com/${it.user.screenName}/status/${it.twtId}"
                bContent = bContent.replace(url, "")
            }


            val contentBitcoinFilesExtracted = extractBitcoinFilesLinks(bContent)



            val parentsList = mutableListOf<PostParent>()
            if (jsonPost.has("parents")) {
                val parentsArray = jsonPost.getAsJsonObject("parents")
                    .getAsJsonArray("nodes")
                parentsArray.forEach { item ->
                    parentsList.add(parsePostParent(item.asJsonObject))
                }
            }


            val filesList = mutableListOf<String>()
            if (jsonPost.has("files") && !jsonPost.get("files").isJsonNull) {
                val serializedFilesJson = jsonPost.get("files").asString
                try {
                    val filesJsonArray = gson.fromJson(serializedFilesJson, JsonArray::class.java)
                    filesJsonArray.forEach { item ->
                        filesList.add(item.asString)
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }

            val mediaFilesList = mutableListOf<String>()
            if (jsonPost.has("mediaByPostId") && !jsonPost.get("mediaByPostId").isJsonNull) {
                try {
                    val mediaJsonArray = jsonPost.getAsJsonObject("mediaByPostId")
                        .getAsJsonArray("nodes")
                    mediaJsonArray.forEach { item ->
                        val fileName = item.asJsonObject.get("filename")
                        mediaFilesList.add(fileName.asString)
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }

            //b-protocol image
            val bProtocolImageList: List<String> = if (jsonPost.has("bContentType")
                && !jsonPost.get("bContentType").isJsonNull
            ) {
                try {
                    val typeValue = jsonPost.get("bContentType").asString;
                    if ("image/jpeg" == typeValue) {
                        listOf(txId)
                    } else {
                        if (typeValue != "text/plain") Timber.d("encountered type: $typeValue ... data: $jsonPost")
                        emptyList()
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                    emptyList()
                }
            } else emptyList()


            val isRemoved = jsonPost.has("bContent") && jsonPost.get("bContent").isJsonNull
                    && jsonPost.has("bContentType") && jsonPost.get("bContentType").isJsonNull

            return PostModel(
                id,
                txId,
                contentBitcoinFilesExtracted.second,
                isBranch,
                isBranchWithComment,
                numLikes,
                numBranches,
                numComments,
                youLiked,
                youBranched,
                millisCreatedAt,
                tweet,
                parentsList,
                filesList,
                mediaFilesList,
                contentBitcoinFilesExtracted.first,
                bProtocolImageList,
                referencedPost,
                user,
                millisNow,
                activeUserId,
                isRemoved,
                jsonPost.toString()
            )
        }

        private fun extractBitcoinFilesLinks(content: String): Pair<List<String>, String> {
            val urlMatcher = PATTERN_BITCOIN_FILES_REF.matcher(content)

            val found = ArrayList<String>()
            while (urlMatcher.find()) {
                val foundSequence = urlMatcher.group() ?: continue
                found.add(foundSequence)
            }

            var updatedContent = content
            found.forEach {
                updatedContent = updatedContent.replace(it, "")
            }
            updatedContent = updatedContent.trim()


            val strippedUrls = mutableListOf<String>()
            found.map {
                it.replace(BITCOIN_FILES_URL_START, "")
                    .replace(BITCOIN_FILES_URL_ALT_START, "")
            }.toCollection(strippedUrls)

            return strippedUrls to updatedContent
        }

        private fun parseUser(json: JsonObject): PostUser {
            val id = json.get("id").asString
            val name = if (json.has("name") && !json.get("name").isJsonNull) {
                json.get("name").asString
            } else {
                "@$id"
            }
            val icon = if (json.has("icon")) {
                val postJson = json.get("icon")
                if (postJson.isJsonNull) {
                    null
                } else {
                    postJson.asString
                }
            } else {
                null
            }

            return PostUser(
                id,
                name,
                icon
            )
        }

        private fun parseTweet(json: JsonObject): TweetModel {
            val createdAt = json.get("created_at").asString
            val twtId = json.get("twt_id").asString
            val text = json.get("text").asString
            val mediaList = mutableListOf<String>()
            if (json.has("media") && !json.get("media").isJsonNull) {
                json.getAsJsonArray("media").forEach { item ->
                    mediaList.add(item.asString)
                }
            }
            val curator = if (json.has("curator")
                && !json.get("curator").isJsonNull) {
                json.get("curator").asString
            } else null
            val userJson = json.getAsJsonObject("user")
            val userName = userJson.get("name").asString
            val userScreenName = userJson.get("screen_name").asString
            val userCreatedAt = userJson.get("created_at").asString
            val userTwtId = userJson.get("twt_id").asString
            val userImageUrl = if (userJson.has("profile_image_url")
                && !userJson.get("profile_image_url").isJsonNull) {
                userJson.get("profile_image_url").asString
            } else null

            val user = TweetUser(
                userName,
                userScreenName,
                userCreatedAt,
                userTwtId,
                userImageUrl
            )

            return TweetModel(
                createdAt,
                twtId,
                text,
                mediaList,
                curator,
                user
            )
        }

        private fun parsePostParent(json: JsonObject): PostParent {
            val txId = json.get("transaction").asString
            val user = parseUser(json.getAsJsonObject("userByUserId"))

            return PostParent(txId, user)
        }


    }

}

data class PostUser(
    val id: String,
    val name: String,
    val icon: String?
)

data class PostParent(
    val transaction: String,
    val user: PostUser
)

data class TweetModel(
    val createdAt: String,
    val twtId: String,
    val text: String,
    val media: List<String>,
    val curator: String?,
    val user: TweetUser
)

data class TweetUser(
    val name: String,
    val screenName: String,
    val createdAt: String,
    val twtId: String,
    val imageUrl: String?
)