package app.bitcoin.baemail.twetch.ui

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.dropbox.android.external.store4.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapLatest
import timber.log.Timber

class TwetchAuthStore(
    private val twetchAuthPrefs: DataStore<Preferences>,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val gson: Gson
) {

    private val authStore: Store<AuthKey, AuthInfo> by lazy {
        StoreBuilder.from(
            fetcher = Fetcher.ofFlow { filterKey ->
                flow {
                    val current: AuthInfo? = if (!filterKey.isForced) {
                        getFromSourceOfTruth(filterKey)
                    } else null

                    val finalIsForced = current == null



                    val info = nodeRuntimeRepository.twetchAuth(
                        filterKey.paymail,
                        finalIsForced
                    )



                    info?.let { gotInfo ->
                        val finalInfo = if (gotInfo.keyAdded && gotInfo.profile == null) {
                            gotInfo.copy(profile = current?.profile)
                        } else gotInfo

                        emit(finalInfo)
                    } ?: throw RuntimeException()

                }
            },
            sourceOfTruth = SourceOfTruth.of(
                reader = ::queryByFilter,
                writer = ::insertAll,
                delete = ::clearByKey,
                deleteAll = ::deleteAll
            )

        ).build()
    }




    private fun queryByFilter(key: AuthKey): Flow<AuthInfo?> {
        return twetchAuthPrefs.data.mapLatest { prefs ->
            val serialisedAuth = prefs[stringPreferencesKey("auth_${key.paymail}")]
                    ?: return@mapLatest null

            val jsonAuth: JsonObject = gson.fromJson(serialisedAuth, JsonObject::class.java)
            AuthInfo.parse(jsonAuth)
        }
    }

    private suspend fun deleteAll() {
        twetchAuthPrefs.edit { prefs ->
            prefs.asMap().keys.forEach {
                Timber.d("removing key: $it")
                prefs.remove(it)
            }

        }
    }

    private suspend fun insertAll(key: AuthKey, info: AuthInfo) {
        twetchAuthPrefs.edit { prefs ->
            prefs[stringPreferencesKey("auth_${key.paymail}")] = info.serialize().toString()
        }
    }

    private suspend fun clearByKey(key: AuthKey) {
        twetchAuthPrefs.edit { prefs ->
            prefs.remove(stringPreferencesKey("auth_${key.paymail}"))
        }
    }






    /////////////////////////////////////////////////////////////////

    suspend fun getFromSourceOfTruth(key: AuthKey): AuthInfo? {
        return queryByFilter(key).first()
    }


    fun getFlowForKey(
            key: AuthKey,
            refresh: Boolean = true
    ): Flow<StoreResponse<AuthInfo>> {
        return authStore.stream(StoreRequest.cached(key, refresh))
    }




    data class AuthKey(
            val paymail: String,
            val isForced: Boolean
    )


    data class AuthInfo(
            val token: String,
            val keyAdded: Boolean,
            val profile: TwetchProfile?,
            val signingAddress: SigningAddress?
    ) {

        fun serialize(): JsonObject {
            val json = JsonObject()

            json.addProperty("token", token)
            json.addProperty("keyAdded", keyAdded)

            if (!keyAdded) {
                val signingAddressJson = JsonObject()
                val signingAddress = signingAddress!!
                signingAddressJson.addProperty("address", signingAddress.address)
                signingAddressJson.addProperty("message", signingAddress.message)
                signingAddressJson.addProperty("signature", signingAddress.signature)

                json.add("signingAddress", signingAddressJson)
            } else {
                val profile = this.profile!!
                val profileJson = JsonObject()
                profileJson.addProperty("id", profile.id)
                profileJson.addProperty("name", profile.name)
                profileJson.addProperty("publicKey", profile.publicKey)
                profileJson.addProperty("description", profile.description)
                profileJson.addProperty("dmConversationId", profile.dmConversationId)
                profileJson.addProperty("followerCount", profile.followerCount)
                profileJson.addProperty("followingCount", profile.followingCount)
                profileJson.addProperty("icon", profile.icon)
                profileJson.addProperty("invitesRemaining", profile.invitesRemaining)
                profileJson.addProperty("conversationId", profile.myConversationId)
                profileJson.addProperty("notificationsCount", profile.notificationsCount)
                profileJson.addProperty("numPosts", profile.numPosts)
                profileJson.addProperty("numLikes", profile.numLikes)
                profileJson.addProperty("numReferrals", profile.numReferrals)
                profileJson.addProperty("paidNotificationsCount", profile.paidNotificationsCount)
                profileJson.addProperty("unreadMessagesCount", profile.unreadMessagesCount)

                val dataJson = JsonObject()
                dataJson.add("me", profileJson)

                json.add("data", dataJson)
            }

            return json
        }

        companion object {

            fun parse(serialized: JsonObject): AuthInfo {

                val token = serialized.get("token").asString
                val keyAdded = serialized.get("keyAdded").asBoolean

                var profile: TwetchProfile? = null


                if (keyAdded && serialized.has("data")
                    && !serialized.get("data").isJsonNull
                ) {

                    val dataJson = serialized.getAsJsonObject("data")

                    val meJson = dataJson.getAsJsonObject("me")

                    val conversationId = meJson.get("myConversationId")?.let {
                        if (it.isJsonNull) {
                            null
                        } else {
                            it.asString
                        }
                    }

                    val description = meJson.get("description")?.let {
                        if (it.isJsonNull) {
                            null
                        } else {
                            it.asString
                        }
                    }

                    val dmConversationId = meJson.get("dmConversationId")?.let {
                        if (it.isJsonNull) {
                            null
                        } else {
                            it.asString
                        }
                    }

                    val icon = meJson.get("icon")?.let {
                        if (it.isJsonNull) {
                            null
                        } else {
                            it.asString
                        }
                    }

                    profile = TwetchProfile(
                        meJson.get("id").asInt,
                        meJson.get("name").asString,
                        meJson.get("publicKey").asString,
                        description,
                        dmConversationId,
                        meJson.get("followerCount").asInt,
                        meJson.get("followingCount").asInt,
                        icon,
                        meJson.get("invitesRemaining").asInt,
                        conversationId,
                        meJson.get("notificationsCount").asInt,
                        meJson.get("numPosts").asInt,
                        meJson.get("numLikes").asInt,
                        meJson.get("numReferrals").asInt,
                        meJson.get("paidNotificationsCount").asInt,
                        meJson.get("unreadMessagesCount").asInt
                    )

                }


                var signingAddress: SigningAddress? = null
                if (!keyAdded) {
                    val signingAddressJson = serialized.getAsJsonObject("signingAddress")
                    signingAddress = SigningAddress(
                            signingAddressJson.get("address").asString,
                            signingAddressJson.get("message").asString,
                            signingAddressJson.get("signature").asString,
                    )
                }


                return AuthInfo(
                        token,
                        keyAdded,
                        profile,
                        signingAddress
                )
            }

        }
    }

    data class TwetchProfile(
            val id: Int,
            val name: String,
            val publicKey: String,
            val description: String?,
            val dmConversationId: String?,
            val followerCount: Int,
            val followingCount: Int,
            val icon: String?,
            val invitesRemaining: Int,
            val myConversationId: String?,
            val notificationsCount: Int,
            val numPosts: Int,
            val numLikes: Int,
            val numReferrals: Int,
            val paidNotificationsCount: Int,
            val unreadMessagesCount: Int
    )

    data class SigningAddress(
            val address: String,
            val message: String,
            val signature: String
    )
}