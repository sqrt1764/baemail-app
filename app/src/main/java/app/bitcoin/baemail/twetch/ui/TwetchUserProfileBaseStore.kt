package app.bitcoin.baemail.twetch.ui

import android.content.Context
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.twetch.db.TwetchDatabase
import app.bitcoin.baemail.twetch.db.entity.UserProfileBaseModel
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.dropbox.android.external.store4.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import timber.log.Timber

class TwetchUserProfileBaseStore(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val twetchDatabase: TwetchDatabase,
    private val gson: Gson
) {


    private val userProfileBaseStore: Store<Key, UserProfileBaseModel> = StoreBuilder.from(
        fetcher = Fetcher.ofFlow { filterKey ->
            Timber.d("userProfileBaseStore#fetcherOfFlow $filterKey")
            return@ofFlow queryServer(filterKey)
        },
        sourceOfTruth = SourceOfTruth.of(
            reader = ::queryByFilter,
            writer = ::insert,
            delete = ::clearByKey,
            deleteAll = ::deleteAll
        )

    ).build()





    private fun queryServer(key: Key): Flow<UserProfileBaseModel> {
        return flow {

            val activeUserId = key.activeUserId
            val userId = key.userId

            val result = nodeRuntimeRepository.runAsyncScript("""
                let millisStart = new Date().getTime();
                
                await context.twetch.ensureAuthHasCompleted();
                
                let millisPostAuth = new Date().getTime();
                
                let queryResult = await context.twetch.instance.query(`
                    {
                        userById(id: "$userId") {
                            id
                            followerCount
                            followingCount
                            icon
                            name
                            publicKey
                            description
                            dmConversationId
                            followsYou: followersByUserId(filter: {followerUserId: {equalTo: "$activeUserId"}, userId: {notEqualTo: "$activeUserId"}}) {
                                totalCount
                            }
                            youFollow: followersByFollowerUserId(condition: {userId: "$activeUserId"}) {
                                totalCount
                                nodes {
                                    nodeId
                                    id
                                    userId
                                    followerUserId
                                    transaction
                                    followerUserId
                                    createdAt
                                }
                            }
                            postsByUserId {
                                totalCount
                            }
                        }
                    }
                `);
                
                queryResult.userById.followsYou = queryResult.userById.followsYou.totalCount != 0
                queryResult.userById.youFollow = queryResult.userById.youFollow.totalCount != 0
                
                let millisPostRequest = new Date().getTime();
                console.log("time spent awaiting twetch-auth:" + (millisPostAuth - millisStart) + "ms; time spent on user-profile-request:" + (millisPostRequest - millisPostAuth) + "ms");
                
                
                return JSON.stringify(queryResult);
            """.trimIndent())

            if (result is RunScript.Response.Success) {

                val parsed = parseUserProfileQueryResponse(result.content, activeUserId)

                Timber.d("queried profile: $parsed")

                emit(parsed)

            } else {
                result as RunScript.Response.Fail
                throw result.throwable
            }
        }
    }

    private fun queryByFilter(key: Key): Flow<UserProfileBaseModel?> {
        return twetchDatabase.userProfileBaseDao().getFlow(key.activeUserId, key.userId)
    }

    private suspend fun insert(key: Key, info: UserProfileBaseModel) {
        twetchDatabase.userProfileBaseDao().insert(info)
    }

    private suspend fun clearByKey(key: Key) {
        twetchDatabase.userProfileBaseDao().delete(key.activeUserId, key.userId)
    }

    private suspend fun deleteAll() {
        twetchDatabase.userProfileBaseDao().deleteAll()
    }



    /////////////////////////////////////////////////////////////////

    suspend fun getFromSourceOfTruth(key: Key): UserProfileBaseModel? {
        return queryByFilter(key).first()
    }

    fun getFlow(key: Key): Flow<StoreResponse<UserProfileBaseModel>> {
        return userProfileBaseStore.stream(StoreRequest.cached(key, true))
    }


    /////////////////////////////////////////////////////////////////

    private fun parseUserProfileQueryResponse(
        content: String,
        activeUserId: String
    ): UserProfileBaseModel {
        val responseJson = gson.fromJson(content, JsonObject::class.java)
        val userByIdJson = responseJson.getAsJsonObject("userById")

        val id = userByIdJson.get("id").asString
        val followerCount = userByIdJson.get("followerCount").asString.toInt()
        val followingCount = userByIdJson.get("followingCount").asString.toInt()
        val icon = if (
            userByIdJson.has("icon") &&
            !userByIdJson.get("icon").isJsonNull
        ) {
            userByIdJson.get("icon").asString
        } else null
        val name = if (userByIdJson.has("name") &&
            !userByIdJson.get("name").isJsonNull) {
            userByIdJson.get("name").asString
        } else "-"
        val publicKey = if (
            userByIdJson.has("publicKey") &&
            !userByIdJson.get("publicKey").isJsonNull
        ) {
            userByIdJson.get("publicKey").asString
        } else null

        val description = if (
            userByIdJson.has("description") &&
            !userByIdJson.get("description").isJsonNull
        ) {
            userByIdJson.get("description").asString
        } else null

        val dmConversationId = if (
            userByIdJson.has("dmConversationId") &&
            !userByIdJson.get("dmConversationId").isJsonNull
        ) {
            userByIdJson.get("dmConversationId").asString
        } else null
        val followsYou = userByIdJson.get("followsYou").asBoolean
        val youFollow = userByIdJson.get("youFollow").asBoolean
        val postsByUserId = userByIdJson.get("postsByUserId").let { userPostJson ->
            userPostJson.asJsonObject.get("totalCount").asInt
        }

        return UserProfileBaseModel(
            activeUserId,
            id,
            followerCount,
            followingCount,
            icon,
            name,
            publicKey,
            description,
            dmConversationId,
            followsYou,
            youFollow,
            postsByUserId
        )
    }







    data class Key(
        val activeUserId: String,
        val userId: String
    )


}