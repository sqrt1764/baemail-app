package app.bitcoin.baemail.twetch.ui

import android.os.Parcelable
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber

@FlowPreview
@ExperimentalCoroutinesApi
class TwetchSearchUseCase(
    private val activeUserId: String,
    private val coroutineUtil: CoroutineUtil,
    private val postListStore: TwetchPostListStore
) {

    companion object {
        const val PAGE_NOT_LOADED = -1
    }

    private var isSetup = false

    private val _content = MutableStateFlow(Model(
        null,
        null,
        null
    ))
    val content: StateFlow<Model>
        get() {
            ensureSetup()
            return _content
        }



    private val _metaState = MutableStateFlow(MetaState(
        false,
        false
    ))
    val metaState: StateFlow<MetaState>
        get() = _metaState



    private val internalRefresh = MutableStateFlow(0)



    private var postListDataObservationJob: Job? = null
    private var postListRefreshingObservationJob: Job? = null
    private var bufferListQueryJob: Job? = null




    var cachedListState: Parcelable? = null




    private val listHelper = TwetchPostListHelper(
        activeUserId,
        null,
        ListType.SEARCH,
        coroutineUtil,
        postListStore,
        false
    )


    fun onRefreshRequested() {
        Timber.d("onRefreshRequested")
        listHelper.refreshFromStart()
    }

    private fun ensureSetup() {
        if (isSetup) {
            return
        }
        isSetup = true

        listHelper.ensureSetup()

        postListDataObservationJob?.cancel()
        postListDataObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            internalRefresh.combine(listHelper.data) { _, data ->
                data
            }.collectLatest { data ->
                delay(15)
                val finalData = if (listHelper.query.isNullOrBlank()) {
                    null
                } else data

                _content.value = _content.value.copy(
                    list = finalData
                )
            }
        }

        postListRefreshingObservationJob?.cancel()
        postListRefreshingObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            internalRefresh.combine(listHelper.refreshing) { _, refreshing ->
                refreshing
            }.collect { refreshing ->
                val finalRefreshing = if (listHelper.query.isNullOrBlank()) {
                    false
                } else refreshing

                _metaState.value = _metaState.value.copy(
                    refreshing = finalRefreshing
                )
            }
        }
    }


    fun requestNextPage() {
        if (_content.value.query.isNullOrBlank()) {
            Timber.d("requestNextPage() dropped because the query is NULL-or-blank")
            return
        }

        listHelper.userRequestedNextPage()
    }



    fun cleanup() {
        isSetup = false

        bufferListQueryJob?.cancel()

        postListDataObservationJob?.cancel()
        postListRefreshingObservationJob?.cancel()

        listHelper.cleanup()

        _content.value = Model(
            null,
            null,
            null
        )
        _metaState.value = MetaState(
            true,
            false
        )
    }



    /////////////////////////////////////////

    fun onRequestClearSearchResults() {
        Timber.d("onRequestClearSearchResults")
        if (content.value.query == null) return

        bufferListQueryJob?.cancel()

        _content.value = _content.value.copy(
            query = null,
            listQuery = null,
            list = null
        )
    }



    var lastAppliedQuery: String? = null

    fun onQueryTextChange(input: String): Boolean {
        if (lastAppliedQuery?.trim() == input.trim()) return false
        lastAppliedQuery = input

        Timber.d("onQueryTextChange: $input")

        _content.value = _content.value.copy(
            query = input
        )


        bufferListQueryChange(input.trim())

        return true
    }

    fun onQueryTextSubmit(input: String): Boolean {
        return onQueryTextChange(input)
    }


    private fun bufferListQueryChange(input: String) {
        val listQuery = _content.value.listQuery
        if (listQuery == input) return

        _content.value = _content.value.copy(
            listQuery = input
        )

        bufferListQueryJob?.cancel()
        bufferListQueryJob = coroutineUtil.appScope.launch {
            delay(1337)

            internalRefresh.value = internalRefresh.value + 1
            listHelper.userQueryChanged(input)
        }
    }


    /////////////////////////////////////////



    data class Model(
        val query: String?,
        val listQuery: String?,
        val list: TwetchPostModel?
    )

    data class MetaState(
        val refreshing: Boolean,
        val error: Boolean
    )


}