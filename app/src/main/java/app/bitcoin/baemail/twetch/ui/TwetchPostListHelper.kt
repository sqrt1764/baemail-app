package app.bitcoin.baemail.twetch.ui

import app.bitcoin.baemail.core.data.util.CoroutineUtil
import com.dropbox.android.external.store4.ResponseOrigin
import com.dropbox.android.external.store4.StoreResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import timber.log.Timber

@ExperimentalCoroutinesApi
@FlowPreview
class TwetchPostListHelper(
    private val activeUserId: String,
    private val userId: String?,
    private val listType: ListType,
    private val coroutineUtil: CoroutineUtil,
    private val postListStore: TwetchPostListStore,
    initialRefreshingValue: Boolean = true
) {

    companion object {
        const val PAGE_NOT_LOADED = -1
    }

    private val _data = MutableStateFlow<TwetchPostModel?>(null)
    val data: StateFlow<TwetchPostModel?>
        get() = _data

    private val _refreshing = MutableStateFlow(initialRefreshingValue)
    val refreshing: StateFlow<Boolean>
        get() = _refreshing

    private val _chatMessagesError = MutableStateFlow(false)
    val chatMessagesError: StateFlow<Boolean>
        get() = _chatMessagesError

    private var isSetup = false

    private var collectPostsJob: Job? = null
    private var collectPostsPageJob: Job? = null


    private val _posts = MutableStateFlow<StoreResponse<List<PostHolder>>>(
        StoreResponse.Loading(ResponseOrigin.Cache)
    )

    private var page = PAGE_NOT_LOADED
    private var _query: String? = null
    val query: String?
        get() = _query

    private var _completedPageFetch = MutableStateFlow(page)
    private var enqueuedGetNextPageJob: Job? = null

    fun ensureSetup() {
        if (isSetup) return
        isSetup = true

        collectPostsJob?.cancel()
        collectPostsJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {


            var lastData = listOf<PostHolder>()
            var lastRefreshingState = _refreshing.value
            var lastErrorState = false

            _posts.collect { response ->


                when (response) {
                    is StoreResponse.Loading -> {
                        Timber.d("StoreResponse.Loading origin:${response.origin}")
                        lastRefreshingState = true

                    }
                    is StoreResponse.Data<*> -> {
                        Timber.d("StoreResponse.Data origin:${response.origin}")
                        if (response.origin == ResponseOrigin.Fetcher) {
                            lastRefreshingState = false
                            lastErrorState = false
                        } else if (response.origin == ResponseOrigin.SourceOfTruth
                            || response.origin == ResponseOrigin.Cache) {
//                            lastRefreshingState = false
                        }

                        lastData = response.requireData()


                    }
                    is StoreResponse.Error -> {
                        Timber.d("StoreResponse.Error $response; ")
                        if (response.origin == ResponseOrigin.Fetcher) {
                            //no longer loading
                            lastRefreshingState = false
                        }

                        if (response is StoreResponse.Error.Exception) {
                            Timber.e(response.error, "StoreResponse.Error.Exception")
                        }

                        //show error
                        lastErrorState = true

                    }
                    is StoreResponse.NoNewData -> {
                        Timber.d("StoreResponse.NoNewData")
                        lastRefreshingState = false
                    }
                }





                _refreshing.value = lastRefreshingState
                _data.value = TwetchPostModel(lastData, page, checkHasMorePages())
                _chatMessagesError.value = lastErrorState

            }


        }



    }

    private fun checkHasMorePages(): Boolean {
        return postListStore.checkChatHasMorePages(
            listType,
            activeUserId,
            userId,
            _query
        )
    }

    fun userQueryChanged(query: String) {
        this._query = query

        refreshFromStart()
    }

    fun userRequestedNextPage() {
        Timber.d(">>>> userRequestedNextPage()")

        if (enqueuedGetNextPageJob != null) {
            Timber.d(">>>> userRequestedNextPage() dropped because a request is already enqueued")
            return
        }

        if (!checkHasMorePages()) {
            Timber.d(">>>> userRequestedNextPage() dropped because there are no more pages to fetch")
            return
        }

        enqueuedGetNextPageJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            Timber.d(">>>> userRequestedNextPage() the request is now enqueued")

            _completedPageFetch.collect { completedPage ->
                if (completedPage != page) {
                    Timber.d(">>>> message-page-load is in progress; page:$page completedPage:$completedPage")
                    return@collect
                }

                val didError = chatMessagesError.value
                if (didError) {
                    Timber.d(">>>> not carrying out an enqueued request because there was an error")
                    return@collect
                }

                if (!checkHasMorePages()) {
                    //cleanup
                    Timber.d(">>>> not carrying out an enqueued request because there was are no more pages")
                    val thisJob = enqueuedGetNextPageJob
                    enqueuedGetNextPageJob = null
                    thisJob?.cancel()

                    return@collect
                }

                Timber.d(">>>>carrying out an enqueued request because the messages NOT refreshing")

                requestNextPage()

                //cleanup
                val thisJob = enqueuedGetNextPageJob
                enqueuedGetNextPageJob = null
                thisJob?.cancel()
            }
        }
    }

    private fun requestNextPage(forceFresh: Boolean = false) {
        if (!isSetup) throw RuntimeException()

        Timber.d("requestNextPageOf $listType page:${page + 1}")

        collectPostsPageJob?.cancel()
        collectPostsPageJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            page += 1
            val currentPage = page


            val key = TwetchPostListStore.Key(listType, activeUserId, userId, _query)
            key.page  = currentPage

            //eagerly return the cached content
            if (currentPage == 0) {
                postListStore.getFromSourceOfTruth(key).let {
                    //return what is already cached
                    if (it.isEmpty()) return@let
                    _refreshing.value = true
                    _posts.value = StoreResponse.Data(it, ResponseOrigin.SourceOfTruth)
                }
            }

            postListStore.getFlow(key, forceFresh).collect { response ->
                _posts.value = response

                //update the tracking of fetch progress
                when (response) {
                    is StoreResponse.Loading -> { }
                    is StoreResponse.Data<*> -> {
                        if (response.origin == ResponseOrigin.Fetcher) {
                            _completedPageFetch.value = currentPage
                        }

                    }
                    is StoreResponse.Error -> {
                        if (response.origin == ResponseOrigin.Fetcher) {
                            _completedPageFetch.value = currentPage
                        }

                    }
                    is StoreResponse.NoNewData -> {
                        _completedPageFetch.value = currentPage
                    }
                }
            }
        }

    }

    fun refreshFromStart() {
        collectPostsPageJob?.cancel()
        collectPostsPageJob = null

        enqueuedGetNextPageJob?.cancel()
        enqueuedGetNextPageJob = null

        page = PAGE_NOT_LOADED
        _completedPageFetch.value = page

        ////

        requestNextPage(true)
    }


    fun cleanup() {
        isSetup = false

        collectPostsJob?.cancel()
        collectPostsJob = null
        collectPostsPageJob?.cancel()
        collectPostsPageJob = null

        enqueuedGetNextPageJob?.cancel()
        enqueuedGetNextPageJob = null

        page = PAGE_NOT_LOADED
        _completedPageFetch.value = page
        _posts.value = StoreResponse.Loading(ResponseOrigin.Cache)

        _data.value = null
        _refreshing.value = true
        _chatMessagesError.value = false
    }


}