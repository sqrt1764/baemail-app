package app.bitcoin.baemail.twetch.ui

import android.content.Context
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.twetch.db.TwetchDatabase
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.dropbox.android.external.store4.*
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapLatest
import timber.log.Timber

class TwetchPostDetailsStore(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val twetchDatabase: TwetchDatabase,
    private val postCache: TwetchPostCache,
    private val gson: Gson,
    private val userCache: TwetchPostUserCache,
    private val twetchFilesMimeTypeHelper: TwetchFilesMimeTypeHelper,
) {

    companion object {
        private const val PAGE_SIZE = 15
    }


    private val dummyReplyListOrderCache = HashMap<Key, MutableList<String>>()

    private val dummyPagingMetaCache = HashMap<Key, PagingMeta>()


    private val postDetailsStore: Store<Key, PostDetails> = StoreBuilder.from(
        fetcher = Fetcher.ofFlow { filterKey ->
            Timber.d("postDetailsStore#fetcherOfFlow $filterKey")
            return@ofFlow queryServer(filterKey)
        },
        sourceOfTruth = SourceOfTruth.of(
            reader = ::queryByFilter,
            writer = ::insert,
            delete = ::clearByKey,
            deleteAll = ::deleteAll
        )

    ).build()


    private fun queryServer(key: Key): Flow<PostDetails> {
        return flow {

            val locallyAvailable = getFromSourceOfTruth(key)

            val page = key.replyPage

            val cursor = if (page != 0) {
                dummyPagingMetaCache[key]?.afterCursor
            } else null

            val finalCursor = if (cursor == null) {
                "null"
            } else {
                "'\"$cursor\"'"
            }



            var mustFetchFullDetailsOfMain = locallyAvailable.main?.hasMissingRefPosts() ?: true

            val finalMainPostId = if (!mustFetchFullDetailsOfMain) {
                "\"${locallyAvailable.main!!.post.id}\""
            } else {
                "null"
            }


            var mustFetchParents = if (locallyAvailable.main == null) {
                true
            } else {
                var mustFetch = false
                locallyAvailable.main.post.parents.forEach { parent ->
                    val matchingLocal = locallyAvailable.parents[parent.transaction]
                    if (matchingLocal == null || matchingLocal.hasMissingRefPosts()) {
                        mustFetch = true
                    }

                }

                mustFetch
            }



            if (key.forceRefresh) {
                Timber.d("doing force-refresh of full details")
                mustFetchFullDetailsOfMain = true
                mustFetchParents = true
            }




            val parentPostTxIdArray = if (mustFetchFullDetailsOfMain) {
                "[]"
            } else {
                val jsonArray = JsonArray()
                locallyAvailable.main!!.post.parents.forEach { parent ->
                    jsonArray.add(parent.transaction)
                }
                jsonArray.toString()
            }

            //query server-side
            val result = nodeRuntimeRepository.runAsyncScript("""
                
                const postFetchingFunction = ${TwetchQueryUtil.printPostFetchingFunction()};
                const extractBranchTxIds = ${TwetchQueryUtil.printBranchTxExtractionFunction()};
                const chunk = (arr, n) => arr.length ? [arr.slice(0, n), ...chunk(arr.slice(n), n)] : [];
                
                
                let parentPostTxIdArray = $parentPostTxIdArray;
                
                
                
                
                let millisStart = new Date().getTime();
            
                await context.twetch.ensureAuthHasCompleted();
                
                let millisPostAuth = new Date().getTime();
                
                
                var mainPostId = $finalMainPostId;
                var mainPost = null;
                var refPostMap = {};
                
                var txIdOfRefOfMain = null;
                var referencedPostsTxIdMap = {};
               
                if ($mustFetchFullDetailsOfMain) {
                    let foundPosts0 = await postFetchingFunction({
                        '${key.postTxId}': '${key.postTxId}' 
                    });
                    
                    mainPost = foundPosts0['${key.postTxId}'];
                    mainPostId = mainPost.id;
                    
                    
                    //extract parent-post-ids
                    mainPost.parents.nodes.forEach((item) => {
                        parentPostTxIdArray.push(item.transaction);
                    });
                    
                    
                    //must fetch referenced post if exists
                    let gotMap = extractBranchTxIds(Object.values(foundPosts0));
                    if (Object.keys(gotMap).length > 1) {
                        throw new Error('unexpected');
                    }
                    
                    Object.values(gotMap).forEach((item) => {
                        txIdOfRefOfMain = item;
                    });
                }
                
                
                
                let millisPostFetchOfMain = new Date().getTime();
                
                
                
                if ($mustFetchParents) {
                    let chunkedParentTxIds = chunk(parentPostTxIdArray, $PAGE_SIZE);
                    
                    for (var arrayOfTxIds of chunkedParentTxIds) {
                        referencedPostsTxIdMap = {};
                        arrayOfTxIds.forEach((item) => {
                            referencedPostsTxIdMap[item] = item;
                        });
                        
                        let parentPostMap = await postFetchingFunction(referencedPostsTxIdMap);
                        refPostMap = Object.assign(refPostMap, parentPostMap);
                        
                    }
                    
                }
                
                
                let millisPostFetchOfParents = new Date().getTime();
                
                
                
                
                
                
                // fetch a page of replies
                let afterCursor = $finalCursor;
                
                const fetchPageOfReplies = ${printFunctionFetchPageOfReplies()};
                
                let gotReplies = await fetchPageOfReplies($PAGE_SIZE, afterCursor, mainPostId);
                
                var replyPage = {
                    "hasNextPage":false,
                    "endCursor":null,
                    "posts":[]
                };
                if (gotReplies.allPosts != null) {
                    let postList = gotReplies.allPosts.edges.map((item) => {
                        return item.node;
                    });
                    replyPage = {
                        hasNextPage: gotReplies.allPosts.pageInfo.hasNextPage,
                        endCursor: gotReplies.allPosts.pageInfo.endCursor,
                        posts: postList
                    };
                }
                
                
                
                
                
                let millisPostFetchOfReplies = new Date().getTime();
                
                
                
                
                
                //
                //
                //must fetch referenced posts
                let parentPostRefTxIdList = Object.values(extractBranchTxIds(Object.values(refPostMap)));
                let replyPostRefTxIdList = Object.values(extractBranchTxIds(replyPage.posts));
                let refTxIdList = [...parentPostRefTxIdList, ...replyPostRefTxIdList];
                if (txIdOfRefOfMain != null) {
                    refTxIdList.push(txIdOfRefOfMain);
                }
                
                
                //must fetch the posts in a paginated manner
                
                let chunkedTxIds = chunk(refTxIdList, $PAGE_SIZE);
                
                for (var arrayOfTxIds of chunkedTxIds) {
                    referencedPostsTxIdMap = {};
                    arrayOfTxIds.forEach((item) => {
                        referencedPostsTxIdMap[item] = item;
                    });
                    
                    let parentPostMap = await postFetchingFunction(referencedPostsTxIdMap);
                    refPostMap = Object.assign(refPostMap, parentPostMap);
                    
                }
                
                
                
                let millisPostFetchOfRefs = new Date().getTime();
                
                
                
                console.log("time spent awaiting twetch-auth:" 
                    + (millisPostAuth - millisStart) 
                    + "ms; time spent on main-post:" 
                    + (millisPostFetchOfMain - millisPostAuth) 
                    + "ms; time spent on parents-of-main:" 
                    + (millisPostFetchOfParents - millisPostFetchOfMain)
                    + "ms; time spent on a page of replies-to-main:"
                    + (millisPostFetchOfReplies - millisPostFetchOfParents)
                    + "ms; time spent on ref-posts:"
                    + (millisPostFetchOfRefs - millisPostFetchOfReplies)
                    + "ms");
                
                
                return JSON.stringify({
                    main: mainPost,
                    refPostMap: refPostMap,
                    replyPage: replyPage
                });
            """.trimIndent())


            if (result is RunScript.Response.Success) {
//                Timber.d("success: ${result.content}") //...was too much spam in logs

                val parsed = parseSuccess(result.content, key.activeUserId)


                val finalMain = if (parsed.main == null) {
                    locallyAvailable.main!!
                } else {
                    val refMap = mutableMapOf<String, PostModel>()

                    parsed.main.referencedPost?.let { refTxId ->
                        val refP = parsed.refPosts[refTxId]

                        refP?.let { refPost ->
                            refMap[refPost.txId] = refPost
                        }

                        refP?.referencedPost?.let { doubleRefTxId ->
                            parsed.refPosts[doubleRefTxId]?.let { doubleRefP ->
                                refMap[doubleRefP.txId] = doubleRefP
                            }
                        }
                    }

                    PostHolder(
                        parsed.main,
                        refMap
                    )
                }

                val parentsMap = mutableMapOf<String, PostHolder>()
                finalMain.post.parents.forEach { parent ->
                    val mainOfParent = parsed.refPosts[parent.transaction]
                        ?: locallyAvailable.parents[parent.transaction]?.post
                    mainOfParent ?: throw RuntimeException()


                    val refMap = mutableMapOf<String, PostModel>()

                    mainOfParent.referencedPost?.let { refTxId ->
                        val refP = parsed.refPosts[refTxId]

                        refP?.let { refPost ->
                            refMap[refPost.txId] = refPost
                        }

                        refP?.referencedPost?.let { doubleRefTxId ->
                            parsed.refPosts[doubleRefTxId]?.let { doubleRefP ->
                                refMap[doubleRefP.txId] = doubleRefP
                            }
                        }
                    }

                    parentsMap[parent.transaction] = PostHolder(
                        mainOfParent,
                        refMap
                    )
                }


                //replies
                val replyHolderPage = parsed.replyPage.map { mainOfReply ->

                    val refMap = mutableMapOf<String, PostModel>()

                    mainOfReply.referencedPost?.let { refTxId ->
                        val refP = parsed.refPosts[refTxId]

                        refP?.let { refPost ->
                            refMap[refPost.txId] = refPost
                        }

                        refP?.referencedPost?.let { doubleRefTxId ->
                            parsed.refPosts[doubleRefTxId]?.let { doubleRefP ->
                                refMap[doubleRefP.txId] = doubleRefP
                            }
                        }
                    }

                    PostHolder(
                        mainOfReply,
                        refMap
                    )
                }


                val totalQueriedReplies = locallyAvailable.replies?.let {
                    if (page == 0) {
                        ArrayList()
                    } else {
                        ArrayList<PostHolder>(it)
                    }
                } ?: ArrayList()

                totalQueriedReplies.addAll(replyHolderPage)




                handleUserCachingAndRetrieval(parsed)




                dummyPagingMetaCache[key] = PagingMeta(
                    key.replyPage,
                    parsed.replyEndCursor,
                    false
                )



                emit(PostDetails(
                    key.postTxId,
                    finalMain,
                    parentsMap,
                    totalQueriedReplies
                ))



            } else {
                result as RunScript.Response.Fail

                dummyPagingMetaCache[key] = PagingMeta(
                    key.replyPage,
                    null,
                    true
                )

                throw result.throwable
            }
        }
    }

    private suspend fun handleUserCachingAndRetrieval(
            parsed: ParsedSuccess
    ) {
        try {
            val mentionedUsers = HashSet<String>()

            if (parsed.main != null) {
                userCache.setUsersOfPost(parsed.main)
                mentionedUsers.addAll(TwetchPostUserCache.findUserMentions(parsed.main.content))
            }

            parsed.refPosts.values.forEach { post ->
                userCache.setUsersOfPost(post)
                mentionedUsers.addAll(TwetchPostUserCache.findUserMentions(post.content))
            }

            parsed.replyPage.forEach { post ->
                userCache.setUsersOfPost(post)
                mentionedUsers.addAll(TwetchPostUserCache.findUserMentions(post.content))
            }



            val missingPostUsers = userCache.findUsersMissingForIds(mentionedUsers)
            if (missingPostUsers.isEmpty()) return




            val foundUsers = TwetchPostUserCache.findUserForIds(
                    nodeRuntimeRepository,
                    gson,
                    missingPostUsers
            )

            Timber.d("found: $foundUsers")

            foundUsers.forEach { user ->
                userCache.setUser(user)
            }



        } catch (e: Exception) {
            Timber.e(e)
        }

    }


    private fun printFunctionFetchPageOfReplies(): String {
        return """
            async function (pageSize, afterCursor, replyPostId) {
                
                let res = await context.twetch.instance.query(`
                    {
                        allPosts(
                            first: ${"$"}{pageSize}
                            condition: { replyPostId: "${"$"}{replyPostId}" }
                            orderBy: CREATED_AT_ASC
                            after: ${"$"}{afterCursor}
                        ) {
                            pageInfo {
                                endCursor
                                hasNextPage
                            }
                            edges {
                                node {
                                    bContent
                                    bContentType
                                    createdAt
                                    id
                                    mapComment
                                    nodeId
                                    numBranches: branchCount
                                    numLikes: likeCount
                                    replyPostId
                                    transaction
                                    userId
                                    youBranched
                                    youLiked
                                    mapTwdata
                                    files
                                    mediaByPostId {
                                        nodes {
                                            filename
                                        }
                                    }
                                    postsByReplyPostId {
                                        totalCount
                                    }
                                    parents {
                                        nodes {
                                            nodeId
                                            id
                                            transaction
                                            userByUserId {
                                                icon
                                                id
                                                name
                                                nodeId
                                            }
                                        }
                                    }
                                    userByUserId {
                                        createdAt
                                        icon
                                        id
                                        name
                                        nodeId
                                    }
                                }
                            }
                        }
                    }
                `);
                
                return res;
            }
        """.trimIndent()
    }



    private fun queryByFilter(key: Key): Flow<PostDetails> {
        return postCache.updatedState.mapLatest {
            val main = postCache.getPostHolder(key.activeUserId, key.postTxId)
            if (main == null) {
                return@mapLatest PostDetails(
                    key.postTxId,
                    null,
                    emptyMap(),
                    null
                )
            }

            val parentMap = mutableMapOf<String, PostHolder>()
            main.post.parents.map { it.transaction }.mapNotNull { txId ->
                postCache.getPostHolder(key.activeUserId, txId)
            }.forEach { postHolder ->
                parentMap[postHolder.post.txId] = postHolder
            }

            //replies
            val replyList = dummyReplyListOrderCache[key]?.mapNotNull { txId ->
                postCache.getPostHolder(key.activeUserId, txId)
            }

            return@mapLatest PostDetails(
                key.postTxId,
                main,
                parentMap,
                replyList
            )
        }
    }

    private suspend fun insert(key: Key, results: PostDetails) {
        if (results.main == null) return


        val foundFilesRefs = ArrayList<String>()

        results.main.let { holder ->
            postCache.setPost(key.activeUserId, holder.post)

            holder.referenced.values.forEach { post ->
                postCache.setPost(key.activeUserId, post)

                if (post.files.isNotEmpty()) {
                    foundFilesRefs.addAll(post.files)
                }
            }

            if (holder.post.files.isNotEmpty()) {
                foundFilesRefs.addAll(holder.post.files)
            }
        }

        results.parents.values.forEach { holder ->
            postCache.setPost(key.activeUserId, holder.post)

            holder.referenced.values.forEach { post ->
                postCache.setPost(key.activeUserId, post)

                if (post.files.isNotEmpty()) {
                    foundFilesRefs.addAll(post.files)
                }
            }

            if (holder.post.files.isNotEmpty()) {
                foundFilesRefs.addAll(holder.post.files)
            }
        }

        val replyListOrder = mutableListOf<String>()
        results.replies?.forEach { holder ->
            replyListOrder.add(holder.post.txId)
            postCache.setPost(key.activeUserId, holder.post)

            holder.referenced.values.forEach { post ->
                postCache.setPost(key.activeUserId, post)

                if (post.files.isNotEmpty()) {
                    foundFilesRefs.addAll(post.files)
                }
            }

            if (holder.post.files.isNotEmpty()) {
                foundFilesRefs.addAll(holder.post.files)
            }
        }

        dummyReplyListOrderCache[key] = replyListOrder

        postCache.notifyUpdated()



        if (foundFilesRefs.isNotEmpty()) {
            twetchFilesMimeTypeHelper.checkFilesContent(foundFilesRefs)
        }
    }

    private suspend fun clearByKey(key: Key) {
        throw RuntimeException()
    }

    private suspend fun deleteAll() {
        postCache.clear()
    }


    /////////////////////////////////////////////////////////////////

    fun checkPostHasMoreReplies(
        activeUserId: String,
        postTxId: String
    ): Boolean {
        val pagingMeta = dummyPagingMetaCache[Key(activeUserId, postTxId)]

        pagingMeta ?: let {
            //nothing cached yet
            return true
        }

        return pagingMeta.afterCursor != null && !pagingMeta.didError
    }

    fun clearErrorFromMetaCache(
        activeUserId: String,
        postTxId: String
    ) {
        val key = Key(activeUserId, postTxId)
        val pagingMeta = dummyPagingMetaCache[key]

        pagingMeta ?: let {
            //nothing cached yet
            return
        }

        dummyPagingMetaCache[key] = pagingMeta.copy(
            didError = false,
            lastPage = pagingMeta.lastPage - 1
        )
    }

    fun clearFromMetaCache(
        activeUserId: String,
        postTxId: String
    ) {
        val key = Key(activeUserId, postTxId)
        dummyPagingMetaCache.remove(key)
    }


    /////////////////////////////////////////////////////////////////

    suspend fun getFromSourceOfTruth(key: Key): PostDetails {
        return queryByFilter(key).first()
    }

    fun getFlow(key: Key): Flow<StoreResponse<PostDetails>> {
        return postDetailsStore.stream(StoreRequest.cached(key, true))
    }


    /////////////////////////////////////////////////////////////////

    data class ParsedSuccess(
        val main: PostModel?,
        val refPosts: Map<String, PostModel>,
        val replyEndCursor: String?,
        val replyHasNextPage: Boolean,
        val replyPage: List<PostModel>
    )

    private fun parseSuccess(
        content: String,
        activeUserId: String
    ): ParsedSuccess {
        val millisNow = System.currentTimeMillis()

        val responseJson = gson.fromJson(content, JsonObject::class.java)

        val mainPost = if (responseJson.has("main")) {
            val json = responseJson.get("main")
            if (json.isJsonNull) {
                null
            } else {

                PostModel.parsePost(
                    gson,
                    json.asJsonObject,
                    activeUserId,
                    millisNow
                )
            }
        } else null


        val refPostMap = if (responseJson.has("refPostMap")) {
            val json = responseJson.get("refPostMap")
            if (json.isJsonNull) {
                emptyMap()
            } else {
                val parsedRefPostMap = HashMap<String, PostModel>()
                val jsonMap = json.asJsonObject

                jsonMap.keySet().forEach { key ->
                    val post = PostModel.parsePost(
                        gson,
                        jsonMap[key].asJsonObject,
                        activeUserId,
                        millisNow
                    )
                    parsedRefPostMap[key] = post
                }

                parsedRefPostMap
            }
        } else {
            emptyMap()
        }





        var replyEndCursor: String? = null
        var replyHasNextPage = false
        val replyPage = mutableListOf<PostModel>()

        if (responseJson.has("replyPage")) {
            val json = responseJson.get("replyPage")
            if (!json.isJsonNull) {
                val jsonObj = json.asJsonObject

                replyEndCursor = if (jsonObj.has("endCursor")
                    && !jsonObj.get("endCursor").isJsonNull
                ) {
                    jsonObj.get("endCursor").asString
                } else null

                replyHasNextPage = jsonObj.get("hasNextPage").asBoolean

                val replyPostsArray = jsonObj.getAsJsonArray("posts")
                replyPostsArray.forEach { item ->
                    replyPage.add(PostModel.parsePost(
                        gson,
                        item.asJsonObject,
                        activeUserId,
                        millisNow
                    ))
                }

                if (replyPostsArray.size() < PAGE_SIZE) {
                    replyEndCursor = null
                }

            }
        }




        return ParsedSuccess(
            mainPost,
            refPostMap,
            replyEndCursor,
            replyHasNextPage,
            replyPage
        )
    }










    data class Key(
        val activeUserId: String,
        val postTxId: String//,
//        val forceRefreshOfMain: Boolean = false,
//        val forceRefreshOfDeeperPost: Boolean = false
    ) {
        var replyPage = 0
        var forceRefresh = false

        override fun toString(): String {
            return "Key(activeUserId='$activeUserId', postTxId='$postTxId', replyPage=$replyPage)"
        }

    }

    data class PostDetails(
        val txId: String,
        val main: PostHolder?,
        val parents: Map<String, PostHolder>,
        val replies: List<PostHolder>?
    )


    data class PagingMeta(
        val lastPage: Int,
        val afterCursor: String?,
        val didError: Boolean
    )


}