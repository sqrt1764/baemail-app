package app.bitcoin.baemail.twetch.db.entity

import androidx.room.Entity
import app.bitcoin.baemail.core.data.room.TABLE_TWETCH_USER_PROFILE_BASE

@Entity(tableName = TABLE_TWETCH_USER_PROFILE_BASE, primaryKeys = ["activeUserId", "userId"])
data class UserProfileBaseModel(
    val activeUserId: String,
    val userId: String,
    var followerCount: Int,
    val followingCount: Int,
    val icon: String?,
    val name: String,
    val publicKey: String?,
    val description: String?,
    val dmConversationId: String?,
    val followsYou: Boolean,
    var youFollow: Boolean,
    val postCount: Int
) {

    fun onFollowedByMe() {
        youFollow = true
        followerCount++
    }

    fun onUnfollowedByMe() {
        youFollow = false
        followerCount--
    }
}