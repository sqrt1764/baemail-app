package app.bitcoin.baemail.twetch.ui

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.ViewVideoClip
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import com.google.android.material.button.MaterialButton
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.collectLatest
import timber.log.Timber
import javax.inject.Inject

class TwetchVideoClipFragment : AppCompatDialogFragment() {

    companion object {
        const val KEY_TX_ID = "tx_id"
    }

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TwetchViewModel

    private lateinit var videoView: ViewVideoClip
    private lateinit var back: MaterialButton


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_twetch_video_clip, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return FullScreenDialog(requireContext())
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]


        videoView = requireView().findViewById(R.id.video_view)
        back = requireView().findViewById(R.id.back)



        val txId = requireArguments().getString(KEY_TX_ID)!!




        back.setOnClickListener {
            findNavController().popBackStack()
        }




        val itemHolder = viewModel.twetchFilesMimeTypeHelper.ensureMemoryCacheItem(txId)

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            var isVideoViewSetup = false

            itemHolder.state.collectLatest { mimeState ->
                Timber.d("gotMime: $mimeState")
                if (TwetchFilesMimeTypeHelper.State.VIDEO == mimeState) {

                    if (!isVideoViewSetup) {
                        isVideoViewSetup = true

                        Timber.d("`quotePostVideo` setup for tx: $txId")

                        videoView.setup(
                            this@TwetchVideoClipFragment,
                            viewModel.videoClipManager.requestClip(
                                "https://ink.twetch.com/dogefiles/$txId",
                                true
                            ),
                            showController = true,
                            fitContent = true,
                            soundOn = true
                        )
                    }


                } else if (TwetchFilesMimeTypeHelper.State.IMAGE == mimeState) {
                    findNavController().popBackStack()
                }
            }
        }



    }




}