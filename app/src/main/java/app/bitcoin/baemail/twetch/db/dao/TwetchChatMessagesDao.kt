package app.bitcoin.baemail.twetch.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.bitcoin.baemail.core.data.room.TABLE_TWETCH_CHAT_MESSAGES
import app.bitcoin.baemail.twetch.db.entity.TwetchChatMessage
import kotlinx.coroutines.flow.Flow

@Dao
interface TwetchChatMessagesDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list: List<TwetchChatMessage>)

    @Query("select * from $TABLE_TWETCH_CHAT_MESSAGES where paymail = :paymail and chatId = :chatId order by millisCreatedAt desc limit :limit offset :offset")
    suspend fun getForChat(paymail: String, chatId: String, offset: Int, limit: Int): List<TwetchChatMessage>

    @Query("select * from $TABLE_TWETCH_CHAT_MESSAGES where paymail = :paymail and chatId = :chatId order by millisCreatedAt desc limit :limit offset :offset")
    fun flowForChat(paymail: String, chatId: String, offset: Int, limit: Int): Flow<List<TwetchChatMessage>>

    @Query("select * from $TABLE_TWETCH_CHAT_MESSAGES where paymail = :paymail and chatId = :chatId and id = :messageId")
    suspend fun getMessage(paymail: String, chatId: String, messageId: String): TwetchChatMessage?

    @Query("select * from $TABLE_TWETCH_CHAT_MESSAGES where paymail = :paymail and chatId = :chatId and millisCreatedAt <= :millisOfLatest order by millisCreatedAt desc limit :limit ")
    suspend fun getPageBeforeMillis(paymail: String, chatId: String, millisOfLatest: Long, limit: Int): List<TwetchChatMessage>

    @Query("delete from $TABLE_TWETCH_CHAT_MESSAGES")
    suspend fun deleteAll()

    @Query("delete from $TABLE_TWETCH_CHAT_MESSAGES where paymail = :paymail and chatId = :chatId")
    suspend fun deleteForChat(paymail: String, chatId: String)

}