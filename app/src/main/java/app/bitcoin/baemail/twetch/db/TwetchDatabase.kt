package app.bitcoin.baemail.twetch.db

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import app.bitcoin.baemail.core.data.room.TABLE_TWETCH_CHAT_MESSAGES
import app.bitcoin.baemail.core.data.room.TABLE_TWETCH_USER_PROFILE_BASE
import app.bitcoin.baemail.core.data.room.converters.DbConverters
import app.bitcoin.baemail.twetch.db.dao.TwetchChatMessagesDao
import app.bitcoin.baemail.twetch.db.dao.UserProfileBaseDao
import app.bitcoin.baemail.twetch.db.entity.TwetchChatMessage
import app.bitcoin.baemail.twetch.db.entity.UserProfileBaseModel
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.data.util.twetchChatMessageSyncMetaPrefs
import kotlinx.coroutines.launch
import timber.log.Timber

@Database(entities = [
    TwetchChatMessage::class,
    UserProfileBaseModel::class
], version = 8)
@TypeConverters(DbConverters::class)
abstract class TwetchDatabase : RoomDatabase() {

    abstract fun twetchChatMessagesDao(): TwetchChatMessagesDao

    abstract fun userProfileBaseDao(): UserProfileBaseDao


}

class SafeEmptyingMigration(
    val appContext: Context,
    val coroutineUtil: CoroutineUtil,
    val fromVersion: Int,
    val toVersion: Int
) : Migration(fromVersion, toVersion) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.beginTransaction()
        try {

            database.execSQL("drop table if exists `$TABLE_TWETCH_CHAT_MESSAGES`")
            database.execSQL("drop table if exists `$TABLE_TWETCH_USER_PROFILE_BASE`")

            database.execSQL(CREATE_CHAT_MESSAGE_TABLE)
            database.execSQL(CREATE_USER_PROFILE_BASE_TABLE)

            database.setTransactionSuccessful()

            coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
                appContext.twetchChatMessageSyncMetaPrefs.edit { prefs ->
                    prefs.clear()
                }

                //todo should the chat-images be deleted?
            }

            Timber.d("TwetchDatabase migrated .. V$fromVersion to V$toVersion")

        } finally {
            database.endTransaction()
        }
    }

    companion object {
        const val CREATE_CHAT_MESSAGE_TABLE = "CREATE TABLE IF NOT EXISTS `twetch_chat_messages` (`paymail` TEXT NOT NULL, `id` TEXT NOT NULL, `chatId` TEXT NOT NULL, `millisCreatedAt` INTEGER NOT NULL, `userId` TEXT NOT NULL, `description` TEXT NOT NULL, `images` TEXT NOT NULL, `user` TEXT, PRIMARY KEY(`paymail`, `chatId`, `id`))"
        const val CREATE_USER_PROFILE_BASE_TABLE = "CREATE TABLE IF NOT EXISTS `twetch_user_profile_base` (`activeUserId` TEXT NOT NULL, `userId` TEXT NOT NULL, `followerCount` INTEGER NOT NULL, `followingCount` INTEGER NOT NULL, `icon` TEXT, `name` TEXT NOT NULL, `publicKey` TEXT, `description` TEXT, `dmConversationId` TEXT, `followsYou` INTEGER NOT NULL, `youFollow` INTEGER NOT NULL, `postCount` INTEGER NOT NULL, PRIMARY KEY(`activeUserId`, `userId`))"
    }

}