package app.bitcoin.baemail.twetch.ui

import app.bitcoin.baemail.core.data.util.CoroutineUtil
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class TwetchNewPostUseCase(
    private val coroutineUtil: CoroutineUtil,
    private val postCache: TwetchPostCache,
    private val getActiveUserId: ()->String
) {

    lateinit var actionHelper: TwetchActionHelper

    private val _openComposeChannel = Channel<Long>(Channel.CONFLATED)
    val openComposeChannel: ReceiveChannel<Long>
        get() = _openComposeChannel

    private val _content = MutableStateFlow(Model(
            "",
            null,
            null
    ))
    val content: StateFlow<Model>
        get() = _content


    fun discardDelayed() {
        coroutineUtil.appScope.launch {
            delay(500)
            discard()
        }
    }

    fun discard() {

        _content.value = Model(
                "",
                null,
                null
        )
    }

    suspend fun prepareQuoteBranch(postTx: String) {
        val gotPost = postCache.getPost(getActiveUserId(), postTx) ?: throw RuntimeException()
        prepareQuoteBranch(gotPost)
    }

    suspend fun prepareQuoteBranch(model: PostModel) {
        _content.value = Model(
            "",
            model,
            null
        )

        _openComposeChannel.send(System.currentTimeMillis())

    }

    suspend fun prepareReply(model: PostModel) {
        _content.value = Model(
            "",
            null,
            model
        )

        _openComposeChannel.send(System.currentTimeMillis())
    }

    fun onContentChanged(value: String) {
        if (_content.value.content.trim() == value.trim()) return

        _content.value = _content.value.copy(content = value)
    }

    fun sendPost(): Boolean {
        val model = _content.value

        return when {
            model.quoted != null -> {
                actionHelper.executeQuoteBranch(
                        model.quoted.txId,
                        model.content.replace("\n", " ")
                ) { success ->
                    if (success) {
                        discard()
                    }
                }
            }
            model.replying != null -> {
                actionHelper.executeReply(
                        model.replying.txId,
                        model.content
                ) { success ->
                    if (success) {
                        discard()
                    }
                }
            }
            else -> {
                actionHelper.executeNewPost(
                        model.content
                ) { success ->
                    if (success) {
                        discard()
                    }
                }
            }
        }
    }


    data class Model(
        val content: String,
        val quoted: PostModel?,
        val replying: PostModel?
    )
}