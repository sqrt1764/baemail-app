package app.bitcoin.baemail.twetch.ui

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.combine
import timber.log.Timber
import javax.inject.Inject

class TwetchAuthFragment : Fragment(R.layout.fragment_twetch_auth) {

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TwetchViewModel


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)



        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]






        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    isEnabled = false


                    requireActivity().finish()
                }

            })








        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            var isNavigatingHolder = false

            viewModel.authState.combine(viewModel.authInfo) { status, info ->
                status to info
            }.collect { (status, info) ->

                if (isNavigatingHolder) {
                    Timber.d("dropping update; isNavigatingHolder == TRUE")
                    return@collect
                }

                if (info != null && info.keyAdded) {
                    isNavigatingHolder = true
                    findNavController().popBackStack(R.id.twetchProfileFragment, false)
                    return@collect
                }


                when (status) {
                    Auth.SUCCESS -> {
                        isNavigatingHolder = true
                        findNavController().popBackStack(R.id.twetchProfileFragment, false)
                    }
                    Auth.NOT_LINKED -> {
                        isNavigatingHolder = true
                        findNavController().navigate(R.id.action_twetchAuthFragment_to_twetchAuthSigningAddressFragment)
                    }
                    else -> {
                        Timber.d("twetch auth-state:$status")
                    }
                }
            }



        }


    }



}