package app.bitcoin.baemail.twetch.ui

import android.os.Parcelable
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@FlowPreview
@ExperimentalCoroutinesApi
class TwetchTopUseCase(
    private val activeUserId: String,
    private val coroutineUtil: CoroutineUtil,
    private val postListStore: TwetchPostListStore
) {

    companion object {
        const val PAGE_NOT_LOADED = -1
    }

    private var isSetup = false

    private val _content = MutableStateFlow(Model(
        TopPostTab.DAY,
        null,
        null,
        null,
        null,
        null
    ))
    val content: StateFlow<Model>
        get() {
            ensureSetup()
            return _content
        }



    private val _metaState = MutableStateFlow(MetaState(
        true,
        true,
        true,
        true,
        true,
        false,
        false,
        false,
        false,
        false
    ))
    val metaState: StateFlow<MetaState>
        get() = _metaState



    var postListDataObservationJob: Job? = null
    var postListRefreshingObservationJob: Job? = null


    var cachedHourListState: Parcelable? = null
    var cachedDayListState: Parcelable? = null
    var cachedWeekListState: Parcelable? = null
    var cachedMonthListState: Parcelable? = null
    var cachedAllListState: Parcelable? = null


    var cachedTabsScrolledAmount = 0


    private val hourListHelper = TwetchPostListHelper(
        activeUserId,
        null,
        ListType.TOP_HOUR,
        coroutineUtil,
        postListStore
    )

    private val dayListHelper = TwetchPostListHelper(
        activeUserId,
        null,
        ListType.TOP_DAY,
        coroutineUtil,
        postListStore
    )

    private val weekListHelper = TwetchPostListHelper(
        activeUserId,
        null,
        ListType.TOP_WEEK,
        coroutineUtil,
        postListStore
    )

    private val monthListHelper = TwetchPostListHelper(
        activeUserId,
        null,
        ListType.TOP_MONTH,
        coroutineUtil,
        postListStore
    )

    private val allListHelper = TwetchPostListHelper(
        activeUserId,
        null,
        ListType.TOP_ALL,
        coroutineUtil,
        postListStore
    )

    fun onTabChanged(newTab: TopPostTab): Boolean {
        if (_content.value.activeTab == newTab) return false

        _content.value = _content.value.copy(activeTab = newTab)
        return true
    }

    fun onRefreshRequested() {
        when (_content.value.activeTab) {
            TopPostTab.HOUR -> {
                hourListHelper.refreshFromStart()
            }
            TopPostTab.DAY -> {
                dayListHelper.refreshFromStart()
            }
            TopPostTab.WEEK -> {
                weekListHelper.refreshFromStart()
            }
            TopPostTab.MONTH -> {
                monthListHelper.refreshFromStart()
            }
            TopPostTab.ALL -> {
                allListHelper.refreshFromStart()
            }
        }
    }

    private fun ensureSetup() {
        if (isSetup) {
            return
        }
        isSetup = true

        hourListHelper.ensureSetup()
        dayListHelper.ensureSetup()
        weekListHelper.ensureSetup()
        monthListHelper.ensureSetup()
        allListHelper.ensureSetup()



        postListDataObservationJob?.cancel()
        postListDataObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            //todo maybe consider using kotlin-channels??
            hourListHelper.data.combine(dayListHelper.data) { hour, day ->
                ListDataHolder(
                    hour,
                    day,
                    null,
                    null,
                    null
                )
            }.combine(weekListHelper.data) { holder, week ->
                holder.copy(
                    week = week
                )
            }.combine(monthListHelper.data) { holder, month ->
                holder.copy(
                    month = month
                )
            }.combine(allListHelper.data) { holder, all ->
                holder.copy(
                    all = all
                )
            }.collectLatest { holder ->
                delay(15)
                _content.value = _content.value.copy(
                    hour = holder.hour,
                    day = holder.day,
                    week = holder.week,
                    month = holder.month,
                    all = holder.all
                )
            }
        }

        postListRefreshingObservationJob?.cancel()
        postListRefreshingObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            //todo maybe consider using kotlin-channels??
            hourListHelper.refreshing.combine(dayListHelper.refreshing) { hour, day ->
                RefreshingHolder(
                    hour,
                    day,
                    null,
                    null,
                    null
                )
            }.combine(weekListHelper.refreshing) { holder, week ->
                holder.copy(
                    week = week
                )
            }.combine(monthListHelper.refreshing) { holder, month ->
                holder.copy(
                    month = month
                )
            }.combine(allListHelper.refreshing) { holder, all ->
                holder.copy(
                    all = all
                )
            }.collect { holder ->
                _metaState.value = _metaState.value.copy(
                    hourRefreshing = holder.hour!!,
                    dayRefreshing = holder.day!!,
                    weekRefreshing = holder.week!!,
                    monthRefreshing = holder.month!!,
                    allRefreshing = holder.all!!
                )
            }

        }

    }

    fun requestNextPage() {
        when (_content.value.activeTab) {
            TopPostTab.HOUR -> {
                hourListHelper.userRequestedNextPage()
            }
            TopPostTab.DAY -> {
                dayListHelper.userRequestedNextPage()
            }
            TopPostTab.WEEK -> {
                weekListHelper.userRequestedNextPage()
            }
            TopPostTab.MONTH -> {
                monthListHelper.userRequestedNextPage()
            }
            TopPostTab.ALL -> {
                allListHelper.userRequestedNextPage()
            }
        }
    }

    fun cleanup() {
        isSetup = false

        postListDataObservationJob?.cancel()
        postListRefreshingObservationJob?.cancel()

        hourListHelper.cleanup()
        dayListHelper.cleanup()
        weekListHelper.cleanup()
        monthListHelper.cleanup()
        allListHelper.cleanup()

        _content.value = Model(
            TopPostTab.DAY,
            null,
            null,
            null,
            null,
            null
        )
        _metaState.value = MetaState(
            true,
            true,
            true,
            true,
            true,
            false,
            false,
            false,
            false,
            false
        )
    }





    enum class TopPostTab {
        HOUR,
        DAY,
        WEEK,
        MONTH,
        ALL
    }

    data class Model(
        val activeTab: TopPostTab,
        val hour: TwetchPostModel?,
        val day: TwetchPostModel?,
        val week: TwetchPostModel?,
        val month: TwetchPostModel?,
        val all: TwetchPostModel?
    )

    data class MetaState(
        val hourRefreshing: Boolean,
        val dayRefreshing: Boolean,
        val weekRefreshing:Boolean,
        val monthRefreshing: Boolean,
        val allRefreshing: Boolean,
        val hourError: Boolean,
        val dayError: Boolean,
        val weekError:Boolean,
        val monthError: Boolean,
        val allError: Boolean,

    )

    data class ListDataHolder(
        val hour: TwetchPostModel?,
        val day: TwetchPostModel?,
        val week: TwetchPostModel?,
        val month: TwetchPostModel?,
        val all: TwetchPostModel?
    )

    data class RefreshingHolder(
        val hour: Boolean?,
        val day: Boolean?,
        val week: Boolean?,
        val month: Boolean?,
        val all: Boolean?
    )
}