package app.bitcoin.baemail.twetch.ui

import android.content.res.Resources
import android.util.Base64
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.data.wallet.ExchangeRateHelper
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.twetch.db.TwetchDatabase
import app.bitcoin.baemail.core.data.util.ConnectivityLiveData
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import timber.log.Timber

class TwetchActionHelper(
    private val resources: Resources,
    private val coroutineUtil: CoroutineUtil,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val postCache: TwetchPostCache,
    private val twetchDatabase: TwetchDatabase,
    private val gson: Gson,
    private val coinManagementUseCase: CoinManagementUseCase,
    private val connectivityLiveData: ConnectivityLiveData,
    private val composePostUseCase: TwetchNewPostUseCase,
    private val coinFlipSignalHelper: CoinFlipSignalHelper,
    private val notifyStartOfAction: (String)->Boolean,
    private val notifyEndOfAction: (String)->Unit,
    private val getActiveUserId: ()->String
) {
    companion object {
        const val ERROR_GENERIC = "Action failed"
        const val ERROR_CHAIN_SYNC_IN_PROGRESS = "Funding wallet is synchronizing coins"
        const val ERROR_NO_FUNDS = "No funds"
        const val ERROR_NO_INTERNET = "Not connected to internet"
        const val ERROR_INVALID_ACTION = "Invalid action"

        private const val CENTS_IN_LIKE = 5
        private const val CENTS_IN_FOLLOW = 10
        private const val CENTS_IN_UNFOLLOW = 0
        private const val CENTS_IN_BRANCH = 3
        private const val CENTS_IN_BRANCH_AND_LIKE = CENTS_IN_BRANCH + CENTS_IN_LIKE
        private const val CENTS_IN_REPLY = 2
        private const val CENTS_IN_NEW_POST = 2
        private const val CENTS_IN_QUOTE_BRANCH = 3
    }

    private val _snackbarChannel = Channel<TwetchSnackbarHelper.Model>()
    val snackbarChannel: ReceiveChannel<TwetchSnackbarHelper.Model>
        get() = _snackbarChannel


    init {
        composePostUseCase.actionHelper = this
    }

    private fun postErrorInChannel(errorValue: String) {
        coroutineUtil.appScope.launch {
            doPostErrorInChannel(errorValue)
        }
    }

    private suspend fun doPostErrorInChannel(errorValue: String) {
        _snackbarChannel.send(TwetchSnackbarHelper.Model(
            TwetchSnackbarHelper.Type.ERROR,
            errorValue
        ))
    }

    private suspend fun doPostPop(content: String) {
        _snackbarChannel.send(TwetchSnackbarHelper.Model(
            TwetchSnackbarHelper.Type.POP,
            content
        ))
    }


    fun executeLikeOfPost(postTransaction: String, parentId: String?): Boolean {

        //check has internet
        val hasInternet = connectivityLiveData.value?.isConnected ?: false
        if (!hasInternet) {
            postErrorInChannel(ERROR_NO_INTERNET)
            return false
        }

        val coinState = coinManagementUseCase.state.value

        //check sync has finished
        if (ChainSyncStatus.LIVE != coinState.syncStatus) {
            postErrorInChannel(ERROR_CHAIN_SYNC_IN_PROGRESS)
            return false
        }



        //check wallet has enough $$$
        var hasFunds = false
        if (ExchangeRateHelper.INVALID_STATE != coinState.exchangeRate) {
            val satsRequired = (CENTS_IN_LIKE + 1) * coinState.exchangeRate.satsInACent
            hasFunds = satsRequired < coinState.satsAvailable
        }

        if (!hasFunds) {
            postErrorInChannel(ERROR_NO_FUNDS)
            return false
        }


        val actionId = "like_$postTransaction"
        val notified = notifyStartOfAction(actionId)
        if (!notified) return false //disallow accidental double-clicks .. no error in this situation

        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            try {

                launch {
                    delay(600)
                    coinFlipSignalHelper.onSuccessLikingAPost(postTransaction, parentId)
                }



                val result = nodeRuntimeRepository.runAsyncScript("""
                    let millisStart = new Date().getTime();
            
                    await context.twetch.ensureAuthHasCompleted();
                    
                    let millisPostAuth = new Date().getTime();
                    
                    
                    
                    
                    let action = 'twetch/like@0.0.1';
                    let payload = {
                        postTransaction: '$postTransaction'
                    };
                    
                    let response = await context.twetch.postTwetchAction(action, payload);
                    
                    
                    
                    let millisPostPublish = new Date().getTime();
                    
                    console.log("time spent awaiting twetch-auth:" 
                    + (millisPostAuth - millisStart) 
                    + "ms; time spent on publishing a like of post:" 
                    + (millisPostPublish - millisPostAuth)
                    + "ms");
                    
                    
                    
                    return JSON.stringify(response);
                    
                """.trimIndent())

                if (result is RunScript.Response.Success) {
                    Timber.d("success-no-crash: ${result.content}")


                    val json = gson.fromJson(result.content, JsonObject::class.java)
                    val published = json.get("published").asBoolean
                    val error = json.getAsJsonArray("errors").let { errorArray ->
                        if (errorArray.size() == 0)  null
                        else errorArray[0].asString.replace("\n", "; ")
                    }

                    if (published) {
                        //update the cached post-entry
                        val post = postCache.getPost(getActiveUserId(), postTransaction)!!
                        post.onLikedByMe()
                        postCache.setPost(getActiveUserId(), post)
                        postCache.notifyUpdated()

                        doPostPop(resources.getString(R.string.spent_pop_content, CENTS_IN_LIKE))

                    } else {
                        //create an error-notification
                        val finalError = error ?: ERROR_GENERIC
                        doPostErrorInChannel(finalError)

                        postCache.notifyUpdated() //this will force refresh of adapter items undoing the `like`

                    }


                } else {
                    result as RunScript.Response.Fail

                    Timber.e(result.throwable, "RIP:" + result.responseData)

                    doPostErrorInChannel(ERROR_GENERIC)

                    postCache.notifyUpdated() //this will force refresh of adapter items undoing the `like`
                }

            } finally {
                notifyEndOfAction(actionId)
            }
        }


        return true
    }

    fun executeFollowOfUser(userId: String): Boolean {

        //disallow following self
        if (getActiveUserId.toString() == userId) {
            postErrorInChannel(ERROR_INVALID_ACTION)
            return false
        }

        //check has internet
        val hasInternet = connectivityLiveData.value?.isConnected ?: false
        if (!hasInternet) {
            postErrorInChannel(ERROR_NO_INTERNET)
            return false
        }

        val coinState = coinManagementUseCase.state.value

        //check sync has finished
        if (ChainSyncStatus.LIVE != coinState.syncStatus) {
            postErrorInChannel(ERROR_CHAIN_SYNC_IN_PROGRESS)
            return false
        }



        //check wallet has enough $$$
        var hasFunds = false
        if (ExchangeRateHelper.INVALID_STATE != coinState.exchangeRate) {
            val satsRequired = (CENTS_IN_FOLLOW + 1) * coinState.exchangeRate.satsInACent
            hasFunds = satsRequired < coinState.satsAvailable
        }

        if (!hasFunds) {
            postErrorInChannel(ERROR_NO_FUNDS)
            return false
        }


        val actionId = "follow_$userId"
        val notified = notifyStartOfAction(actionId)
        if (!notified) return false //disallow accidental double-clicks .. no error in this situation

        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            try {

                val result = nodeRuntimeRepository.runAsyncScript("""
                    let millisStart = new Date().getTime();
            
                    await context.twetch.ensureAuthHasCompleted();
                    
                    let millisPostAuth = new Date().getTime();
                    
                    
                    
                    
                    let action = 'twetch/follow@0.0.1';
                    let payload = {
                        userId: '${getActiveUserId()}',
                        followerUserId: '$userId'
                    };
                    
                    let response = await context.twetch.postTwetchAction(action, payload);
                    
                    
                    
                    let millisPostPublish = new Date().getTime();
                    
                    console.log("time spent awaiting twetch-auth:" 
                    + (millisPostAuth - millisStart) 
                    + "ms; time spent on publishing a follow of user:" 
                    + (millisPostPublish - millisPostAuth)
                    + "ms");
                    
                    
                    
                    return JSON.stringify(response);
                    
                """.trimIndent())


                if (result is RunScript.Response.Success) {
                    Timber.d("success-no-crash: ${result.content}")


                    val json = gson.fromJson(result.content, JsonObject::class.java)
                    val published = json.get("published").asBoolean
                    val error = json.getAsJsonArray("errors").let { errorArray ->
                        if (errorArray.size() == 0)  null
                        else errorArray[0].asString.replace("\n", "; ")
                    }

                    if (published) {
                        //update the cached post-entry
                        val profile = twetchDatabase.userProfileBaseDao().getFlow(
                            getActiveUserId(),
                            userId
                        ).first()

                        profile!!.onFollowedByMe()

                        twetchDatabase.userProfileBaseDao().insert(profile)


                        doPostPop(resources.getString(R.string.spent_pop_content, CENTS_IN_FOLLOW))

                    } else {
                        //create an error-notification
                        val finalError = error ?: ERROR_GENERIC
                        doPostErrorInChannel(finalError)

                        twetchDatabase.userProfileBaseDao().notifyUpdated() //this will force refresh of adapter items undoing the `follow`

                    }


                } else {
                    result as RunScript.Response.Fail

                    Timber.e(result.throwable, "RIP:" + result.responseData)

                    doPostErrorInChannel(ERROR_GENERIC)

                    twetchDatabase.userProfileBaseDao().notifyUpdated() //this will force refresh of adapter items undoing the `follow`
                }



            } finally {
                notifyEndOfAction(actionId)
            }

        }

        return true
    }

    fun executeUnfollowOfUser(userId: String): Boolean {

        //disallow following self
        if (getActiveUserId.toString() == userId) {
            postErrorInChannel(ERROR_INVALID_ACTION)
            return false
        }

        //check has internet
        val hasInternet = connectivityLiveData.value?.isConnected ?: false
        if (!hasInternet) {
            postErrorInChannel(ERROR_NO_INTERNET)
            return false
        }

        val coinState = coinManagementUseCase.state.value

        //check sync has finished
        if (ChainSyncStatus.LIVE != coinState.syncStatus) {
            postErrorInChannel(ERROR_CHAIN_SYNC_IN_PROGRESS)
            return false
        }



        //check wallet has enough $$$
        var hasFunds = false
        if (ExchangeRateHelper.INVALID_STATE != coinState.exchangeRate) {
            val satsRequired = (CENTS_IN_UNFOLLOW + 1) * coinState.exchangeRate.satsInACent
            hasFunds = satsRequired < coinState.satsAvailable
        }

        if (!hasFunds) {
            postErrorInChannel(ERROR_NO_FUNDS)
            return false
        }


        val actionId = "unfollow_$userId"
        val notified = notifyStartOfAction(actionId)
        if (!notified) return false //disallow accidental double-clicks .. no error in this situation

        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            try {

                val result = nodeRuntimeRepository.runAsyncScript("""
                    let millisStart = new Date().getTime();
            
                    await context.twetch.ensureAuthHasCompleted();
                    
                    let millisPostAuth = new Date().getTime();
                    
                    
                    
                    let action = 'twetch/unfollow@0.0.1';
                    let payload = {
                        userId: '${getActiveUserId()}',
                        followerUserId: '$userId'
                    };
                    
                    let response = await context.twetch.postTwetchAction(action, payload);
                    
                    
                    
                    let millisPostPublish = new Date().getTime();
                    
                    console.log("time spent awaiting twetch-auth:" 
                    + (millisPostAuth - millisStart) 
                    + "ms; time spent on publishing a unfollow of user:" 
                    + (millisPostPublish - millisPostAuth)
                    + "ms");
                    
                    
                    return JSON.stringify(response);
                    
                """.trimIndent())


                if (result is RunScript.Response.Success) {
                    Timber.d("success-no-crash: ${result.content}")


                    val json = gson.fromJson(result.content, JsonObject::class.java)
                    val published = json.get("published").asBoolean
                    val error = json.getAsJsonArray("errors").let { errorArray ->
                        if (errorArray.size() == 0)  null
                        else errorArray[0].asString.replace("\n", "; ")
                    }

                    if (published) {
                        //update the cached post-entry
                        val profile = twetchDatabase.userProfileBaseDao().getFlow(
                            getActiveUserId(),
                            userId
                        ).first()

                        profile!!.onUnfollowedByMe()

                        twetchDatabase.userProfileBaseDao().insert(profile)


                        doPostPop(resources.getString(R.string.spent_pop_content, CENTS_IN_UNFOLLOW))

                    } else {
                        //create an error-notification
                        val finalError = error ?: ERROR_GENERIC
                        doPostErrorInChannel(finalError)

                        twetchDatabase.userProfileBaseDao().notifyUpdated() //this will force refresh of adapter items undoing the `unfollow`

                    }


                } else {
                    result as RunScript.Response.Fail

                    Timber.e(result.throwable, "RIP:" + result.responseData)

                    doPostErrorInChannel(ERROR_GENERIC)

                    twetchDatabase.userProfileBaseDao().notifyUpdated() //this will force refresh of adapter items undoing the `unfollow`
                }



            } finally {
                notifyEndOfAction(actionId)
            }

        }

        return true
    }


    fun executeBranch(postTransaction: String, parentId: String?): Boolean {
        Timber.d("executeBranch ...postTransaction:$postTransaction")



        //check has internet
        val hasInternet = connectivityLiveData.value?.isConnected ?: false
        if (!hasInternet) {
            postErrorInChannel(ERROR_NO_INTERNET)
            return false
        }

        val coinState = coinManagementUseCase.state.value

        //check sync has finished
        if (ChainSyncStatus.LIVE != coinState.syncStatus) {
            postErrorInChannel(ERROR_CHAIN_SYNC_IN_PROGRESS)
            return false
        }



        //check wallet has enough $$$
        var hasFunds = false
        if (ExchangeRateHelper.INVALID_STATE != coinState.exchangeRate) {
            val satsRequired = (CENTS_IN_BRANCH + 1) * coinState.exchangeRate.satsInACent
            hasFunds = satsRequired < coinState.satsAvailable
        }

        if (!hasFunds) {
            postErrorInChannel(ERROR_NO_FUNDS)
            return false
        }


        val actionId = "branch_$postTransaction"
        val notified = notifyStartOfAction(actionId)
        if (!notified) return false //disallow accidental double-clicks .. no error in this situation



        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            try {

                //update the persisted value
                val post = postCache.getPost(getActiveUserId(), postTransaction)!!
                post.onBranchedByMe()
                postCache.setPost(getActiveUserId(), post)
                postCache.notifyUpdated()



                launch {
                    delay(600)
                    coinFlipSignalHelper.onSuccessBranchingAPost(postTransaction, parentId)
                }



                val result = nodeRuntimeRepository.runAsyncScript("""
                    let millisStart = new Date().getTime();
            
                    await context.twetch.ensureAuthHasCompleted();
                    
                    let millisPostAuth = new Date().getTime();
                    
                    
                    
                    
                    let action = 'twetch/post@0.0.1';
                    let payload = {
                        bContent: 'https://twetch.com/t/' + '$postTransaction',
                        bContentType: 'text/plain',
                        bEncoding: 'text',
                        bFilename: 'twetch_twtext_' + millisPostAuth + '.txt'
                    };
                    
                    let response = await context.twetch.postTwetchAction(action, payload);
                    
                    
                    
                    let millisPostPublish = new Date().getTime();
                    
                    console.log("time spent awaiting twetch-auth:" 
                    + (millisPostAuth - millisStart) 
                    + "ms; time spent on publishing a branch of post:" 
                    + (millisPostPublish - millisPostAuth)
                    + "ms");
                    
                    
                    
                    return JSON.stringify(response);
                    
                """.trimIndent())

                if (result is RunScript.Response.Success) {
                    Timber.d("success-no-crash: ${result.content}")


                    val json = gson.fromJson(result.content, JsonObject::class.java)
                    val published = json.get("published").asBoolean
                    val error = json.getAsJsonArray("errors").let { errorArray ->
                        if (errorArray.size() == 0)  null
                        else errorArray[0].asString.replace("\n", "; ")
                    }

                    if (published) {
                        //update the cached post-entry ... not necessary it was done before publishing
//                        val post = postCache.getPost(getActiveUserId(), postTransaction)!!
//                        post.onBranchedByMe()
//                        postCache.setPost(getActiveUserId(), post)
//                        postCache.notifyUpdated()


                        doPostPop(resources.getString(R.string.spent_pop_content, CENTS_IN_BRANCH))

                    } else {
                        //create an error-notification
                        val finalError = error ?: ERROR_GENERIC
                        doPostErrorInChannel(finalError)

                        //must undo the branch-by-me call
                        val post0 = postCache.getPost(getActiveUserId(), postTransaction)!!
                        post0.onBranchingError()
                        postCache.setPost(getActiveUserId(), post0)
                        postCache.notifyUpdated()

                    }


                } else {
                    result as RunScript.Response.Fail

                    Timber.e(result.throwable, "RIP:" + result.responseData)

                    doPostErrorInChannel(ERROR_GENERIC)

                    postCache.notifyUpdated() //this will force refresh of adapter items undoing the `like`
                }

            } finally {
                notifyEndOfAction(actionId)
            }
        }


        return true
    }



    fun executeBranchAndLike(postTransaction: String, parentId: String?): Boolean {
        Timber.d("executeBranchAndLike ...postTxId:$postTransaction ...not implemented")



        //check has internet
        val hasInternet = connectivityLiveData.value?.isConnected ?: false
        if (!hasInternet) {
            postErrorInChannel(ERROR_NO_INTERNET)
            return false
        }

        val coinState = coinManagementUseCase.state.value

        //check sync has finished
        if (ChainSyncStatus.LIVE != coinState.syncStatus) {
            postErrorInChannel(ERROR_CHAIN_SYNC_IN_PROGRESS)
            return false
        }



        //check wallet has enough $$$
        var hasFunds = false
        if (ExchangeRateHelper.INVALID_STATE != coinState.exchangeRate) {
            val satsRequired = (CENTS_IN_BRANCH_AND_LIKE + 1) * coinState.exchangeRate.satsInACent
            hasFunds = satsRequired < coinState.satsAvailable
        }

        if (!hasFunds) {
            postErrorInChannel(ERROR_NO_FUNDS)
            return false
        }


        val actionId = "branchAndLike_$postTransaction"
        val notified = notifyStartOfAction(actionId)
        if (!notified) return false //disallow accidental double-clicks .. no error in this situation



        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            try {

                //update the persisted value
                val post = postCache.getPost(getActiveUserId(), postTransaction)!!
                post.onBranchedByMe()
                post.onLikedByMe()
                postCache.setPost(getActiveUserId(), post)
                postCache.notifyUpdated()


                launch {
                    delay(600)
                    coinFlipSignalHelper.onSuccessBranchAndLikingAPost(postTransaction, parentId)
                }


                val result = nodeRuntimeRepository.runAsyncScript("""
                    let millisStart = new Date().getTime();
            
                    await context.twetch.ensureAuthHasCompleted();
                    
                    let millisPostAuth = new Date().getTime();
                    
                    
                    
                    
                    let action = 'twetch/branch-and-like@0.0.1';
                    let payload = {
                        bContent: 'https://twetch.com/t/' + '$postTransaction',
                        postTransaction: '$postTransaction',
                        bContentType: 'text/plain',
                        bEncoding: 'text',
                        bFilename: 'twetch_twtext_' + millisPostAuth + '.txt'
                    };
                    
                    let response = await context.twetch.postTwetchAction(action, payload);
                    
                    
                    
                    let millisPostPublish = new Date().getTime();
                    
                    console.log("time spent awaiting twetch-auth:" 
                    + (millisPostAuth - millisStart) 
                    + "ms; time spent on publishing a branch of post:" 
                    + (millisPostPublish - millisPostAuth)
                    + "ms");
                    
                    
                    
                    return JSON.stringify(response);
                    
                """.trimIndent())

                if (result is RunScript.Response.Success) {
                    Timber.d("success-no-crash: ${result.content}")


                    val json = gson.fromJson(result.content, JsonObject::class.java)
                    val published = json.get("published").asBoolean
                    val error = json.getAsJsonArray("errors").let { errorArray ->
                        if (errorArray.size() == 0)  null
                        else errorArray[0].asString.replace("\n", "; ")
                    }

                    if (published) {
                        //update the cached post-entry ... not necessary it was done before publishing
//                        val post = postCache.getPost(getActiveUserId(), postTransaction)!!
//                        post.onBranchedByMe()
//                        postCache.setPost(getActiveUserId(), post)
//                        postCache.notifyUpdated()


                        doPostPop(resources.getString(R.string.spent_pop_content, CENTS_IN_BRANCH_AND_LIKE))

                    } else {
                        //create an error-notification
                        val finalError = error ?: ERROR_GENERIC
                        doPostErrorInChannel(finalError)

                        //must undo the branch-by-me call
                        val post0 = postCache.getPost(getActiveUserId(), postTransaction)!!
                        post0.onBranchingError()
                        post0.onLikingError()
                        postCache.setPost(getActiveUserId(), post0)
                        postCache.notifyUpdated()

                    }


                } else {
                    result as RunScript.Response.Fail

                    Timber.e(result.throwable, "RIP:" + result.responseData)

                    doPostErrorInChannel(ERROR_GENERIC)

                    postCache.notifyUpdated() //this will force refresh of adapter items undoing the `like`
                }

            } finally {
                notifyEndOfAction(actionId)
            }
        }


        return true
    }






    fun executeQuoteBranch(
        postTransaction: String,
        content: String,
        callback: (Boolean)->Unit
    ): Boolean {
        Timber.d("executeQuoteBranch ...postTransaction:$postTransaction content:$content")



        //check has internet
        val hasInternet = connectivityLiveData.value?.isConnected ?: false
        if (!hasInternet) {
            postErrorInChannel(ERROR_NO_INTERNET)
            return false
        }

        val coinState = coinManagementUseCase.state.value

        //check sync has finished
        if (ChainSyncStatus.LIVE != coinState.syncStatus) {
            postErrorInChannel(ERROR_CHAIN_SYNC_IN_PROGRESS)
            return false
        }



        //check wallet has enough $$$
        var hasFunds = false
        if (ExchangeRateHelper.INVALID_STATE != coinState.exchangeRate) {
            val satsRequired = (CENTS_IN_QUOTE_BRANCH + 1) * coinState.exchangeRate.satsInACent
            hasFunds = satsRequired < coinState.satsAvailable
        }

        if (!hasFunds) {
            postErrorInChannel(ERROR_NO_FUNDS)
            return false
        }


        val actionId = "quote_branch_$postTransaction"
        val notified = notifyStartOfAction(actionId)
        if (!notified) return false //disallow accidental double-clicks .. no error in this situation



        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            try {

                val result = nodeRuntimeRepository.runAsyncScript("""
                    let millisStart = new Date().getTime();
            
                    await context.twetch.ensureAuthHasCompleted();
                    
                    let millisPostAuth = new Date().getTime();
                    
                    let action = 'twetch/post@0.0.1';
                    let payload = {
                        bContent: `$content https://twetch.com/t/` + '$postTransaction',
                        bContentType: 'text/plain',
                        bEncoding: 'text',
                        bFilename: 'twetch_twtext_' + millisPostAuth + '.txt'
                    };
                    
                    let response = await context.twetch.postTwetchAction(action, payload);
                    
                    
                    
                    let millisPostPublish = new Date().getTime();
                    
                    console.log("time spent awaiting twetch-auth:" 
                    + (millisPostAuth - millisStart) 
                    + "ms; time spent on publishing a reply of post:" 
                    + (millisPostPublish - millisPostAuth)
                    + "ms");
                    
                    
                    
                    return JSON.stringify(response);
                    
                """.trimIndent())

                if (result is RunScript.Response.Success) {
                    Timber.d("success-no-crash: ${result.content}")


                    val json = gson.fromJson(result.content, JsonObject::class.java)
                    val published = json.get("published").asBoolean
                    val error = json.getAsJsonArray("errors").let { errorArray ->
                        if (errorArray.size() == 0)  null
                        else errorArray[0].asString.replace("\n", "; ")
                    }

                    if (published) {
                        doPostPop(resources.getString(R.string.spent_pop_content, CENTS_IN_QUOTE_BRANCH))
                        //do nothing ATM

                    } else {
                        //create an error-notification
                        val finalError = error ?: ERROR_GENERIC
                        doPostErrorInChannel(finalError)

                    }

                    callback(published)


                } else {
                    result as RunScript.Response.Fail

                    Timber.e(result.throwable, "RIP:" + result.responseData)

                    doPostErrorInChannel(ERROR_GENERIC)

                    callback(false)
                }

            } finally {
                notifyEndOfAction(actionId)
            }
        }


        return true
    }

    fun executeReply(
        postTransaction: String,
        content: String,
        callback: (Boolean)->Unit
    ): Boolean {
        Timber.d("executeReply ...postTransaction:$postTransaction")



        //check has internet
        val hasInternet = connectivityLiveData.value?.isConnected ?: false
        if (!hasInternet) {
            postErrorInChannel(ERROR_NO_INTERNET)
            return false
        }

        val coinState = coinManagementUseCase.state.value

        //check sync has finished
        if (ChainSyncStatus.LIVE != coinState.syncStatus) {
            postErrorInChannel(ERROR_CHAIN_SYNC_IN_PROGRESS)
            return false
        }



        //check wallet has enough $$$
        var hasFunds = false
        if (ExchangeRateHelper.INVALID_STATE != coinState.exchangeRate) {
            val satsRequired = (CENTS_IN_REPLY + 1) * coinState.exchangeRate.satsInACent
            hasFunds = satsRequired < coinState.satsAvailable
        }

        if (!hasFunds) {
            postErrorInChannel(ERROR_NO_FUNDS)
            return false
        }


        val actionId = "reply_$postTransaction"
        val notified = notifyStartOfAction(actionId)
        if (!notified) return false //disallow accidental double-clicks .. no error in this situation



        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            try {

                val result = nodeRuntimeRepository.runAsyncScript("""
                    let millisStart = new Date().getTime();
            
                    await context.twetch.ensureAuthHasCompleted();
                    
                    let millisPostAuth = new Date().getTime();
                    
                    
                    
                    
                    let action = 'twetch/post@0.0.1';
                    let payload = {
                        bContent: `$content`,
                        bContentType: 'text/plain',
                        mapReply: '$postTransaction',
                        bEncoding: 'text',
                        bFilename: 'twetch_twtext_' + millisPostAuth + '.txt'
                    };
                    
                    let response = await context.twetch.postTwetchAction(action, payload);
                    
                    
                    
                    let millisPostPublish = new Date().getTime();
                    
                    console.log("time spent awaiting twetch-auth:" 
                    + (millisPostAuth - millisStart) 
                    + "ms; time spent on publishing a reply of post:" 
                    + (millisPostPublish - millisPostAuth)
                    + "ms");
                    
                    
                    
                    return JSON.stringify(response);
                    
                """.trimIndent())

                if (result is RunScript.Response.Success) {
                    Timber.d("success-no-crash: ${result.content}")


                    val json = gson.fromJson(result.content, JsonObject::class.java)
                    val published = json.get("published").asBoolean
                    val error = json.getAsJsonArray("errors").let { errorArray ->
                        if (errorArray.size() == 0)  null
                        else errorArray[0].asString.replace("\n", "; ")
                    }

                    if (published) {
                        doPostPop(resources.getString(R.string.spent_pop_content, CENTS_IN_REPLY))
                        //do nothing ATM

                    } else {
                        //create an error-notification
                        val finalError = error ?: ERROR_GENERIC
                        doPostErrorInChannel(finalError)

                    }

                    callback(published)


                } else {
                    result as RunScript.Response.Fail

                    Timber.e(result.throwable, "RIP:" + result.responseData)

                    doPostErrorInChannel(ERROR_GENERIC)

                    callback(false)
                }

            } finally {
                notifyEndOfAction(actionId)
            }
        }


        return true
    }

    fun executeNewPost(
        content: String,
        callback: (Boolean)->Unit
    ): Boolean {
        Timber.d("executeNewPost")

        val encoded = Base64.encodeToString(content.toByteArray(), Base64.DEFAULT)


        //check has internet
        val hasInternet = connectivityLiveData.value?.isConnected ?: false
        if (!hasInternet) {
            postErrorInChannel(ERROR_NO_INTERNET)
            return false
        }

        val coinState = coinManagementUseCase.state.value

        //check sync has finished
        if (ChainSyncStatus.LIVE != coinState.syncStatus) {
            postErrorInChannel(ERROR_CHAIN_SYNC_IN_PROGRESS)
            return false
        }



        //check wallet has enough $$$
        var hasFunds = false
        if (ExchangeRateHelper.INVALID_STATE != coinState.exchangeRate) {
            val satsRequired = (CENTS_IN_NEW_POST + 1) * coinState.exchangeRate.satsInACent
            hasFunds = satsRequired < coinState.satsAvailable
        }

        if (!hasFunds) {
            postErrorInChannel(ERROR_NO_FUNDS)
            return false
        }


        val actionId = "new_post_$encoded"
        val notified = notifyStartOfAction(actionId)
        if (!notified) return false //disallow accidental double-clicks .. no error in this situation



        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            try {

                val result = nodeRuntimeRepository.runAsyncScript("""
                    let millisStart = new Date().getTime();
            
                    await context.twetch.ensureAuthHasCompleted();
                    
                    let millisPostAuth = new Date().getTime();
                    
                    
                    
                    
                    let action = 'twetch/post@0.0.1';
                    let payload = {
                        bContent: `$content`,
                        bContentType: 'text/plain',
                        bEncoding: 'text',
                        bFilename: 'twetch_twtext_' + millisPostAuth + '.txt'
                    };
                    
                    let response = await context.twetch.postTwetchAction(action, payload);
                    
                    
                    
                    let millisPostPublish = new Date().getTime();
                    
                    console.log("time spent awaiting twetch-auth:" 
                    + (millisPostAuth - millisStart) 
                    + "ms; time spent on publishing a new twetch post:" 
                    + (millisPostPublish - millisPostAuth)
                    + "ms");
                    
                    
                    
                    return JSON.stringify(response);
                    
                """.trimIndent())

                if (result is RunScript.Response.Success) {
                    Timber.d("success-no-crash: ${result.content}")


                    val json = gson.fromJson(result.content, JsonObject::class.java)
                    val published = json.get("published").asBoolean
                    val error = json.getAsJsonArray("errors").let { errorArray ->
                        if (errorArray.size() == 0)  null
                        else errorArray[0].asString.replace("\n", "; ")
                    }

                    if (published) {
                        doPostPop(resources.getString(R.string.spent_pop_content, CENTS_IN_NEW_POST))
                        //do nothing ATM

                    } else {
                        //create an error-notification
                        val finalError = error ?: ERROR_GENERIC
                        doPostErrorInChannel(finalError)

                    }

                    callback(published)


                } else {
                    result as RunScript.Response.Fail

                    Timber.e(result.throwable, "RIP:" + result.responseData)

                    doPostErrorInChannel(ERROR_GENERIC)

                    callback(false)
                }

            } finally {
                notifyEndOfAction(actionId)
            }
        }


        return true
    }



    fun startReply(post: PostModel) {
        coroutineUtil.appScope.launch {
            composePostUseCase.prepareReply(post)

        }
    }

}