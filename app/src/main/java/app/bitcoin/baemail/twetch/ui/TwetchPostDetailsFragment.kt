package app.bitcoin.baemail.twetch.ui

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.graphics.ColorUtils
import androidx.core.widget.TextViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableCircleProgress
import app.bitcoin.baemail.intercept.AppDeepLink
import app.bitcoin.baemail.intercept.AppDeepLinkType
import app.bitcoin.baemail.core.presentation.util.Event
import app.bitcoin.baemail.core.presentation.util.RelativeTimeHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.VideoClipManager
import app.bitcoin.baemail.core.presentation.view.recycler.ATContentLoading0
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchPostDetailMainItem
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchPostItem
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchPostLoadingItem
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchPostSecondaryLoadingItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.math.min

class TwetchPostDetailsFragment : Fragment(R.layout.fragment_twetch_post_details) {

    companion object {
        const val KEY_POST_TX_ID = "tx_id"
    }

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory


    private lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel

    private lateinit var helper: TwetchPostDetailsUseCase

    private val postItemHelper: TwetchPostItemHelper by lazy {
        TwetchPostItemHelper(
            requireContext(),
            viewModel.postUserCache,
            TextViewCompat.getTextMetricsParams(dummyBodyText)
        )
    }


//    private val mainPostDateFormat: java.text.DateFormat by lazy {
//        DateFormat.getMediumDateFormat(requireContext())
//    }
//    private val mainPostTimeFormat: java.text.DateFormat by lazy {
//        DateFormat.getTimeFormat(requireContext())
//    }



    private lateinit var topbar: View
    private lateinit var close: Button
    private lateinit var refresh: Button
    private lateinit var recycler: RecyclerView
    private lateinit var loadingIndicator: ImageView
    private lateinit var retry: TextView
    private lateinit var title: TextView

    private lateinit var dummyBodyText: TextView



    lateinit var layoutManager: LinearLayoutManager
//    private var lastSavedRecyclerState: Parcelable? = null

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter

    private var wasCreatedOnce = false

    private var listRestoredOnce = false


    private val dp: Float by lazy {
        resources.displayMetrics.density
    }

    private val colorOnSurfaceSubtle: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorOnSurfaceSubtle)
    }

    private val colorDanger: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorDanger)
    }

    private val colorPrimaryTransparent: Int by lazy {
        ColorUtils.setAlphaComponent(colorDanger, 123)
    }

    private lateinit var drawableLoadingProgress: DrawableCircleProgress


    private val bottomHeightLD = MutableLiveData<Int>()
    private val bottomHeightItem = ATHeightDecorDynamic("0", bottomHeightLD)


    private val altLoadingItemLD = MutableLiveData<Int>()
    private val altLoadingItem = ATHeightDecorDynamic("1", altLoadingItemLD)

    private var indexOfMainPostItem = -1

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(TwetchUtilViewModel::class.java)


        val postTxId = requireArguments().getString(KEY_POST_TX_ID)!!


        helper = viewModel.getPostDetailsContent(postTxId)




        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true








        dummyBodyText = requireView().findViewById(R.id.dummy_body_text)

        topbar = requireView().findViewById(R.id.topbar)
        close = requireView().findViewById(R.id.close)
        refresh = requireView().findViewById(R.id.refresh)
        recycler = requireView().findViewById(R.id.list)
        loadingIndicator = requireView().findViewById(R.id.loading_indicator)
        retry = requireView().findViewById(R.id.retry)
        title = requireView().findViewById(R.id.title)



        bottomHeightLD.value = (80 * dp).toInt()
        altLoadingItemLD.value = (108 * dp).toInt()


        //////////////////////

        val colorPrimary = requireContext().getColorFromAttr(R.attr.colorPrimary)
        val colorTransparentPrimary = Color.argb(
            200,
            Color.red(colorPrimary),
            Color.green(colorPrimary),
            Color.blue(colorPrimary)
        )

        drawableLoadingProgress = DrawableCircleProgress(requireContext())
        drawableLoadingProgress.setStyle(CircularProgressDrawable.DEFAULT)
        drawableLoadingProgress.setColorSchemeColors(
            requireContext().getColorFromAttr(R.attr.colorOnPrimary))
        loadingIndicator.setImageDrawable(drawableLoadingProgress)
        loadingIndicator.setBackgroundColor(colorTransparentPrimary)

        loadingIndicator.clipToOutline = true
        loadingIndicator.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val rounding = min(view.width / 2f, view.height / 2f)
                outline.setRoundRect(
                    0,
                    0,
                    view.width,
                    view.height,
                    rounding
                )
            }

        }

        //wire up progress animation
        viewLifecycleOwner.lifecycle.addObserver(object : DefaultLifecycleObserver {

            var resumed = false
            var showing = false

            init {
                drawableLoadingProgress.visibilityListener = { visible ->
                    showing = visible
                    initAnimationState()
                }
            }

            override fun onResume(owner: LifecycleOwner) {
                resumed = true
                initAnimationState()
            }

            override fun onPause(owner: LifecycleOwner) {
                resumed = false
                initAnimationState()
            }

            private fun initAnimationState() {
                if (resumed && showing) {
                    drawableLoadingProgress.start()
                } else {
                    drawableLoadingProgress.stop()
                }
            }
        })


        ////////////////////////////


        retry.clipToOutline = true
        retry.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, p0.height / 2f)
            }
        }
        val dangerColor = requireContext().getColorFromAttr(R.attr.colorDanger)
        val transparentDangerColor = Color.argb(
            200,
            Color.red(dangerColor),
            Color.green(dangerColor),
            Color.blue(dangerColor)
        )
        retry.background = ColorDrawable(transparentDangerColor)

        retry.setOnClickListener {
            if (helper.content.value.page <= 0) {
                if (helper.onRefreshRequested()) {
                    if (indexOfMainPostItem != -1) {
                        recycler.smoothScrollToPosition(indexOfMainPostItem)
                    }
                }
            } else {
                helper.onRetryLastRequested()
            }
        }


        ////////////////////////////


        val topbarElevatedHeight = resources.getDimension(R.dimen.inner_topbar_floating_elevation) / dp
        val topbarBackground = MaterialShapeDrawable.createWithElevationOverlay(requireContext(), 0f)

        topbar.background = topbarBackground





        val contentLoadingListener = object : BaseListAdapter.ATContentLoadingListener {
            override fun onBound() {
                if (helper.metaState.value.refreshing) {
                    Timber.d("contentLoadingListener#onBound ...dropped because refreshing")
                    return
                }

                swapOutRefreshingItem()
            }
        }


        val postListener = TwetchPostListener(
            requireContext(),
            findNavController(),
            viewModel.actionHelper,
            viewModel.videoClipManager,
            R.id.action_global_twetchUserProfile,
            R.id.action_twetchPostDetailsFragment7_to_twetchChatFullImageFragment8,
            R.id.action_twetchPostDetailsFragment7_self,
            R.id.action_twetchPostDetailsFragment7_to_twetchBranchOptionsFragment,
            R.id.action_twetchPostDetailsFragment7_to_twetchVideoClipFragment7,
            viewModel.coinFlipSignalHelper.coinFlipChannel
        )
        val mainPostListener = MainPostListener(
            requireContext(),
            findNavController(),
            postListener,
            viewModel.videoClipManager
        )

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atContentLoadingListener = contentLoadingListener,
                atTwetchPostListener = postListener,
                atTwetchPostDetailsMainPostListener = mainPostListener
            )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                when (layoutManager.findFirstCompletelyVisibleItemPosition()) {
                    RecyclerView.NO_POSITION -> false
                    0 -> false
                    else -> true
                }.let { showShadow ->
                    if (showShadow) {
                        utilViewModel.onMainContentScrollChanged(false)
                        title.elevation = topbarElevatedHeight * dp
                        topbarBackground.elevation = topbarElevatedHeight
                        topbar.elevation = topbarElevatedHeight * dp
                        close.elevation = topbarElevatedHeight * dp
                        refresh.elevation = topbarElevatedHeight * dp
                    } else {
                        utilViewModel.onMainContentScrollChanged(true)
                        title.elevation = 0f
                        topbarBackground.elevation = 0f
                        topbar.elevation = 0f
                        close.elevation = 0f
                        refresh.elevation = 0f
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (RecyclerView.SCROLL_STATE_IDLE == newState) {
                    Timber.d("new state saved")
                    cacheCurrentScrollState()
                }
            }
        })








        close.setOnClickListener {
            findNavController().popBackStack()
        }

        refresh.setOnClickListener {
            if (helper.onRefreshRequested()) {
                if (indexOfMainPostItem != -1) {
                    recycler.smoothScrollToPosition(indexOfMainPostItem)
                }
            }
        }







        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            helper.metaState.collect {
                refreshLoadingIndicatorState(it.refreshing)
                refreshErrorIndicatorState(it.error)
            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {

            var lastAppliedDetails: TwetchPostDetailsStore.PostDetails? = null

//            val separatorItemAbove = ATTwetchSeparatorItem("0", colorOnSurfaceSubtle, dp.toInt())
//            val separatorItemBelow = ATTwetchSeparatorItem("1", colorPrimaryTransparent, (2.1 * dp).toInt())

            helper.content.collect { model ->

                if (lastAppliedDetails == model.post) {
                    Timber.d("post-details are unchanged; returning")
                    return@collect
                }
                lastAppliedDetails = model.post



                awaitRecyclerAnimationsFinish()

                val itemList = mutableListOf<AdapterType>()

                if (model.post.main == null) {

                    itemList.add(ATTwetchPostSecondaryLoadingItem("0"))
//                    itemList.add(separatorItemAbove)
                    itemList.add(ATTwetchPostLoadingItem("0"))
//                    itemList.add(separatorItemBelow)
                    itemList.add(ATTwetchPostSecondaryLoadingItem("reply_0"))

                    indexOfMainPostItem = -1
                    adapter.setItems(itemList)

                    if (TwetchPostDetailsUseCase.PAGE_NOT_LOADED == model.page) {
                        helper.userRequestedNextPage()
                    }

                    return@collect
                }


                //do not layout branches without comment; load the branched post instead
                if (model.post.main.post.isBranch && !model.post.main.post.isBranchWithComment) {
                    findNavController().popBackStack()
                    viewModel.deepLinkHelper.requestNavigateToLink(AppDeepLink(
                        AppDeepLinkType.TWETCH_POST,
                        model.post.main.post.referencedPost!!
                    ))
                    return@collect
                }



                val isRefreshing = helper.metaState.value.refreshing

                val loadingItem = if (!isRefreshing && model.hasMorePages) {
                    ATContentLoading0(model.page)
                } else {
                    altLoadingItem
                }


                withContext(Dispatchers.IO) {
                    val relativeTimeHelper = RelativeTimeHelper(
                        requireContext(),
                        System.currentTimeMillis()
                    )

                    model.post.main.post.parents.forEachIndexed { index, parent ->
                        val matchingHolder = model.post.parents[parent.transaction]
                        if (matchingHolder == null) {
                            itemList.add(
                                ATTwetchPostSecondaryLoadingItem(
                                index.toString(),
                                isLoadingPostParent = true,
                                isLoadingFirstOfPostParents = index == 0
                            )
                            )
                        } else {
                            val postItem = assemblePostItem(
                                matchingHolder,
                                relativeTimeHelper,
                                isParent = true,
                                isFirstOfParents = index == 0
                            )
                            itemList.add(postItem)
                        }
                    }


                    val mainPost = assembleMainPostItem(
                        model.post.main,
                        relativeTimeHelper
                    )
                    itemList.add(mainPost)
                    indexOfMainPostItem = itemList.size - 1

                    if (model.post.replies == null) {
                        itemList.add(ATTwetchPostSecondaryLoadingItem("reply_0"))
                    } else {
                        model.post.replies.forEach { replyHolder ->
                            itemList.add(assemblePostItem(
                                replyHolder,
                                relativeTimeHelper,
                                isReply = true
                            ))
                        }
                    }

                }


                Timber.d("new post-details-list; size: ${itemList.size}; page:${model.page} hasMorePages:${model.hasMorePages} isRefreshing:$isRefreshing")

                itemList.add(loadingItem)
                itemList.add(bottomHeightItem)

                adapter.setItems(itemList)


                if (!listRestoredOnce) {
                    listRestoredOnce = true
                    layoutManager.onRestoreInstanceState(helper.cachedListState)
                }
            }

        }


    }

    /////////////////////////

    data class LoadingAnimationState(
        val loadingAnimator: ValueAnimator,
        private var _mustBeShowing: Boolean
    ) {
        val mustBeShowing: Boolean
            get() { return _mustBeShowing }

        fun ensureShowing() {
            if (_mustBeShowing) return
            _mustBeShowing = true
            loadingAnimator.start()
        }

        fun ensureHidden() {
            if (!_mustBeShowing) return
            _mustBeShowing = false
            loadingAnimator.reverse()
        }
    }
    private var loadingAnimatorState: LoadingAnimationState? = null

    private fun ensureLoadingAnimatorInitialized(): LoadingAnimationState {
        val animState = loadingAnimatorState
        if (animState != null) return animState

        val animator = ValueAnimator.ofFloat(0f, 1f)
        animator.duration = 300L
        animator.addUpdateListener(object : ValueAnimator.AnimatorUpdateListener {
            val scaleMin = 0.1f
            val scaleMax = 1f
            val alphaMin = 0.4f
            val alphaMax = 1f

            override fun onAnimationUpdate(animation: ValueAnimator) {
                val progress = animator.animatedFraction

                if (progress == 0f) {
                    if (loadingIndicator.visibility != View.INVISIBLE) {
                        loadingIndicator.visibility = View.INVISIBLE
                    }
                } else if (progress > 0f) {
                    if (loadingIndicator.visibility != View.VISIBLE) {
                        loadingIndicator.visibility = View.VISIBLE
                    }
                }

                val currentScale = scaleMin + (scaleMax - scaleMin) * progress
                val currentAlpha = alphaMin + (alphaMax - alphaMin) * progress

                loadingIndicator.alpha = currentAlpha
                loadingIndicator.scaleX = currentScale
                loadingIndicator.scaleY = currentScale
            }
        })

        LoadingAnimationState(animator, false).let {
            loadingAnimatorState = it
            return it
        }
    }


    private fun refreshLoadingIndicatorState(refreshing: Boolean) {
        val animatorState = ensureLoadingAnimatorInitialized()

        if (refreshing) {
            animatorState.ensureShowing()
        } else{
            animatorState.ensureHidden()
        }
    }

    //////////////////

    private fun refreshErrorIndicatorState(didError: Boolean) {
        if (didError) {
            if (retry.visibility != View.VISIBLE) {
                retry.visibility = View.VISIBLE
            }
        } else {
            if (retry.visibility != View.INVISIBLE) {
                retry.visibility = View.INVISIBLE
            }
        }
    }






    private fun assemblePostItem(
        holder: PostHolder,
        relativeTimeHelper: RelativeTimeHelper,
        isParent: Boolean = false,
        isReply: Boolean = false,
        isFirstOfParents: Boolean = false
    ): ATTwetchPostItem {
        return postItemHelper.createItem(
            holder,
            relativeTimeHelper,
            isParent,
            isReply,
            isFirstOfParents
        )
    }


    private fun assembleMainPostItem(
        holder: PostHolder,
        relativeTimeHelper: RelativeTimeHelper
    ): ATTwetchPostDetailMainItem {
        return postItemHelper.createMainPostItem(
            holder,
            relativeTimeHelper
        )
    }


    private fun cacheCurrentScrollState() {
        helper.cachedListState = layoutManager.onSaveInstanceState()
    }








    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!wasCreatedOnce) return

        recycler.stopScroll()
        cacheCurrentScrollState()
    }








    private suspend fun awaitRecyclerAnimationsFinish(fromListChange: Boolean = true) {
        if (fromListChange) {
            swapOutRefreshingItemJob?.cancel()
            swapOutRefreshingItemJob = null
        }

        recycler.itemAnimator?.let { animator ->
            //making sure that the any previous animation get completed
            if (!animator.isRunning) {
                return@let
            }

            suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                animator.isRunning { cancellableContinuation.resume(Unit) }
            }
        }

        suspendCancellableCoroutine<Unit> { continuation ->
            if (recycler.isComputingLayout) {
                recycler.post {
                    continuation.resume(Unit)
                }
            } else {
                continuation.resume(Unit)
            }

        }
    }







    var swapOutRefreshingItemJob: Job? = null

    private fun swapOutRefreshingItem() {
        if (swapOutRefreshingItemJob?.isActive == true) return

        swapOutRefreshingItemJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            awaitRecyclerAnimationsFinish(false)

            val items = adapter.getCopyOfItems()

            var loadingItemPos = -1

            val countToCheck = min(5, items.size)
            for (i in 0 until countToCheck) {
                val item = items[items.lastIndex - i]
                if (item is ATContentLoading0) {
                    loadingItemPos = items.lastIndex - i
                    break
                }
            }

            ensureActive()

            if (loadingItemPos == -1) {
                Timber.d(">>> loadingItemPost not found")
                helper.userRequestedNextPage()
                return@launchWhenStarted
            }

            adapter.swapOutItem(loadingItemPos, altLoadingItem)

            awaitRecyclerAnimationsFinish(false)
            helper.userRequestedNextPage()
        }
    }












    class MainPostListener(
        private val context: Context,
        private val navController: NavController,
        private val postListener: TwetchPostListener,
        private val videoClipManager: VideoClipManager
    ) : BaseListAdapter.ATTwetchPostDetailsMainPostListener {
        override fun onContentClicked(holder: PostHolder, ofPost: PostModel) {
            if (ofPost != holder.post) {
                postListener.onContentClicked(holder, ofPost)
            } else {
                Toast.makeText(context, "copy post content", Toast.LENGTH_SHORT).show()
            }
        }

        override fun onUnloadedPostClicked(holder: PostHolder, ofPost: PostModel, txId: String) {
            postListener.onUnloadedPostClicked(holder, ofPost, txId)
        }

        override fun onImageClicked(holder: PostHolder, allImageUrlList: List<String>) {
            postListener.onImageClicked(holder, allImageUrlList)
        }

        override fun onProfileClicked(holder: PostHolder, ofPost: PostModel) {
            postListener.onProfileClicked(holder, ofPost)
        }

        override fun onLikeClicked(holder: PostHolder, ofPost: PostModel): Boolean {
            return postListener.onLikeClicked(holder, ofPost)
        }

        override fun onCommentClicked(holder: PostHolder, ofPost: PostModel) {
            postListener.onCommentClicked(holder, ofPost)
        }

        override fun onBranchClicked(holder: PostHolder, ofPost: PostModel) {
            postListener.onBranchClicked(holder, ofPost)
        }

        override fun onCopyLinkClicked(holder: PostHolder, ofPost: PostModel) {
            postListener.onCopyLinkClicked(holder, ofPost)
        }

        override fun onCreatedLabelClicked(holder: PostHolder, ofPost: PostModel) {
            postListener.onCreatedLabelClicked(holder, ofPost)
        }

        override fun onTweetClicked(holder: PostHolder, ofPost: PostModel) {
            postListener.onTweetClicked(holder, ofPost)
        }

        override fun onViewLikesClicked(holder: PostHolder, ofPost: PostModel) {
            Toast.makeText(context, "open view-likes screen", Toast.LENGTH_SHORT).show()
        }

        override fun onViewBranchesClicked(holder: PostHolder, ofPost: PostModel) {
            postListener.onBranchClicked(holder, ofPost)
        }

        override fun onUserIdBadgeClicked(holder: PostHolder, userId: String) {
            postListener.onUserIdBadgeClicked(holder, userId)
        }

        override fun getVideoClipManager(): VideoClipManager {
            return videoClipManager
        }

        override fun onVideoClipClicked(holder: PostHolder, videoTxId: String) {
            postListener.onVideoClipClicked(holder, videoTxId)
        }

        override fun getCoinFlipSuccessSignal(): StateFlow<Event<CoinFlipModel>?> {
            return postListener.getCoinFlipSuccessSignal()
        }

    }



}