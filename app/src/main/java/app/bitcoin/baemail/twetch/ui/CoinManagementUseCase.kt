package app.bitcoin.baemail.twetch.ui

import app.bitcoin.baemail.core.data.wallet.ChainSyncStatus
import app.bitcoin.baemail.core.data.wallet.DynamicChainSync
import app.bitcoin.baemail.core.data.wallet.ExchangeRate
import app.bitcoin.baemail.core.data.wallet.ExchangeRateHelper
import app.bitcoin.baemail.core.data.room.dao.CoinDao
import app.bitcoin.baemail.core.data.room.entity.Coin
import app.bitcoin.baemail.core.data.util.ConnectivityLiveData
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.AuthRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch

//todo rename to CoinManagementHelper
class CoinManagementUseCase(
    private val authRepository: AuthRepository,
    private val coinDao: CoinDao,
    private val exchangeRateHelper: ExchangeRateHelper,
    private val chainSync: DynamicChainSync,
    private val connectivityLiveData: ConnectivityLiveData,
    private val coroutineUtil: CoroutineUtil
) {

    companion object {
        private const val MIN_USD_ABOVE_LOW_FUNDS = 1F
        private const val MIN_COINS_ABOVE_TOO_FEW_COINS = 5

        val UNINITIALIZED = Model(
            ChainSyncStatus.DISCONNECTED,
            FundState.NO_FUNDS,
            0,
            ExchangeRateHelper.INVALID_STATE,
            -99,
            0,
            false
        )
    }

    private var lastInitializedPaymail: String? = null

    private var coinObservationJob: Job? = null

    private val availableCoins = MutableStateFlow(emptyList<Coin>())


    private val _state = MutableStateFlow(UNINITIALIZED)

    val state: StateFlow<Model>
        get() = _state.asStateFlow()


    init {
        coroutineUtil.appScope.launch {
            authRepository.activePaymail.collectLatest { activePaymail ->
                activePaymail?.paymail ?: return@collectLatest

                if (lastInitializedPaymail == activePaymail.paymail) return@collectLatest
                lastInitializedPaymail = activePaymail.paymail

                coinObservationJob?.cancel()
                coinObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
                    availableCoins.value = emptyList()
                    coinDao.getUnspentFundingCoinFlow(activePaymail.paymail).collectLatest {
                        delay(400)
                        availableCoins.value = it
                    }
                }
            }
        }

        coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            availableCoins.combine(chainSync.status) { coinList, syncStatus ->
                coinList to syncStatus
            }.combine(exchangeRateHelper.state) { (coinList, syncStatus), exchangeRate ->
                Triple(coinList, syncStatus, exchangeRate)
            }.collectLatest { (coinList, syncStatus, exchangeRate) ->

                delay(500)

                if (ExchangeRateHelper.INVALID_STATE == exchangeRate.rate) return@collectLatest

                val satsAvailable = coinList.fold(0L) { acc, coin ->
                    acc + coin.sats
                }

                val usdAvailable = satsAvailable / (exchangeRate.rate.satsInACent * 100f)

                val fundState = when {
                    coinList.isEmpty() -> FundState.NO_FUNDS
                    usdAvailable < MIN_USD_ABOVE_LOW_FUNDS -> FundState.LOW_FUNDS
                    MIN_COINS_ABOVE_TOO_FEW_COINS > coinList.size -> FundState.TOO_FEW_COINS
                    else -> FundState.ACCEPTABLE
                }

                _state.value = Model(
                    syncStatus,
                    fundState,
                    satsAvailable,
                    exchangeRate.rate,
                    exchangeRate.rate.satsInACent,
                    coinList.size,
                    connectivityLiveData.value?.isConnected ?: false
                )
            }
        }
    }


    data class Model(
        val syncStatus: ChainSyncStatus,
        val fundStatus: FundState,
        val satsAvailable: Long,
        val exchangeRate: ExchangeRate,
        val satsInACent: Int,
        val coinCount: Int,
        val hasInternet: Boolean
    )

    enum class FundState {
        NO_FUNDS,
        LOW_FUNDS,
        TOO_FEW_COINS,
        ACCEPTABLE
    }
}