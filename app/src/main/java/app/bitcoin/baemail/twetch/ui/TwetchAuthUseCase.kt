package app.bitcoin.baemail.twetch.ui

import app.bitcoin.baemail.core.data.wallet.AppStateLiveData
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import com.dropbox.android.external.store4.ResponseOrigin
import com.dropbox.android.external.store4.StoreResponse
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import timber.log.Timber

class TwetchAuthUseCase(
    private val coroutineUtil: CoroutineUtil,
    private val appStateLD: AppStateLiveData,
    private val profileDatastore: TwetchAuthStore,
    private val updateInitializedForPaymail: (String) -> Unit
) {

    private val _authState = MutableStateFlow(Auth.NOT_STARTED)
    val authState: StateFlow<Auth>
        get() = _authState

    private var _authInfo = MutableStateFlow<TwetchAuthStore.AuthInfo?>(null)
    val authInfo: StateFlow<TwetchAuthStore.AuthInfo?>
        get() = _authInfo

    private var authObservationJob: Job? = null

    fun clear() {
        _authState.value = Auth.NOT_STARTED
        _authInfo.value = null

        authObservationJob?.cancel()
        authObservationJob = null

    }

    fun refreshAuth(isForced: Boolean) {
        val paymail = appStateLD.value?.activePaymail ?: throw RuntimeException()

        updateInitializedForPaymail(paymail)

        authObservationJob?.cancel()
        authObservationJob = coroutineUtil.appScope.launch(coroutineUtil.defaultDispatcher) {

            var refreshingHolder = true
            var errorHolder = false
            var infoHolder: TwetchAuthStore.AuthInfo? = null

            val key = TwetchAuthStore.AuthKey(paymail, isForced)


            //todo is this step really necessary? seems a bit redundant
            //eagerly return the cached content
            profileDatastore.getFromSourceOfTruth(key)?.let {
                infoHolder = it

                Timber.d("eagerly returned auth-info: $it")
                _authInfo.value = it
            }

            profileDatastore.getFlowForKey(key).collect { response ->

                when (response) {
                    is StoreResponse.Loading -> {
                        refreshingHolder = true

                    }
                    is StoreResponse.Data<*> -> {
                        if (response.origin == ResponseOrigin.Fetcher) {
                            refreshingHolder = false
                            errorHolder = false
                        } else if (response.origin == ResponseOrigin.SourceOfTruth
                            || response.origin == ResponseOrigin.Cache) {
                            //refreshingHolder = false
                        }

                        infoHolder = response.requireData()


                    }
                    is StoreResponse.Error -> {
                        if (response.origin == ResponseOrigin.Fetcher) {
                            //no longer loading
                            refreshingHolder = false
                        }

                        //show error
                        errorHolder = true

                    }
                    is StoreResponse.NoNewData -> {
                        refreshingHolder = false
                        Timber.d("StoreResponse.NoNewData")
                    }
                }





                //update state scoped to this repository
                //
                //

                val newState = if (errorHolder) {
                    Auth.ERROR
                } else if (refreshingHolder && infoHolder == null) {
                    Auth.STARTED
                } else if (refreshingHolder && infoHolder != null) {
                    Auth.STARTED //SUCCESS
                } else if (infoHolder != null) {
                    if (infoHolder!!.keyAdded) {
                        Auth.SUCCESS
                    } else {
                        Auth.NOT_LINKED
                    }
                } else {
                    Auth.NOT_LINKED
                }

                infoHolder?.let {
                    _authInfo.value = it
                }

                Timber.d("authState.value = $newState")
                _authState.value = newState

            }
        }

    }


}


enum class Auth {
    NOT_STARTED,
    STARTED,
    SUCCESS,
    NOT_LINKED,
    ERROR
}

