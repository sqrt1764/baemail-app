package app.bitcoin.baemail.twetch.ui

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.TextView
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.widget.TextViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.bitcoin.baemail.R
import app.bitcoin.baemail.fundingWallet.core.presentation.CoinActivity
import app.bitcoin.baemail.fundingWallet.TopUpActivity
import app.bitcoin.baemail.intercept.AppDeepLinkType
import app.bitcoin.baemail.core.presentation.util.Event
import app.bitcoin.baemail.core.presentation.util.RelativeTimeHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.VideoClipManager
import app.bitcoin.baemail.core.presentation.view.ViewCoinManagementHelper
import app.bitcoin.baemail.core.presentation.view.recycler.ATContentLoading0
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchNoPostsFoundItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber
import java.util.ArrayList
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.math.min

class TwetchNotificationsFragment : Fragment(R.layout.fragment_twetch_notifications) {

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel

    private lateinit var helper: TwetchNotificationUseCase


    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var recycler: RecyclerView
    private lateinit var scrollToTop: TextView
    private lateinit var coinManagementHelper: ViewCoinManagementHelper

    private lateinit var dummyBodyText: TextView

    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter

    private var wasCreatedOnce = false

    private var listRestoredOnce = false

    private val noPostsFoundItem = ATTwetchNoPostsFoundItem("0") //todo

    private val topHeightLD = MutableLiveData<Int>()
    private val topHeightItem = ATHeightDecorDynamic("0", topHeightLD)

    private val altLoadingItemLD = MutableLiveData<Int>()
    private val altLoadingItem = ATHeightDecorDynamic("1", altLoadingItemLD)


    val cbm: ClipboardManager by lazy {
        requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }


    private val postItemHelper: TwetchPostItemHelper by lazy {
        TwetchPostItemHelper(
            requireContext(),
            viewModel.postUserCache,
            TextViewCompat.getTextMetricsParams(dummyBodyText)
        )
    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)


        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
                .get(TwetchUtilViewModel::class.java)


        helper = viewModel.getNotificationContent()

        wasCreatedOnce = true



        dummyBodyText = requireView().findViewById(R.id.dummy_body_text)

        swipeRefresh = requireView().findViewById(R.id.swipe_refresh)
        recycler = requireView().findViewById(R.id.list)
        scrollToTop = requireView().findViewById(R.id.scroll_to_top)
        coinManagementHelper = requireView().findViewById(R.id.coin_management_helper)


        val dp = resources.displayMetrics.density

        val colorPrimary = requireContext().getColorFromAttr(R.attr.colorPrimary)
        val colorTransparentPrimary = Color.argb(
            200,
            Color.red(colorPrimary),
            Color.green(colorPrimary),
            Color.blue(colorPrimary)
        )

        topHeightLD.value = (4 * dp).toInt()
        altLoadingItemLD.value = (108 * dp).toInt()

        swipeRefresh.setProgressViewOffset(true, (5 * dp).toInt(), (58 * dp).toInt())
        swipeRefresh.setColorSchemeColors(requireContext().getColorFromAttr(R.attr.colorOnPrimary))
        swipeRefresh.setProgressBackgroundColorSchemeColor(colorTransparentPrimary)
        swipeRefresh.setOnRefreshListener {
            helper.onRefreshRequested()
        }




        scrollToTop.clipToOutline = true
        scrollToTop.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, p0.height / 2f)
            }
        }
        scrollToTop.background = ColorDrawable(colorTransparentPrimary)

        scrollToTop.setOnClickListener {
            recycler.smoothScrollToPosition(0)
            scrollToTop.visibility = View.INVISIBLE
        }





        val contentLoadingListener = object : BaseListAdapter.ATContentLoadingListener {
            override fun onBound() {

                if (helper.metaState.value.refreshing) {
                    return
                }

                swapOutRefreshingItem()
            }
        }

        val notificationListener = object : BaseListAdapter.ATTwetchNotificationListener {
            override fun onContentClicked(notification: TwetchNotificationsStore.Notification) {
                notification.post?.let { p ->
                    val args = Bundle().also {
                        it.putString(TwetchPostDetailsFragment.KEY_POST_TX_ID, p.txId)

                    }
                    findNavController().navigate(R.id.action_global_twetchPostDetails, args)
                }

                val args = Bundle().also {
                    val userId = notification.actor.id

                    it.putString(TwetchUserProfileFragment.KEY_USER_ID, userId)
                }
                findNavController().navigate(R.id.action_global_twetchUserProfile, args)
            }

            override fun onActorClicked(notification: TwetchNotificationsStore.Notification) {
                val args = Bundle().also {
                    val userId = notification.actor.id

                    it.putString(TwetchUserProfileFragment.KEY_USER_ID, userId)
                }
                findNavController().navigate(R.id.action_global_twetchUserProfile, args)
            }

            override fun onPostClicked(notification: TwetchNotificationsStore.Notification) {
                notification.post?.let { p ->
                    val args = Bundle().also {
                        it.putString(TwetchPostDetailsFragment.KEY_POST_TX_ID, p.txId)

                    }
                    findNavController().navigate(R.id.action_global_twetchPostDetails, args)
                }
            }

            override fun onPostUserClicked(notification: TwetchNotificationsStore.Notification) {
                notification.post?.let { p ->
                    val args = Bundle().also {
                        val userId = p.user.id

                        it.putString(TwetchUserProfileFragment.KEY_USER_ID, userId)
                    }
                    findNavController().navigate(R.id.action_global_twetchUserProfile, args)
                }
            }

            override fun onPostTweetClicked(notification: TwetchNotificationsStore.Notification) {
                notification.post?.let { p ->
                    val twtId = p.tweet!!.twtId
                    val screenName = p.tweet.user.screenName

                    val url = "https://twitter.com/$screenName/status/$twtId"
                    val builder = CustomTabsIntent.Builder()
                    val customTabsIntent = builder.build()
                    customTabsIntent.launchUrl(requireContext(), Uri.parse(url))
                }
            }

            override fun onLikeClicked(notification: TwetchNotificationsStore.Notification): Boolean {
                return notification.post?.let { p ->
                    viewModel.actionHelper.executeLikeOfPost(p.txId, notification.id)
                } ?: false
            }

            override fun onCommentClicked(notification: TwetchNotificationsStore.Notification) {
                notification.post?.let { p ->
                    viewModel.actionHelper.startReply(p)
                }
            }

            override fun onBranchClicked(notification: TwetchNotificationsStore.Notification) {
                notification.post?.let { p ->
                    val args = Bundle().also {
                        it.putString(TwetchBranchOptionsFragment.KEY_POST_TX_ID, p.txId)
                        it.putString(TwetchBranchOptionsFragment.KEY_POST_PARENT_ID, notification.id)
                    }
                    findNavController().navigate(R.id.action_twetchNotificationsFragment_to_twetchBranchOptionsFragment8, args)
                }
            }

            override fun onCopyLinkClicked(notification: TwetchNotificationsStore.Notification) {
                notification.post?.let { p ->
                    cbm.let { manager ->
                        val url = "https://twetch.com/t/${p.txId}"
                        val clip = ClipData.newPlainText("Twetch post URL", url)

                        manager.setPrimaryClip(clip)
                        Toast.makeText(
                            requireContext(),
                            requireContext().getString(R.string.twetch_url_copied_info),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            override fun onUserIdBadgeClicked(
                notification: TwetchNotificationsStore.Notification,
                userId: String
            ) {
                val args = Bundle().also {
                    it.putString(TwetchUserProfileFragment.KEY_USER_ID, userId)
                }
                findNavController().navigate(R.id.action_global_twetchUserProfile, args)
            }

            override fun onUnloadedPostClicked(
                notification: TwetchNotificationsStore.Notification,
                txId: String
            ) {
                val args = Bundle().also {
                    it.putString(TwetchPostDetailsFragment.KEY_POST_TX_ID, txId)

                }
                findNavController().navigate(R.id.action_global_twetchPostDetails, args)
            }

            override fun onImageClicked(
                notification: TwetchNotificationsStore.Notification,
                allImageUrlList: List<String>
            ) {
                val args = Bundle().also {
                    it.putStringArrayList(TwetchChatFullImageFragment.KEY_IMAGE_URL, ArrayList(allImageUrlList))

                }
                findNavController().navigate(R.id.twetchChatFullImageFragment9, args)
            }

            override fun getVideoClipManager(): VideoClipManager {
                return viewModel.videoClipManager
            }

            override fun onVideoClipClicked(
                notification: TwetchNotificationsStore.Notification,
                videoTxId: String
            ) {
                val args = Bundle().also {
                    it.putString(TwetchVideoClipFragment.KEY_TX_ID, videoTxId)

                }
                findNavController().navigate(R.id.twetchVideoClipFragment8, args)
            }

            override fun getCoinFlipSuccessSignal(): StateFlow<Event<CoinFlipModel>?> {
                return viewModel.coinFlipSignalHelper.coinFlipChannel
            }

        }


        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atContentLoadingListener = contentLoadingListener,
                atTwetchNotificationListener = notificationListener
            )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var lastShowShadow: Boolean? = null

            var hasScrollUpBeenInitialized = false

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                recyclerView.canScrollVertically(-1).let { showShadow ->
                    if (lastShowShadow == showShadow) return@let
                    lastShowShadow = showShadow

                    utilViewModel.onMainContentScrollChanged(!showShadow)
                }

                if (!hasScrollUpBeenInitialized) {
                    refreshShowingOfScrollToTop()
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (RecyclerView.SCROLL_STATE_IDLE == newState) {
                    cacheCurrentScrollState()

                    refreshShowingOfScrollToTop()
                }
            }

            fun refreshShowingOfScrollToTop() {
                hasScrollUpBeenInitialized = true

                requireView().post {
                    if (layoutManager.findFirstVisibleItemPosition() > 10) {
                        if (scrollToTop.visibility != View.VISIBLE) {
                            scrollToTop.visibility = View.VISIBLE
                        }
                    } else {
                        if (scrollToTop.visibility != View.INVISIBLE) {
                            scrollToTop.visibility = View.INVISIBLE
                        }
                    }
                }
            }

        })




        coinManagementHelper.clickTopUpListener = {
            startActivity(Intent(requireActivity(), TopUpActivity::class.java))
        }

        coinManagementHelper.clickSplitCoinsListener = {
            startActivity(Intent(requireActivity(), CoinActivity::class.java))
        }

        coinManagementHelper.clickRetryReconnectListener = {
            viewModel.chainSync.considerRetry()
        }






        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            utilViewModel.mainContentTopDistance.collect {
                refreshDynamicBottomHeight()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.coinManagementUseCase.state.mapLatest { it }
                .combine(utilViewModel.snackBarShowingState) { coinModel, snackBarReservation ->
                    coinModel to snackBarReservation
                }.collectLatest { (coinModel, snackBarReservation) ->
                    if (!snackBarReservation) {
                        coinManagementHelper.applyModel(coinModel)
                    } else {
                        //apply a model that will result in this bar being hidden
                        coinManagementHelper.applyModel(ViewCoinManagementHelper.HELPER_HIDDEN)
                    }
                }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            helper.metaState.collectLatest {
                refreshLoadingIndicatorState()
            }
        }





        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            val supported = listOf(
                AppDeepLinkType.TWETCH_POST,
                AppDeepLinkType.TWETCH_USER
            )
            viewModel.deepLinkHelper.unsatisfiedLink.collectLatest { link ->
                link ?: return@collectLatest

                if (!supported.contains(link.type))return@collectLatest

                when (link.type) {
                    AppDeepLinkType.TWETCH_POST -> {
                        val args = Bundle().also {
                            it.putString(TwetchPostDetailsFragment.KEY_POST_TX_ID, link.value)

                        }
                        findNavController().navigate(R.id.action_global_twetchPostDetails, args)

                        viewModel.deepLinkHelper.informLinkNavigationSatisfied()

                    }
                    AppDeepLinkType.TWETCH_USER -> {
                        val args = Bundle().also {
                            it.putString(TwetchUserProfileFragment.KEY_USER_ID, link.value)
                        }
                        findNavController().navigate(R.id.action_global_twetchUserProfile, args)

                        viewModel.deepLinkHelper.informLinkNavigationSatisfied()

                    }
                    else -> {
                        //do nothing
                    }
                }

            }
        }



        viewLifecycleOwner.lifecycleScope.launchWhenStarted {

            var lastAppliedModel: TwetchNotificationUseCase.Model? = null

            helper.content.collect { model ->
                //Timber.d(">>>> $model")

                if (lastAppliedModel == model) {
                    Timber.d("notification-state is unchanged; returning")
                    return@collect
                }
                lastAppliedModel = model

                //refreshLoadingIndicatorState()
                awaitRecyclerAnimationsFinish()


                val isRefreshing = helper.metaState.value.refreshing

                val loadingItem = if (!isRefreshing && model.hasMorePages) {
                    ATContentLoading0(model.page)
                } else {
                    altLoadingItem
                }


                if (TwetchPostListHelper.PAGE_NOT_LOADED == model.page) {
                    Timber.d("model.page == PAGE_NOT_LOADED")
                    helper.userRequestedNextPage()
                }

                if (model.notifications.isEmpty()) {
                    if (model.hasMorePages) {
                        adapter.setItems(
                            listOf(
                                topHeightItem,
                                loadingItem
                            )
                        )
                    } else {
                        adapter.setItems(
                            listOf(
                                topHeightItem,
                                noPostsFoundItem
                            )
                        )
                    }


                } else {

                    val itemList = mutableListOf<AdapterType>()

                    withContext(Dispatchers.Default) {
                        val relativeTimeHelper = RelativeTimeHelper(
                            requireContext(),
                            System.currentTimeMillis()
                        )

                        model.notifications.map { notification ->
                            postItemHelper.createNotificationItem(notification, relativeTimeHelper)

                        }.toCollection(itemList)
                    }

                    Timber.d("new notification-list; size: ${itemList.size}; page:${model.page} hasMorePages:${model.hasMorePages} isRefreshing:$isRefreshing")


                    itemList.add(loadingItem)
                    itemList.add(0, topHeightItem)

                    adapter.setItems(itemList)

                    if (!listRestoredOnce) {
                        listRestoredOnce = true
                        layoutManager.onRestoreInstanceState(helper.cachedListState)
                    }

                    helper.onShouldInformSeen()

                }
            }
        }

    }






    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!wasCreatedOnce) return

        recycler.stopScroll()
        cacheCurrentScrollState()
    }



    private fun refreshLoadingIndicatorState() {
        val metaState = helper.metaState.value

        if (swipeRefresh.isRefreshing == metaState.refreshing) return
        swipeRefresh.isRefreshing = metaState.refreshing
    }



    private fun cacheCurrentScrollState() {
        helper.cachedListState = layoutManager.onSaveInstanceState()
    }





    private suspend fun awaitRecyclerAnimationsFinish(fromListChange: Boolean = true) {
        if (fromListChange) {
            swapOutRefreshingItemJob?.cancel()
            swapOutRefreshingItemJob = null
        }

        recycler.itemAnimator?.let { animator ->
            //making sure that the any previous animation get completed
            if (!animator.isRunning) {
                return@let
            }

            suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                animator.isRunning { cancellableContinuation.resume(Unit) }
            }
        }

        suspendCancellableCoroutine<Unit> { continuation ->
            if (recycler.isComputingLayout) {
                recycler.post {
                    continuation.resume(Unit)
                }
            } else {
                continuation.resume(Unit)
            }

        }
    }



    var swapOutRefreshingItemJob: Job? = null

    private fun swapOutRefreshingItem() {
        if (swapOutRefreshingItemJob?.isActive == true) return

        swapOutRefreshingItemJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            awaitRecyclerAnimationsFinish(false)

            val items = adapter.getCopyOfItems()

            var loadingItemPos = -1

            val countToCheck = min(5, items.size)
            for (i in 0 until countToCheck) {
                val item = items[items.lastIndex - i]
                if (item is ATContentLoading0) {
                    loadingItemPos = items.lastIndex - i
                    break
                }
            }

            ensureActive()

            if (loadingItemPos == -1) {
                Timber.d(">>> loadingItemPost not found")
                helper.userRequestedNextPage()
                return@launchWhenStarted
            }

            adapter.swapOutItem(loadingItemPos, altLoadingItem)

            awaitRecyclerAnimationsFinish(false)
            helper.userRequestedNextPage()
        }
    }



    private fun refreshDynamicBottomHeight() {
        val appbarOffset = utilViewModel.mainContentTopDistance.value

        coinManagementHelper.translationY = -1f * appbarOffset
    }





}