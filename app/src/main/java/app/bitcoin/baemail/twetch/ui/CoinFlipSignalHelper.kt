package app.bitcoin.baemail.twetch.ui

import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.presentation.util.Event
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class CoinFlipSignalHelper(
    val coroutineUtil: CoroutineUtil
) {

    private val _coinFlipChannel = MutableStateFlow<Event<CoinFlipModel>?>(null)
    val coinFlipChannel: StateFlow<Event<CoinFlipModel>?>
        get() = _coinFlipChannel


    fun onSuccessLikingAPost(postTxId: String, parentId: String?) {
        coroutineUtil.appScope.launch {
            _coinFlipChannel.value = Event(CoinFlipModel(
                postTxId,
                parentId,
                flipLike = true,
                flipBranch = false
            ))
        }

    }

    fun onSuccessBranchingAPost(postTxId: String, parentId: String?) {
        coroutineUtil.appScope.launch {
            _coinFlipChannel.value = Event(CoinFlipModel(
                postTxId,
                parentId,
                flipLike = false,
                flipBranch = true
            ))
        }

    }

    fun onSuccessBranchAndLikingAPost(postTxId: String, parentId: String?) {
        coroutineUtil.appScope.launch {
            _coinFlipChannel.value = Event(CoinFlipModel(
                postTxId,
                parentId,
                flipLike = true,
                flipBranch = true
            ))
        }
    }
}

data class CoinFlipModel(
    val postTxId: String,
    val parentId: String?,
    val flipLike: Boolean,
    val flipBranch: Boolean
)