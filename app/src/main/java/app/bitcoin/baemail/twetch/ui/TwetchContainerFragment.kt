package app.bitcoin.baemail.twetch.ui

import android.content.res.ColorStateList
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.HorizontalScrollView
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableNumberBadge
import app.bitcoin.baemail.core.presentation.drawable.DrawableState
import app.bitcoin.baemail.core.presentation.drawable.DrawableStroke
import app.bitcoin.baemail.intercept.AppDeepLinkType
import app.bitcoin.baemail.core.presentation.CentralViewModel
import app.bitcoin.baemail.core.presentation.util.FragmentStateHelper
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.button.MaterialButton
import com.google.android.material.progressindicator.LinearProgressIndicator
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.receiveAsFlow
import timber.log.Timber
import javax.inject.Inject

class TwetchContainerFragment : Fragment(R.layout.fragment_twetch_container) {

//    companion object {
//        private const val STATE_HELPER = "helper"
//    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant

    private lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel
    private lateinit var centralViewModel: CentralViewModel


    private var initializedSection: TwetchViewModel.Section? = null

    private lateinit var fragmentHelper: FragmentStateHelper
    private var navHost: NavHostFragment? = null


    private lateinit var container: CoordinatorLayout
    private lateinit var errorSnackbarContainer: CoordinatorLayout

    private lateinit var actionScroller: HorizontalScrollView
    private lateinit var actionProfile: MaterialButton
    private lateinit var actionChat: MaterialButton
    private lateinit var actionFollowing: MaterialButton
    private lateinit var actionLatest: MaterialButton
    private lateinit var actionTop: MaterialButton

    private lateinit var appBar: AppBarLayout
    private lateinit var topbarNew: AppCompatImageView
    private lateinit var topbarSearch: AppCompatImageView
    private lateinit var topbarNotifications: AppCompatImageView

    private lateinit var loadingIndicatorBar: LinearProgressIndicator




    val dp: Float by lazy {
        resources.displayMetrics.density
    }

    private val colorPrimary: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorPrimary)
    }
    private val colorDanger: Int by lazy {
        requireContext().getColorFromAttr(R.attr.colorDanger)
    }

    private val drawableTopbarNew: Drawable by lazy {
        R.drawable.ic_twetch_new.let {
            ContextCompat.getDrawable(requireContext(), it)!!.mutate()
        }
    }
    private val drawableTopbarSearch: Drawable by lazy {
        R.drawable.round_search_24.let {
            ContextCompat.getDrawable(requireContext(), it)!!.mutate()
        }
    }
    private val drawableTopbarNotifications: Drawable by lazy {
        R.drawable.baseline_notifications_none_24.let {
            ContextCompat.getDrawable(requireContext(), it)!!.mutate()
        }
    }

    private val notificationBadge: DrawableNumberBadge by lazy {
        DrawableNumberBadge(
            resources.displayMetrics.density,
            "",
            13 * resources.displayMetrics.scaledDensity,
            requireContext().getColorFromAttr(R.attr.colorOnPrimary),
            requireContext().getColorFromAttr(R.attr.colorDanger)
        )
    }

    private val chatBadge: DrawableNumberBadge by lazy {
        DrawableNumberBadge(
            resources.displayMetrics.density,
            "",
            13 * resources.displayMetrics.scaledDensity,
            requireContext().getColorFromAttr(R.attr.colorOnPrimary),
            requireContext().getColorFromAttr(R.attr.colorDanger)
        )
    }

    private val appBarBackground = DrawableStroke()

    private var wasCreatedOnce = false

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true


        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactory
        )[TwetchUtilViewModel::class.java]

        centralViewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[CentralViewModel::class.java]




        container = requireView().findViewById(R.id.container)
        errorSnackbarContainer = requireView().findViewById(R.id.error_snackbar_container)

        actionScroller = requireView().findViewById(R.id.header_scroller)
        actionProfile = requireView().findViewById(R.id.action_profile)
        actionChat = requireView().findViewById(R.id.action_chat)
        actionFollowing = requireView().findViewById(R.id.action_following)
        actionLatest = requireView().findViewById(R.id.action_latest)
        actionTop = requireView().findViewById(R.id.action_top)



        appBar = requireView().findViewById(R.id.app_bar)
        topbarSearch = requireView().findViewById(R.id.topbar_search)
        topbarNew = requireView().findViewById(R.id.topbar_new)
        topbarNotifications = requireView().findViewById(R.id.topbar_notifications)

        loadingIndicatorBar = requireView().findViewById(R.id.loading_indicator_bar)



        errorSnackbarContainer.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            WindowInsetsCompat.toWindowInsetsCompat(windowInsets).consumeSystemWindowInsets().toWindowInsets()
        }



        fragmentHelper = FragmentStateHelper(childFragmentManager)


        viewModel.getNavState()?.let { helperState ->
            fragmentHelper.restoreHelperState(helperState)

        }






        val topBarElevation = resources.getDimension(R.dimen.added_paymails_fragment__top_bar_elevation)





//        appBarBackground.setPadding(0, 0, 0, 0)
//        appBarBackground.setStrokeColor(requireContext().getColorFromAttr(R.attr.colorPrimary))
//        appBarBackground.setStrokeWidths(0, 0, 0, (2.1f * dp).toInt())
//        appBar.background = LayerDrawable(arrayOf(
//            ColorDrawable(requireContext().getColorFromAttr(R.attr.colorSurface)),
//            appBarBackground
//        ))



        appBar.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
                appBarLayout ?: return
                val calculated = verticalOffset + appBarLayout.height
                //Timber.d("offset:$verticalOffset calculated:$calculated")

                utilViewModel.onMainContentTopDistanceChanged(calculated)
            }

        })






        initNewPostButton()
        initActionButton(topbarSearch, drawableTopbarSearch)
        initActionButton(topbarNotifications, drawableTopbarNotifications)






        topbarNew.setOnClickListener {
            findNavController().navigate(R.id.action_twetchProfileFragment_to_twetchNewMessageFragment)
        }

        topbarSearch.setOnClickListener {
            if (!viewModel.changeSection(TwetchViewModel.Section.SEARCH))
                navigateToStartOfChildNavController()
        }

        topbarNotifications.setOnClickListener {
            if (!viewModel.changeSection(TwetchViewModel.Section.NOTIFICATIONS))
                navigateToStartOfChildNavController()
        }

        actionProfile.setOnClickListener {
            if (!viewModel.changeSection(TwetchViewModel.Section.PROFILE))
                navigateToStartOfChildNavController()

            scrollToViewOfBar(it, actionScroller)
        }

        actionChat.setOnClickListener {
            if (!viewModel.changeSection(TwetchViewModel.Section.CHAT))
                navigateToStartOfChildNavController()

            scrollToViewOfBar(it, actionScroller)
        }

        actionFollowing.setOnClickListener {
            if (!viewModel.changeSection(TwetchViewModel.Section.FOLLOWING))
                navigateToStartOfChildNavController()

            scrollToViewOfBar(it, actionScroller)
        }

        actionLatest.setOnClickListener {
            if (!viewModel.changeSection(TwetchViewModel.Section.LATEST))
                navigateToStartOfChildNavController()

            scrollToViewOfBar(it, actionScroller)
        }

        actionTop.setOnClickListener {
            if (!viewModel.changeSection(TwetchViewModel.Section.TOP))
                navigateToStartOfChildNavController()

            scrollToViewOfBar(it, actionScroller)
        }



        topbarNotifications.foreground = notificationBadge
        actionChat.foreground = chatBadge




        val appbarElevatedHeight = 22.0f
        val appbarBackground = MaterialShapeDrawable.createWithElevationOverlay(requireContext(), 0f)
        appBar.background = appbarBackground


        childFragmentManager.registerFragmentLifecycleCallbacks(object : FragmentManager.FragmentLifecycleCallbacks() {
            override fun onFragmentStarted(fm: FragmentManager, f: Fragment) {

                requireView().post {
                    //avoid drawing the shadow appBar automatically lays out with at first

                    appBar.doOnPreDraw {
                        val scrolledToTop = utilViewModel.mainContentTopScrolled.value

                        if (scrolledToTop) {
                            appBar.elevation = 0f
                            appBarBackground.setStrokeWidths(bottom = (0f * dp).toInt())
                        } else {
                            appBar.elevation = appbarElevatedHeight * dp
                            appBarBackground.setStrokeWidths(bottom = (2.1f * dp).toInt())
                        }
                    }
                    appBar.invalidate()
                }


            }
        }, true)


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.activeSection.collect { section ->
                if (TwetchViewModel.Section.PROFILE == section) {
                    actionProfile.iconTint = ColorStateList.valueOf(colorDanger)
                    actionProfile.strokeColor = ColorStateList.valueOf(colorDanger)
                    actionProfile.rippleColor = ColorStateList.valueOf(colorDanger)
                    actionProfile.setTextColor(colorDanger)
                } else {
                    actionProfile.iconTint = ColorStateList.valueOf(colorPrimary)
                    actionProfile.strokeColor = ColorStateList.valueOf(colorPrimary)
                    actionProfile.rippleColor = ColorStateList.valueOf(colorDanger)
                    actionProfile.setTextColor(colorPrimary)
                }

                if (TwetchViewModel.Section.CHAT == section) {
                    actionChat.iconTint = ColorStateList.valueOf(colorDanger)
                    actionChat.strokeColor = ColorStateList.valueOf(colorDanger)
                    actionChat.rippleColor = ColorStateList.valueOf(colorDanger)
                    actionChat.setTextColor(colorDanger)
                } else {
                    actionChat.iconTint = ColorStateList.valueOf(colorPrimary)
                    actionChat.strokeColor = ColorStateList.valueOf(colorPrimary)
                    actionChat.rippleColor = ColorStateList.valueOf(colorDanger)
                    actionChat.setTextColor(colorPrimary)
                }

                if (TwetchViewModel.Section.FOLLOWING == section) {
                    actionFollowing.iconTint = ColorStateList.valueOf(colorDanger)
                    actionFollowing.strokeColor = ColorStateList.valueOf(colorDanger)
                    actionFollowing.rippleColor = ColorStateList.valueOf(colorDanger)
                    actionFollowing.setTextColor(colorDanger)
                } else {
                    actionFollowing.iconTint = ColorStateList.valueOf(colorPrimary)
                    actionFollowing.strokeColor = ColorStateList.valueOf(colorPrimary)
                    actionFollowing.rippleColor = ColorStateList.valueOf(colorDanger)
                    actionFollowing.setTextColor(colorPrimary)
                }

                if (TwetchViewModel.Section.LATEST == section) {
                    actionLatest.iconTint = ColorStateList.valueOf(colorDanger)
                    actionLatest.strokeColor = ColorStateList.valueOf(colorDanger)
                    actionLatest.rippleColor = ColorStateList.valueOf(colorDanger)
                    actionLatest.setTextColor(colorDanger)
                } else {
                    actionLatest.iconTint = ColorStateList.valueOf(colorPrimary)
                    actionLatest.strokeColor = ColorStateList.valueOf(colorPrimary)
                    actionLatest.rippleColor = ColorStateList.valueOf(colorDanger)
                    actionLatest.setTextColor(colorPrimary)
                }

                if (TwetchViewModel.Section.TOP == section) {
                    actionTop.iconTint = ColorStateList.valueOf(colorDanger)
                    actionTop.strokeColor = ColorStateList.valueOf(colorDanger)
                    actionTop.rippleColor = ColorStateList.valueOf(colorDanger)
                    actionTop.setTextColor(colorDanger)
                } else {
                    actionTop.iconTint = ColorStateList.valueOf(colorPrimary)
                    actionTop.strokeColor = ColorStateList.valueOf(colorPrimary)
                    actionTop.rippleColor = ColorStateList.valueOf(colorDanger)
                    actionTop.setTextColor(colorPrimary)
                }

                if (TwetchViewModel.Section.NOTIFICATIONS == section) {
                    drawableTopbarNotifications.setTint(colorDanger)
                } else {
                    drawableTopbarNotifications.setTint(colorPrimary)
                }

                if (TwetchViewModel.Section.SEARCH == section) {
                    drawableTopbarSearch.setTint(colorDanger)
                } else {
                    drawableTopbarSearch.setTint(colorPrimary)
                }



                //todo
                //todo
                //todo
                //todo
                //todo
                //todo

                val host = navHost

                //todo if navHost is not null & tapped again on the same section-title ...

                //save the state of the current tab
                if (host != null && initializedSection != section){
                    fragmentHelper.saveState(host, initializedSection!!.const)
                }



                //restore the state of the new tab if it had any

                val graphResource = when (section) {
                    TwetchViewModel.Section.PROFILE -> R.navigation.nav_twetch_profile
                    TwetchViewModel.Section.CHAT -> R.navigation.nav_twetch_chat
                    TwetchViewModel.Section.FOLLOWING -> R.navigation.nav_twetch_following
                    TwetchViewModel.Section.LATEST -> R.navigation.nav_twetch_latest
                    TwetchViewModel.Section.TOP -> R.navigation.nav_twetch_top
                    TwetchViewModel.Section.NOTIFICATIONS -> R.navigation.nav_twetch_notifications
                    TwetchViewModel.Section.SEARCH -> R.navigation.nav_twetch_search
                }

                val args = Bundle() //todo


                val f = NavHostFragment.create(graphResource, args)
                fragmentHelper.restoreState(f, section.const)


                childFragmentManager.beginTransaction()
                        .replace(R.id.section_content, f)
                        .setPrimaryNavigationFragment(f)
                        .commitNowAllowingStateLoss()

                navHost = f
                initializedSection = section






            }
        }






        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            var isNavigatingHolder = false

            viewModel.authState.combine(viewModel.authInfo) { status, info ->
                status to info
            }.collect { (status, info) ->

                if (isNavigatingHolder) {
                    Timber.d("dropping update; isNavigatingHolder == TRUE")
                    return@collect
                }


                Timber.d("auth status: $status")

                if (info == null || !info.keyAdded) {
                    when (status) {
                        //Auth.NOT_STARTED,
                        Auth.STARTED,
                        Auth.NOT_LINKED,
                        Auth.ERROR -> {
                            isNavigatingHolder = true
                            //navigate away to a different screen
                            findNavController().navigate(R.id.action_twetchProfileFragment_to_twetchAuthFragment)
                            return@collect
                        }
                        else -> {
                            //do nothing
                        }
                    }
                }

            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            utilViewModel.mainContentTopScrolled.collect { scrolledToTop ->
                if (scrolledToTop) {
                    appBar.elevation = 0f
                    appBarBackground.setStrokeWidths(bottom = (0f * dp).toInt())
                    appbarBackground.elevation = 0f
//                    drawableTopbarNew.setTint(appbarBackground.fillColor?.defaultColor ?: Color.RED)
                } else {
                    appBar.elevation = appbarElevatedHeight * dp
                    appBarBackground.setStrokeWidths(bottom = (2.1f * dp).toInt())
                    appbarBackground.elevation = appbarElevatedHeight * dp
//                    drawableTopbarNew.setTint(appbarBackground.fillColor?.defaultColor ?: Color.RED)
                }
            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.loadingIndicator.collect { mustShow ->
                if (mustShow) {
                    loadingIndicatorBar.visibility = View.VISIBLE
                } else {
                    loadingIndicatorBar.visibility = View.INVISIBLE
                }

            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.chatBadgeCount.collect { totalUnreadCount ->
                if (totalUnreadCount == 0) {
                    chatBadge.updateLabel("")
                } else {
                    chatBadge.updateLabel(totalUnreadCount.toString())
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.notificationInfo.collect { info ->
                if (info.unreadCount > 9) {
                    notificationBadge.updateLabel("9+")
                } else if (info.unreadCount == 0) {
                    notificationBadge.updateLabel("")
                } else {
                    notificationBadge.updateLabel(info.unreadCount.toString())
                }
            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.actionHelper.snackbarChannel.receiveAsFlow().collect { model ->
                //Timber.d("receiving model: $model")

                utilViewModel.onChangeSnackBarSpaceReservation(true)
                delay(150)
                try {
                    TwetchSnackbarHelper.displaySnackbar(model, errorSnackbarContainer)
                    delay(150)

                } finally {
                    utilViewModel.onChangeSnackBarSpaceReservation(false)
                }


            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.composePostUseCase.openComposeChannel.receiveAsFlow().collect {
                topbarNew.performClick()
            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            val supported = listOf(
                AppDeepLinkType.TWETCH_POST,
                AppDeepLinkType.TWETCH_USER,
                AppDeepLinkType.TWETCH_CHAT
            )
            viewModel.deepLinkHelper.unsatisfiedLink.collectLatest { link ->
                link ?: return@collectLatest

                if (!supported.contains(link.type))return@collectLatest

                navigateToStartOfChildNavController()
            }
        }


    }

    private var tabBarScrollingJob: Job? = null

    fun scrollToViewOfBar(child: View, scrollView: HorizontalScrollView) {
        tabBarScrollingJob?.cancel()

        tabBarScrollingJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            delay(500)
            val distanceFromLeft = child.left - scrollView.scrollX
            val mustScrollAmount = distanceFromLeft - dp * 150
            scrollView.smoothScrollBy(mustScrollAmount.toInt(), 0)
        }
    }


    private fun navigateToStartOfChildNavController() {
        navHost?.let {
            val navController = it.navController
            //the user is already seeing this tab
            //bring the user to the start of the nav-graph that is showing
            val currentDestinationId = navController.currentDestination?.id
            val startDestinationId = navController.graph.startDestinationId

            if (currentDestinationId == startDestinationId) {
                //the user is already looking at the start destination
                return
            }

            navController.popBackStack(startDestinationId, false)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        navHost?.let {
            //save current state
            fragmentHelper.saveState(it, initializedSection!!.const)
        }

        if (::fragmentHelper.isInitialized) {
            viewModel.setNavState(fragmentHelper.saveHelperState())
        }

        super.onSaveInstanceState(outState)
    }

    private fun initActionButton(action: ImageView, drawable: Drawable) {
        val colorSelector = ContextCompat.getColor(requireContext(), R.color.selector_on_surface)

        val topBarActionInset = (resources.displayMetrics.density * 6).toInt()

        val actionScale = 0.95f

        val drawableCreateIcon = drawable.let { d ->
//            val d = ContextCompat.getDrawable(requireContext(), it)!!.mutate()
//            d.setTint(requireContext().getColorFromAttr(R.attr.colorPrimary))

            LayerDrawable(arrayOf(d)).also { ld ->
                ld.setLayerInset(0, topBarActionInset, topBarActionInset,
                    topBarActionInset, topBarActionInset)
            }
        }

        val drawableCreateSelector = DrawableState.getNew(colorSelector, ovalShape = true)
        action.background = LayerDrawable(arrayOf(
            drawableCreateSelector,
            drawableCreateIcon
        ))
        action.scaleType = ImageView.ScaleType.CENTER
        action.scaleX = actionScale
        action.scaleY = actionScale
    }

    private fun initNewPostButton() {
        val colorSelector = ColorUtils.setAlphaComponent(
                requireContext().getColorFromAttr(R.attr.colorDanger),
                190
        )

        val topBarActionInset = (resources.displayMetrics.density * 6).toInt()

        val actionScale = 0.95f

        drawableTopbarNew.setTint(requireContext().getColorFromAttr(R.attr.colorOnPrimary))

        val drawableCreateIcon = drawableTopbarNew.let { d ->
//            val d = ContextCompat.getDrawable(requireContext(), it)!!.mutate()
//            d.setTint(requireContext().getColorFromAttr(R.attr.colorPrimary))

            LayerDrawable(arrayOf(
                ColorDrawable(colorPrimary),
                d
            )).also { ld ->
                ld.setLayerInset(1, topBarActionInset, topBarActionInset,
                    topBarActionInset, topBarActionInset)
            }
        }

        val drawableCreateSelector = DrawableState.getNew(colorSelector)
        topbarNew.background = drawableCreateIcon
        topbarNew.foreground = drawableCreateSelector
        topbarNew.scaleType = ImageView.ScaleType.CENTER
        topbarNew.scaleX = actionScale
        topbarNew.scaleY = actionScale

        topbarNew.clipToOutline = true
        topbarNew.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                outline.setOval(0, 0, view.width, view.height)
            }

        }
    }

}