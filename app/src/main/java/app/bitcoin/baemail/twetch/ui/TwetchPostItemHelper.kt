package app.bitcoin.baemail.twetch.ui

import android.content.Context
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.format.DateFormat
import android.text.style.ForegroundColorSpan
import androidx.core.graphics.ColorUtils
import androidx.core.text.PrecomputedTextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.span.LabelSpan
import app.bitcoin.baemail.core.presentation.span.PlainTextLinkSpan
import app.bitcoin.baemail.core.presentation.span.RoundedBackgroundSpan
import app.bitcoin.baemail.core.presentation.span.TwetchUserNameSpan
import app.bitcoin.baemail.core.presentation.util.RelativeTimeHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchNotificationItem
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchPostDetailMainItem
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchPostItem
import app.bitcoin.baemail.core.presentation.view.recycler.SpanType
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.trySendBlocking
import java.text.DecimalFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.HashMap

class TwetchPostItemHelper(
    val context: Context,
    val userCache: TwetchPostUserCache,
    val bodyTextParams: PrecomputedTextCompat.Params
) {

    val dp = context.resources.displayMetrics.density
    val contentTextSize = context.resources.getDimensionPixelSize(R.dimen.body_text_size)

    val colorPrimary = context.getColorFromAttr(R.attr.colorPrimary)
    val colorSecondary = context.getColorFromAttr(R.attr.colorSecondary)
    val colorOnPrimary = context.getColorFromAttr(R.attr.colorOnPrimary)

    val userIdPattern: Pattern = Pattern.compile("(@)([0-9]+)")


    private val mainPostDateFormat: java.text.DateFormat by lazy {
        DateFormat.getMediumDateFormat(context)
    }
    private val mainPostTimeFormat: java.text.DateFormat by lazy {
        DateFormat.getTimeFormat(context)
    }

    fun createItem(
        postHolder: PostHolder,
        relativeTimeHelper: RelativeTimeHelper,
        isParent: Boolean = false,
        isReply: Boolean = false,
        isFirstOfParents: Boolean = false
    ): ATTwetchPostItem {

        val spanClicksChannel = Channel<Pair<SpanType, String>>(Channel.CONFLATED)

        val onUserIdClicked: (String) -> Unit = { userId ->
            spanClicksChannel.trySendBlocking(SpanType.USER_ID to userId)
        }
        val onUnloadedPostClicked: (String) -> Unit = { txId ->
            spanClicksChannel.trySendBlocking(SpanType.REF_POST to txId)
        }




        val contentMap: HashMap<String, PrecomputedTextCompat> = HashMap()




        val refPostTxId = postHolder.post.referencedPost
        val refPost = refPostTxId?.let { txId ->
            postHolder.referenced[txId]
        }

        val postCreatedLabel = relativeTimeHelper.getLabel(postHolder.post.millisCreatedAt)

        postHolder.post.let { p ->
            contentMap[p.txId] = createContentLabel(
                p,
                postHolder,
                true,
                onUserIdClicked,
                onUnloadedPostClicked
            )
        }


        val refPostCreatedLabel = refPost?.millisCreatedAt?.let { millis ->
            relativeTimeHelper.getLabel(millis)
        }

        refPost?.let { p ->
            val referencedPostButtonAllowed = !postHolder.post.isBranchWithComment
            contentMap[p.txId] = createContentLabel(
                p,
                postHolder,
                referencedPostButtonAllowed,
                onUserIdClicked,
                onUnloadedPostClicked
            )
        }



        val doubleRefPostCreatedLabel = refPost?.referencedPost?.let { txId ->
            postHolder.referenced[txId]?.let { p ->

                contentMap[p.txId] = createContentLabel(
                    p,
                    postHolder,
                    false,
                    onUserIdClicked,
                    onUnloadedPostClicked
                )

                relativeTimeHelper.getLabel(p.millisCreatedAt)
            }
        }



        val item = ATTwetchPostItem(
            postHolder.post.id,
            postHolder,
            postCreatedLabel,
            refPostCreatedLabel,
            doubleRefPostCreatedLabel,
            contentMap,
            isParent,
            isFirstOfParents,
            isReply
        )

        item.spanClicks = spanClicksChannel



        return item
    }




    fun createMainPostItem(
        holder: PostHolder,
        relativeTimeHelper: RelativeTimeHelper
    ): ATTwetchPostDetailMainItem {

        val spanClicksChannel = Channel<Pair<SpanType, String>>(Channel.CONFLATED)

        val onUserIdClicked: (String) -> Unit = { userId ->
            spanClicksChannel.trySendBlocking(SpanType.USER_ID to userId)
        }
        val onUnloadedPostClicked: (String) -> Unit = { txId ->
            spanClicksChannel.trySendBlocking(SpanType.REF_POST to txId)
        }




        val contentMap: HashMap<String, PrecomputedTextCompat> = HashMap()




        val refPostTxId = holder.post.referencedPost
        val refPost = refPostTxId?.let { txId ->
            holder.referenced[txId]
        }

        val postLabel = mainPostDateFormat.format(Date(holder.post.millisCreatedAt)) + " • " +
                mainPostTimeFormat.format(Date(holder.post.millisCreatedAt))

        holder.post.let { p ->
            contentMap[p.txId] = createContentLabel(
                p,
                holder,
                true,
                onUserIdClicked,
                onUnloadedPostClicked
            )
        }


        val refPostLabel = refPost?.millisCreatedAt?.let { millis ->
            relativeTimeHelper.getLabel(millis)
        }

        refPost?.let { p ->
            contentMap[p.txId] = createContentLabel(
                p,
                holder,
                false,
                onUserIdClicked,
                onUnloadedPostClicked
            )
        }


        val item = ATTwetchPostDetailMainItem(
            holder.post.id,
            holder,
            postLabel,
            refPostLabel,
            contentMap
        )

        item.spanClicks = spanClicksChannel



        return item
    }



    fun createNotificationItem(
        notification: TwetchNotificationsStore.Notification,
        relativeTimeHelper: RelativeTimeHelper
    ): ATTwetchNotificationItem {

        val spanClicksChannel = Channel<Pair<SpanType, String>>(Channel.CONFLATED)

        val onUserIdClicked: (String) -> Unit = { userId ->
            spanClicksChannel.trySendBlocking(SpanType.USER_ID to userId)
        }
        val onUnloadedPostClicked: (String) -> Unit = { txId ->
            spanClicksChannel.trySendBlocking(SpanType.REF_POST to txId)
        }



        val contentMap: HashMap<String, PrecomputedTextCompat> = HashMap()



        val notificationCreatedLabel = relativeTimeHelper.getLabel(notification.millisCreatedAt)

        val postCreatedLabel = notification.post?.let { post ->
            relativeTimeHelper.getLabel(post.millisCreatedAt)
        }

        contentMap["notification_description"] = PrecomputedTextCompat.create(
            SpannableStringBuilder(notification.description),
            bodyTextParams
        )





        val priceLabel = if (notification.priceUsd < 0.01f) {
            val formatted = DecimalFormat("##.#").format(notification.priceSats / 1000f)
            context.resources.getString(R.string.notification_price_sats, formatted)
        } else {
            val formatted = DecimalFormat("#.##").format(notification.priceUsd)
            context.resources.getString(R.string.notification_price_usd, formatted)
        }



        notification.post?.let { p ->
            contentMap[p.txId] = createContentLabel(
                p,
                null,
                true,
                onUserIdClicked,
                onUnloadedPostClicked
            )
        }




        val item = ATTwetchNotificationItem(
            notification.id,
            notification,
            notificationCreatedLabel,
            postCreatedLabel,
            priceLabel,
            contentMap
        )

        item.spanClicks = spanClicksChannel

        return item
    }




    fun createContentLabel(
        post: PostModel,
        holder: PostHolder?,
        referencedPostButtonAllowed: Boolean,
        onUserIdClicked: (String) -> Unit,
        onUnloadedPostClicked: (String) -> Unit
    ): PrecomputedTextCompat {

        if (post.isRemoved) {
            val finalContent = SpannableStringBuilder(context.getString(R.string.twetch_post_removed))
            finalContent.setSpan(
                ForegroundColorSpan(ColorUtils.setAlphaComponent(
                    context.getColorFromAttr(R.attr.colorOnSurface),
                    150
                )),
                0,
                finalContent.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            return PrecomputedTextCompat.create(
                finalContent,
                bodyTextParams
            )
        }

        val finalContent = SpannableStringBuilder(post.content)

        val idMatcher = userIdPattern.matcher(post.content)
        while (idMatcher.find()) {
            val foundSequence = idMatcher.group(2) ?: continue
            val cachedName = userCache.getUser(foundSequence)?.name ?: "??"

            val start = idMatcher.start()
            val end = idMatcher.end()

            finalContent.setSpan(
                TwetchUserNameSpan(
                    context,
                    cachedName,
                    ColorUtils.setAlphaComponent(colorPrimary, 120),
                    dp * 1f,
                    colorSecondary,
                    colorPrimary,
                    dp * 8,
                    dp * 12,
                ),
                start,
                end,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            finalContent.setSpan(
                PlainTextLinkSpan(
                    foundSequence,
                    onUserIdClicked
                ),
                start,
                end,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }


        val postTxIds = PostModel.findReferencedPosts(post.content)
        postTxIds.forEach { postTxId ->
            val url = PostModel.TWETCH_POST_URL_START + postTxId

            val startPos = post.content.indexOf(url)
            if (startPos == -1) return@forEach

            val endPos = startPos + url.length

            finalContent.setSpan(
                    PlainTextLinkSpan(
                            postTxId,
                            onUnloadedPostClicked
                    ),
                    startPos,
                    endPos,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            finalContent.setSpan(
                    LabelSpan(
                        context.resources.getString(R.string.referenced_post).toUpperCase(Locale.getDefault()),
                        ColorUtils.setAlphaComponent(colorPrimary, 150),
                        colorOnPrimary,
                        dp * 20,
                        dp * 12,
                        dp * 2
                    ),
                    startPos,
                    endPos,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )

        }




        post.referencedPost?.let { txId ->
            val gotReferenced = holder?.referenced?.get(txId)
            if (referencedPostButtonAllowed && gotReferenced == null) {
                finalContent.append("\n")
                val startPos = finalContent.length
                finalContent.append(context.resources.getString(R.string.referenced_post).toUpperCase(Locale.getDefault()))


                finalContent.setSpan(
                    PlainTextLinkSpan(
                        txId,
                        onUnloadedPostClicked
                    ),
                    startPos,
                    finalContent.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                val verticalPadding = (dp * 10).toInt()
                finalContent.setSpan(
                    RoundedBackgroundSpan(
                        ColorUtils.setAlphaComponent(colorPrimary, 150),
                        colorOnPrimary,
                        (contentTextSize + 2 * verticalPadding).toInt(),
                        dp * 8,
                        dp * 12,
                        dp * 2
                    ),
                    startPos,
                    finalContent.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        }


        return PrecomputedTextCompat.create(
            finalContent,
            bodyTextParams
        )

    }


}