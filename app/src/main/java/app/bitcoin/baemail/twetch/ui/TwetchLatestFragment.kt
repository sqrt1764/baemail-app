package app.bitcoin.baemail.twetch.ui

import android.content.Intent
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.TextView
import androidx.core.widget.TextViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.bitcoin.baemail.R
import app.bitcoin.baemail.fundingWallet.core.presentation.CoinActivity
import app.bitcoin.baemail.fundingWallet.TopUpActivity
import app.bitcoin.baemail.core.presentation.util.RelativeTimeHelper
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import app.bitcoin.baemail.core.presentation.view.ViewCoinManagementHelper
import app.bitcoin.baemail.core.presentation.view.recycler.ATContentLoading0
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATTwetchNoPostsFoundItem
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.di.ViewModelProviderAssistant
import com.google.gson.Gson
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.mapLatest
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.math.min

class TwetchLatestFragment : Fragment(R.layout.fragment_twetch_latest) {

    @Inject
    lateinit var savedStateViewModelFactoryAssistant: ViewModelProviderAssistant


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var gson: Gson

    private lateinit var viewModel: TwetchViewModel
    private lateinit var utilViewModel: TwetchUtilViewModel

    private lateinit var helper: TwetchLatestUseCase

    private val postItemHelper: TwetchPostItemHelper by lazy {
        TwetchPostItemHelper(
            requireContext(),
            viewModel.postUserCache,
            TextViewCompat.getTextMetricsParams(dummyBodyText)
        )
    }

    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var recycler: RecyclerView
    private lateinit var scrollToTop: TextView
    private lateinit var coinManagementHelper: ViewCoinManagementHelper

    private lateinit var dummyBodyText: TextView

    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter

    private var wasCreatedOnce = false

    private var listRestoredOnce = false

    private val noPostsFoundItem = ATTwetchNoPostsFoundItem("0")

    private val topHeightLD = MutableLiveData<Int>()
    private val topHeightItem = ATHeightDecorDynamic("0", topHeightLD)

    private val altLoadingItemLD = MutableLiveData<Int>()
    private val altLoadingItem = ATHeightDecorDynamic("1", altLoadingItemLD)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            requireActivity(),
            savedStateViewModelFactoryAssistant.create(this)
        )[TwetchViewModel::class.java]

        utilViewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(TwetchUtilViewModel::class.java)


        helper = viewModel.getLatestContent()

        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true




        dummyBodyText = requireView().findViewById(R.id.dummy_body_text)

        swipeRefresh = requireView().findViewById(R.id.swipe_refresh)
        recycler = requireView().findViewById(R.id.list)
        scrollToTop = requireView().findViewById(R.id.scroll_to_top)
        coinManagementHelper = requireView().findViewById(R.id.coin_management_helper)


        val dp = resources.displayMetrics.density


        val colorPrimary = requireContext().getColorFromAttr(R.attr.colorPrimary)
        val colorTransparentPrimary = Color.argb(
            200,
            Color.red(colorPrimary),
            Color.green(colorPrimary),
            Color.blue(colorPrimary)
        )

        topHeightLD.value = (4 * dp).toInt()
        altLoadingItemLD.value = (108 * dp).toInt()

        swipeRefresh.setProgressViewOffset(true, (5 * dp).toInt(), (58 * dp).toInt())
        swipeRefresh.setColorSchemeColors(requireContext().getColorFromAttr(R.attr.colorOnPrimary))
        swipeRefresh.setProgressBackgroundColorSchemeColor(colorTransparentPrimary)
        swipeRefresh.setOnRefreshListener {
            helper.onRefreshRequested()

        }






        scrollToTop.clipToOutline = true
        scrollToTop.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(p0: View?, p1: Outline?) {
                p0 ?: return
                p1 ?: return

                p1.setRoundRect(0, 0, p0.width, p0.height, p0.height / 2f)
            }
        }
        scrollToTop.background = ColorDrawable(colorTransparentPrimary)

        scrollToTop.setOnClickListener {
            recycler.smoothScrollToPosition(0)
            scrollToTop.visibility = View.INVISIBLE
        }







        val contentLoadingListener = object : BaseListAdapter.ATContentLoadingListener {
            override fun onBound() {

                if (helper.metaState.value.listRefreshing) {
                    return
                }

                swapOutRefreshingItem()
            }
        }

        val postListener = TwetchPostListener(
            requireContext(),
            findNavController(),
            viewModel.actionHelper,
            viewModel.videoClipManager,
            R.id.action_global_twetchUserProfile,
            R.id.action_twetchLatestFragment_to_twetchChatFullImageFragment5,
            R.id.action_global_twetchPostDetails,
            R.id.action_twetchLatestFragment_to_twetchBranchOptionsFragment3,
            R.id.action_twetchLatestFragment_to_twetchVideoClipFragment4,
            viewModel.coinFlipSignalHelper.coinFlipChannel
        )

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atContentLoadingListener = contentLoadingListener,
                atTwetchPostListener = postListener
            )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var lastShowShadow: Boolean? = null

            var hasScrollUpBeenInitialized = false

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                recyclerView.canScrollVertically(-1).let { showShadow ->
                    if (lastShowShadow == showShadow) return@let
                    lastShowShadow = showShadow

                    utilViewModel.onMainContentScrollChanged(!showShadow)
                }

                if (!hasScrollUpBeenInitialized) {
                    refreshShowingOfScrollToTop()
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (RecyclerView.SCROLL_STATE_IDLE == newState) {
                    cacheCurrentScrollState()

                    refreshShowingOfScrollToTop()
                }
            }

            fun refreshShowingOfScrollToTop() {
                hasScrollUpBeenInitialized = true

                requireView().post {
                    if (layoutManager.findFirstVisibleItemPosition() > 10) {
                        if (scrollToTop.visibility != View.VISIBLE) {
                            scrollToTop.visibility = View.VISIBLE
                        }
                    } else {
                        if (scrollToTop.visibility != View.INVISIBLE) {
                            scrollToTop.visibility = View.INVISIBLE
                        }
                    }
                }
            }

        })





        coinManagementHelper.clickTopUpListener = {
            startActivity(Intent(requireActivity(), TopUpActivity::class.java))
        }

        coinManagementHelper.clickSplitCoinsListener = {
            startActivity(Intent(requireActivity(), CoinActivity::class.java))
        }

        coinManagementHelper.clickRetryReconnectListener = {
            viewModel.chainSync.considerRetry()
        }






        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            utilViewModel.mainContentTopDistance.collect {
                refreshDynamicBottomHeight()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.coinManagementUseCase.state.mapLatest { it }
                .combine(utilViewModel.snackBarShowingState) { coinModel, snackBarReservation ->
                    coinModel to snackBarReservation
                }.collectLatest { (coinModel, snackBarReservation) ->
                    if (!snackBarReservation) {
                        coinManagementHelper.applyModel(coinModel)
                    } else {
                        //apply a model that will result in this bar being hidden
                        coinManagementHelper.applyModel(ViewCoinManagementHelper.HELPER_HIDDEN)
                    }
                }
        }



        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            helper.metaState.collect {
                refreshLoadingIndicatorState()
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {

            var lastAppliedList: TwetchPostModel? = null

            helper.content.collectLatest { model ->


                if (lastAppliedList == model.list) {
                    Timber.d("post-list is unchanged; returning")
                    return@collectLatest
                }
                lastAppliedList = model.list


                //refreshLoadingIndicatorState()
                awaitRecyclerAnimationsFinish()


                if (model.list == null) {
                    Timber.d("model.list == null")
                    adapter.setItems(emptyList())
                } else {

                    val isRefreshing = helper.metaState.value.listRefreshing

                    val loadingItem = if (!isRefreshing && model.list.hasMorePages) {
                        ATContentLoading0(model.list.page)
                    } else {
                        altLoadingItem
                    }

                    if (TwetchPostListHelper.PAGE_NOT_LOADED == model.list.page) {
                        Timber.d("model.list == PAGE_NOT_LOADED")
                        helper.requestNextPage()
                    }

                    if (model.list.list.isEmpty()) {
                        if (model.list.hasMorePages) {
                            adapter.setItems(
                                listOf(
                                    topHeightItem,
                                    loadingItem
                                )
                            )
                        } else {
                            adapter.setItems(
                                listOf(
                                    topHeightItem,
                                    noPostsFoundItem
                                )
                            )
                        }

                    } else {

                        val itemList = mutableListOf<AdapterType>()

                        withContext(Dispatchers.Default) {
                            val relativeTimeHelper = RelativeTimeHelper(
                                requireContext(),
                                System.currentTimeMillis()
                            )

                            model.list.list.map {

                                postItemHelper.createItem(it, relativeTimeHelper)

//                                val refPostTxId = it.post.referencedPost
//                                val refPost = refPostTxId?.let { txId ->
//                                    it.referenced[txId]
//                                }
//
//                                val postLabel = relativeTimeHelper.getLabel(it.post.millisCreatedAt)
//                                val refPostLabel = refPost?.millisCreatedAt?.let { millis ->
//                                    relativeTimeHelper.getLabel(millis)
//                                }
//                                val doubleRefPostLabel = refPost?.referencedPost?.let { txId ->
//                                    it.referenced[txId]?.let { p ->
//                                        relativeTimeHelper.getLabel(p.millisCreatedAt)
//                                    }
//                                }
//
//                                ATTwetchPostItem(
//                                    it.post.id,
//                                    it,
//                                    postLabel,
//                                    refPostLabel,
//                                    doubleRefPostLabel
//                                )
                            }.toCollection(itemList)
                        }

                        Timber.d("new post-list; size: ${itemList.size}; page:${model.list.page} hasMorePages:${model.list.hasMorePages} isRefreshing:$isRefreshing")

                        if (model.list.hasMorePages) {
                            itemList.add(loadingItem)
                        }

                        itemList.add(0, topHeightItem)

                        adapter.setItems(itemList)

                        if (!listRestoredOnce) {
                            listRestoredOnce = true
                            layoutManager.onRestoreInstanceState(helper.cachedListState)
                        }


                    }

                }
            }
        }

    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!wasCreatedOnce) return

        recycler.stopScroll()
        cacheCurrentScrollState()
    }

    private fun refreshLoadingIndicatorState() {
        val metaState = helper.metaState.value

        if (swipeRefresh.isRefreshing == metaState.listRefreshing) return
        swipeRefresh.isRefreshing = metaState.listRefreshing
    }



    private fun cacheCurrentScrollState() {
        helper.cachedListState = layoutManager.onSaveInstanceState()
    }




    private suspend fun awaitRecyclerAnimationsFinish(fromListChange: Boolean = true) {
        if (fromListChange) {
            swapOutRefreshingItemJob?.cancel()
            swapOutRefreshingItemJob = null
        }

        recycler.itemAnimator?.let { animator ->
            //making sure that the any previous animation get completed
            if (!animator.isRunning) {
                return@let
            }

            suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                animator.isRunning { cancellableContinuation.resume(Unit) }
            }
        }

        suspendCancellableCoroutine<Unit> { continuation ->
            if (recycler.isComputingLayout) {
                recycler.post {
                    continuation.resume(Unit)
                }
            } else {
                continuation.resume(Unit)
            }

        }
    }




    var swapOutRefreshingItemJob: Job? = null

    private fun swapOutRefreshingItem() {
        if (swapOutRefreshingItemJob?.isActive == true) return

        swapOutRefreshingItemJob = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            awaitRecyclerAnimationsFinish(false)

            val items = adapter.getCopyOfItems()

            var loadingItemPos = -1

            val countToCheck = min(5, items.size)
            for (i in 0 until countToCheck) {
                val item = items[items.lastIndex - i]
                if (item is ATContentLoading0) {
                    loadingItemPos = items.lastIndex - i
                    break
                }
            }

            ensureActive()

            if (loadingItemPos == -1) {
                Timber.d(">>> loadingItemPost not found")
                helper.requestNextPage()
                return@launchWhenStarted
            }

            adapter.swapOutItem(loadingItemPos, altLoadingItem)

            awaitRecyclerAnimationsFinish(false)
            helper.requestNextPage()
        }
    }



    private fun refreshDynamicBottomHeight() {
        val appbarOffset = utilViewModel.mainContentTopDistance.value

        coinManagementHelper.translationY = -1f * appbarOffset
    }


}