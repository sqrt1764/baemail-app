package app.bitcoin.baemail.twetch.ui

import android.content.Context
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class TwetchUtilViewModel @Inject constructor(
        private val appContext: Context
) : ViewModel() {

    private val _mainContentTopScrolled = MutableStateFlow(true)
    val mainContentTopScrolled: StateFlow<Boolean>
        get() = _mainContentTopScrolled

    private val _mainContentTopDistance = MutableStateFlow(0)
    val mainContentTopDistance: StateFlow<Int>
        get() = _mainContentTopDistance

    private val _snackBarShowingState = MutableStateFlow(false)
     val snackBarShowingState: StateFlow<Boolean>
        get() = _snackBarShowingState


    fun onMainContentScrollChanged(scrolledToTop: Boolean) {
        _mainContentTopScrolled.value = scrolledToTop
    }

    fun onMainContentTopDistanceChanged(distance: Int) {
        _mainContentTopDistance.value = distance
    }

    fun onChangeSnackBarSpaceReservation(mustReserve: Boolean) {
        _snackBarShowingState.value = mustReserve
    }
}