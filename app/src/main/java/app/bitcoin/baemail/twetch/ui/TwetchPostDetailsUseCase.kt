package app.bitcoin.baemail.twetch.ui

import android.os.Parcelable
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import com.dropbox.android.external.store4.ResponseOrigin
import com.dropbox.android.external.store4.StoreResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import timber.log.Timber

@FlowPreview
@ExperimentalCoroutinesApi
class TwetchPostDetailsUseCase(
    private val activeUserId: String,
    private val txId: String,
    private val coroutineUtil: CoroutineUtil,
    private val postDetailsStore: TwetchPostDetailsStore
) {

    companion object {
        const val PAGE_NOT_LOADED = -1
    }

    private var isSetup = false

    private var page = PAGE_NOT_LOADED


    private val _content = MutableStateFlow(Model(
        TwetchPostDetailsStore.PostDetails(
            txId,
            null,
            emptyMap(),
            null
        ),
        page,
        true
    ))
    val content: StateFlow<Model>
        get() {
            ensureSetup()
            return _content
        }



    private val _metaState = MutableStateFlow(MetaState(
        true,
        false
    ))
    val metaState: StateFlow<MetaState>
        get() = _metaState



    private val _details = MutableStateFlow<StoreResponse<TwetchPostDetailsStore.PostDetails>>(
        StoreResponse.Loading(ResponseOrigin.Cache)
    )





    private var _completedPageFetch = MutableStateFlow(page)


    private var dataObservationJob: Job? = null
//    var refreshingObservationJob: Job? = null

    private var enqueuedGetNextPageJob: Job? = null

    private var pagingJob: Job? = null




    var cachedListState: Parcelable? = null



    private fun ensureSetup() {
        if (isSetup) {
            return
        }
        isSetup = true


        dataObservationJob?.cancel()
        dataObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

//            val key = TwetchPostDetailsStore.Key(
//                activeUserId,
//                txId
//            )

            var lastData = TwetchPostDetailsStore.PostDetails(
                txId,
                null,
                emptyMap(),
                null
            )
            var lastRefreshing = true
            var lastError = false

            _metaState.value = _metaState.value.copy(
                refreshing = lastRefreshing
            )


            _details.collect { response ->
                when (response) {
                    is StoreResponse.Loading -> {
                        Timber.d("StoreResponse.Loading origin:${response.origin}")
                        lastRefreshing = true

                    }
                    is StoreResponse.Data<*> -> {
                        Timber.d("StoreResponse.Data origin:${response.origin}")
                        if (response.origin == ResponseOrigin.Fetcher) {
                            lastRefreshing = false
                            lastError = false
                        } else if (response.origin == ResponseOrigin.SourceOfTruth
                            || response.origin == ResponseOrigin.Cache) {
//                            lastRefreshingState = false
                        }

                        lastData = response.requireData()


                    }
                    is StoreResponse.Error -> {
                        Timber.d("StoreResponse.Error $response; ")
                        if (response.origin == ResponseOrigin.Fetcher) {
                            //no longer loading
                            lastRefreshing = false
                        }

                        if (response is StoreResponse.Error.Exception) {
                            Timber.e(response.error, "StoreResponse.Error.Exception")
                        }

                        //show error
                        lastError = true

                    }
                    is StoreResponse.NoNewData -> {
                        Timber.d("StoreResponse.NoNewData")
                        lastRefreshing = false
                    }
                }


                _content.value = _content.value.copy(
                    post = lastData,
                    page = page

                )

                _metaState.value = _metaState.value.copy(
                    refreshing = lastRefreshing,
                    error = lastError
                )

            }
        }

//        refreshingObservationJob?.cancel()
//        refreshingObservationJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
//            //
//        }

    }





    private fun checkHasMorePages(): Boolean {
        return postDetailsStore.checkPostHasMoreReplies(
            activeUserId,
            txId
        )
    }


    fun userRequestedNextPage() {
        Timber.d(">>>> userRequestedNextPage()")

        if (enqueuedGetNextPageJob != null) {
            Timber.d(">>>> userRequestedNextPage() dropped because a request is already enqueued")
            return
        }

        if (!checkHasMorePages()) {
            Timber.d(">>>> userRequestedNextPage() dropped because there are no more pages to fetch")
            return
        }


        enqueuedGetNextPageJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {
            Timber.d(">>>> userRequestedNextPage() the request is now enqueued")

            _completedPageFetch.collect { completedPage ->
                if (completedPage != page) {
                    Timber.d(">>>> message-page-load is in progress; page:$page completedPage:$completedPage")
                    return@collect
                }

                val didError = _metaState.value.error
                if (didError) {
                    Timber.d(">>>> not carrying out an enqueued request because there was an error")
                    return@collect
                }

                if (!checkHasMorePages()) {
                    //cleanup
                    Timber.d(">>>> not carrying out an enqueued request because there was are no more pages")
                    val thisJob = enqueuedGetNextPageJob
                    enqueuedGetNextPageJob = null
                    thisJob?.cancel()

                    return@collect
                }

                Timber.d(">>>>carrying out an enqueued request because the messages NOT refreshing")

                requestNextPage()

                //cleanup
                val thisJob = enqueuedGetNextPageJob
                enqueuedGetNextPageJob = null
                thisJob?.cancel()
            }
        }
    }



    private fun requestNextPage(forceFresh: Boolean = false) {
        if (!isSetup) throw RuntimeException()

        Timber.d("requestNextPage page:${page + 1}")

        pagingJob?.cancel()
        pagingJob = coroutineUtil.appScope.launch(coroutineUtil.ioDispatcher) {

            page += 1
            val currentPage = page

            val key = TwetchPostDetailsStore.Key(
                activeUserId,
                txId
            )
            key.replyPage = currentPage
            key.forceRefresh = forceFresh

            //eagerly return the cached content
            if (currentPage == 0) {
                postDetailsStore.getFromSourceOfTruth(key).let {
                    if (it.main == null) return@let
                    _details.value = StoreResponse.Data(it, ResponseOrigin.SourceOfTruth)
                }
            }

            postDetailsStore.getFlow(key).collect { response ->
                _details.value = response

                //update the tracking of fetch progress
                when (response) {
                    is StoreResponse.Loading -> { }
                    is StoreResponse.Data<*> -> {
                        if (response.origin == ResponseOrigin.Fetcher) {
                            _completedPageFetch.value = currentPage
                        }

                    }
                    is StoreResponse.Error -> {
                        if (response.origin == ResponseOrigin.Fetcher) {
                            _completedPageFetch.value = currentPage
                        }

                    }
                    is StoreResponse.NoNewData -> {
                        _completedPageFetch.value = currentPage
                    }
                }
            }


        }
    }



    fun onRefreshRequested(): Boolean {
        val currentMetaState = _metaState.value
        if (currentMetaState.refreshing) {
            Timber.d("refresh-request dropped; currently loading")
            return false
        }

        isSetup = false

        dataObservationJob?.cancel()

        pagingJob?.cancel()
        pagingJob = null

        enqueuedGetNextPageJob?.cancel()
        enqueuedGetNextPageJob = null

        _metaState.value = currentMetaState.copy(
            refreshing = false,
            error = false
        )

        _details.value = StoreResponse.Data(_content.value.post, ResponseOrigin.SourceOfTruth)

        page = PAGE_NOT_LOADED
        _completedPageFetch.value = page

        postDetailsStore.clearFromMetaCache(
            activeUserId,
            txId
        )

        ensureSetup()

        requestNextPage(true)

        return true
    }

    fun onRetryLastRequested() {
        val currentMetaState = _metaState.value
        if (currentMetaState.refreshing) {
            Timber.d("retry-last-request dropped; currently loading")
            return
        }

        isSetup = false

        dataObservationJob?.cancel()

        pagingJob?.cancel()
        pagingJob = null

        enqueuedGetNextPageJob?.cancel()
        enqueuedGetNextPageJob = null

        _metaState.value = currentMetaState.copy(
            refreshing = false,
            error = false
        )

        _details.value = StoreResponse.Data(_content.value.post, ResponseOrigin.SourceOfTruth)

        if (PAGE_NOT_LOADED != page) {
            page -= 1
            _completedPageFetch.value = page
        }


        postDetailsStore.clearErrorFromMetaCache(
            activeUserId,
            txId
        )


        ensureSetup()


        requestNextPage(false)

    }


    fun cleanup() {
        isSetup = false

        dataObservationJob?.cancel()
//        refreshingObservationJob?.cancel()



        enqueuedGetNextPageJob?.cancel()
        enqueuedGetNextPageJob = null

        pagingJob?.cancel()
        pagingJob = null

        page = PAGE_NOT_LOADED
        _completedPageFetch.value = page

        _details.value = StoreResponse.Loading(ResponseOrigin.Cache)

        _content.value = Model(
            TwetchPostDetailsStore.PostDetails(
                txId,
                null,
                emptyMap(),
                null
            ),
            page,
            true
        )

        _metaState.value = MetaState(
            true,
            false
        )
    }



    data class Model(
        val post: TwetchPostDetailsStore.PostDetails,
        val page: Int,
        val hasMorePages: Boolean
    )

    data class MetaState(
        val refreshing: Boolean,
        val error: Boolean
    )



}