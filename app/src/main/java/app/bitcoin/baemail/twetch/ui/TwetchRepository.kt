package app.bitcoin.baemail.twetch.ui

import android.content.Context
import androidx.lifecycle.Observer
import app.bitcoin.baemail.core.data.wallet.AppStateLiveData
import app.bitcoin.baemail.core.data.nodejs.NodeConnectedLiveData
import app.bitcoin.baemail.core.data.net.AppApiService
import app.bitcoin.baemail.core.data.room.AppDatabase
import app.bitcoin.baemail.twetch.db.TwetchDatabase
import app.bitcoin.baemail.core.data.util.ConnectivityLiveData
import app.bitcoin.baemail.core.data.util.CoroutineUtil
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import com.google.gson.Gson
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber
import kotlin.collections.HashMap
import kotlin.collections.HashSet

class TwetchRepository(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil,
    private val apiService: AppApiService,
    private val appStateLD: AppStateLiveData,
    private val nodeConnectedLiveData: NodeConnectedLiveData,
    private val appDatabase: AppDatabase,
    private val twetchDatabase: TwetchDatabase,
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val profileDatastore: TwetchAuthStore,
    private val twetchChatInfoStore: TwetchChatInfoStore,
    private val twetchChatMessageStore: TwetchChatMessageStore,
    private val userProfileBaseStore: TwetchUserProfileBaseStore,
    private val postListStore: TwetchPostListStore,
    private val postDetailsStore: TwetchPostDetailsStore,
    private val notificationsStore: TwetchNotificationsStore,
    private val postCache: TwetchPostCache,
    val postUserCache: TwetchPostUserCache,
    private val gson: Gson,
    val coinManagementUseCase: CoinManagementUseCase,
    private val connectivityLiveData: ConnectivityLiveData
) {

    companion object {
        const val INVALID_PAYMAIL = "invalid"
    }

    private var initializedForPaymail: String = INVALID_PAYMAIL


    private val ongoingActionTrackingSet = HashSet<String>()

    val composePostUseCase = TwetchNewPostUseCase(
            coroutineUtil,
            postCache
    ) {
        authUseCase.authInfo.value?.profile?.id?.toString() ?: throw RuntimeException()
    }

    val coinFlipSignalHelper = CoinFlipSignalHelper(coroutineUtil)

    val postActionHelper = TwetchActionHelper(
        appContext.resources,
        coroutineUtil,
        nodeRuntimeRepository,
        postCache,
        twetchDatabase,
        gson,
        coinManagementUseCase,
        connectivityLiveData,
        composePostUseCase,
        coinFlipSignalHelper,
        ::onStartOfNewAction,
        ::onEndOfAction
    ) {
        authUseCase.authInfo.value?.profile?.id?.toString() ?: throw RuntimeException()
    }



    private val _loadingIndicator = MutableStateFlow(false)
    val loadingIndicator: StateFlow<Boolean>
        get() = _loadingIndicator


    private val _notificationInfo = MutableStateFlow(NotificationInfo(0, -1L))
    val notificationInfo: StateFlow<NotificationInfo>
        get() = _notificationInfo



    val authUseCase = TwetchAuthUseCase(
        coroutineUtil,
        appStateLD,
        profileDatastore
    ) { initializedPaymail ->
        initializedForPaymail = initializedPaymail
    }

    val chatListUseCase = TwetchChatListUseCase(
        coroutineUtil,
        twetchChatInfoStore
    )



    private val chatContentUseCaseCache = HashMap<String, TwetchChatContentUseCase>()
    private val userProfileUseCaseCache = HashMap<String, TwetchUserProfileUseCase>()
    private val postDetailsUseCaseCache = HashMap<String, TwetchPostDetailsUseCase>()

    val notificationDetectorManager = TwetchNotificationDetectorManager(
        coroutineUtil,
        nodeRuntimeRepository,
        gson,
        twetchChatInfoStore
    )

    private var myProfileUseCase: TwetchUserProfileUseCase? = null
    private var followingUseCase: TwetchFollowingUseCase? = null
    private var latestUseCase: TwetchLatestUseCase? = null
    private var topUseCase: TwetchTopUseCase? = null
    private var searchUseCase: TwetchSearchUseCase? = null
    private var notificationUseCase: TwetchNotificationUseCase? = null

    init {
        appStateLD.observeForever { state ->
            state.activePaymail?.let { paymail ->
                if (paymail == initializedForPaymail) return@observeForever

                //clear state when active paymail changes & init the new one
                initForPaymail(true)
            }
        }


        nodeConnectedLiveData.observeForever(object : Observer<Boolean> {
            var didDisconnect = false

            override fun onChanged(isConnected: Boolean) {
                isConnected ?: return

                if (isConnected) {
                    var isFirstStart = false
                    if (!didDisconnect) {
                        isFirstStart = true
                        return
                    }
                    didDisconnect = false


                    //
                    //start periodic refreshing of latest chat-messages
                    if (!isFirstStart) {
                        initForPaymail(false)
                    } // allow `appStateLD` to initially start the message-detector

                } else {
                    Timber.d("didDisconnect set to TRUE")
                    didDisconnect = true

                    //node shut down because the app is not in use ...
                    //stop periodic refreshing of latest chat-messages
                    notificationDetectorManager.clear()
                }
            }

        })


        coroutineUtil.appScope.launch {
            authUseCase.authInfo.collect { info ->
                info?.profile?.let { profile ->

                    if (INVALID_PAYMAIL == initializedForPaymail) throw RuntimeException()

                    val key = TwetchChatInfoStore.ChatInfoKey(
                        initializedForPaymail,
                        profile.id.toString()
                    )
                    chatListUseCase.ensureSetup(key)
                    ensureTwetchMessageDetectorStarted(key)

                } ?: let {
                    Timber.d("clearing chat-message related stuff")
                    chatListUseCase.cleanup()
                    notificationDetectorManager.clear()
                }
            }
        }

    }


    private fun initForPaymail(isActivePaymailChange: Boolean) {
        Timber.d("initializing now")
        clear(isActivePaymailChange)
        authUseCase.refreshAuth(false)

        if (isActivePaymailChange) {
            if (INVALID_PAYMAIL != initializedForPaymail) {
                twetchChatInfoStore.clearAllListeners()
            }

            val onRemovedChatsDetected: suspend (List<String>) -> Unit = lambda@{ list ->
                if (INVALID_PAYMAIL == initializedForPaymail) return@lambda

                Timber.d("onRemovedChatsDetected")

                wipeRemovedChatContent(initializedForPaymail, list)
            }
            val onNewMessagesDetected: suspend (List<TwetchChatInfoStore.ChatNewMessageCount>) -> Unit = {
                Timber.d("onNewMessagesDetected $it")

                refreshMessagesOfChatHelpers(it)
            }

            twetchChatInfoStore.addListeners(
                initializedForPaymail,
                onNewMessagesDetected,
                onRemovedChatsDetected
            )
        }

        //chat list & message-detecting is setup to auto-init on successful auth
    }

    private fun clear(isActivePaymailChange: Boolean) {
        Timber.d("#clear isActivePaymailChange:$isActivePaymailChange")

        initializedForPaymail = INVALID_PAYMAIL

        authUseCase.clear()


        if (isActivePaymailChange) {
            chatListUseCase.cleanup()

            cleanupAllMessageHelpers()
            cleanupAllUserProfileHelpers()

            cleanupMyProfileUseCase()
            cleanupFollowingUseCase()
            cleanupLatestUseCase()
            cleanupTopUseCase()
            cleanupSearchUseCase()
            cleanupNotificationUseCase()

            cleanupAllPostDetails()
        }


        //stop periodic refreshing of latest chat-messages
        notificationDetectorManager.clear()
    }


    private fun onStartOfNewAction(actionId: String): Boolean {
        if (ongoingActionTrackingSet.contains(actionId)) return false
        ongoingActionTrackingSet.add(actionId)

        _loadingIndicator.value = true

        return true
    }

    private fun onEndOfAction(actionId: String) {
        ongoingActionTrackingSet.remove(actionId)

        _loadingIndicator.value = ongoingActionTrackingSet.isNotEmpty()
    }



    private fun ensureTwetchMessageDetectorStarted(key: TwetchChatInfoStore.ChatInfoKey) {
        if (notificationDetectorManager.isStarted) {
            Timber.d("TwetchMessageDetector isStarted == TRUE")
            return
        }

        val onAddedChatsDetected: suspend () -> Unit = suspend {
            Timber.d("onAddedChatsDetected")

            twetchChatInfoStore.refreshChatList(key)
        }
        val onRemovedChatsDetected: suspend (List<String>) -> Unit = { list ->
            Timber.d("onRemovedChatsDetected")

            wipeRemovedChatContent(initializedForPaymail, list)
        }
        val onNewMessagesDetected: suspend (List<TwetchChatInfoStore.ChatNewMessageCount>) -> Unit = {
            Timber.d("onNewMessagesDetected $it")

            twetchChatInfoStore.updateChats(key, it)

            refreshMessagesOfChatHelpers(it)

        }
        val onNotificationInfoUpdated: suspend (Int, Long) -> Unit = { count, millisLastRead ->
            if (count != -1) {
                _notificationInfo.value = NotificationInfo(
                    count,
                    millisLastRead
                )
            }
        }
        val onError: suspend () -> Unit = {
            Timber.d("onErrorFetchingLatestChatActivityInfo")
        }

        notificationDetectorManager.start(
            key,
            onAddedChatsDetected,
            onRemovedChatsDetected,
            onNewMessagesDetected,
            onNotificationInfoUpdated,
            onError
        )
    }


    fun getChatInfo(chatId: String): Flow<TwetchChatInfoStore.ChatInfo?> {
        val paymail = appStateLD.value?.activePaymail ?: throw RuntimeException()
        return twetchChatInfoStore.getChatFlow(paymail, chatId)
    }



    private suspend fun refreshMessagesOfChatHelpers(
            updatedChats: List<TwetchChatInfoStore.ChatNewMessageCount>
    ) {
        chatContentUseCaseCache.values.forEach { chatHelper ->
            val matchingUpdated = updatedChats.find { it.chatId == chatHelper.chatId }
                    ?: return@forEach

            chatHelper.onNewMessagesDetected(
                    initializedForPaymail,
                    matchingUpdated
            )
        }

    }

    private suspend fun wipeRemovedChatContent(
            paymail: String,
            removedList: List<String>
    ) {
        twetchChatInfoStore.removeChats(initializedForPaymail, removedList)

        //handle the matching chats that can be found in the state
        chatContentUseCaseCache.values.forEach { chatHelper ->
            removedList.find { it == chatHelper.chatId } ?: return@forEach

            chatHelper.onChatRemovalDetected()
        }

        //must remove the messages belonging to these chats
        removedList.forEach { chatId ->
            twetchChatMessageStore.clearByKey(TwetchChatMessageStore.Key(
                    paymail,
                    chatId,
                    -1
            ))
        }

        //todo must delete the image-folder ... be careful this could cause issues for other authenticated paymails if they are participants of the same chat

    }



    fun cleanupAllMessageHelpers() {
        chatContentUseCaseCache.values.forEach {
            it.cleanup()
        }
        chatContentUseCaseCache.clear()
    }

    fun ensureChatContentUseCase(chatId: String): TwetchChatContentUseCase {
        var helper = chatContentUseCaseCache[chatId]
        if (helper == null) {
            val paymail = appStateLD.value?.activePaymail ?: throw RuntimeException() //todo use activeUserId instead
            helper = TwetchChatContentUseCase(
                paymail,
                chatId,
                nodeRuntimeRepository,
                notificationDetectorManager,
                twetchChatMessageStore,
                twetchChatInfoStore,
                coroutineUtil,
                gson,
                { _chatId ->
                    getChatInfo(_chatId)
                },
                {
                    initializedForPaymail
                },
                {
                    authUseCase.authInfo.value?.profile
                }
            )
            chatContentUseCaseCache[chatId] = helper
        }

        return helper
    }



    private fun cleanupMyProfileUseCase() {
        myProfileUseCase?.cleanup()
        myProfileUseCase = null
    }

    fun ensureMyProfileContent(activeUserId: String): TwetchUserProfileUseCase {
        return myProfileUseCase ?: let {
            val instance = TwetchUserProfileUseCase(
                activeUserId,
                activeUserId,
                coroutineUtil,
                userProfileBaseStore,
                postListStore,
                TwetchUserProfileUseCase.UserProfileTab.LATEST
            )
            myProfileUseCase = instance

            instance
        }
    }

    fun checkMyProfileContentUseCaseInitialized(): Boolean {
        return myProfileUseCase != null
    }





    fun cleanupAllUserProfileHelpers() {
        userProfileUseCaseCache.values.forEach {
            it.cleanup()
        }
        userProfileUseCaseCache.clear()
    }

    fun ensureUserProfileContent(userId: String, activeUserId: String): TwetchUserProfileUseCase {
        var helper = userProfileUseCaseCache[userId]
        if (helper == null) {
//            val activeUserId = authUseCase.authInfo.value?.profile?.id ?: throw RuntimeException()
            helper = TwetchUserProfileUseCase(
                activeUserId.toString(),
                userId,
                coroutineUtil,
                userProfileBaseStore,
                postListStore,
                TwetchUserProfileUseCase.UserProfileTab.PROFILE
            )
            userProfileUseCaseCache[userId] = helper
        }
        return helper
    }





    fun cleanupFollowingUseCase() {
        followingUseCase?.cleanup()
        followingUseCase = null
    }

    fun ensureFollowingContent(activeUserId: String): TwetchFollowingUseCase {
        return followingUseCase ?: let {
            val instance = TwetchFollowingUseCase(
                activeUserId,
                coroutineUtil,
                postListStore
            )
            followingUseCase = instance

            instance
        }
    }





    fun cleanupLatestUseCase() {
        latestUseCase?.cleanup()
        latestUseCase = null
    }

    fun ensureLatestContent(activeUserId: String): TwetchLatestUseCase {
        return latestUseCase ?: let {
            val instance = TwetchLatestUseCase(
                activeUserId,
                coroutineUtil,
                postListStore
            )
            latestUseCase = instance

            instance
        }
    }






    fun cleanupTopUseCase() {
        topUseCase?.cleanup()
        topUseCase = null
    }

    fun ensureTopContent(activeUserId: String): TwetchTopUseCase {
        return topUseCase ?: let {
            val instance = TwetchTopUseCase(
                activeUserId,
                coroutineUtil,
                postListStore
            )
            topUseCase = instance

            instance
        }
    }







    fun cleanupSearchUseCase() {
        searchUseCase?.cleanup()
        searchUseCase = null
    }

    fun ensureSearchContent(activeUserId: String): TwetchSearchUseCase {
        return searchUseCase ?: let {
            val instance = TwetchSearchUseCase(
                activeUserId,
                coroutineUtil,
                postListStore
            )
            searchUseCase = instance

            instance
        }
    }






    fun cleanupAllPostDetails() {
        postDetailsUseCaseCache.values.forEach {
            it.cleanup()
        }
        postDetailsUseCaseCache.clear()
    }

    fun ensurePostDetailContent(postTxId: String, activeUserId: String): TwetchPostDetailsUseCase {
        var helper = postDetailsUseCaseCache[postTxId]
        if (helper == null) {
            helper = TwetchPostDetailsUseCase(
                activeUserId,
                postTxId,
                coroutineUtil,
                postDetailsStore
            )
            postDetailsUseCaseCache[postTxId] = helper

        }
        return helper
    }



    fun cleanupNotificationUseCase() {
        notificationUseCase?.cleanup()
        notificationUseCase = null
    }

    fun ensureNotificationUseCase(activeUserId: String): TwetchNotificationUseCase {
        return notificationUseCase ?: let {
            val instance = TwetchNotificationUseCase(
                activeUserId,
                coroutineUtil,
                _notificationInfo,
                notificationsStore,
                nodeRuntimeRepository,
                gson
            )
            notificationUseCase = instance
            instance
        }
    }


    data class NotificationInfo(
        val unreadCount: Int,
        val millisLastRead: Long
    )
}
