package app.bitcoin.baemail.twetch.ui

import androidx.coordinatorlayout.widget.CoordinatorLayout
import app.bitcoin.baemail.core.presentation.util.NoSwipeBehavior
import app.bitcoin.baemail.core.presentation.view.ErrorSnackBar
import app.bitcoin.baemail.core.presentation.view.TwetchPopSnackBar
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

class TwetchSnackbarHelper {

    companion object {

        suspend fun displaySnackbar(
            model: Model,
            layout: CoordinatorLayout
        ) {

            suspendCancellableCoroutine<Unit> { continuation ->

                when (model.type) {
                    Type.ERROR -> {
                        val snackBar = ErrorSnackBar.make(layout.context, layout, model.value)
                        snackBar.behavior = NoSwipeBehavior()
                        snackBar.duration = Snackbar.LENGTH_SHORT
                        snackBar.addCallback(object : BaseTransientBottomBar.BaseCallback<ErrorSnackBar>() {
                            override fun onDismissed(transientBottomBar: ErrorSnackBar?, event: Int) {
                                continuation.resume(Unit)
                            }
                        })

                        snackBar.show()
                    }
                    Type.POP -> {
                        val snackBar = TwetchPopSnackBar.make(layout.context, layout, model.value)
                        snackBar.behavior = NoSwipeBehavior()
                        snackBar.duration = Snackbar.LENGTH_SHORT
                        snackBar.addCallback(object : BaseTransientBottomBar.BaseCallback<TwetchPopSnackBar>() {
                            override fun onDismissed(transientBottomBar: TwetchPopSnackBar?, event: Int) {
                                continuation.resume(Unit)
                            }
                        })

                        snackBar.show()

                    }
                }


            }
        }



    }

    enum class Type {
        ERROR,
        POP
    }

    data class Model(
        val type: Type,
        val value: String
    )

}