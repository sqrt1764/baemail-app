package app.bitcoin.baemail.repl.domain.usecase

import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.repl.domain.repository.SavedReplInputRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class RemoveSavedReplInputUseCaseImpl(
    private val authRepository: AuthRepository,
    private val savedReplRepository: SavedReplInputRepository,
    private val dispatcher: CoroutineDispatcher
) : RemoveSavedReplInputUseCase {

    override suspend fun invoke(millisCreated: Long) = withContext(dispatcher) {
        val paymail = authRepository.activePaymail.value?.paymail ?: throw Exception()
        savedReplRepository.removeItem(paymail, millisCreated)
    }

}

interface RemoveSavedReplInputUseCase {
    suspend operator fun invoke(millisCreated: Long)
}