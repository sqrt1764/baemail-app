package app.bitcoin.baemail.repl.presentation

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowInsets
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.doOnLayout
import androidx.core.view.doOnNextLayout
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.drawable.DrawableLineNumbers
import app.bitcoin.baemail.core.presentation.util.AppliedInsets
import app.bitcoin.baemail.core.presentation.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.logs.presentation.LogsActivity
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATReplInput
import app.bitcoin.baemail.core.presentation.view.recycler.ATReplOutput
import app.bitcoin.baemail.core.presentation.view.recycler.ATReplOutputActions
import app.bitcoin.baemail.core.presentation.view.recycler.ATReplReturn
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.twetch.ui.TwetchFilesMimeTypeHelper
import app.bitcoin.baemail.core.presentation.util.doOnApplyWindowInsets
import app.bitcoin.baemail.core.presentation.util.getColorFromAttr
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.math.max
import kotlin.math.min


class ReplFragment : Fragment(R.layout.fragment_repl) {

    companion object {
        const val KEY_OUTPUT_RESERVED_HEIGHT = "output_reserved_height"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ReplViewModel


    private lateinit var contentContainer: ConstraintLayout
    private lateinit var floatingContainer: ConstraintLayout
    private lateinit var recycler: RecyclerView
    private lateinit var input: EditText
    private lateinit var lineNumbers: View
    private lateinit var submit: Button
    private lateinit var clear: Button

    private lateinit var buttonApostrophe: Button
    private lateinit var buttonQuote: Button
    private lateinit var buttonBar: Button
    private lateinit var buttonCurlyOpen: Button
    private lateinit var buttonCurlyClose: Button
    private lateinit var buttonParenthesisOpen: Button
    private lateinit var buttonParenthesisClose: Button
    private lateinit var buttonBracketOpen: Button
    private lateinit var buttonBracketClose: Button
    private lateinit var buttonLessThan: Button
    private lateinit var buttonMoreThan: Button
    private lateinit var buttonNot: Button
    private lateinit var buttonMultiply: Button
    private lateinit var buttonDivide: Button
    private lateinit var buttonPlus: Button
    private lateinit var buttonMinus: Button
    private lateinit var buttonUnderscore: Button
    private lateinit var buttonEquals: Button
    private lateinit var buttonAmp: Button
    private lateinit var buttonPower: Button
    private lateinit var buttonSemicolon: Button

    private lateinit var save: Button

    private val dp: Float by lazy {
        resources.displayMetrics.density
    }

    private lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    private lateinit var adapter: BaseListAdapter

    private val outputReservedHeightLD = MutableLiveData<Int>()
    private val dynamicHeightItem = ATHeightDecorDynamic("0", outputReservedHeightLD)
    private val topGapHeightItem = ATHeightDecorDynamic("1", MutableLiveData<Int>())

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private val cbm: ClipboardManager by lazy {
        requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }

    private val inputWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            s ?: return

            viewModel.onCodeChanged(s.toString())
            lineNumbers.postInvalidate()
        }
    }


    private val availableHeightHelper = AvailableScreenHeightHelper()
    //private val keyboardHeightState = MutableStateFlow(0)
    private val appliedInsetsState = MutableStateFlow(AppliedInsets())


    private val keyboardCoveredHeight = MutableStateFlow(0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactory
        )[ReplViewModel::class.java]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        topGapHeightItem.heightLD.value = (resources.displayMetrics.density * 90).toInt()
        outputReservedHeightLD.value = savedInstanceState?.getInt(KEY_OUTPUT_RESERVED_HEIGHT) ?: 0

        contentContainer = requireView().findViewById(R.id.content_container)
        floatingContainer = requireView().findViewById(R.id.floating_container)
        recycler = requireView().findViewById(R.id.recycler)
        input = requireView().findViewById(R.id.input)
        lineNumbers = requireView().findViewById(R.id.line_numbers)
        submit = requireView().findViewById(R.id.submit)
        clear = requireView().findViewById(R.id.clear)

        buttonApostrophe = requireView().findViewById(R.id.b0)
        buttonQuote = requireView().findViewById(R.id.b1)
        buttonBar = requireView().findViewById(R.id.b2)
        buttonCurlyOpen = requireView().findViewById(R.id.b3)
        buttonCurlyClose = requireView().findViewById(R.id.b4)
        buttonParenthesisOpen = requireView().findViewById(R.id.b5)
        buttonParenthesisClose = requireView().findViewById(R.id.b6)
        buttonBracketOpen = requireView().findViewById(R.id.b7)
        buttonBracketClose = requireView().findViewById(R.id.b8)
        buttonLessThan = requireView().findViewById(R.id.b9)
        buttonMoreThan = requireView().findViewById(R.id.b10)
        buttonNot = requireView().findViewById(R.id.b11)
        buttonMultiply = requireView().findViewById(R.id.b12)
        buttonDivide = requireView().findViewById(R.id.b13)
        buttonPlus = requireView().findViewById(R.id.b14)
        buttonMinus = requireView().findViewById(R.id.b15)
        buttonUnderscore = requireView().findViewById(R.id.b16)
        buttonEquals = requireView().findViewById(R.id.b17)
        buttonAmp = requireView().findViewById(R.id.b18)
        buttonPower = requireView().findViewById(R.id.b19)
        buttonSemicolon = requireView().findViewById(R.id.b20)

        save = requireView().findViewById(R.id.save)

        setupExtraButtonClick(buttonApostrophe, "'")
        setupExtraButtonClick(buttonQuote, "\"")
        setupExtraButtonClick(buttonBar, "|")
        setupExtraButtonClick(buttonCurlyOpen, "{")
        setupExtraButtonClick(buttonCurlyClose, "}")
        setupExtraButtonClick(buttonParenthesisOpen, "(")
        setupExtraButtonClick(buttonParenthesisClose, ")")
        setupExtraButtonClick(buttonBracketOpen, "[")
        setupExtraButtonClick(buttonBracketClose, "]")
        setupExtraButtonClick(buttonLessThan, "<")
        setupExtraButtonClick(buttonMoreThan, ">")
        setupExtraButtonClick(buttonNot, "!")
        setupExtraButtonClick(buttonMultiply, "*")
        setupExtraButtonClick(buttonDivide, "/")
        setupExtraButtonClick(buttonPlus, "+")
        setupExtraButtonClick(buttonMinus, "-")
        setupExtraButtonClick(buttonUnderscore, "_")
        setupExtraButtonClick(buttonEquals, "=")
        setupExtraButtonClick(buttonAmp, "&")
        setupExtraButtonClick(buttonPower, "^")
        setupExtraButtonClick(buttonSemicolon, ";")

        save.setOnClickListener {
            val added = viewModel.onAddToSaved()
            if (added) {
                Toast.makeText(requireContext(), R.string.saved, Toast.LENGTH_SHORT).show()
            }
        }

        clear.setOnClickListener {
            input.setText("")
        }

        submit.setOnClickListener {
            viewModel.runCommand()
        }




        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            availableHeightHelper.setup(
                requireContext(),
                requireActivity().window,
                viewLifecycleOwner,
                { requireView() }
            )

            availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
                height ?: return@Observer

                val maxHeight = contentContainer.height - appliedInsetsState.value.top - appliedInsetsState.value.bottom
                val keyboardHeight = max(maxHeight - height, 0)

                keyboardCoveredHeight.value = keyboardHeight
            })
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            contentContainer.doOnApplyWindowInsets { v, windowInsets, _ ->
                val keyboardInset = max(windowInsets.getInsets(WindowInsets.Type.ime()).bottom, 0)

                if (v.height != 0 && keyboardInset != 0) {
                    val decorViewHeight = requireActivity().window.decorView.height

                    val location = IntArray(2)
                    view.getLocationOnScreen(location)
                    val yPosOfView = location[1]

                    val bottomHeightOutsideView = decorViewHeight - (v.height + yPosOfView)
                    keyboardCoveredHeight.value = keyboardInset - bottomHeightOutsideView
                } else {
                    keyboardCoveredHeight.value = 0
                }

                windowInsets
            }
        }

        contentContainer.doOnNextLayout {
            Timber.d("...contentContainer layout; width:${it.width}; height:${it.height}; isLaidOut:${it.isLaidOut}; isInLayout:${it.isInLayout}")
            it.post {
                ensureLayoutConfiguration(viewModel.config.value.isCollapsed)
            }
        }

        floatingContainer.addOnLayoutChangeListener { v, _, top, _, bottom, _, oldTop, _, oldBottom ->
            if (bottom - top == oldBottom - oldTop) return@addOnLayoutChangeListener
            if (!v.isVisible) return@addOnLayoutChangeListener

            v.post {
                refreshOutputReservedHeight()
            }
        }

        val inputbarElevatedHeight = 6.0f
        val inputbarBackground = MaterialShapeDrawable.createWithElevationOverlay(
            requireContext(),
            inputbarElevatedHeight
        )
        inputbarBackground.fillColor = ColorStateList
            .valueOf(requireContext().getColorFromAttr(R.attr.colorSurfaceVariant))
        inputbarBackground.strokeColor = ColorStateList
            .valueOf(requireContext().getColorFromAttr(R.attr.colorPrimary))
        inputbarBackground.strokeWidth = dp * 2.1f
        inputbarBackground.setCornerSize(dp * 12)

        floatingContainer.background = inputbarBackground

        input.addTextChangedListener(inputWatcher)


        val drawableLineNumbers = DrawableLineNumbers(
            dp,
            (34 * dp).toInt(),
            textSize = input.textSize,
            labelColor = requireContext().getColorFromAttr(R.attr.colorPrimary),
            align = Paint.Align.RIGHT
        )
        drawableLineNumbers.getBaseline = {
            input.baseline
        }
        drawableLineNumbers.getLineHeight = {
            input.lineHeight
        }
        drawableLineNumbers.getScrollY = {
            input.scrollY
        }
        drawableLineNumbers.getLineCount = {
            max(input.lineCount, 1)
        }

        input.setOnScrollChangeListener(object : View.OnScrollChangeListener {
            override fun onScrollChange(v: View?, c: Int, y: Int, oldX: Int, oldY: Int) {
                if (y != oldY) {
                    lineNumbers.postInvalidate()
                }
            }
        })

        lineNumbers.background = drawableLineNumbers

        input.setHorizontallyScrolling(true)

        input.doOnLayout {
            lineNumbers.postInvalidate()
        }


        val terminalInputListener = object : BaseListAdapter.ATTerminalInputListener {
            override fun onCopyClick(code: String) {
                cbm.let { manager ->
                    val clip = ClipData.newPlainText(getString(R.string.code), code)
                    manager.setPrimaryClip(clip)
                    Toast.makeText(requireContext(), R.string.copied, Toast.LENGTH_SHORT).show()
                }
            }
        }

        val terminalReturnListener = object : BaseListAdapter.ATTerminalReturnListener {
            override fun onCopyClick(code: String) {
                cbm.let { manager ->
                    val clip = ClipData.newPlainText(getString(R.string.value), code)
                    manager.setPrimaryClip(clip)
                    Toast.makeText(requireContext(), R.string.copied, Toast.LENGTH_SHORT).show()
                }
            }
        }

        val terminalOutputItemListener = object : BaseListAdapter.ATTerminalOutputActionListener {
            override fun onToggleCollapsed() {
                viewModel.toggleOutputCollapsed()
            }

            override fun getToggleCollapsedLabel(): String {
                return if (viewModel.config.value.isCollapsed) {
                    "▼"//resources.getString(R.string.write)
                } else {
                    "▲"//resources.getString(R.string.read)
                }
            }

            override fun onClear() {
                viewModel.onClear()
            }

            override fun onFullLog() {
                startActivity(Intent(requireActivity(), LogsActivity::class.java), null)
            }

            override fun onFullLogLong() {
                viewModel.deleteAllLogs()
                Toast.makeText(
                    requireContext(),
                    R.string.logs_have_been_deleted,
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onSaved() {
                findNavController().navigate(R.id.action_terminalFragment_to_savedReplInputFragment)
            }

        }

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atTerminalOutputActionsListener = terminalOutputItemListener,
                atTerminalInputListener = terminalInputListener,
                atTerminalReturnListener = terminalReturnListener
            )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)
        layoutManager.stackFromEnd = true

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                keyboardCoveredHeight.collectLatest { height ->
                    //the keyboard is overlapping the input; reposition it
                    floatingContainer.translationY = -1f * height

                    ensureLayoutConfiguration(viewModel.config.value.isCollapsed)
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.code.collectLatest { code ->
                    input.removeTextChangedListener(inputWatcher)

                    if (input.text.toString() != code) {
                        //todo maybe use editable?
                        val currentSelection = input.selectionStart
                        input.setText(code)
                        min(code.length, currentSelection).let {
                            input.setSelection(it)
                        }

                    }

                    input.addTextChangedListener(inputWatcher)
                    lineNumbers.postInvalidate()
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.output.collectLatest { lines ->
                    applyOutput(lines)
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.cachedReplOutputState?.let {
                    layoutManager.onRestoreInstanceState(it)
                }

                viewModel.config.collectLatest { config ->
                    ensureLayoutConfiguration(config.isCollapsed)
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.executionState.collectLatest { executionState ->
                    //todo update the enabled state of the 'run' button
                    //todo
                    //todo
                    //todo
                }
            }
        }


    }


    override fun onStop() {
        imm.hideSoftInputFromWindow(
                requireView().windowToken,
                0
        )

        super.onStop()
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (view == null) return

        viewModel.cachedReplOutputState = layoutManager.onSaveInstanceState()

        outState.putInt(KEY_OUTPUT_RESERVED_HEIGHT, outputReservedHeightLD.value ?: 0)
    }


    private suspend fun applyOutput(output: List<Line>) {

        val outputItemList = ArrayList<AdapterType>()
        outputItemList.add(topGapHeightItem)

        output.mapIndexedNotNull { index, line ->
            if (line is ReplInput) {
                ATReplInput(
                    (index + 1).toString(),
                    line.code
                )
            } else if (line is ReplOutput) {
                ATReplOutput(
                    (index + 1).toString(),
                    line.content
                )
            } else if (line is ReplReturn) {
                ATReplReturn(
                    (index + 1).toString(),
                    line.success,
                    line.data
                )
            } else {
                Timber.e("unexpected model: $line")
                return@mapIndexedNotNull null
            }



        }.toCollection(outputItemList)

        //add action-strip to the output
        outputItemList.add(ATReplOutputActions("0"))

        //add a gap to the bottom of the list
        outputItemList.add(dynamicHeightItem)

        withContext(Dispatchers.Main) {
            recycler.itemAnimator?.let {
                //making sure that the any previous animation get completed
                if (!it.isRunning) {
                    return@let
                }

                suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                    it.isRunning(object : RecyclerView.ItemAnimator.ItemAnimatorFinishedListener {
                        override fun onAnimationsFinished() {
                            cancellableContinuation.resume(Unit)
                        }
                    })

                }
            }


            var shouldForceScrollToBottom = false
            val pos = layoutManager.findLastVisibleItemPosition()
            if (pos == -1 || pos + 1 > outputItemList.size) {
                //shouldForceScrollToBottom = true
            } else {
                val itemAdapterType = adapter.getItemOfPosition(pos)
                if (itemAdapterType is ATHeightDecorDynamic) {
                    shouldForceScrollToBottom = true
                }
            }


            adapter.setItems(outputItemList)
            Timber.d("adapter#setItems  shouldForceScrollToBottom:$shouldForceScrollToBottom")

            //if the content is scrolled to bottom of messages, then
            // await until the adapter has finished animating in the list-items
            // and make sure that the recycler is scrolled to bottom to insure
            // that the newly added messages are in view.
            if (shouldForceScrollToBottom) {
                recycler.itemAnimator?.let {
                    //making sure that the any previous animation get completed
                    if (!it.isRunning) {
                        return@let
                    }

                    suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                        it.isRunning(object : RecyclerView.ItemAnimator.ItemAnimatorFinishedListener {
                            override fun onAnimationsFinished() {
                                cancellableContinuation.resume(Unit)
                            }
                        })

                    }
                }

                val lastCompletelyVisiblePos = layoutManager.findLastCompletelyVisibleItemPosition()
                if (RecyclerView.NO_POSITION == lastCompletelyVisiblePos) {
                    Timber.d("findLastCompletelyVisibleItemPosition: NO_POSITION")
                    return@withContext
                }

                if (outputItemList.size != lastCompletelyVisiblePos + 1) {
                    recycler.smoothScrollToPosition(outputItemList.size -1)
                }
            }
        }
    }


    private fun setupExtraButtonClick(button: Button, symbol: String) {
        button.setOnClickListener {
            val currentSelection = input.selectionStart
            val sb = StringBuilder(input.text.toString())
            sb.insert(currentSelection, symbol)
            input.setText(sb.toString())
            input.setSelection(currentSelection + symbol.length)

        }
    }

    private fun refreshOutputReservedHeight() {
        outputReservedHeightLD.value = (keyboardCoveredHeight.value +
                floatingContainer.height + 8 * dp).toInt()
        Timber.d("floatingContainer height: ${floatingContainer.height}")
    }

    private fun ensureLayoutConfiguration(isOutputCollapsed: Boolean) {
        //fragment-height is required to accomplish the required layout-adjustments
        val maxFragmentHeight = contentContainer.height
        if (contentContainer.height <= 0) {
            Timber.d("ensureLayoutConfiguration ...exited because the view is not laid out")
            return
        }

        Timber.d("ensureLayoutConfiguration")

        val inputHeightWhenOutputExpanded = (140 * dp).toInt()
        val outputHeightWhenOutputCollapsed = (230 * dp).toInt()
        val inputHeightWhenOutputCollapsed = maxFragmentHeight -
                outputHeightWhenOutputCollapsed - keyboardCoveredHeight.value

        if (isOutputCollapsed && input.height != inputHeightWhenOutputCollapsed) {
            val constraintSet = ConstraintSet()
            constraintSet.clone(floatingContainer)
            constraintSet.constrainHeight(R.id.input, inputHeightWhenOutputCollapsed)
            constraintSet.applyTo(floatingContainer)

            floatingContainer.visibility = View.VISIBLE

            Timber.d("ensureLayoutConfiguration set to collapsed")

        } else if (!isOutputCollapsed) {
            if (input.height != inputHeightWhenOutputExpanded) {
                val constraintSet = ConstraintSet()
                constraintSet.clone(floatingContainer)
                constraintSet.constrainHeight(R.id.input, inputHeightWhenOutputExpanded)
                constraintSet.applyTo(floatingContainer)

                floatingContainer.visibility = View.VISIBLE

                Timber.d("ensureLayoutConfiguration set to expanded")
            } else {
                Timber.d("ensureLayoutConfiguration set to expanded; the input does not need resizing, refreshing the output-size")
                refreshOutputReservedHeight()
            }
        }
    }

}