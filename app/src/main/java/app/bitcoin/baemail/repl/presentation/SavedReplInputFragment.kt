package app.bitcoin.baemail.repl.presentation

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.core.presentation.view.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.core.presentation.view.recycler.ATSavedReplInfo
import app.bitcoin.baemail.core.presentation.view.recycler.ATSavedReplInput
import app.bitcoin.baemail.core.presentation.view.recycler.AdapterType
import app.bitcoin.baemail.core.presentation.view.recycler.BaseAdapterHelper
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapter
import app.bitcoin.baemail.core.presentation.view.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.twetch.ui.TwetchFilesMimeTypeHelper
import app.bitcoin.baemail.core.presentation.util.RoundedBottomSheetDialogFragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.inject.Inject
import kotlin.coroutines.resume

class SavedReplInputFragment : RoundedBottomSheetDialogFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ReplViewModel


    private lateinit var recycler: RecyclerView

    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var twetchFilesMimeHelper: TwetchFilesMimeTypeHelper

    private val adapterFactory: BaseListAdapterFactory by lazy {
        BaseListAdapterFactory(
            requireContext(),
            twetchFilesMimeHelper
        )
    }
    lateinit var adapter: BaseListAdapter



    private var wasCreatedOnce = false


    private val heightLD = MutableLiveData<Int>()
    private val dynamicHeightItem = ATHeightDecorDynamic("0", heightLD)


    val cbm: ClipboardManager by lazy {
        requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactory
        )[ReplViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_saved_repl_input, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true

        recycler = requireView().findViewById(R.id.recycler)

        heightLD.value = (resources.displayMetrics.density * 48).toInt()


        val savedInputListener = object : BaseListAdapter.ATSavedReplInputListener {
            override fun onCopy(model: ATSavedReplInput) {
                cbm.let { manager ->
                    val clip = ClipData.newPlainText("value", model.content)
                    manager.setPrimaryClip(clip)
                    Toast.makeText(requireContext(), "Copied", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onRemove(model: ATSavedReplInput) {
                viewModel.onRemoveFromSaved(model.millisSaved)
            }

        }


        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            BaseAdapterHelper(
                adapterFactory,
                atReplSavedInputListener = savedInputListener
            )
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER





        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.savedReplInputState.collectLatest { list ->
                    val savedItemList = mutableListOf<AdapterType>()

                    if (list.isNotEmpty()) {
                        list.map {
                            ATSavedReplInput(
                                it.millisCreated.toString(),
                                it.millisCreated,
                                it.content
                            )
                        }.toCollection(savedItemList)
                    } else {
                        savedItemList.add(ATSavedReplInfo("0"))
                    }

                    savedItemList.add(dynamicHeightItem)

                    recycler.itemAnimator?.let {
                        //making sure that the any previous animation get completed
                        if (!it.isRunning) {
                            return@let
                        }

                        suspendCancellableCoroutine<Unit> { cancellableContinuation ->
                            it.isRunning(object : RecyclerView.ItemAnimator.ItemAnimatorFinishedListener {
                                override fun onAnimationsFinished() {
                                    cancellableContinuation.resume(Unit)
                                }
                            })

                        }
                    }

                    adapter.setItems(savedItemList)
                }
            }
        }

    }


    override fun getPeekHeight(): Int {
        return CONST_UNSET
    }

    override fun shouldFitToContents(): Boolean {
        return true
    }



    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!wasCreatedOnce) return

        val state = layoutManager.onSaveInstanceState()
        viewModel.cachedSavedReplInputState = state
    }


}