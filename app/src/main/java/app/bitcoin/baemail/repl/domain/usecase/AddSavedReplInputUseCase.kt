package app.bitcoin.baemail.repl.domain.usecase

import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.repl.domain.repository.SavedReplInputRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class AddSavedReplInputUseCaseImpl(
    private val authRepository: AuthRepository,
    private val savedReplRepository: SavedReplInputRepository,
    private val dispatcher: CoroutineDispatcher
) : AddSavedReplInputUseCase {

    override suspend fun invoke(content: String) = withContext(dispatcher) {
        val paymail = authRepository.activePaymail.value?.paymail ?: throw Exception()
        savedReplRepository.addItem(paymail, content)
    }

}

interface AddSavedReplInputUseCase {
    suspend operator fun invoke(content: String)
}