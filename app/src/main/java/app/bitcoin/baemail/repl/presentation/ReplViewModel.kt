package app.bitcoin.baemail.repl.presentation

import android.os.Parcelable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.core.domain.usecases.DeleteAllLogsUseCase
import app.bitcoin.baemail.repl.domain.usecase.AddSavedReplInputUseCase
import app.bitcoin.baemail.repl.domain.usecase.GetSavedReplInputsUseCase
import app.bitcoin.baemail.repl.domain.usecase.RemoveSavedReplInputUseCase
import app.bitcoin.baemail.repl.domain.usecase.ReplRunCommandUseCase
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class ReplViewModel @Inject constructor(
    private val deleteAllLogsUseCase: DeleteAllLogsUseCase,
    private val addSavedReplInputUseCase: AddSavedReplInputUseCase,
    private val removeSavedReplInputUseCase: RemoveSavedReplInputUseCase,
    private val replRunCommandUseCase: ReplRunCommandUseCase,
    getSavedReplInputsUseCase: GetSavedReplInputsUseCase,
) : ViewModel() {


    private val _output = MutableStateFlow<List<Line>>(
        """
            |// Welcome to the Wallet-REPL; the most powerful Bitcoin wallet interface. You have the freedom to construct whatever transaction by directly interacting with moneybutton-bsv node-library.
            |
            |// Any console.log(..) output generated during the execution of your commands will be printed here.
            |// Try executing code like this
            |let twetchMe = JSON.stringify(await context.twetch.instance.me());
            |console.log("twetch-me:", twetchMe);
            |
            |// Or maybe?
            |console.log("info:", JSON.stringify(context.availableCoins));
        """.trimMargin().split("\n").let { line ->
            line.map { ReplOutput(it.trim()) }
        }
    )
    val output: StateFlow<List<Line>>
        get() = _output.asStateFlow()



    private val _code = MutableStateFlow("")
    val code: StateFlow<String>
        get() = _code.asStateFlow()


    private val _config = MutableStateFlow(ReplConfig(
        false
    ))
    val config: StateFlow<ReplConfig>
        get() = _config.asStateFlow()


    private val _executionState = MutableStateFlow(Execution.READY)
    val executionState: StateFlow<Execution>
        get() = _executionState


    val savedReplInputState = getSavedReplInputsUseCase().stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = listOf()
    )

    var cachedReplOutputState: Parcelable? = null
    var cachedSavedReplInputState: Parcelable? = null


    private val prettyGson: Gson by lazy {
        val builder = GsonBuilder()
            .setPrettyPrinting()
        builder.create()
    }

    fun toggleOutputCollapsed() {
        config.value.let { c ->
            _config.value = c.copy(isCollapsed = !c.isCollapsed)
        }
    }

    fun onCodeChanged(latest: String) {
        _code.value = latest
    }

    fun onClear() {
        _output.value = emptyList()
    }

    fun deleteAllLogs() {
        viewModelScope.launch {
            deleteAllLogsUseCase()
        }
    }


    fun onAddToSaved(): Boolean {
        val content = _code.value

        if (content.trim().isEmpty()) return false

        viewModelScope.launch {
            try {
                addSavedReplInputUseCase(content)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }

        return true
    }


    fun onRemoveFromSaved(millisCreated: Long) {
        viewModelScope.launch {
            try {
                removeSavedReplInputUseCase(millisCreated)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }


    fun runCommand() {
        val code = code.value

        if (Execution.READY != _executionState.value) {
            return
        }
        if (code.trim().isEmpty()) {
            return
        }
        _executionState.value = Execution.BLOCKED

        viewModelScope.launch {
            //update the output
            ArrayList(output.value).let { updatedOutput ->
                updatedOutput.add(
                    ReplInput(
                        code
                    )
                )

                _output.value = updatedOutput
            }


            //run the command
            val result = replRunCommandUseCase(code)

            if (result is RunScript.Response.Success) {

                val parsedLogs = mutableListOf<ReplOutput>()
                try {
                    result.logs.map {
                        ReplOutput(it)
                    }.toCollection(parsedLogs)

                } catch (e: Exception) {
                    Timber.e(e)
                }

                ArrayList(output.value).let { updatedOutput ->
                    updatedOutput.addAll(parsedLogs)

                    val finalReturnValue = try {
                        val asJson = prettyGson.fromJson(result.content, JsonElement::class.java)
                        prettyGson.toJson(asJson)

                    } catch (e: Exception) {
                        Timber.e(e)
                        result.content
                    }
                    updatedOutput.add(
                        ReplReturn(
                            true,
                            finalReturnValue
                        )
                    )

                    _output.value = updatedOutput
                }

            } else {

                result as RunScript.Response.Fail

                ArrayList(output.value).let { updatedOutput ->
                    updatedOutput.add(
                        ReplReturn(
                            false,
                            result.responseData
                        )
                    )
                    _output.value = updatedOutput
                }

            }

            _executionState.value = Execution.READY

        }
    }

}



enum class Execution {
    READY,
    BLOCKED
}


data class ReplConfig(
    val isCollapsed: Boolean
)


sealed class Line()

data class ReplInput(
    var code: String
) : Line()

data class ReplOutput(
    val content: String
) : Line()

data class ReplReturn(
    val success: Boolean,
    val data: String
) : Line()


