package app.bitcoin.baemail.repl.domain.repository

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.flow.Flow

interface SavedReplInputRepository {
    suspend fun addItem(paymail: String, content: String)
    suspend fun removeItem(paymail: String, millisCreated: Long)
    fun contentFlow(paymail: String): Flow<List<SavedReplInputItem>>
}

@Keep
data class SavedReplInputItem(
    @SerializedName("paymail") val paymail: String,
    @SerializedName("millisCreated") val millisCreated: Long,
    @SerializedName("content") val content: String
)
