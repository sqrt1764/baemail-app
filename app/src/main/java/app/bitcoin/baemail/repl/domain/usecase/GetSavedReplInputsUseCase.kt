package app.bitcoin.baemail.repl.domain.usecase

import app.bitcoin.baemail.core.domain.repository.AuthRepository
import app.bitcoin.baemail.repl.domain.repository.SavedReplInputItem
import app.bitcoin.baemail.repl.domain.repository.SavedReplInputRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext

class GetSavedReplInputsUseCaseImpl(
    private val authRepository: AuthRepository,
    private val savedReplRepository: SavedReplInputRepository,
    private val dispatcher: CoroutineDispatcher
) : GetSavedReplInputsUseCase {
    override fun invoke(): Flow<List<SavedReplInputItem>> =
        authRepository.activePaymail.flatMapLatest { paymail ->
            withContext(dispatcher) {
                paymail?.paymail ?: return@withContext flow {
                    emit(listOf())
                }
                savedReplRepository.contentFlow(paymail.paymail)
            }
        }

}

interface GetSavedReplInputsUseCase {
    operator fun invoke(): Flow<List<SavedReplInputItem>>
}