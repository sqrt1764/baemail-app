package app.bitcoin.baemail.repl.data

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import app.bitcoin.baemail.repl.domain.repository.SavedReplInputItem
import app.bitcoin.baemail.repl.domain.repository.SavedReplInputRepository
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapLatest

class SavedReplInputRepositoryImpl(
    private val ds: DataStore<Preferences>,
    private val gson: Gson
) : SavedReplInputRepository {

    override suspend fun addItem(paymail: String, content: String) {
        ds.edit { prefs ->
            var millisCreated = System.currentTimeMillis()
            var key: Preferences.Key<String> = stringPreferencesKey(
                "saved_${paymail}_${millisCreated}"
            )
            while (prefs.contains(key)) {
                millisCreated++
                key = stringPreferencesKey("saved_${paymail}_${millisCreated}")
            }

            prefs[key] = gson.toJson(
                SavedReplInputItem(paymail, millisCreated, content)
            )
        }
    }

    override suspend fun removeItem(paymail: String, millisCreated: Long) {
        ds.edit { prefs ->
            val key = stringPreferencesKey("saved_${paymail}_${millisCreated}")
            prefs.remove(key)
        }
    }

    override fun contentFlow(paymail: String): Flow<List<SavedReplInputItem>> {
        return ds.data.mapLatest { prefs ->
            val list = mutableListOf<SavedReplInputItem>()
            prefs.asMap().mapNotNull {  entry ->
                if (entry.key.name.startsWith("saved_${paymail}")) {
                    val serialized = entry.value as String
                    return@mapNotNull gson.fromJson(serialized, SavedReplInputItem::class.java)
                }
                return@mapNotNull null

            }.toCollection(list)

            return@mapLatest list
        }
    }

}


