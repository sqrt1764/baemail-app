package app.bitcoin.baemail.repl.domain.usecase

import app.bitcoin.baemail.core.data.nodejs.request.RunScript
import app.bitcoin.baemail.core.domain.repository.NodeRuntimeRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext


class ReplRunCommandUseCaseImpl(
    private val nodeRuntimeRepository: NodeRuntimeRepository,
    private val coroutineDispatcher: CoroutineDispatcher
) : ReplRunCommandUseCase {

    //TODO should this launch a new coroutine which will not be cancelled if this suspend fun is cancelled
    override suspend fun invoke(code: String): RunScript.Response =
        withContext(coroutineDispatcher) {
            return@withContext nodeRuntimeRepository.runAsyncScript(code)
        }

}

interface ReplRunCommandUseCase {
    suspend operator fun invoke(code: String): RunScript.Response
}